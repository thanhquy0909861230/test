package com.deanshoes.AppQAM.adapter.reportadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.fragment.IP_BatchFragment;
import com.deanshoes.AppQAM.model.QAM.RP_LIST_BATCH;

import java.util.ArrayList;

public class ListIPBatchAdapter extends ArrayAdapter<RP_LIST_BATCH> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<RP_LIST_BATCH> items;
    private final int mResource;
    private final String[] name;
    private String[] scale1,scale2;

    public ListIPBatchAdapter(@NonNull Context context, int resource, @NonNull ArrayList<RP_LIST_BATCH> objects,String[] list_name) {
        super(context, resource, 0,objects);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
        name = list_name;

    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_batch = view.findViewById(R.id.tv_batch);
        TextView tv_style = view.findViewById(R.id.tv_style);
        TextView tv_color = view.findViewById(R.id.tv_color);
        TextView tv_hard = view.findViewById(R.id.tv_hard);

        TextView tv_mtr1 = view.findViewById(R.id.tv_mtr1);
        TextView tv_mtr2 = view.findViewById(R.id.tv_mtr2);
        TextView tv_mtr3 = view.findViewById(R.id.tv_mtr3);
        TextView tv_mtr4 = view.findViewById(R.id.tv_mtr4);
        TextView tv_mtr5 = view.findViewById(R.id.tv_mtr5);
        TextView tv_mtr6 = view.findViewById(R.id.tv_mtr6);
        TextView tv_mtr7 = view.findViewById(R.id.tv_mtr7);
        TextView tv_mtr8 = view.findViewById(R.id.tv_mtr8);
        TextView tv_mtr9 = view.findViewById(R.id.tv_mtr9);
        TextView tv_mtr10 = view.findViewById(R.id.tv_mtr10);

        TextView tv_chem1 = view.findViewById(R.id.tv_chem1);
        TextView tv_chem2 = view.findViewById(R.id.tv_chem2);
        TextView tv_chem3 = view.findViewById(R.id.tv_chem3);
        TextView tv_chem4 = view.findViewById(R.id.tv_chem4);
        TextView tv_chem5 = view.findViewById(R.id.tv_chem5);
        TextView tv_chem6 = view.findViewById(R.id.tv_chem6);
        TextView tv_chem7 = view.findViewById(R.id.tv_chem7);
        TextView tv_chem8 = view.findViewById(R.id.tv_chem8);
        TextView tv_chem9 = view.findViewById(R.id.tv_chem9);
        TextView tv_chem10 = view.findViewById(R.id.tv_chem10);
        TextView tv_chem11 = view.findViewById(R.id.tv_chem11);
        TextView tv_chem12 = view.findViewById(R.id.tv_chem12);
        TextView tv_chem13 = view.findViewById(R.id.tv_chem13);
        TextView tv_chem14 = view.findViewById(R.id.tv_chem14);
        TextView tv_chem15 = view.findViewById(R.id.tv_chem15);
        TextView tv_chem16 = view.findViewById(R.id.tv_chem16);
        TextView tv_chem17 = view.findViewById(R.id.tv_chem17);
        TextView tv_chem18 = view.findViewById(R.id.tv_chem18);
        TextView tv_chem19 = view.findViewById(R.id.tv_chem19);
        TextView tv_chem20 = view.findViewById(R.id.tv_chem20);

        TextView tv_pig1 = view.findViewById(R.id.tv_pig1);
        TextView tv_pig2 = view.findViewById(R.id.tv_pig2);
        TextView tv_pig3 = view.findViewById(R.id.tv_pig3);
        TextView tv_pig4 = view.findViewById(R.id.tv_pig4);
        TextView tv_pig5 = view.findViewById(R.id.tv_pig5);
        TextView tv_pig6 = view.findViewById(R.id.tv_pig6);
        TextView tv_pig7 = view.findViewById(R.id.tv_pig7);
        TextView tv_pig8 = view.findViewById(R.id.tv_pig8);
        TextView tv_pig9 = view.findViewById(R.id.tv_pig9);
        TextView tv_pig10 = view.findViewById(R.id.tv_pig10);
        TextView tv_sum1 = view.findViewById(R.id.tv_sum1);
        TextView tv_sum2 = view.findViewById(R.id.tv_sum2);


//        IP_BatchFragment.scale1  = new String[]{};
//        IP_BatchFragment.scale2 = new String[]{};

        scale1 = new String[]{"TAIC","AZO-805","AR316","EVA8502","LS3123","TLA-142","LS1655","UP-55","MD-3","1/10 LS1132",
                "1/10 LS3123","1/10 EVA8502","LS5005","SQ116"};
        scale2 = new String[]{"LS1132","VT-35","STA-1801","EVA5502","EVA4643","LS2506","AC3000","LS5013","LS3125","R103",
                "SK-901","LS1186","9353"};

        RP_LIST_BATCH batch = items.get(position);
        tv_batch.setText(batch.getBTC_BATCH_NO());
        tv_style.setText(batch.getSTYLE());
        tv_color.setText(batch.getCOLOR());
        tv_hard.setText(batch.getHARD());
        tv_sum1.setText("V");
        tv_sum2.setText("V");

        TextView[] list_tv = new TextView[]{tv_mtr1,tv_mtr2,tv_mtr3,tv_mtr4,tv_mtr5,tv_mtr6,tv_mtr7,tv_mtr8,tv_mtr9,tv_mtr10,
                tv_chem1,tv_chem2,tv_chem3,tv_chem4,tv_chem5,tv_chem6,tv_chem7,tv_chem8,tv_chem9,tv_chem10,
                tv_chem11,tv_chem12,tv_chem13,tv_chem14,tv_chem15,tv_chem16,tv_chem17,tv_chem18,tv_chem19,tv_chem20,
                tv_pig1,tv_pig2,tv_pig3,tv_pig4,tv_pig5,tv_pig6,tv_pig7,tv_pig8,tv_pig9,tv_pig10};

        String[] list_check = new String[]{batch.getCHK_MTR1(),batch.getCHK_MTR2(),batch.getCHK_MTR3(),batch.getCHK_MTR4(),
                batch.getCHK_MTR5(),batch.getCHK_MTR6(),batch.getCHK_MTR7(),batch.getCHK_MTR8(),batch.getCHK_MTR9(),
                batch.getCHK_MTR10(),batch.getCHK_CHEM1(),batch.getCHK_CHEM2(),batch.getCHK_CHEM3(),batch.getCHK_CHEM4(),
                batch.getCHK_CHEM5(),batch.getCHK_CHEM6(),batch.getCHK_CHEM7(),batch.getCHK_CHEM8(),batch.getCHK_CHEM9(),
                batch.getCHK_CHEM10(),batch.getCHK_CHEM11(),batch.getCHK_CHEM12(),batch.getCHK_CHEM13(),batch.getCHK_CHEM14(),
                batch.getCHK_CHEM15(),batch.getCHK_CHEM16(),batch.getCHK_CHEM17(),batch.getCHK_CHEM18(),batch.getCHK_CHEM19(),
                batch.getCHK_CHEM20(),batch.getCHK_PIG1(),batch.getCHK_PIG2(),batch.getCHK_PIG3(),batch.getCHK_PIG4(),
                batch.getCHK_PIG5(),batch.getCHK_PIG6(),batch.getCHK_PIG7(),batch.getCHK_PIG8(),batch.getCHK_PIG9(),
                batch.getCHK_PIG10()};

        String[] list_vl = new String[]{batch.getNOTE_MTR1(),batch.getNOTE_MTR2(),batch.getNOTE_MTR3(),batch.getNOTE_MTR4(),
                batch.getNOTE_MTR5(),batch.getNOTE_MTR6(),batch.getNOTE_MTR7(),batch.getNOTE_MTR8(),batch.getNOTE_MTR9(),
                batch.getNOTE_MTR10(),batch.getNOTE_CHEM1(),batch.getNOTE_CHEM2(),batch.getNOTE_CHEM3(),batch.getNOTE_CHEM4(),
                batch.getNOTE_CHEM5(),batch.getNOTE_CHEM6(),batch.getNOTE_CHEM7(),batch.getNOTE_CHEM8(),batch.getNOTE_CHEM9(),
                batch.getNOTE_CHEM10(),batch.getNOTE_CHEM11(),batch.getNOTE_CHEM12(),batch.getNOTE_CHEM13(),batch.getNOTE_CHEM14(),
                batch.getNOTE_CHEM15(),batch.getNOTE_CHEM16(),batch.getNOTE_CHEM17(),batch.getNOTE_CHEM18(),batch.getNOTE_CHEM19(),
                batch.getNOTE_CHEM20(),batch.getNOTE_PIG1(),batch.getNOTE_PIG2(),batch.getNOTE_PIG3(),batch.getNOTE_PIG4(),
                batch.getNOTE_PIG5(),batch.getNOTE_PIG6(),batch.getNOTE_PIG7(),batch.getNOTE_PIG8(),batch.getNOTE_PIG9(),
                batch.getNOTE_PIG10()};
        setValue(list_check,list_tv,list_vl);

        return view;
    }


    private void setValue(String[] chk,TextView[] list_tv,String[] str){
        ArrayList<String> lst1 = new ArrayList<>();
        ArrayList<String> lst2 = new ArrayList<>();
        ArrayList<String> lst3 = new ArrayList<>();

        ArrayList<String> note1 = new ArrayList<>();
        ArrayList<String> note2 = new ArrayList<>();
        ArrayList<String> note3 = new ArrayList<>();

        int pos=0;

        for (int i =0;i<chk.length;i++){
            if (checkValue(chk[i])){
                list_tv[pos].setText(str[i] !=null ? str[i] : "V");
                list_tv[pos].setVisibility(View.VISIBLE);

                pos=pos+1;
            }else{
                list_tv[pos].setVisibility(View.GONE);
            }
        }

        for (int i=0;i<name.length;i++){
            if (checkValue(name[i])){
//                System.out.println(i + "==" +name[i]);
                if (checkScale(scale1,name[i])){
//                    System.out.println("type 1---" +name[i]);
                    lst1.add(chk[i]);
                    note1.add(str[i]);
                }else if (checkScale(scale2,name[i])){
//                    System.out.println("type 2---" +name[i]);
                    lst2.add(chk[i]);
                    note2.add(str[i]);
                }else {
//                    System.out.println("type 3---" +name[i]);
                    lst3.add(chk[i]);
                    note3.add(str[i]);
                }
            }
        }

        for (int i=0;i<lst3.size();i++){
//            if (checkValue(lst3.get(i))){
            list_tv[pos].setText(note3.get(i) !=null ? note3.get(i) : "V");
            list_tv[pos].setVisibility(View.VISIBLE);

            pos=pos+1;
//            }

        }


        pos=13;
        for (int i=0;i<lst1.size();i++){
//            if (checkValue(lst1.get(i))){
            list_tv[pos].setText(note1.get(i) !=null ? note1.get(i) : "V");
            list_tv[pos].setVisibility(View.VISIBLE);

            pos=pos+1;
//            }
        }

        pos=26;
        for (int i=0;i<lst2.size();i++){
//            if (checkValue(lst1.get(i))){
            list_tv[pos].setText(note2.get(i) !=null ? note2.get(i) : "V");
            list_tv[pos].setVisibility(View.VISIBLE);

            pos=pos+1;
            //           }
        }

//        for (int i =0;i<list_tv.length;i++){
//            if (checkValue(chk[i])){
//                list_tv[i].setText(str[i] !=null ? str[i] : "V");
//                list_tv[i].setVisibility(View.VISIBLE);
//            }else{
//                list_tv[i].setVisibility(View.GONE);
//            }
//        }

    }

    private boolean checkValue(String str){
        if (str !=null && !str.equals("")){
            return true;
        }
        return false;
    }

    private boolean checkScale(String[] scale, String val){
        for (String str : scale){
            if (str.toUpperCase().equals(val.toUpperCase())){
                return true;
            }
        }
        return false;
    }

}
