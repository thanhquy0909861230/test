package com.deanshoes.AppQAM.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;


import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.BTC;

import java.util.ArrayList;

public class ListKNAdapter extends ArrayAdapter<BTC> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<BTC> items;
    private final int mResource;
    private int selectedPosition = -1;
    Boolean flag =false;

    public ListKNAdapter(Context context, int resource,
                             ArrayList<BTC> objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;

    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        return createItemView(position, convertView, parent);

    }

    private View createItemView(final int position, View convertView, final ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_style = view.findViewById(R.id.tv_style);
        TextView tv_color = view.findViewById(R.id.tv_color);
        TextView tv_hard = view.findViewById(R.id.tv_hard);
        TextView tv_batch = view.findViewById(R.id.tv_batch);
//        final ImageView imv_edit = view.findViewById(R.id.imv_edit);
//        final ImageView imv_save = view.findViewById(R.id.imv_save);
//        final ImageView imv_cancel = view.findViewById(R.id.imv_cancel);

        RadioButton rdo_check = view.findViewById(R.id.rdo_check);
        BTC btc = items.get(position);
        items.get(position).getBTC_NO();
        btc.getBTC_NO();
        tv_batch.setText(btc.getBTC_BATCH_NO());
        tv_style.setText(btc.getBTC_STYLE());
        tv_color.setText(btc.getBTC_COLOR());
        tv_hard.setText(btc.getBTC_HARD());

        rdo_check.setChecked(position == selectedPosition);
        rdo_check.setTag(position);

//        if (CheckValue(btc.getKN_TEMP_F()) && CheckValue(btc.getKN_TIME_F()) && CheckValue(btc.getKN_TEMP_1())
//                && CheckValue(btc.getKN_TIME_1()) && CheckValue(btc.getKN_TEMP_2()) && CheckValue(btc.getKN_TIME_2())
//                && CheckValue(btc.getKN_TEMP_3()) && CheckValue(btc.getKN_TIME_3()) && CheckValue(btc.getKN_TEMP_4())
//                && CheckValue(btc.getKN_TIME_4()) && CheckValue(btc.getKN_TEMP_END()) && CheckValue(btc.getKN_TIME_END())){
//            tv_batch.setBackgroundColor(Color.parseColor("#088A4B"));
//            tv_style.setBackgroundColor(Color.parseColor("#088A4B"));
//            tv_color.setBackgroundColor(Color.parseColor("#088A4B"));
//            tv_hard.setBackgroundColor(Color.parseColor("#088A4B"));
//        }else if (CheckValue(btc.getKN_TEMP_F_NOTE()) || CheckValue(btc.getKN_TEMP_END_NOTE()) || CheckValue(btc.getKN_TEMP_1_NOTE())
//                || CheckValue(btc.getKN_TEMP_2_NOTE()) || CheckValue(btc.getKN_TEMP_3_NOTE()) || CheckValue(btc.getKN_TEMP_4_NOTE())){
//            tv_batch.setBackgroundColor(Color.parseColor("#FE9A2E"));
//            tv_style.setBackgroundColor(Color.parseColor("#FE9A2E"));
//            tv_color.setBackgroundColor(Color.parseColor("#FE9A2E"));
//            tv_hard.setBackgroundColor(Color.parseColor("#FE9A2E"));
//        }
        if (CheckValue(btc.getKN_TEMP_F()) && CheckValue(btc.getKN_TIME_F()) && CheckValue(btc.getKN_TEMP_1())
                && CheckValue(btc.getKN_TIME_1()) && CheckValue(btc.getKN_TEMP_2()) && CheckValue(btc.getKN_TIME_2())
                && CheckValue(btc.getKN_TEMP_3()) && CheckValue(btc.getKN_TIME_3()) && CheckValue(btc.getKN_TEMP_4())
                && CheckValue(btc.getKN_TIME_4()) && CheckValue(btc.getKN_TEMP_END()) && CheckValue(btc.getKN_TIME_END())
                && btc.getKN_TEMP_F_NOTE()==null && btc.getKN_TEMP_END_NOTE()==null && btc.getKN_TEMP_1_NOTE()==null
                && btc.getKN_TEMP_2_NOTE()==null && btc.getKN_TEMP_3_NOTE()==null && btc.getKN_TEMP_4_NOTE()==null){
            tv_batch.setBackgroundColor(Color.parseColor("#088A4B"));
            tv_style.setBackgroundColor(Color.parseColor("#088A4B"));
            tv_color.setBackgroundColor(Color.parseColor("#088A4B"));
            tv_hard.setBackgroundColor(Color.parseColor("#088A4B"));
        }
        else if (CheckValue(btc.getKN_TEMP_F()) && CheckValue(btc.getKN_TIME_F()) && CheckValue(btc.getKN_TEMP_1())
                && CheckValue(btc.getKN_TIME_1()) && CheckValue(btc.getKN_TEMP_2()) && CheckValue(btc.getKN_TIME_2())
                && CheckValue(btc.getKN_TEMP_3()) && CheckValue(btc.getKN_TIME_3()) && CheckValue(btc.getKN_TEMP_4())
                && CheckValue(btc.getKN_TIME_4()) && CheckValue(btc.getKN_TEMP_END()) && CheckValue(btc.getKN_TIME_END())
                && btc.getKN_TEMP_F_NOTE()!=null || btc.getKN_TEMP_END_NOTE()!=null || btc.getKN_TEMP_1_NOTE()!=null
                || btc.getKN_TEMP_2_NOTE()!=null || btc.getKN_TEMP_3_NOTE()!=null || btc.getKN_TEMP_4_NOTE()!=null)
        {
            tv_batch.setBackgroundColor(Color.parseColor("#FE9A2E"));
            tv_style.setBackgroundColor(Color.parseColor("#FE9A2E"));
            tv_color.setBackgroundColor(Color.parseColor("#FE9A2E"));
            tv_hard.setBackgroundColor(Color.parseColor("#FE9A2E"));

             if (CheckValue(btc.getKN_TEMP_F()) && CheckValue(btc.getKN_TIME_F()) && CheckValue(btc.getKN_TEMP_1())
                     && CheckValue(btc.getKN_TIME_1()) && CheckValue(btc.getKN_TEMP_2()) && CheckValue(btc.getKN_TIME_2())
                     && CheckValue(btc.getKN_TEMP_3()) && CheckValue(btc.getKN_TIME_3()) && CheckValue(btc.getKN_TEMP_4())
                     && CheckValue(btc.getKN_TIME_4()) && CheckValue(btc.getKN_TEMP_END()) && CheckValue(btc.getKN_TIME_END())
                     && UnCheckValue(btc.getKN_TEMP_F_NOTE()) && UnCheckValue(btc.getKN_TEMP_END_NOTE())
                     && UnCheckValue(btc.getKN_TEMP_1_NOTE()) && UnCheckValue(btc.getKN_TEMP_2_NOTE())
                     && UnCheckValue(btc.getKN_TEMP_3_NOTE()) && UnCheckValue(btc.getKN_TEMP_4_NOTE())
             )
            {
            tv_batch.setBackgroundColor(Color.parseColor("#088A4B"));
            tv_style.setBackgroundColor(Color.parseColor("#088A4B"));
            tv_color.setBackgroundColor(Color.parseColor("#088A4B"));
            tv_hard.setBackgroundColor(Color.parseColor("#088A4B"));
          }

        }

        else if (CheckValue(btc.getKN_TEMP_F_NOTE()) || CheckValue(btc.getKN_TEMP_END_NOTE()) || CheckValue(btc.getKN_TEMP_1_NOTE())
                || CheckValue(btc.getKN_TEMP_2_NOTE()) || CheckValue(btc.getKN_TEMP_3_NOTE()) || CheckValue(btc.getKN_TEMP_4_NOTE())){
            tv_batch.setBackgroundColor(Color.parseColor("#FE9A2E"));
            tv_style.setBackgroundColor(Color.parseColor("#FE9A2E"));
            tv_color.setBackgroundColor(Color.parseColor("#FE9A2E"));
            tv_hard.setBackgroundColor(Color.parseColor("#FE9A2E"));
        }


//        if (btc.getAMT_NOTE()!=null){
//            if(btc.getAMT_NOTE().equals("Y")){
//                tv_batch.setBackgroundColor(Color.parseColor("#FE9A2E"));
//                tv_style.setBackgroundColor(Color.parseColor("#FE9A2E"));
//                tv_color.setBackgroundColor(Color.parseColor("#FE9A2E"));
//                tv_hard.setBackgroundColor(Color.parseColor("#FE9A2E"));
//            }
//        }else if (CheckValue(btc.getKN_TEMP_F()) && CheckValue(btc.getKN_TIME_F()) && CheckValue(btc.getKN_TEMP_1())
//                    && CheckValue(btc.getKN_TIME_1()) && CheckValue(btc.getKN_TEMP_2()) && CheckValue(btc.getKN_TIME_2())
//                    && CheckValue(btc.getKN_TEMP_3()) && CheckValue(btc.getKN_TIME_3()) && CheckValue(btc.getKN_TEMP_4())
//                    && CheckValue(btc.getKN_TIME_4()) && CheckValue(btc.getKN_TEMP_END()) && CheckValue(btc.getKN_TIME_END())){
//                tv_batch.setBackgroundColor(Color.parseColor("#088A4B"));
//                tv_style.setBackgroundColor(Color.parseColor("#088A4B"));
//                tv_color.setBackgroundColor(Color.parseColor("#088A4B"));
//                tv_hard.setBackgroundColor(Color.parseColor("#088A4B"));
//        }else if (CheckValue(btc.getKN_TEMP_F_NOTE()) || CheckValue(btc.getKN_TEMP_END_NOTE()) || CheckValue(btc.getKN_TEMP_1_NOTE())
//                || CheckValue(btc.getKN_TEMP_2_NOTE()) || CheckValue(btc.getKN_TEMP_3_NOTE()) || CheckValue(btc.getKN_TEMP_4_NOTE())){
//            tv_batch.setBackgroundColor(Color.parseColor("#FE9A2E"));
//            tv_style.setBackgroundColor(Color.parseColor("#FE9A2E"));
//            tv_color.setBackgroundColor(Color.parseColor("#FE9A2E"));
//            tv_hard.setBackgroundColor(Color.parseColor("#FE9A2E"));
//        }


//        imv_save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((ListView) parent).performItemClick(v, position, 0);
//                imv_save.setVisibility(View.GONE);
//                imv_cancel.setVisibility(View.GONE);
//                imv_edit.setVisibility(View.VISIBLE);
//                rdo_check.setEnabled(true);
//            }
//        });
//
//        imv_cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((ListView) parent).performItemClick(v, position, 1);
//                imv_save.setVisibility(View.GONE);
//                imv_cancel.setVisibility(View.GONE);
//                imv_edit.setVisibility(View.VISIBLE);
//                rdo_check.setEnabled(true);
//            }
//        });
//
//        imv_edit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                ((ListView) parent).performItemClick(v, position, 2);
//                imv_save.setVisibility(View.VISIBLE);
//                imv_cancel.setVisibility(View.VISIBLE);
//                imv_edit.setVisibility(View.GONE);
//                rdo_check.setChecked(true);
//                rdo_check.setEnabled(false);
//
//            }
//        });
        rdo_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 4);
                itemCheckChanged(v);

            }
        });

        if(flag)
        {
            rdo_check.setEnabled(false);
        }
        return view;
    }
    
    private boolean CheckValue(String val){
        if (val!=null && !val.equals("")){
            return true;
        }else return false;
    }

    private boolean UnCheckValue(String val){
        if (val!=null && !val.equals("")){
            return false;
        }else return true;
    }
    
    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }


    public void disableitem(boolean fl){
       flag=fl;
        notifyDataSetChanged();
    }

}