package com.deanshoes.AppQAM.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.QAM_MCS;

import java.util.ArrayList;

public class ListmcsAdapter extends ArrayAdapter<QAM_MCS> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<QAM_MCS> items;
    private final int mResource;
    private int selectedPosition = -1;

    public ListmcsAdapter(Context context, int resource,
                          ArrayList<QAM_MCS> objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_mcs = view.findViewById(R.id.tv_mcs);
        TextView tv_shift = view.findViewById(R.id.tv_shift);
        TextView tv_style = view.findViewById(R.id.tv_style);
        TextView tv_color = view.findViewById(R.id.tv_color);
        TextView tv_hard = view.findViewById(R.id.tv_hard);
        TextView tv_er = view.findViewById(R.id.tv_er);

        RadioButton rdo_check1 = view.findViewById(R.id.rdo_check1);

        QAM_MCS mcs = items.get(position);
        items.get(position).getMCS_NO();
        tv_mcs.setText(mcs.getMCS());
        tv_shift.setText(mcs.getMCS_SHIFT());
        tv_style.setText(mcs.getMCS_STYLE());
        tv_color.setText(mcs.getMCS_COLOR());
        tv_hard.setText(mcs.getMCS_HARD());
        String str = mcs.getER();
        if (str!=null){
            tv_er.setText(str);
//            tv_er.setText(str.substring(0,str.indexOf("±"))+"±"+str.substring(str.indexOf("±")+1)+"%");
        }else tv_er.setText("");


        rdo_check1.setChecked(position == selectedPosition);
        rdo_check1.setTag(position);

        rdo_check1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 4);
                itemCheckChanged(v);

            }
        });

        return view;
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();

    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
