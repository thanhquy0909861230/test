package com.deanshoes.AppQAM.adapter.rpadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.QAM_MODEL;

import java.util.ArrayList;

public class ListModel extends ArrayAdapter<QAM_MODEL> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<QAM_MODEL> items;
    private final int mResource;
    private int selectedPosition = -1;
    Boolean flag =true;

    public ListModel(Context context, int resource,
                     ArrayList<QAM_MODEL> objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }


    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        RadioButton rdo_checkmodel = view.findViewById(R.id.rdo_checkmodel);
        TextView tv_model = view.findViewById(R.id.tv_view);
//        TextView tv_color = view.findViewById(R.id.tv_color);
        QAM_MODEL mcs = items.get(position);
        items.get(position).getSTYLE_NO();
        tv_model.setText(mcs.getSTYLE());

        rdo_checkmodel.setChecked(position == selectedPosition);
        rdo_checkmodel.setTag(position);

        rdo_checkmodel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 4);
                itemCheckChanged(v);

            }
        });

        if(flag==false)
        {
            rdo_checkmodel.setEnabled(false);
        }

        return view;
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();

    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    public void disableitem(boolean fl){
        flag=fl;
        notifyDataSetChanged();
    }
}
