package com.deanshoes.AppQAM.adapter.rpadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;

import com.deanshoes.AppQAM.step4.Temp1;

import java.util.ArrayList;

public class NewBatchNoAdapter extends ArrayAdapter<Temp1> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<Temp1> items;
    private final int mResource;
    private int selectedPosition = -1;

    public NewBatchNoAdapter(Context context, int resource,
                             ArrayList<Temp1> objects){
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;

    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        return createItemView(position, convertView, parent);

    }

    private View createItemView(final int position, View convertView, final ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_new_batch = view.findViewById(R.id.tv_new_batch);
        Temp1 ins= items.get(position);

        items.get(position).getIdColor();
        tv_new_batch.setText(ins.getIdColor());
        return view;
    }
    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

}
