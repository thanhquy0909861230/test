package com.deanshoes.AppQAM.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.QAM_AMT;

import java.util.ArrayList;

public class ListAMTAdapter extends ArrayAdapter<QAM_AMT> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<QAM_AMT> items;
    private final int mResource;
    private int selectedPosition = -1;
    Boolean flag =true;

    public ListAMTAdapter(Context context, int resource,
                              ArrayList<QAM_AMT> objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_batchno = view.findViewById(R.id.tv_batchno);
        TextView tv_check = view.findViewById(R.id.tv_check);

        RadioButton rdo_check = view.findViewById(R.id.rdo_check);

        QAM_AMT amt = items.get(position);
        items.get(position).getAMT_NO();
        tv_batchno.setText(amt.getBTC_BATCH_NO());
        tv_check.setText(amt.getAMT_CHECK());
        String[] lstNote= new String[]{amt.getNOTE_MTR1(),amt.getNOTE_MTR2(),amt.getNOTE_MTR3(),amt.getNOTE_MTR4()
                ,amt.getNOTE_MTR5(),amt.getNOTE_MTR6(),amt.getNOTE_MTR7(),amt.getNOTE_MTR8(),amt.getNOTE_MTR9()
                ,amt.getNOTE_MTR10(),amt.getNOTE_CHEM1(),amt.getNOTE_CHEM2(),amt.getNOTE_CHEM3(),amt.getNOTE_CHEM4()
                ,amt.getNOTE_CHEM5(),amt.getNOTE_CHEM6(),amt.getNOTE_CHEM7(),amt.getNOTE_CHEM8(),amt.getNOTE_CHEM9()
                ,amt.getNOTE_CHEM10(),amt.getNOTE_CHEM11(),amt.getNOTE_CHEM12(),amt.getNOTE_CHEM13(),amt.getNOTE_CHEM14()
                ,amt.getNOTE_CHEM15(),amt.getNOTE_CHEM16(),amt.getNOTE_CHEM17(),amt.getNOTE_CHEM18(),amt.getNOTE_CHEM19()
                ,amt.getNOTE_CHEM20(),amt.getNOTE_PIG1(),amt.getNOTE_PIG2(),amt.getNOTE_PIG3(),amt.getNOTE_PIG4()
                ,amt.getNOTE_PIG5(),amt.getNOTE_PIG6(),amt.getNOTE_PIG7(),amt.getNOTE_PIG8(),amt.getNOTE_PIG9()
                ,amt.getNOTE_PIG10()};
        for (int i=0;i<lstNote.length;i++){
            if (lstNote[i]!=null){
                tv_batchno.setBackgroundColor(Color.parseColor("#FE9A2E"));
                tv_check.setBackgroundColor(Color.parseColor("#FE9A2E"));
                break;
            }
        }


        rdo_check.setChecked(position == selectedPosition);
        rdo_check.setTag(position);

        rdo_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 2);
                itemCheckChanged(v);

            }
        });

        if(flag==false)
        {
            rdo_check.setEnabled(false);
        }
        return view;
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    public void disableitem(boolean fl){
        flag=fl;
        notifyDataSetChanged();
    }
}
