package com.deanshoes.AppQAM.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.QAM_MAC;

import java.util.ArrayList;


public class ListMacAdapter extends ArrayAdapter<QAM_MAC> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<QAM_MAC> items;
    private final int mResource;
    private int selectedPosition = -1;

    public ListMacAdapter(Context context, int resource, ArrayList<QAM_MAC> objects){
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }


    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_macno = view.findViewById(R.id.tv_macno);
        TextView tv_raw1 = view.findViewById(R.id.tv_raw1);
        TextView tv_raw2 = view.findViewById(R.id.tv_raw2);
        TextView tv_raw3 = view.findViewById(R.id.tv_raw3);
        TextView tv_chem1 = view.findViewById(R.id.tv_chem1);
        TextView tv_chem2 = view.findViewById(R.id.tv_chem2);
        TextView tv_chem3 = view.findViewById(R.id.tv_chem3);
        TextView tv_pig1 = view.findViewById(R.id.tv_pig1);
        TextView tv_pig2 = view.findViewById(R.id.tv_pig2);
        TextView tv_pig3 = view.findViewById(R.id.tv_pig3);
        RadioButton rdo_check = view.findViewById(R.id.rdo_check);

        QAM_MAC mac = items.get(position);
        items.get(position).getMAC_NO();
        mac.getMAC_NO();
        tv_macno.setText(mac.getINS_NO());
        tv_raw1.setText(mac.getMAC_MTR1());
        tv_raw2.setText(mac.getMAC_MTR2());
        tv_raw3.setText(mac.getMAC_MTR3());
        tv_chem1.setText(mac.getMAC_CHEM1());
        tv_chem2.setText(mac.getMAC_CHEM2());
        tv_chem3.setText(mac.getMAC_CHEM3());
        tv_pig1.setText(mac.getMAC_PIG1());
        tv_pig2.setText(mac.getMAC_PIG2());
        tv_pig3.setText(mac.getMAC_PIG3());

        rdo_check.setChecked(position == selectedPosition);
        rdo_check.setTag(position);

        rdo_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 4);
                itemCheckChanged(v);

            }
        });

        return view;
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

}
