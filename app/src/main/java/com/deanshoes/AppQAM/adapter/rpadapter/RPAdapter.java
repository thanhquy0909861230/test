package com.deanshoes.AppQAM.adapter.rpadapter;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.report_model.RP_MCS;

import java.util.List;

public class RPAdapter extends ArrayAdapter<RP_MCS> {
    //    boolean ignoreDisabled = false;
    Activity context;
    int resource;
    List<RP_MCS> objects;
    /**
     * @param context
     * @param resource
     * @param objects
     * */
    public RPAdapter(Activity context, int resource, List<RP_MCS> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater= this.context.getLayoutInflater();
        View row = inflater.inflate(this.resource,null);

        TextView txt_btc_no = (TextView) row.findViewById(R.id.txt_mcs);
        /** Set data to row*/
        final RP_MCS qam_rp = this.objects.get(position);
        txt_btc_no.setText(qam_rp.getMCS());
//        if(){}


        return row;

    }

}

