package com.deanshoes.AppQAM.adapter.rpadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.QAM_MTR;

import java.util.ArrayList;

public class ListMTRAdapter extends ArrayAdapter<QAM_MTR> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<QAM_MTR> items;
    private final int mResource;
    private int selectedPosition = -1;
    Boolean flag =true;

    public ListMTRAdapter(Context context, int resource,
                          ArrayList<QAM_MTR> objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_mtr = view.findViewById(R.id.tv_mtr);
//        TextView tv_mtr_weight = view.findViewById(R.id.tv_mtr_weight);
        RadioButton rdo_checkmtr = view.findViewById(R.id.rdo_checkmtr);

        QAM_MTR mcs = items.get(position);
        items.get(position).getMTR_NO();
        tv_mtr.setText(mcs.getMTR());
//        tv_mtr_weight.setText(mcs.getMTR_WEIGHT());


        rdo_checkmtr.setChecked(position == selectedPosition);
        rdo_checkmtr.setTag(position);

        rdo_checkmtr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 4);
                itemCheckChanged(v);

            }
        });

        if(flag==false)
        {
            rdo_checkmtr.setEnabled(false);
        }

        return view;
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();

    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    public void disableitem(boolean fl){
        flag=fl;
        notifyDataSetChanged();
    }
}
