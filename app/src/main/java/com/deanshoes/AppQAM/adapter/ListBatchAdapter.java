package com.deanshoes.AppQAM.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.BTC;
import com.deanshoes.AppQAM.model.QAM.QAM_BTC;

import java.util.ArrayList;

public class ListBatchAdapter extends ArrayAdapter<QAM_BTC> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<QAM_BTC> items;
    private final int mResource;
    private int selectedPosition = -1;

    public ListBatchAdapter(Context context, int resource,
                            ArrayList<QAM_BTC> objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;

    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        return createItemView(position, convertView, parent);

    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_batch_no = view.findViewById(R.id.tv_batch_no);
        TextView tv_shift = view.findViewById(R.id.tv_shift);
        TextView tv_date = view.findViewById(R.id.tv_date);
        TextView tv_style = view.findViewById(R.id.tv_style);
        TextView tv_color = view.findViewById(R.id.tv_color);
        TextView tv_hard = view.findViewById(R.id.tv_hard);

        RadioButton rdo_check = view.findViewById(R.id.rdo_check);

        QAM_BTC btc = items.get(position);
        items.get(position).getBTC_NO();
        tv_batch_no.setText(btc.getBTC_BATCH_NO());
        tv_shift.setText(btc.getBTC_SHIFT());
        tv_date.setText(btc.getBTC_DATE());
        tv_style.setText(btc.getBTC_STYLE());
        tv_color.setText(btc.getBTC_COLOR());
        tv_hard.setText(btc.getBTC_HARD());
        if (btc.getCHECK() != null){
            if (btc.getCHECK().equals("Y")){
                tv_batch_no.setBackgroundColor(Color.parseColor("#088A4B"));
                tv_shift.setBackgroundColor(Color.parseColor("#088A4B"));
                tv_date.setBackgroundColor(Color.parseColor("#088A4B"));
                tv_style.setBackgroundColor(Color.parseColor("#088A4B"));
                tv_color.setBackgroundColor(Color.parseColor("#088A4B"));
                tv_hard.setBackgroundColor(Color.parseColor("#088A4B"));
            }
        }


        rdo_check.setChecked(position == selectedPosition);
        rdo_check.setTag(position);

        rdo_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 4);
                itemCheckChanged(v);

            }
        });

        return view;
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
