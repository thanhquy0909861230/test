package com.deanshoes.AppQAM.adapter.reportadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.BTC_PELL;

import java.util.ArrayList;

public class ListPellAdapter extends ArrayAdapter<BTC_PELL> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<BTC_PELL> items;
    private final int mResource;

    public ListPellAdapter(@NonNull Context context, int resource, @NonNull ArrayList<BTC_PELL> objects) {
        super(context, resource, 0,objects);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_mcs = view.findViewById(R.id.tv_mcs);
        TextView tv_batch = view.findViewById(R.id.tv_batch);
        TextView tv_style = view.findViewById(R.id.tv_style);
        TextView tv_color = view.findViewById(R.id.tv_color);

        TextView tv_time_cre = view.findViewById(R.id.tv_time_cre);
        TextView tv_temp1 = view.findViewById(R.id.tv_temp1);
        TextView tv_temp2 = view.findViewById(R.id.tv_temp2);
        TextView tv_temp3 = view.findViewById(R.id.tv_temp3);
        TextView tv_temp5 = view.findViewById(R.id.tv_tempE);
        TextView tv_temp_head = view.findViewById(R.id.tv_temp_head);
        TextView tv_new_batch = view.findViewById(R.id.tv_new_batch);
        TextView tv_time_mix = view.findViewById(R.id.tv_time_mix);
        TextView tv_temp_out = view.findViewById(R.id.tv_temp_out);
        TextView tv_time_out = view.findViewById(R.id.tv_time_out);
        TextView tv_time_clean = view.findViewById(R.id.tv_time_clean);

        BTC_PELL btc = items.get(position);
        tv_mcs.setText(btc.getMCS());
        tv_batch.setText(btc.getBTC_BATCH_NO());
        tv_style.setText(btc.getBTC_STYLE());
        tv_color.setText(btc.getBTC_COLOR());

        tv_time_cre.setText(btc.getPELL_GRAN());
        tv_temp1.setText(btc.getPELL_TEMP_1_NOTE() == null ? btc.getPELL_TEMP_1() : btc.getPELL_TEMP_1_NOTE());
        tv_temp2.setText(btc.getPELL_TEMP_2_NOTE() == null ? btc.getPELL_TEMP_2() : btc.getPELL_TEMP_2_NOTE());
        tv_temp3.setText(btc.getPELL_TEMP_3_NOTE() == null ? btc.getPELL_TEMP_3() : btc.getPELL_TEMP_3_NOTE());
        tv_temp5.setText(btc.getPELL_TEMP_6_NOTE() == null ? btc.getPELL_TEMP_5() : btc.getPELL_TEMP_6_NOTE());
        tv_temp_head.setText(btc.getPELL_TEMP_HEAD_NOTE() == null ? btc.getPELL_TEMP_HEAD() : btc.getPELL_TEMP_HEAD_NOTE());
        tv_new_batch.setText(btc.getNEW_BATCH_NO());
        tv_time_mix.setText(btc.getPELL_MIXING_TIME());
        tv_temp_out.setText(btc.getPELL_OUT_TEMP_NOTE() == null ? btc.getPELL_OUT_TEMP() : btc.getPELL_OUT_TEMP_NOTE());
        tv_time_out.setText(btc.getPELL_OUT_TIME());
        tv_time_clean.setText(btc.getPELL_CLEAN_TIME());

        return view;

    }
}
