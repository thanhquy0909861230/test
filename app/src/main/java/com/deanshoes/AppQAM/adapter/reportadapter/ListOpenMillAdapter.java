package com.deanshoes.AppQAM.adapter.reportadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.BTC_OP;

import java.util.ArrayList;

public class ListOpenMillAdapter extends ArrayAdapter<BTC_OP> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<BTC_OP> items;
    private final int mResource;

    public ListOpenMillAdapter(@NonNull Context context, int resource, @NonNull ArrayList<BTC_OP> objects) {
        super(context, resource, 0,objects);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_mcs = view.findViewById(R.id.tv_mcs);
        TextView tv_batch = view.findViewById(R.id.tv_batch);
        TextView tv_style = view.findViewById(R.id.tv_style);
        TextView tv_color = view.findViewById(R.id.tv_color);

        TextView tv_temp1 = view.findViewById(R.id.tv_temp1);
        TextView tv_thick1 = view.findViewById(R.id.tv_thick1);
        TextView tv_tempthin = view.findViewById(R.id.tv_tempthin);
        TextView tv_thin = view.findViewById(R.id.tv_thin);
        TextView tv_temp2 = view.findViewById(R.id.tv_temp2);
        TextView tv_thick2 = view.findViewById(R.id.tv_thick2);

        BTC_OP btc = items.get(position);
        tv_mcs.setText(btc.getMCS());
        tv_batch.setText(btc.getBTC_BATCH_NO());
        tv_style.setText(btc.getBTC_STYLE());
        tv_color.setText(btc.getBTC_COLOR());

        tv_temp1.setText(btc.getOP_TEMP_1_NOTE() == null ? btc.getOP_TEMP_1() : btc.getOP_TEMP_1_NOTE());
        tv_thick1.setText(btc.getOP_THICK1_NOTE() == null ? btc.getOP_THICK_1() : btc.getOP_THICK1_NOTE());
        tv_tempthin.setText(btc.getOP_TEMP_THIN());
        tv_thin.setText(btc.getOP_THIN_NOTE() == null ? btc.getOP_THICK_THIN() : btc.getOP_THIN_NOTE());
        tv_temp2.setText(btc.getOP_TEMP_2());
        tv_thick2.setText(btc.getOP_THICK2_NOTE() == null ? btc.getOP_THICK_2() : btc.getOP_THICK2_NOTE());

        return view;
    }
}
