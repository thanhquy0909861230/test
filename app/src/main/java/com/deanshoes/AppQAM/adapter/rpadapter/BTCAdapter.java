package com.deanshoes.AppQAM.adapter.rpadapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.report_model.RP_BTC;

import java.util.ArrayList;

public class BTCAdapter extends ArrayAdapter<RP_BTC> {
    //    boolean ignoreDisabled = false;
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final int mResource;
    private final ArrayList<RP_BTC> items;

    /**
     * @param context
     * @param resource
     * @param objects
     * */
    public BTCAdapter(Context context, int resource, ArrayList<RP_BTC> objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;

    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);

        TextView txt_btc_no = (TextView) view.findViewById(R.id.tv_btc);
        TextView txt_btc_batch_no = (TextView) view.findViewById(R.id.txt_btc_batch_no);
        /** Set data to row*/
        RP_BTC rpBtc = items.get(position);
        txt_btc_no.setText(rpBtc.getBTC_NO());
        txt_btc_batch_no.setText(rpBtc.getBTC_BATCH_NO());
//        if(){}


        return view;

    }

}

