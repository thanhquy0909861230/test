package com.deanshoes.AppQAM.adapter.rpadapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.report_model.BTC_NO1;


import java.util.ArrayList;

public class BTC_NO1Adapter extends ArrayAdapter<BTC_NO1> {
    //    boolean ignoreDisabled = false;
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final int mResource;
    private final ArrayList<BTC_NO1> items;
    /**
     * @param context
     * @param resource
     * @param objects
     * */
    public BTC_NO1Adapter(Context context, int resource, ArrayList<BTC_NO1> objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;

    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);

        TextView txt_btc_no = (TextView) view.findViewById(R.id.txt_btc_batch_no);
        /** Set data to row*/
        BTC_NO1 rpBtc = items.get(position);
        txt_btc_no.setText(rpBtc.getBTC_NO1());

//        if(){}


        return view;

    }

}

