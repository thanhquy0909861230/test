package com.deanshoes.AppQAM.adapter.reportadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.BTC;

import java.util.ArrayList;

public class ListKneaderAdapter extends ArrayAdapter<BTC> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<BTC> items;
    private final int mResource;

    public ListKneaderAdapter(@NonNull Context context, int resource, @NonNull ArrayList<BTC> objects) {
        super(context, resource, 0,objects);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_batch = view.findViewById(R.id.tv_batch);
        TextView tv_style = view.findViewById(R.id.tv_style);
        TextView tv_color = view.findViewById(R.id.tv_color);
        TextView tv_hard = view.findViewById(R.id.tv_hard);

        TextView tv_tempf = view.findViewById(R.id.tv_tempf);
        TextView tv_timef = view.findViewById(R.id.tv_timef);
        TextView tv_temp1 = view.findViewById(R.id.tv_temp1);
        TextView tv_time1 = view.findViewById(R.id.tv_time1);
        TextView tv_temp2 = view.findViewById(R.id.tv_temp2);
        TextView tv_time2 = view.findViewById(R.id.tv_time2);
        TextView tv_temp3 = view.findViewById(R.id.tv_temp3);
        TextView tv_time3 = view.findViewById(R.id.tv_time3);
        TextView tv_temp4 = view.findViewById(R.id.tv_temp4);
        TextView tv_time4 = view.findViewById(R.id.tv_time4);
        TextView tv_tempe = view.findViewById(R.id.tv_tempe);
        TextView tv_timee = view.findViewById(R.id.tv_timee);
        TextView tv_note = view.findViewById(R.id.tv_note);

        BTC btc = items.get(position);
        tv_batch.setText(btc.getBTC_BATCH_NO());
        tv_style.setText(btc.getBTC_STYLE());
        tv_color.setText(btc.getBTC_COLOR());
        tv_hard.setText(btc.getBTC_HARD());

        tv_tempf.setText(btc.getKN_TEMP_F_NOTE() == null ? btc.getKN_TEMP_F() : btc.getKN_TEMP_F_NOTE());
        tv_timef.setText(btc.getKN_TIME_F());
        tv_temp1.setText(btc.getKN_TEMP_1_NOTE() == null ? btc.getKN_TEMP_1() : btc.getKN_TEMP_1_NOTE());
        tv_time1.setText(btc.getKN_TIME_1());
        tv_temp2.setText(btc.getKN_TEMP_2_NOTE() == null ? btc.getKN_TEMP_2() : btc.getKN_TEMP_2_NOTE());
        tv_time2.setText(btc.getKN_TIME_2());
        tv_temp3.setText(btc.getKN_TEMP_3_NOTE() == null ? btc.getKN_TEMP_3() : btc.getKN_TEMP_3_NOTE());
        tv_time3.setText(btc.getKN_TIME_3());
        tv_temp4.setText(btc.getKN_TEMP_4_NOTE() == null ? btc.getKN_TEMP_4() : btc.getKN_TEMP_4_NOTE());
        tv_time4.setText(btc.getKN_TIME_4());
        tv_tempe.setText(btc.getKN_TEMP_END_NOTE() == null ? btc.getKN_TEMP_END() : btc.getKN_TEMP_END_NOTE());
        tv_timee.setText(btc.getKN_TIME_END());
        tv_note.setText(btc.getKN_NOTE());

        return view;
    }
}
