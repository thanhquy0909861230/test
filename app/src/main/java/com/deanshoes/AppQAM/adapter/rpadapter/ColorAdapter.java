package com.deanshoes.AppQAM.adapter.rpadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.report_model.RP_COLOR;


import java.util.List;

public class ColorAdapter extends ArrayAdapter<RP_COLOR> {
    Activity context;
    int resource;
    List<RP_COLOR> objects;
    /**
     * @param context
     * @param resource
     * @param objects
     * */
    public ColorAdapter(Activity context, int resource, List<RP_COLOR> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater= this.context.getLayoutInflater();
        View row = inflater.inflate(this.resource,null);

        TextView txt_btc_no = (TextView) row.findViewById(R.id.txt_color);
        /** Set data to row*/
        final RP_COLOR qam_rp = this.objects.get(position);
        txt_btc_no.setText(qam_rp.getBTC_COLOR());
//        if(){}


        return row;

    }

}