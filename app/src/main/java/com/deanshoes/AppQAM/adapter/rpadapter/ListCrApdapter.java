package com.deanshoes.AppQAM.adapter.rpadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.QAM_CHEM;
import com.deanshoes.AppQAM.model.QAM.QAM_CR;

import java.util.ArrayList;

public class ListCrApdapter extends ArrayAdapter<QAM_CR> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<QAM_CR> items;
    private final int mResource;
    private int selectedPosition = -1;
    Boolean flag =true;

    public ListCrApdapter(Context context, int resource,
                           ArrayList<QAM_CR> objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }
    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_crmcs = view.findViewById(R.id.tv_crmcs);
//        TextView tv_crname = view.findViewById(R.id.tv_crname);
        TextView tv_crfullname = view.findViewById(R.id.tv_crfullname);
        RadioButton rdo_checkcr = view.findViewById(R.id.rdo_checkcr);

        QAM_CR mcs = items.get(position);
        items.get(position).getCR_ID();
        tv_crmcs.setText(mcs.getCR_MCS());
//        tv_crname.setText(mcs.getCR_NAME());
        tv_crfullname.setText(mcs.getCR_FULLNAME());

        rdo_checkcr.setChecked(position == selectedPosition);
        rdo_checkcr.setTag(position);

        rdo_checkcr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 4);
                itemCheckChanged(v);

            }
        });

        if(flag==false)
        {
            rdo_checkcr.setEnabled(false);
        }

        return view;
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();

    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    public void disableitem(boolean fl){
        flag=fl;
        notifyDataSetChanged();
    }
}
