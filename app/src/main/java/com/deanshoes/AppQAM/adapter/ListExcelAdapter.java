package com.deanshoes.AppQAM.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.UploadFile;

import java.util.ArrayList;

public class ListExcelAdapter extends ArrayAdapter<UploadFile> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<UploadFile> items;
    private final int mResource;
    private int selectedPosition = -1;

    public ListExcelAdapter(@NonNull Context context, int resource, ArrayList<UploadFile> objects) {
        super(context, resource,0,objects);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_file = view.findViewById(R.id.tv_file);
        TextView tv_date = view.findViewById(R.id.tv_date);

        RadioButton rdo_checkfile = view.findViewById(R.id.rdo_checkfile);

        UploadFile btc = items.get(position);
        items.get(position).getUP_NO();
        tv_file.setText(btc.getUP_NAME());
        tv_date.setText(btc.getUP_DATE());

        rdo_checkfile.setChecked(position == selectedPosition);
        rdo_checkfile.setTag(position);

        rdo_checkfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 4);
                itemCheckChanged(v);

            }
        });

        return view;
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
