package com.deanshoes.AppQAM.adapter.rpadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.QAM_CHEM;

import java.util.ArrayList;

public class ListChemAdapter extends ArrayAdapter<QAM_CHEM> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<QAM_CHEM> items;
    private final int mResource;
    private int selectedPosition = -1;
    Boolean flag =true;

    public ListChemAdapter(Context context, int resource,
                           ArrayList<QAM_CHEM> objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_chem = view.findViewById(R.id.tv_chem);
//        TextView tv_chem_weight = view.findViewById(R.id.tv_chem_weight);
        RadioButton rdo_checkchem = view.findViewById(R.id.rdo_checkchem);

        QAM_CHEM mcs = items.get(position);
        items.get(position).getCHEM_NO();
        tv_chem.setText(mcs.getCHEM());
//        tv_chem_weight.setText(mcs.getCHEM_WEIGHT());

        rdo_checkchem.setChecked(position == selectedPosition);
        rdo_checkchem.setTag(position);

        rdo_checkchem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 4);
                itemCheckChanged(v);

            }
        });

        if(flag==false)
        {
            rdo_checkchem.setEnabled(false);
        }
        return view;
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();

    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
    public void disableitem(boolean fl){
        flag=fl;
        notifyDataSetChanged();
    }
}
