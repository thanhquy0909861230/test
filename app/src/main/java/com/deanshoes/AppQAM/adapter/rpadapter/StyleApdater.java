package com.deanshoes.AppQAM.adapter.rpadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.report_model.RP_STYLE;

import java.util.List;

public class StyleApdater extends ArrayAdapter<RP_STYLE> {
    Activity context;
    int resource;
    List<RP_STYLE> objects;

    /**
     * @param context
     * @param resource
     * @param objects
     */
    public StyleApdater(Activity context, int resource, List<RP_STYLE> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = this.context.getLayoutInflater();
        View row = inflater.inflate(this.resource, null);

        TextView txt_style = (TextView) row.findViewById(R.id.txt_style);
        /** Set data to row*/
        final RP_STYLE qam_rp = this.objects.get(position);
        txt_style.setText(qam_rp.getBTC_STYLE());
//        if(){}


        return row;

    }
}