package com.deanshoes.AppQAM.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.BTC_OP;

import java.util.ArrayList;

public class ListOPAdapter extends ArrayAdapter<BTC_OP> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<BTC_OP> items;
    private final int mResource;
    private int selectedPosition = -1;
    Boolean flag =false;
    public ListOPAdapter(Context context, int resource,
                         ArrayList<BTC_OP> objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, final ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_style = view.findViewById(R.id.tv_style);
        TextView tv_color = view.findViewById(R.id.tv_color);
        TextView tv_batch = view.findViewById(R.id.tv_batch);
        RadioButton rdo_check = view.findViewById(R.id.rdo_check);

        BTC_OP btc = items.get(position);
        items.get(position).getBTC_NO();
        btc.getBTC_NO();
        tv_batch.setText(btc.getBTC_BATCH_NO());
        tv_style.setText(btc.getBTC_STYLE());
        tv_color.setText(btc.getBTC_COLOR());

        rdo_check.setChecked(position == selectedPosition);
        rdo_check.setTag(position);

        if (CheckValue(btc.getOP_TEMP_1()) && CheckValue(btc.getOP_THICK_1()) && CheckValue(btc.getOP_THICK_THIN())
                && CheckValue(btc.getOP_THICK_2())
                && btc.getOP_TEMP_1_NOTE()==null && btc.getOP_THICK1_NOTE()==null && btc.getOP_THIN_NOTE()==null
                && btc.getOP_THICK2_NOTE()==null){
            tv_batch.setBackgroundColor(Color.parseColor("#088A4B"));
            tv_style.setBackgroundColor(Color.parseColor("#088A4B"));
            tv_color.setBackgroundColor(Color.parseColor("#088A4B"));
        }

        else if (CheckValue(btc.getOP_TEMP_1()) && CheckValue(btc.getOP_THICK_1()) && CheckValue(btc.getOP_THICK_THIN())
                && CheckValue(btc.getOP_THICK_2())
                && btc.getOP_TEMP_1_NOTE()!=null || btc.getOP_THICK1_NOTE()!=null || btc.getOP_THIN_NOTE()!=null
                || btc.getOP_THICK2_NOTE()!=null )
        {
            tv_batch.setBackgroundColor(Color.parseColor("#FE9A2E"));
            tv_style.setBackgroundColor(Color.parseColor("#FE9A2E"));
            tv_color.setBackgroundColor(Color.parseColor("#FE9A2E"));

            if (CheckValue(btc.getOP_TEMP_1()) && CheckValue(btc.getOP_THICK_1()) && CheckValue(btc.getOP_THICK_THIN())
                    && CheckValue(btc.getOP_THICK_2())
                    && UnCheckValue(btc.getOP_TEMP_1_NOTE()) && UnCheckValue(btc.getOP_THICK1_NOTE())
                    && UnCheckValue(btc.getOP_THIN_NOTE()) && UnCheckValue(btc.getOP_THICK2_NOTE())
            )
            {
                tv_batch.setBackgroundColor(Color.parseColor("#088A4B"));
                tv_style.setBackgroundColor(Color.parseColor("#088A4B"));
                tv_color.setBackgroundColor(Color.parseColor("#088A4B"));
            }

        }

        else if (CheckValue(btc.getOP_TEMP_1_NOTE()) || CheckValue(btc.getOP_THICK1_NOTE())
                || CheckValue(btc.getOP_THIN_NOTE()) || CheckValue(btc.getOP_THICK2_NOTE())){
            tv_batch.setBackgroundColor(Color.parseColor("#FE9A2E"));
            tv_style.setBackgroundColor(Color.parseColor("#FE9A2E"));
            tv_color.setBackgroundColor(Color.parseColor("#FE9A2E"));
        }

//        imv_save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((ListView) parent).performItemClick(v, position, 0);
//                imv_save.setVisibility(View.GONE);
//                imv_cancel.setVisibility(View.GONE);
//                imv_edit.setVisibility(View.VISIBLE);
//                rdo_check.setEnabled(true);
//            }
//        });
//
//        imv_cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((ListView) parent).performItemClick(v, position, 1);
//                imv_save.setVisibility(View.GONE);
//                imv_cancel.setVisibility(View.GONE);
//                imv_edit.setVisibility(View.VISIBLE);
//                rdo_check.setEnabled(true);
//            }
//        });
//
//        imv_edit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                ((ListView) parent).performItemClick(v, position, 2);
//                imv_save.setVisibility(View.VISIBLE);
//                imv_cancel.setVisibility(View.VISIBLE);
//                imv_edit.setVisibility(View.GONE);
//                rdo_check.setChecked(true);
//                rdo_check.setEnabled(false);
//
//            }
//        });

        rdo_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 3);
                itemCheckChanged(v);

            }
        });
        if(flag)
        {
            rdo_check.setEnabled(false);
        }
        return view;
    }
    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    private boolean CheckValue(String val){
        if (val!=null && !val.equals("")){
            return true;
        }
        return false;
    }
    private boolean UnCheckValue(String val){
        if (val!=null && !val.equals("")){
            return false;
        }else return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
    public void disableitem(boolean fl){
        flag=fl;
        notifyDataSetChanged();
    }

}