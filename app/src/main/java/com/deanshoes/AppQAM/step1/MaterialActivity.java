package com.deanshoes.AppQAM.step1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.rpadapter.ListMTRAdapter;
import com.deanshoes.AppQAM.model.QAM.QAM_MTR;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
//import com.deanshoes.AppQAM.step2.IPBatchActivity;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MaterialActivity extends AppCompatActivity {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    Button btn_cr,btn_mtr, btn_pig, btn_chem, btn_model, btn_mcs, btn_next, btn_insert, btn_update, btn_save, btn_cancel,btn_delete;
    ImageButton ib_logout;
    String user = "",mtr_no;
    ListView lv_raw;
    EditText edt_raw;
    private List<QAM_MTR> qam_mtrList;
    ArrayList<QAM_MTR> qam_mtrArrayList;
    ListMTRAdapter listMTRAdapter;
    String factory = "FVI";
    int flag = 0;
    int posMTR = -1;
    boolean isselected=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material);
        Intent intent = getIntent();
        final Bundle bundle = intent.getExtras();
        if (bundle != null) {
            user = bundle.getString("user", "ADMIN");
        }
        getmtrlist();
        edt_raw = (EditText) findViewById(R.id.edt_raw);
//        edt_wmtr = (EditText) findViewById(R.id.edt_wmtr);
        lv_raw = (ListView) findViewById(R.id.lv_raw);
        btn_mtr = (Button) findViewById(R.id.btn_mtr);
        btn_pig = (Button) findViewById(R.id.btn_pig);
        btn_chem = (Button) findViewById(R.id.btn_chem);
        btn_model = (Button) findViewById(R.id.btn_model);
        btn_mcs = (Button) findViewById(R.id.btn_mcs);
        btn_next = (Button) findViewById(R.id.btn_next);
//        btn_prev = (Button) findViewById(R.id.btn_prev);
        btn_insert = (Button) findViewById(R.id.btn_insert);
        btn_update = (Button) findViewById(R.id.btn_update);
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cr = (Button) findViewById(R.id.btn_cr);
        ib_logout=findViewById(R.id.ib_logout) ;
        btn_delete = findViewById(R.id.btn_delete);

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MaterialActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });
        btn_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MaterialActivity.this, CrossActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_pig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MaterialActivity.this, PigmentActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_chem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MaterialActivity.this, ChemicalActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MaterialActivity.this, ModelActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_mcs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MaterialActivity.this, McsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MaterialActivity.this, Mac1Activity_FVI.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_insert.setEnabled(false);
                btn_cancel.setEnabled(true);
                btn_save.setEnabled(true);
                ib_logout.setEnabled(false);
                edt_raw.setEnabled(true);
//                edt_wmtr.setEnabled(true);
                btn_update.setEnabled(false);
                edt_raw.getText().clear();
                btn_delete.setEnabled(false);
//                edt_wmtr.getText().clear();
                flag = 1;
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_raw.getText().toString().isEmpty()){
                    ToastCustom.message(getApplicationContext(), "Liệu chủ không thể để trống!", Color.RED);
                }else{
                    if (isselected){
                        btn_delete.setEnabled(true);
                    }
                    if (flag == 1) {
                        final QAM_MTR mtr = new QAM_MTR();
                        mtr.setMTR(edt_raw.getText().toString());
                        mtr.setMTR_WEIGHT(null);
                        mtr.setUP_USER(user);
                        try {
                            CallApi.insertQAM_MTR(factory, mtr, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                                    getmtrlist();
                                    btn_insert.setEnabled(true);
                                    edt_raw.getText().clear();
//                                edt_wmtr.getText().clear();
                                    flag = 0;
//                                edt_wmtr.setEnabled(false);
                                    edt_raw.setEnabled(false);
                                    btn_save.setEnabled(false);
                                    btn_cancel.setEnabled(false);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (flag == 2) {

                        final QAM_MTR mtr = new QAM_MTR();
                        mtr.setMTR_NO(qam_mtrArrayList.get(posMTR).getMTR_NO());
                        mtr.setMTR(edt_raw.getText().toString());
                        mtr.setMTR_WEIGHT(null);
                        mtr.setUP_USER(user);
                        try {
                            CallApi.updatemtr(factory, mtr, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                                    flag = 0;
//                                edt_wmtr.setEnabled(false);
                                    edt_raw.setEnabled(false);
                                    btn_insert.setEnabled(true);
                                    qam_mtrArrayList.set(posMTR, mtr);
                                    listMTRAdapter.notifyDataSetChanged();
                                    btn_save.setEnabled(false);
                                    btn_cancel.setEnabled(false);
                                    btn_update.setEnabled(false);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    ib_logout.setEnabled(true);
                }
            }
        });
        lv_raw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                posMTR = position;
                if (viewId == R.id.rdo_checkmtr) {
                    int vt = position;
//                    btn_cancel.setEnabled(true);
                    btn_update.setEnabled(true);
                    btn_insert.setEnabled(false);
                    edt_raw.setText(qam_mtrArrayList.get(vt).getMTR());
                    btn_delete.setEnabled(true);
                    isselected=true;
                    mtr_no=qam_mtrArrayList.get(vt).getMTR_NO();
//                    edt_wmtr.setText(qam_mtrArrayList.get(vt).getMTR_WEIGHT());
                }
            }
        });
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = 2;
//                edt_wmtr.setEnabled(true);
                edt_raw.setEnabled(true);
                btn_save.setEnabled(true);
                btn_update.setEnabled(false);
                ib_logout.setEnabled(false);
                btn_delete.setEnabled(false);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_cancel.setEnabled(false);
//                edt_wmtr.getText().clear();
                edt_raw.getText().clear();
//                edt_wmtr.setEnabled(false);
                edt_raw.setEnabled(false);
                btn_update.setEnabled(false);
                btn_save.setEnabled(false);
                btn_insert.setEnabled(true);
                ib_logout.setEnabled(true);
                if (isselected){
                    btn_delete.setEnabled(true);
                }
                flag = 0;
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_dialog();
            }
        });
    }

    public void delete_dialog(){
        AlertDialog.Builder b = new AlertDialog.Builder(MaterialActivity.this);
        b.setTitle("Xác nhận");
        b.setMessage("Bạn có muốn xóa liệu chủ này?");
        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                checkMTR();
            }
        });
        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog al = b.create();
        al.show();
    }

    public void checkMTR(){
        try {
            CallApi.checkMTR(factory, edt_raw.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    MatArm matArm = gson.fromJson(response.toString(), MatArm.class);
                    ArrayList<String> lst = matArm.getItems() == null ? new ArrayList<String>() : matArm.getItems();
                    if (lst.size()<=0){
                        try {
                            CallApi.deleteMTR(factory, mtr_no, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                                    getmtrlist();
                                    btn_delete.setEnabled(false);
                                    btn_cancel.setEnabled(false);
                                    btn_update.setEnabled(false);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                        } catch (JSONException e) {
                            ToastCustom.message(getApplicationContext(), "Không thể xóa!", Color.RED);
                            e.printStackTrace();
                        }
                    }else {
                        AlertDialog.Builder b = new AlertDialog.Builder(MaterialActivity.this);
                        b.setTitle("Thông báo!");
                        b.setMessage("Liệu chủ này đã được dùng. Không thể xóa!");
                        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getmtrlist() {
        try {
            CallApi.getmtr(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    MtrArm mtrArm = gson.fromJson(response.toString(), MtrArm.class);
                    qam_mtrList = mtrArm.getItems() == null ? new ArrayList<QAM_MTR>() : mtrArm.getItems();
                    qam_mtrArrayList = new ArrayList<>();
                    for (QAM_MTR sd : qam_mtrList) {
                        qam_mtrArrayList.add(sd);
                    }
                    listMTRAdapter = new ListMTRAdapter(getApplicationContext(), R.layout.list_mtr_adapter, qam_mtrArrayList);
                    lv_raw.setAdapter(listMTRAdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class MtrArm extends ApiReturnModel<ArrayList<QAM_MTR>> {
    }

    public class MatArm extends ApiReturnModel<ArrayList<String>> {
    }
}
