package com.deanshoes.AppQAM.step1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.rpadapter.ListModel;
import com.deanshoes.AppQAM.adapter.rpadapter.ListModelFVI;
import com.deanshoes.AppQAM.model.QAM.QAM_MODEL;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
//import com.deanshoes.AppQAM.step2.IPBatchActivity;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ModelActivity extends AppCompatActivity {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    Button btn_cr, btn_chem, btn_model, btn_mcs, btn_next, btn_insert, btn_update, btn_save, btn_cancel,btn_delete;
    ImageButton ib_logout;
    EditText edt_style, edt_color;
    String user = "";
    String factory = "";
    ArrayList<QAM_MODEL> qam_modelArrayList;
    private List<QAM_MODEL> qam_modelList;
    ListModelFVI listModelAdapter;
    ListView lv_model;
    int flag = 0;
    int posMTR = -1;
    String stl_no;
    boolean isselected=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model);
        Intent intent=getIntent();
        final Bundle bundle=intent.getExtras();
        if (bundle!=null){
            user=bundle.getString("user","ADMIN");
            factory=bundle.getString("fac");
        }
        edt_style = (EditText) findViewById(R.id.edt_model);
        edt_color = (EditText) findViewById(R.id.edt_color);
        lv_model = (ListView) findViewById(R.id.lv_model) ;
//        btn_mtr = (Button) findViewById(R.id.btn_mtr);
//        btn_pig = (Button) findViewById(R.id.btn_pig);
        btn_chem = (Button) findViewById(R.id.btn_chem);
        btn_model = (Button) findViewById(R.id.btn_model);
        btn_mcs = (Button) findViewById(R.id.btn_mcs);
        btn_next = (Button) findViewById(R.id.btn_next);
//        btn_prev = (Button) findViewById(R.id.btn_prev);
        btn_insert = (Button) findViewById(R.id.btn_insert);
        btn_update = (Button) findViewById(R.id.btn_update);
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cr = (Button) findViewById(R.id.btn_cr);
        btn_delete = findViewById(R.id.btn_delete);

        ib_logout=findViewById(R.id.ib_logout) ;

        getModellist();
        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ModelActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btn_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ModelActivity.this, CrossActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btn_chem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(ModelActivity.this, ChemicalActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btn_mcs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent =new Intent(ModelActivity.this, McsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(ModelActivity.this, Mac1Activity_FVI.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_cancel.setEnabled(true);
                btn_insert.setEnabled(false);
                btn_save.setEnabled(true);
                edt_color.setEnabled(true);
                edt_style.setEnabled(true);
                btn_update.setEnabled(false);
                edt_color.getText().clear();
                edt_style.getText().clear();
                ib_logout.setEnabled(false);
                btn_delete.setEnabled(false);
                flag = 1;
                listModelAdapter.disableitem(false);
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_style.getText().toString().isEmpty()){
                    ToastCustom.message(getApplicationContext(), "Hình thể không thể để trống!", Color.RED);
                }
//                else if (edt_color.getText().toString().isEmpty()){
//                    ToastCustom.message(getApplicationContext(), "Màu sắc không thể để trống!", Color.RED);
//                }
                else{
                    if (flag == 1) {
                        final QAM_MODEL model = new QAM_MODEL();
                        model.setSTYLE(edt_style.getText().toString());
                        model.setCOLOR(edt_color.getText().toString());
                        model.setUP_USER(user);
                        try {
                            CallApi.insertQAM_MODEL(factory, model, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    btn_cancel.performClick();
                                    ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                                    getModellist();
                                    edt_color.getText().clear();
                                    edt_style.getText().clear();
                                    flag = 0;
                                    edt_color.setEnabled(false);
                                    edt_color.setEnabled(false);

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (flag == 2) {

                        final QAM_MODEL model = new QAM_MODEL();
                        model.setSTYLE_NO(qam_modelArrayList.get(posMTR).getSTYLE_NO());
                        model.setSTYLE(edt_style.getText().toString());
                        model.setCOLOR(edt_color.getText().toString());
                        model.setUP_USER(user);
                        try {
                            CallApi.updateQAM_MODEL(factory, model, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                                    flag = 0;
                                    edt_color.setEnabled(false);
                                    edt_style.setEnabled(false);
                                    qam_modelArrayList.set(posMTR, model);
                                    listModelAdapter.notifyDataSetChanged();
                                    btn_save.setEnabled(false);
                                    btn_save.setEnabled(false);
                                    btn_cancel.setEnabled(false);
                                    btn_update.setEnabled(false);
                                    btn_insert.setEnabled(true);

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    ib_logout.setEnabled(true);
                    listModelAdapter.disableitem(true);
                }
            }
        });
        lv_model.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                posMTR = position;
                if (viewId == R.id.rdo_checkmodel) {
                    int vt = position;
                    btn_update.setEnabled(true);
//                    btn_insert.setEnabled(false);
//                    btn_cancel.setEnabled(true);
                    edt_style.setText(qam_modelArrayList.get(vt).getSTYLE());
                    edt_color.setText(qam_modelArrayList.get(vt).getCOLOR());
                    stl_no =qam_modelArrayList.get(vt).getSTYLE_NO();
                    btn_delete.setEnabled(true);
                }
            }
        });
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = 2;
                edt_color.setEnabled(true);
                edt_style.setEnabled(true);
                btn_save.setEnabled(true);
                btn_cancel.setEnabled(true);
                btn_insert.setEnabled(false);
                btn_update.setEnabled(false);
                ib_logout.setEnabled(false);
                btn_delete.setEnabled(false);
                listModelAdapter.disableitem(false);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_color.getText().clear();
                edt_style.getText().clear();
                edt_color.setEnabled(false);
                edt_style.setEnabled(false);
                btn_update.setEnabled(false);
                btn_save.setEnabled(false);
                btn_insert.setEnabled(true);
                btn_cancel.setEnabled(false);
                ib_logout.setEnabled(true);
                listModelAdapter.disableitem(true);
                flag = 0;
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_dialog();
            }
        });
    }

    public void delete_dialog(){
        AlertDialog.Builder b = new AlertDialog.Builder(ModelActivity.this);
        b.setTitle("Xác nhận");
        b.setMessage("Bạn có muốn xóa hình thể này?");
        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                checkStyle();
            }
        });
        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog al = b.create();
        al.show();
    }

    public void checkStyle(){
        try {
            CallApi.checkStyle_fvi(factory, edt_style.getText().toString(), edt_color.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    StyleArm styleArm = gson.fromJson(response.toString(), StyleArm.class);
                    ArrayList<String> lst = styleArm.getItems() == null ? new ArrayList<String>() : styleArm.getItems();
                    if (lst.size()<=0){
                        try {
                            CallApi.deleteStyle(factory, stl_no, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                                    getModellist();
                                    btn_delete.setEnabled(false);
                                    btn_update.setEnabled(false);
                                    edt_style.setText("");
                                    edt_color.setText("");
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                        } catch (JSONException e) {
                            ToastCustom.message(getApplicationContext(), "Không thể xóa!", Color.RED);
                            e.printStackTrace();
                        }
                    }else {
                        AlertDialog.Builder b = new AlertDialog.Builder(ModelActivity.this);
                        b.setTitle("Thông báo!");
                        b.setMessage("Hình thể này đã được dùng. Không thể xóa!");
                        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getModellist() {
        try {
            CallApi.getlistmodel(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    ModelArm modelArm = gson.fromJson(response.toString(), ModelArm.class);
                    qam_modelList = modelArm.getItems() == null ? new ArrayList<QAM_MODEL>() : modelArm.getItems();
                    qam_modelArrayList = new ArrayList<>();
                    for (QAM_MODEL df : qam_modelList) {
                        qam_modelArrayList.add(df);
                    }
                    listModelAdapter = new ListModelFVI(getApplicationContext(), R.layout.list_model_adapter_fvi, qam_modelArrayList);
                    lv_model.setAdapter(listModelAdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public class ModelArm extends ApiReturnModel<ArrayList<QAM_MODEL>> {
    }

    public class StyleArm extends ApiReturnModel<ArrayList<String>> {
    }
}
