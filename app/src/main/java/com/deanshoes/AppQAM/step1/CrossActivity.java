package com.deanshoes.AppQAM.step1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.rpadapter.ListCrApdapter;
import com.deanshoes.AppQAM.model.QAM.QAM_CR;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CrossActivity extends AppCompatActivity {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    Button btn_cr, btn_chem, btn_model, btn_mcs, btn_next, btn_insert, btn_update, btn_save, btn_cancel,btn_delete;
    ImageButton ib_logout;
    ListView lv_cross;
    String user = "",crocs_no;
    String factory = "";
    private List<QAM_CR> crList;
    ArrayList<QAM_CR> crArrayList;
    ListCrApdapter listCrApdapter;
    EditText edt_mcs, edt_crname;
    int flag = 0;
    int posMTR = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        final Bundle bundle = intent.getExtras();
        if (bundle != null) {
            user = bundle.getString("user", "ADMIN");
            factory=bundle.getString("fac");
        }

        if (factory.equals("FVI")){
            setContentView(R.layout.activity_cross);
        }else if (factory.equals("FVI_II_305")){
            setContentView(R.layout.activity_cross_fvi2);
        }


        edt_mcs = (EditText) findViewById(R.id.edt_mcs);
        edt_crname = (EditText) findViewById(R.id.edt_crname);
     //   edt_subcr = (EditText) findViewById(R.id.edt_subcr);
        lv_cross = (ListView) findViewById(R.id.lv_cross);
//        btn_mtr = (Button) findViewById(R.id.btn_mtr);
//        btn_pig = (Button) findViewById(R.id.btn_pig);
        btn_chem = (Button) findViewById(R.id.btn_chem);
        btn_model = (Button) findViewById(R.id.btn_model);
        btn_mcs = (Button) findViewById(R.id.btn_mcs);
        btn_next = (Button) findViewById(R.id.btn_next);
//        btn_prev = (Button) findViewById(R.id.btn_prev);
        btn_insert = (Button) findViewById(R.id.btn_insert);
        btn_update = (Button) findViewById(R.id.btn_update);
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cr = (Button) findViewById(R.id.btn_cr);
        btn_delete = findViewById(R.id.btn_delete);
        ib_logout=findViewById(R.id.ib_logout) ;

        getcrlist();

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(CrossActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btn_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CrossActivity.this, CrossActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
//        btn_mtr.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent =new Intent(CrossActivity.this, MaterialActivity.class);
//                intent.putExtras(bundle);
//                startActivity(intent);
//            }
//        });
//        btn_prev.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(CrossActivity.this, MaterialActivity.class);
//                intent.putExtras(bundle);
//                startActivity(intent);
//            }
//        });
//        btn_pig.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(CrossActivity.this, PigmentActivity.class);
//                intent.putExtras(bundle);
//                startActivity(intent);
//            }
//        });
        btn_chem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CrossActivity.this, ChemicalActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (factory.equals("FVI")){
                    Intent intent = new Intent(CrossActivity.this, ModelActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                if (factory.equals("FVI_II_305")){
                    Intent intent = new Intent(CrossActivity.this, ModelActivity_FVI2.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
        btn_mcs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CrossActivity.this, McsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CrossActivity.this, Mac1Activity_FVI.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_cancel.setEnabled(true);
                btn_insert.setEnabled(false);
                btn_save.setEnabled(true);
                ib_logout.setEnabled(false);
                edt_mcs.setEnabled(true);
                edt_crname.setEnabled(true);
                btn_update.setEnabled(false);
                edt_crname.getText().clear();
                edt_mcs.getText().clear();
                btn_delete.setEnabled(false);
                flag = 1;
                listCrApdapter.disableitem(false);
            }
        });
/*        edt_mcs.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                edt_subcr.setText((edt_crname.getText().toString()).substring(9));
            }
        });*/
        //  edt_subcr.setText((edt_crname.getText().toString()).substring(9));
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_mcs.getText().toString().isEmpty()){
                    ToastCustom.message(getApplicationContext(), "MCS không thể để trống!", Color.RED);
                }else if (edt_crname.getText().toString().isEmpty()){
                    ToastCustom.message(getApplicationContext(), "CROCS không thể để trống!", Color.RED);
                }else{
                    if (flag == 1) {
                        final QAM_CR cr = new QAM_CR();
                        cr.setCR_MCS(edt_mcs.getText().toString().replaceAll("\\s+",""));
                        cr.setCR_FULLNAME(edt_crname.getText().toString());
                        String str = edt_crname.getText().toString();
                        if (str.equals("Crocs")){
                            cr.setCR_NAME("Crocs");
                        }else{
                            cr.setCR_NAME(str.substring(str.indexOf(" ")+1));
                        }

                        try {
                            CallApi.insertQAM_CR(factory, cr, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    getcrlist();

                                    ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                                    edt_mcs.getText().clear();
                                    edt_crname.getText().clear();
                                    //  edt_subcr.getText().clear();
                                    flag = 0;
                                    edt_crname.setEnabled(false);
                                    //  edt_subcr.setEnabled(false);
                                    edt_mcs.setEnabled(false);
                                    btn_save.setEnabled(false);
                                    btn_cancel.setEnabled(false);
                                    btn_insert.setEnabled(true);
                                    ib_logout.setEnabled(true);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();

                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (flag == 2) {

                        final QAM_CR pig = new QAM_CR();
                        pig.setCR_ID(crArrayList.get(posMTR).getCR_ID());
                        pig.setCR_MCS(edt_mcs.getText().toString().replaceAll("\\s+",""));
                        pig.setCR_FULLNAME(edt_crname.getText().toString());
                        String str = edt_crname.getText().toString();
                        pig.setCR_NAME(str.substring(str.indexOf(" ")+1));
                        try {
                            CallApi.updateQAM_CR(factory, pig, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                                    flag = 0;
                                    edt_mcs.setEnabled(false);
                                    edt_crname.setEnabled(false);
                                    //  edt_subcr.setEnabled(false);
                                    btn_insert.setEnabled(true);
                                    crArrayList.set(posMTR, pig);
                                    listCrApdapter.notifyDataSetChanged();
                                    btn_save.setEnabled(false);
                                    btn_cancel.setEnabled(false);
                                    btn_update.setEnabled(false);
                                    ib_logout.setEnabled(true);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    listCrApdapter.disableitem(true);
                }
            }
        });
        lv_cross.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                posMTR = position;
                if (viewId == R.id.rdo_checkcr) {
                    int vt = position;
//                    btn_cancel.setEnabled(false);
//                    btn_save.setEnabled(false);
                    btn_update.setEnabled(true);
//                    btn_insert.setEnabled(false);
                    edt_mcs.setText(crArrayList.get(vt).getCR_MCS());
                    edt_crname.setText(crArrayList.get(vt).getCR_FULLNAME());
                    btn_delete.setEnabled(true);
                    crocs_no=crArrayList.get(vt).getCR_ID();
                  //  edt_subcr.setText(crArrayList.get(vt).getCR_NAME());
                }
            }
        });
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = 2;
                edt_crname.setEnabled(true);
                edt_mcs.setEnabled(true);
                btn_save.setEnabled(true);
                btn_insert.setEnabled(false);
                btn_update.setEnabled(false);
                btn_cancel.setEnabled(true);
                ib_logout.setEnabled(false);
                btn_delete.setEnabled(false);
                listCrApdapter.disableitem(false);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_mcs.getText().clear();
                edt_crname.getText().clear();
                edt_mcs.setEnabled(false);
                edt_crname.setEnabled(false);
                btn_update.setEnabled(false);
                btn_save.setEnabled(false);
                btn_cancel.setEnabled(false);
                btn_insert.setEnabled(true);
                ib_logout.setEnabled(true);
                flag = 0;
                listCrApdapter.disableitem(true);
//                getcrlist();

            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_dialog();
            }
        });
    }

    public void delete_dialog(){
        AlertDialog.Builder b = new AlertDialog.Builder(CrossActivity.this);
        b.setTitle("Xác nhận");
        b.setMessage("Bạn có muốn xóa CROCS này?");
        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                checkCrocs();
            }
        });
        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog al = b.create();
        al.show();
    }

    public void checkCrocs(){
        try {
            CallApi.checkCrocs(factory, edt_mcs.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CrocsArm crocsArm = gson.fromJson(response.toString(), CrocsArm.class);
                    ArrayList<String> lst = crocsArm.getItems() == null ? new ArrayList<String>() : crocsArm.getItems();
                    if (lst.size()<=0){
                        try {
                            CallApi.deleteCrocs(factory, crocs_no, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                                    getcrlist();
                                    btn_delete.setEnabled(false);
                                    btn_update.setEnabled(false);
                                    edt_crname.setText("");
                                    edt_mcs.setText("");
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                        } catch (JSONException e) {
                            ToastCustom.message(getApplicationContext(), "Không thể xóa!", Color.RED);
                            e.printStackTrace();
                        }
                    }else {
                        AlertDialog.Builder b = new AlertDialog.Builder(CrossActivity.this);
                        b.setTitle("Thông báo!");
                        b.setMessage("CROCS này đã được dùng. Không thể xóa!");
                        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getcrlist() {
        try {
            CallApi.getcrlist(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CrArm crArm = gson.fromJson(response.toString(), CrArm.class);
                    crList = crArm.getItems() == null ? new ArrayList<QAM_CR>() : crArm.getItems();
                    crArrayList = new ArrayList<>();
                    for (QAM_CR sd : crList) {
                        crArrayList.add(sd);
                        System.out.println(sd.getCR_FULLNAME());
                    }
                    listCrApdapter = new ListCrApdapter(getApplicationContext(), R.layout.cross_adapter, crArrayList);
                    lv_cross.setAdapter(listCrApdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CrArm extends ApiReturnModel<ArrayList<QAM_CR>> {
    }

    public class CrocsArm extends ApiReturnModel<ArrayList<String>> {
    }
}
