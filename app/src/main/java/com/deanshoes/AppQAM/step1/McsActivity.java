package com.deanshoes.AppQAM.step1;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.ListmcsAdapter;
import com.deanshoes.AppQAM.model.QAM.QAM_CR;
import com.deanshoes.AppQAM.model.QAM.QAM_MCS;
import com.deanshoes.AppQAM.model.QAM.QAM_MODEL;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class McsActivity extends AppCompatActivity {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    public Button btn_cr, btn_next3, btn_insert, btn_save, btn_cancel, btn_update,
             btn_chem, btn_model, btn_mcs,btn_delete;
    Spinner spn_shift, spn_mcs, spn_mcs1, spn_style,spn_color;
    ImageButton ib_logout;
    EditText edt_date, edt_hard, edt_mcs,edt_ER,edt_percent;// edt_style,
    String style_update="";
    ArrayList<String> lstColor = new ArrayList<>();
    String user = "",mcs_no="";
    String factory = "";
    private List<QAM_CR> crList, crmcsList;
    private List<QAM_MODEL> qam_modelList;
    ArrayList<QAM_MCS> arraymcsList;
    private DatePickerDialog mDatePickerDialog;
    ListmcsAdapter listMcsAdapter;
    ListView lv_mcs;
    ArrayAdapter adapter, adapter1, adapter2,adaptercolor;
    QAM_MCS qam_mcs;
    mcsArm mcsarm;
    private List<QAM_MCS> qam_mcsList;
    int flag = 0;
    int posMCS = -1;
    int pos=-1;
    boolean flagstyle=false,isselected=false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcs);
        Intent intent = getIntent();
        final Bundle bundle = intent.getExtras();
        if (bundle != null) {
            user = bundle.getString("user", "ADMIN");
            factory=bundle.getString("fac");
        }
        getshift();
        btn_chem = (Button) findViewById(R.id.btn_chem);
        btn_model = (Button) findViewById(R.id.btn_model);
        btn_mcs = (Button) findViewById(R.id.btn_mcs);
        edt_mcs = (EditText) findViewById(R.id.edt_mcs);
        lv_mcs = (ListView) findViewById(R.id.lv_mcs);
        edt_date = (EditText) findViewById(R.id.edt_date);
        spn_color =  findViewById(R.id.spn_color);
        edt_hard = (EditText) findViewById(R.id.edt_hard);
        spn_mcs = (Spinner) findViewById(R.id.spn_mcs);
        spn_style = (Spinner) findViewById(R.id.spn_style);
        spn_mcs1 = (Spinner) findViewById(R.id.spn_mcs1);
        btn_insert = (Button) findViewById(R.id.btn_insert);
        btn_update = (Button) findViewById(R.id.btn_update);
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_next3 = (Button) findViewById(R.id.btn_next3);
        btn_cr = (Button) findViewById(R.id.btn_cr);
        ib_logout=findViewById(R.id.ib_logout) ;
        btn_delete = findViewById(R.id.btn_delete);
        edt_ER =findViewById(R.id.edt_ER);
        edt_percent = findViewById(R.id.edt_percent);

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(McsActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });
        btn_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(McsActivity.this, CrossActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_next3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(McsActivity.this, Mac1Activity_FVI.class);
                myIntent.putExtras(bundle);
                McsActivity.this.startActivity(myIntent);
            }
        });
        btn_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (factory.equals("FVI")){
                    Intent intent = new Intent(McsActivity.this, ModelActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                if (factory.equals("FVI_II_305")){
                    Intent intent = new Intent(McsActivity.this, ModelActivity_FVI2.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });

        spn_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getmcslist(edt_date.getText().toString(), spn_shift.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_chem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(McsActivity.this, ChemicalActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btn_mcs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent =new Intent(McsActivity.this, McsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = 2;
                flagstyle=true;
                getmodel();
                btn_update.setEnabled(false);
                ib_logout.setEnabled(false);
                btn_save.setEnabled(true);
                btn_cancel.setEnabled(true);
                spn_style.setEnabled(true);
                edt_hard.setEnabled(true);
                spn_color.setEnabled(true);
                btn_insert.setEnabled(false);
                btn_delete.setEnabled(false);
                edt_date.setEnabled(false);
                spn_shift.setEnabled(false);
                edt_ER.setEnabled(true);
                edt_percent.setEnabled(true);
            }
        });
        btn_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flagstyle=false;
                if (factory.equals("FVI")){
                    getmodel();
                }else if (factory.equals("FVI_II_305")){
                    getModel_FVI2();
                }
                getshift();
                edt_mcs.setText("");
                getcrname();
                flag = 1;
                spn_mcs.setEnabled(true);
                spn_mcs1.setEnabled(true);
                spn_style.setEnabled(true);
                spn_color.setEnabled(true);
                edt_hard.setEnabled(true);
                edt_ER.setEnabled(true);
                edt_percent.setEnabled(true);
                btn_cancel.setEnabled(true);
                btn_save.setEnabled(true);
                btn_insert.setEnabled(false);
                btn_update.setEnabled(false);
                ib_logout.setEnabled(false);
                btn_delete.setEnabled(false);
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flagstyle=false;
                setdefault();
                edt_mcs.setText("");
                edt_hard.getText().clear();
                edt_hard.setEnabled(false);
                edt_ER.setEnabled(false);
                edt_percent.setEnabled(false);
                edt_percent.setText("");
                edt_ER.setText("");
                spn_color.setAdapter(null);
                getmcslist(edt_date.getText().toString(), spn_shift.getSelectedItem().toString());
                spn_style.setAdapter(null);
                btn_update.setEnabled(false);
                ib_logout.setEnabled(true);
                btn_save.setEnabled(false);
                btn_cancel.setEnabled(false);
                btn_insert.setEnabled(true);
                if (flag==2) {
                    edt_date.setEnabled(true);
                    spn_shift.setEnabled(true);
                }
                flag = 0;
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flagstyle=false;
//                if (isselected){
//                    btn_delete.setEnabled(true);
//                }
                if (flag == 1) {
                    String cr=spn_mcs1.getSelectedItem().toString();
                    String mcsname="";
                    if (cr.equals("L") || cr.equals("S")){
                        mcsname=cr;
                    }else{
                        mcsname = spn_mcs.getSelectedItem().toString() + "|" + spn_mcs1.getSelectedItem().toString();
                    }
                    final QAM_MCS mcs = new QAM_MCS();
                    mcs.setMCS(mcsname);
                    mcs.setUP_USER(user);
                    mcs.setMCS_SHIFT(spn_shift.getSelectedItem().toString());
                    mcs.setMCS_DATE(edt_date.getText().toString());
                    mcs.setMCS_STYLE(spn_style.getSelectedItem().toString());
                    mcs.setMCS_COLOR(spn_color.getSelectedItem().toString());
                    mcs.setMCS_HARD(edt_hard.getText().toString());
                    if (!edt_ER.getText().toString().isEmpty() && !edt_percent.getText().toString().isEmpty()){
                        mcs.setER(edt_ER.getText().toString()+"±"+edt_percent.getText().toString());
                    }

                    try {
                        CallApi.insertmcs(factory, mcs, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                                setdefault();
                                getmcslist(edt_date.getText().toString(), spn_shift.getSelectedItem().toString());
                                btn_save.setEnabled(false);
                                btn_insert.setEnabled(true);
                                btn_cancel.setEnabled(false);
                                spn_style.setAdapter(null);
                                spn_style.setEnabled(false);
                                spn_color.setAdapter(null);
                                spn_color.setEnabled(false);
                                edt_hard.setEnabled(false);
                                edt_hard.getText().clear();
                                edt_ER.setEnabled(false);
                                edt_percent.setEnabled(false);
                                edt_percent.setText("");
                                edt_ER.setText("");
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                }
                if (flag == 2) {

                    final QAM_MCS mcs = new QAM_MCS();
                    mcs.setMCS_NO(arraymcsList.get(posMCS).getMCS_NO());
                    mcs.setMCS(edt_mcs.getText().toString());
                    mcs.setUP_USER(user);
                    mcs.setMCS_SHIFT(spn_shift.getSelectedItem().toString());
                    mcs.setMCS_DATE(edt_date.getText().toString());
                    mcs.setMCS_STYLE(spn_style.getSelectedItem().toString());
                    mcs.setMCS_COLOR(spn_color.getSelectedItem().toString());
                    mcs.setMCS_HARD(edt_hard.getText().toString());
                    if (!edt_ER.getText().toString().isEmpty() && !edt_percent.getText().toString().isEmpty()){
                        mcs.setER(edt_ER.getText().toString()+"±"+edt_percent.getText().toString());
                    }
                    try {
                        CallApi.updatemcsList(factory, mcs, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                                arraymcsList.set(posMCS,mcs);
                                listMcsAdapter.notifyDataSetChanged();
                                btn_save.setEnabled(false);
                                btn_insert.setEnabled(true);
                                btn_cancel.setEnabled(false);
                                spn_style.setEnabled(false);
                                spn_color.setEnabled(false);
                                edt_hard.setEnabled(false);
                                edt_ER.setEnabled(false);
                                edt_percent.setEnabled(false);
                                edt_percent.setText("");
                                edt_ER.setText("");
                                btn_insert.setEnabled(true);
                                edt_date.setEnabled(true);
                                spn_shift.setEnabled(true);

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                ib_logout.setEnabled(true);
            }
        });
        setDateTimeField();
        edt_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mDatePickerDialog.show();
                return false;
            }
        });
        spn_mcs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spn_mcs.getSelectedItem().toString() != null) {
                    getcrmcs();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spn_style.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (factory.equals("FVI")){
                    setcolor();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        lv_mcs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                posMCS = position;
                if (viewId == R.id.rdo_check1) {
                    spn_style.setEnabled(false);
                    spn_color.setEnabled(false);
                    edt_hard.setEnabled(false);
                    btn_delete.setEnabled(true);
                    btn_update.setEnabled(true);
//                    btn_cancel.setEnabled(true);
                    flagstyle=false;
                    setdefault();
                    int vt = position;
                    mcs_no=arraymcsList.get(vt).getMCS_NO();
                    int foo = Integer.parseInt(arraymcsList.get(vt).getMCS_SHIFT());
                    int spinnerPosition = foo - 1;
                    spn_shift.setSelection(spinnerPosition);
                    edt_mcs.setText(arraymcsList.get(vt).getMCS());
                    List<String> list = new ArrayList<String>();
                    list.add(arraymcsList.get(vt).getMCS_STYLE());
                    style_update=arraymcsList.get(vt).getMCS_STYLE();
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, list);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spn_style.setAdapter(dataAdapter);

                    List<String> lst = new ArrayList<String>();
                    lst.add(arraymcsList.get(vt).getMCS_COLOR());
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_dropdown_item, lst);
                    spn_color.setAdapter(adapter);

//                    edt_date.setText(arraymcsList.get(vt).getMCS_DATE());
                    edt_hard.setText(arraymcsList.get(vt).getMCS_HARD());
                    String str = arraymcsList.get(vt).getER();
                    System.out.println(str);
                    if (str!= null){
                        if (str.contains(":")){
                            edt_ER.setText(str.substring(0,str.indexOf(":")));
                            edt_percent.setText(str.substring(str.indexOf(":")+1));
                        }
                        if (str.contains("±")){
                            edt_ER.setText(str.substring(0,str.indexOf("±")));
                            edt_percent.setText(str.substring(str.indexOf("±")+1));
                        }

                    }
                    isselected=true;
                }
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_dialog();
            }
        });
    }

    public void delete_dialog(){
        AlertDialog.Builder b = new AlertDialog.Builder(McsActivity.this);
        b.setTitle("Xác nhận");
        b.setMessage("Bạn có muốn xóa MCS# này?");
        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                checkMCS();
            }
        });
        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog al = b.create();
        al.show();
    }


    public void checkMCS(){
        try {
            CallApi.checkMCS(factory, mcs_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckArm checkArm = gson.fromJson(response.toString(), CheckArm.class);
                    ArrayList<String> lst = checkArm.getItems() == null ? new ArrayList<String>() : checkArm.getItems();
                    if (lst.size()<=0){
                        try {
                            CallApi.deleteMCS(factory, mcs_no, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                                    getmcslist(edt_date.getText().toString(), spn_shift.getSelectedItem().toString());
                                    btn_delete.setEnabled(false);
                                    btn_update.setEnabled(false);
                                    edt_mcs.setText("");
                                    edt_hard.setText("");
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                        } catch (JSONException e) {
                            ToastCustom.message(getApplicationContext(), "Không thể xóa!", Color.RED);
                            e.printStackTrace();
                        }
                    }else {
                        AlertDialog.Builder b = new AlertDialog.Builder(McsActivity.this);
                        b.setTitle("Thông báo!");
                        b.setMessage("MCS# này đã được dùng. Không thể xóa!");
                        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getshift() {
        spn_shift = (Spinner) findViewById(R.id.spn_shift);
        List<String> list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_shift.setAdapter(dataAdapter);
    }

    private void setDateTimeField() {

        Calendar newCalendar = Calendar.getInstance();
        mDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat sd = new SimpleDateFormat("MM/dd/yyyy");
                final Date startDate = newDate.getTime();
                String fdate = sd.format(startDate);
                edt_date.setText(fdate);
                getmcslist(fdate, spn_shift.getSelectedItem().toString());
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
       // mDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }


    public void getcrname() {
        try {
            CallApi.getcrmane(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CrArm crArm = gson.fromJson(response.toString(), CrArm.class);
                    crList = crArm.getItems() == null ? new ArrayList<QAM_CR>() : crArm.getItems();
                    ArrayList<String> arrcrname = new ArrayList<>();
                    for (QAM_CR df : crList) {
                        arrcrname.add(df.getCR_NAME());
                    }
                    if (arrcrname.size()>0){
                        adapter1 = new ArrayAdapter<String>(McsActivity.this, R.layout.check_spiner_item, arrcrname);
                        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spn_mcs.setAdapter(adapter1);
                    }else spn_mcs.setAdapter(null);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getcrmcs() {
        try {
            CallApi.getcrmcs(factory, spn_mcs.getSelectedItem().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CrmcsArm crmcsArm = gson.fromJson(response.toString(), CrmcsArm.class);
                    crmcsList = crmcsArm.getItems() == null ? new ArrayList<QAM_CR>() : crmcsArm.getItems();
                    ArrayList<String> arrcrmcs = new ArrayList<>();
                    for (QAM_CR df : crmcsList) {
                        arrcrmcs.add(df.getCR_MCS());
                    }
                    if (arrcrmcs.size()>0){
                        adapter = new ArrayAdapter<String>(McsActivity.this, R.layout.check_spiner_item, arrcrmcs);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spn_mcs1.setAdapter(adapter);
                    }else {
                        spn_mcs1.setAdapter(null);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getmcslist(String date, String shift) {
        try {
            CallApi.getmcslist(factory,date, shift, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    mcsarm = gson.fromJson(response.toString(), mcsArm.class);
                    qam_mcsList = mcsarm.getItems() == null ? new ArrayList<QAM_MCS>() : mcsarm.getItems();
                    arraymcsList = new ArrayList<>();
                    arraymcsList.addAll(qam_mcsList);
                    listMcsAdapter = new ListmcsAdapter(getApplicationContext(), R.layout.list_mcs_adapter, arraymcsList);
                    lv_mcs.setAdapter(listMcsAdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setdefault() {
        List<String> list = new ArrayList<String>();
        list.add("");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_mcs1.setAdapter(dataAdapter);
        spn_mcs.setAdapter(dataAdapter);

    }

    public void getModel_FVI2(){
        try{
            CallApi.getlistmodel(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    ModelArm modelArm = gson.fromJson(response.toString(), ModelArm.class);
                    qam_modelList = modelArm.getItems() == null ? new ArrayList<QAM_MODEL>() : modelArm.getItems();
                    ArrayList<String> arrmodellist = new ArrayList<>();
                    ArrayList<String> arrcolorlist = new ArrayList<>();
                    for (QAM_MODEL df : qam_modelList) {
                        if (df.getSTYLE()!=null){
                            arrmodellist.add(df.getSTYLE());
                        }
                        if (df.getCOLOR()!=null){
                            arrcolorlist.add(df.getCOLOR());
                        }
                    }
                    Collections.sort(arrmodellist);
                    Collections.sort(arrcolorlist);
                    adapter2 = new ArrayAdapter<String>(McsActivity.this, R.layout.check_spiner_item, arrmodellist);
                    adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spn_style.setAdapter(adapter2);

                    adaptercolor = new ArrayAdapter<>(McsActivity.this, android.R.layout.simple_spinner_dropdown_item, arrcolorlist);
                    spn_color.setAdapter(adaptercolor);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getmodel() {
        try {
            CallApi.getmodel(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    ModelArm modelArm = gson.fromJson(response.toString(), ModelArm.class);
                    qam_modelList = modelArm.getItems() == null ? new ArrayList<QAM_MODEL>() : modelArm.getItems();
                    ArrayList<String> arrmodellist = new ArrayList<>();
                    for (QAM_MODEL df : qam_modelList) {
                        if (df.getSTYLE()!=null){
                            arrmodellist.add(df.getSTYLE());
                        }
                    }
                    adapter2 = new ArrayAdapter<>(McsActivity.this, R.layout.check_spiner_item, arrmodellist);
                    adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spn_style.setAdapter(adapter2);
                    if(flagstyle) {
                        pos = arrmodellist.indexOf(style_update);
                        spn_style.setSelection(pos);
                    }
                    if (arrmodellist.size() < 0) {
                        spn_color.setAdapter(null);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setcolor() {
        try {
            CallApi.setColor(factory, spn_style.getSelectedItem().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    ColorArm colorArm = gson.fromJson(response.toString(), ColorArm.class);
                    lstColor = colorArm.getItems() == null ? new ArrayList<String>() : colorArm.getItems();
                    adaptercolor = new ArrayAdapter<>(McsActivity.this, android.R.layout.simple_spinner_dropdown_item, lstColor);
                    spn_color.setAdapter(adaptercolor);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class CrArm extends ApiReturnModel<ArrayList<QAM_CR>> {
    }

    public class ColorArm extends ApiReturnModel<ArrayList<String>> {
    }

    public class CrmcsArm extends ApiReturnModel<ArrayList<QAM_CR>> {
    }

    public class mcsArm extends ApiReturnModel<ArrayList<QAM_MCS>> {
    }

    public class ModelArm extends ApiReturnModel<ArrayList<QAM_MODEL>> {
    }

    public class CheckArm extends ApiReturnModel<ArrayList<String>> {
    }
}
