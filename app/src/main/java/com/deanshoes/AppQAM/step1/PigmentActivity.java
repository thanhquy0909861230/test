package com.deanshoes.AppQAM.step1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.rpadapter.ListPigAdapter;
import com.deanshoes.AppQAM.model.QAM.QAM_PIG;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
//import com.deanshoes.AppQAM.step2.IPBatchActivity;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PigmentActivity extends AppCompatActivity {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    Button btn_cr,btn_mtr, btn_pig, btn_chem, btn_model, btn_mcs, btn_next,
            btn_insert, btn_update, btn_save, btn_cancel,btn_delete;
    ImageButton ib_logout;
    EditText edt_pig; //edt_wpig
    String user = "",pig_no="";
    String factory = "FVI";
    private List<QAM_PIG> qam_pigs;
    ArrayList<QAM_PIG> qam_pigArrayList;
    ListPigAdapter listPigAdapter;
    ListView lv_pigment;
    int flag = 0;
    int posMTR = -1;
    boolean isselected=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pigment);
        Intent intent=getIntent();
        final Bundle bundle=intent.getExtras();
        if (bundle!=null){
            user=bundle.getString("user","ADMIN");
        }
        getpiglist();
        edt_pig = (EditText) findViewById(R.id.edt_pig);
//                edt_wpig = (EditText) findViewById(R.id.edt_wpig);
        lv_pigment = (ListView) findViewById(R.id.lv_pigment);
        btn_mtr = (Button) findViewById(R.id.btn_mtr);
        btn_pig = (Button) findViewById(R.id.btn_pig);
        btn_chem = (Button) findViewById(R.id.btn_chem);
        btn_model = (Button) findViewById(R.id.btn_model);
        btn_mcs = (Button) findViewById(R.id.btn_mcs);
        btn_next = (Button) findViewById(R.id.btn_next);
//        btn_prev = (Button) findViewById(R.id.btn_prev);
        btn_insert = (Button) findViewById(R.id.btn_insert);
        btn_update = (Button) findViewById(R.id.btn_update);
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cr = (Button) findViewById(R.id.btn_cr);
        ib_logout=findViewById(R.id.ib_logout) ;
        btn_delete = findViewById(R.id.btn_delete);

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(PigmentActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });
        btn_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PigmentActivity.this, CrossActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
//        btn_prev.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent =new Intent(PigmentActivity.this, MaterialActivity.class);
//                intent.putExtras(bundle);
//                startActivity(intent);
//            }
//        });
        btn_mtr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(PigmentActivity.this, MaterialActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_pig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(PigmentActivity.this, PigmentActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_chem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(PigmentActivity.this, ChemicalActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(PigmentActivity.this, ModelActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_mcs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent =new Intent(PigmentActivity.this, McsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(PigmentActivity.this, Mac1Activity_FVI.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_cancel.setEnabled(true);
                btn_save.setEnabled(true);
                edt_pig.setEnabled(true);
//                edt_wpig.setEnabled(true);
                btn_update.setEnabled(false);
                ib_logout.setEnabled(false);
                edt_pig.getText().clear();
                btn_delete.setEnabled(false);
//               edt_wpig.getText().clear();
                flag = 1;
                btn_insert.setEnabled(false);
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_pig.getText().toString().isEmpty()){
                    ToastCustom.message(getApplicationContext(), "Liệu màu không thể để trống!", Color.RED);
                }else{
                    ib_logout.setEnabled(true);
                    if (isselected){
                        btn_delete.setEnabled(true);
                    }
                    if (flag == 1) {
                        final QAM_PIG pig = new QAM_PIG();
                        pig.setPIG(edt_pig.getText().toString());
                        pig.setPIG_WEIGHT(null);
                        pig.setUP_USER(user);
                        try {
                            CallApi.insertQAM_PIG(factory, pig, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                                    getpiglist();
                                    edt_pig.getText().clear();
//                                edt_wpig.getText().clear();
                                    flag = 0;
                                    edt_pig.setEnabled(false);
//                                edt_wpig.setEnabled(false);
                                    btn_save.setEnabled(false);
                                    btn_cancel.setEnabled(false);
                                    btn_insert.setEnabled(true);

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (flag == 2) {

                        final QAM_PIG pig = new QAM_PIG();
                        pig.setPIG_NO(qam_pigArrayList.get(posMTR).getPIG_NO());
                        pig.setPIG(edt_pig.getText().toString());
                        pig.setPIG_WEIGHT(null);
                        pig.setUP_USER(user);
                        try {
                            CallApi.updateQAM_PIG(factory, pig, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                                    flag = 0;
                                    edt_pig.setEnabled(false);
//                                edt_wpig.setEnabled(false);
                                    btn_insert.setEnabled(true);
                                    qam_pigArrayList.set(posMTR, pig);
                                    listPigAdapter.notifyDataSetChanged();
                                    btn_save.setEnabled(false);
                                    btn_cancel.setEnabled(false);
                                    btn_update.setEnabled(false);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        });
        lv_pigment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                posMTR = position;
                if (viewId == R.id.rdo_checkpig) {
                    int vt = position;
                    btn_update.setEnabled(true);
                    btn_insert.setEnabled(false);
//                    edt_wpig.setText(qam_pigArrayList.get(vt).getPIG_WEIGHT());
                    edt_pig.setText(qam_pigArrayList.get(vt).getPIG());
//                    btn_cancel.setEnabled(true);
                    pig_no=qam_pigArrayList.get(vt).getPIG_NO();
                    btn_delete.setEnabled(true);
                    isselected=true;
                }
            }
        });
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = 2;
//                edt_wpig.setEnabled(true);
                edt_pig.setEnabled(true);
                btn_save.setEnabled(true);
                btn_update.setEnabled(false);
                ib_logout.setEnabled(false);
                btn_delete.setEnabled(false);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ib_logout.setEnabled(true);
                btn_cancel.setEnabled(false);
//                edt_wpig.getText().clear();
                edt_pig.getText().clear();
//                edt_wpig.setEnabled(false);
                edt_pig.setEnabled(false);
                btn_update.setEnabled(false);
                btn_save.setEnabled(false);
                btn_insert.setEnabled(true);
                if (isselected){
                    btn_delete.setEnabled(true);
                }
                flag = 0;
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_dialog();
            }
        });
    }

    public void delete_dialog(){
        AlertDialog.Builder b = new AlertDialog.Builder(PigmentActivity.this);
        b.setTitle("Xác nhận");
        b.setMessage("Bạn có muốn xóa liệu màu này?");
        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                checkPig();
            }
        });
        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog al = b.create();
        al.show();
    }

    public void checkPig(){
        try {
            CallApi.checkPigment(factory, edt_pig.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    PArm pArm = gson.fromJson(response.toString(), PArm.class);
                    ArrayList<String> lst = pArm.getItems() == null ? new ArrayList<String>() : pArm.getItems();
                    if (lst.size() <= 0){
                        try {
                            CallApi.deletePigment(factory, pig_no, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                                    getpiglist();
                                    btn_delete.setEnabled(false);
                                    btn_cancel.setEnabled(false);
                                    btn_update.setEnabled(false);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                        } catch (JSONException e) {
                            ToastCustom.message(getApplicationContext(), "Không thể xóa!", Color.RED);
                            e.printStackTrace();
                        }
                    }else {
                        AlertDialog.Builder b = new AlertDialog.Builder(PigmentActivity.this);
                        b.setTitle("Thông báo!");
                        b.setMessage("Liệu màu này đã được dùng. Không thể xóa!");
                        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getpiglist() {
        try {
            CallApi.getpig(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    PigArm pigArm = gson.fromJson(response.toString(), PigArm.class);
                    qam_pigs = pigArm.getItems() == null ? new ArrayList<QAM_PIG>() : pigArm.getItems();
                    qam_pigArrayList = new ArrayList<>();
                    for (QAM_PIG sd : qam_pigs) {
                        qam_pigArrayList.add(sd);
                    }
                    listPigAdapter = new ListPigAdapter(getApplicationContext(), R.layout.list_pig_adapter, qam_pigArrayList);
                    lv_pigment.setAdapter(listPigAdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public class PigArm extends ApiReturnModel<ArrayList<QAM_PIG>> {
    }

    public class PArm extends ApiReturnModel<ArrayList<String>> {
    }
}
