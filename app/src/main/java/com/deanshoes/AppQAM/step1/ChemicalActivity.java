package com.deanshoes.AppQAM.step1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.rpadapter.ListChemAdapter;
import com.deanshoes.AppQAM.adapter.rpadapter.ListMTRAdapter;
import com.deanshoes.AppQAM.adapter.rpadapter.ListPigAdapter;
import com.deanshoes.AppQAM.model.QAM.QAM_CHEM;

import com.deanshoes.AppQAM.model.QAM.QAM_MTR;
import com.deanshoes.AppQAM.model.QAM.QAM_PIG;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;

import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class    ChemicalActivity extends AppCompatActivity {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    Button btn_cr, btn_mtr, btn_pig, btn_chem, btn_model, btn_mcs, btn_next, btn_save, btn_cancel,
            btn_insert_mtr,btn_update_mtr,btn_delete_mtr,btn_insert_chem,btn_update_chem,btn_delete_chem,
            btn_insert_pig,btn_update_pig,btn_delete_pig;
    ImageButton ib_logout;
    EditText edt_chem,edt_raw,edt_pig;
    ListView lv_chemical,lv_raw,lv_pigment;
    String user = "",chem_no="",pig_no="",mtr_no="";
    String factory = "";
    private List<QAM_CHEM> qam_chemList;
    ArrayList<QAM_CHEM> qam_chemArrayList;
    private List<QAM_MTR> qam_mtrList;
    ArrayList<QAM_MTR> qam_mtrArrayList;
    private List<QAM_PIG> qam_pigs;
    ArrayList<QAM_PIG> qam_pigArrayList;
    ListMTRAdapter listMTRAdapter;
    ListChemAdapter listChemAdapter;
    ListPigAdapter listPigAdapter;
    String flag = "";
    int posMTR = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chemical);
        Intent intent = getIntent();
        final Bundle bundle = intent.getExtras();
        if (bundle != null) {
            user = bundle.getString("user", "ADMIN");
            factory=bundle.getString("fac");
        }
        edt_chem = (EditText) findViewById(R.id.edt_chem);
        lv_chemical = (ListView) findViewById(R.id.lv_chemical);
        btn_mtr = (Button) findViewById(R.id.btn_mtr);
        btn_pig = (Button) findViewById(R.id.btn_pig);
        btn_chem = (Button) findViewById(R.id.btn_chem);
        btn_model = (Button) findViewById(R.id.btn_model);
        btn_mcs = (Button) findViewById(R.id.btn_mcs);
        btn_next = (Button) findViewById(R.id.btn_next);
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cr = (Button) findViewById(R.id.btn_cr) ;
        ib_logout=findViewById(R.id.ib_logout) ;
        
        btn_insert_mtr = findViewById(R.id.btn_insert_mtr);
        btn_update_mtr = findViewById(R.id.btn_update_mtr);
        btn_delete_mtr = findViewById(R.id.btn_delete_mtr);

        btn_insert_chem = findViewById(R.id.btn_insert_chem);
        btn_update_chem = findViewById(R.id.btn_update_chem);
        btn_delete_chem = findViewById(R.id.btn_delete_chem);

        btn_insert_pig = findViewById(R.id.btn_insert_pig);
        btn_update_pig = findViewById(R.id.btn_update_pig);
        btn_delete_pig = findViewById(R.id.btn_delete_pig);

        edt_raw = findViewById(R.id.edt_raw);
        edt_pig = findViewById(R.id.edt_pig);

        lv_raw = findViewById(R.id.lv_raw);
        lv_pigment = findViewById(R.id.lv_pigment);
        getmtrlist();
        getchemlist();
        getpiglist();

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ChemicalActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btn_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChemicalActivity.this, CrossActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btn_chem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChemicalActivity.this, ChemicalActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (factory.equals("FVI")){
                    Intent intent = new Intent(ChemicalActivity.this, ModelActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                if (factory.equals("FVI_II_305")){
                    Intent intent = new Intent(ChemicalActivity.this, ModelActivity_FVI2.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

            }
        });
        btn_mcs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChemicalActivity.this, McsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChemicalActivity.this, Mac1Activity_FVI.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btn_insert_mtr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_update_mtr.setEnabled(false);
                setEnable(1);
                flag="1.0";
                disableListView(false);
            }
        });

        btn_update_mtr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_insert_mtr.setEnabled(false);
                setEnable(1);
                flag="1.1";
                disableListView(false);
            }
        });

        btn_delete_mtr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_dialog(1);
            }
        });

        btn_insert_chem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_update_chem.setEnabled(false);
                setEnable(2);
                flag="2.0";
                disableListView(false);
            }
        });

        btn_update_chem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_insert_mtr.setEnabled(false);
                setEnable(2);
                flag="2.1";
                disableListView(false);
            }
        });

        btn_delete_chem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_dialog(2);
            }
        });

        btn_insert_pig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_update_pig.setEnabled(false);
                setEnable(3);
                flag="3.0";
                disableListView(false);
            }
        });

        btn_update_pig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_insert_pig.setEnabled(false);
                btn_update_pig.setEnabled(false);
                setEnable(3);
                flag="3.1";
                disableListView(false);
            }
        });

        btn_delete_pig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_dialog(3);
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag.equals("1.0") || flag.equals("1.1")){
                    if (edt_raw.getText().toString().isEmpty()){
                        ToastCustom.message(getApplicationContext(), "Liệu chủ không thể để trống!", Color.RED);
                    }else if(checkDuplicate(qam_mtrArrayList,qam_chemArrayList,qam_pigArrayList,
                            edt_raw.getText().toString(),1)==false){
                        ToastCustom.message(getApplicationContext(), "Liệu màu không thể tạo trùng lặp!", Color.RED);
                    }else{
                        btn_insert_pig.setEnabled(true);
                        btn_insert_chem.setEnabled(true);
                        btn_insert_mtr.setEnabled(true);
                        if (flag.equals("1.0")) {
                            final QAM_MTR mtr = new QAM_MTR();
                            mtr.setMTR(edt_raw.getText().toString());
                            mtr.setMTR_WEIGHT(null);
                            mtr.setUP_USER(user);
                            try {
                                CallApi.insertQAM_MTR(factory, mtr, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                                        getmtrlist();
                                        edt_raw.getText().clear();
                                        flag = "";
                                        edt_raw.setEnabled(false);
                                        btn_save.setEnabled(false);
                                        btn_cancel.setEnabled(false);
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (flag.equals("1.1")) {

                            final QAM_MTR mtr = new QAM_MTR();
                            mtr.setMTR_NO(qam_mtrArrayList.get(posMTR).getMTR_NO());
                            mtr.setMTR(edt_raw.getText().toString());
                            mtr.setMTR_WEIGHT(null);
                            mtr.setUP_USER(user);
                            try {
                                CallApi.updatemtr(factory, mtr, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                                        flag = "";
//                                edt_wmtr.setEnabled(false);
                                        edt_raw.setEnabled(false);
                                        qam_mtrArrayList.set(posMTR, mtr);
                                        listMTRAdapter.notifyDataSetChanged();
                                        btn_save.setEnabled(false);
                                        btn_cancel.setEnabled(false);
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        ib_logout.setEnabled(true);
                        disableListView(true);
                    }
                }
                if (flag.equals("2.0") || flag.equals("2.1")){
                    if (edt_chem.getText().toString().isEmpty()){
                        ToastCustom.message(getApplicationContext(), "Hóa chất không thể để trống!", Color.RED);
                    }else if(checkDuplicate(qam_mtrArrayList,qam_chemArrayList,qam_pigArrayList,
                            edt_chem.getText().toString(),2)==false){
                        ToastCustom.message(getApplicationContext(), "Liệu màu không thể tạo trùng lặp!", Color.RED);
                    }else{
                        btn_insert_pig.setEnabled(true);
                        btn_insert_chem.setEnabled(true);
                        btn_insert_mtr.setEnabled(true);
                        disableListView(true);
                        if (flag.equals("2.0")) {
                            final QAM_CHEM pig = new QAM_CHEM();
                            pig.setCHEM(edt_chem.getText().toString());
                            pig.setCHEM_WEIGHT(null);
                            pig.setUP_USER(user);
                            try {
                                CallApi.insertQAM_CHEM(factory, pig, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                                        getchemlist();
                                        //       edt_weight.getText().clear();
                                        edt_chem.getText().clear();
                                        flag = "";
                                        //      edt_weight.setEnabled(false);
                                        edt_chem.setEnabled(false);
                                        btn_save.setEnabled(false);
                                        btn_cancel.setEnabled(false);
                                        ib_logout.setEnabled(true);
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (flag.equals("2.1")) {

                            final QAM_CHEM pig = new QAM_CHEM();
                            pig.setCHEM_NO(qam_chemArrayList.get(posMTR).getCHEM_NO());
                            pig.setCHEM(edt_chem.getText().toString());
                            pig.setCHEM_WEIGHT(null);
                            pig.setUP_USER(user);
                            try {
                                CallApi.updateQAM_CHEM(factory, pig, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                                        flag = "";
                                        //       edt_weight.setEnabled(false);
                                        edt_chem.setEnabled(false);
                                        qam_chemArrayList.set(posMTR, pig);
                                        listChemAdapter.notifyDataSetChanged();
                                        btn_save.setEnabled(false);
                                        btn_cancel.setEnabled(false);
                                        ib_logout.setEnabled(true);

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                if (flag.equals("3.0") || flag.equals("3.1")){
                    if (edt_pig.getText().toString().isEmpty()){
                        ToastCustom.message(getApplicationContext(), "Liệu màu không thể để trống!", Color.RED);
                    }else if(checkDuplicate(qam_mtrArrayList,qam_chemArrayList,qam_pigArrayList,
                            edt_pig.getText().toString(),3)==false){
                        ToastCustom.message(getApplicationContext(), "Liệu màu không thể tạo trùng lặp!", Color.RED);
                    }else{
                        btn_insert_pig.setEnabled(true);
                        btn_insert_chem.setEnabled(true);
                        btn_insert_mtr.setEnabled(true);
                        disableListView(true);
                        ib_logout.setEnabled(true);
                        if (flag.equals("3.0")) {
                            final QAM_PIG pig = new QAM_PIG();
                            pig.setPIG(edt_pig.getText().toString());
                            pig.setPIG_WEIGHT(null);
                            pig.setUP_USER(user);
                            try {
                                CallApi.insertQAM_PIG(factory, pig, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                                        getpiglist();
                                        edt_pig.getText().clear();
                                        flag = "";
                                        edt_pig.setEnabled(false);
                                        btn_save.setEnabled(false);
                                        btn_cancel.setEnabled(false);

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (flag.equals("3.1")) {

                            final QAM_PIG pig = new QAM_PIG();
                            pig.setPIG_NO(qam_pigArrayList.get(posMTR).getPIG_NO());
                            pig.setPIG(edt_pig.getText().toString());
                            pig.setPIG_WEIGHT(null);
                            pig.setUP_USER(user);
                            try {
                                CallApi.updateQAM_PIG(factory, pig, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                                        flag = "";
                                        edt_pig.setEnabled(false);
                                        qam_pigArrayList.set(posMTR, pig);
                                        listPigAdapter.notifyDataSetChanged();
                                        btn_save.setEnabled(false);
                                        btn_cancel.setEnabled(false);
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_cancel.setEnabled(false);
                edt_chem.getText().clear();
                edt_chem.setEnabled(false);
                btn_save.setEnabled(false);
                edt_raw.getText().clear();
                edt_raw.setEnabled(false);
                edt_pig.getText().clear();
                edt_pig.setEnabled(false);
                ib_logout.setEnabled(true);

                btn_insert_mtr.setEnabled(true);
                btn_insert_chem.setEnabled(true);
                btn_insert_pig.setEnabled(true);
                disableListView(true);

                flag = "";
//                getchemlist();

            }
        });

        lv_raw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                posMTR = position;
                if (viewId == R.id.rdo_checkmtr) {
                    int vt = position;
                    edt_raw.setText(qam_mtrArrayList.get(vt).getMTR());
                    btn_update_mtr.setEnabled(true);
                    btn_delete_mtr.setEnabled(true);
                    mtr_no=qam_mtrArrayList.get(vt).getMTR_NO();
                }
            }
        });


        lv_chemical.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                posMTR = position;
                if (viewId == R.id.rdo_checkchem) {
                    int vt = position;
                    edt_chem.setText(qam_chemArrayList.get(vt).getCHEM());
                    chem_no= qam_chemArrayList.get(vt).getCHEM_NO();
                    btn_update_chem.setEnabled(true);
                    btn_delete_chem.setEnabled(true);
                }
            }
        });

        lv_pigment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                posMTR = position;
                if (viewId == R.id.rdo_checkpig) {
                    int vt = position;
                    edt_pig.setText(qam_pigArrayList.get(vt).getPIG());
                    pig_no=qam_pigArrayList.get(vt).getPIG_NO();
                    btn_update_pig.setEnabled(true);
                    btn_delete_pig.setEnabled(true);
                }
            }
        });

    }

    private boolean checkDuplicate(ArrayList<QAM_MTR> arrmtr,ArrayList<QAM_CHEM> arrchem,
                                   ArrayList<QAM_PIG> arrpig,String str, int type){
        if (type==1){
            if (arrmtr.size()>0){
                for (int i=0;i<arrmtr.size();i++){
                    if (str.equals(arrmtr.get(i).getMTR())){
                        return false;
                    }
                }
            }
        }else if (type==2){
            if (arrchem.size()>0){
                for (int i=0;i<arrchem.size();i++){
                    if (str.equals(arrchem.get(i).getCHEM())){
                        return false;
                    }
                }
            }
        } else{
            if (arrpig.size()>0){
                for (int i=0;i<arrpig.size();i++){
                    if (str.equals(arrpig.get(i).getPIG())){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void disableListView(boolean bl){
        listMTRAdapter.disableitem(bl);
        listChemAdapter.disableitem(bl);
        listPigAdapter.disableitem(bl);
    }

    private void setEnable(int po){
        btn_save.setEnabled(true);
        btn_cancel.setEnabled(true);
        if (po==1){
            edt_raw.setEnabled(true);
            btn_insert_mtr.setEnabled(false);
            btn_update_mtr.setEnabled(false);
            btn_delete_mtr.setEnabled(false);

            btn_insert_chem.setEnabled(false);
            btn_update_chem.setEnabled(false);
            btn_delete_chem.setEnabled(false);

            btn_insert_pig.setEnabled(false);
            btn_update_pig.setEnabled(false);
            btn_delete_pig.setEnabled(false);
        }
        if (po==2){
            edt_chem.setEnabled(true);
            btn_insert_chem.setEnabled(false);
            btn_update_chem.setEnabled(false);
            btn_delete_chem.setEnabled(false);

            btn_insert_mtr.setEnabled(false);
            btn_update_mtr.setEnabled(false);
            btn_delete_mtr.setEnabled(false);

            btn_insert_pig.setEnabled(false);
            btn_update_pig.setEnabled(false);
            btn_delete_pig.setEnabled(false);
        }
        if (po==3){
            edt_pig.setEnabled(true);
            btn_insert_pig.setEnabled(false);
            btn_update_pig.setEnabled(false);
            btn_delete_pig.setEnabled(false);

            btn_insert_chem.setEnabled(false);
            btn_update_chem.setEnabled(false);
            btn_delete_chem.setEnabled(false);

            btn_insert_mtr.setEnabled(false);
            btn_update_mtr.setEnabled(false);
            btn_delete_mtr.setEnabled(false);
        }
    }

    public void delete_dialog(final int po){
        AlertDialog.Builder b = new AlertDialog.Builder(ChemicalActivity.this);
        b.setTitle("Xác nhận");
        if (po==1){
            b.setMessage("Bạn có muốn xóa liệu chủ này?");
        }
        if (po==2){
            b.setMessage("Bạn có muốn xóa hóa chất này?");
        }
        if (po==3){
            b.setMessage("Bạn có muốn xóa liệu màu này?");
        }

        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (po==1){
                    checkMTR();
                }
                if (po==2){
                    checkChem();
                }
                if (po==3){
                    checkPig();
                }

            }
        });
        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog al = b.create();
        al.show();
    }

    public void checkMTR(){
        try {
            CallApi.checkMTR(factory, edt_raw.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    MatArm matArm = gson.fromJson(response.toString(), MatArm.class);
                    ArrayList<String> lst = matArm.getItems() == null ? new ArrayList<String>() : matArm.getItems();
                    if (lst.size()<=0){
                        try {
                            CallApi.deleteMTR(factory, mtr_no, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                                    getmtrlist();
                                    edt_raw.setText("");
                                    btn_delete_mtr.setEnabled(false);
                                    btn_update_mtr.setEnabled(false);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                        } catch (JSONException e) {
                            ToastCustom.message(getApplicationContext(), "Không thể xóa!", Color.RED);
                            e.printStackTrace();
                        }
                    }else {
                        AlertDialog.Builder b = new AlertDialog.Builder(ChemicalActivity.this);
                        b.setTitle("Thông báo!");
                        b.setMessage("Liệu chủ này đã được dùng. Không thể xóa!");
                        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void checkChem(){
        try {
            CallApi.checkChemical(factory, edt_chem.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    ChArm chArm = gson.fromJson(response.toString(), ChArm.class);
                    ArrayList<String> lst = chArm.getItems() == null ? new ArrayList<String>() : chArm.getItems();
                    if (lst.size() <= 0){
                        try {
                            CallApi.deleteChemical(factory, chem_no, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                                    getchemlist();
                                    edt_chem.setText("");
                                    btn_delete_chem.setEnabled(false);
                                    btn_update_chem.setEnabled(false);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                        } catch (JSONException e) {
                            ToastCustom.message(getApplicationContext(), "Không thể xóa!", Color.RED);
                            e.printStackTrace();
                        }
                    }else {
                        AlertDialog.Builder b = new AlertDialog.Builder(ChemicalActivity.this);
                        b.setTitle("Thông báo!");
                        b.setMessage("Hóa chất này đã được dùng. Không thể xóa!");
                        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void checkPig(){
        try {
            CallApi.checkPigment(factory, edt_pig.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    PArm pArm = gson.fromJson(response.toString(), PArm.class);
                    ArrayList<String> lst = pArm.getItems() == null ? new ArrayList<String>() : pArm.getItems();
                    if (lst.size() <= 0){
                        try {
                            CallApi.deletePigment(factory, pig_no, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                                    getpiglist();
                                    edt_pig.setText("");
                                    btn_delete_pig.setEnabled(false);
                                    btn_update_pig.setEnabled(false);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                        } catch (JSONException e) {
                            ToastCustom.message(getApplicationContext(), "Không thể xóa!", Color.RED);
                            e.printStackTrace();
                        }
                    }else {
                        AlertDialog.Builder b = new AlertDialog.Builder(ChemicalActivity.this);
                        b.setTitle("Thông báo!");
                        b.setMessage("Liệu màu này đã được dùng. Không thể xóa!");
                        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getmtrlist() {
        try {
            CallApi.getmtr(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    MtrArm mtrArm = gson.fromJson(response.toString(), MtrArm.class);
                    qam_mtrList = mtrArm.getItems() == null ? new ArrayList<QAM_MTR>() : mtrArm.getItems();
                    qam_mtrArrayList = new ArrayList<>();
                    for (QAM_MTR sd : qam_mtrList) {
                        qam_mtrArrayList.add(sd);
                    }
                    listMTRAdapter = new ListMTRAdapter(getApplicationContext(), R.layout.list_mtr_adapter, qam_mtrArrayList);
                    lv_raw.setAdapter(listMTRAdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getchemlist() {
        try {
            CallApi.getchemical(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    ChemArm chemArm = gson.fromJson(response.toString(), ChemArm.class);
                    qam_chemList = chemArm.getItems() == null ? new ArrayList<QAM_CHEM>() : chemArm.getItems();
                    qam_chemArrayList = new ArrayList<>();
                    for (QAM_CHEM sd : qam_chemList) {
                        qam_chemArrayList.add(sd);
                    }
                    listChemAdapter = new ListChemAdapter(getApplicationContext(), R.layout.list_chem_adapter, qam_chemArrayList);
                    lv_chemical.setAdapter(listChemAdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getpiglist() {
        try {
            CallApi.getpig(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    PigArm pigArm = gson.fromJson(response.toString(), PigArm.class);
                    qam_pigs = pigArm.getItems() == null ? new ArrayList<QAM_PIG>() : pigArm.getItems();
                    qam_pigArrayList = new ArrayList<>();
                    for (QAM_PIG sd : qam_pigs) {
                        qam_pigArrayList.add(sd);
                    }
                    listPigAdapter = new ListPigAdapter(getApplicationContext(), R.layout.list_pig_adapter, qam_pigArrayList);
                    lv_pigment.setAdapter(listPigAdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class PigArm extends ApiReturnModel<ArrayList<QAM_PIG>> {
    }

    public class PArm extends ApiReturnModel<ArrayList<String>> {
    }

    public class MtrArm extends ApiReturnModel<ArrayList<QAM_MTR>> {
    }

    public class MatArm extends ApiReturnModel<ArrayList<String>> {
    }

    public class ChemArm extends ApiReturnModel<ArrayList<QAM_CHEM>> {
    }

    public class ChArm extends ApiReturnModel<ArrayList<String>> {
    }
}
