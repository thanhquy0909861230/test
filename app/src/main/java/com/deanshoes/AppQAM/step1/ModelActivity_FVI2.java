package com.deanshoes.AppQAM.step1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.rpadapter.ListColor;
import com.deanshoes.AppQAM.adapter.rpadapter.ListModel;
import com.deanshoes.AppQAM.model.QAM.QAM_MODEL;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ModelActivity_FVI2 extends AppCompatActivity {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    Button btn_cr, btn_chem, btn_model, btn_mcs, btn_next, btn_save, btn_cancel;
    Button btn_insert_style,btn_update_style,btn_delete_style,btn_insert_color,btn_update_color,btn_delete_color;
    ImageButton ib_logout;
    EditText edt_style, edt_color;
    String user = "";
    String factory = "";
    ArrayList<QAM_MODEL> styleArr,colorArr;
    private List<QAM_MODEL> style_list, color_list;
    ListModel listStylelAdapter;
    ListColor listColorlAdapter;
    ListView lv_style,lv_color;
    String flag = "";
    int posMTR = -1;
    String stl_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_fvi2);
        Intent intent=getIntent();
        final Bundle bundle=intent.getExtras();
        if (bundle!=null){
            user=bundle.getString("user","ADMIN");
            factory=bundle.getString("fac");
        }
        edt_style = (EditText) findViewById(R.id.edt_style);
        edt_color = (EditText) findViewById(R.id.edt_color);
        lv_color = (ListView) findViewById(R.id.lv_color) ;
        lv_style = (ListView) findViewById(R.id.lv_style) ;
        btn_chem = (Button) findViewById(R.id.btn_chem);
        btn_model = (Button) findViewById(R.id.btn_model);
        btn_mcs = (Button) findViewById(R.id.btn_mcs);
        btn_next = (Button) findViewById(R.id.btn_next);
        btn_insert_style = (Button) findViewById(R.id.btn_insert_style);
        btn_update_style = (Button) findViewById(R.id.btn_update_style);
        btn_delete_style = findViewById(R.id.btn_delete_style);
        btn_insert_color = (Button) findViewById(R.id.btn_insert_color);
        btn_update_color = (Button) findViewById(R.id.btn_update_color);
        btn_delete_color = findViewById(R.id.btn_delete_color);
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cr = (Button) findViewById(R.id.btn_cr);
        

        ib_logout=findViewById(R.id.ib_logout) ;

        getModellist();

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ModelActivity_FVI2.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btn_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ModelActivity_FVI2.this, CrossActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btn_chem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(ModelActivity_FVI2.this, ChemicalActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btn_mcs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent =new Intent(ModelActivity_FVI2.this, McsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(ModelActivity_FVI2.this, Mac1Activity_FVI.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btn_insert_style.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_cancel.setEnabled(true);
                btn_insert_style.setEnabled(false);
                btn_save.setEnabled(true);
                edt_style.setEnabled(true);
                btn_update_style.setEnabled(false);
                edt_style.getText().clear();
                ib_logout.setEnabled(false);
                btn_delete_style.setEnabled(false);
                flag = "1";
                listStylelAdapter.disableitem(false);
                listColorlAdapter.disableitem(false);
            }
        });

        btn_insert_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_cancel.setEnabled(true);
                btn_insert_color.setEnabled(false);
                btn_save.setEnabled(true);
                edt_color.setEnabled(true);
                btn_update_color.setEnabled(false);
                edt_color.getText().clear();
                ib_logout.setEnabled(false);
                btn_delete_color.setEnabled(false);
                flag = "2";
                listColorlAdapter.disableitem(false);
                listStylelAdapter.disableitem(false);
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_insert_style.setEnabled(true);
                btn_insert_color.setEnabled(true);
                if (flag.equals("1") || flag.equals("1.1")){
                    if (edt_style.getText().toString().isEmpty()){
                        ToastCustom.message(getApplicationContext(), "Hình thể không thể để trống!", Color.RED);
                    }else if (checkDuplicate(styleArr, colorArr, edt_style.getText().toString(),1)==false){
                        ToastCustom.message(getApplicationContext(), "Hình thể không thể tạo trùng lặp!", Color.RED);
                    }else {
                        if (flag.equals("1")) {
                            final QAM_MODEL model = new QAM_MODEL();
                            model.setSTYLE(edt_style.getText().toString());
                            model.setUP_USER(user);
                            try {
                                CallApi.insertQAM_MODEL_STYLE(factory, model, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        btn_cancel.performClick();
                                        ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                                        getModellist();
                                        edt_style.getText().clear();
                                        flag = "0";
                                        edt_style.setEnabled(false);

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (flag.equals("1.1")) {
                            final QAM_MODEL model = new QAM_MODEL();
                            model.setSTYLE_NO(styleArr.get(posMTR).getSTYLE_NO());
                            model.setSTYLE(edt_style.getText().toString());
                            model.setUP_USER(user);
                            try {
                                CallApi.updateQAM_MODEL_STYLE(factory, model, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                                        flag = "0";
                                        edt_style.setEnabled(false);
                                        styleArr.set(posMTR, model);
                                        listStylelAdapter.notifyDataSetChanged();
                                        btn_save.setEnabled(false);
                                        btn_cancel.setEnabled(false);

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        ib_logout.setEnabled(true);

                    }
                }
                if (flag.equals("2") || flag.equals("2.1")){
                    if (edt_color.getText().toString().isEmpty()){
                        ToastCustom.message(getApplicationContext(), "Màu sắc không thể để trống!", Color.RED);
                    }else if(checkDuplicate(styleArr, colorArr, edt_style.getText().toString(),2)==false){
                        ToastCustom.message(getApplicationContext(), "Màu sắc không thể tạo trùng lặp!", Color.RED);
                    }else {
                        if (flag.equals("2")) {
                            final QAM_MODEL model = new QAM_MODEL();
                            model.setCOLOR(edt_color.getText().toString());
                            model.setUP_USER(user);
                            try {
                                CallApi.insertQAM_MODEL_COLOR(factory, model, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        btn_cancel.performClick();
                                        ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                                        getModellist();
                                        edt_color.getText().clear();
                                        flag = "0";
                                        edt_color.setEnabled(false);

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (flag.equals("2.1")) {
                            final QAM_MODEL model = new QAM_MODEL();
                            model.setSTYLE_NO(colorArr.get(posMTR).getSTYLE_NO());
                            model.setCOLOR(edt_color.getText().toString());
                            model.setUP_USER(user);
                            try {
                                CallApi.updateQAM_MODEL_COLOR(factory, model, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                                        flag = "0";
                                        edt_color.setEnabled(false);
                                        colorArr.set(posMTR, model);
                                        listColorlAdapter.notifyDataSetChanged();
                                        btn_save.setEnabled(false);
                                        btn_cancel.setEnabled(false);

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        ib_logout.setEnabled(true);

                    }
                }
                listStylelAdapter.disableitem(true);
                listColorlAdapter.disableitem(true);

            }
        });
        lv_style.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                posMTR = position;
                if (viewId == R.id.rdo_checkmodel) {
                    btn_update_style.setEnabled(true);
                    edt_style.setText(styleArr.get(posMTR).getSTYLE());
                    stl_no =styleArr.get(posMTR).getSTYLE_NO();
                    btn_delete_style.setEnabled(true);
                }
            }
        });

        lv_color.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                posMTR = position;
                if (viewId == R.id.rdo_checkmodel) {
                    int vt = position;
                    btn_update_color.setEnabled(true);
                    edt_color.setText(colorArr.get(vt).getCOLOR());
                    stl_no =colorArr.get(vt).getSTYLE_NO();
                    btn_delete_color.setEnabled(true);
                }
            }
        });

        btn_update_style.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = "1.1";
                edt_style.setEnabled(true);
                btn_save.setEnabled(true);
                btn_cancel.setEnabled(true);
                btn_insert_style.setEnabled(false);
                btn_update_style.setEnabled(false);
                ib_logout.setEnabled(false);
                btn_delete_style.setEnabled(false);
                listStylelAdapter.disableitem(false);
                listColorlAdapter.disableitem(false);
            }
        });

        btn_update_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = "2.1";
                edt_color.setEnabled(true);
                btn_save.setEnabled(true);
                btn_cancel.setEnabled(true);
                btn_insert_color.setEnabled(false);
                btn_update_color.setEnabled(false);
                ib_logout.setEnabled(false);
                btn_delete_color.setEnabled(false);
                listColorlAdapter.disableitem(false);
                listStylelAdapter.disableitem(false);
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_color.getText().clear();
                edt_style.getText().clear();
                edt_color.setEnabled(false);
                edt_style.setEnabled(false);
                btn_save.setEnabled(false);
                btn_cancel.setEnabled(false);
                ib_logout.setEnabled(true);
                btn_insert_style.setEnabled(true);
                btn_insert_color.setEnabled(true);
                listStylelAdapter.disableitem(true);
                listColorlAdapter.disableitem(true);
                flag = "0";
            }
        });

        btn_delete_style.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_dialog(1);
            }
        });

        btn_delete_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_dialog(2);
            }
        });
    }

    public void delete_dialog(final int po){
        AlertDialog.Builder b = new AlertDialog.Builder(ModelActivity_FVI2.this);
        b.setTitle("Xác nhận");
        if (po==1){
            b.setMessage("Bạn có muốn xóa hình thể này?");
        }
        if (po==2){
            b.setMessage("Bạn có muốn xóa màu sắc này?");
        }

        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (po==1){
                    checkStyle();
                }
                if (po==2){
                    checkColor();
                }
            }
        });
        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog al = b.create();
        al.show();
    }

    public void checkStyle(){
        try {
            CallApi.checkStyle_fvi2(factory, edt_style.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckArm checkArm = gson.fromJson(response.toString(), CheckArm.class);
                    ArrayList<String> lst = checkArm.getItems() == null ? new ArrayList<String>() : checkArm.getItems();
                    if (lst.size()<=0){
                        try {
                            CallApi.deleteStyle(factory, stl_no, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                                    getModellist();
                                    btn_delete_style.setEnabled(false);
                                    btn_update_style.setEnabled(false);
                                    edt_style.setText("");
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                        } catch (JSONException e) {
                            ToastCustom.message(getApplicationContext(), "Không thể xóa!", Color.RED);
                            e.printStackTrace();
                        }
                    }else {
                        AlertDialog.Builder b = new AlertDialog.Builder(ModelActivity_FVI2.this);
                        b.setTitle("Thông báo!");
                        b.setMessage("Hình thể này đã được dùng. Không thể xóa!");
                        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void checkColor(){
        try {
            CallApi.checkColor_fvi2(factory, edt_color.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    CheckArm checkArm = gson.fromJson(response.toString(), CheckArm.class);
                    ArrayList<String> lst = checkArm.getItems() == null ? new ArrayList<String>() : checkArm.getItems();
                    if (lst.size()<=0){
                        try {
                            CallApi.deleteStyle(factory, stl_no, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                                    getModellist();
                                    btn_delete_style.setEnabled(false);
                                    btn_update_style.setEnabled(false);
                                    edt_color.setText("");
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                }
                            });
                        } catch (JSONException e) {
                            ToastCustom.message(getApplicationContext(), "Không thể xóa!", Color.RED);
                            e.printStackTrace();
                        }
                    }else {
                        AlertDialog.Builder b = new AlertDialog.Builder(ModelActivity_FVI2.this);
                        b.setTitle("Thông báo!");
                        b.setMessage("Hình thể này đã được dùng. Không thể xóa!");
                        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private boolean checkDuplicate(ArrayList<QAM_MODEL> arrstyle,ArrayList<QAM_MODEL> arrcolor,
                                   String str, int type){
        if (type==1){
            if (arrstyle.size()>0){
                for (int i=0;i<arrstyle.size();i++){
                    if (str.toUpperCase().equals(arrstyle.get(i).getSTYLE().toUpperCase())){
                        return false;
                    }
                }
            }
        }else if (type==2){
            if (arrcolor.size()>0){
                for (int i=0;i<arrcolor.size();i++){
                    if (str.toUpperCase().equals(arrcolor.get(i).getCOLOR().toUpperCase())){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void getModellist() {
        try {
            CallApi.getlistmodel(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    StyleArm styleArm = gson.fromJson(response.toString(), StyleArm.class);
                    style_list = styleArm.getItems() == null ? new ArrayList<QAM_MODEL>() : styleArm.getItems();
                    styleArr = new ArrayList<>();
                    colorArr = new ArrayList<>();
                    for (QAM_MODEL df : style_list) {
                        if (df.getSTYLE()!=null){
                            styleArr.add(df);
                        }
                        if (df.getCOLOR()!=null){
                            colorArr.add(df);
                        }
                    }

                    listStylelAdapter = new ListModel(getApplicationContext(), R.layout.list_model_adapter, styleArr);
                    lv_style.setAdapter(listStylelAdapter);

                    listColorlAdapter = new ListColor(getApplicationContext(), R.layout.list_model_adapter, colorArr);
                    lv_color.setAdapter(listColorlAdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class StyleArm extends ApiReturnModel<ArrayList<QAM_MODEL>> {
    }

    public class CheckArm extends ApiReturnModel<ArrayList<String>> {
    }

}
