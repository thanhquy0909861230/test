package com.deanshoes.AppQAM.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.helper.AppAttr;
import com.deanshoes.dsapptools.tools.DSCommonTools;
import com.deanshoes.dsapptools.tools.DSLogTools;
import com.deanshoes.dsapptools.tools.attr.DSPlatform;
import com.deanshoes.dsapptools.tools.model.AppUseHistoryModel;
import com.deanshoes.dsapptools.tools.model.UserModel;
import com.deanshoes.dsapptools.tools.network.DSCallApi;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Locale;


public class GeneralService extends Service {

    public final static String TAG = GeneralService.class.getSimpleName();


    public final static String SEND_APP_INFO_START = "com.deanshoes.AppQAM.service.SEND_APP_INFO_START";

    public final static String SEND_APP_INFO_FINISH = "com.deanshoes.AppQAM.service.SEND_APP_INFO_FINISH";

    public final static String SEND_APP_INFO_ERROR = "com.deanshoes.AppQAM.service.SEND_APP_INFO_ERROR";

    private Handler mHandler;

    JsonObjectRequest mApiRequest;


    static class MyHandler extends Handler {

        private final WeakReference<GeneralService> mThis;

        public MyHandler(GeneralService service) {
            mThis = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            GeneralService service = mThis.get();
            if (service != null) {

                try {
                    switch (msg.what) {


                        default:
                            break;


                    }
                } catch (Exception e) {

                    DSLogTools.e(TAG, "Exception: " + e.getMessage());

                }

            }
        }
    }


    //== 用來回傳 Service 實例
    public class LocalBinder extends Binder {
        public GeneralService getService() {
            return GeneralService.this;
        }
    }


    private final IBinder mBinder = new LocalBinder();


    @Override
    public void onCreate() {

        super.onCreate();

        mHandler = new MyHandler(this);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        DSLogTools.d("RunStep", "GeneralService - onDestroy");

        stopApiRequest();
    }



    // startService 才會呼叫的方法
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        DSLogTools.d("RunStep", "onStartCommand");

        // 在這裡增加要在背景處理的事情
        mHandler.postDelayed(mSendAppInfoAction, 10000);

        return super.onStartCommand(intent, flags, startId);

    }



    // bindService 才會呼叫的方法
    @Override
    public IBinder onBind(Intent intent) {

        DSLogTools.d("RunStep", "onBind");

        return mBinder;
    }

    // bindService 才會呼叫的方法
    @Override
    public boolean onUnbind(Intent intent) {

        DSLogTools.d("RunStep", "onUnbind");

        return super.onUnbind(intent);
    }

    //== 廣播訊息
    private void doSendBroadcast(String action) {

        Intent intent = new Intent(action);

        sendBroadcast(intent);
    }


    public boolean initialize() {

        return true;
    }


    private Runnable mSendAppInfoAction = new Runnable() {
        @Override
        public void run() {

            sendAppInfo();
        }
    };


    private void sendAppInfo() {

        try {


            doSendBroadcast(SEND_APP_INFO_START);

            UserModel user = DSCommonTools.getAppUser(getApplicationContext());

            AppUseHistoryModel appHistory = new AppUseHistoryModel();
            appHistory.DEVICE_ID = "";
            appHistory.DEVICE_TOKEN = "";
            appHistory.APP_ID = AppAttr.AppID;
            appHistory.FACTORY = user.mFactoryName;
            appHistory.PLATFORM = DSPlatform.ANDROID;
            appHistory.SDK_VER = String.format(Locale.getDefault(),"%d", Build.VERSION.SDK_INT);
            appHistory.APP_VER_CODE = DSCommonTools.getVersionCode(getApplicationContext());

            mApiRequest = DSCallApi.saveAppInfo(getApplicationContext(), user.mFactoryName, AppAttr.AppID, AppAttr.AppKey, user.mUserID, appHistory,

                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject jObj) {

                            doSendBroadcast(SEND_APP_INFO_FINISH);

                            DSLogTools.e("AJAX", "Response: " + jObj.toString());

                            // 把 Service 停掉
                            stopSelf();

                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {


                            doSendBroadcast(SEND_APP_INFO_ERROR);

                            DSLogTools.e("AJAX", "Response Error: " + error.getMessage());

                            if (error.networkResponse != null) {
                                DSLogTools.e("AJAX", "Response statusCode: " + Integer.toString(error.networkResponse.statusCode));
                            }

                        }
                    });


        } catch (Exception ex) {

            DSLogTools.e(getApplicationContext(), "General Service Exception: " + ex.toString());
        }

    }

    public void stopApiRequest() {

        try {

            if (mApiRequest != null) {
                mApiRequest.cancel();
            }

        } catch (Exception e) {
            DSLogTools.d(TAG, e.getMessage());
        }

    }

    private void sendNotification(int notificationID, String title, String message) {


        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(NOTIFICATION_SERVICE);

        int requestID = (int) System.currentTimeMillis();

        Intent StartInten = new Intent(Intent.ACTION_VIEW);
        StartInten.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent contentIntent = PendingIntent.getActivity(this, requestID,
                StartInten, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true);

        mBuilder.setContentIntent(contentIntent);

        mNotificationManager.notify(notificationID, mBuilder.build());

    }

}
