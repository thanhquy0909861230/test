package com.deanshoes.AppQAM.helper;

import com.deanshoes.dsapptools.tools.attr.DSFactoryID;

/**
 * 設定此 App 的廠區，非必要。目前為開發測試時會用到。
 * App正式發佈後，會使用入口App傳送過來的值
 */

public class AppFactory {

    public final static String APP_USE = DSFactoryID.FVI;

//    public final static String APP_USE = DSFactoryID.FVI_II_305;

}
