package com.deanshoes.AppQAM.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.deanshoes.dsapptools.tools.DSLogTools;


public class AppReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if(action.equals(Intent.ACTION_BOOT_COMPLETED)) {

            // 監聽開機結束事件

            DSLogTools.d("RunStep", "BroadcastReceiver : " + Intent.ACTION_BOOT_COMPLETED);

        }


    }
}
