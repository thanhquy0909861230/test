package com.deanshoes.AppQAM.step4;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.fragment.Card2Fragment;
import com.deanshoes.AppQAM.fragment.IP_BatchFVI2Fragment;
import com.deanshoes.AppQAM.fragment.IP_BatchFragment;
import com.deanshoes.AppQAM.fragment.KneaderFragment;
import com.deanshoes.AppQAM.fragment.OpenMillFragment;
import com.deanshoes.AppQAM.fragment.PellFragment;
import com.deanshoes.AppQAM.model.QAM.BTC;
import com.deanshoes.AppQAM.model.QAM.BTC_OP;
import com.deanshoes.AppQAM.model.QAM.BTC_PELL;
import com.deanshoes.AppQAM.model.QAM.RP_LIST_BATCH;
import com.deanshoes.AppQAM.model.QAM.RP_MAC;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.network.CallApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ReportActivity extends AppCompatActivity {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    FragmentPagerAdapter adapterViewPager;
    ViewPager vpPager;
    public static String factory="", date="", shift ="", ins_no, MCS, style, color, hard;

    int i=0, time=4500;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        Intent intent = getIntent();

        ins_no=pdfActivity.ins_no;
        System.out.println("ins no: " + ins_no);

        final Bundle bundle=intent.getExtras();
        if (bundle!=null){
            factory=bundle.getString("fac");
        }
        System.out.println("rp fac: "+factory);

        createfolder();
//        changepage(time);

        vpPager = (ViewPager) findViewById(R.id.viewPager);
        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);

//        vpPager.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return true;
//            }
//        });
    }

    private class MyPagerAdapter extends FragmentPagerAdapter{

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    if (factory.equals("FVI")){
                        return IP_BatchFragment.newInstance("IP_BATCHFragment, Instance 1");
                    }else if (factory.equals("FVI_II_305")){
                        return IP_BatchFragment.newInstance("IP_BATCHFragment, Instance 1");
//                        return IP_BatchFVI2Fragment.newInstance("IP_BATCHFragment, Instance 1");
                    }

                case 1:
                    return KneaderFragment.newInstance("KneaderFragment, Instance 2");
                case 2:
                    return OpenMillFragment.newInstance("OpenMillFragment, Instance 3");
                case 3:
                    return PellFragment.newInstance("PellFragment, Instance 4");
                case 4 :
                    return Card2Fragment.newInstance("Card2Fragment, Instance 5");
                default:
                    return null;

            }
        }



        // chuyển trang report
        @Override
        public int getCount() {
            return 4;
        }
    }

    public void createfolder() {
        File folder = new File(Environment.getExternalStorageDirectory() + "/QAM_EXCEL");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            // Do something on success
        } else {
            // Do something else on failure
        }
    }

    public void changepage(int time) {
        final Handler handler = new Handler();
        TimerTask myTimerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (i == 5) {
                            i = 0;
                        }
                        vpPager.setCurrentItem(i, true);
                        i++;
                    }
                });
            }
        };
        Timer timer = new Timer();
        timer.schedule(myTimerTask, 0, time);
    }
}
