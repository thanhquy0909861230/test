package com.deanshoes.AppQAM.step4;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.rpadapter.BTCAdapter;
import com.deanshoes.AppQAM.adapter.rpadapter.NewBatchNoAdapter;
import com.deanshoes.AppQAM.model.QAM.MODEL_SEND_MAIL;
import com.deanshoes.AppQAM.model.QAM.QAM_INS;
import com.deanshoes.AppQAM.model.report_model.RP_BTC;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.deanshoes.AppQAM.step3.FVI.Kneader_Controller;
import com.deanshoes.AppQAM.step5.mac1fvi2;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.deanshoes.AppQAM.fragment.IP_BatchFragment.createStyleForTitle;

public class pdfActivity extends AppCompatActivity {
    private static final String TAG = pdfActivity.class.getSimpleName();
    private static String FILE_NAME = "";
    private Spinner spn_shiftrp, sp_groupID;
    TextView tv_color, tv_model, tv_mcs, tv_countdown;
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    BTCAdapter btcAdapter;
    ListView lv_btc;
    EditText edt_weight, edt_timestart, edt_timend, edt_er, edt_tytrong, edt_dc, edt_daterp;
    Button btn_print, btn_create, btn_clean, btn_step3, btn_search, btn_report, btn_part5;
    ImageButton ib_logout;
    private DatePickerDialog datePickerDialog;

    public static final String FONTF = "assets/NotoSansCJKsc-Regular.otf";
    String user = "";

    public static String factory = "", date = "", shift = "", ins_no= "", MCS="", style, hard, wight = "",er="", TL="", EXDATE="";
    public static String color = "";
    String mcs_no, mcs_name;
    private List<QAM_INS> insl;
    private List<com.deanshoes.AppQAM.model.QAM.MCS> mcsinfor;
    ArrayList<String> arrGroupID;
    ArrayList<QAM_INS> arrINS;
    ArrayList<RP_BTC> arrBTC;
    ArrayList<String> btc_no1ArrayList;
    boolean creMode = false;

    @SuppressLint("ClickableViewAccessibility")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        final Intent intent = getIntent();
        final Bundle bundle = intent.getExtras();
        if (bundle != null) {
            user = bundle.getString("user", "QAM");
            factory = bundle.getString("fac");
        }
//        if (factory.equals("FVI")) {
//            setContentView(R.layout.activity_pdffvi);
//        } else {
//            setContentView(R.layout.activity_pdf);
//        }
        createfolder();
        DeteleFileBefore7days();
        getshift();



        mcsinfor = new ArrayList<>();
        edt_daterp = findViewById(R.id.edt_daterp);
        spn_shiftrp = findViewById(R.id.spn_shiftrp);
        btn_step3 = findViewById(R.id.btn_step3);
        btn_clean = findViewById(R.id.btn_clean);
        btn_create = findViewById(R.id.btn_create);
        btn_search = findViewById(R.id.btn_search);
        edt_timestart = findViewById(R.id.edt_timestart);
        edt_timend = findViewById(R.id.edt_timend);
        btn_print = findViewById(R.id.btn_print);
        edt_weight = findViewById(R.id.edt_weight);
        lv_btc = findViewById(R.id.lv_btcno);
        sp_groupID = findViewById(R.id.sp_groupID);
        tv_color = findViewById(R.id.tv_color);
        tv_mcs = findViewById(R.id.tv_mcs);
        tv_model = findViewById(R.id.tv_model);
        tv_countdown = findViewById(R.id.tv_cdown);
        ib_logout = findViewById(R.id.ib_logout);
        btn_report = findViewById(R.id.btn_report);
        btn_part5 = findViewById(R.id.btn_part5);

        edt_daterp.setEnabled(false);
        spn_shiftrp.setEnabled(false);
        sp_groupID.setEnabled(false);
        edt_weight.setEnabled(false);
        edt_timestart.setEnabled(false);
        edt_timend.setEnabled(false);
        edt_er = findViewById(R.id.edt_er);
       // edt_tytrong = findViewById(R.id.edt_tytrong);
        edt_er.setEnabled(false);
       //  edt_tytrong.setEnabled(false);
        //  edt_dc.setEnabled(false);
        btn_print.setEnabled(false);
        btn_clean.setEnabled(false);
        btn_report.setEnabled(false);

        spn_shiftrp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!edt_daterp.getText().toString().isEmpty()) {
                    getGROUP_ID(spn_shiftrp.getSelectedItem().toString(), edt_daterp.getText().toString());
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_step3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(pdfActivity.this, Kneader_Controller.class);
                myIntent.putExtras(bundle);
                pdfActivity.this.startActivity(myIntent);
            }
        });
        if (factory.equals("FVI_II_305")) {
            btn_part5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myIntent = new Intent(pdfActivity.this, mac1fvi2.class);
                    myIntent.putExtras(bundle);
                    pdfActivity.this.startActivity(myIntent);
                }
            });
        } else btn_part5.setVisibility(View.INVISIBLE);


        sp_groupID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                String s1= sp_groupID.getSelectedItem().toString();
                String[] words = s1.split("_");
                ins_no = words[0];
                for (int i = 0; i < arrGroupID.size(); i++){
                    if (s1.equals(arrINS.get(i).getINS_NO()) && position == i) {
                        mcs_no = arrINS.get(i).getMCS_NO();
                        getInfor(mcs_no);
                        getData(ins_no, spn_shiftrp.getSelectedItem().toString(), edt_daterp.getText().toString());
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

//        tv_mcs.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//            @Override
//            public void afterTextChanged(Editable s) {
//                if (!tv_mcs.getText().toString().isEmpty()){
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            cancelTimer2();
//                            return;
//                        }
//                    }, 500);
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            startTimer();
//                            return;
//                        }
//                    }, 1000);
//
//                }
//
//            }
//        });

        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edt_daterp.setEnabled(true);
                spn_shiftrp.setEnabled(true);
                sp_groupID.setEnabled(true);
                edt_weight.setEnabled(true);
                edt_timestart.setEnabled(true);
                edt_timend.setEnabled(true);
                ib_logout.setEnabled(false);
                btn_clean.setEnabled(true);
                btn_create.setEnabled(false);
                btn_search.setEnabled(false);
              edt_er.setEnabled(true);
     //         edt_tytrong.setEnabled(true);
//              edt_dc.setEnabled(true);
//              btn_print.setEnabled(true);
                creMode = true;
            }
        });

        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_create.setEnabled(false);
                String d = edt_daterp.getText().toString().substring(0, 2);
                String m = edt_daterp.getText().toString().substring(3, 5);
                String y = edt_daterp.getText().toString().substring(6);
//              String name="IP-BATCH TRACKING CARD.2_"+d+"-"+m+"-"+y+"_"+
//              tv_mcs.getText().toString()+".pdf";
                String name = "IP-BATCH TRACKING CARD.2_" + d + "-" + m + "-" + y + "_" +
                        tv_mcs.getText().toString() + ".xls";
                String DEST = Environment.getExternalStorageDirectory().toString() + "/Qam_rp/" + name;
                File file = new File(DEST);
                file.getParentFile().mkdirs();
                try {
                    //  createPdf(DEST);
                    if(factory.equals("FVI_II_305")){
                        createExcel_FVI_II();
                        sendmail_Card2(DEST);
                    }else{
                        createExcel_FVI();
                        sendmail_Card2(DEST);
                    }
                    btn_create.setEnabled(true);
                    btn_clean.setEnabled(false);
                    btn_search.setEnabled(true);
//                  clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btn_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //chạy 4 màn hình
                //update 1.0, cho phep chu dong gui mail, khong can cho
                er = edt_er.getText().toString();
                TL = edt_weight.getText().toString();
                EXDATE = edt_timend.getText().toString();
                btn_report.setEnabled(false);
                Intent myIntent = new Intent(pdfActivity.this, ReportActivity.class);
                myIntent.putExtras(bundle);
                myIntent.putExtra("MCS", MCS);
                myIntent.putExtra("date", date);
                myIntent.putExtra("shift", shift);
                myIntent.putExtra("er", er);
                myIntent.putExtra("TL", TL);
                myIntent.putExtra("EXDATE", EXDATE);
                pdfActivity.this.startActivity(myIntent);

            }
        });

        btn_clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
                cancelTimer();
                btn_create.setEnabled(true);
                btn_clean.setEnabled(false);
                btn_search.setEnabled(true);
                ib_logout.setEnabled(true);
                creMode = false;
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_search();
            }
        });

        edt_timestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog2(edt_timestart);
            }
        });

        edt_daterp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog1(edt_daterp);

            }
        });
        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(pdfActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });
    }

    private void clear() {
        edt_daterp.getText().clear();
        spn_shiftrp.setSelection(0);
        sp_groupID.setAdapter(null);
        tv_mcs.setText("");
        tv_model.setText("");
        tv_color.setText("");
        edt_daterp.setEnabled(false);
        spn_shiftrp.setEnabled(false);
        sp_groupID.setEnabled(false);
        edt_weight.getText().clear();
        edt_timestart.getText().clear();
        edt_timend.getText().clear();
        btn_print.setEnabled(false);
        btn_report.setEnabled(false);
        edt_weight.setEnabled(false);
        edt_timestart.setEnabled(false);
        edt_timend.setEnabled(false);


        //old
           edt_er.setEnabled(false);
         //  edt_tytrong.setEnabled(false);
        //   edt_dc.setEnabled(false);

        btcAdapter = new BTCAdapter(getApplicationContext(), R.layout.btc_adapter, new ArrayList<RP_BTC>());
        lv_btc.setAdapter(btcAdapter);
    }

    private static final long NUMBER_MILLIS = 10000;
    //private static final String MILLISECONDS_FORMAT = "%02d";
    private int secondsLeft = 0;
    CountDownTimer cTimer = null;
    void startTimer() {
        cTimer =  new CountDownTimer(NUMBER_MILLIS, 1) {
            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            public void onTick(long millisUntilFinished) {

                if (Math.round((float)millisUntilFinished / 1000.0f) != secondsLeft)
                {
                    secondsLeft = Math.round((float)millisUntilFinished / 1000.0f);
                }
                long roundMillis = secondsLeft * 1000;
                if(roundMillis==NUMBER_MILLIS){
                    tv_countdown.setText("Gửi mail sau:" + " " +  secondsLeft);
//                            + "." + String.format(MILLISECONDS_FORMAT, 0));

                }else {
                    tv_countdown.setText("Gửi mail sau:" + " " + secondsLeft);
//                            + "." + String.format(MILLISECONDS_FORMAT, millisUntilFinished % 1000));
                }
            }
            @SuppressLint("SetTextI18n")
            public void onFinish() {
                if(btn_report.isEnabled()){
                    tv_countdown.setText("Đang gửi Mail");
                    btn_report.performClick();
                }
                else {
                    tv_countdown.setText("Không có dữ liệu");
                }
            }
        };
        cTimer.start();
    }

    @SuppressLint("SetTextI18n")
    void cancelTimer() {
        if(cTimer!=null){
            tv_countdown.setText("Đã hủy tự động!");
            cTimer.cancel();
        }
    }
    @SuppressLint("SetTextI18n")
    void cancelTimer2() {
        if(cTimer!=null){
            tv_countdown.setText("Gửi mail sau: --");
            cTimer.cancel();
        }
    }

    String DEST = Environment.getExternalStorageDirectory().toString() + "/Qam_rp/";
    public void datePickerDialog1(final EditText edt) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(pdfActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int monthcurrent = monthOfYear + 1;
                        String month = "";
                        String day = "";
                        if (dayOfMonth > 9) {
                            day = String.valueOf(dayOfMonth);
                        } else {
                            day = "0" + String.valueOf(dayOfMonth);
                        }
                        if (monthcurrent > 9) {
                            month = String.valueOf(monthcurrent);
                        } else {
                            month = "0" + String.valueOf(monthcurrent);
                        }
                        edt.setText(month + "/" + day + "/" + year);
                        getGROUP_ID(spn_shiftrp.getSelectedItem().toString(), edt_daterp.getText().toString());
                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }
    public void datePickerDialog2(final EditText edt) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        final int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(pdfActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int monthcurrent = monthOfYear + 1;
                        String month = "";
                        String day = "";
                        if (dayOfMonth > 9) {
                            day = String.valueOf(dayOfMonth);
                        } else {
                            day = "0" + String.valueOf(dayOfMonth);
                        }
                        if (monthcurrent > 9) {
                            month = String.valueOf(monthcurrent);
                        } else {
                            month = "0" + String.valueOf(monthcurrent);
                        }
                        edt.setText(day + "/" + month + "/" + year);
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        try {

                            newDate.set(year, monthOfYear + 3, dayOfMonth);
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat sd1 = new SimpleDateFormat("dd/MM/yyyy");
                            final Date Endate = newDate.getTime();
                            String Endate1 = sd1.format(Endate);
                            edt_timend.setText(Endate1);
                            ;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @SuppressLint("SetTextI18n")
    private void dialog_search() {
        LayoutInflater inflater = getLayoutInflater();
        final View noteLayout = inflater.inflate(R.layout.dialog_search_pdf, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(pdfActivity.this);
        alert.setView(noteLayout);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        final Button btn_qry = noteLayout.findViewById(R.id.btn_qry);
        final Button btn_close = noteLayout.findViewById(R.id.btn_close);
        final EditText dig_date = noteLayout.findViewById(R.id.dig_date);
        final Spinner dig_shift = noteLayout.findViewById(R.id.dig_shift);
        final EditText edt_count = noteLayout.findViewById(R.id.edt_count);
        final ListView dig_lst_newBatch = noteLayout.findViewById(R.id.dig_lst_newBatch);

        String day = "", month = "";
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        if (mDay > 9) {
            day = String.valueOf(mDay);
        } else {
            day = "0" + String.valueOf(mDay);
        }
        if (mMonth > 9) {
            month = String.valueOf(mMonth);
        } else {
            month = "0" + String.valueOf(mMonth);
        }
        dig_date.setText(month + "/" + day + "/" + mYear);

        ArrayList<String> listShift = new ArrayList<>();
        listShift.add("");
        listShift.add("1");
        listShift.add("2");
        listShift.add("3");
        ArrayAdapter ShiftAdapter = new ArrayAdapter(pdfActivity.this, android.R.layout.simple_dropdown_item_1line, listShift);
        dig_shift.setAdapter(ShiftAdapter);
        dig_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialogSearch(dig_date);
            }
        });

        btn_qry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    CallApi.getIdColor(factory, dig_shift.getSelectedItem().toString(), dig_date.getText().toString(), new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            getIdColor getIdColora = gson.fromJson(response.toString(), getIdColor.class);
                            ArrayList<Temp1> getcolor = getIdColora.getItems() == null ? new ArrayList<Temp1>() : getIdColora.getItems();
                            if (getcolor.size() > 0) {
                                NewBatchNoAdapter newBatchNoAdapter = new NewBatchNoAdapter(pdfActivity.this,
                                        R.layout.list_newbatchno_adapter, getcolor);
                                dig_lst_newBatch.setAdapter(newBatchNoAdapter);
                                edt_count.setText(String.valueOf(getcolor.size()));
                            } else {
                                NewBatchNoAdapter newBatchNoAdapter = new NewBatchNoAdapter(pdfActivity.this,
                                        R.layout.list_newbatchno_adapter, new ArrayList<Temp1>());
                                dig_lst_newBatch.setAdapter(newBatchNoAdapter);
                                edt_count.setText("0");
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(layoutParams);
    }

    public void datePickerDialogSearch(final EditText edt_date) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(pdfActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int monthcurrent = monthOfYear + 1;
                        String month = "";
                        String day = "";
                        if (dayOfMonth > 9) {
                            day = String.valueOf(dayOfMonth);
                        } else {
                            day = "0" + String.valueOf(dayOfMonth);
                        }
                        if (monthcurrent > 9) {
                            month = String.valueOf(monthcurrent);
                        } else {
                            month = "0" + String.valueOf(monthcurrent);
                        }
                        edt_date.setText(month + "/" + day + "/" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    //delete
    public void DeteleFileBefore7days() {
        Calendar time = Calendar.getInstance();
        time.add(Calendar.DAY_OF_YEAR, -8);
        String path = Environment.getExternalStorageDirectory().toString() + "/Qam_rp/";
        File directory = new File(path);
        File[] files = directory.listFiles();
        for (int i = 0; i < files.length; i++) {
            File file = new File(Environment.getExternalStorageDirectory().toString() + "/Qam_rp/", files[i].getName());
            Date lastModified1 = new Date(file.lastModified());
            if (lastModified1.before(time.getTime())) {
                boolean deleted = file.delete();
            }
        }
    }

    public void getGROUP_ID(String mcs_shift, String mcs_date) {
        try {
//            CallApi.getGroupID(factory, mcs_shift, mcs_date, new Response.Listener<JSONObject>()
            CallApi.getGroupIDpdf(factory, mcs_shift, mcs_date, new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response) {
                    InsArm insArm = gson.fromJson(response.toString(), InsArm.class);
                    insl = insArm.getItems() == null ? new ArrayList<QAM_INS>() : insArm.getItems();
                    arrINS = new ArrayList<>();
                    arrGroupID = new ArrayList<>();
                    if (insl.size() > 0) {
                        for (QAM_INS ob : insl) {
                            arrINS.add(ob);
                            arrGroupID.add(ob.getINS_NO());
                        }
                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(getApplicationContext(), R.layout.login_spiner, arrGroupID);
//                        adapter.setDropDownViewResource(R.layout.login_spiner);
                        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                        sp_groupID.setAdapter(adapter);

                    } else {
                        ArrayList listMCS = new ArrayList<>();
                        listMCS.add("");
                        ArrayAdapter mcsAdapter = new ArrayAdapter(getApplicationContext(), R.layout.login_spiner, listMCS);
                        sp_groupID.setAdapter(mcsAdapter);
                        btcAdapter = new BTCAdapter(getApplicationContext(), R.layout.btc_adapter, new ArrayList<RP_BTC>());
                        lv_btc.setAdapter(btcAdapter);
                        tv_mcs.setText("");
                        tv_model.setText("");
                        tv_color.setText("");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getInfor(String mcs_no) {
        try {
            CallApi.getInforMcs(factory, mcs_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Mac1Activity_FVI.McsArm mcsArm = gson.fromJson(response.toString(), Mac1Activity_FVI.McsArm.class);
                    mcsinfor = mcsArm.getItems() == null ? new ArrayList<com.deanshoes.AppQAM.model.QAM.MCS>() : mcsArm.getItems();
                    if (mcsinfor.size() != 0) {
                        tv_mcs.setText(mcsinfor.get(0).getMSC());
                        String str = mcsinfor.get(0).getMSC();
                        mcs_name = str.substring(str.indexOf("|") + 1);
                        tv_model.setText(mcsinfor.get(0).getMCS_STYLE());
                        tv_color.setText(mcsinfor.get(0).getMCS_COLOR());
                        edt_er.setText(mcsinfor.get(0).getER());
                        shift = spn_shiftrp.getSelectedItem().toString();
                        date = edt_daterp.getText().toString();
                        MCS = mcsinfor.get(0).getMSC();
                        style = mcsinfor.get(0).getMCS_STYLE();
                        color = mcsinfor.get(0).getMCS_COLOR();
                        hard = mcsinfor.get(0).getMCS_HARD();
                    } else {
                        tv_mcs.setText("");
                        tv_model.setText("");
                        tv_color.setText("");
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public void getData(String ins_no, String shift, String date) {
        arrBTC = new ArrayList<>();
        try {
            CallApi.getData(factory, ins_no, shift, date, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    BtcArm btcArm = gson.fromJson(response.toString(), BtcArm.class);
                    arrBTC = btcArm.getItems() == null ? new ArrayList<RP_BTC>() : btcArm.getItems();
                    btcAdapter = new BTCAdapter(getApplicationContext(), R.layout.btc_adapter, arrBTC);
                    lv_btc.setAdapter(btcAdapter);
                    btc_no1ArrayList = new ArrayList<>();
                    for (int i = 0; i < arrBTC.size(); i++) {
                        btc_no1ArrayList.add(arrBTC.get(i).getBTC_BATCH_NO());
                    }
                    if (arrBTC.size() > 0 && creMode == true) {
                        btn_print.setEnabled(true);
                        btn_report.setEnabled(true);
                    } else {
                        btn_print.setEnabled(false);
                        btn_report.setEnabled(false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("LOGCAT", "Exception", e);
        }
    }

    MODEL_SEND_MAIL model;
    //    public void createPdf(String dest) throws IOException, DocumentException {
//
//        Document document = new Document();
//        try {
//            model=new MODEL_SEND_MAIL();
//            model.setMSC(tv_mcs.getText().toString());
//            model.setDATE(edt_daterp.getText().toString());
//            model.setSHIFT(spn_shiftrp.getSelectedItem().toString());
//            model.setSTYLE(tv_model.getText().toString());
//            model.setCOLOR(tv_color.getText().toString());
//            Font font = FontFactory.getFont(FONTF, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//            font.setSize(6);
//            Font font2 = FontFactory.getFont(FONTF, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//            font2.setStyle(Font.BOLD);
//            font2.setSize(12);
//            Font font3 = FontFactory.getFont(FONTF, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//            font3.setSize(5);
//            Font font4 = FontFactory.getFont(FONTF, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//            font4.setSize(8);
//            Font fontdynamic = FontFactory.getFont(FONTF, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//            fontdynamic.setSize(11);
//            fontdynamic.setColor(BaseColor.RED);
//
//
//            PdfWriter.getInstance(document, new FileOutputStream(dest));
//            document.open();
//            PdfPTable table = new PdfPTable(2);
//            table.setWidthPercentage(35);
//            //head
//            if(factory.equals("FVI")){
//                PdfPCell cellhead = new PdfPCell(new Paragraph("IP - BATCH TRACKING CARD .2 \n \n THẺ THEO DÕI IP -", font2));
//                cellhead.setColspan(2);
//                cellhead.setHorizontalAlignment(Element.ALIGN_CENTER);
//                cellhead.setFixedHeight(40);
//                table.addCell(cellhead);
//           }
////            else if (factory.equals("FVI_II_305")){
////                PdfPCell cellhead = new PdfPCell(new Paragraph("ID - BATCH TRACKING CARD .2 \n \n THẺ THEO DÕI ID -", font2));
////                cellhead.setColspan(2);
////                cellhead.setHorizontalAlignment(Element.ALIGN_CENTER);
////                cellhead.setFixedHeight(40);
////                table.addCell(cellhead);
////            }
//
//
//            //dong 1, 2
//            final PdfPCell cell1 = new PdfPCell(new Paragraph("MCS # : " + tv_mcs.getText().toString(), font4));
//            cell1.setColspan(1);
//            PdfPCell cell2 = new PdfPCell(new Paragraph("ER :  " + edt_er.getText().toString(), font4));
//            cell2.setColspan(1);
//            table.addCell(cell1);
//            table.addCell(cell2);
//            //dong 3
//            PdfPCell cell3 = new PdfPCell(new Paragraph("型體/Model/Hình thể: " + "  " + tv_model.getText().toString(), font4));
//            cell3.setColspan(2);
//            table.addCell(cell3);
//
//            //dong 4
//            PdfPCell cell4 = new PdfPCell(new Paragraph("顏色/Color/Màu sắc: " + "  " + tv_color.getText().toString(), font4));
//            cell4.setColspan(2);
//            table.addCell(cell4);
//            // //dong 5
//            PdfPCell cell5 = new PdfPCell(new Paragraph("混合手數/# of batch combined/số mẻ kết hợp", font));
//            cell5.setColspan(2);
//            table.addCell(cell5);
//
//            String[] batch = new String[15];
//            for (int i=0;i<15;i++){
//                if (i<btc_no1ArrayList.size()){
//                    batch[i]=btc_no1ArrayList.get(i);
//                }else batch[i]=" ";
//            }
//            int count = Math.round(btc_no1ArrayList.size()/2);
//            PdfPCell cellb1,cellb2;
//            for (int i=0;i<=count;i++){
//                cellb1 = new PdfPCell(new Paragraph(i+1+"-batch #: "+btc_no1ArrayList.get(i), font));
//                cellb1.setColspan(1);
//                table.addCell(cellb1);
//                if (i==count){
//                    cellb2 = new PdfPCell(new Paragraph("", font));
//                    cellb2.setColspan(1);
//                    table.addCell(cellb2);
//                }else{
//                    cellb2 = new PdfPCell(new Paragraph(i+count+2+"-batch #: "+btc_no1ArrayList.get(i+count), font));
//                    cellb2.setColspan(1);
//                    table.addCell(cellb2);
//                }
//
//            }
//
//            // list batch_no
////            PdfPCell cellb1 = new PdfPCell(new Paragraph("1-batch #: "+batch[0], font));
////            cellb1.setColspan(1);
////            table.addCell(cellb1);
////            PdfPCell cellb9 = new PdfPCell(new Paragraph("9-batch #: "+batch[8], font));
////            cellb9.setColspan(1);
////            table.addCell(cellb9);
////
////            PdfPCell cellb2 = new PdfPCell(new Paragraph("2-batch #: "+batch[1], font));
////            cellb2.setColspan(1);
////            table.addCell(cellb2);
////            PdfPCell cellb10 = new PdfPCell(new Paragraph("10-batch #: "+batch[9], font));
////            cellb10.setColspan(1);
////            table.addCell(cellb10);
////
////
////            PdfPCell cellb3 = new PdfPCell(new Paragraph("3-batch #: "+batch[2], font));
////            cellb3.setColspan(1);
////            table.addCell(cellb3);
////            PdfPCell cellb11 = new PdfPCell(new Paragraph("11-batch #: "+batch[10], font));
////            cellb11.setColspan(1);
////            table.addCell(cellb11);
////
////            PdfPCell cellb4 = new PdfPCell(new Paragraph("4-batch #: "+batch[3], font));
////            cellb4.setColspan(1);
////            table.addCell(cellb4);
////            PdfPCell cellb12 = new PdfPCell(new Paragraph("12-batch #: "+batch[11], font));
////            cellb12.setColspan(1);
////            table.addCell(cellb12);
////
////            PdfPCell cellb5 = new PdfPCell(new Paragraph("5-batch #: "+batch[4], font));
////            cellb5.setColspan(1);
////            table.addCell(cellb5);
////            PdfPCell cellb13 = new PdfPCell(new Paragraph("13-batch #: "+batch[12], font));
////            cellb13.setColspan(1);
////            table.addCell(cellb13);
////
////            PdfPCell cellb6 = new PdfPCell(new Paragraph("6-batch #: "+batch[5], font));
////            cellb6.setColspan(1);
////            table.addCell(cellb6);
////            PdfPCell cellb14 = new PdfPCell(new Paragraph("14-batch #: "+batch[13], font));
////            cellb14.setColspan(1);
////            table.addCell(cellb14);
////
////            PdfPCell cellb7 = new PdfPCell(new Paragraph("7-batch #: "+batch[6], font));
////            cellb7.setColspan(1);
////            table.addCell(cellb7);
////            PdfPCell cellb15 = new PdfPCell(new Paragraph("15-batch #: "+batch[14], font));
////            cellb15.setColspan(1);
////            table.addCell(cellb15);
////
////            PdfPCell cellb8 = new PdfPCell(new Paragraph("8-batch #: "+batch[7], font));
////            cellb8.setColspan(1);
////            table.addCell(cellb8);
////            PdfPCell cellb16 = new PdfPCell(new Paragraph("", font));
////            cellb16.setColspan(1);
////            table.addCell(cellb16);
//
//
//            // //dong 6
//
//            PdfPCell cell6 = new PdfPCell(new Paragraph("混合批號/Batch # after combined/số mẻ sau kết hợp", font));
//            cell6.setColspan(2);
//            table.addCell(cell6);
//            //dong 7
//            PdfPCell cell7 = new PdfPCell(new Paragraph(arrBTC.get(0).getBTC_NO(), fontdynamic));
//            cell7.setColspan(2);
//            cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
//            table.addCell(cell7);
//            //LOT_WEIGHT
//            PdfPCell cell8 = new PdfPCell(new Paragraph("批次重量/Lot Weight/ Trọng lượng kết hợp" + ":" + edt_weight.getText().toString(), font));
//            cell8.setColspan(2);
//            table.addCell(cell8);
//            //dong 9 tgsd
//            PdfPCell cell9 = new PdfPCell(new Paragraph("過期時間/Shelf life/Thời gian sử dụng" + ": " + edt_timestart.getText().toString() + " -> " + edt_timend.getText().toString(), font));
//            cell9.setColspan(2);
//            table.addCell(cell9);
//
//            // sửa QA xác nhận
////            if (factory.equals("FVI_II_305")){
//                //dong 10
//                PdfPCell cell10 = new PdfPCell(new Paragraph("2h.S.G 比重 tỷ trọng" + ": " + edt_tytrong.getText().toString(), font));
//                cell10.setColspan(1);
//                table.addCell(cell10);
//                PdfPCell cell102 = new PdfPCell(new Paragraph("QC 判定", font4));
//                cell102.setColspan(1);
//                cell102.setRowspan(4);
//                table.addCell(cell102);
////            }else{
////                PdfPCell cell102 = new PdfPCell(new Paragraph("QA 判定/ QA XÁC NHẬN", font4));
////                cell102.setColspan(2);
////                cell102.setRowspan(4);
////                table.addCell(cell102);
////            }
//
//
//
////            if (factory.equals("FVI_II_305")){
//                //dong 11
//                PdfPCell cell11 = new PdfPCell(new Paragraph("2h.H.D 硬度 độ cứng" + ": " + edt_dc.getText().toString(), font));
//                cell11.setColspan(1);
//                table.addCell(cell11);
//                //dong 12
//                PdfPCell cell12 = new PdfPCell(new Paragraph("發泡倍率/E.R/Tỉ lệ pha phao" + ": " + edt_er.getText().toString(), font));
//                cell12.setColspan(1);
//                cell12.setRowspan(2);
//                table.addCell(cell12);
////            }
//
//
//            //
//            PdfPCell cell13 = new PdfPCell(new Paragraph("QA-4-020", font));
//            cell13.setColspan(2);
//            cell13.setBorder(0);
//            table.addCell(cell13);
//            //
//            PdfPCell cell14 = new PdfPCell(new Paragraph("Rev 01(01.10.2015)", font));
//            cell14.setColspan(2);
//            cell14.setBorder(0);
//            table.addCell(cell14);
//            //add Table into PDF
//            document.add(table);
//            document.close();
//
//            try {
//                SendMail sendmail = new SendMail();
//                sendmail.sendmail(factory,dest,model);
//                View toastView = getLayoutInflater().inflate(R.layout.activity_toast_custom_view, null);
//
//                // Initiate the Toast instance.
//                Toast toast = new Toast(getApplicationContext());
//                // Set custom view in toast.
//                toast.setView(toastView);
//                toast.setDuration(Toast.LENGTH_SHORT);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            File file = new File(dest);
//            Uri photoURI = FileProvider.getUriForFile(pdfActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);
//            Intent intent = ShareCompat.IntentBuilder.from(this)
//                    .setStream(photoURI) // uri from FileProvider
//                    .setType("text/html")
//                    .getIntent()
//                    .setAction(Intent.ACTION_VIEW) //Change if needed
//                    .setDataAndType(photoURI, "application/pdf")
//                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            startActivity(intent);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    public void createfolder() {
        File folder = new File(Environment.getExternalStorageDirectory() + "/Qam_rp");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            // Do something on success
        } else {
            // Do something else on failure
        }
    }

    public void createExcel_FVI_II( ) throws FileNotFoundException {
        AssetManager am = getApplicationContext().getAssets();
        FileOutputStream outFile = null;
        InputStream inputStream = null;
        Workbook workbook = null;
//        model = new MODEL_SEND_MAIL();
//        model.setDATE(date);
//        model.setSHIFT(shift);
        try {
            FILE_NAME = arrBTC.get(0).getBTC_NO()+"_ CARD 2";
            File ex = new File(Environment.getExternalStorageDirectory().toString() +
                    FILE_NAME + ".xls");
            if (!ex.exists()) {
                inputStream = am.open("ExCard2_fiv2.xls");
                workbook = getWorkbook(inputStream, "ExCard2_fiv2.xls");
            } else {
                inputStream = new FileInputStream(ex);
                workbook = getWorkbook(inputStream, FILE_NAME + ".xls");
            }

            Sheet sheet = workbook.getSheetAt(0);
            Cell cell;
            Row row;
            CellStyle style = createStyleForTitle(workbook);
//msc vs Er
            row = sheet.getRow(2);
            cell = row.getCell(1);
            cell.setCellValue(tv_mcs.getText().toString());
            cell.setCellStyle(style);

//hinh the
            row = sheet.getRow(3);
            cell = row.getCell(2);
            cell.setCellValue(tv_model.getText().toString());
            cell.setCellStyle(style);
//mau sác
            row = sheet.getRow(4);
            cell = row.getCell(2);
            cell.setCellValue(tv_color.getText().toString());
            cell.setCellStyle(style);

            row = sheet.getRow(16);
            cell = row.getCell(3);
            cell.setCellValue(edt_weight.getText().toString());
            cell.setCellStyle(style);

            row = sheet.getRow(17);
            cell = row.getCell(3);
            cell.setCellValue(edt_timestart.getText().toString() + "-" + edt_timend.getText().toString());
            cell.setCellStyle(style);

            //list ma dot
            int rownum=6;
            for (int i=0;i<arrBTC.size();i++){
                row = sheet.getRow(rownum+i);
                cell = row.getCell(1);
                cell.setCellValue(arrBTC.get(i).getBTC_BATCH_NO() == null ? "" : arrBTC.get(i).getBTC_BATCH_NO());
                cell.setCellStyle(style);
                if (i==8){
                    break;
                }
            }
            if (arrBTC.size()>8){
                rownum=6;
                int count=0;
                for (int i=8;i<arrBTC.size();i++){
                    row = sheet.getRow(rownum+count);
                    cell = row.getCell(3);
                    cell.setCellValue(arrBTC.get(i).getBTC_BATCH_NO() == null ? "" : arrBTC.get(i).getBTC_BATCH_NO());
                    cell.setCellStyle(style);
                    count++;
                }
            }

//            row = sheet.getRow(6);
//            cell = row.getCell(1);
//            cell.setCellValue(arrBTC.get(0).getBTC_BATCH_NO() == null ? "" : arrBTC.get(0).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(7);
//            cell = row.getCell(1);
//            cell.setCellValue(arrBTC.get(1).getBTC_BATCH_NO() == null ? "" : arrBTC.get(1).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(8);
//            cell = row.getCell(1);
//            cell.setCellValue(arrBTC.get(2).getBTC_BATCH_NO() == null ? "" : arrBTC.get(2).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(9);
//            cell = row.getCell(1);
//            cell.setCellValue(arrBTC.get(3).getBTC_BATCH_NO() == null ? "" : arrBTC.get(3).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(10);
//            cell = row.getCell(1);
//            cell.setCellValue(arrBTC.get(4).getBTC_BATCH_NO() == null ? "" : arrBTC.get(4).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(11);
//            cell = row.getCell(1);
//            cell.setCellValue(arrBTC.get(5).getBTC_BATCH_NO() == null ? "" : arrBTC.get(5).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(12);
//            cell = row.getCell(1);
//            cell.setCellValue(arrBTC.get(6).getBTC_BATCH_NO() == null ? "" : arrBTC.get(6).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(13);
//            cell = row.getCell(1);
//            cell.setCellValue(arrBTC.get(7).getBTC_BATCH_NO() == null ? "" : arrBTC.get(7).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(6);
//            cell = row.getCell(3);
//            cell.setCellValue(arrBTC.get(8).getBTC_BATCH_NO() == null ? "" : arrBTC.get(8).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(7);
//            cell = row.getCell(3);
//            cell.setCellValue(arrBTC.get(9).getBTC_BATCH_NO() == null ? "" : arrBTC.get(9).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(8);
//            cell = row.getCell(3);
//            cell.setCellValue(arrBTC.get(10).getBTC_BATCH_NO() == null ? "" : arrBTC.get(10).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(9);
//            cell = row.getCell(3);
//            cell.setCellValue(arrBTC.get(11).getBTC_BATCH_NO() == null ? "" : arrBTC.get(11).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(10);
//            cell = row.getCell(3);
//            cell.setCellValue(arrBTC.get(12).getBTC_BATCH_NO() == null ? "" : arrBTC.get(12).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(11);
//            cell = row.getCell(3);
//            cell.setCellValue(arrBTC.get(13).getBTC_BATCH_NO() == null ? "" : arrBTC.get(13).getBTC_BATCH_NO());
//            cell.setCellStyle(style);
//
//            row = sheet.getRow(12);
//            cell = row.getCell(3);
//            cell.setCellValue(arrBTC.get(14).getBTC_BATCH_NO() == null ? "" : arrBTC.get(14).getBTC_BATCH_NO());
//            cell.setCellStyle(style);



// mã số sau khi kêt hop
            row = sheet.getRow(15);
            cell = row.getCell(0);
            cell.setCellValue(arrBTC.get(0).getBTC_NO());
            sheet.addMergedRegion(new CellRangeAddress(15, 15, 0, 3));
            cell.setCellStyle(style);

            File check = new File(Environment.getExternalStorageDirectory().toString() +
                    "/Qam_rp/" + FILE_NAME + ".xls");
            if (check.exists()) {
                check.delete();
            }
            outFile = new FileOutputStream(check);
            workbook.write(outFile);

            System.out.println("Created file: " + check.getAbsolutePath());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outFile != null) {
                try {
                    outFile.flush();
                    outFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void createExcel_FVI() throws FileNotFoundException {
        AssetManager am = getApplicationContext().getAssets();
        FileOutputStream outFile = null;
        InputStream inputStream = null;
        Workbook workbook = null;
        try {
            FILE_NAME = "ID - BATCH TRACKING CARD .2 ";
            File ex = new File(Environment.getExternalStorageDirectory().toString() +
                    FILE_NAME + ".xls");
            if (!ex.exists()) {
                inputStream = am.open("ExCard2_fiv.xls");
                workbook = getWorkbook(inputStream, "ExCard2_fiv.xls");
            } else {
                inputStream = new FileInputStream(ex);
                workbook = getWorkbook(inputStream, FILE_NAME + ".xls");
            }
            Sheet sheet = workbook.getSheetAt(0);
            Cell cell;
            Row row;
            CellStyle style = createStyleForTitle(workbook);
//msc vs Er
            row = sheet.getRow(2);
            cell = row.getCell(1);
            cell.setCellValue(tv_mcs.getText().toString());
            cell.setCellStyle(style);

            row = sheet.getRow(2);
            cell = row.getCell(3);
            cell.setCellValue(edt_er.getText().toString());
            cell.setCellStyle(style);
//hinh the
            row = sheet.getRow(3);
            cell = row.getCell(2);
            cell.setCellValue(tv_model.getText().toString());
            cell.setCellStyle(style);
//mau sác
            row = sheet.getRow(4);
            cell = row.getCell(2);
            cell.setCellValue(tv_color.getText().toString());
            cell.setCellStyle(style);

//ti trong vs trong luong

            row = sheet.getRow(16);
            cell = row.getCell(3);
            cell.setCellValue(edt_weight.getText().toString());
            cell.setCellStyle(style);

            row = sheet.getRow(17);
            cell = row.getCell(3);
            cell.setCellValue(edt_timend.getText().toString());
            cell.setCellStyle(style);

            //list ma dot
            int rownum=6;
            for (int i=0;i<arrBTC.size();i++){
                row = sheet.getRow(rownum+i);
                cell = row.getCell(1);
                cell.setCellValue(arrBTC.get(i).getBTC_BATCH_NO() == null ? "" : arrBTC.get(i).getBTC_BATCH_NO());
                cell.setCellStyle(style);
                if (i==8){
                    break;
                }
            }
            if (arrBTC.size()>8){
                rownum=6;
                int count=0;
                for (int i=8;i<arrBTC.size();i++){
                    row = sheet.getRow(rownum+count);
                    cell = row.getCell(3);
                    cell.setCellValue(arrBTC.get(i).getBTC_BATCH_NO() == null ? "" : arrBTC.get(i).getBTC_BATCH_NO());
                    cell.setCellStyle(style);
                    count++;
                }
            }
// mã số sau khi kêt hop
            row = sheet.getRow(15);
            cell = row.getCell(0);
            cell.setCellValue(arrBTC.get(0).getBTC_NO());
            sheet.addMergedRegion(new CellRangeAddress(15, 15, 0, 3));
            cell.setCellStyle(style);

            File check = new File(Environment.getExternalStorageDirectory().toString() +
                    "/Qam_rp/" + FILE_NAME + ".xls");
            if (check.exists()) {
                check.delete();
            }
            outFile = new FileOutputStream(check);
            workbook.write(outFile);
            System.out.println("Created file: " + check.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outFile != null) {
                try {
                    outFile.flush();
                    outFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
        Workbook workbook = null;
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }
        return workbook;
    }


    public void sendmail_Card2(String name1) {
        AlertDialog.Builder b = new AlertDialog.Builder(pdfActivity.this);
        b.setTitle("Xác nhận");
        b.setMessage("Bạn có muốn gửi báo biểu về Mail?");
        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //sendmail
                MODEL_SEND_MAIL model = new MODEL_SEND_MAIL();
                model.setMSC(tv_mcs.getText().toString());
                model.setCOLOR(tv_color.getText().toString());
                model.setSTYLE(tv_model.getText().toString());
                model.setDATE(edt_daterp.getText().toString());
                model.setSHIFT(spn_shiftrp.getSelectedItem().toString());
                model.setBTC_NO(arrBTC.get(0).getBTC_NO());
                String name1 = DEST + FILE_NAME + ".xls";
                SendMail sendmail = new SendMail();
                sendmail.sendmail_ExcelCard2(factory, model,name1);
                ToastCustom.message(pdfActivity.this, "Gửi thành công!", Color.GREEN);
            }
        });
        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog al = b.create();
        al.show();
    }

    public void getshift() {
        spn_shiftrp = (Spinner) findViewById(R.id.spn_shiftrp);
        List<String> list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(R.layout.login_spiner);
        spn_shiftrp.setAdapter(dataAdapter);
    }

    public class InsArm extends ApiReturnModel<ArrayList<QAM_INS>> {
    }

    public class BtcArm extends ApiReturnModel<ArrayList<RP_BTC>> {
    }

    public class getIdColor extends ApiReturnModel<ArrayList<Temp1>> {
    }

}