package com.deanshoes.AppQAM.step4;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;

public class Temp_Note extends DialogFragment {

    private PopupFragementListener TempListener ;
    TextView tempNote;
    Button tempCancel,tempSave,tempClearNote;
    private static String mNote="";
    private static boolean mStatus=false;
    private static int posis=0;

    public static Temp_Note newInstance(PopupFragementListener listener, String note, boolean status, int pos) {

        Temp_Note fragement = new Temp_Note() ;
        fragement.TempListener = listener ;
        Bundle args = new Bundle();
        posis=pos;
        mStatus=status;
        mNote=note;
        fragement.setArguments(args);

        return fragement ;
    }

    public interface PopupFragementListener {

        void onSelected(String nt, int pos) ;
        void onCancel() ;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.temp_window, parent, false);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        setCancelable(false);
        tempNote= view.findViewById(R.id.txt_note);
        tempSave= view.findViewById(R.id.btn_save);
        tempCancel= view.findViewById(R.id.btn_cancel);
//        tempClearNote = view.findViewById(R.id.btn_clearNote);
        return view ;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

            if(mStatus==true)
            {
                tempNote.setEnabled(true);
                tempSave.setVisibility(View.GONE);
                tempNote.setFocusable(true);

            }
            else
            {
                tempNote.setEnabled(true);
                tempSave.setEnabled(true);
                tempNote.setFocusable(true);


            }
            tempNote.setText(mNote);
        tempEvent();

    }
    private void tempEvent() {
        tempSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TempListener.onSelected(tempNote.getText().toString(),posis);
            }
        });
        tempCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( TempListener != null ) {
                    TempListener.onCancel();
                }
            }
        });
//        tempClearNote.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                clearNote();
//            }
//        });

    }
    private void clearNote(){
        tempNote.setText("");
    }


}
