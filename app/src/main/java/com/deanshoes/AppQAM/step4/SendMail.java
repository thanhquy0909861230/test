package com.deanshoes.AppQAM.step4;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.deanshoes.AppQAM.model.QAM.MODEL_SEND_MAIL;
import com.deanshoes.AppQAM.model.QAM.QAM_TEMP_MODEL;
import com.deanshoes.AppQAM.model.report_model.RP_BTC;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;

import static com.deanshoes.AppQAM.step4.pdfActivity.date;
import static com.deanshoes.AppQAM.step4.pdfActivity.shift;

public class    SendMail {


    private String host = "5.1.1.1";
    private String from = "fvgit.public@freetrend.com.vn";
    SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy");

    private String toFVI = "kyo.loan@freetrend.com.vn";
    private String ccFVI = "kyo.loan@freetrend.com.vn,john.to@freetrend.com.vn";

//    private String toFVI = "kurt.chen@longyi-vn.com,fvi-hc02@longyi-vn.com,fvi-ch@longyi-vn.com";
//    private String ccFVI = "fvi-zl3@longyi-vn.com, fvi.zl5@longyi-vn.com,prince.hoang@freetrend.com.vn";



//    private String toFVI2 = "prince.hoang@freetrend.com.vn";
//    private String ccFVI2 = "";
    private String toFVI2 = "fvi-1.hc03@longyi-vn.com,fvi-1.hc05@longyi-vn.com";
    private String ccFVI2 = "kyo.loan@freetrend.com.vn,john.to@freetrend.com.vn";

//    private String toFVI2 = "colincheng@longyi-vn.com";
//    private String ccFVI2 = "fvi-1.hc03@longyi-vn.com,fvi-1.hc02@longyi-vn.com";

    SimpleDateFormat datefilename = new SimpleDateFormat("yyyyMMdd");
    String NAMEPDF = datefilename.format(new Date());
    String DEST = Environment.getExternalStorageDirectory().toString() + "/Qam_rp/" + NAMEPDF + "_Batch_Tracking.pdf";

    private String subject2_fvi = "IP-BATCH TRACKING CARD.2 - ";
    private String subject1_fvi = "IP-BATCH TRACKING CARD.1 - ";
    private String subject2_fvi2 = "ID-BATCH TRACKING CARD.2 - ";
    private String subject1_fvi2 = "ID-BATCH TRACKING CARD.1 - ";
    private String subject_report = "REPORT-";

    public void sendmail(final String factory,final String file,final MODEL_SEND_MAIL model) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                StringBuffer messageBf = new StringBuffer();
                messageBf.append("<div>");
                messageBf.append("Dear Manager,");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("<b>");
                if (factory.equals("FVI")){
                    messageBf.append(subject2_fvi);
                }else{
                    messageBf.append(subject2_fvi2);
                }

                messageBf.append("</b>");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("MCS : "+ model.getMSC());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("日期/Date/Ngày : "+ model.getDATE());

                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("班別/Shift/Ca biệt : "+ model.getSHIFT());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("型體/Model/Hình thể : "+ model.getSTYLE());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("顏色/Color/Màu Sắc : "+ model.getCOLOR());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<br>");
                messageBf.append("<div >");
                messageBf.append("<i>");
                messageBf.append("This email has been generated automatically, please do not reply!");
                messageBf.append("</i>");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div>");
                messageBf.append("<i>");
                messageBf.append("Best regards");
                messageBf.append("</i>");
                messageBf.append("</div>");


                Properties props = System.getProperties();
                props.put("mail.smtp.host", host);//set host
                Session session = Session.getDefaultInstance(props, null);
                session.setDebug(false);
                MimeMessage msg = new MimeMessage(session);
                try {
                    msg.setFrom(new InternetAddress(from));
                    if (factory.equals("FVI")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI != null && !"".equals(ccFVI)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
                        }
                    }else if (factory.equals("FVI_II_305")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI2);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI2 != null && !"".equals(ccFVI2)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI2);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
                        }
                    }

                    Multipart multipart = new MimeMultipart();
                    MimeBodyPart messageBodyPart1 = new MimeBodyPart();
                    messageBodyPart1.setContent(messageBf.toString(),"text/html; charset=UTF-8");
                    multipart.addBodyPart(messageBodyPart1);

                    MimeBodyPart mbp = new MimeBodyPart();
                    mbp.setContent(multipart);

                    Multipart mp2 = new MimeMultipart("mixed");
                    mp2.addBodyPart(mbp);

                    // create the second message part with the attachment from a OutputStrean
                    MimeBodyPart attachment = new MimeBodyPart();
                    DataSource ds = new ByteArrayDataSource(file.getBytes("UTF-8"), "application/octet-stream");
//                    FileDataSource fds = new FileDataSource(file);
                    attachment.setDataHandler(new DataHandler(ds));
                    if (factory.equals("FVI")){
                        attachment.setFileName(file.substring(file.lastIndexOf("/")));
                    }else{
                        attachment.setFileName(file.substring(file.lastIndexOf("/")).replace("IP","ID"));
                    }

                    mp2.addBodyPart(attachment);

                    if (factory.equals("FVI")){
                        msg.setSubject(subject2_fvi, "UTF-8");
                    }else{
                        msg.setSubject(subject2_fvi2, "UTF-8");
                    }

//                    msg.setText(messageBf.toString(),"UTF-8", "html");
                    msg.setContent(mp2);

                    Transport.send(msg);
                    Log.e("SUCCESS","ss");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void sendmail_card1(final String factory, final String file, final MODEL_SEND_MAIL model, final ArrayList<String> batch) {
        String Subject2 = "\n" +
                "ID-BATCH TRACKING CARD.2 - \n";

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                StringBuffer messageBf = new StringBuffer();
                messageBf.append("<html>");
                messageBf.append("<body>");
                messageBf.append("<div>");
                messageBf.append("Dear Manager,");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px; '>");
                messageBf.append("<b>");
                if (factory.equals("FVI")){
                    messageBf.append(subject1_fvi);
                }else{
                    messageBf.append(subject1_fvi2);
                }
                messageBf.append("</b>");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("MCS# : "+ model.getMSC());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("日期/Date/Ngày : "+ model.getDATE());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("班別/Shift/Ca biệt : "+ model.getSHIFT());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("型體/Model/Hình thể : "+ model.getSTYLE());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("顏色/Color/Màu Sắc : "+ model.getCOLOR());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<br>");
                messageBf.append("<table style='border-collapse: collapse;border: 1px solid black; width:40%; font-family: Noto Sans CJK SC Regular'>");
                messageBf.append("<tr style='border-collapse: collapse;border: 1px solid black; width:7%; font-family: Noto Sans CJK SC Regular'>");
                messageBf.append("<th style='background-color:Cornsilk; border-collapse: collapse;border: 1px solid black;width:100%' colspan='2'><p>批號/Batch No/Số Đợt</p></th>");
                messageBf.append("</tr>");

                int count = Math.round(batch.size()/2);
                for (int i=0;i<=count;i++){
                    messageBf.append("<tr style='border-collapse: collapse;border: 1px solid black'>");
                    if (batch.get(i)==null){
                        messageBf.append("<th style='border-collapse: collapse;border: 1px solid black'></th>");
                    }else{
                        messageBf.append("<th style='border-collapse: collapse;border: 1px solid black'><p>"+batch.get(i)+"</p></th>");
                    }
                    if (i==count){
                        messageBf.append("<th style='border-collapse: collapse;border: 1px solid black'></th>");
                        messageBf.append("</tr>");
                    }
                    else{
                        if (batch.get(i +count)==null){
                            messageBf.append("<th style='border-collapse: collapse;border: 1px solid black'></th>");
                        }else{
                            if (batch.size() % 2 == 0){
                                messageBf.append("<th style='border-collapse: collapse;border: 1px solid black'><p>"+batch.get(i +count)+"</p></th>");
                            }
                            else if (batch.size() % 2 != 0){
                                messageBf.append("<th style='border-collapse: collapse;border: 1px solid black'><p>"+batch.get(i + count + 1)+"</p></th>");
                            }
                        }
                        messageBf.append("</tr>");
                    }
                }

                messageBf.append("</table>");
                messageBf.append("<br>");
                messageBf.append("<div>");
                messageBf.append("Best Regards");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div>");
                messageBf.append("----- This email has been generated automatically, please do not reply----");
                messageBf.append("</div>");
                messageBf.append("</body>");
                messageBf.append("</html>");

                //
                Properties props = System.getProperties();
                props.put("mail.smtp.host", host);//set host
                Session session = Session.getDefaultInstance(props, null);
                session.setDebug(false);
                MimeMessage msg = new MimeMessage(session);
                try {
                    msg.setFrom(new InternetAddress(from));
                    if (factory.equals("FVI")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI != null && !"".equals(ccFVI)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
//InternetAddress[] addressBCC = new InternetAddress().parse(bccFVI);
//msg.setRecipients(Message.RecipientType.BCC, addressBCC);
                        }
                    }else if (factory.equals("FVI_II_305")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI2);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI2 != null && !"".equals(ccFVI2)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI2);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
                        }
                    }
                    Multipart multipart = new MimeMultipart();
                    MimeBodyPart messageBodyPart1 = new MimeBodyPart();
                    messageBodyPart1.setContent(messageBf.toString(),"text/html; charset=UTF-8");
                    // create the second message part with the attachment from a OutputStrean
                    multipart.addBodyPart(messageBodyPart1);
                    MimeBodyPart mbp = new MimeBodyPart();
                    mbp.setContent(multipart);

                    Multipart mp2 = new MimeMultipart("mixed");
                    mp2.addBodyPart(mbp);

                    MimeBodyPart attachment = new MimeBodyPart();
                    FileDataSource fds = new FileDataSource(file);
                    attachment.setDataHandler(new DataHandler(fds));
//                    attachment.setFileName(fds.getName());
                    if (factory.equals("FVI")){
                        attachment.setFileName(file.substring(file.lastIndexOf("/")));
                    }else{
                        attachment.setFileName(file.substring(file.lastIndexOf("/")).replace("IP","ID"));
                    }

                    mp2.addBodyPart(attachment);

                    if (factory.equals("FVI")){
                        msg.setSubject(subject1_fvi + model.getDATE() +"-"+ model.getSHIFT(), "UTF-8");
                    }else{
                        msg.setSubject(subject1_fvi2 + model.getDATE() +"-"+ model.getSHIFT(), "UTF-8");
                    }

                    msg.setContent(mp2);

                    Transport.send(msg);
                    Log.e("SUCCESS","ss");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void sendmail_report(final String factory, final String[] file, final MODEL_SEND_MAIL model, final String name) {
        String s1 = model.getMSC();
        final String s2= s1.replace('|','/');

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                StringBuffer messageBf = new StringBuffer();
                messageBf.append("<div>");
                messageBf.append("Dear Manager,");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("MCS : "+ s2);
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("日期/Date/Ngày : "+ model.getDATE());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("班別/Shift/Ca biệt : "+ model.getSHIFT());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("型體/Model/Hình thể : "+ model.getSTYLE());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("顏色/Color/Màu Sắc : "+ model.getCOLOR());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<br>");
                messageBf.append("<div >");
                messageBf.append("<i>");
                messageBf.append("This email has been generated automatically, please do not reply!");
                messageBf.append("</i>");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div>");
                messageBf.append("<i>");
                messageBf.append("Best regards");
                messageBf.append("</i>");
                messageBf.append("</div>");

                //
                Properties props = System.getProperties();
                props.put("mail.smtp.host", host);//set host
                Session session = Session.getDefaultInstance(props, null);
                session.setDebug(false);
                MimeMessage msg = new MimeMessage(session);
                try {
                    msg.setFrom(new InternetAddress(from));
                    if (factory.equals("FVI")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI != null && !"".equals(ccFVI)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
//InternetAddress[] addressBCC = new InternetAddress().parse(bccFVI);
//msg.setRecipients(Message.RecipientType.BCC, addressBCC);
                        }
                    }else if (factory.equals("FVI_II_305")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI2);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI2 != null && !"".equals(ccFVI2)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI2);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
                        }
                    }

                    Multipart multipart = new MimeMultipart();
                    MimeBodyPart messageBodyPart1 = new MimeBodyPart();
                    messageBodyPart1.setContent(messageBf.toString(),"text/html; charset=UTF-8");
                    // create the second message part with the attachment from a OutputStrean
                    multipart.addBodyPart(messageBodyPart1);
                    addAttachment_report(multipart,file[0], model.getCOLOR() ,name);
//                    addAttachment(multipart,file[1]);
//                    addAttachment(multipart,file[2]);
//                    addAttachment(multipart,file[3]);

                    if(factory.equals("FVI_II_305")){
                        msg.setSubject(name +"_" + model.getINS_NO()+"_"+ model.getCOLOR()+"_"+model.getMSC() + "_"+ model.getDATE() +"_"+ model.getSHIFT(), "UTF-8");
                        msg.setContent(multipart);
                    }else {
                        msg.setSubject(subject_report+model.getINS_NO()+" "+model.getCOLOR()+"_"+model.getMSC()+"_"+model.getDATE()+"_"+model.getSHIFT(), "UTF-8");
                        msg.setContent(multipart);
                    }

                    Transport.send(msg);
                    Log.e("SUCCESS","ss");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void sendmail_report1(final String factory, final String[] file, final MODEL_SEND_MAIL model, final String name) {
        String s1 = model.getMSC() ;
        final String s2= s1.replace('|','/');

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                StringBuffer messageBf = new StringBuffer();
                messageBf.append("<div>");
                messageBf.append("Dear Manager,");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("MCS : "+ s2);
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("日期/Date/Ngày : "+ model.getDATE());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("班別/Shift/Ca biệt : "+ model.getSHIFT());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("型體/Model/Hình thể : "+ model.getSTYLE());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("顏色/Color/Màu Sắc : "+ model.getCOLOR());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<br>");
                messageBf.append("<div >");
                messageBf.append("<i>");
                messageBf.append("This email has been generated automatically, please do not reply!");
                messageBf.append("</i>");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div>");
                messageBf.append("<i>");
                messageBf.append("Best regards");
                messageBf.append("</i>");
                messageBf.append("</div>");


                //
                Properties props = System.getProperties();
                props.put("mail.smtp.host", host);//set host
                Session session = Session.getDefaultInstance(props, null);
                session.setDebug(false);
                MimeMessage msg = new MimeMessage(session);
                try {
                    msg.setFrom(new InternetAddress(from));
                    if (factory.equals("FVI")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI != null && !"".equals(ccFVI)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
//InternetAddress[] addressBCC = new InternetAddress().parse(bccFVI);
//msg.setRecipients(Message.RecipientType.BCC, addressBCC);
                        }
                    }else if (factory.equals("FVI_II_305")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI2);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI2 != null && !"".equals(ccFVI2)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI2);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
                        }
                    }

                    Multipart multipart = new MimeMultipart();
                    MimeBodyPart messageBodyPart1 = new MimeBodyPart();
                    messageBodyPart1.setContent(messageBf.toString(),"text/html; charset=UTF-8");
                    // create the second message part with the attachment from a OutputStrean
                    multipart.addBodyPart(messageBodyPart1);
                    // addAttachment_report(multipart,file[0],name);
//                    addAttachment(multipart,file[1]);
//                    addAttachment(multipart,file[2]);
//                    addAttachment(multipart,file[3]);

                    if(factory.equals("FVI_II_305")){

                        msg.setSubject(subject_report+model.getINS_NO()+s2+"_"+model.getDATE()+"_"+model.getSHIFT(), "UTF-8");
                        msg.setContent(multipart);
                    }else {
//                        msg.setSubject(subject_report+model.getINS_NO()+model.getMSC()+"_"+model.getCOLOR()+"_"+model.getDATE()+"_"+model.getSHIFT(), "UTF-8");
                        msg.setSubject(subject_report+model.getINS_NO()+model.getMSC()+"_"+model.getCOLOR()+"_"+model.getDATE()+"_"+model.getSHIFT(), "UTF-8");
                        msg.setContent(multipart);
                    }



                    Transport.send(msg);
                    Log.e("SUCCESS","ss");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }



    public void sendmail_ExcelCard2(final String factory,final MODEL_SEND_MAIL model, final String file) {
//        final String s2 = model.getDATE();

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

//                MODEL_SEND_MAIL   model = new MODEL_SEND_MAIL();
//                String s1 =  model.getSHIFT();



//                StringBuffer messageBf = new StringBuffer();
//                messageBf.append("<div>");
//                messageBf.append("Dear Manager,");
//                messageBf.append("</div>");
//                messageBf.append("<br>");
//
//                messageBf.append("<div >");
//                messageBf.append("<i>");
//                messageBf.append("This email has been generated automatically, please do not reply!");
//                messageBf.append("</i>");
//                messageBf.append("</div>");
//                messageBf.append("<br>");
//                messageBf.append("<div>");
//                messageBf.append("<i>");
//                messageBf.append("Best regards");
//                messageBf.append("</i>");
//                messageBf.append("</div>");
                StringBuffer messageBf = new StringBuffer();
                messageBf.append("<html>");
                messageBf.append("<body>");
                messageBf.append("<div>");
                messageBf.append("Dear Manager,");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px; '>");
                messageBf.append("<b>");
                if (factory.equals("FVI")){
                    messageBf.append(subject2_fvi);
                }else{
                    messageBf.append(subject2_fvi2);
                }
                messageBf.append("</b>");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("MCS# : "+ model.getMSC());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("日期/Date/Ngày : "+ model.getDATE());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("班別/Shift/Ca biệt : "+ model.getSHIFT());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("型體/Model/Hình thể : "+ model.getSTYLE());
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div style = 'text-indent: 30px;'>");
                messageBf.append("顏色/Color/Màu Sắc : "+ model.getCOLOR());
                messageBf.append("</div>");

                Properties props = System.getProperties();
                props.put("mail.smtp.host", host);//set host
                Session session = Session.getDefaultInstance(props, null);
                session.setDebug(false);
                MimeMessage msg = new MimeMessage(session);
                try {
                    msg.setFrom(new InternetAddress(from));
                    if (factory.equals("FVI")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI != null && !"".equals(ccFVI)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
//InternetAddress[] addressBCC = new InternetAddress().parse(bccFVI);
//msg.setRecipients(Message.RecipientType.BCC, addressBCC);
                        }
                    }else if (factory.equals("FVI_II_305")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI2);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI2 != null && !"".equals(ccFVI2)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI2);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
                        }
                    }

                    Multipart multipart = new MimeMultipart();
                    MimeBodyPart messageBodyPart1 = new MimeBodyPart();
                    messageBodyPart1.setContent(messageBf.toString(),"text/html; charset=UTF-8");
                    // create the second message part with the attachment from a OutputStrean
                    multipart.addBodyPart(messageBodyPart1);
                    addAttachment(multipart,file);
//                    addAttachment(multipart,file[1]);
//                    addAttachment(multipart,file[2]);
//                    addAttachment(multipart,file[3]);

                    msg.setSubject(subject_report+model.getBTC_NO()+"_CARD 2_"+model.getMSC()+"_"+model.getCOLOR()+"_"+model.getDATE(), "UTF-8");
                    msg.setContent(multipart);

                    Transport.send(msg);
                    Log.e("SUCCESS","ss");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }


    public  void getDateShilt(String date, String shift,String color,String MCS, MODEL_SEND_MAIL model){
        model.setSHIFT(shift);
        model.setDATE(date);

    }



    private String CA ="",Date = "";
    public void sendmail_TempModel(final String factory, final String file, final QAM_TEMP_MODEL model) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                StringBuffer messageBf = new StringBuffer();
                messageBf.append("<div>");
                messageBf.append("Dear Manager,");
                messageBf.append("</div>");
                messageBf.append("<br>");

                messageBf.append("<div >");
                messageBf.append("<i>");
                messageBf.append("This email has been generated automatically, please do not reply!");
                messageBf.append("</i>");
                messageBf.append("</div>");
                messageBf.append("<br>");
                messageBf.append("<div>");
                messageBf.append("<i>");
                messageBf.append("Best regards");
                messageBf.append("</i>");
                messageBf.append("</div>");

                //
                Properties props = System.getProperties();
                props.put("mail.smtp.host", host);//set host
                Session session = Session.getDefaultInstance(props, null);
                session.setDebug(false);
                MimeMessage msg = new MimeMessage(session);
                try {
                    msg.setFrom(new InternetAddress(from));
                    if (factory.equals("FVI")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI != null && !"".equals(ccFVI)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
//InternetAddress[] addressBCC = new InternetAddress().parse(bccFVI);
//msg.setRecipients(Message.RecipientType.BCC, addressBCC);
                        }
                    }else if (factory.equals("FVI_II_305")){
                        InternetAddress[] addressTo = new InternetAddress().parse(toFVI2);
                        msg.setRecipients(Message.RecipientType.TO, addressTo);
                        if (ccFVI2 != null && !"".equals(ccFVI2)) {
                            InternetAddress[] addressCC = new InternetAddress().parse(ccFVI2);
                            msg.setRecipients(Message.RecipientType.CC, addressCC);
                        }
                    }

                    Multipart multipart = new MimeMultipart();
                    MimeBodyPart messageBodyPart1 = new MimeBodyPart();
                    messageBodyPart1.setContent(messageBf.toString(),"text/html; charset=UTF-8");
                    // create the second message part with the attachment from a OutputStrean
                    multipart.addBodyPart(messageBodyPart1);
                    addAttachment(multipart,file);
//                    addAttachment(multipart,file[1]);
//                    addAttachment(multipart,file[2]);
//                    addAttachment(multipart,file[3]);

                    msg.setSubject("BIỂU KIỂM NGHIỆM NHIỆT ĐỘ KHUÔN IP - " + "Ca: " + model.getCA()
                            + "__" + " Máy: " + model.getMAY() + "__" + " Ngày: " + model.getCHECK_DATE(), "UTF-8");
                    msg.setContent(multipart);

                    Transport.send(msg);
                    Log.e("SUCCESS","ss");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }



    private static void addAttachment(Multipart multipart, String filename) throws MessagingException {
        DataSource source = new FileDataSource(filename);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(filename.substring(filename.lastIndexOf("/")));
        multipart.addBodyPart(messageBodyPart);
    }

    private static void addAttachment_report(Multipart multipart, String filename, String model, String name ) throws MessagingException {
        DataSource source = new FileDataSource(filename);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setDataHandler(new DataHandler(source));
        String ss = filename.substring(filename.lastIndexOf("/"));
        String str= ss.substring(ss.indexOf("_")+1);
        messageBodyPart.setFileName(name +  "_" + model + "_" + str  );
        multipart.addBodyPart(messageBodyPart);
    }
}
