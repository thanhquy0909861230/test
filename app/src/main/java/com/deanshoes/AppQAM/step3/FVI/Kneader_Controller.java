package com.deanshoes.AppQAM.step3.FVI;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.ListKNAdapter;
import com.deanshoes.AppQAM.model.QAM.BTC;
import com.deanshoes.AppQAM.model.QAM.MCS;
import com.deanshoes.AppQAM.model.QAM.QAM_INS;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.deanshoes.AppQAM.step3.Popup_Note;
import com.deanshoes.AppQAM.step4.pdfActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Kneader_Controller extends AppCompatActivity {
    private Gson gson = new GsonBuilder().setDateFormat("MM/dd/yyyy HH:mm:ss").create();
    Spinner spn_shift, spn_batchno;
    String arrShift[] = {"1", "2", "3"};
    ArrayAdapter<String> adtListShift;
    private ArrayList<BTC> listBtcs;
    ListKNAdapter listKNAdapter;
    ListView lv_knearder;
    EditText edt_Date, edt_firsttime, edt_time1, edt_time2, edt_time3, edt_time4, edt_endtime, edt_firsttemp, edt_temp1, edt_temp2, edt_temp3, edt_temp4, edt_endtemp, edt_note;
    Button btn_first, btn_1, btn_2, btn_3, btn_4, btn_end, btn_edit, btn_save, btn_cancel, btn_next,btn_mac,btn_kn,btn_step2,btn_step4;
    DatePickerDialog datePickerDialog;
    ImageView temp_note1,temp_note2,temp_note3,temp_note4,temp_note5,temp_note6;
    ImageView temp_recheck1,temp_recheck2,temp_recheck3,temp_recheck4,temp_recheck5,temp_recheck6;

    private List<QAM_INS> mcsl;
    ArrayList<QAM_INS> arrMCS;
    String line;
    private List<MCS> mcsinfor;
    ArrayList<String> arrGroupID;
    EditText edt_firststd_min,edt_temp1std_min,edt_temp2std_min,edt_temp3std_min,edt_temp4std_min ,edt_endstd_min;
    EditText edt_firststd_max,edt_temp1std_max,edt_temp2std_max,edt_temp3std_max,edt_temp4std_max ,edt_endstd_max;

    String factory = "";
    BTC btc;
    BTC btc_current;
    String btc_no = "";
    String mcs_no = "",batch_no="",model="",hardness="",color="",user ="",mcs_name;
    String ins_no11, itemfix;
    private static boolean statusPop;
    int positions=-1;
    ImageButton ib_logout;

    String[] STD_170FS_min, STD_XL4MB_min, STD_ALL_min,STD_170FS_max, STD_XL4MB_max, STD_ALL_max,
            STD_CR128_min, STD_CR128_max;

    String[] STD_170FS_min_II, STD_XL4MB_min_II, STD_ALL_min_II,STD_170FS_max_II, STD_XL4MB_max_II, STD_ALL_max_II;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kneader);

        Intent intent=getIntent();
        final Bundle bundle=intent.getExtras();
        if (bundle!=null){
            user=bundle.getString("user","ADMIN");
            factory=bundle.getString("fac");
        }

        STD_170FS_min = new String[]{"75","95","105","115","125","122"};
        STD_XL4MB_min = new String[]{"75","100","110","115","125","130"};
        STD_CR128_min = new String[]{"75","95","110","110","115","110"};
        STD_ALL_min =   new String[]{"75","95","110","110","115","110"};

        STD_170FS_max = new String[]{"85","105","115","125","130","126"};
        STD_XL4MB_max = new String[]{"85","110","120","125","135","135"};
        STD_CR128_max = new String[]{"85","110","120","120","125","120"};
        STD_ALL_max =   new String[]{"85","110","120","120","125","120"};

        STD_170FS_min_II = new String[]{"75","95","105","115","125","122"};
        STD_XL4MB_min_II = new String[]{"75","100","110","115","125","130"};
        STD_ALL_min_II = new String[]{"75","95","105","110","115","110"};

        STD_170FS_max_II = new String[]{"85","105","115","125","130","126"};
        STD_XL4MB_max_II = new String[]{"85","110","120","125","135","135"};
        STD_ALL_max_II = new String[]{"85","110","115","120","125","120"};

        btc_current=new BTC();
        temp_note1 = findViewById(R.id.temp_note1);
        temp_note2 = findViewById(R.id.temp_note2);
        temp_note3 = findViewById(R.id.temp_note3);
        temp_note4 = findViewById(R.id.temp_note4);
        temp_note5 = findViewById(R.id.temp_note5);
        temp_note6 = findViewById(R.id.temp_note6);

        temp_recheck1 = findViewById(R.id.temp_recheck1);
        temp_recheck2 = findViewById(R.id.temp_recheck2);
        temp_recheck3 = findViewById(R.id.temp_recheck3);
        temp_recheck4 = findViewById(R.id.temp_recheck4);
        temp_recheck5 = findViewById(R.id.temp_recheck5);
        temp_recheck6 = findViewById(R.id.temp_recheck6);

        btn_step2  = findViewById(R.id.btn_step2);
        btn_step4 = findViewById(R.id.btn_step4);
        lv_knearder = findViewById(R.id.lv_knearder);
        spn_shift = findViewById(R.id.spn_shift);
        edt_Date = findViewById(R.id.edt_date);
        spn_batchno = findViewById(R.id.spn_batchno);
        adtListShift = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrShift);
        spn_shift.setAdapter(adtListShift);

        edt_firsttime = findViewById(R.id.edt_firsttime);
        edt_time1 = findViewById(R.id.edt_time1);
        edt_time2 = findViewById(R.id.edt_time2);
        edt_time3 = findViewById(R.id.edt_time3);
        edt_time4 = findViewById(R.id.edt_time4);
        edt_endtime = findViewById(R.id.edt_endtime);
        btn_first = findViewById(R.id.btn_first);
        btn_1 = findViewById(R.id.btn_1);
        btn_2 = findViewById(R.id.btn_2);
        btn_3 = findViewById(R.id.btn_3);
        btn_4 = findViewById(R.id.btn_4);
        btn_end = findViewById(R.id.btn_end);

        edt_firststd_min = findViewById(R.id.edt_firststd_min);
        edt_temp1std_min = findViewById(R.id.edt_temp1std_min);
        edt_temp2std_min = findViewById(R.id.edt_temp2std_min);
        edt_temp3std_min = findViewById(R.id.edt_temp3std_min);
        edt_temp4std_min = findViewById(R.id.edt_temp4std_min);
        edt_endstd_min = findViewById(R.id.edt_endstd_min);

        edt_firststd_max = findViewById(R.id.edt_firststd_max);
        edt_temp1std_max = findViewById(R.id.edt_temp1std_max);
        edt_temp2std_max = findViewById(R.id.edt_temp2std_max);
        edt_temp3std_max = findViewById(R.id.edt_temp3std_max);
        edt_temp4std_max = findViewById(R.id.edt_temp4std_max);
        edt_endstd_max = findViewById(R.id.edt_endstd_max);

        edt_firsttemp = findViewById(R.id.edt_firsttemp);
        edt_temp1 = findViewById(R.id.edt_temp1);
        edt_temp2 = findViewById(R.id.edt_temp2);
        edt_temp3 = findViewById(R.id.edt_temp3);
        edt_temp4 = findViewById(R.id.edt_temp4);
        edt_endtemp = findViewById(R.id.edt_endtemp);
        edt_note = findViewById(R.id.edt_note);
        ib_logout = findViewById(R.id.ib_logout);

        btn_edit = findViewById(R.id.btn_edit);
        btn_save = findViewById(R.id.btn_save);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_next = findViewById(R.id.btn_next);
        btn_mac= findViewById(R.id.btn_mac);
        //btn_kn= findViewById(R.id.btn_kn);
        btn_next.setEnabled(true);
        btn_mac.setEnabled(true);
        event_lv();

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_save.setEnabled(true);
                btn_cancel.setEnabled(true);
                btn_next.setEnabled(false);
                btn_mac.setEnabled(false);
                btn_edit.setEnabled(false);
                spn_shift.setEnabled(false);
                spn_batchno.setEnabled(false);
                edt_Date.setEnabled(false);
                btnDefault(true);
                listKNAdapter.disableitem(true);
                statusPop=false;


            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (savekn(btc_no) == 1) {

                    btnDefault(false);
                    btn_save.setEnabled(false);
                    btn_cancel.setEnabled(false);
                    btn_next.setEnabled(true);
                    btn_mac.setEnabled(true);
                    spn_shift.setEnabled(true);
                    btn_edit.setEnabled(true);
                    spn_batchno.setEnabled(true);
                    edt_Date.setEnabled(true);
                    ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                    listKNAdapter.disableitem(false);
                    statusPop=true;
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDefault(false);
                btn_save.setEnabled(false);
                btn_cancel.setEnabled(false);
                btn_next.setEnabled(true);
                btn_mac.setEnabled(true);
                btn_edit.setEnabled(true);
                spn_shift.setEnabled(true);
                spn_batchno.setEnabled(true);
                edt_Date.setEnabled(true);
                listKNAdapter.disableitem(false);
                statusPop=true;
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Kneader_Controller.this, OpenMill_Controller.class);
                myIntent.putExtras(bundle);
                Kneader_Controller.this.startActivity(myIntent);
            }
        });
        btn_mac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Kneader_Controller.this, Pelletization_Controller.class);
                myIntent.putExtras(bundle);
                Kneader_Controller.this.startActivity(myIntent);
            }
        });

        btn_step2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Kneader_Controller.this, Mac1Activity_FVI.class);
                myIntent.putExtras(bundle);
                Kneader_Controller.this.startActivity(myIntent);
            }
        });
        btn_step4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Kneader_Controller.this, pdfActivity.class);
                myIntent.putExtras(bundle);
                Kneader_Controller.this.startActivity(myIntent);
            }
        });



        edt_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog(edt_Date);
            }
        });
        spn_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String item = parentView.getItemAtPosition(position).toString();

//                String[] parts = item.split("|");
//                String part1 = parts[0]; // 004
//                //String part2 = parts[1]; // 034556

                try {
                    if (edt_Date.getText().toString().isEmpty()) {
                    } else {
                        //getMCS(edt_Date.getText().toString(), item);
                        getMCS(item, edt_Date.getText().toString());

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        spn_batchno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
//                String item1[];
                String item = spn_batchno.getSelectedItem().toString();
//                item1 =item.split("|");
//                itemfix = item1[0];


                String[] ins_no1 = spn_batchno.getSelectedItem().toString().split("_");
                ins_no11 = ins_no1[0];
                try {
                    //getBTC(parentView.getItemAtPosition(position).toString(), edt_Date.getText().toString(), spn_shift.getSelectedItem().toString());
                    //   getBTC2(parentView.getItemAtPosition(position).toString(), spn_shift.getSelectedItem().toString(), edt_Date.getText().toString());

                    for (int i=0;i<arrGroupID.size();i++){
                        if (item.equals(arrMCS.get(i).getINS_NO()) && position==i){
                            mcs_no=arrMCS.get(i).getMCS_NO();
                            getInfor(mcs_no);
                            getBTC2(ins_no11,spn_shift.getSelectedItem().toString(),edt_Date.getText().toString());

                        }
//                        if (item.equals(arrMCS.get(i).getINS_NO()) && position==i){
//                            mcs_no=arrMCS.get(i).getMCS_NO();
//
//                            String[] ins_no1 = spn_batchno.getSelectedItem().toString().split("_");
//                            ins_no11 = ins_no1[1];
//                            getInfor(ins_no11);
//                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Kneader_Controller.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btn_clicksettime();

        bindevent();
        statusPop=true;

    }

    public void Type_STD(String name){
        String[] str_min = new String[6], str_max = new String[6];
        if (factory.equals("FVI")){
            if (name.contains("170FS|170FS")){
                str_min=STD_170FS_min;
                str_max=STD_170FS_max;
            }else if(name.contains("XL4MB") || name.contains("Xl4MB")){
                str_min=STD_XL4MB_min;
                str_max=STD_XL4MB_max;
            }else {
                str_min=STD_ALL_min;
                str_max=STD_ALL_max;
            }
        }else if(factory.equals("FVI_II_305")){
            if (name.contains("170FS")){
                str_min=STD_170FS_min_II;
                str_max=STD_170FS_max_II;
            }else if(name.contains("XL4MB")){
                str_min=STD_XL4MB_min_II;
                str_max=STD_XL4MB_max_II;
            }else {
                str_min=STD_ALL_min_II;
                str_max=STD_ALL_max_II;
            }
        }


        edt_firststd_min.setText(str_min[0]);
        edt_temp1std_min.setText(str_min[1]);
        edt_temp2std_min.setText(str_min[2]);
        edt_temp3std_min.setText(str_min[3]);
        edt_temp4std_min.setText(str_min[4]);
        edt_endstd_min.setText(str_min[5]);

        edt_firststd_max.setText(str_max[0]);
        edt_temp1std_max.setText(str_max[1]);
        edt_temp2std_max.setText(str_max[2]);
        edt_temp3std_max.setText(str_max[3]);
        edt_temp4std_max.setText(str_max[4]);
        edt_endstd_max.setText(str_max[5]);
    }

    public String getValueSTD(String str, int type){
        if (type==0){
            str=str.substring(0,str.indexOf("-"));
        }else{
            str=str.substring(str.indexOf("-")+1);
        }
        return str;
    }

    public String STD_Type(String name, int pos,int type){
        String[] str = new String[6];
        if (factory.equals("FVI")){
            if (type==0){
                if (name.contains("170FS|170FS")){
                    str=STD_170FS_min;
                }else if(name.contains("XL4MB") || name.contains("Xl4MB")){
                    str=STD_XL4MB_min;
                }else str=STD_ALL_min;
            }else{
                if (name.contains("170FS|170FS")){
                    str=STD_170FS_max;
                }else if(name.contains("XL4MB") || name.contains("Xl4MB")){
                    str=STD_XL4MB_max;
                }else str=STD_ALL_max;
            }
        }else if(factory.equals("FVI_II_305")){
            if (type==0){
                if (name.contains("170FS")){
                    str=STD_170FS_min_II;
                }else if(name.contains("XL4MB")){
                    str=STD_XL4MB_min_II;
                }else str=STD_ALL_min_II;
            }else{
                if (name.contains("170FS")){
                    str=STD_170FS_max_II;
                }else if(name.contains("XL4MB")){
                    str=STD_XL4MB_max_II;
                }else str=STD_ALL_max_II;
            }
        }



        switch (pos){
            case 0:
                return str[0];
            case 1:
                return str[1];
            case 2:
                return str[2];
            case 3:
                return str[3];
            case 4:
                return str[4];
            case 5:
                return str[5];
        }

        return "";
    }


    public void getInfor(String mcs_no){
        try {
            CallApi.getInforMcs(factory, mcs_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Mac1Activity_FVI.McsArm mcsArm = gson.fromJson(response.toString(), Mac1Activity_FVI.McsArm.class);
                    mcsinfor = mcsArm.getItems() == null ? new ArrayList<MCS>() : mcsArm.getItems();
                    if(mcsinfor.size()!=0) {
                        mcs_name = mcsinfor.get(0).getMSC();
                        Type_STD(mcs_name);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public String CountTime(String Itime, int Imin, int Isec){
        String hour="",min="",sec="";
        Calendar time = Calendar.getInstance();
        if(Itime.length()<=5){
            Itime = Itime+":00";
        }

        hour=Itime.substring(0,Itime.indexOf(":"));
        min=Itime.substring(Itime.indexOf(":")+1,Itime.lastIndexOf(":"));
        sec=Itime.substring(Itime.lastIndexOf(":")+1);
        try{
            time.set(Calendar.HOUR_OF_DAY,Integer.parseInt(hour));
            time.set(Calendar.MINUTE,Integer.parseInt(min));
            time.set(Calendar.SECOND,Integer.parseInt(sec));

            time.add(Calendar.MINUTE,+Imin);
            time.add(Calendar.SECOND,+Isec);
        }catch (Exception e){
            e.printStackTrace();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        return sdf.format(time.getTime());
    }

    public void bindevent()
    {
        temp_note1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_F_NOTE(),statusPop,1);
            }
        });
        temp_note2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_1_NOTE(),statusPop,2);
            }
        });
        temp_note3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_2_NOTE(),statusPop,3);
            }
        });
        temp_note4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_3_NOTE(),statusPop,4);
            }
        });
        temp_note5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_4_NOTE(),statusPop,5);
            }
        });
        temp_note6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_END_NOTE(),statusPop,6);
            }
        });
        temp_recheck1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_F_RENOTE(),statusPop,7);
            }
        });
        temp_recheck2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_1_RENOTE(),statusPop,8);
            }
        });
        temp_recheck3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_2_RENOTE(),statusPop,9);
            }
        });
        temp_recheck4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_3_RENOTE(),statusPop,10);
            }
        });
        temp_recheck5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_4_RENOTE(),statusPop,11);
            }
        });
        temp_recheck6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getKN_TEMP_END_RENOTE(),statusPop,12);
            }
        });
    }

    private void openNoteDialog(String note,boolean status,int pos) {

        Popup_Note fragement = (Popup_Note) getSupportFragmentManager().findFragmentByTag(Popup_Note.class.getSimpleName());
        if (fragement != null) fragement.dismiss();
        Popup_Note.newInstance(popupFragementListener,note,status,pos).show(getSupportFragmentManager(), Popup_Note.class.getSimpleName());

    }

    Popup_Note.PopupFragementListener popupFragementListener = new Popup_Note.PopupFragementListener() {

        @Override
        public void onSelected(String nt,int pos) {

            Popup_Note fragement = (Popup_Note) getSupportFragmentManager().findFragmentByTag(Popup_Note.class.getSimpleName());
            if (fragement != null) fragement.dismiss();
            if(pos==1)
            {
                btc_current.setKN_TEMP_F_NOTE(nt);
                if ( !btc_current.getKN_TEMP_F_NOTE().equals("")  ){
                    temp_note1.setBackgroundColor(Color.YELLOW);
                }
                else temp_note1.setBackgroundColor(Color.WHITE);
            }
            if(pos==2)
            {
                btc_current.setKN_TEMP_1_NOTE(nt);
                if ( !btc_current.getKN_TEMP_1_NOTE().equals("")  ){
                    temp_note2.setBackgroundColor(Color.YELLOW);
                }
                else temp_note2.setBackgroundColor(Color.WHITE);
            }
            if(pos==3)
            {
                btc_current.setKN_TEMP_2_NOTE(nt);
                if ( !btc_current.getKN_TEMP_2_NOTE().equals("")  ){
                    temp_note3.setBackgroundColor(Color.YELLOW);
                }
                else temp_note3.setBackgroundColor(Color.WHITE);
            }
            if(pos==4)
            {
                btc_current.setKN_TEMP_3_NOTE(nt);
                if ( !btc_current.getKN_TEMP_3_NOTE().equals("")  ){
                    temp_note4.setBackgroundColor(Color.YELLOW);
                }
                else temp_note4.setBackgroundColor(Color.WHITE);
            }
            if(pos==5)
            {
                btc_current.setKN_TEMP_4_NOTE(nt);
                if ( !btc_current.getKN_TEMP_4_NOTE().equals("")  ){
                    temp_note5.setBackgroundColor(Color.YELLOW);
                }
                else temp_note5.setBackgroundColor(Color.WHITE);
            }
            if(pos==6)
            {
                btc_current.setKN_TEMP_END_NOTE(nt);
                if ( !btc_current.getKN_TEMP_END_NOTE().equals("")  ){
                    temp_note6.setBackgroundColor(Color.YELLOW);
                }
                else temp_note6.setBackgroundColor(Color.WHITE);
            }

            if(pos==7)
            {
                btc_current.setKN_TEMP_F_RENOTE(nt);
                if ( !btc_current.getKN_TEMP_F_RENOTE().equals("")  ){
                    temp_recheck1.setBackgroundColor(Color.GREEN);
                }
                else temp_recheck1.setBackgroundColor(Color.WHITE);
            }
            if(pos==8)
            {
                btc_current.setKN_TEMP_1_RENOTE(nt);
                if ( !btc_current.getKN_TEMP_1_RENOTE().equals("")  ){
                    temp_recheck2.setBackgroundColor(Color.GREEN);
                }
                else temp_recheck2.setBackgroundColor(Color.WHITE);
            }
            if(pos==9)
            {
                btc_current.setKN_TEMP_2_RENOTE(nt);
                if ( !btc_current.getKN_TEMP_2_RENOTE().equals("")  ){
                    temp_recheck3.setBackgroundColor(Color.GREEN);
                }
                else temp_recheck3.setBackgroundColor(Color.WHITE);
            }
            if(pos==10)
            {
                btc_current.setKN_TEMP_3_RENOTE(nt);
                if ( !btc_current.getKN_TEMP_3_RENOTE().equals("")  ){
                    temp_recheck4.setBackgroundColor(Color.GREEN);
                }
                else temp_recheck4.setBackgroundColor(Color.WHITE);
            }
            if(pos==11)
            {
                btc_current.setKN_TEMP_4_RENOTE(nt);
                if ( !btc_current.getKN_TEMP_4_RENOTE().equals("")  ){
                    temp_recheck5.setBackgroundColor(Color.GREEN);
                }
                else temp_recheck5.setBackgroundColor(Color.WHITE);
            }
            if(pos==12)
            {
                btc_current.setKN_TEMP_END_RENOTE(nt);
                if ( !btc_current.getKN_TEMP_END_RENOTE().equals("")  ){
                    temp_recheck6.setBackgroundColor(Color.GREEN  );
                }
                else temp_recheck6.setBackgroundColor(Color.WHITE);
            }

        }

        @Override
        public void onCancel() {
            Popup_Note fragement = (Popup_Note) getSupportFragmentManager().findFragmentByTag(Popup_Note.class.getSimpleName());
            if (fragement != null) fragement.dismiss();
        }
    };




    public void btn_clicksettime() {

        btn_first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (positions==0){
                    try {
                        CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    edt_firsttime.setText(response.getString("curent_time").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    if (listBtcs.get(0).getKN_TIME_F()!=null){
                        if (mcs_name.contains("170FS")) {
                            edt_firsttime.setText(CountTime(listBtcs.get(0).getKN_TIME_F(),16*positions,0));
                        } else if (mcs_name.contains("150X4|XL4MB") || mcs_name.contains("150X4|Xl4MB")) {
                            edt_firsttime.setText(CountTime(listBtcs.get(0).getKN_TIME_F(),16*positions,0));
                        } else{
                            edt_firsttime.setText(CountTime(listBtcs.get(0).getKN_TIME_F(),12*positions,0));
                        }

                    }else if (listBtcs.get(positions-1).getKN_TIME_F()!=null){
                        if (mcs_name.contains("170FS")) {
                            edt_firsttime.setText(CountTime(listBtcs.get(positions-1).getKN_TIME_F(),16,0));
                        } else if (mcs_name.contains("150X4|XL4MB") || mcs_name.contains("150X4|Xl4MB")) {
                            edt_firsttime.setText(CountTime(listBtcs.get(positions-1).getKN_TIME_F(),16,0));
                        } else{
                            edt_firsttime.setText(CountTime(listBtcs.get(positions-1).getKN_TIME_F(),12,0));
                        }
                    }
                }
            }
        });



        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_firsttime.getText().toString().isEmpty()){
                    try {
                        CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    edt_time1.setText(response.getString("curent_time").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    if (mcs_name.contains("170FS")) {
                        edt_time1.setText(CountTime(edt_firsttime.getText().toString(),5,0));
                    } else if (mcs_name.contains("150X4|XL4MB") || mcs_name.contains("150X4|Xl4MB")) {
                        edt_time1.setText(CountTime(edt_firsttime.getText().toString(),6,30));
                    }
                    else if (mcs_name.contains("170FS|CR128-2") || mcs_name.contains("170FS|CR128-3")) {
                        edt_time1.setText(CountTime(edt_firsttime.getText().toString(),5,0));
                    }
                    else
                        edt_time1.setText(CountTime(edt_firsttime.getText().toString(),5,0));
                }
            }
        });
        btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_time1.getText().toString().isEmpty()){
                    try {
                        CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    edt_time2.setText(response.getString("curent_time").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    if (mcs_name.contains("170FS")) {
                        edt_time2.setText(CountTime(edt_time1.getText().toString(),3,0));
                    } else if (mcs_name.contains("150X4|XL4MB") || mcs_name.contains("150X4|Xl4MB")) {
                        edt_time2.setText(CountTime(edt_time1.getText().toString(),2,40));
                    }
                    else if (mcs_name.contains("170FS|CR128-2") || mcs_name.contains("170FS|CR128-3")) {
                        edt_time1.setText(CountTime(edt_firsttime.getText().toString(),2,0));
                    }
                    else
                        edt_time2.setText(CountTime(edt_time1.getText().toString(),2,0));
                }

            }
        });
        btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_time2.getText().toString().isEmpty()){
                    try {
                        CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    edt_time3.setText(response.getString("curent_time").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    if (mcs_name.contains("170FS")) {
                        edt_time3.setText(CountTime(edt_time2.getText().toString(),3,0));
                    } else if (mcs_name.contains("150X4|XL4MB") || mcs_name.contains("150X4|Xl4MB")) {
                        edt_time3.setText(CountTime(edt_time2.getText().toString(),2,40));
                    }else if (mcs_name.contains("170FS|CR128-2") || mcs_name.contains("170FS|CR128-3")) {
                        edt_time1.setText(CountTime(edt_firsttime.getText().toString(),1,30));
                    } else
                        edt_time3.setText(CountTime(edt_time2.getText().toString(),1,30));
                }
            }
        });
        btn_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_time3.getText().toString().isEmpty()){
                    try {
                        CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    edt_time4.setText(response.getString("curent_time").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{

                    if (mcs_name.contains("170FS")) {
                        edt_time4.setText(CountTime(edt_time3.getText().toString(),3,0));
                    } else if (mcs_name.contains("150X4|XL4MB") || mcs_name.contains("150X4|Xl4MB")) {
                        edt_time4.setText(CountTime(edt_time3.getText().toString(),2,40));
                    }else if (mcs_name.contains("170FS|CR128-2") || mcs_name.contains("170FS|CR128-3")) {
                        edt_time1.setText(CountTime(edt_firsttime.getText().toString(),1,30));
                    } else edt_time4.setText(CountTime(edt_time3.getText().toString(),1,30));

                }

            }
        });
        btn_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_time4.getText().toString().isEmpty()){
                    try {
                        CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    edt_endtime.setText(response.getString("curent_time").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    edt_endtime.setText(CountTime(edt_time4.getText().toString(),1,0));
                }

            }
        });
    }

    public void datePickerDialog(final EditText edt_date1) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(Kneader_Controller.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int monthcurrent=monthOfYear + 1;
                        String month="";
                        String day="";
                        if(dayOfMonth>9) {
                            day=String.valueOf(dayOfMonth);
                        }
                        else
                        {
                            day="0"+String.valueOf(dayOfMonth);
                        }
                        if(monthcurrent>9)
                        {
                            month=String.valueOf(monthcurrent);
                        }
                        else
                        {
                            month="0"+String.valueOf(monthcurrent);

                        }
                        edt_date1.setText(month + "/" + day + "/" + year);
                        //getMCS(edt_date1.getText().toString(), spn_shift.getSelectedItem().toString());
                        getMCS( spn_shift.getSelectedItem().toString(), edt_date1.getText().toString());

                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    public void setdefault(int position) {
        edt_firsttemp.setText(listBtcs.get(position).getKN_TEMP_F());
        edt_firsttime.setText(listBtcs.get(position).getKN_TIME_F());
        edt_temp1.setText(listBtcs.get(position).getKN_TEMP_1());
        edt_time1.setText(listBtcs.get(position).getKN_TIME_1());
        edt_temp2.setText(listBtcs.get(position).getKN_TEMP_2());
        edt_time2.setText(listBtcs.get(position).getKN_TIME_2());
        edt_temp3.setText(listBtcs.get(position).getKN_TEMP_3());
        edt_time3.setText(listBtcs.get(position).getKN_TIME_3());
        edt_temp4.setText(listBtcs.get(position).getKN_TEMP_4());
        edt_time4.setText(listBtcs.get(position).getKN_TIME_4());
        edt_endtemp.setText(listBtcs.get(position).getKN_TEMP_END());
        edt_endtime.setText(listBtcs.get(position).getKN_TIME_END());
        edt_note.setText(listBtcs.get(position).getKN_NOTE());

        edt_firststd_min.setText(listBtcs.get(position).getKN_FSTD()== null ? STD_Type(mcs_name,0,0) : getValueSTD(listBtcs.get(position).getKN_FSTD(),0));
        edt_temp1std_min.setText(listBtcs.get(position).getKN_1STD() == null ? STD_Type(mcs_name,1,0) : getValueSTD(listBtcs.get(position).getKN_1STD(),0));
        edt_temp2std_min.setText(listBtcs.get(position).getKN_2STD() == null ? STD_Type(mcs_name,2,0) : getValueSTD(listBtcs.get(position).getKN_2STD(),0));
        edt_temp3std_min.setText(listBtcs.get(position).getKN_3STD() == null ? STD_Type(mcs_name,3,0) : getValueSTD(listBtcs.get(position).getKN_3STD(),0));
        edt_temp4std_min.setText(listBtcs.get(position).getKN_4STD() == null ? STD_Type(mcs_name,4,0) : getValueSTD(listBtcs.get(position).getKN_4STD(),0));
        edt_endstd_min.setText(listBtcs.get(position).getKN_ESTD() == null ? STD_Type(mcs_name,5,0) : getValueSTD(listBtcs.get(position).getKN_ESTD(),0));

        edt_firststd_max.setText(listBtcs.get(position).getKN_FSTD() == null ? STD_Type(mcs_name,0,1) : getValueSTD(listBtcs.get(position).getKN_FSTD(),1));
        edt_temp1std_max.setText(listBtcs.get(position).getKN_1STD() == null ? STD_Type(mcs_name,1,1) : getValueSTD(listBtcs.get(position).getKN_1STD(),1));
        edt_temp2std_max.setText(listBtcs.get(position).getKN_2STD() == null ? STD_Type(mcs_name,2,1) : getValueSTD(listBtcs.get(position).getKN_2STD(),1));
        edt_temp3std_max.setText(listBtcs.get(position).getKN_3STD() == null ? STD_Type(mcs_name,3,1) : getValueSTD(listBtcs.get(position).getKN_3STD(),1));
        edt_temp4std_max.setText(listBtcs.get(position).getKN_4STD() == null ? STD_Type(mcs_name,4,1) : getValueSTD(listBtcs.get(position).getKN_4STD(),1));
        edt_endstd_max.setText(listBtcs.get(position).getKN_ESTD() == null ? STD_Type(mcs_name,5,1) : getValueSTD(listBtcs.get(position).getKN_ESTD(),1));
    }

    public int savekn(String btc_no) {
        int result = 0;
        boolean flag = true;
        if (check_standard(edt_firsttemp.getText().toString(),edt_firststd_min.getText().toString(),edt_firststd_max.getText().toString())) {
            ToastCustom.message(getApplicationContext(), "Nhiệt độ bỏ liệu vào phải nhập đúng theo tiêu chuẩn", Color.RED);
            flag = false;
        }
        else  if (check_standard(edt_temp1.getText().toString(),edt_temp1std_min.getText().toString(),edt_temp1std_max.getText().toString())) {
            ToastCustom.message(getApplicationContext(), "Nhiệt độ liệu lần thứ 1 phải nhập đúng theo tiêu chuẩn", Color.RED);
            flag = false;
        }
        else  if (check_standard(edt_temp2.getText().toString(),edt_temp2std_min.getText().toString(),edt_temp2std_max.getText().toString())) {
            ToastCustom.message(getApplicationContext(), "Nhiệt độ liệu lần thứ 2 phải nhập đúng theo tiêu chuẩn", Color.RED);
            flag = false;
        }
        else  if (check_standard(edt_temp3.getText().toString(),edt_temp3std_min.getText().toString(),edt_temp3std_max.getText().toString())) {
            ToastCustom.message(getApplicationContext(), "Nhiệt độ liệu lần thứ 3 phải nhập đúng theo tiêu chuẩn", Color.RED);
            flag = false;
        }
        else  if (check_standard(edt_temp4.getText().toString(),edt_temp4std_min.getText().toString(),edt_temp4std_max.getText().toString())) {
            ToastCustom.message(getApplicationContext(), "Nhiệt độ liệu lần thứ 4 phải nhập đúng theo tiêu chuẩn", Color.RED);
            flag = false;
        }
        else  if (check_standard(edt_endtemp.getText().toString(),edt_endstd_min.getText().toString(),edt_endstd_max.getText().toString())) {
            ToastCustom.message(getApplicationContext(), "Nhiệt độ đổ liệu phải nhập đúng theo tiêu chuẩn", Color.RED);
            flag = false;
        }

//        if (!edt_firsttime.getText().toString().isEmpty()){
//            if (edt_firsttime.getText().toString().length()>5 || edt_firsttime.getText().toString().length()<4){
//                ToastCustom.message(getApplicationContext(), "Thời gian bỏ liệu vào phải nhập đúng định dạng HH:mm!", Color.RED);
//                flag = false;
//            }
//        }
//        if (!edt_time1.getText().toString().isEmpty()){
//            if (edt_time1.getText().toString().length()>5 || edt_time1.getText().toString().length()<4){
//                ToastCustom.message(getApplicationContext(), "Thời gian lần 1 phải nhập đúng định dạng HH:mm!", Color.RED);
//                flag = false;
//            }
//        }
//        if (!edt_time2.getText().toString().isEmpty()){
//            if (edt_time2.getText().toString().length()>5 || edt_time2.getText().toString().length()<4){
//                ToastCustom.message(getApplicationContext(), "Thời gian lần 2 phải nhập đúng định dạng HH:mm!", Color.RED);
//                flag = false;
//            }
//        }
//        if (!edt_time3.getText().toString().isEmpty()){
//            if (edt_time3.getText().toString().length()>5 || edt_time3.getText().toString().length()<4){
//                ToastCustom.message(getApplicationContext(), "Thời gian lần 3 phải nhập đúng định dạng HH:mm!", Color.RED);
//                flag = false;
//            }
//        }
//        if (!edt_time4.getText().toString().isEmpty()){
//            if (edt_time4.getText().toString().length()>5 || edt_time4.getText().toString().length()<4){
//                ToastCustom.message(getApplicationContext(), "Thời gian lần 4 phải nhập đúng định dạng HH:mm!", Color.RED);
//                flag = false;
//            }
//        }
//        if (!edt_endtime.getText().toString().isEmpty()){
//            if (edt_endtime.getText().toString().length()>5 || edt_endtime.getText().toString().length()<4){
//                ToastCustom.message(getApplicationContext(), "Thời gian đổ liệu phải nhập đúng định dạng HH:mm!", Color.RED);
//                flag = false;
//            }
//        }

        if (flag == true) {
            btc = new BTC();
            btc.setBTC_NO(btc_no);
            btc.setKN_TEMP_F(edt_firsttemp.getText().toString());
            btc.setKN_TIME_F(edt_firsttime.getText().toString());
            btc.setKN_TEMP_1(edt_temp1.getText().toString());
            btc.setKN_TIME_1(edt_time1.getText().toString());
            btc.setKN_TEMP_2(edt_temp2.getText().toString());
            btc.setKN_TIME_2(edt_time2.getText().toString());
            btc.setKN_TEMP_3(edt_temp3.getText().toString());
            btc.setKN_TIME_3(edt_time3.getText().toString());
            btc.setKN_TEMP_4(edt_temp4.getText().toString());
            btc.setKN_TIME_4(edt_time4.getText().toString());
            btc.setKN_TEMP_END(edt_endtemp.getText().toString());
            btc.setKN_TIME_END(edt_endtime.getText().toString());
            btc.setKN_NOTE(edt_note.getText().toString());
            btc.setUP_USER(user);
            ///edit
            btc.setBTC_BATCH_NO(batch_no);
            btc.setBTC_HARD(hardness);
            btc.setBTC_COLOR(color);
            btc.setBTC_STYLE(model);

            btc.setKN_TEMP_F_NOTE(btc_current.getKN_TEMP_F_NOTE());
            btc.setKN_TEMP_1_NOTE(btc_current.getKN_TEMP_1_NOTE());
            btc.setKN_TEMP_2_NOTE(btc_current.getKN_TEMP_2_NOTE());
            btc.setKN_TEMP_3_NOTE(btc_current.getKN_TEMP_3_NOTE());
            btc.setKN_TEMP_4_NOTE(btc_current.getKN_TEMP_4_NOTE());
            btc.setKN_TEMP_END_NOTE(btc_current.getKN_TEMP_END_NOTE());
            btc.setKN_TEMP_F_RENOTE(btc_current.getKN_TEMP_F_RENOTE());
            btc.setKN_TEMP_1_RENOTE(btc_current.getKN_TEMP_1_RENOTE());
            btc.setKN_TEMP_2_RENOTE(btc_current.getKN_TEMP_2_RENOTE());
            btc.setKN_TEMP_3_RENOTE(btc_current.getKN_TEMP_3_RENOTE());
            btc.setKN_TEMP_4_RENOTE(btc_current.getKN_TEMP_4_RENOTE());
            btc.setKN_TEMP_END_RENOTE(btc_current.getKN_TEMP_END_RENOTE());
            btc.setAMT_NOTE(btc_current.getAMT_NOTE());
            btc.setKN_FSTD(edt_firststd_min.getText().toString() + "-" + edt_firststd_max.getText().toString());
            btc.setKN_1STD(edt_temp1std_min.getText().toString() + "-" + edt_temp1std_max.getText().toString());
            btc.setKN_2STD(edt_temp2std_min.getText().toString() + "-" + edt_temp2std_max.getText().toString());
            btc.setKN_3STD(edt_temp3std_min.getText().toString() + "-" + edt_temp3std_max.getText().toString());
            btc.setKN_4STD(edt_temp4std_min.getText().toString() + "-" + edt_temp4std_max.getText().toString());
            btc.setKN_ESTD(edt_endstd_min.getText().toString() + "-" + edt_endstd_max.getText().toString());


            try {
                CallApi.insertKneader(factory, btc, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listBtcs.set(positions, btc);
                        listKNAdapter.notifyDataSetChanged();
                        ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

            result = 1;
        }
        return result;
    }

    public boolean check_standard(String edt, String std_min, String std_max){
        if(!edt.equals("") && !std_min.equals("") && !std_max.equals("")){
            int min=0,max=0,value=0;
            min=Integer.parseInt(std_min);
            max = Integer.parseInt(std_max);
            value= Integer.parseInt(edt);

            if (value<min || value>max){
                return true;
            }
        }
        return false;
    }



    public void event_lv() {
        lv_knearder.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                long viewId = view.getId();
                if (viewId == R.id.rdo_check) {
                    btn_save.setEnabled(false);
                    btn_cancel.setEnabled(false);
                    btn_edit.setEnabled(true);
                    btn_next.setEnabled(true);
                    btn_mac.setEnabled(true);
                    setdefault(position);
                    btnDefault(false);
                    spn_shift.setEnabled(true);
                    spn_batchno.setEnabled(true);
                    edt_Date.setEnabled(true);

                    btc_no = listBtcs.get(position).getBTC_NO();
                    batch_no = listBtcs.get(position).getBTC_BATCH_NO();
                    model = listBtcs.get(position).getBTC_STYLE();
                    hardness = listBtcs.get(position).getBTC_HARD();
                    color = listBtcs.get(position).getBTC_COLOR();

                    positions=position;

                    btc_current.setBTC_BATCH_NO(listBtcs.get(position).getBTC_BATCH_NO());
                    btc_current.setBTC_NO(listBtcs.get(position).getBTC_NO());
                    btc_current.setKN_TEMP_F_NOTE(listBtcs.get(position).getKN_TEMP_F_NOTE());
                    btc_current.setKN_TEMP_1_NOTE(listBtcs.get(position).getKN_TEMP_1_NOTE());
                    btc_current.setKN_TEMP_2_NOTE(listBtcs.get(position).getKN_TEMP_2_NOTE());
                    btc_current.setKN_TEMP_3_NOTE(listBtcs.get(position).getKN_TEMP_3_NOTE());
                    btc_current.setKN_TEMP_4_NOTE(listBtcs.get(position).getKN_TEMP_4_NOTE());
                    btc_current.setKN_TEMP_END_NOTE(listBtcs.get(position).getKN_TEMP_END_NOTE());


                    btc_current.setKN_TEMP_F_RENOTE(listBtcs.get(position).getKN_TEMP_F_RENOTE());
                    btc_current.setKN_TEMP_1_RENOTE(listBtcs.get(position).getKN_TEMP_1_RENOTE());
                    btc_current.setKN_TEMP_2_RENOTE(listBtcs.get(position).getKN_TEMP_2_RENOTE());
                    btc_current.setKN_TEMP_3_RENOTE(listBtcs.get(position).getKN_TEMP_3_RENOTE());
                    btc_current.setKN_TEMP_4_RENOTE(listBtcs.get(position).getKN_TEMP_4_RENOTE());
                    btc_current.setKN_TEMP_END_RENOTE(listBtcs.get(position).getKN_TEMP_END_RENOTE());

                    btc_current.setKN_FSTD(listBtcs.get(position).getKN_FSTD());
                    btc_current.setKN_1STD(listBtcs.get(position).getKN_1STD());
                    btc_current.setKN_2STD(listBtcs.get(position).getKN_2STD());
                    btc_current.setKN_3STD(listBtcs.get(position).getKN_3STD());
                    btc_current.setKN_4STD(listBtcs.get(position).getKN_4STD());
                    btc_current.setKN_ESTD(listBtcs.get(position).getKN_ESTD());
                    btc_current.setAMT_NOTE(listBtcs.get(position).getAMT_NOTE());



                    if (listBtcs.get(position).getKN_TEMP_F_NOTE() != null) {
                        if (!listBtcs.get(position).getKN_TEMP_F_NOTE().equals("")) {
                            temp_note1.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note1.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_note1.setBackgroundColor(Color.WHITE);
                    }

                    if (listBtcs.get(position).getKN_TEMP_1_NOTE() != null) {
                        if (!listBtcs.get(position).getKN_TEMP_1_NOTE().equals("")) {
                            temp_note2.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note2.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_note2.setBackgroundColor(Color.WHITE);
                    }

                    if (listBtcs.get(position).getKN_TEMP_2_NOTE() != null) {
                        if (!listBtcs.get(position).getKN_TEMP_2_NOTE().equals("")) {
                            temp_note3.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note3.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_note3.setBackgroundColor(Color.WHITE);
                    }


                    if (listBtcs.get(position).getKN_TEMP_3_NOTE() != null) {
                        if (!listBtcs.get(position).getKN_TEMP_3_NOTE().equals("")) {
                            temp_note4.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note4.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_note4.setBackgroundColor(Color.WHITE);
                    }


                    if (listBtcs.get(position).getKN_TEMP_4_NOTE() != null) {

                        if (!listBtcs.get(position).getKN_TEMP_4_NOTE().equals("")) {
                            temp_note5.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note5.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_note5.setBackgroundColor(Color.WHITE);
                    }

                    if (listBtcs.get(position).getKN_TEMP_END_NOTE() != null) {
                        if (!listBtcs.get(position).getKN_TEMP_END_NOTE().equals("")) {
                            temp_note6.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note6.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_note6.setBackgroundColor(Color.WHITE);
                    }

                    //recheck
                    if (listBtcs.get(position).getKN_TEMP_F_RENOTE() != null) {
                        if (!listBtcs.get(position).getKN_TEMP_F_RENOTE().equals("")) {
                            temp_recheck1.setBackgroundColor(Color.GREEN);
                        } else {
                            temp_recheck1.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_recheck1.setBackgroundColor(Color.WHITE);
                    }

                    if (listBtcs.get(position).getKN_TEMP_1_RENOTE() != null) {
                        if (!listBtcs.get(position).getKN_TEMP_1_RENOTE().equals("")) {
                            temp_recheck2.setBackgroundColor(Color.GREEN);
                        } else {
                            temp_recheck2.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_recheck2.setBackgroundColor(Color.WHITE);
                    }

                    if (listBtcs.get(position).getKN_TEMP_2_RENOTE() != null) {
                        if (!listBtcs.get(position).getKN_TEMP_2_RENOTE().equals("")) {
                            temp_recheck3.setBackgroundColor(Color.GREEN);
                        } else {
                            temp_recheck3.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_recheck3.setBackgroundColor(Color.WHITE);
                    }

                    if (listBtcs.get(position).getKN_TEMP_3_RENOTE() != null) {
                        if (!listBtcs.get(position).getKN_TEMP_3_RENOTE().equals("")) {
                            temp_recheck4.setBackgroundColor(Color.GREEN);
                        } else {
                            temp_recheck4.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_recheck4.setBackgroundColor(Color.WHITE);
                    }

                    if (listBtcs.get(position).getKN_TEMP_4_RENOTE() != null) {

                        if (!listBtcs.get(position).getKN_TEMP_4_RENOTE().equals("")) {
                            temp_recheck5.setBackgroundColor(Color.GREEN);
                        } else {
                            temp_recheck5.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_recheck5.setBackgroundColor(Color.WHITE);
                    }

                    if (listBtcs.get(position).getKN_TEMP_END_RENOTE() != null) {
                        if (!listBtcs.get(position).getKN_TEMP_END_RENOTE().equals("")) {
                            temp_recheck6.setBackgroundColor(Color.GREEN);
                        } else {
                            temp_recheck6.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_recheck6.setBackgroundColor(Color.WHITE);
                    }
                }

            }
        });
    }

    public void getBTC(String no, String mcs_date, String mcs_shift) {
        try {
            CallApi.getbtc(factory, no, mcs_date, mcs_shift, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    listBtcs = new ArrayList<>();
                    BtcsArm btcsArm = gson.fromJson(response.toString(), BtcsArm.class);
                    listBtcs = btcsArm.getItems() == null ? new ArrayList<BTC>() : btcsArm.getItems();
                    listKNAdapter = new ListKNAdapter(getApplicationContext(), R.layout.list_kneader_adapter, listBtcs);
                    lv_knearder.setAdapter(listKNAdapter);
                    if(lv_knearder.getCount()<=0)
                    {
                        cleardata();
                        btn_edit.setEnabled(false);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getBTC2(String mcs_no, String mcs_shift, String mcs_date) {
        try {
            CallApi.getbtc2(factory, mcs_no, mcs_shift, mcs_date, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    listBtcs = new ArrayList<>();
                    BtcsArm btcsArm = gson.fromJson(response.toString(), BtcsArm.class);
                    listBtcs = btcsArm.getItems() == null ? new ArrayList<BTC>() : btcsArm.getItems();
                    listKNAdapter = new ListKNAdapter(getApplicationContext(), R.layout.list_kneader_adapter, listBtcs);
                    lv_knearder.setAdapter(listKNAdapter);
                    if(lv_knearder.getCount()<=0)
                    {
                        cleardata();
                        btn_edit.setEnabled(false);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void btnDefault(boolean ena) {
        edt_firsttime.setEnabled(ena);
        edt_time1.setEnabled(ena);
        edt_time2.setEnabled(ena);
        edt_time3.setEnabled(ena);
        edt_time4.setEnabled(ena);
        edt_endtime.setEnabled(ena);
        btn_first.setEnabled(ena);
        btn_1.setEnabled(ena);
        btn_2.setEnabled(ena);
        btn_3.setEnabled(ena);
        btn_4.setEnabled(ena);
        btn_end.setEnabled(ena);
        edt_firsttemp.setEnabled(ena);
        edt_temp1.setEnabled(ena);
        edt_temp2.setEnabled(ena);
        edt_temp3.setEnabled(ena);
        edt_temp4.setEnabled(ena);
        edt_endtemp.setEnabled(ena);
        edt_note.setEnabled(ena);
        edt_firststd_min.setEnabled(ena);
        edt_temp1std_min.setEnabled(ena);;
        edt_temp2std_min.setEnabled(ena);
        edt_temp3std_min.setEnabled(ena);
        edt_temp4std_min.setEnabled(ena);
        edt_endstd_min.setEnabled(ena);

        edt_firststd_max.setEnabled(ena);
        edt_temp1std_max.setEnabled(ena);;
        edt_temp2std_max.setEnabled(ena);
        edt_temp3std_max.setEnabled(ena);
        edt_temp4std_max.setEnabled(ena);
        edt_endstd_max.setEnabled(ena);
    }

    public void getMCS(String mcs_shift, String mcs_date){
        // public void getMCS(String mcs_date, String mcs_shift) {
        try {
            // CallApi.getmcs(factory, mcs_date, mcs_shift, new Response.Listener<JSONObject>()
            CallApi.getGroupID2(factory, mcs_shift, mcs_date, new Response.Listener<JSONObject>()
//            CallApi.getGroupIDstep3(factory, mcs_shift, mcs_date, new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response) {
                    McsArm mcsArm = gson.fromJson(response.toString(), McsArm.class);
                    mcsl = mcsArm.getItems() == null ? new ArrayList<QAM_INS>() : mcsArm.getItems();
                    arrMCS = new ArrayList<>();
                    arrGroupID = new ArrayList<>();
                    for (QAM_INS df : mcsl) {
                        arrMCS.add(df);
                        arrGroupID.add(df.getINS_NO());
                    }
                    ArrayAdapter<QAM_INS> adapter =
                            new ArrayAdapter<QAM_INS>(getApplicationContext(), R.layout.spinner_item, arrMCS);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spn_batchno.setAdapter(adapter);
                    if (spn_batchno.getCount() <= 0) {
                        listBtcs = new ArrayList<BTC>();
                        listKNAdapter = new ListKNAdapter(getApplicationContext(), R.layout.list_kneader_adapter, listBtcs);
                        lv_knearder.setAdapter(listKNAdapter);
                        cleardata();
                        btn_edit.setEnabled(false);
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void cleardata()
    {
        edt_endtemp.setText("");
        edt_endtime.setText("");
        edt_firsttemp.setText("");
        edt_firsttime.setText("");
        edt_note.setText("");
        edt_temp1.setText("");
        edt_temp2.setText("");
        edt_temp3.setText("");
        edt_temp4.setText("");
        edt_time1.setText("");
        edt_time2.setText("");
        edt_time3.setText("");
        edt_time4.setText("");

        edt_firststd_min.setText("");
        edt_temp1std_min.setText("");
        edt_temp2std_min.setText("");
        edt_temp3std_min.setText("");
        edt_temp4std_min.setText("");
        edt_endstd_min.setText("");

        edt_firststd_max.setText("");
        edt_temp1std_max.setText("");
        edt_temp2std_max.setText("");
        edt_temp3std_max.setText("");
        edt_temp4std_max.setText("");
        edt_endstd_max.setText("");

    }

    public class BtcsArm extends ApiReturnModel<ArrayList<BTC>> {
    }

    public class McsArm extends ApiReturnModel<ArrayList<QAM_INS>> {
    }

}
