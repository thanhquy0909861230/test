package com.deanshoes.AppQAM.step3;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.deanshoes.AppQAM.R;

public class Popup_Note  extends DialogFragment {


    private PopupFragementListener mListener ;
    TextView mtxtNote;
    Button mbtnCancel,mbtnSave;
    private static String mNote="";
    private static boolean mStatus=false;
    private static int posis=0;

    public static Popup_Note newInstance(PopupFragementListener listener, String note, boolean status, int pos) {

        Popup_Note fragement = new Popup_Note() ;
        fragement.mListener = listener ;
        Bundle args = new Bundle();
        posis=pos;
        mStatus=status;
        mNote=note;
        fragement.setArguments(args);

        return fragement ;
    }

    public interface PopupFragementListener {

        void onSelected(String nt, int pos) ;
        void onCancel() ;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.popup_window, container, false);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        setCancelable(false);
        mtxtNote= view.findViewById(R.id.txt_note);
        mbtnSave= view.findViewById(R.id.btn_save);
        mbtnCancel= view.findViewById(R.id.btn_cancel);

        return view ;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

            if(mStatus==true)
            {
                mtxtNote.setEnabled(false);
                mbtnSave.setVisibility(View.GONE);
                mtxtNote.setFocusable(false);
            }
            else
            {
                mtxtNote.setEnabled(true);
                mbtnSave.setVisibility(View.VISIBLE);
                mtxtNote.setFocusable(true);
            }
            mtxtNote.setText(mNote);
        bindEvent();

    }
    private void bindEvent() {
        mbtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               mListener.onSelected(mtxtNote.getText().toString(),posis);
            }
        });
        mbtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( mListener != null ) {
                    mListener.onCancel();
                }
            }
        });
    }

}
