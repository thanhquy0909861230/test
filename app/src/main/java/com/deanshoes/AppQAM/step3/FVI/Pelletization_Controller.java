package com.deanshoes.AppQAM.step3.FVI;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.ListPELLAdapter;
import com.deanshoes.AppQAM.model.QAM.BTC_PELL;
import com.deanshoes.AppQAM.model.QAM.MCS;
import com.deanshoes.AppQAM.model.QAM.QAM_INS;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.deanshoes.AppQAM.step3.Popup_Note;
import com.deanshoes.AppQAM.step4.pdfActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class Pelletization_Controller extends AppCompatActivity {
    private Gson gson = new GsonBuilder().setDateFormat("MM/dd/yyyy HH:mm:ss").create();
    Spinner spn_shift, spn_batchno;
    String arrShift[] = {"1", "2", "3"};
    ArrayAdapter<String> adtListShift;
    DatePickerDialog datePickerDialog;

    String factory = "";
    EditText edt_Date,edt_temp1,edt_temp2,edt_temp3,edt_timecreate,edt_timecombine,edt_newbno,edt_dumptemp,edt_clean_time,edt_timeout,edt_tempfirst,edt_newbno0,edt_newbno1,edt_temp5;
    private List<QAM_INS> mcsl;
    ArrayList<QAM_INS> arrMCS;
    ArrayList<String> arrGroupID;
    ListView lv_pelletization;
    BTC_PELL btc_pell;
    String btc_no = "",mcs="", name_mcs="";
    String str = "",batch_no="",model="",color="",user ="",time_kn;
    String ins_no11;

    private ArrayList<BTC_PELL> listBtc_pells;
    ListPELLAdapter listPELLAdapter;
    Button btn_edit, btn_save, btn_cancel,btn_timecreate,btn_timecombine,btn_timeclean,btn_timeout,btn_kneader,btn_openmill,btn_step2,btn_step4;
    int positions=-1;
    boolean flag1=true;
    boolean flagsave=false, isExsist = false;
    ImageView temp_note1,temp_note2,temp_note3,temp_note4,temp_note5,temp_note6;
    private static boolean statusPop;
    BTC_PELL btc_current;
    ImageButton ib_logout;
    CheckBox chk_last;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelletization);

        Intent intent=getIntent();
        final Bundle bundle=intent.getExtras();
        if (bundle!=null){
            user=bundle.getString("user","ADMIN");
            factory=bundle.getString("fac");
        }
        btn_step2= findViewById(R.id.btn_step2);
        btn_step4= findViewById(R.id.btn_step4);
        spn_shift = findViewById(R.id.spn_shift);
        adtListShift = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrShift);
        spn_shift.setAdapter(adtListShift);
        lv_pelletization= findViewById(R.id.lv_pelletization);
        edt_Date = findViewById(R.id.edt_date);
        spn_batchno = findViewById(R.id.spn_batchno);
        chk_last = findViewById(R.id.chk_last);

        btn_edit= findViewById(R.id.btn_edit);
        btn_save= findViewById(R.id.btn_save);
        btn_cancel= findViewById(R.id.btn_cancel);
        btn_timecreate= findViewById(R.id.btn_timecreate);
        btn_timecombine= findViewById(R.id.btn_timecombine);
        btn_timeclean= findViewById(R.id.btn_timeclean);
        btn_timeout= findViewById(R.id.btn_timeout);
        btn_kneader= findViewById(R.id.btn_kneader);
        btn_openmill= findViewById(R.id.btn_openmill);

        edt_temp1= findViewById(R.id.edt_temp1);
        edt_temp2= findViewById(R.id.edt_temp2);
        edt_temp3= findViewById(R.id.edt_temp3);
        edt_temp5= findViewById(R.id.edt_temp5);
        edt_tempfirst= findViewById(R.id.edt_tempfirst);

        edt_timecreate= findViewById(R.id.edt_timecreate);
        edt_timecombine= findViewById(R.id.edt_timecombine);
        edt_newbno= findViewById(R.id.edt_newbno);
        edt_newbno0= findViewById(R.id.edt_newbno0);
        edt_newbno1= findViewById(R.id.edt_newbno1);
        edt_dumptemp= findViewById(R.id.edt_dumptemp);
        edt_clean_time= findViewById(R.id.edt_clean_time);
        edt_timeout= findViewById(R.id.edt_timeout);

        temp_note1 = findViewById(R.id.temp_note1);
        temp_note2 = findViewById(R.id.temp_note2);
        temp_note3 = findViewById(R.id.temp_note3);
        temp_note4 = findViewById(R.id.temp_note4);
        temp_note5 = findViewById(R.id.temp_note5);
        temp_note6 = findViewById(R.id.temp_note6);
        ib_logout = findViewById(R.id.ib_logout);

        temp_note1.setVisibility(View.VISIBLE);
        temp_note2.setVisibility(View.VISIBLE);
        temp_note3.setVisibility(View.VISIBLE);
        temp_note4.setVisibility(View.VISIBLE);
        temp_note5.setVisibility(View.VISIBLE);
        temp_note6.setVisibility(View.VISIBLE);

        btc_current=new BTC_PELL();

        getDate();

        btn_kneader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Pelletization_Controller.this, Kneader_Controller.class);
                myIntent.putExtras(bundle);
                Pelletization_Controller.this.startActivity(myIntent);

            }
        });
        btn_openmill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Pelletization_Controller.this, OpenMill_Controller.class);
                myIntent.putExtras(bundle);
                Pelletization_Controller.this.startActivity(myIntent);
            }
        });
        btn_step2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Pelletization_Controller.this, Mac1Activity_FVI.class);
                myIntent.putExtras(bundle);
                Pelletization_Controller.this.startActivity(myIntent);
            }
        });
        btn_step4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Pelletization_Controller.this, pdfActivity.class);
                myIntent.putExtras(bundle);
                Pelletization_Controller.this.startActivity(myIntent);
            }
        });

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_save.setEnabled(true);
                btn_cancel.setEnabled(true);
                btn_edit.setEnabled(false);
                btnDefault(true);
                spn_shift.setEnabled(false);
                spn_batchno.setEnabled(false);
                edt_Date.setEnabled(false);
                btn_kneader.setEnabled(false);
                btn_openmill.setEnabled(false);
                if (isExsist == true){
                    chk_last.setEnabled(false);
                }else{
                    chk_last.setEnabled(true);
                }
                int sizelist= listBtc_pells.size();
                String newval="";
                for (int i=0;i<sizelist;i++)
                {
                    if(listBtc_pells.get(i).getNEW_BATCH_NO()!=null)
                    newval+= listBtc_pells.get(i).getNEW_BATCH_NO();
                }
                if (!newval.isEmpty() || newval!="" || !newval.equals(""))
                {
                    edt_newbno0.setEnabled(false);
                    edt_newbno1.setEnabled(false);
                    edt_newbno.setEnabled(false);
                }
                listPELLAdapter.disableitem(true);
                statusPop=false;

            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveop(btc_no) == 1) {
                    btnDefault(false);
                    btn_save.setEnabled(false);
                    btn_cancel.setEnabled(false);
                    spn_shift.setEnabled(true);
                    spn_batchno.setEnabled(true);
                    edt_Date.setEnabled(true);
                    btn_kneader.setEnabled(true);
                    btn_openmill.setEnabled(true);
                    edt_newbno0.setVisibility(View.GONE);
                    edt_newbno1.setVisibility(View.GONE);
                    edt_newbno.setVisibility(View.VISIBLE);
                    edt_newbno0.setEnabled(false);
                    edt_newbno1.setEnabled(false);
                    edt_newbno.setEnabled(false);
                    listPELLAdapter.disableitem(false);
                    chk_last.setEnabled(false);
                    statusPop=true;

                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDefault(false);
                btn_save.setEnabled(false);
                btn_cancel.setEnabled(false);
                btn_edit.setEnabled(true);
                spn_shift.setEnabled(true);
                spn_batchno.setEnabled(true);
                edt_Date.setEnabled(true);
                btn_kneader.setEnabled(true);
                btn_openmill.setEnabled(true);
                edt_newbno0.setVisibility(View.GONE);
                edt_newbno1.setVisibility(View.GONE);
                edt_newbno.setVisibility(View.VISIBLE);
                listPELLAdapter.disableitem(false);
                chk_last.setChecked(false);
                chk_last.setEnabled(false);
                cleardata();
                statusPop=true;
            }
        });

        chk_last.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    edt_newbno0.setVisibility(View.VISIBLE);
                    edt_newbno1.setVisibility(View.VISIBLE);
                    edt_newbno.setVisibility(View.GONE);
                }else{
                    edt_newbno0.setVisibility(View.GONE);
                    edt_newbno1.setVisibility(View.GONE);
                    edt_newbno.setVisibility(View.VISIBLE);
                }
            }
        });


        edt_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog(edt_Date);
            }
        });
        spn_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String item = parentView.getItemAtPosition(position).toString();
                try {
                    if (edt_Date.getText().toString().isEmpty()) {
                    } else {
                      //  getMCS(item,edt_Date.getText().toString());
                        getMCS2(item,edt_Date.getText().toString());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spn_batchno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String item = spn_batchno.getSelectedItem().toString();
                String[] ins_no1 = spn_batchno.getSelectedItem().toString().split("_");
                ins_no11 = ins_no1[0];
                for (int i=0;i<arrGroupID.size();i++){
                    if (item.equals(arrMCS.get(i).getINS_NO()) && position==i){
                        getInfor(arrMCS.get(i).getMCS_NO());
                    }
                }
                try {
                  //  getBTC_PELL(parentView.getItemAtPosition(position).toString(), edt_Date.getText().toString(), spn_shift.getSelectedItem().toString());
                    getBTC_PELL(ins_no11, edt_Date.getText().toString(), spn_shift.getSelectedItem().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Pelletization_Controller.this, LoginActivity.class);
                startActivity(i);
            }
        });
        event_lv();
        btn_clicksettime();
        bindevent();
        statusPop=true;
    }
    public void bindevent()
    {
        temp_note1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getPELL_OUT_TEMP_NOTE(),statusPop,1);
            }
        });
        temp_note2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getPELL_TEMP_1_NOTE(),statusPop,2);
            }
        });
        temp_note3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getPELL_TEMP_2_NOTE(),statusPop,3);
            }
        });
        temp_note4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getPELL_TEMP_3_NOTE(),statusPop,4);
            }
        });
        temp_note5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getPELL_TEMP_HEAD_NOTE(),statusPop,5);
            }
        });
        temp_note6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getPELL_TEMP_6_NOTE(),statusPop,6);
            }
        });
    }

    public void getInfor(String mcs_no){
        try {
            CallApi.getInforMcs(factory, mcs_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Mac1Activity_FVI.McsArm mcsArm = gson.fromJson(response.toString(), Mac1Activity_FVI.McsArm.class);
                    ArrayList<MCS> mcsinfor = mcsArm.getItems() == null ? new ArrayList<MCS>() : mcsArm.getItems();
                    if(mcsinfor.size()!=0) {
                        String str = mcsinfor.get(0).getMSC();
                        name_mcs=str;
                        if (str.indexOf("CR")!=-1){
                            mcs = str.substring(str.indexOf("CR")+2);
                        }else{
                            if (str.indexOf("|")!=-1){
                                mcs=str.substring(str.indexOf("|")+1);
                            }else mcs=str;
                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void openNoteDialog(String note,boolean status,int pos) {

        Popup_Note fragement = (Popup_Note) getSupportFragmentManager().findFragmentByTag(Popup_Note.class.getSimpleName());
        if (fragement != null) fragement.dismiss();
        Popup_Note.newInstance(popupFragementListener,note,status,pos).show(getSupportFragmentManager(), Popup_Note.class.getSimpleName());

    }

    Popup_Note.PopupFragementListener popupFragementListener = new Popup_Note.PopupFragementListener() {

        @Override
        public void onSelected(String nt,int pos) {

            Popup_Note fragement = (Popup_Note) getSupportFragmentManager().findFragmentByTag(Popup_Note.class.getSimpleName());
            if (fragement != null) fragement.dismiss();
            if(pos==1)
            {
                btc_current.setPELL_OUT_TEMP_NOTE(nt);
                if ( !btc_current.getPELL_OUT_TEMP_NOTE().equals("")  ){
                    temp_note1.setBackgroundColor(Color.YELLOW);
                }
                else temp_note1.setBackgroundColor(Color.WHITE);
            }
            if(pos==2)
            {
                btc_current.setPELL_TEMP_1_NOTE(nt);
                if ( !btc_current.getPELL_TEMP_1_NOTE().equals("")  ){
                    temp_note2.setBackgroundColor(Color.YELLOW);
                }
                else temp_note2.setBackgroundColor(Color.parseColor("#e8eaf6"));

            }
            if(pos==3)
            {
                btc_current.setPELL_TEMP_2_NOTE(nt);
                if ( !btc_current.getPELL_TEMP_2_NOTE().equals("")  ){
                    temp_note3.setBackgroundColor(Color.YELLOW);
                }
                else temp_note3.setBackgroundColor(Color.parseColor("#e8eaf6"));

            }
            if(pos==4)
            {
                btc_current.setPELL_TEMP_3_NOTE(nt);
                if ( !btc_current.getPELL_TEMP_3_NOTE().equals("")  ){
                    temp_note4.setBackgroundColor(Color.YELLOW);
                }
                else temp_note4.setBackgroundColor(Color.parseColor("#e8eaf6"));

            }
            if(pos==5)
            {
                btc_current.setPELL_TEMP_HEAD_NOTE(nt);
                if ( !btc_current.getPELL_TEMP_HEAD_NOTE().equals("")  ){
                    temp_note5.setBackgroundColor(Color.YELLOW);
                }
                else temp_note5.setBackgroundColor(Color.parseColor("#e8eaf6"));

            }

            if(pos==6)
            {
                btc_current.setPELL_TEMP_6_NOTE(nt);
                if ( !btc_current.getPELL_TEMP_6_NOTE().equals("")  ){
                    temp_note6.setBackgroundColor(Color.YELLOW);
                }
                else temp_note6.setBackgroundColor(Color.parseColor("#e8eaf6"));

            }


        }

        @Override
        public void onCancel() {
            Popup_Note fragement = (Popup_Note) getSupportFragmentManager().findFragmentByTag(Popup_Note.class.getSimpleName());
            if (fragement != null) fragement.dismiss();
        }
    };


    public void btn_clicksettime() {

        edt_newbno0.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (!edt_newbno0.getText().toString().isEmpty()) {
                    String value2 = getDate() + mcs;
                    edt_newbno1.setText(value2);
                    System.out.println("1:"+value2);
                    flag1 = true;
                }
            }
        });

        btn_timecreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           //     getTimeKneader(listBtc_pells.get(positions).getBTC_NO(),spn_batchno.getSelectedItem().toString());
                getTimeKneader(listBtc_pells.get(positions).getBTC_NO(),ins_no11);
            }
        });

        btn_timecombine.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_timecreate.getText().toString().isEmpty()){
                    try {
                        CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    edt_timecombine.setText(response.getString("curent_time").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    edt_timecombine.setText(CountTime(edt_timecreate.getText().toString(),20,0));
                }

            }
        });
        btn_timeout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_timecombine.getText().toString().isEmpty()){
                    try {
                        CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    edt_timeout.setText(response.getString("curent_time").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    edt_timeout.setText(CountTime(edt_timecombine.getText().toString(),30, 0));
                }

            }
        });

        btn_timeclean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (positions==0){
                    if (edt_timecreate.getText().toString().isEmpty()){
                        try {
                            CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        edt_clean_time.setText(response.getString("curent_time").toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else{
                        edt_clean_time.setText(CleanTime(edt_timecreate.getText().toString(),10, 0));
                    }
                }else{
                    try {
                        CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    edt_clean_time.setText(response.getString("curent_time").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

    }

    public String getDate(){
        Date date = new Date();
        Calendar ca = Calendar.getInstance();
        Calendar cal = Calendar.getInstance();
        ca.setTimeZone(TimeZone.getTimeZone("ICT"));
        cal.setTimeZone(TimeZone.getTimeZone("ICT"));
        ca.setTime(date);
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        SimpleDateFormat isoFormat = new SimpleDateFormat("HHmm");
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyy");

        System.out.println("Time zone:"+ca.getTimeZone().getDisplayName());
        int timec = Integer.parseInt(isoFormat.format(new Date()));
        if(timec>=600 && timec<=2359){
            System.out.println("Time:" + isoFormat.format(ca.getTime()));
            System.out.println("Date:"+dateFormat.format(ca.getTime()));
            return dateFormat.format(ca.getTime());
        }else return dateFormat.format(cal.getTime());
    }

    public int saveop(String btc_no) {
        int result = 0;
        boolean flag=true;
        if (!edt_temp1.getText().toString().isEmpty()) {
            if (Integer.parseInt(edt_temp1.getText().toString()) < 65 || Integer.parseInt(edt_temp1.getText().toString()) > 75) {
                ToastCustom.message(getApplicationContext(), "Nhiệt độ đoạn 1 phải trong khoảng từ 65 đên 75", Color.RED);
                flag = false;
            }
        }
        else if (!edt_temp2.getText().toString().isEmpty()) {
            if (Integer.parseInt(edt_temp2.getText().toString()) < 65 || Integer.parseInt(edt_temp2.getText().toString()) > 75) {
                ToastCustom.message(getApplicationContext(), "Nhiệt độ đoạn 2 phải trong khoảng từ 65 đên 75", Color.RED);
                flag = false;
            }
        }
        else if (!edt_temp3.getText().toString().isEmpty()) {
            if (Integer.parseInt(edt_temp3.getText().toString()) < 65 || Integer.parseInt(edt_temp3.getText().toString()) > 75) {
                ToastCustom.message(getApplicationContext(), "Nhiệt độ đoạn 3 phải trong khoảng từ 65 đên 75", Color.RED);
                flag = false;
            }
        }
        else if (!edt_temp5.getText().toString().isEmpty()) {
            if (Integer.parseInt(edt_temp5.getText().toString()) < 65 || Integer.parseInt(edt_temp5.getText().toString()) > 75) {
                ToastCustom.message(getApplicationContext(), "Nhiệt độ chuyền E phải trong khoảng từ 65 đên 75", Color.RED);
                flag = false;
            }
        }
        else if (!edt_tempfirst.getText().toString().isEmpty()) {
            if (Integer.parseInt(edt_tempfirst.getText().toString()) < 80 || Integer.parseInt(edt_tempfirst.getText().toString()) > 90) {
                ToastCustom.message(getApplicationContext(), "Nhiệt đầu khuôn phải trong khoảng từ 80 đên 90", Color.RED);
                flag = false;
            }
        }
        else if (chk_last.isChecked()){
            if (!edt_newbno0.getText().toString().isEmpty()) {
                if (edt_newbno0.getText().length() != 3) {
                    ToastCustom.message(getApplicationContext(), "Mã số đợt mới bắt buộc là 3 chữ số", Color.RED);
                    flag = false;
                }
            }
            else
            {
                if(edt_newbno0.isEnabled()) {
                    ToastCustom.message(getApplicationContext(), "Vui lòng nhập mã số đợt mới", Color.RED);
                    flag = false;
                }
            }
        }

        if (flag == true && flag1==true) {
            btc_pell = new BTC_PELL();
            btc_pell.setBTC_NO(btc_no);
            btc_pell.setPELL_TEMP_1(edt_temp1.getText().toString());
            btc_pell.setPELL_TEMP_2(edt_temp2.getText().toString());
            btc_pell.setPELL_TEMP_3(edt_temp3.getText().toString());
            btc_pell.setPELL_TEMP_5(edt_temp5.getText().toString());
            btc_pell.setPELL_TEMP_HEAD(edt_tempfirst.getText().toString());
            if (chk_last.isChecked()){
                if (factory.equals("FVI")){
                    String newb = edt_newbno1.getText().toString().substring(0,6)+edt_newbno0.getText().toString()
                            +edt_newbno1.getText().toString().substring(6);
                    edt_newbno.setText(newb);
                    btc_pell.setNEW_BATCH_NO(newb);
                    btc_pell.setPELL_CLEAN_TIME(getCleanTime(listBtc_pells.get(0).getPELL_GRAN(),listBtc_pells.get(0).getPELL_CLEAN_TIME()));
                }else if (factory.equals("FVI_II_305")){
                    String newb = edt_newbno0.getText().toString() +edt_newbno1.getText().toString();
                    edt_newbno.setText(newb);
                    btc_pell.setNEW_BATCH_NO(newb);
                }

            }else{
                btc_pell.setPELL_CLEAN_TIME(edt_clean_time.getText().toString());
                if (isExsist==true){
                    btc_pell.setNEW_BATCH_NO(edt_newbno1.getText().toString());
                }
            }
            btc_pell.setPELL_MIXING_TIME(edt_timecombine.getText().toString());
            btc_pell.setPELL_OUT_TEMP(edt_dumptemp.getText().toString());
            btc_pell.setPELL_OUT_TIME(edt_timeout.getText().toString());

            ///edit
            btc_pell.setBTC_BATCH_NO(batch_no);
            btc_pell.setBTC_COLOR(color);
            btc_pell.setBTC_STYLE(model);
            btc_pell.setPELL_GRAN(edt_timecreate.getText().toString());
            btc_pell.setUP_USER(user);

            btc_pell.setPELL_OUT_TEMP_NOTE(btc_current.getPELL_OUT_TEMP_NOTE());
            btc_pell.setPELL_TEMP_1_NOTE(btc_current.getPELL_TEMP_1_NOTE());
            btc_pell.setPELL_TEMP_2_NOTE(btc_current.getPELL_TEMP_2_NOTE());
            btc_pell.setPELL_TEMP_3_NOTE(btc_current.getPELL_TEMP_3_NOTE());
            btc_pell.setPELL_TEMP_HEAD_NOTE(btc_current.getPELL_TEMP_HEAD_NOTE());
            btc_pell.setPELL_TEMP_6_NOTE(btc_current.getPELL_TEMP_6_NOTE());
            btc_pell.setAMT_NOTE(btc_current.getAMT_NOTE());

            if (chk_last.isChecked()) {
                    try {
                        CallApi.insertNEW_BATCH(factory, btc_pell, ins_no11, edt_Date.getText().toString(), spn_shift.getSelectedItem().toString(), new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                getBTC_PELL(ins_no11, edt_Date.getText().toString(), spn_shift.getSelectedItem().toString());
                                listPELLAdapter.notifyDataSetChanged();
                                ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                                chk_last.setChecked(false);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                            }
                        });
                        result = 1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        result = 0;
                    }
                btn_edit.setEnabled(false);
                    chk_last.setChecked(false);
            }

            else{
                try {
                    CallApi.insertPellezation_NEW(factory, btc_pell, spn_batchno.getSelectedItem().toString(), edt_Date.getText().toString(), spn_shift.getSelectedItem().toString(), new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            listBtc_pells.set(positions, btc_pell);
                            listPELLAdapter.notifyDataSetChanged();
                            ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    result = 1;
                } catch (JSONException e) {
                    e.printStackTrace();
                    result = 0;
                }
                 btn_edit.setEnabled(true);
            }
        }
//        }
        return result;
    }

    public String getCleanTime(String create, String clean){
        str = "";
        if (clean==null || clean.equals("")){
            if (create!=null && !create.equals("")){
                str =CleanTime(create,10, 0);
            }else{
                try {
                    CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                str = response.getString("curent_time");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }else str = clean;
        return str;
    }

    public String CountTime(String Itime, int Imin, int Isec){
        String hour="", min="", sec="";
        Calendar time = Calendar.getInstance();
        if(Itime.length()<=5){
            Itime = Itime+":00";
        }

        hour=Itime.substring(0,Itime.indexOf(":"));
        min=Itime.substring(Itime.indexOf(":")+1,Itime.lastIndexOf(":"));
        sec=Itime.substring(Itime.lastIndexOf(":")+1);
        try{
            time.set(Calendar.HOUR_OF_DAY,Integer.parseInt(hour));
            time.set(Calendar.MINUTE,Integer.parseInt(min));
            time.set(Calendar.SECOND,Integer.parseInt(sec));

            time.add(Calendar.MINUTE,+Imin);
            time.add(Calendar.SECOND,+Isec);

        }catch (Exception e){
            e.printStackTrace();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        return sdf.format(time.getTime());
    }

    public String CountTime_min(String Itime, int Imin, int Isec){
        String hour="",min="",sec="";
        Calendar time = Calendar.getInstance();
        if(Itime.length()<=5){
            Itime = Itime+":00";
        }

        hour=Itime.substring(0,Itime.indexOf(":"));
        min=Itime.substring(Itime.indexOf(":")+1,Itime.lastIndexOf(":"));
        sec=Itime.substring(Itime.lastIndexOf(":")+1);
        try{
            time.set(Calendar.HOUR_OF_DAY,Integer.parseInt(hour));
            time.set(Calendar.MINUTE,Integer.parseInt(min));
            time.set(Calendar.SECOND,Integer.parseInt(sec));

            time.add(Calendar.MINUTE,+Imin);
            time.add(Calendar.SECOND,+Isec);

        }catch (Exception e){
            e.printStackTrace();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        return sdf.format(time.getTime());
    }

    public void getTimeKneader(String btc_no,String ins_no) {
        time_kn = "" ;
        try {
            CallApi.gettimekneader(factory, btc_no, ins_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        time_kn = response.getString("time");
                        if (time_kn!=null && !time_kn.equals("")){
                            if (name_mcs.contains("170FS")){
                                edt_timecreate.setText(CountTime(time_kn,19,0));
                            }else if (name_mcs.contains("XL4MB") || name_mcs.contains("Xl4MB")){
                                edt_timecreate.setText(CountTime_min(time_kn,19,30));
                            }else edt_timecreate.setText(CountTime(time_kn,25,0));
                        }else{
                            try {
                                CallApi.getCurrentTime(factory, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            edt_timecreate.setText(response.getString("curent_time"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String CleanTime(String Itime, int Imin, int Isec){
        String hour="", min="", sec="";
        Calendar time = Calendar.getInstance();
        if(Itime.length()<=5){
            Itime = Itime+":00";
        }

        hour=Itime.substring(0,Itime.indexOf(":"));
        min=Itime.substring(Itime.indexOf(":")+1,Itime.lastIndexOf(":"));
        sec=Itime.substring(Itime.lastIndexOf(":")+1);
        try{
            time.set(Calendar.HOUR_OF_DAY,Integer.parseInt(hour));
            time.set(Calendar.MINUTE,Integer.parseInt(min));
            time.set(Calendar.SECOND,Integer.parseInt(sec));

            time.add(Calendar.MINUTE,-Imin);
            time.add(Calendar.SECOND,+Isec);

        }catch (Exception e){
            e.printStackTrace();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        return sdf.format(time.getTime());
    }

    public void event_lv() {
        lv_pelletization.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                long viewId = view.getId();
                if (viewId == R.id.rdo_check) {
                    btn_save.setEnabled(false);
                    btn_cancel.setEnabled(false);
                    setdefault(position);
                    btnDefault(false);
                    btn_edit.setEnabled(true);
                    spn_shift.setEnabled(true);
                    spn_batchno.setEnabled(true);
                    edt_Date.setEnabled(true);
                    btc_no = listBtc_pells.get(position).getBTC_NO();
                    batch_no = listBtc_pells.get(position).getBTC_BATCH_NO();
                    model = listBtc_pells.get(position).getBTC_STYLE();
                    color = listBtc_pells.get(position).getBTC_COLOR();
                    btn_kneader.setEnabled(true);
                    btn_openmill.setEnabled(true);
                    positions=position;

                    btc_current.setBTC_BATCH_NO(listBtc_pells.get(position).getBTC_BATCH_NO());
                    btc_current.setBTC_NO(listBtc_pells.get(position).getBTC_NO());
                    btc_current.setPELL_OUT_TEMP_NOTE(listBtc_pells.get(position).getPELL_OUT_TEMP_NOTE());
                    btc_current.setPELL_TEMP_1_NOTE(listBtc_pells.get(position).getPELL_TEMP_1_NOTE());
                    btc_current.setPELL_TEMP_2_NOTE(listBtc_pells.get(position).getPELL_TEMP_2_NOTE());
                    btc_current.setPELL_TEMP_3_NOTE(listBtc_pells.get(position).getPELL_TEMP_3_NOTE());
                    btc_current.setPELL_TEMP_6_NOTE(listBtc_pells.get(position).getPELL_TEMP_6_NOTE());
                    btc_current.setPELL_TEMP_HEAD_NOTE(listBtc_pells.get(position).getPELL_TEMP_HEAD_NOTE());
                    btc_current.setAMT_NOTE(listBtc_pells.get(position).getAMT_NOTE());

                    if (listBtc_pells.get(position).getPELL_OUT_TEMP_NOTE() != null) {
                        if (!listBtc_pells.get(position).getPELL_OUT_TEMP_NOTE().equals("")) {
                            temp_note1.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note1.setBackgroundColor(Color.WHITE);
                        }
                    } else {
                        temp_note1.setBackgroundColor(Color.WHITE);
                    }

                    if (listBtc_pells.get(position).getPELL_TEMP_1_NOTE() != null) {
                        if (!listBtc_pells.get(position).getPELL_TEMP_1_NOTE().equals("")) {
                            temp_note2.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note2.setBackgroundColor(Color.parseColor("#e8eaf6"));
                        }
                    } else {
                        temp_note2.setBackgroundColor(Color.parseColor("#e8eaf6"));
                    }

                    if (listBtc_pells.get(position).getPELL_TEMP_2_NOTE() != null) {
                        if (!listBtc_pells.get(position).getPELL_TEMP_2_NOTE().equals("")) {
                            temp_note3.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note3.setBackgroundColor(Color.parseColor("#e8eaf6"));
                        }
                    } else {
                        temp_note3.setBackgroundColor(Color.parseColor("#e8eaf6"));
                    }


                    if (listBtc_pells.get(position).getPELL_TEMP_3_NOTE() != null) {
                        if (!listBtc_pells.get(position).getPELL_TEMP_3_NOTE().equals("")) {
                            temp_note4.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note4.setBackgroundColor(Color.parseColor("#e8eaf6"));
                        }
                    } else {
                        temp_note4.setBackgroundColor(Color.parseColor("#e8eaf6"));
                    }


                    if (listBtc_pells.get(position).getPELL_TEMP_HEAD_NOTE() != null) {

                        if (!listBtc_pells.get(position).getPELL_TEMP_HEAD_NOTE().equals("")) {
                            temp_note5.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note5.setBackgroundColor(Color.parseColor("#e8eaf6"));
                        }
                    } else {
                        temp_note5.setBackgroundColor(Color.parseColor("#e8eaf6"));
                    }

                    if (listBtc_pells.get(position).getPELL_TEMP_6_NOTE() != null) {

                        if (!listBtc_pells.get(position).getPELL_TEMP_6_NOTE().equals("")) {
                            temp_note6.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note6.setBackgroundColor(Color.parseColor("#e8eaf6"));
                        }
                    } else {
                        temp_note6.setBackgroundColor(Color.parseColor("#e8eaf6"));
                    }

                }

            }
        });
    }
    public void btnDefault(boolean ena) {

        edt_temp1.setEnabled(ena);
        edt_temp2.setEnabled(ena);
        edt_temp3.setEnabled(ena);
        edt_temp5.setEnabled(ena);
        edt_tempfirst.setEnabled(ena);
        edt_newbno.setEnabled(ena);
        edt_dumptemp.setEnabled(ena);

        edt_timecreate.setEnabled(ena);
        edt_timecombine.setEnabled(ena);
        edt_timeout.setEnabled(ena);
        edt_clean_time.setEnabled(ena);

        btn_timecreate.setEnabled(ena);
        btn_timecombine.setEnabled(ena);
        btn_timeclean.setEnabled(ena);
        btn_timeout.setEnabled(ena);
        edt_newbno.setEnabled(ena);

    }
    public void setdefault(int position) {
        edt_temp1.setText(listBtc_pells.get(position).getPELL_TEMP_1());
        edt_temp2.setText(listBtc_pells.get(position).getPELL_TEMP_2());
        edt_temp3.setText(listBtc_pells.get(position).getPELL_TEMP_3());
        edt_temp5.setText(listBtc_pells.get(position).getPELL_TEMP_5());
        edt_tempfirst.setText(listBtc_pells.get(position).getPELL_TEMP_HEAD());
        edt_timecreate.setText(listBtc_pells.get(position).getPELL_GRAN());
        edt_timecombine.setText(listBtc_pells.get(position).getPELL_MIXING_TIME());
        if(listBtc_pells.get(position).getNEW_BATCH_NO()!=null ) {
            String value = listBtc_pells.get(position).getNEW_BATCH_NO().substring(0, 3);
            String value1 = listBtc_pells.get(position).getNEW_BATCH_NO().substring(3);
            edt_newbno0.setText(value);
            edt_newbno1.setText(value1);
            edt_newbno0.setEnabled(false);
            flagsave=false;
            isExsist = true;
        }
        else {
            edt_newbno0.setEnabled(true);
            edt_newbno0.setText("");
            edt_newbno1.setText("");
            flagsave = true;
            isExsist = false;
        }
        edt_newbno.setText(listBtc_pells.get(position).getNEW_BATCH_NO());
        edt_dumptemp.setText(listBtc_pells.get(position).getPELL_OUT_TEMP());
        edt_clean_time.setText(listBtc_pells.get(position).getPELL_CLEAN_TIME());
        edt_timeout.setText(listBtc_pells.get(position).getPELL_OUT_TIME());
    }


    public void datePickerDialog(final EditText edt_date1) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(Pelletization_Controller.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int monthcurrent=monthOfYear + 1;
                        String month="";
                        String day="";
                        if(dayOfMonth>9) {
                            day=String.valueOf(dayOfMonth);
                        }
                        else
                        {
                            day="0"+String.valueOf(dayOfMonth);
                        }
                        if(monthcurrent>9)
                        {
                            month=String.valueOf(monthcurrent);
                        }
                        else
                        {
                            month="0"+String.valueOf(monthcurrent);

                        }
                        edt_date1.setText(month + "/" + day + "/" + year);
                       // getMCS(spn_shift.getSelectedItem().toString(),edt_date1.getText().toString());
                        getMCS2(spn_shift.getSelectedItem().toString(),edt_date1.getText().toString());

                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.show();


    }

    //get group id
   // public void getMCS(String mcs_shift,String mcs_date) {
    public void getMCS2(String mcs_shift,String mcs_date) {
        try {
         //   CallApi.getGroupID(factory, mcs_shift, mcs_date, new Response.Listener<JSONObject>() {
            CallApi.getGroupID2(factory, mcs_shift, mcs_date, new Response.Listener<JSONObject>()
//            CallApi.getGroupIDstep3(factory, mcs_shift, mcs_date, new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response) {
                    McsArm mcsArm = gson.fromJson(response.toString(), McsArm.class);
                    mcsl = mcsArm.getItems() == null ? new ArrayList<QAM_INS>() : mcsArm.getItems();
                    arrMCS = new ArrayList<>();
                    arrGroupID = new ArrayList<>();
                    for (QAM_INS df : mcsl) {
                        arrMCS.add(df);
                        arrGroupID.add(df.getINS_NO());
                    }
                    ArrayAdapter<String> adapter =
                            new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, arrGroupID);
//                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                    spn_batchno.setAdapter(adapter);
                    if (spn_batchno.getCount() <= 0) {
                        listBtc_pells = new ArrayList<BTC_PELL>();
                        listPELLAdapter = new ListPELLAdapter(getApplicationContext(), R.layout.list_pelletization_adapter, listBtc_pells);
                        lv_pelletization.setAdapter(listPELLAdapter);
                        cleardata();
                        btn_edit.setEnabled(false);
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getBTC_PELL(String ins_no, String mcs_date, String mcs_shift) {
        try {
            CallApi.getbtc_pell(factory, ins_no, mcs_date, mcs_shift, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    listBtc_pells = new ArrayList<>();
                    Btcs_pellArm btcs_pellArm = gson.fromJson(response.toString(), Btcs_pellArm.class);
                    listBtc_pells = btcs_pellArm.getItems() == null ? new ArrayList<BTC_PELL>() : btcs_pellArm.getItems();
                    listPELLAdapter = new ListPELLAdapter(getApplicationContext(), R.layout.list_pelletization_adapter, listBtc_pells);
                    lv_pelletization.setAdapter(listPELLAdapter);
                    if (lv_pelletization.getCount()<=0)
                    {
                        cleardata();
                        btn_edit.setEnabled(false);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void cleardata()
    {
        edt_newbno.setText("");
        edt_newbno0.setText("");
        edt_newbno1.setText("");
        edt_timecreate.setText("");
        edt_clean_time.setText("");
        edt_timeout.setText("");
        edt_timecombine.setText("");
        edt_temp3.setText("");
        edt_temp2.setText("");
        edt_temp1.setText("");
        edt_temp5.setText("");
        edt_dumptemp.setText("");
        edt_tempfirst.setText("");

    }

    public class McsArm extends ApiReturnModel<ArrayList<QAM_INS>> {
    }
    public class Btcs_pellArm extends ApiReturnModel<ArrayList<BTC_PELL>> {
    }



}
