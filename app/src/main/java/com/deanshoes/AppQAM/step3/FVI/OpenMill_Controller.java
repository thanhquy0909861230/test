package com.deanshoes.AppQAM.step3.FVI;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.ListOPAdapter;
import com.deanshoes.AppQAM.model.QAM.BTC_OP;
import com.deanshoes.AppQAM.model.QAM.QAM_INS;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.deanshoes.AppQAM.step3.Popup_Note;
import com.deanshoes.AppQAM.step4.pdfActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class OpenMill_Controller extends AppCompatActivity {
    private Gson gson = new GsonBuilder().setDateFormat("MM/dd/yyyy HH:mm:ss").create();
    Spinner spn_shift, spn_batchno;
    String arrShift[] = {"1", "2", "3"};
    ArrayAdapter<String> adtListShift;
    DatePickerDialog datePickerDialog;

    String factory = "";
    EditText edt_Date,edt_temp1,edt_thin1,edt_thick_thin,edt_thin2,edt_batchno,edt_model,edt_color;
    EditText edt_std_tem1_d,edt_std_tem1_l,edt_std_thick1_d,edt_std_thick1_l,edt_std_thin_d,edt_std_thin_l,
        edt_std_thick2_d,edt_std_thick2_l;
    private List<QAM_INS> mcsl;
    ArrayList<QAM_INS> arrMCS;
    ListView lv_openmill;
    BTC_OP btc_op;
    BTC_OP btc_current;
    String btc_no = "";
    String mcs_no = "",batch_no="",model="",color="",user ="";
    String ins_no11;
    private ArrayList<BTC_OP> listBtc_ops;
    ListOPAdapter listOPAdapter;
    Button  btn_edit, btn_save, btn_cancel,btn_kn,btn_mac,btn_openmill,btn_step2,btn_step4;
    ImageView temp_note1,thick_note,thick_note2,thick_note3;
    int positions=-1;
    private static boolean statusPop;
    ImageButton ib_logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_mill);

        Intent intent=getIntent();
        final Bundle bundle=intent.getExtras();
        if (bundle!=null){
            user=bundle.getString("user","ADMIN");
            factory=bundle.getString("fac");
        }
        btn_step2  = findViewById(R.id.btn_step2);
        btn_step4 = findViewById(R.id.btn_step4);
        spn_shift = findViewById(R.id.spn_shift);
        adtListShift = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrShift);
        spn_shift.setAdapter(adtListShift);
        lv_openmill= findViewById(R.id.lv_openmill);
        edt_Date = findViewById(R.id.edt_date);
        spn_batchno = findViewById(R.id.spn_batchno);
        btn_edit= findViewById(R.id.btn_edit);
        btn_save= findViewById(R.id.btn_save);
        btn_cancel= findViewById(R.id.btn_cancel);
        btn_kn= findViewById(R.id.btn_kn);
        btn_mac= findViewById(R.id.btn_mac);
        btn_openmill= findViewById(R.id.btn_openmill);
        edt_temp1= findViewById(R.id.edt_temp1);
        edt_thin1= findViewById(R.id.edt_thin1);
//        edt_temp_thin= findViewById(R.id.edt_temp_thin);
        edt_thick_thin= findViewById(R.id.edt_thick_thin);
//        edt_temp2= findViewById(R.id.edt_temp2);
        edt_thin2= findViewById(R.id.edt_thin2);

        edt_batchno= findViewById(R.id.edt_batchno);
        edt_model= findViewById(R.id.edt_model);
        edt_color= findViewById(R.id.edt_color);
        temp_note1 = findViewById(R.id.temp_note1);
        thick_note = findViewById(R.id.thick_note);
        thick_note2 = findViewById(R.id.thick_note2);
        thick_note3 = findViewById(R.id.thick_note3);
        ib_logout = findViewById(R.id.ib_logout);

        edt_std_tem1_d = findViewById(R.id.edt_std_tem1_d);
        edt_std_tem1_l = findViewById(R.id.edt_std_tem1_l);
        edt_std_thick1_d= findViewById(R.id.edt_std_thick1_d);
        edt_std_thick1_l= findViewById(R.id.edt_std_thick1_l);
        edt_std_thin_d= findViewById(R.id.edt_std_thin_d);
        edt_std_thin_l= findViewById(R.id.edt_std_thin_l);
        edt_std_thick2_d= findViewById(R.id.edt_std_thick2_d);
        edt_std_thick2_l= findViewById(R.id.edt_std_thick2_l);

        temp_note1.setVisibility(View.VISIBLE);
        thick_note.setVisibility(View.VISIBLE);
        thick_note2.setVisibility(View.VISIBLE);
        thick_note3.setVisibility(View.VISIBLE);

        btc_current=new BTC_OP();

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_save.setEnabled(true);
                btn_cancel.setEnabled(true);
                btn_edit.setEnabled(false);
                btnDefault(true);
                spn_shift.setEnabled(false);
                spn_batchno.setEnabled(false);
                edt_Date.setEnabled(false);
                btn_kn.setEnabled(false);
                btn_mac.setEnabled(false);
                listOPAdapter.disableitem(true);
                statusPop=false;
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (saveop(btc_no) == 1) {

                    btnDefault(false);
                    btn_save.setEnabled(false);
                    btn_cancel.setEnabled(false);
                    spn_shift.setEnabled(true);
                    spn_batchno.setEnabled(true);
                    edt_Date.setEnabled(true);
                    btn_edit.setEnabled(true);
                    btn_kn.setEnabled(true);
                    btn_mac.setEnabled(true);
                    listOPAdapter.disableitem(false);
                    statusPop=true;
                }

            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDefault(false);
                btn_save.setEnabled(false);
                btn_cancel.setEnabled(false);
                btn_edit.setEnabled(true);
                spn_shift.setEnabled(true);
                spn_batchno.setEnabled(true);
                edt_Date.setEnabled(true);
                btn_kn.setEnabled(true);
                btn_mac.setEnabled(true);
                listOPAdapter.disableitem(false);
                statusPop=true;
            }
        });

        btn_kn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(OpenMill_Controller.this, Kneader_Controller.class);
                myIntent.putExtras(bundle);
                OpenMill_Controller.this.startActivity(myIntent);
            }
        });
        btn_mac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(OpenMill_Controller.this, Pelletization_Controller.class);
                myIntent.putExtras(bundle);
                OpenMill_Controller.this.startActivity(myIntent);
            }
        });
        btn_step2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(OpenMill_Controller.this, Mac1Activity_FVI.class);
                myIntent.putExtras(bundle);
                OpenMill_Controller.this.startActivity(myIntent);
            }
        });
        btn_step4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(OpenMill_Controller.this, pdfActivity.class);
                myIntent.putExtras(bundle);
                OpenMill_Controller.this.startActivity(myIntent);
            }
        });


        edt_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog(edt_Date);
            }
        });
        spn_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String item = parentView.getItemAtPosition(position).toString();
                try {
                    if (edt_Date.getText().toString().isEmpty()) {
                    } else {
                    //    getMCS(edt_Date.getText().toString(), item);
                        getMCS2(item, edt_Date.getText().toString());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        spn_batchno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String item = parentView.getItemAtPosition(position).toString();
                mcs_no = item.substring(item.length() - 6, item.length());
                String[] ins_no1 = spn_batchno.getSelectedItem().toString().split("_");
                ins_no11 = ins_no1[0];
                try {
//                    getBTC_OP(parentView.getItemAtPosition(position).toString(), edt_Date.getText().toString(), spn_shift.getSelectedItem().toString());
                    getBTC_OP(ins_no11, edt_Date.getText().toString(), spn_shift.getSelectedItem().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(OpenMill_Controller.this, LoginActivity.class);
                startActivity(i);
            }
        });
        event_lv();
        bindevent();
        statusPop=true;

    }

    public void bindevent()
    {
        temp_note1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getOP_TEMP_1_NOTE(),statusPop,1);

            }
        });

        thick_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getOP_THICK1_NOTE(),statusPop,2);
            }
        });
        thick_note2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getOP_THIN_NOTE(),statusPop,3);
            }
        });
        thick_note3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btc_current!=null)
                    openNoteDialog(btc_current.getOP_THICK2_NOTE(),statusPop,4);
            }
        });


    }
    private void openNoteDialog(String note,boolean status,int pos) {

        Popup_Note fragement = (Popup_Note) getSupportFragmentManager().findFragmentByTag(Popup_Note.class.getSimpleName());
        if (fragement != null) fragement.dismiss();
        Popup_Note.newInstance(popupFragementListener,note,status,pos).show(getSupportFragmentManager(), Popup_Note.class.getSimpleName());

    }

    Popup_Note.PopupFragementListener popupFragementListener = new Popup_Note.PopupFragementListener() {

        @Override
        public void onSelected(String nt,int pos) {

            Popup_Note fragement = (Popup_Note) getSupportFragmentManager().findFragmentByTag(Popup_Note.class.getSimpleName());
            if (fragement != null) fragement.dismiss();
            if(pos==1)
            {
                btc_current.setOP_TEMP_1_NOTE(nt);
                if ( !btc_current.getOP_TEMP_1_NOTE().equals("")  ){
                    temp_note1.setBackgroundColor(Color.YELLOW);
                }
                else temp_note1.setBackgroundColor(Color.parseColor("#e8eaf6"));
            }
            if(pos==2)
            {
                btc_current.setOP_THICK1_NOTE(nt);
                if ( !btc_current.getOP_THICK1_NOTE().equals("")  ){
                    thick_note.setBackgroundColor(Color.YELLOW);
                }
                else thick_note.setBackgroundColor(Color.parseColor("#e8eaf6"));
            }
            if(pos==3)
            {
                btc_current.setOP_THIN_NOTE(nt);
                if ( !btc_current.getOP_THIN_NOTE().equals("")  ){
                    thick_note2.setBackgroundColor(Color.YELLOW);
                }
                else thick_note2.setBackgroundColor(Color.parseColor("#e8eaf6"));
            }
            if(pos==4)
            {
                btc_current.setOP_THICK2_NOTE(nt);
                if ( !btc_current.getOP_THICK2_NOTE().equals("")  ){
                    thick_note3.setBackgroundColor(Color.YELLOW);
                }
                else thick_note3.setBackgroundColor(Color.parseColor("#e8eaf6"));
            }



        }

        @Override
        public void onCancel() {
            Popup_Note fragement = (Popup_Note) getSupportFragmentManager().findFragmentByTag(Popup_Note.class.getSimpleName());
            if (fragement != null) fragement.dismiss();
        }
    };
    public int saveop(String btc_no) {
        int result = 0;
        boolean flag = true;
        if (!edt_thin1.getText().toString().isEmpty()) {
            float vl=Float.valueOf(edt_thin1.getText().toString().replaceAll(",","."));
            if (vl<3.0 || vl>4.0){
                ToastCustom.message(getApplicationContext(), "Độ dày của lần cán liệu thứ 1 phải trong khoảng từ 3mm đến 4mm", Color.RED);
                flag = false;
            }
        }
        if (!edt_thick_thin.getText().toString().isEmpty()) {
            float vl=Float.valueOf(edt_thick_thin.getText().toString().replaceAll(",","."));
            if (vl<1.0 || vl>2.0){
                ToastCustom.message(getApplicationContext(), "Độ dày của độ mỏng phải trong khoảng từ 1mm đến 2mm", Color.RED);
                flag = false;
            }

        }
        if (!edt_thin2.getText().toString().isEmpty()) {
            float vl=Float.valueOf(edt_thin2.getText().toString().replaceAll(",","."));
            if (vl<3.0 || vl>4.0){
                ToastCustom.message(getApplicationContext(), "Độ dày của lần cán liệu thứ 2 phải trong khoảng từ 3mm đến 4mm", Color.RED);
                flag = false;
            }
        }
        if (!edt_temp1.getText().toString().isEmpty()) {
            if (Integer.parseInt(edt_temp1.getText().toString()) < 50 || Integer.parseInt(edt_temp1.getText().toString()) > 75) {
                ToastCustom.message(getApplicationContext(), "Nhiệt độ của lần cán liệu thứ 1 phải trong khoảng từ 55 đên 75 hoặc 50 đến 70", Color.RED);
                flag = false;
            }
        }

        try {

//            else if (!edt_temp_thin.getText().toString().isEmpty()) {
//                if (Integer.parseInt(edt_temp_thin.getText().toString()) < 55 || Integer.parseInt(edt_temp_thin.getText().toString()) > 75) {
//                    ToastCustom.message(getApplicationContext(), "Nhiệt độ của độ mỏng phải trong khoảng từ 55 đên 75", Color.RED);
//                    flag = false;
//                }
//            }
//            else if (!edt_temp2.getText().toString().isEmpty()) {
//                if (Integer.parseInt(edt_temp2.getText().toString()) < 55 || Integer.parseInt(edt_temp2.getText().toString()) > 75) {
//                    ToastCustom.message(getApplicationContext(), "Nhiệt độ của lần cán liệu thứ 2 phải trong khoảng từ 55 đên 75", Color.RED);
//                    flag = false;
//                }
//            }
//            else
//            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        if (flag == true) {
            btc_op = new BTC_OP();
            btc_op.setBTC_NO(btc_no);
            btc_op.setOP_TEMP_1(edt_temp1.getText().toString());
            btc_op.setOP_THICK_1(edt_thin1.getText().toString().replaceAll(",","."));
//            btc_op.setOP_TEMP_THIN(edt_temp_thin.getText().toString());
            btc_op.setOP_THICK_THIN(edt_thick_thin.getText().toString().replaceAll(",","."));
//            btc_op.setOP_TEMP_2(edt_temp2.getText().toString());
            btc_op.setOP_THICK_2(edt_thin2.getText().toString().replaceAll(",","."));
            btc_op.setUP_USER(user);

            ///edit
            btc_op.setBTC_BATCH_NO(batch_no);
            btc_op.setBTC_COLOR(color);
            btc_op.setBTC_STYLE(model);

            btc_op.setOP_TEMP_1_NOTE(btc_current.getOP_TEMP_1_NOTE());
//                btc_op.setOP_TEMP_2_NOTE(btc_current.getOP_TEMP_2_NOTE());
//                btc_op.setOP_TEMP_THIN_NOTE(btc_current.getOP_TEMP_THIN_NOTE());
            btc_op.setOP_STD_TEMP_D(edt_std_tem1_d.getText().toString());
            btc_op.setOP_STD_TEMP_L(edt_std_tem1_l.getText().toString());
            btc_op.setOP_STD_THICK1_D(edt_std_thick1_d.getText().toString());
            btc_op.setOP_STD_THICK1_L(edt_std_thick1_l.getText().toString());
            btc_op.setOP_STD_THIN_D(edt_std_thin_d.getText().toString());
            btc_op.setOP_STD_THIN_L(edt_std_thin_l.getText().toString());
            btc_op.setOP_STD_THICK2_D(edt_std_thick2_d.getText().toString());
            btc_op.setOP_STD_THICK2_L(edt_std_thick2_l.getText().toString());
            btc_op.setOP_THICK1_NOTE(btc_current.getOP_THICK1_NOTE());
            btc_op.setOP_THIN_NOTE(btc_current.getOP_THIN_NOTE());
            btc_op.setOP_THICK2_NOTE(btc_current.getOP_THICK2_NOTE());
            btc_op.setAMT_NOTE(btc_current.getAMT_NOTE());

            try {
                CallApi.insertOpenmill(factory, btc_op, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listBtc_ops.set(positions, btc_op);
                        listOPAdapter.notifyDataSetChanged();
                        ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

            result = 1;
        }
        return result;
    }

    public boolean check_standard(String edt, String std_d, String std_l){
        if(!edt.equals("") && (!std_d.equals("") || !std_l.equals(""))){
            int min=0,max=0,value=0;
            min=Integer.parseInt(std_d.substring(0,std_d.indexOf("-")));
            max = Integer.parseInt(std_d.substring(std_d.indexOf("-")+1));
            value= Integer.parseInt(edt);

            if (value<min || value>max){
                return true;
            }
        }
        return false;
    }

    public void event_lv() {
        lv_openmill.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                long viewId = view.getId();
                if (viewId == R.id.rdo_check) {
                    btn_save.setEnabled(false);
                    btn_cancel.setEnabled(false);
                    btn_edit.setEnabled(true);
                    btn_mac.setEnabled(true);
                    btn_kn.setEnabled(true);
                    setdefault(position);
                    btnDefault(false);
                    spn_shift.setEnabled(true);
                    spn_batchno.setEnabled(true);
                    edt_Date.setEnabled(true);
                    btc_no = listBtc_ops.get(position).getBTC_NO();
                    batch_no = listBtc_ops.get(position).getBTC_BATCH_NO();
                    model = listBtc_ops.get(position).getBTC_STYLE();
                    color = listBtc_ops.get(position).getBTC_COLOR();

                    positions=position;
                    btc_current.setBTC_BATCH_NO(listBtc_ops.get(position).getBTC_BATCH_NO());
                    btc_current.setBTC_NO(listBtc_ops.get(position).getBTC_NO());
//                        btc_current.setOP_TEMP_THIN_NOTE(listBtc_ops.get(position).getOP_TEMP_THIN_NOTE());
                    btc_current.setOP_TEMP_1_NOTE(listBtc_ops.get(position).getOP_TEMP_1_NOTE());
//                        btc_current.setOP_TEMP_2_NOTE(listBtc_ops.get(position).getOP_TEMP_2_NOTE());
                    btc_current.setOP_STD_TEMP_D(listBtc_ops.get(position).getOP_STD_TEMP_D());
                    btc_current.setOP_STD_TEMP_L(listBtc_ops.get(position).getOP_STD_TEMP_L());
                    btc_current.setOP_STD_THICK1_D(listBtc_ops.get(position).getOP_STD_THICK1_D());
                    btc_current.setOP_STD_THICK1_L(listBtc_ops.get(position).getOP_STD_THICK1_L());
                    btc_current.setOP_STD_THIN_D(listBtc_ops.get(position).getOP_STD_THIN_D());
                    btc_current.setOP_STD_THIN_L(listBtc_ops.get(position).getOP_STD_THIN_L());
                    btc_current.setOP_STD_THICK2_D(listBtc_ops.get(position).getOP_STD_THICK2_D());
                    btc_current.setOP_STD_THICK2_L(listBtc_ops.get(position).getOP_STD_THICK2_L());
                    btc_current.setOP_THICK1_NOTE(listBtc_ops.get(position).getOP_THICK1_NOTE());
                    btc_current.setOP_THIN_NOTE(listBtc_ops.get(position).getOP_THIN_NOTE());
                    btc_current.setOP_THICK2_NOTE(listBtc_ops.get(position).getOP_THICK2_NOTE());
                    btc_current.setAMT_NOTE(listBtc_ops.get(position).getAMT_NOTE());

                    if (listBtc_ops.get(position).getOP_TEMP_1_NOTE() != null) {
                        if (!listBtc_ops.get(position).getOP_TEMP_1_NOTE().equals("")) {
                            temp_note1.setBackgroundColor(Color.YELLOW);
                        } else {
                            temp_note1.setBackgroundColor(Color.parseColor("#e8eaf6"));
                        }
                    } else {
                        temp_note1.setBackgroundColor(Color.parseColor("#e8eaf6"));
                    }

                    if (listBtc_ops.get(position).getOP_THICK1_NOTE() != null) {
                        if (!listBtc_ops.get(position).getOP_THICK1_NOTE().equals("")) {
                            thick_note.setBackgroundColor(Color.YELLOW);
                        } else {
                            thick_note.setBackgroundColor(Color.parseColor("#e8eaf6"));
                        }
                    } else {
                        thick_note.setBackgroundColor(Color.parseColor("#e8eaf6"));
                    }

                    if (listBtc_ops.get(position).getOP_THIN_NOTE() != null) {
                        if (!listBtc_ops.get(position).getOP_THIN_NOTE().equals("")) {
                            thick_note2.setBackgroundColor(Color.YELLOW);
                        } else {
                            thick_note2.setBackgroundColor(Color.parseColor("#e8eaf6"));
                        }
                    } else {
                        thick_note2.setBackgroundColor(Color.parseColor("#e8eaf6"));
                    }


                    if (listBtc_ops.get(position).getOP_THICK2_NOTE() != null) {
                        if (!listBtc_ops.get(position).getOP_THICK2_NOTE().equals("")) {
                            thick_note3.setBackgroundColor(Color.YELLOW);
                        } else {
                            thick_note3.setBackgroundColor(Color.parseColor("#e8eaf6"));
                        }
                    } else {
                        thick_note3.setBackgroundColor(Color.parseColor("#e8eaf6"));
                    }


                }

            }
        });
    }
    public void btnDefault(boolean ena) {
        edt_temp1.setEnabled(ena);
        edt_thin1.setEnabled(ena);
//        edt_temp_thin.setEnabled(ena);
        edt_thick_thin.setEnabled(ena);
//        edt_temp2.setEnabled(ena);
        edt_thin2.setEnabled(ena);
        edt_std_tem1_d.setEnabled(ena);
        edt_std_tem1_l.setEnabled(ena);
        edt_std_thick1_d.setEnabled(ena);
        edt_std_thick1_l.setEnabled(ena);
        edt_std_thin_d.setEnabled(ena);
        edt_std_thin_l.setEnabled(ena);
        edt_std_thick2_d.setEnabled(ena);
        edt_std_thick2_l.setEnabled(ena);

    }
    public void setdefault(int position) {
        edt_batchno.setText(listBtc_ops.get(position).getBTC_BATCH_NO());
        edt_model.setText(listBtc_ops.get(position).getBTC_STYLE());
        edt_color.setText(listBtc_ops.get(position).getBTC_COLOR());
        edt_temp1.setText(listBtc_ops.get(position).getOP_TEMP_1());
        edt_thin1.setText(listBtc_ops.get(position).getOP_THICK_1());
//        edt_temp_thin.setText(listBtc_ops.get(position).getOP_TEMP_THIN());
        edt_thick_thin.setText(listBtc_ops.get(position).getOP_THICK_THIN());
//        edt_temp2.setText(listBtc_ops.get(position).getOP_TEMP_2());
        edt_thin2.setText(listBtc_ops.get(position).getOP_THICK_2());

        edt_std_tem1_d.setText(listBtc_ops.get(position).getOP_STD_TEMP_D() == null ? "55-75" : listBtc_ops.get(position).getOP_STD_TEMP_D());
        edt_std_tem1_l.setText(listBtc_ops.get(position).getOP_STD_TEMP_L() == null ? "50-70" : listBtc_ops.get(position).getOP_STD_TEMP_L());
        edt_std_thick1_d.setText(listBtc_ops.get(position).getOP_STD_THICK1_D() == null ? "3-4" : listBtc_ops.get(position).getOP_STD_THICK1_D());
        edt_std_thick1_l.setText(listBtc_ops.get(position).getOP_STD_THICK1_L() == null ? "3-4" : listBtc_ops.get(position).getOP_STD_THICK1_L());
        edt_std_thin_d.setText(listBtc_ops.get(position).getOP_STD_THIN_D() == null ? "1-2" : listBtc_ops.get(position).getOP_STD_THIN_D());
        edt_std_thin_l.setText(listBtc_ops.get(position).getOP_STD_THIN_L() == null ? "1-2" : listBtc_ops.get(position).getOP_STD_THIN_L());
        edt_std_thick2_d.setText(listBtc_ops.get(position).getOP_STD_THICK2_D() == null ? "3-4" : listBtc_ops.get(position).getOP_STD_THICK2_D());
        edt_std_thick2_l.setText(listBtc_ops.get(position).getOP_STD_THICK2_L() == null ? "3-4" : listBtc_ops.get(position).getOP_STD_THICK2_L());
    }


    public void datePickerDialog(final EditText edt_date1) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(OpenMill_Controller.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int monthcurrent=monthOfYear + 1;
                        String month="";
                        String day="";
                        if(dayOfMonth>9) {
                            day=String.valueOf(dayOfMonth);

                        }
                        else
                        {
                            day="0"+String.valueOf(dayOfMonth);
                        }
                        if(monthcurrent>9)
                        {
                            month=String.valueOf(monthcurrent);
                        }
                        else
                        {
                            month="0"+String.valueOf(monthcurrent);

                        }
                        edt_date1.setText(month + "/" + day + "/" + year);
                       // getMCS(edt_date1.getText().toString(), spn_shift.getSelectedItem().toString());
                        getMCS2(spn_shift.getSelectedItem().toString(), edt_date1.getText().toString());

                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.show();


    }


   // public void getMCS(String mcs_date, String mcs_shift){
    public void getMCS2(String mcs_shift, String mcs_date)
    {
        try {
           // CallApi.getmcs(factory, mcs_date, mcs_shift, new Response.Listener<JSONObject>()
            CallApi.getGroupID2(factory, mcs_shift, mcs_date,  new Response.Listener<JSONObject>()
//            CallApi.getGroupIDstep3(factory, mcs_shift, mcs_date,  new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response) {
                    McsArm mcsArm = gson.fromJson(response.toString(), McsArm.class);
                    mcsl = mcsArm.getItems() == null ? new ArrayList<QAM_INS>() : mcsArm.getItems();
                    arrMCS = new ArrayList<>();
                    for (QAM_INS df : mcsl) {
                        arrMCS.add(df);
                    }
                    ArrayAdapter<QAM_INS> adapter =
                            new ArrayAdapter<QAM_INS>(getApplicationContext(), R.layout.spinner_item, arrMCS);
//                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                    spn_batchno.setAdapter(adapter);
                    if (spn_batchno.getCount() <= 0) {
                        listBtc_ops = new ArrayList<BTC_OP>();
                        listOPAdapter = new ListOPAdapter(getApplicationContext(), R.layout.list_openmill_adapter, listBtc_ops);
                        lv_openmill.setAdapter(listOPAdapter);
                        cleardata();
                        btn_edit.setEnabled(false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getBTC_OP(String mcs_no, String mcs_date, String mcs_shift) {
        try {
            CallApi.getbtc_op(factory, mcs_no, mcs_date, mcs_shift, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    listBtc_ops = new ArrayList<>();
                    Btcs_opArm btcs_opArm = gson.fromJson(response.toString(), Btcs_opArm.class);
                    listBtc_ops = btcs_opArm.getItems() == null ? new ArrayList<BTC_OP>() : btcs_opArm.getItems();
                    listOPAdapter = new ListOPAdapter(getApplicationContext(), R.layout.list_openmill_adapter, listBtc_ops);
                    lv_openmill.setAdapter(listOPAdapter);
                    if (lv_openmill.getCount() <= 0)
                    {
                        cleardata();
                        btn_edit.setEnabled(false);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void cleardata() {
        edt_batchno.setText("");
        edt_model.setText("");
        edt_color.setText("");
        edt_temp1.setText("");
//        edt_temp2.setText("");
//        edt_temp_thin.setText("");
        edt_thick_thin.setText("");
        edt_thin1.setText("");
        edt_thin2.setText("");

        edt_std_tem1_d.setText("");
        edt_std_tem1_l.setText("");
        edt_std_thick1_d.setText("");
        edt_std_thick1_l.setText("");
        edt_std_thin_d.setText("");
        edt_std_thin_l.setText("");
        edt_std_thick2_d.setText("");
        edt_std_thick2_l.setText("");
    }


    public class McsArm extends ApiReturnModel<ArrayList<QAM_INS>> {
    }
    public class Btcs_opArm extends ApiReturnModel<ArrayList<BTC_OP>> {
    }

}
