package com.deanshoes.AppQAM.db;

import android.content.Context;

import com.deanshoes.dsapptools.tools.db.BaseDAO;
import com.deanshoes.dsapptools.tools.model.DBBaseModel;


/**
 * Created by afc on 16/1/8.
 * 資料庫存取器
 */
abstract class MyBaseDAO<T extends DBBaseModel> extends BaseDAO<T> {

    // 建構子，一般的應用都不需要修改
    MyBaseDAO(Context context) {
        super(context);
        mDB = DBHelper.getDatabase(context);
    }


}
