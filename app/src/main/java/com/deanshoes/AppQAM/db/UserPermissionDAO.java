package com.deanshoes.AppQAM.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;


import com.deanshoes.dsapptools.tools.model.UserPermissionModel;

import java.util.ArrayList;


public class UserPermissionDAO  extends MyBaseDAO<UserPermissionModel> {

    // 表格名稱
    public static final String TABLE_NAME = "ds_user_permission";

    // 其它表格欄位名稱
    public static final String USER_ID_COLUMN = "_user_id";

    public static final String APP_ID_COLUMN = "_app_id";

    public static final String FUNCTION_ID_COLUMN = "_function_id";

    public static final String IS_OPEN_COLUMN = "_is_open";

    public static final String CAN_INQUIRY_COLUMN = "_can_inquiry";

    public static final String CAN_CREATE_COLUMN = "_can_create";

    public static final String CAN_EDIT_COLUMN = "_can_edit";

    public static final String CAN_DELETE_COLUMN = "_can_delete";

    public static final String UPDATEDATE_COLUMN = "_updatedate";


    // 使用上面宣告的變數建立表格的SQL指令
    public static final String CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                    USER_ID_COLUMN + " TEXT NOT NULL, " +
                    APP_ID_COLUMN + " TEXT NOT NULL, " +
                    FUNCTION_ID_COLUMN + " INTEGER NOT NULL, " +
                    IS_OPEN_COLUMN + " INTEGER NOT NULL DEFAULT 0 , " +
                    CAN_INQUIRY_COLUMN + " INTEGER NOT NULL DEFAULT 0 , " +
                    CAN_CREATE_COLUMN + " INTEGER NOT NULL DEFAULT 0 , " +
                    CAN_EDIT_COLUMN + " INTEGER NOT NULL DEFAULT 0 , " +
                    CAN_DELETE_COLUMN + " INTEGER NOT NULL DEFAULT 0 , " +
                    UPDATEDATE_COLUMN + " INTEGER NOT NULL ," +
                    "PRIMARY KEY ( " + USER_ID_COLUMN + ", " + APP_ID_COLUMN + ", " + FUNCTION_ID_COLUMN + ")" +
                    " )";


    // 建構子，一般的應用都不需要修改
    public UserPermissionDAO(Context context) {
        super(context);
    }



    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public UserPermissionModel insert(UserPermissionModel item) {

        // 建立準備新增資料的ContentValues物件
        ContentValues cv = new ContentValues();


        // 加入ContentValues物件包裝的新增資料
        // 第一個參數是欄位名稱， 第二個參數是欄位的資料
        cv.put(USER_ID_COLUMN, item.mUserID);
        cv.put(APP_ID_COLUMN, item.mAppID);
        cv.put(FUNCTION_ID_COLUMN, item.mFunctionID);
        cv.put(IS_OPEN_COLUMN, item.mIsOpen ? 1 : 0);
        cv.put(CAN_INQUIRY_COLUMN, item.mCanInquiry ? 1 : 0);
        cv.put(CAN_CREATE_COLUMN, item.mCanCreate ? 1 : 0);
        cv.put(CAN_EDIT_COLUMN, item.mCanEdit ? 1 : 0);
        cv.put(CAN_DELETE_COLUMN, item.mCanDelete ? 1 : 0);
        cv.put(UPDATEDATE_COLUMN, 0);


        // 新增一筆資料並取得編號
        // 第一個參數是表格名稱
        // 第二個參數是沒有指定欄位值的預設值
        // 第三個參數是包裝新增資料的ContentValues物件
        long id = mDB.insert(TABLE_NAME, null, cv);

        item.mKid = id;

        // 回傳結果
        return item;
    }


    public void insert(ArrayList<UserPermissionModel> items) {

        try {

            String sql = "INSERT INTO " + TABLE_NAME
                    + " ( "
                    + USER_ID_COLUMN + ", "
                    + APP_ID_COLUMN + ", "
                    + FUNCTION_ID_COLUMN + ", "
                    + IS_OPEN_COLUMN + ", "
                    + CAN_INQUIRY_COLUMN + ", "
                    + CAN_CREATE_COLUMN + ", "
                    + CAN_EDIT_COLUMN + ", "
                    + CAN_DELETE_COLUMN + ", "
                    + UPDATEDATE_COLUMN+ " "
                    + " ) "
                    + "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? )";


            mDB.beginTransactionNonExclusive();

            SQLiteStatement stmt = mDB.compileStatement(sql);

            for (int x = 0; x < items.size(); x++) {


                stmt.bindString(1, items.get(x).mUserID);
                stmt.bindString(2, items.get(x).mAppID);
                stmt.bindLong(3, items.get(x).mFunctionID);
                stmt.bindLong(4, items.get(x).mIsOpen ? 1 : 0);
                stmt.bindLong(5, items.get(x).mCanInquiry ? 1 : 0);
                stmt.bindLong(6, items.get(x).mCanCreate ? 1 : 0);
                stmt.bindLong(7, items.get(x).mCanEdit ? 1 : 0);
                stmt.bindLong(8, items.get(x).mCanDelete ? 1 : 0);
                stmt.bindLong(9, 0);

                stmt.execute();
                stmt.clearBindings();

            }

            mDB.setTransactionSuccessful();


        } catch (Exception e) {


        } finally {
            mDB.endTransaction();
        }
    }

    /**
     * 取得使用者某功能的權限
     * @param userID
     * @param appID
     * @param functionID
     * @return
     */
    public UserPermissionModel get(String userID, String appID, int functionID) {


        UserPermissionModel item = new UserPermissionModel();

        String where = USER_ID_COLUMN + "='" + userID + "' AND " + APP_ID_COLUMN + "='" + appID + "' AND " + FUNCTION_ID_COLUMN + "=" + functionID;

        // 執行查詢
        Cursor cursor = mDB.query(
                TABLE_NAME, null, where, null, null, null, null, null);

        // 如果有查詢結果

        // 如果有查詢結果
        if (cursor.moveToFirst()) {
            item = getRecord(cursor);
        }
        // 關閉Cursor物件
        cursor.close();

        // 回傳結果
        return item;
    }


    public ArrayList<UserPermissionModel> get(String userID, String appID) {


        ArrayList<UserPermissionModel> result = new ArrayList<UserPermissionModel>();

        String where = USER_ID_COLUMN + "='" + userID + "' AND " + APP_ID_COLUMN + "='" + appID + "'";

        // 執行查詢
        Cursor cursor = mDB.query(
                TABLE_NAME, null, where, null, null, null, null, null);

        // 如果有查詢結果

        // 如果有查詢結果
        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        // 關閉Cursor物件
        cursor.close();

        // 回傳結果
        return result;
    }



    /**
     * 把Cursor目前的資料包裝為物件
     * @param cursor cursor
     * @return BloodPressure 資料
     */
    @Override
    public UserPermissionModel getRecord(Cursor cursor) {

        // 準備回傳結果用的物件
        UserPermissionModel result = new UserPermissionModel();

        result.mAppID = cursor.getString(cursor.getColumnIndex(APP_ID_COLUMN));
        result.mFunctionID = cursor.getInt(cursor.getColumnIndex(FUNCTION_ID_COLUMN));
        result.mIsOpen = cursor.getInt(cursor.getColumnIndex(IS_OPEN_COLUMN)) == 1;
        result.mCanInquiry = cursor.getInt(cursor.getColumnIndex(CAN_INQUIRY_COLUMN)) == 1;
        result.mCanCreate = cursor.getInt(cursor.getColumnIndex(CAN_CREATE_COLUMN)) == 1;
        result.mCanEdit = cursor.getInt(cursor.getColumnIndex(CAN_EDIT_COLUMN)) == 1;
        result.mCanDelete = cursor.getInt(cursor.getColumnIndex(CAN_DELETE_COLUMN)) == 1;

        // 回傳結果
        return result;
    }


}
