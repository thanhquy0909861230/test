package com.deanshoes.AppQAM.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by polory.cheng on 2016/12/12.
 * SQLite
 */

public class DBHelper extends SQLiteOpenHelper {

    // TODO 資料庫名稱，依專案設定資料庫名稱
    public static final String DATABASE_NAME = "ds.default.db";
    // 資料庫版本，資料結構改變的時候要更改這個數字，通常是加一
    public static final int VERSION = 1;
    // 資料庫物件，固定的欄位變數
    private static SQLiteDatabase database;

    // 建構子，在一般的應用都不需要修改
    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                    int version) {
        super(context, name, factory, version);
    }

    // 需要資料庫的元件呼叫這個方法，這個方法在一般的應用都不需要修改
    public static SQLiteDatabase getDatabase(Context context) {

        // 單例，避免重覆建立物件
        SQLiteDatabase inst = database;
        if (inst == null || !inst.isOpen()) {
            synchronized (SQLiteDatabase.class) {
                inst = database;
                if (inst == null || !inst.isOpen()) {
                    inst = new DBHelper(context, DATABASE_NAME,
                            null, VERSION).getWritableDatabase();;
                    database = inst;
                }
            }
        }
        return inst;

    }


    /**
     * 資料庫初始化時呼叫
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        // 依專案，建立所需資料表
        db.execSQL(UserPermissionDAO.CREATE_TABLE);

    }

    /**
     * 在資料庫版本號碼更新時自動呼叫
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        // 視需求刪除表格或作其他操作

        // 呼叫onCreate建立新版的表格
        onCreate(db);

    }



}