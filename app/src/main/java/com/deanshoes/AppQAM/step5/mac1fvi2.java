
package com.deanshoes.AppQAM.step5;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.QAM_TEMP_MODEL;
import com.deanshoes.AppQAM.model.QAM.TEMP;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step2.fvi.IPBatchActivity_FVI;
import com.deanshoes.AppQAM.step4.SendMail;
import com.deanshoes.AppQAM.step4.Temp_Note;
import com.deanshoes.AppQAM.step4.pdfActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class mac1fvi2 extends AppCompatActivity {
    //    private static final String TAG = NDK_IP.class.getSimpleName();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;
    Button btnClear;
    TEMP temp;
    int c1, d1, c2, d2, a1;
    Button btnSave, btn_step5, btnEx, btnSearch, btnCancle, btnNote,btn_part6, btnDelete, btnEdit;
    ImageButton ib_logout;
    String FILE_NAME = "";
    String user = "";
    String blu = "";
    String arrShift[] = {"1", "2", "3"};
    public static String factory = "";
    public static String ca = "";
    public static String may = "";
    public static String ngay = "";
    DatePickerDialog datePickerDialog;
    ArrayAdapter<String> adtListShift;
    private static boolean statusPop;
    boolean flag = false;
    String date, spntemp;
    private ArrayList<String> listShift, listTime;
    private ArrayAdapter ShiftAdapter, arrIdModel;
    public Spinner spnMay, spn_id, spr_shift,spr_shift2,spr_time1,spr_time2;
    ArrayList<String> arrTemp;
//    TextView id1;
    EditText[] edtl,edtnd;


    private ArrayList<QAM_TEMP_MODEL> tempmodel;
    ArrayList<String> listIDmodel,listempID;
    ArrayList<QAM_TEMP_MODEL> listID;
    ArrayList<TEMP> listTemp;
    EditText edtMay, edt_date, edt_dateCheck, edtCa, edtTcndT, edtTctgT, edtTcndP, edtTctgP, edtHT;
    //    TextView edtMac;
    SimpleDateFormat parseFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
    SimpleDateFormat sd = new SimpleDateFormat("HH:mm:ss");

    EditText edtHt1, edtHt2, edtHt3, edtHt4, edtHt5, edtHt6, edtHt7, edtHt8, edtHt9, edtHt10, edtHt11, edtHt12, edtHt13, edtHt14, edtHt15, edtHt16,
            edts1, edts2, edts3, edts4, edts5, edts6, edts7, edts8, edts9, edts10, edts11, edts12, edts13, edts14, edts15, edts16, edtSearchDate,
            edtNdt1, edtNdt2, edtNdt3, edtNdt4, edtNdt5, edtNdt6, edtNdt7, edtNdt8, edtNdt9, edtNdt10, edtNdt11, edtNdt12, edtNdt13, edtNdt14, edtNdt15, edtNdt16,
            edtNdd1, edtNdd2, edtNdd3, edtNdd4, edtNdd5, edtNdd6, edtNdd7, edtNdd8, edtNdd9, edtNdd10, edtNdd11, edtNdd12, edtNdd13, edtNdd14, edtNdd15, edtNdd16,
            edtTg1, edtTg2, edtTg3, edtTg4, edtTg5, edtTg6, edtTg7, edtTg8, edtTg9, edtTg10, edtTg11, edtTg12, edtTg13, edtTg14, edtTg15, edtTg16;

    private SimpleDateFormat mDateFormat = new SimpleDateFormat("MM/dd/yyyy");
    private void initPreferences() {

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mac1fvi2);
        final Intent intent = getIntent();
        final Bundle bundle = intent.getExtras();
//        Search_date.setText(mDateFormat.format(new Date()));

        if (bundle != null) {
            user = bundle.getString("user", "QAM");
            factory = bundle.getString("fac");
            blu = bundle.getString("May");
        }

        initPreferences();
        findID();
        listShift = new ArrayList<>();
//        listShift.add("0");
        listShift.add("±1");
        listShift.add("±2");
        listShift.add("±3");
        ShiftAdapter = new ArrayAdapter(mac1fvi2.this, R.layout.spn_layout, listShift);
        spr_shift.setAdapter(ShiftAdapter);
        spr_shift2.setAdapter(ShiftAdapter);
        ShiftAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        listTime = new ArrayList<>();
        listTime.add("0");
        listTime.add("±20");
        listTime.add("±30");
        ShiftAdapter = new ArrayAdapter(mac1fvi2.this, R.layout.spn_layout, listTime);
        spr_time1.setAdapter(ShiftAdapter);
        spr_time2.setAdapter(ShiftAdapter);
        ShiftAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);

        edtl = new EditText[]{ edtHt1, edtHt2, edtHt3, edtHt4, edtHt5, edtHt6, edtHt7, edtHt8, edtHt9, edtHt10, edtHt11, edtHt12, edtHt13, edtHt14, edtHt15, edtHt16,
                edts1, edts2, edts3, edts4, edts5, edts6, edts7, edts8, edts9, edts10, edts11, edts12, edts13, edts14, edts15, edts16, edtMay, edt_date, edt_dateCheck, edtCa, edtTcndT, edtTctgT, edtTcndP, edtTctgP,
                edtNdt1, edtNdt2, edtNdt3, edtNdt4, edtNdt5, edtNdt6, edtNdt7, edtNdt8, edtNdt9, edtNdt10, edtNdt11, edtNdt12, edtNdt13, edtNdt14, edtNdt15, edtNdt16,
                edtNdd1, edtNdd2, edtNdd3, edtNdd4, edtNdd5, edtNdd6, edtNdd7, edtNdd8, edtNdd9, edtNdd10, edtNdd11, edtNdd12, edtNdd13, edtNdd14, edtNdd15, edtNdd16,
                edtTg1, edtTg2, edtTg3, edtTg4, edtTg5, edtTg6, edtTg7, edtTg8, edtTg9, edtTg10, edtTg11, edtTg12, edtTg13, edtTg14, edtTg15, edtTg16
        };

        edtnd = new EditText[]{
                edtNdt1, edtNdt2, edtNdt3, edtNdt4, edtNdt5, edtNdt6, edtNdt7, edtNdt8, edtNdt9, edtNdt10, edtNdt11, edtNdt12, edtNdt13, edtNdt14, edtNdt15, edtNdt16,
                edtNdd1, edtNdd2, edtNdd3, edtNdd4, edtNdd5, edtNdd6, edtNdd7, edtNdd8, edtNdd9, edtNdd10, edtNdd11, edtNdd12, edtNdd13, edtNdd14, edtNdd15, edtNdd16

        };

        getKeyputValue();
        savaDaTa();
        createfolder();
        DeteleFileBefore7days();

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mac1fvi2.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btn_step5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(mac1fvi2.this, pdfActivity.class);
                myIntent.putExtras(bundle);
                mac1fvi2.this.startActivity(myIntent);
            }
        });


        btn_part6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(mac1fvi2.this,checkTempIP.class);
                intent1.putExtras(bundle);
                mac1fvi2.this.startActivity(intent1);
            }
        });



        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(mac1fvi2.this);
                b.setTitle("Xác nhận");
                b.setMessage("Bạn có muốn xóa mã đợt này?");
                b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            CallApi.deleteID(factory, spn_id.getSelectedItem().toString(), new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog al = b.create();
                al.show();
            }
        });

        edt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog(edt_date);
//                if (!edtCa.getText().toString().isEmpty() && !edt_date.getText().toString().equals("")) {
//                    Toast.makeText(getApplicationContext(), tempmodel.get(0).getMAY(), Toast.LENGTH_LONG).show();
                //   TempModel();
//                }
            }
        });
        btnEx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (checkBeforSave() == true) {
                        // saveTempModel();
                        createExcel();
                        sendmail_Temp();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        spn_id.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    getListQAM_TEMP_MODEL();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        edtTcndT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!edtTcndT.getText().toString().isEmpty()){
                    String ndtieuchuantrai = edtTcndT.getText().toString();
                    final int b1 = Integer.parseInt(ndtieuchuantrai);
                    spr_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String[] shift1 = spr_shift.getSelectedItem().toString().split("±");
                            String shift1a = shift1[1];
                            int a1 = Integer.parseInt(shift1a);
//                    int a1 = Integer.parseInt(spr_shift.getSelectedItem().toString());
                            c1 = b1 + a1;
                            d1 = b1 - a1;
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                    for (int i = 0; i < edtnd.length; i++){
                        final int temp = i;
                        edtnd[temp].addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                String abc1 = edtnd[temp].getText().toString();
                                if (!edtnd[temp].getText().toString().equals("")){
                                    int value1 = Integer.parseInt(abc1);
                                    if ( c1 < value1 || value1 < d1 ) {
                                        edtnd[temp].setBackgroundColor(Color.YELLOW);
                                    }
                                    else edtnd[temp].setBackgroundColor(Color.WHITE);
                                }else edtnd[temp].setBackgroundColor(Color.WHITE);
                            }
                            @Override
                            public void afterTextChanged(Editable s) {
                            }
                        });
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        spr_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] shift1 = spr_shift.getSelectedItem().toString().split("±");
                String shift1a = shift1[1];
                a1 = Integer.parseInt(shift1a);
                edtTcndT.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String ndtieuchuantrai = edtTcndT.getText().toString();
                        final int b1 = Integer.parseInt(ndtieuchuantrai);
                        c1 = b1 + a1;
                        d1 = b1 - a1;
                        for (int i = 0; i < edtnd.length; i++){
                            final int temp = i;
                            edtnd[temp].addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                }
                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    String abc1 = edtnd[temp].getText().toString();
                                    if (!edtnd[temp].getText().toString().equals("")){
                                        int value1 = Integer.parseInt(abc1);
                                        if ( c1 < value1 || value1 < d1 ) {
                                            edtnd[temp].setBackgroundColor(Color.YELLOW);
                                        }
                                        else edtnd[temp].setBackgroundColor(Color.WHITE);
                                    }else edtnd[temp].setBackgroundColor(Color.WHITE);
                                }
                                @Override
                                public void afterTextChanged(Editable s) {
                                }
                            });
                        }
                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getIdModel();
            }
        });



        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clear();
            }
        });

        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(mac1fvi2.this, mac1fvi2.class);
                myIntent.putExtras(bundle);
                mac1fvi2.this.startActivity(myIntent);
            }
        });

        edtTcndT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        spr_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });



        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
                try {
                    if (checkBeforSave() == true) {
                        final QAM_TEMP_MODEL qtm = new QAM_TEMP_MODEL();
                        saveTempModel(qtm);
                                //sendmail
                                try {
                                    CallApi.update_tempmodel(factory, qtm,spn_id.getSelectedItem().toString(), new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();

                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                ToastCustom.message(mac1fvi2.this, "Lưu Thành Công!", Color.GREEN);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getIdModel() {//String date, String ca, String may

        try {
            CallApi.getIdModel(factory, edt_date.getText().toString(), edtCa.getText().toString(), edtMay.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    IDmodel iDmodel = gson.fromJson(response.toString(), IDmodel.class);
                    listIDmodel = iDmodel.getItems() == null ? new ArrayList<String>() : iDmodel.getItems();
                    arrIdModel = new ArrayAdapter(mac1fvi2.this, R.layout.spn_layout, listIDmodel);
                    arrIdModel.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                    spn_id.setAdapter(arrIdModel);


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void getListQAM_TEMP_MODEL() {//String date, String ca, String may
        try {

            CallApi.getListQAM_TEMP_MODEL(factory, spn_id.getSelectedItem().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    listQtm listQtm = gson.fromJson(response.toString(), mac1fvi2.listQtm.class);
                    listID = listQtm.getItems() == null ? new ArrayList<QAM_TEMP_MODEL>() : listQtm.getItems();
                    if (listID.size() != 0) {
                        edtMay.setText(listID.get(0).getMAY());
                        edt_date.setText(edt_date.getText().toString());
                        edtCa.setText(listID.get(0).getCA());
                        edt_dateCheck.setText(listID.get(0).getCHECK_HOURS());
                        edtTctgP.setText(listID.get(0).getTCTGP());
                        edtTctgT.setText(listID.get(0).getTCTGT());
                        edtTcndP.setText(listID.get(0).getNDTCP());
                        edtTcndT.setText(listID.get(0).getNDTCT());
                        edtHt1.setText(listID.get(0).getHT1());
                        edtHt2.setText(listID.get(0).getHT2());
                        edtHt3.setText(listID.get(0).getHT3());
                        edtHt4.setText(listID.get(0).getHT4());
                        edtHt5.setText(listID.get(0).getHT5());
                        edtHt6.setText(listID.get(0).getHT6());
                        edtHt7.setText(listID.get(0).getHT7());
                        edtHt8.setText(listID.get(0).getHT8());
                        edtHt9.setText(listID.get(0).getHT9());
                        edtHt10.setText(listID.get(0).getHT10());
                        edtHt11.setText(listID.get(0).getHT11());
                        edtHt12.setText(listID.get(0).getHT12());
                        edtHt13.setText(listID.get(0).getHT13());
                        edtHt14.setText(listID.get(0).getHT14());
                        edtHt15.setText(listID.get(0).getHT15());
                        edtHt16.setText(listID.get(0).getHT16());
                        edts1.setText(listID.get(0).getSM1T());
                        edts2.setText(listID.get(0).getSM1D());
                        edts3.setText(listID.get(0).getSM2T());
                        edts4.setText(listID.get(0).getSM2D());
                        edts5.setText(listID.get(0).getSM3T());
                        edts6.setText(listID.get(0).getSM3D());
                        edts7.setText(listID.get(0).getSM4T());
                        edts8.setText(listID.get(0).getSM4D());
                        edts9.setText(listID.get(0).getSM5T());
                        edts10.setText(listID.get(0).getSM5D());
                        edts11.setText(listID.get(0).getSM6T());
                        edts12.setText(listID.get(0).getSM6D());
                        edts13.setText(listID.get(0).getSM7T());
                        edts14.setText(listID.get(0).getSM7D());
                        edts15.setText(listID.get(0).getSM8T());
                        edts16.setText(listID.get(0).getSM8D());
                        edtNdt1.setText(listID.get(0).getNDM1TT());
                        edtNdt2.setText(listID.get(0).getNDM1TP());
                        edtNdd1.setText(listID.get(0).getNDM1DT());
                        edtNdd2.setText(listID.get(0).getNDM1DP());
                        edtNdt3.setText(listID.get(0).getNDM2TT());
                        edtNdt4.setText(listID.get(0).getNDM2TP());
                        edtNdd3.setText(listID.get(0).getNDM2DT());
                        edtNdd4.setText(listID.get(0).getNDM2DP());
                        edtNdt5.setText(listID.get(0).getNDM3TT());
                        edtNdt6.setText(listID.get(0).getNDM3TP());
                        edtNdd5.setText(listID.get(0).getNDM3DT());
                        edtNdd6.setText(listID.get(0).getNDM3DP());
                        edtNdt7.setText(listID.get(0).getNDM4TT());
                        edtNdt8.setText(listID.get(0).getNDM4TP());
                        edtNdd7.setText(listID.get(0).getNDM4DT());
                        edtNdd8.setText(listID.get(0).getNDM4DP());
                        edtNdt9.setText(listID.get(0).getNDM5TT());
                        edtNdt10.setText(listID.get(0).getNDM5TP());
                        edtNdd9.setText(listID.get(0).getNDM5DT());
                        edtNdd10.setText(listID.get(0).getNDM5DP());
                        edtNdt11.setText(listID.get(0).getNDM6TT());
                        edtNdt12.setText(listID.get(0).getNDM6TP());
                        edtNdd11.setText(listID.get(0).getNDM6DT());
                        edtNdd12.setText(listID.get(0).getNDM6DP());
                        edtNdt13.setText(listID.get(0).getNDM7TT());
                        edtNdt14.setText(listID.get(0).getNDM7TP());
                        edtNdd13.setText(listID.get(0).getNDM7DT());
                        edtNdd14.setText(listID.get(0).getNDM7DP());
                        edtNdt15.setText(listID.get(0).getNDM8TT());
                        edtNdt16.setText(listID.get(0).getNDM8TP());
                        edtNdd15.setText(listID.get(0).getNDM8DT());
                        edtNdd16.setText(listID.get(0).getNDM8DP());
                        edtTg1.setText(listID.get(0).getSET_TIMEM1T());
                        edtTg2.setText(listID.get(0).getSET_TIMEM1D());
                        edtTg3.setText(listID.get(0).getSET_TIMEM2T());
                        edtTg4.setText(listID.get(0).getSET_TIMEM2D());
                        edtTg5.setText(listID.get(0).getSET_TIMEM3T());
                        edtTg6.setText(listID.get(0).getSET_TIMEM3D());
                        edtTg7.setText(listID.get(0).getSET_TIMEM4T());
                        edtTg8.setText(listID.get(0).getSET_TIMEM4D());
                        edtTg9.setText(listID.get(0).getSET_TIMEM5T());
                        edtTg10.setText(listID.get(0).getSET_TIMEM5D());
                        edtTg11.setText(listID.get(0).getSET_TIMEM6T());
                        edtTg12.setText(listID.get(0).getSET_TIMEM6D());
                        edtTg13.setText(listID.get(0).getSET_TIMEM7T());
                        edtTg14.setText(listID.get(0).getSET_TIMEM7D());
                        edtTg15.setText(listID.get(0).getSET_TIMEM8T());
                        edtTg16.setText(listID.get(0).getSET_TIMEM8D());
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });

            System.out.println("da chay ham");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    String DEST = Environment.getExternalStorageDirectory().toString() + "/TEMP_EXCEL/";

    public void datePickerDialog(final EditText edt_date) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(mac1fvi2.this,
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int monthcurrent = monthOfYear + 1;
                        String month = "";
                        String day = "";
                        if (dayOfMonth > 9) {
                            day = String.valueOf(dayOfMonth);

                        } else {
                            day = "0" + String.valueOf(dayOfMonth);
                        }
                        if (monthcurrent > 9) {
                            month = String.valueOf(monthcurrent);
                        } else {
                            month = "0" + String.valueOf(monthcurrent);

                        }
                        edt_date.setText(month + "/" + day + "/" + year);
                        date = edt_date.getText().toString();
                    }
                }, mYear, mMonth, mDay);


        datePickerDialog.show();
    }

    public void saveTempModel(QAM_TEMP_MODEL qtm) {
        qtm.setMAY(edtMay.getText().toString());
        qtm.setCA(edtCa.getText().toString());
        String date = edt_date.getText().toString();
        qtm.setID(date.substring(date.length() - 2) + date.substring(0, 2) + date.substring(3, 5));
        qtm.setCHECK_DATE(edt_date.getText().toString());
        qtm.setCHECK_HOURS(edt_dateCheck.getText().toString());
        qtm.setTCTGP(edtTctgP.getText().toString());
        qtm.setTCTGT(edtTctgT.getText().toString());
        qtm.setNDTCP(edtTcndP.getText().toString());
        qtm.setNDTCT(edtTcndT.getText().toString());
        qtm.setHT1(edtHt1.getText().toString().equals("") ? "" : edtHt1.getText().toString());
        qtm.setHT2(edtHt2.getText().toString().equals("") ? "" : edtHt2.getText().toString());
        qtm.setHT3(edtHt3.getText().toString().equals("") ? "" : edtHt3.getText().toString());
        qtm.setHT4(edtHt4.getText().toString().equals("") ? "" : edtHt4.getText().toString());
        qtm.setHT5(edtHt5.getText().toString().equals("") ? "" : edtHt5.getText().toString());
        qtm.setHT6(edtHt6.getText().toString().equals("") ? "" : edtHt6.getText().toString());
        qtm.setHT7(edtHt7.getText().toString().equals("") ? "" : edtHt7.getText().toString());
        qtm.setHT8(edtHt8.getText().toString().equals("") ? "" : edtHt8.getText().toString());
        qtm.setHT9(edtHt9.getText().toString().equals("") ? "" : edtHt9.getText().toString());
        qtm.setHT10(edtHt10.getText().toString().equals("") ? "" : edtHt10.getText().toString());
        qtm.setHT11(edtHt11.getText().toString().equals("") ? "" : edtHt11.getText().toString());
        qtm.setHT12(edtHt12.getText().toString().equals("") ? "" : edtHt12.getText().toString());
        qtm.setHT13(edtHt13.getText().toString().equals("") ? "" : edtHt13.getText().toString());
        qtm.setHT14(edtHt14.getText().toString().equals("") ? "" : edtHt14.getText().toString());
        qtm.setHT15(edtHt15.getText().toString().equals("") ? "" : edtHt15.getText().toString());
        qtm.setHT16(edtHt16.getText().toString().equals("") ? "" : edtHt16.getText().toString());
        qtm.setSM1T(edts1.getText().toString().equals("") ? "" : edts1.getText().toString());
        qtm.setSM1D(edts2.getText().toString().equals("") ? "" : edts2.getText().toString());
        qtm.setSM2T(edts3.getText().toString().equals("") ? "" : edts3.getText().toString());
        qtm.setSM2D(edts4.getText().toString().equals("") ? "" : edts4.getText().toString());
        qtm.setSM3T(edts5.getText().toString().equals("") ? "" : edts5.getText().toString());
        qtm.setSM3D(edts6.getText().toString().equals("") ? "" : edts6.getText().toString());
        qtm.setSM4T(edts7.getText().toString().equals("") ? "" : edts7.getText().toString());
        qtm.setSM4D(edts8.getText().toString().equals("") ? "" : edts8.getText().toString());
        qtm.setSM5T(edts9.getText().toString().equals("") ? "" : edts9.getText().toString());
        qtm.setSM5D(edts10.getText().toString().equals("") ? "" : edts10.getText().toString());
        qtm.setSM6T(edts11.getText().toString().equals("") ? "" : edts11.getText().toString());
        qtm.setSM6D(edts12.getText().toString().equals("") ? "" : edts12.getText().toString());
        qtm.setSM7T(edts13.getText().toString().equals("") ? "" : edts13.getText().toString());
        qtm.setSM7D(edts14.getText().toString().equals("") ? "" : edts14.getText().toString());
        qtm.setSM8T(edts15.getText().toString().equals("") ? "" : edts15.getText().toString());
        qtm.setSM8D(edts16.getText().toString().equals("") ? "" : edts16.getText().toString());
        qtm.setNDM1TT(edtNdt1.getText().toString().equals("") ? "" : edtNdt1.getText().toString());
        qtm.setNDM1TP(edtNdt2.getText().toString().equals("") ? "" : edtNdt2.getText().toString());
        qtm.setNDM1DT(edtNdd1.getText().toString().equals("") ? "" : edtNdd1.getText().toString());
        qtm.setNDM1DP(edtNdd2.getText().toString().equals("") ? "" : edtNdd2.getText().toString());
        qtm.setNDM2TT(edtNdt3.getText().toString().equals("") ? "" : edtNdt3.getText().toString());
        qtm.setNDM2TP(edtNdt4.getText().toString().equals("") ? "" : edtNdt4.getText().toString());
        qtm.setNDM2DT(edtNdd3.getText().toString().equals("") ? "" : edtNdd3.getText().toString());
        qtm.setNDM2DP(edtNdd4.getText().toString().equals("") ? "" : edtNdd4.getText().toString());
        qtm.setNDM3TT(edtNdt5.getText().toString().equals("") ? "" : edtNdt5.getText().toString());
        qtm.setNDM3TP(edtNdt6.getText().toString().equals("") ? "" : edtNdt6.getText().toString());
        qtm.setNDM3DT(edtNdd5.getText().toString().equals("") ? "" : edtNdd5.getText().toString());
        qtm.setNDM3DP(edtNdd6.getText().toString().equals("") ? "" : edtNdd6.getText().toString());
        qtm.setNDM4TT(edtNdt7.getText().toString().equals("") ? "" : edtNdt7.getText().toString());
        qtm.setNDM4TP(edtNdt8.getText().toString().equals("") ? "" : edtNdt8.getText().toString());
        qtm.setNDM4DT(edtNdd7.getText().toString().equals("") ? "" : edtNdd7.getText().toString());
        qtm.setNDM4DP(edtNdd8.getText().toString().equals("") ? "" : edtNdd8.getText().toString());
        qtm.setNDM5TT(edtNdt9.getText().toString().equals("") ? "" : edtNdt9.getText().toString());
        qtm.setNDM5TP(edtNdt10.getText().toString().equals("") ? "" : edtNdt10.getText().toString());
        qtm.setNDM5DT(edtNdd9.getText().toString().equals("") ? "" : edtNdd9.getText().toString());
        qtm.setNDM5DP(edtNdd10.getText().toString().equals("") ? "" : edtNdd10.getText().toString());
        qtm.setNDM6TT(edtNdt11.getText().toString().equals("") ? "" : edtNdt11.getText().toString());
        qtm.setNDM6TP(edtNdt12.getText().toString().equals("") ? "" : edtNdt12.getText().toString());
        qtm.setNDM6DT(edtNdd11.getText().toString().equals("") ? "" : edtNdd11.getText().toString());
        qtm.setNDM6DP(edtNdd12.getText().toString().equals("") ? "" : edtNdd12.getText().toString());
        qtm.setNDM7TT(edtNdt13.getText().toString().equals("") ? "" : edtNdt13.getText().toString());
        qtm.setNDM7TP(edtNdt14.getText().toString().equals("") ? "" : edtNdt14.getText().toString());
        qtm.setNDM7DT(edtNdd13.getText().toString().equals("") ? "" : edtNdd13.getText().toString());
        qtm.setNDM7DP(edtNdd14.getText().toString().equals("") ? "" : edtNdd14.getText().toString());
        qtm.setNDM8TT(edtNdt15.getText().toString().equals("") ? "" : edtNdt15.getText().toString());
        qtm.setNDM8TP(edtNdt16.getText().toString().equals("") ? "" : edtNdt16.getText().toString());
        qtm.setNDM8DT(edtNdd15.getText().toString().equals("") ? "" : edtNdd15.getText().toString());
        qtm.setNDM8DP(edtNdd16.getText().toString().equals("") ? "" : edtNdd16.getText().toString());
        qtm.setSET_TIMEM1T(edtTg1.getText().toString().equals("") ? "" : edtTg1.getText().toString());
        qtm.setSET_TIMEM1D(edtTg2.getText().toString().equals("") ? "" : edtTg2.getText().toString());
        qtm.setSET_TIMEM2T(edtTg3.getText().toString().equals("") ? "" : edtTg3.getText().toString());
        qtm.setSET_TIMEM2D(edtTg4.getText().toString().equals("") ? "" : edtTg4.getText().toString());
        qtm.setSET_TIMEM3T(edtTg5.getText().toString().equals("") ? "" : edtTg5.getText().toString());
        qtm.setSET_TIMEM3D(edtTg6.getText().toString().equals("") ? "" : edtTg6.getText().toString());
        qtm.setSET_TIMEM4T(edtTg7.getText().toString().equals("") ? "" : edtTg7.getText().toString());
        qtm.setSET_TIMEM4D(edtTg8.getText().toString().equals("") ? "" : edtTg8.getText().toString());
        qtm.setSET_TIMEM5T(edtTg9.getText().toString().equals("") ? "" : edtTg9.getText().toString());
        qtm.setSET_TIMEM5D(edtTg10.getText().toString().equals("") ? "" : edtTg10.getText().toString());
        qtm.setSET_TIMEM6T(edtTg11.getText().toString().equals("") ? "" : edtTg11.getText().toString());
        qtm.setSET_TIMEM6D(edtTg12.getText().toString().equals("") ? "" : edtTg12.getText().toString());
        qtm.setSET_TIMEM7T(edtTg13.getText().toString().equals("") ? "" : edtTg13.getText().toString());
        qtm.setSET_TIMEM7D(edtTg14.getText().toString().equals("") ? "" : edtTg14.getText().toString());
        qtm.setSET_TIMEM8T(edtTg15.getText().toString().equals("") ? "" : edtTg15.getText().toString());
        qtm.setSET_TIMEM8D(edtTg16.getText().toString().equals("") ? "" : edtTg16.getText().toString());

        try {
            CallApi.SaveTempModel_FVI2(factory, qtm, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public boolean checkBeforSearch() {

//        getIdModel();
        boolean doCheck = true;
        if (spn_id.getSelectedItem().toString().length() ==0) {
            ToastCustom.message(getApplicationContext(), "ID không thể để trống!", Color.RED);
            doCheck =  false;
        }
        return doCheck;
    }

    public boolean checkBeforSave() {

        if (edtMay.getText().toString().equals("")) {
            ToastCustom.message(getApplicationContext(), "Ca không thể để trống!", Color.RED);
            return false;
        }

        if (edtCa.getText().toString().equals("")) {
            ToastCustom.message(getApplicationContext(), "Ca không thể để trống!", Color.RED);
            return false;
        }
        if (edt_date.getText().toString().equals("")) {
            ToastCustom.message(getApplicationContext(), "Thời gian không thể để trống!", Color.RED);
            return false;
        }
        if (edt_dateCheck.getText().toString().equals("")) {
            ToastCustom.message(getApplicationContext(), "Giờ kiểm tra không thể để trống!", Color.RED);
            return false;
        }
        if (edtTcndP.getText().toString().equals("")) {
            ToastCustom.message(getApplicationContext(), "Tiêu chuẩn nhiệt độ Phải không thể để trống!", Color.RED);
            return false;
        }
        if (edtTcndT.getText().toString().equals("")) {
            ToastCustom.message(getApplicationContext(), "Tiêu chuẩn nhiệt độ Trái không thể để trống!", Color.RED);
            return false;
        }
        if (edtTctgP.getText().toString().equals("")) {
            ToastCustom.message(getApplicationContext(), "Tiêu chuẩn Thời gian Phải không thể để trống!", Color.RED);
            return false;
        }
        if (edtTctgT.getText().toString().equals("")) {
            ToastCustom.message(getApplicationContext(), "Tiêu chuẩn Thời gian Trái không thể để trống!", Color.RED);
            return false;
        }

//        String st = edtTcndT.getText().toString();
//        int ndtct = Integer.parseInt(st);
//        if(ndtct >=165 && ndtct<=178){
//
//        }else{
//            ToastCustom.message(getApplicationContext(), "Nhiệt độ Trái chỉ trong khoản 165 - 176!!", Color.RED);
//            return false;
//        }
//        String sp = edtTcndP.getText().toString();
//        int ndtcP = Integer.parseInt(sp);
//        if(ndtcP >=165 && ndtcP<=178){
//        }else{
//            ToastCustom.message(getApplicationContext(), "Nhiệt độ Phải chỉ trong khoản 165 - 176!", Color.RED);
//            return false;
//        }
        return true;


    }

    public void createfolder() {
        File folder = new File(Environment.getExternalStorageDirectory() + "/TEMP_EXCEL");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            // Do something on success
        } else {
            // Do something else on failure
        }
    }

    public void DeteleFileBefore7days() {

        Calendar time = Calendar.getInstance();
        time.add(Calendar.DAY_OF_YEAR, -4);
        String path = Environment.getExternalStorageDirectory().toString() + "/TEMP_EXCEL";
        File directory = new File(path);
        File[] files = directory.listFiles();
        for (int i = 0; i < files.length; i++) {
            File file = new File(Environment.getExternalStorageDirectory().toString() + "/TEMP_EXCEL/", files[i].getName());
            Date lastModified1 = new Date(file.lastModified());
            if (lastModified1.before(time.getTime())) {
                boolean deleted = file.delete();
            }
        }
    }

    public void createExcel() throws FileNotFoundException {
        AssetManager am = getApplicationContext().getAssets();
        FileOutputStream outFile = null;
        InputStream inputStream = null;
        Workbook workbook = null;

        try {
            FILE_NAME = "Ca " + edtCa.getText().toString() + "__" + "May " + edtMay.getText().toString() + "__" + "Ngay " +edt_date.getText().toString().replaceAll("/", "-");
            File ex = new File(Environment.getExternalStorageDirectory().toString() +
                    FILE_NAME + ".xls");
            if (!ex.exists()) {

                inputStream = am.open("tempmodel.xls");
                workbook = getWorkbook(inputStream, "tempmodel.xls");

            } else {
                inputStream = new FileInputStream(ex);
                workbook = getWorkbook(inputStream, FILE_NAME + ".xls");
            }


            Sheet sheet = workbook.getSheetAt(0);
            Cell cell;
            Row row;
            //int rownum=4;

            CellStyle style = createStyleForTitle(workbook);

            row = sheet.getRow(1);
            cell = row.getCell(1);
            cell.setCellValue(edtMay.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(4);
            cell.setCellValue(edtCa.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(9);
            cell.setCellValue(edt_date.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(15);
            cell.setCellValue(edt_dateCheck.getText().toString());
            cell.setCellStyle(style);

            row = sheet.getRow(2);
            cell = row.getCell(2);
            cell.setCellValue(edtTcndT.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(3);
            cell.setCellValue(spr_shift.getSelectedItem().toString());
            cell.setCellStyle(style);

            cell = row.getCell(7);
            cell.setCellValue(edtTctgT.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(8);
            cell.setCellValue(spr_time1.getSelectedItem().toString());
            cell.setCellStyle(style);

            cell = row.getCell(12);
            cell.setCellValue(edtTcndP.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(13);
            cell.setCellValue(spr_shift2.getSelectedItem().toString());
            cell.setCellStyle(style);

            cell = row.getCell(16);
            cell.setCellValue(edtTctgP.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(17);
            cell.setCellValue(spr_time2.getSelectedItem().toString());
            cell.setCellStyle(style);
//hinh the
            row = sheet.getRow(5);
            cell = row.getCell(2);
            cell.setCellValue(edtHt1.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(3);
            cell.setCellValue(edtHt2.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(4);
            cell.setCellValue(edtHt3.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(5);
            cell.setCellValue(edtHt4.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(6);
            cell.setCellValue(edtHt5.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(7);
            cell.setCellValue(edtHt6.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(8);
            cell.setCellValue(edtHt7.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(9);
            cell.setCellValue(edtHt8.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(10);
            cell.setCellValue(edtHt9.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(11);
            cell.setCellValue(edtHt10.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(12);
            cell.setCellValue(edtHt11.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(13);
            cell.setCellValue(edtHt12.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(14);
            cell.setCellValue(edtHt13.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(15);
            cell.setCellValue(edtHt14.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(16);
            cell.setCellValue(edtHt15.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(17);
            cell.setCellValue(edtHt16.getText().toString());
            cell.setCellStyle(style);

//            row = sheet.getRow(5);
//            cell = row.getCell(2);
//            cell.setCellValue(edtHT.getText().toString());
//            sheet.addMergedRegion(new CellRangeAddress(5, 5, 2, 17));
//            cell.setCellStyle(style);


//size
            row = sheet.getRow(6);
            cell = row.getCell(2);
            cell.setCellValue(edts1.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(3);
            cell.setCellValue(edts2.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(4);
            cell.setCellValue(edts3.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(5);
            cell.setCellValue(edts4.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(6);
            cell.setCellValue(edts5.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(7);
            cell.setCellValue(edts6.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(8);
            cell.setCellValue(edts7.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(9);
            cell.setCellValue(edts8.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(10);
            cell.setCellValue(edts9.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(11);
            cell.setCellValue(edts10.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(12);
            cell.setCellValue(edts11.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(13);
            cell.setCellValue(edts12.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(14);
            cell.setCellValue(edts13.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(15);
            cell.setCellValue(edts14.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(16);
            cell.setCellValue(edts15.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(17);
            cell.setCellValue(edts16.getText().toString());
            cell.setCellStyle(style);

            row = sheet.getRow(7);
            cell = row.getCell(2);
            cell.setCellValue(edtNdt1.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(3);
            cell.setCellValue(edtNdt2.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(4);
            cell.setCellValue(edtNdt3.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(5);
            cell.setCellValue(edtNdt4.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(6);
            cell.setCellValue(edtNdt5.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(7);
            cell.setCellValue(edtNdt6.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(8);
            cell.setCellValue(edtNdt7.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(9);
            cell.setCellValue(edtNdt8.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(10);
            cell.setCellValue(edtNdt9.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(11);
            cell.setCellValue(edtNdt10.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(12);
            cell.setCellValue(edtNdt11.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(13);
            cell.setCellValue(edtNdt12.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(14);
            cell.setCellValue(edtNdt13.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(15);
            cell.setCellValue(edtNdt14.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(16);
            cell.setCellValue(edtNdt15.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(17);
            cell.setCellValue(edtNdt16.getText().toString());
            cell.setCellStyle(style);

            row = sheet.getRow(8);
            cell = row.getCell(2);
            cell.setCellValue(edtNdd1.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(3);
            cell.setCellValue(edtNdd2.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(4);
            cell.setCellValue(edtNdd3.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(5);
            cell.setCellValue(edtNdd4.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(6);
            cell.setCellValue(edtNdd5.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(7);
            cell.setCellValue(edtNdd6.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(8);
            cell.setCellValue(edtNdd7.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(9);
            cell.setCellValue(edtNdd8.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(10);
            cell.setCellValue(edtNdd9.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(11);
            cell.setCellValue(edtNdd10.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(12);
            cell.setCellValue(edtNdd11.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(13);
            cell.setCellValue(edtNdd12.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(14);
            cell.setCellValue(edtNdd13.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(15);
            cell.setCellValue(edtNdd14.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(16);
            cell.setCellValue(edtNdd15.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(17);
            cell.setCellValue(edtNdd16.getText().toString());
            cell.setCellStyle(style);

            row = sheet.getRow(9);
            cell = row.getCell(2);
            cell.setCellValue(edtTg1.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(3);
            cell.setCellValue(edtTg2.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(4);
            cell.setCellValue(edtTg3.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(5);
            cell.setCellValue(edtTg4.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(6);
            cell.setCellValue(edtTg5.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(7);
            cell.setCellValue(edtTg6.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(8);
            cell.setCellValue(edtTg7.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(9);
            cell.setCellValue(edtTg8.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(10);
            cell.setCellValue(edtTg9.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(11);
            cell.setCellValue(edtTg10.getText().toString());
            cell.setCellStyle(style);


            cell = row.getCell(12);
            cell.setCellValue(edtTg11.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(13);
            cell.setCellValue(edtTg12.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(14);
            cell.setCellValue(edtTg15.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(15);
            cell.setCellValue(edtTg14.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(16);
            cell.setCellValue(edtTg15.getText().toString());
            cell.setCellStyle(style);

            cell = row.getCell(17);
            cell.setCellValue(edtTg16.getText().toString());
            cell.setCellStyle(style);

            File check = new File(Environment.getExternalStorageDirectory().toString() +
                    "/TEMP_EXCEL/" + FILE_NAME + ".xls");
            if (check.exists()) {
                check.delete();
            }
            outFile = new FileOutputStream(check);
            workbook.write(outFile);

            System.out.println("Created file: " + check.getAbsolutePath());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outFile != null) {
                try {
                    outFile.flush();
                    outFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
        Workbook workbook = null;
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }

    private static CellStyle createStyleForTitle(Workbook workbook) {

        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        // Font Color
        font.setColor(IndexedColors.BLACK.getIndex());
        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setBorderLeft((short) 1);
        style.setBorderRight((short) 1);
        style.setBorderTop((short) 1);
        style.setBorderBottom((short) 1);
        style.setLocked(true);

        return style;
    }

    public void sendmail_Temp() {
        AlertDialog.Builder b = new AlertDialog.Builder(mac1fvi2.this);
        b.setTitle("Xác nhận");
        b.setMessage("Bạn có muốn gửi báo biểu về Mail?");
        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //sendmail
                QAM_TEMP_MODEL model = new QAM_TEMP_MODEL();
                String name1 = DEST + FILE_NAME + ".xls";
                SendMail sendmail = new SendMail();
                sendmail.sendmail_TempModel(factory, name1, model);
                model.setCA(edtCa.getText().toString());
                model.setMAY(edtMay.getText().toString());
                model.setCHECK_DATE(edt_date.getText().toString());
                ToastCustom.message(mac1fvi2.this, "Gửi thành công!", Color.GREEN);
            }
        });
        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog al = b.create();
        al.show();
    }


    // sử lý code btn save
    public void findID(){
        btn_part6 =findViewById(R.id.btn_part6);
        btnCancle = findViewById(R.id.btnCancle);
//        btnNote = findViewById(R.id.btnNote);
        spn_id = findViewById(R.id.spn_id);
        spr_shift=findViewById(R.id.spr_shift);
        spr_shift2=findViewById(R.id.spr_shift2);
        spr_time1=findViewById(R.id.spr_time1);
        spr_time2=findViewById(R.id.spr_time2);
//        id1 = findViewById(R.id.id1);
        btnClear = findViewById(R.id.btnClear);
        btnSave = findViewById(R.id.btnSave);
        btnEdit = findViewById(R.id.btnedit);
        btnSearch = findViewById(R.id.btnSearch);
        btnDelete = findViewById(R.id.btnDelete);

        ib_logout = findViewById(R.id.ib_logout);
        btn_step5 = findViewById(R.id.btn_step5);
        edt_date = findViewById(R.id.edt_date);
        edt_dateCheck = findViewById(R.id.edt_dateCheck);
        edtMay = findViewById(R.id.edtMay);
        adtListShift = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrShift);
        edtCa = findViewById(R.id.edtCa);
        edtTcndP = findViewById(R.id.edtTcndP);
        edtTcndT = findViewById(R.id.edtTcndT);
        edtTctgT = findViewById(R.id.edtTctgT);
        edtTctgP = findViewById(R.id.edtTctgP);
        edtHt1 = findViewById(R.id.edtHt1);
        edtHt2 = findViewById(R.id.edtHt2);
        edtHt3 = findViewById(R.id.edtHt3);
        edtHt4 = findViewById(R.id.edtHt4);
        edtHt5 = findViewById(R.id.edtHt5);
        edtHt6 = findViewById(R.id.edtHt6);
        edtHt7 = findViewById(R.id.edtHt7);
        edtHt8 = findViewById(R.id.edtHt8);
        edtHt9 = findViewById(R.id.edtHt9);
        edtHt10 = findViewById(R.id.edtHt10);
        edtHt11 = findViewById(R.id.edtHt11);
        edtHt12 = findViewById(R.id.edtHt12);
        edtHt13 = findViewById(R.id.edtHt13);
        edtHt14 = findViewById(R.id.edtHt14);
        edtHt15 = findViewById(R.id.edtHt15);
        edtHt16 = findViewById(R.id.edtHt16);


//        edtHT = findViewById(R.id.edtHT);
        edts1 = findViewById(R.id.edts1);
        edts2 = findViewById(R.id.edts2);
        edts3 = findViewById(R.id.edts3);
        edts4 = findViewById(R.id.edts4);
        edts5 = findViewById(R.id.edts5);
        edts6 = findViewById(R.id.edts6);
        edts7 = findViewById(R.id.edts7);
        edts8 = findViewById(R.id.edts8);
        edts9 = findViewById(R.id.edts9);
        edts10 = findViewById(R.id.edts10);
        edts11 = findViewById(R.id.edts11);
        edts12 = findViewById(R.id.edts12);
        edts13 = findViewById(R.id.edts13);
        edts14 = findViewById(R.id.edts14);
        edts15 = findViewById(R.id.edts15);
        edts16 = findViewById(R.id.edts16);


        temp = new TEMP();
        edtNdt1 = findViewById(R.id.edtNdt1);
        edtNdt2 = findViewById(R.id.edtNdt2);
        edtNdt3 = findViewById(R.id.edtNdt3);
        edtNdt4 = findViewById(R.id.edtNdt4);
        edtNdt5 = findViewById(R.id.edtNdt5);
        edtNdt6 = findViewById(R.id.edtNdt6);
        edtNdt7 = findViewById(R.id.edtNdt7);
        edtNdt8 = findViewById(R.id.edtNdt8);
        edtNdt9 = findViewById(R.id.edtNdt9);
        edtNdt10 = findViewById(R.id.edtNdt10);
        edtNdt11 = findViewById(R.id.edtNdt11);
        edtNdt12 = findViewById(R.id.edtNdt12);
        edtNdt13 = findViewById(R.id.edtNdt13);
        edtNdt14 = findViewById(R.id.edtNdt14);
        edtNdt15 = findViewById(R.id.edtNdt15);
        edtNdt16 = findViewById(R.id.edtNdt16);

        edtNdd1 = findViewById(R.id.edtNdd1);
        edtNdd2 = findViewById(R.id.edtNdd2);
        edtNdd3 = findViewById(R.id.edtNdd3);
        edtNdd4 = findViewById(R.id.edtNdd4);
        edtNdd5 = findViewById(R.id.edtNdd5);
        edtNdd6 = findViewById(R.id.edtNdd6);
        edtNdd7 = findViewById(R.id.edtNdd7);
        edtNdd8 = findViewById(R.id.edtNdd8);
        edtNdd9 = findViewById(R.id.edtNdd9);
        edtNdd10 = findViewById(R.id.edtNdd10);
        edtNdd11 = findViewById(R.id.edtNdd11);
        edtNdd12 = findViewById(R.id.edtNdd12);
        edtNdd13 = findViewById(R.id.edtNdd13);
        edtNdd14 = findViewById(R.id.edtNdd14);
        edtNdd15 = findViewById(R.id.edtNdd15);
        edtNdd16 = findViewById(R.id.edtNdd16);

        edtTg1 = findViewById(R.id.edtTg1);
        edtTg2 = findViewById(R.id.edtTg2);
        edtTg3 = findViewById(R.id.edtTg3);
        edtTg4 = findViewById(R.id.edtTg4);
        edtTg5 = findViewById(R.id.edtTg5);
        edtTg6 = findViewById(R.id.edtTg6);
        edtTg7 = findViewById(R.id.edtTg7);
        edtTg8 = findViewById(R.id.edtTg8);
        edtTg9 = findViewById(R.id.edtTg9);
        edtTg10 = findViewById(R.id.edtTg10);
        edtTg11 = findViewById(R.id.edtTg11);
        edtTg12 = findViewById(R.id.edtTg12);
        edtTg13 = findViewById(R.id.edtTg13);
        edtTg14 = findViewById(R.id.edtTg14);
        edtTg15 = findViewById(R.id.edtTg15);
        edtTg16 = findViewById(R.id.edtTg16);

        btnEx = findViewById(R.id.btnEx);

    }
    public void getKeyputValue(){
        String May = sharedPreferences.getString("May", "");
        edtMay.setText(May);


        String CHECK_DATE = sharedPreferences.getString("check_date", "");
        edt_date.setText(CHECK_DATE);

        String Ca = sharedPreferences.getString("Ca", "");
        edtCa.setText(Ca);


        String CHECK_HOURS = sharedPreferences.getString("check_hours", "");
        edt_dateCheck.setText(CHECK_HOURS);

        String TCTGT = sharedPreferences.getString("TCTGT", "");
        edtTctgT.setText(TCTGT);

        String TCTGP = sharedPreferences.getString("TCTGP", "");
        edtTctgP.setText(TCTGP);

        String NDTCT = sharedPreferences.getString("NDTCT", "");
        edtTcndT.setText(NDTCT);

        String NDTCP = sharedPreferences.getString("NDTCP", "");
        edtTcndP.setText(NDTCP);


        String Ht1 = sharedPreferences.getString("Ht1", "");
        edtHt1.setText(Ht1);
        String Ht2 = sharedPreferences.getString("Ht2", "");
        edtHt2.setText(Ht2);
        String Ht3 = sharedPreferences.getString("Ht3", "");
        edtHt3.setText(Ht3);
        String Ht4 = sharedPreferences.getString("Ht4", "");
        edtHt4.setText(Ht4);
        String Ht5 = sharedPreferences.getString("Ht5", "");
        edtHt5.setText(Ht5);
        String Ht6 = sharedPreferences.getString("Ht6", "");
        edtHt6.setText(Ht6);
        String Ht7 = sharedPreferences.getString("Ht7", "");
        edtHt7.setText(Ht7);
        String Ht8 = sharedPreferences.getString("Ht8", "");
        edtHt8.setText(Ht8);
        String Ht9 = sharedPreferences.getString("Ht9", "");
        edtHt9.setText(Ht9);
        String Ht10 = sharedPreferences.getString("Ht10", "");
        edtHt10.setText(Ht10);
        String Ht11 = sharedPreferences.getString("Ht11", "");
        edtHt11.setText(Ht11);
        String Ht12 = sharedPreferences.getString("Ht12", "");
        edtHt12.setText(Ht12);
        String Ht13 = sharedPreferences.getString("Ht13", "");
        edtHt13.setText(Ht13);
        String Ht14 = sharedPreferences.getString("Ht14", "");
        edtHt14.setText(Ht14);
        String Ht15 = sharedPreferences.getString("Ht15", "");
        edtHt15.setText(Ht15);
        String Ht16 = sharedPreferences.getString("Ht16", "");
        edtHt16.setText(Ht16);



        String s1 = sharedPreferences.getString("s1", "");
        edts1.setText(s1);
        String s2 = sharedPreferences.getString("s2", "");
        edts2.setText(s2);
        String s3 = sharedPreferences.getString("s3", "");
        edts3.setText(s3);
        String s4 = sharedPreferences.getString("s4", "");
        edts4.setText(s4);
        String s5 = sharedPreferences.getString("s5", "");
        edts5.setText(s5);
        String s6 = sharedPreferences.getString("s6", "");
        edts6.setText(s6);
        String s7 = sharedPreferences.getString("s7", "");
        edts7.setText(s7);
        String s8 = sharedPreferences.getString("s8", "");
        edts8.setText(s8);
        String s9 = sharedPreferences.getString("s9", "");
        edts9.setText(s9);
        String s10 = sharedPreferences.getString("s10", "");
        edts10.setText(s10);
        String s11 = sharedPreferences.getString("s11", "");
        edts11.setText(s11);
        String s12 = sharedPreferences.getString("s12", "");
        edts12.setText(s12);
        String s13 = sharedPreferences.getString("s13", "");
        edts13.setText(s13);
        String s14 = sharedPreferences.getString("s14", "");
        edts14.setText(s14);
        String s15 = sharedPreferences.getString("s15", "");
        edts15.setText(s15);
        String s16 = sharedPreferences.getString("s16", "");
        edts16.setText(s16);


        String Ndt1 = sharedPreferences.getString("Ndt1", "");
        edtNdt1.setText(Ndt1);
        String Ndt2 = sharedPreferences.getString("Ndt2", "");
        edtNdt2.setText(Ndt2);
        String Ndt3 = sharedPreferences.getString("Ndt3", "");
        edtNdt3.setText(Ndt3);
        String Ndt4 = sharedPreferences.getString("Ndt4", "");
        edtNdt4.setText(Ndt4);
        String Ndt5 = sharedPreferences.getString("Ndt5", "");
        edtNdt5.setText(Ndt5);
        String Ndt6 = sharedPreferences.getString("Ndt6", "");
        edtNdt6.setText(Ndt6);
        String Ndt7 = sharedPreferences.getString("Ndt7", "");
        edtNdt7.setText(Ndt7);
        String Ndt8 = sharedPreferences.getString("Ndt8", "");
        edtNdt8.setText(Ndt8);
        String Ndt9 = sharedPreferences.getString("Ndt9", "");
        edtNdt9.setText(Ndt9);
        String Ndt10 = sharedPreferences.getString("Ndt10", "");
        edtNdt10.setText(Ndt10);
        String Ndt11 = sharedPreferences.getString("Ndt11", "");
        edtNdt11.setText(Ndt11);
        String Ndt12 = sharedPreferences.getString("Ndt12", "");
        edtNdt12.setText(Ndt12);
        String Ndt13 = sharedPreferences.getString("Ndt13", "");
        edtNdt13.setText(Ndt13);
        String Ndt14 = sharedPreferences.getString("Ndt14", "");
        edtNdt14.setText(Ndt14);
        String Ndt15 = sharedPreferences.getString("Ndt15", "");
        edtNdt15.setText(Ndt15);
        String Ndt16 = sharedPreferences.getString("Ndt16", "");
        edtNdt16.setText(Ndt16);


        String Ndd1 = sharedPreferences.getString("Ndd1", "");
        edtNdd1.setText(Ndd1);
        String Ndd2 = sharedPreferences.getString("Ndd2", "");
        edtNdd2.setText(Ndd2);
        String Ndd3 = sharedPreferences.getString("Ndd3", "");
        edtNdd3.setText(Ndd3);
        String Ndd4 = sharedPreferences.getString("Ndd4", "");
        edtNdd4.setText(Ndd4);
        String Ndd5 = sharedPreferences.getString("Ndd5", "");
        edtNdd5.setText(Ndd5);
        String Ndd6 = sharedPreferences.getString("Ndd6", "");
        edtNdd6.setText(Ndd6);
        String Ndd7 = sharedPreferences.getString("Ndd7", "");
        edtNdd7.setText(Ndd7);
        String Ndd8 = sharedPreferences.getString("Ndd8", "");
        edtNdd8.setText(Ndd8);
        String Ndd9 = sharedPreferences.getString("Ndd9", "");
        edtNdd9.setText(Ndd9);
        String Ndd10 = sharedPreferences.getString("Ndd10", "");
        edtNdd10.setText(Ndd10);
        String Ndd11 = sharedPreferences.getString("Ndd11", "");
        edtNdd11.setText(Ndd11);
        String Ndd12 = sharedPreferences.getString("Ndd12", "");
        edtNdd12.setText(Ndd12);
        String Ndd13 = sharedPreferences.getString("Ndd13", "");
        edtNdd13.setText(Ndd13);
        String Ndd14 = sharedPreferences.getString("Ndd14", "");
        edtNdd14.setText(Ndd14);
        String Ndd15 = sharedPreferences.getString("Ndd15", "");
        edtNdd15.setText(Ndd15);
        String Ndd16 = sharedPreferences.getString("Ndd16", "");
        edtNdd16.setText(Ndd16);

        String Tg1 = sharedPreferences.getString("Tg1", "");
        edtTg1.setText(Tg1);
        String Tg2 = sharedPreferences.getString("Tg2", "");
        edtTg2.setText(Tg2);
        String Tg3 = sharedPreferences.getString("Tg3", "");
        edtTg3.setText(Tg3);
        String Tg4 = sharedPreferences.getString("Tg4", "");
        edtTg4.setText(Tg4);
        String Tg5 = sharedPreferences.getString("Tg5", "");
        edtTg5.setText(Tg5);
        String Tg6 = sharedPreferences.getString("Tg6", "");
        edtTg6.setText(Tg6);
        String Tg7 = sharedPreferences.getString("Tg7", "");
        edtTg7.setText(Tg7);
        String Tg8 = sharedPreferences.getString("Tg8", "");
        edtTg8.setText(Tg8);
        String Tg9 = sharedPreferences.getString("Tg9", "");
        edtTg9.setText(Tg9);
        String Tg10 = sharedPreferences.getString("Tg10", "");
        edtTg10.setText(Tg10);
        String Tg11 = sharedPreferences.getString("Tg11", "");
        edtTg11.setText(Tg11);
        String Tg12 = sharedPreferences.getString("Tg12", "");
        edtTg12.setText(Tg12);
        String Tg13 = sharedPreferences.getString("Tg13", "");
        edtTg13.setText(Tg13);
        String Tg14 = sharedPreferences.getString("Tg14", "");
        edtTg14.setText(Tg14);
        String Tg15 = sharedPreferences.getString("Tg15", "");
        edtTg15.setText(Tg15);
        String Tg16 = sharedPreferences.getString("Tg16", "");
        edtTg16.setText(Tg16);

    }


    public void savaDaTa() {

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
                try {
                    if (checkBeforSave() == true) {
                        final QAM_TEMP_MODEL qtm = new QAM_TEMP_MODEL();
                        saveTempModel(qtm);

                        AlertDialog.Builder b = new AlertDialog.Builder(mac1fvi2.this);
                        b.setTitle("Xác nhận");
                        b.setMessage("Bạn có chắc muốn Lưu dữ liệu không?");
                        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                //sendmail
                                try {
                                    CallApi.insertQAM_TEMP_MODEL(factory, qtm, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {

                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();

                                        }
                                    });

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                ToastCustom.message(mac1fvi2.this, "Lưu Thành Công!", Color.GREEN);
                            }
                        });
                        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog al = b.create();
                        al.show();
//                        ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
//                            //clear();
//                            ib_logout.setEnabled(true);
//                ToastCustom.message(mac1fvi2.this, "Bạn đã Lưu dữ liệu thành công, sau khi thoát ra dữ liệu vẫn được giữ lại!", Color.GREEN);
            }
        });

    }

    public void save(){
        String May = edtMay.getText().toString();
        editor.putString("May", May);
        String UP_DATE = edt_date.getText().toString();
        editor.putString("UP_DATE", UP_DATE);
        String ca = edtCa.getText().toString();
        editor.putString("Ca", ca);
        String CHECK_DATE = edt_dateCheck.getText().toString();
        editor.putString("CHECK_DATE", CHECK_DATE);
        String TCTGT = edtTctgT.getText().toString();
        editor.putString("TCTGT", TCTGT);
        String TCTGP = edtTctgP.getText().toString();
        editor.putString("TCTGP", TCTGP);
        String NDTCT = edtTcndT.getText().toString();
        editor.putString("NDTCT", NDTCT);
        String NDTCP = edtTcndP.getText().toString();
        editor.putString("NDTCP", NDTCP);
        String Ht1 = edtHt1.getText().toString();
        editor.putString("Ht1", Ht1);
        String Ht2 = edtHt2.getText().toString();
        editor.putString("Ht2", Ht2);
        String Ht3 = edtHt3.getText().toString();
        editor.putString("Ht3", Ht3);
        String Ht4 = edtHt4.getText().toString();
        editor.putString("Ht4", Ht4);
        String Ht5 = edtHt5.getText().toString();
        editor.putString("Ht5", Ht5);
        String Ht6 = edtHt6.getText().toString();
        editor.putString("Ht6", Ht6);
        String Ht7 = edtHt7.getText().toString();
        editor.putString("Ht7", Ht7);
        String Ht8 = edtHt8.getText().toString();
        editor.putString("Ht8", Ht8);
        String Ht9 = edtHt9.getText().toString();
        editor.putString("Ht9", Ht9);
        String Ht10 = edtHt10.getText().toString();
        editor.putString("Ht10", Ht10);
        String Ht11 = edtHt11.getText().toString();
        editor.putString("Ht11", Ht11);
        String Ht12 = edtHt12.getText().toString();
        editor.putString("Ht12", Ht12);
        String Ht13 = edtHt13.getText().toString();
        editor.putString("Ht13", Ht13);
        String Ht14 = edtHt14.getText().toString();
        editor.putString("Ht14", Ht14);
        String Ht15 = edtHt15.getText().toString();
        editor.putString("Ht15", Ht15);
        String Ht16 = edtHt16.getText().toString();
        editor.putString("Ht16", Ht16);

//        String HT = edtHT.getText().toString();
//         editor.putString("HT", HT);
        String s1 = edts1.getText().toString();
        editor.putString("s1", s1);
        String s2 = edts2.getText().toString();
        editor.putString("s2", s2);
        String s3 = edts3.getText().toString();
        editor.putString("s3", s3);
        String s4 = edts4.getText().toString();
        editor.putString("s4", s4);
        String s5 =  edts5.getText().toString();
        editor.putString("s5", s5);
        String s6 =  edts6.getText().toString();
        editor.putString("s6", s6);
        String s7 =  edts7.getText().toString();
        editor.putString("s7", s7);
        String s8 =  edts8.getText().toString();
        editor.putString("s8", s8);
        String s9 = edts9.getText().toString();
        editor.putString("s9", s9);
        String s10 =  edts10.getText().toString();
        editor.putString("s10", s10);
        String s11 = edts11.getText().toString();
        editor.putString("s11", s11);
        String s12 =  edts12.getText().toString();
        editor.putString("s12", s12);
        String s13 =  edts13.getText().toString();
        editor.putString("s13", s13);
        String s14 =  edts14.getText().toString();
        editor.putString("s14", s14);
        String s15 = edts15.getText().toString();
        editor.putString("s15", s15);
        String s16 =  edts16.getText().toString();
        editor.putString("s16", s16);



        String Ndt1 = edtNdt1.getText().toString();
        editor.putString("Ndt1", Ndt1);
        String Ndt2 = edtNdt2.getText().toString();
        editor.putString("Ndt2", Ndt2);
        String Ndt3 = edtNdt3.getText().toString();
        editor.putString("Ndt3", Ndt3);
        String Ndt4 = edtNdt4.getText().toString();
        editor.putString("Ndt4", Ndt4);
        String Ndt5 = edtNdt5.getText().toString();
        editor.putString("Ndt5", Ndt5);
        String Ndt6 = edtNdt6.getText().toString();
        editor.putString("Ndt6", Ndt6);
        String Ndt7 = edtNdt7.getText().toString();
        editor.putString("Ndt7", Ndt7);
        String Ndt8 = edtNdt8.getText().toString();
        editor.putString("Ndt8", Ndt8);
        String Ndt9 = edtNdt9.getText().toString();
        editor.putString("Ndt9", Ndt9);
        String Ndt10 = edtNdt10.getText().toString();
        editor.putString("Ndt10", Ndt10);
        String Ndt11 = edtNdt11.getText().toString();
        editor.putString("Ndt11", Ndt11);
        String Ndt12 = edtNdt12.getText().toString();
        editor.putString("Ndt12", Ndt12);
        String Ndt13 = edtNdt13.getText().toString();
        editor.putString("Ndt13", Ndt13);
        String Ndt14 = edtNdt14.getText().toString();
        editor.putString("Ndt14", Ndt14);
        String Ndt15 = edtNdt15.getText().toString();
        editor.putString("Ndt15", Ndt15);
        String Ndt16 = edtNdt16.getText().toString();
        editor.putString("Ndt16", Ndt16);


        String Ndd1 =edtTg1.getText().toString();
        editor.putString("Ndd1", Ndd1);
        String Ndd2 =edtTg2.getText().toString();
        editor.putString("Ndd2", Ndd2);
        String Ndd3 =edtTg3.getText().toString();
        editor.putString("Ndd3", Ndd3);
        String Ndd4 =edtTg4.getText().toString();
        editor.putString("Ndd4", Ndd4);
        String Ndd5 =edtTg5.getText().toString();
        editor.putString("Ndd5", Ndd5);
        String Ndd6 =edtTg6.getText().toString();
        editor.putString("Ndd6", Ndd6);
        String Ndd7 =edtTg7.getText().toString();
        editor.putString("Ndd7", Ndd7);
        String Ndd8 =edtTg8.getText().toString();
        editor.putString("Ndd8", Ndd8);
        String Ndd9 =edtTg9.getText().toString();
        editor.putString("Ndd9", Ndd9);
        String Ndd10 =edtTg10.getText().toString();
        editor.putString("Ndd10", Ndd10);
        String Ndd11 =edtTg11.getText().toString();
        editor.putString("Ndd11", Ndd11);
        String Ndd12 =edtTg12.getText().toString();
        editor.putString("Ndd12", Ndd12);
        String Ndd13 =edtTg13.getText().toString();
        editor.putString("Ndd13", Ndd13);
        String Ndd14 =edtTg14.getText().toString();
        editor.putString("Ndd14", Ndd14);
        String Ndd15 =edtTg15.getText().toString();
        editor.putString("Ndd15", Ndd15);
        String Ndd16 =edtTg16.getText().toString();
        editor.putString("Ndd16", Ndd16);


        String Tg1 =edtTg1.getText().toString();
        editor.putString("Tg1", Tg1);
        String Tg2 =edtTg2.getText().toString();
        editor.putString("Tg2", Tg2);
        String Tg3 =edtTg3.getText().toString();
        editor.putString("Tg3", Tg3);
        String Tg4 =edtTg4.getText().toString();
        editor.putString("Tg4", Tg4);
        String Tg5 =edtTg5.getText().toString();
        editor.putString("Tg5", Tg5);
        String Tg6 =edtTg6.getText().toString();
        editor.putString("Tg6", Tg6);
        String Tg7 =edtTg7.getText().toString();
        editor.putString("Tg7", Tg7);
        String Tg8 =edtTg8.getText().toString();
        editor.putString("Tg8", Tg8);
        String Tg9 =edtTg9.getText().toString();
        editor.putString("Tg9", Tg9);
        String Tg10 =edtTg10.getText().toString();
        editor.putString("Tg10", Tg10);
        String Tg11 =edtTg11.getText().toString();
        editor.putString("Tg11", Tg11);
        String Tg12 =edtTg12.getText().toString();
        editor.putString("Tg12", Tg12);
        String Tg13 =edtTg13.getText().toString();
        editor.putString("Tg13", Tg13);
        String Tg14 =edtTg14.getText().toString();
        editor.putString("Tg14", Tg14);
        String Tg15 =edtTg15.getText().toString();
        editor.putString("Tg15", Tg15);
        String Tg16 =edtTg16.getText().toString();
        editor.putString("Tg16", Tg16);
        editor.commit();
    }

    public void clear(){
        for (EditText ed:edtl){
            ed.setText("");
        }
        for (EditText ed:edtnd){
            ed.setBackgroundColor(Color.WHITE);
        }
    }

    public void checkTempValue() {
        ArrayList tvl = new ArrayList();

        tvl.add(edtNdt1.getText().toString().equals("") ? "" : edtNdt1.getText().toString());
        tvl.add(edtNdt2.getText().toString().equals("") ? "" : edtNdt2.getText().toString());
        tvl.add(edtNdt3.getText().toString().equals("") ? "" : edtNdt3.getText().toString());
        tvl.add(edtNdt4.getText().toString().equals("") ? "" : edtNdt4.getText().toString());
        tvl.add(edtNdt5.getText().toString().equals("") ? "" : edtNdt5.getText().toString());
        tvl.add(edtNdt6.getText().toString().equals("") ? "" : edtNdt6.getText().toString());
        tvl.add(edtNdt7.getText().toString().equals("") ? "" : edtNdt7.getText().toString());
        tvl.add(edtNdt8.getText().toString().equals("") ? "" : edtNdt8.getText().toString());
        tvl.add(edtNdt9.getText().toString().equals("") ? "" : edtNdt9.getText().toString());
        tvl.add(edtNdt10.getText().toString().equals("") ? "" : edtNdt10.getText().toString());
        tvl.add(edtNdt11.getText().toString().equals("") ? "" : edtNdt11.getText().toString());
        tvl.add(edtNdt12.getText().toString().equals("") ? "" : edtNdt12.getText().toString());
        tvl.add(edtNdt13.getText().toString().equals("") ? "" : edtNdt13.getText().toString());
        tvl.add(edtNdt14.getText().toString().equals("") ? "" : edtNdt14.getText().toString());
        tvl.add(edtNdt15.getText().toString().equals("") ? "" : edtNdt15.getText().toString());
        tvl.add(edtNdt16.getText().toString().equals("") ? "" : edtNdt16.getText().toString());
        tvl.add(edtNdd1.getText().toString().equals("") ? "" : edtNdd1.getText().toString());
        tvl.add(edtNdd2.getText().toString().equals("") ? "" : edtNdd2.getText().toString());
        tvl.add(edtNdd3.getText().toString().equals("") ? "" : edtNdd3.getText().toString());
        tvl.add(edtNdd4.getText().toString().equals("") ? "" : edtNdd4.getText().toString());
        tvl.add(edtNdd5.getText().toString().equals("") ? "" : edtNdd5.getText().toString());
        tvl.add(edtNdd6.getText().toString().equals("") ? "" : edtNdd6.getText().toString());
        tvl.add(edtNdd7.getText().toString().equals("") ? "" : edtNdd7.getText().toString());
        tvl.add(edtNdd8.getText().toString().equals("") ? "" : edtNdd8.getText().toString());
        tvl.add(edtNdd9.getText().toString().equals("") ? "" : edtNdd9.getText().toString());
        tvl.add(edtNdd10.getText().toString().equals("") ? "" : edtNdd10.getText().toString());
        tvl.add(edtNdd11.getText().toString().equals("") ? "" : edtNdd11.getText().toString());
        tvl.add(edtNdd12.getText().toString().equals("") ? "" : edtNdd12.getText().toString());
        tvl.add(edtNdd13.getText().toString().equals("") ? "" : edtNdd13.getText().toString());
        tvl.add(edtNdd14.getText().toString().equals("") ? "" : edtNdd14.getText().toString());
        tvl.add(edtNdd15.getText().toString().equals("") ? "" : edtNdd15.getText().toString());
        tvl.add(edtNdd16.getText().toString().equals("") ? "" : edtNdd16.getText().toString());



        createNote();// sau khi duyệt phần tử nạp vào array list tvl, nếu có phần thử thì thực hiện hàm createNote, lấy dư liệu nhập
        //từ bàn phím lưu tạp thời trong khay nhớ tạm
        statusPop = true;//set true false display button save of note

//        createNote();
//        statusPop = false;// không hiển thị nút save.

    }



    public void createNote(){
        final String tcndt = edtTcndT.getText().toString();
        final int tcndkhuon = Integer.parseInt(tcndt);
        final int tcndP = tcndkhuon+3;
        final int tcndT = tcndkhuon-3;
        edtNdt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt1.getText().toString();
                if(edtNdt1.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt1.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt1.setBackgroundColor(Color.WHITE);
                    }else{
                        edtNdt1.setBackgroundColor(Color.RED);
                        if(temp!=null){
                            openNoteDialog(temp.getEDTNDT1(),statusPop,1);
                        }
                    }
                }

            }
        });
        edtNdt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt2.getText().toString();
                if(edtNdt2.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt2.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt2.setBackgroundColor(Color.WHITE);

                    }

                    else{
                        edtNdt2.setBackgroundColor(Color.RED);
                        if(temp!=null){
                            openNoteDialog(temp.getEDTNDT2(),statusPop,2);
                        }

                    }
                }

            }
        });
        edtNdt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt3.getText().toString();
                if(edtNdt3.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt3.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt3.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdt3.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT3(), statusPop, 3);
                        }
                    }
                }

            }
        });
        edtNdt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt4.getText().toString();
                if(edtNdt4.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt4.setBackgroundColor(Color.WHITE);
                }else {
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt4.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdt4.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT4(), statusPop, 4);
                        }
                    }
                }

            }
        });
        edtNdt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt5.getText().toString();
                if(edtNdt5.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt5.setBackgroundColor(Color.WHITE);
                }else {
                    int tc = Integer.parseInt(nd);
                    if (tcndT <= tc && tcndP >= tc) {
                        edtNdt5.setBackgroundColor(Color.WHITE);
                    } else {
                        edtNdt5.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT5(), statusPop, 5);
                        }
                    }
                }

            }
        });
        edtNdt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt6.getText().toString();
                if(edtNdt6.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt6.setBackgroundColor(Color.WHITE);
                }else {
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt6.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdt6.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT6(), statusPop, 6);
                        }
                    }
                }

            }
        });
        edtNdt7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt7.getText().toString();
                if(edtNdt7.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt7.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt7.setBackgroundColor(Color.WHITE);
                    }else{
                        edtNdt7.setBackgroundColor(Color.RED);
                        if(temp!=null){
                            openNoteDialog(temp.getEDTNDT7(),statusPop,7);
                        }
                    }
                }

            }
        });
        edtNdt8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt8.getText().toString();
                if(edtNdt8.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt8.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt8.setBackgroundColor(Color.WHITE);
                    }else{
                        edtNdt8.setBackgroundColor(Color.RED);
                        if(temp!=null){
                            openNoteDialog(temp.getEDTNDT8(),statusPop,8);
                        }

                    }
                }

            }
        });
        edtNdt9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nd = edtNdt9.getText().toString();
                if(edtNdt9.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt9.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt9.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdt9.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT9(), statusPop, 9);
                        }
                    }
                }

            }
        });
        edtNdt10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt10.getText().toString();
                if(edtNdt10.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt10.setBackgroundColor(Color.WHITE);
                }else {
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt10.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdt10.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT10(), statusPop, 10);
                        }
                    }
                }

            }
        });
        edtNdt11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt11.getText().toString();
                if(edtNdt11.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt11.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt11.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdt11.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT11(), statusPop, 11);
                        }
                    }
                }

            }
        });
        edtNdt12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt12.getText().toString();
                if(edtNdt12.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt12.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt12.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdt12.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT12(), statusPop, 12);
                        }
                    }
                }

            }
        });
        edtNdt13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt13.getText().toString();
                if(edtNdt13.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt13.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt13.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdt13.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT13(), statusPop, 13);
                        }
                    }
                }

            }
        });
        edtNdt14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt14.getText().toString();
                if(edtNdt14.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt14.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt14.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdt14.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT14(), statusPop, 14);
                        }
                    }                  }

            }
        });
        edtNdt15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt15.getText().toString();
                if(edtNdt15.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt15.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt15.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdt15.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT15(), statusPop, 15);
                        }
                    }
                }

            }
        });
        edtNdt16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdt16.getText().toString();
                if(edtNdt16.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdt16.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdt16.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdt16.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDT16(), statusPop, 16);
                        }
                    }
                }

            }
        });
        edtNdd1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd1.getText().toString();
                if(edtNdd1.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd1.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd1.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd1.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD1(), statusPop, 17);
                        }
                    }
                }

            }
        });
        edtNdd2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd2.getText().toString();
                if(edtNdd2.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd2.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd2.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd2.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD2(), statusPop, 18);
                        }
                    }
                }

            }
        });
        edtNdd3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd3.getText().toString();
                if(edtNdd3.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd3.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd3.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd3.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD3(), statusPop, 19);
                        }
                    }
                }

            }
        });
        edtNdd4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd4.getText().toString();
                if(edtNdd4.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd4.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd4.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd4.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD4(), statusPop, 20);
                        }
                    }
                }

            }
        });
        edtNdd5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd5.getText().toString();
                if(edtNdd5.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd5.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd5.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd5.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD4(), statusPop, 21);
                        }
                    }
                }

            }
        });
        edtNdd6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd6.getText().toString();
                if(edtNdd6.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd6.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd6.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd6.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD4(), statusPop, 22);
                        }
                    }
                }

            }
        });
        edtNdd7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd7.getText().toString();
                if(edtNdd7.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd7.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd7.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd7.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD7(), statusPop, 23);
                        }
                    }
                }

            }
        });

        edtNdd8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd8.getText().toString();
                if(edtNdd8.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd8.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd8.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd8.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD8(), statusPop, 24);
                        }
                    }
                }

            }
        });
        edtNdd9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd9.getText().toString();
                if (edtNdd9.getText().toString().equals("") == true) {
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd9.setBackgroundColor(Color.WHITE);
                } else {
                    int tc = Integer.parseInt(nd);
                    if (tcndT <= tc && tcndP >= tc) {
                        edtNdd9.setBackgroundColor(Color.WHITE);
                    } else {
                        edtNdd9.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD9(), statusPop, 25);
                        }
                    }

                }
            }
        });
        edtNdd10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd10.getText().toString();
                if(edtNdd10.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd10.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd10.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd10.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD10(), statusPop, 26);
                        }
                    }
                }

            }
        });

        edtNdd11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd11.getText().toString();
                if(edtNdd11.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd11.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd11.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd11.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD11(), statusPop, 27);
                        }
                    }
                }

            }
        });
        edtNdd12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd12.getText().toString();
                if(edtNdd12.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd12.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd12.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd12.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD12(), statusPop, 28);
                        }
                    }
                }

            }
        });
        edtNdd13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd13.getText().toString();
                if(edtNdd13.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd13.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd13.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd13.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD13(), statusPop, 29);
                        }
                    }
                }

            }
        });
        edtNdd14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd14.getText().toString();
                if(edtNdd14.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd14.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd14.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd14.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD14(), statusPop, 30);
                        }
                    }
                }

            }
        });
        edtNdd15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd15.getText().toString();
                if(edtNdd15.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd15.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd15.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd15.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD15(), statusPop, 31);
                        }
                    }
                }

            }
        });
        edtNdd16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nd = edtNdd16.getText().toString();
                if(edtNdd16.getText().toString().equals("")==true){
                    ToastCustom.message(getApplicationContext(), "U là trời!Có gì đâu mà đòi Note", Color.RED);
                    edtNdd16.setBackgroundColor(Color.WHITE);
                }else{
                    int tc = Integer.parseInt(nd);
                    if(tcndT <= tc && tcndP >= tc ) {
                        edtNdd16.setBackgroundColor(Color.WHITE);
                    }else {
                        edtNdd16.setBackgroundColor(Color.RED);
                        if (temp != null) {
                            openNoteDialog(temp.getEDTNDD16(), statusPop, 32);
                        }
                    }
                }

            }
        });







    }

    private void openNoteDialog(String note,boolean status,int pos) {

        Temp_Note fragement = (Temp_Note) getSupportFragmentManager().findFragmentByTag(Temp_Note.class.getSimpleName());
        if (fragement != null) fragement.dismiss();

        Temp_Note.newInstance(popupFragementListener, note, status, pos).show(getSupportFragmentManager(), Temp_Note.class.getSimpleName());
    }

    Temp_Note.PopupFragementListener popupFragementListener = new Temp_Note.PopupFragementListener() {

        @Override
        public void onSelected(String nt, int pos) {

            Temp_Note fragement = (Temp_Note) getSupportFragmentManager().findFragmentByTag(Temp_Note.class.getSimpleName());
            if (fragement != null) fragement.dismiss();
            if (pos == 1) {
                temp.setEDTNDT1(nt);
            }
            if (pos == 2) {
                temp.setEDTNDT2(nt);
            }
            if (pos == 3) {
                temp.setEDTNDT3(nt);
            }
            if (pos == 4) {
                temp.setEDTNDT4(nt);
            }
            if (pos == 5) {
                temp.setEDTNDT5(nt);
            }
            if (pos == 6) {
                temp.setEDTNDT6(nt);
            }
            if (pos == 7) {
                temp.setEDTNDT7(nt);
            }
            if (pos == 8) {
                temp.setEDTNDT8(nt);
            }
            if (pos == 9) {
                temp.setEDTNDT9(nt);
            }
            if (pos == 10) {
                temp.setEDTNDT10(nt);
            }
            if (pos == 11) {
                temp.setEDTNDT11(nt);
            }
            if (pos == 12) {
                temp.setEDTNDT12(nt);
            }
            if (pos == 13) {
                temp.setEDTNDT13(nt);
            }
            if (pos == 14) {
                temp.setEDTNDT14(nt);
            }
            if (pos == 15) {
                temp.setEDTNDT15(nt);
            }
            if (pos == 16) {
                temp.setEDTNDT16(nt);
            }

            if (pos == 17) {
                temp.setEDTNDD1(nt);
            }
            if (pos == 18) {
                temp.setEDTNDD2(nt);
            }
            if (pos == 19) {
                temp.setEDTNDD3(nt);
            }
            if (pos == 20) {
                temp.setEDTNDD4(nt);
            }
            if (pos == 21) {
                temp.setEDTNDD5(nt);
            }
            if (pos == 22) {
                temp.setEDTNDD6(nt);
            }
            if (pos == 23) {
                temp.setEDTNDD7(nt);
            }
            if (pos == 24) {
                temp.setEDTNDD8(nt);
            }
            if (pos == 25) {
                temp.setEDTNDD9(nt);
            }
            if (pos == 26) {
                temp.setEDTNDD10(nt);
            }
            if (pos == 27) {
                temp.setEDTNDD11(nt);
            }
            if (pos == 28) {
                temp.setEDTNDD12(nt);
            }
            if (pos == 29) {
                temp.setEDTNDD13(nt);
            }
            if (pos == 30) {
                temp.setEDTNDD14(nt);
            }
            if (pos == 31) {
                temp.setEDTNDD15(nt);
            }
            if (pos == 32) {
                temp.setEDTNDD16(nt);
            }


        }

        @Override
        public void onCancel() {
            Temp_Note fragement = (Temp_Note) getSupportFragmentManager().findFragmentByTag(Temp_Note.class.getSimpleName());
            if (fragement != null) fragement.dismiss();
        }
    };




    public void defaultSpinner() {


        listShift = new ArrayList<>();
        listShift.add("1");
        listShift.add("2");
        listShift.add("3");
        listShift.add("4");
        listShift.add("5");
        listShift.add("6");
        listShift.add("7");
        listShift.add("8");
        ShiftAdapter = new ArrayAdapter(mac1fvi2.this, R.layout.spn_layout, listShift);
        spnMay.setAdapter(ShiftAdapter);

    }

    public void datePickerDialogSearch(final EditText edt_date) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(mac1fvi2.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int monthcurrent = monthOfYear + 1;
                        String month = "";
                        String day = "";
                        if (dayOfMonth > 9) {
                            day = String.valueOf(dayOfMonth);

                        } else {
                            day = "0" + String.valueOf(dayOfMonth);
                        }
                        if (monthcurrent > 9) {
                            month = String.valueOf(monthcurrent);
                        } else {
                            month = "0" + String.valueOf(monthcurrent);

                        }
                        edt_date.setText(month + "/" + day + "/" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public class IDmodel extends ApiReturnModel<ArrayList<String>> {}

    public class listQtm extends ApiReturnModel<ArrayList<QAM_TEMP_MODEL>> {
    }
    public class listTND extends ApiReturnModel<ArrayList<TEMP>> {

    }




}




