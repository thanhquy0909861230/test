package com.deanshoes.AppQAM.step5;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.model.QAM.NOTE_CHECKTEMP;
import com.deanshoes.AppQAM.model.QAM.NOTE_TEMP;
import com.deanshoes.AppQAM.model.QAM.QAM_AMT;
import com.deanshoes.AppQAM.model.QAM.TEMP;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step2.fvi.IPBatchActivity_FVI;
import com.deanshoes.AppQAM.step3.Popup_Note;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class checkTempIP extends mac1fvi2 {

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    ArrayList<TEMP> listID;
    ArrayList<NOTE_TEMP> listID2;
    private ArrayList<NOTE_TEMP> amtlst;
    String[] listIDx;
    String[] listIDx2;
//    String[] listIDx2;
//    ArrayList<NOTE_TEMP> listID2;
    private ArrayAdapter arrIdModel;
    NOTE_CHECKTEMP note_temp;
    String user = "";
    String blu = "";
    int value1, value2, value3, value4;
    String[] posNote;
    String[] valNote;
    boolean creMode=false;
    int posAMT=-1;
    public static String factory = "";
    private static boolean statusPop;
    EditText[] edtl, edtl_Note;
    EditText edt_Temp1, edt_Temp2, edt_Temp3, edt_Temp4, edt_Temp5, edt_Temp6, edt_Temp7, edt_Temp8, edt_Temp9, edt_Temp10, edt_Temp11, edt_Temp12,
            edt_Temp13, edt_Temp14, edt_Temp15, edt_Temp16,
            edt_Dtemp1, edt_Dtemp2, edt_Dtemp3, edt_Dtemp4, edt_Dtemp5, edt_Dtemp6, edt_Dtemp7, edt_Dtemp8, edt_Dtemp9, edt_Dtemp10, edt_Dtemp11, edt_Dtemp12,
            edt_Dtemp13, edt_Dtemp14, edt_Dtemp15, edt_Dtemp16,
            edt_Notemp1, edt_Notemp2, edt_Notemp3, edt_Notemp4, edt_Notemp5, edt_Notemp6, edt_Notemp7, edt_Notemp8, edt_Notemp9, edt_Notemp10, edt_Notemp11, edt_Notemp12,
            edt_Notemp13, edt_Notemp14, edt_Notemp15, edt_Notemp16,
            edt_DNotemp1, edt_DNotemp2, edt_DNotemp3, edt_DNotemp4, edt_DNotemp5, edt_DNotemp6, edt_DNotemp7, edt_DNotemp8, edt_DNotemp9, edt_DNotemp10, edt_DNotemp11, edt_DNotemp12,
            edt_DNotemp13, edt_DNotemp14, edt_DNotemp15, edt_DNotemp16,
            Search_May, Search_Ca, Search_date;
    CheckBox[] chbox1;
    CheckBox chk_temp1, chk_temp2, chk_temp3, chk_temp4, chk_temp5, chk_temp6, chk_temp7, chk_temp8, chk_temp9, chk_temp10, chk_temp11, chk_temp12, chk_temp13,
            chk_temp14, chk_temp15, chk_temp16,
            chk_Dtemp1, chk_Dtemp2, chk_Dtemp3, chk_Dtemp4, chk_Dtemp5, chk_Dtemp6, chk_Dtemp7, chk_Dtemp8, chk_Dtemp9, chk_Dtemp10, chk_Dtemp11, chk_Dtemp12, chk_Dtemp13,
            chk_Dtemp14, chk_Dtemp15, chk_Dtemp16, chk_AllTempUp, chk_AllTempDown;

    TextView edtTcndT, edtTcndP;

    ImageButton[] imbtn;
    ImageButton  ibtn_temp1, ibtn_temp2, ibtn_temp3, ibtn_temp4, ibtn_temp5, ibtn_temp6, ibtn_temp7, ibtn_temp8, ibtn_temp9, ibtn_temp10, ibtn_temp11, ibtn_temp12, ibtn_temp13, ibtn_temp14,
            ibtn_temp15, ibtn_temp16,
            ibtn_Dtemp1, ibtn_Dtemp2, ibtn_Dtemp3, ibtn_Dtemp4, ibtn_Dtemp5, ibtn_Dtemp6, ibtn_Dtemp7, ibtn_Dtemp8, ibtn_Dtemp9, ibtn_Dtemp10, ibtn_Dtemp11, ibtn_Dtemp12, ibtn_Dtemp13, ibtn_Dtemp14,
            ibtn_Dtemp15, ibtn_Dtemp16, ib_logout;
    Button btn_step6, btnSearch, btn_new, btn_edit, btn_save;
    Spinner spn_id,spr_shift, spr_shift2;
    private ArrayList<String> listShift;
    private ArrayAdapter ShiftAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.check_tempip);
        final Intent intent = getIntent();
        final Bundle bundle = intent.getExtras();
        if (bundle != null) {
            user = bundle.getString("user", "QAM");
            factory = bundle.getString("fac");
            blu = bundle.getString("May");
        }

        FindID();
        btn_save.setEnabled(false);
        btnSearch.setEnabled(false);
        btnClear.setEnabled(false);
        btn_edit.setEnabled(false);
        Search_May.setEnabled(false);
        Search_Ca.setEnabled(false);
        Search_date.setEnabled(false);

        listShift = new ArrayList<>();
        listShift.add("±1");
        listShift.add("±2");
        listShift.add("±3");
        ShiftAdapter = new ArrayAdapter(checkTempIP.this, R.layout.spn_layout, listShift);
        spr_shift.setAdapter(ShiftAdapter);
        spr_shift2.setAdapter(ShiftAdapter);
        ShiftAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);

        imbtn = new ImageButton[]{ibtn_temp1, ibtn_temp2, ibtn_temp3, ibtn_temp4, ibtn_temp5, ibtn_temp6, ibtn_temp7, ibtn_temp8, ibtn_temp9, ibtn_temp10, ibtn_temp11, ibtn_temp12, ibtn_temp13, ibtn_temp14,
                ibtn_temp15, ibtn_temp16,
                ibtn_Dtemp1, ibtn_Dtemp2, ibtn_Dtemp3, ibtn_Dtemp4, ibtn_Dtemp5, ibtn_Dtemp6, ibtn_Dtemp7, ibtn_Dtemp8, ibtn_Dtemp9, ibtn_Dtemp10, ibtn_Dtemp11, ibtn_Dtemp12, ibtn_Dtemp13, ibtn_Dtemp14,
                ibtn_Dtemp15, ibtn_Dtemp16};

        chbox1= new CheckBox[]{
                chk_temp1 , chk_temp2, chk_temp3, chk_temp4, chk_temp5, chk_temp6, chk_temp7, chk_temp8, chk_temp9, chk_temp10, chk_temp11, chk_temp12, chk_temp13,
                chk_temp14, chk_temp15, chk_temp16,
                chk_Dtemp1, chk_Dtemp2, chk_Dtemp3, chk_Dtemp4, chk_Dtemp5, chk_Dtemp6, chk_Dtemp7, chk_Dtemp8, chk_Dtemp9, chk_Dtemp10, chk_Dtemp11, chk_Dtemp12, chk_Dtemp13,
                chk_Dtemp14, chk_Dtemp15, chk_Dtemp16
        };
        edtl = new EditText[]{
                edt_Temp1, edt_Temp2, edt_Temp3, edt_Temp4, edt_Temp5, edt_Temp6, edt_Temp7, edt_Temp8, edt_Temp9, edt_Temp10, edt_Temp11, edt_Temp12,
                edt_Temp13, edt_Temp14, edt_Temp15, edt_Temp16,
                edt_Dtemp1, edt_Dtemp2, edt_Dtemp3, edt_Dtemp4, edt_Dtemp5, edt_Dtemp6, edt_Dtemp7, edt_Dtemp8, edt_Dtemp9, edt_Dtemp10, edt_Dtemp11, edt_Dtemp12,
                edt_Dtemp13, edt_Dtemp14, edt_Dtemp15, edt_Dtemp16};
        edtl_Note = new EditText[]{
                edt_Notemp1, edt_Notemp2, edt_Notemp3, edt_Notemp4, edt_Notemp5, edt_Notemp6, edt_Notemp7, edt_Notemp8, edt_Notemp9, edt_Notemp10, edt_Notemp11, edt_Notemp12,
                edt_Notemp13, edt_Notemp14, edt_Notemp15, edt_Notemp16,
                edt_DNotemp1, edt_DNotemp2, edt_DNotemp3, edt_DNotemp4, edt_DNotemp5, edt_DNotemp6, edt_DNotemp7, edt_DNotemp8, edt_DNotemp9, edt_DNotemp10, edt_DNotemp11, edt_DNotemp12,
                edt_DNotemp13, edt_DNotemp14, edt_DNotemp15, edt_DNotemp16};

        posNote = new String[]{"NOTE_TEMP1", "NOTE_TEMP2", "NOTE_TEMP3", "NOTE_TEMP4", "NOTE_TEMP5", "NOTE_TEMP6", "NOTE_TEMP7", "NOTE_TEMP8", "NOTE_TEMP9", "NOTE_TEMP10",
                "NOTE_TEMP11", "NOTE_TEMP12", "NOTE_TEMP13", "NOTE_TEMP14", "NOTE_TEMP15", "NOTE_TEMP16", "NOTE_DTEMP1", "NOTE_DTEMP2", "NOTE_DTEMP3", "NOTE_DTEMP4",
                "NOTE_DTEMP5", "NOTE_DTEMP6", "NOTE_DTEMP7", "NOTE_DTEMP8", "NOTE_DTEMP9", "NOTE_DTEMP10", "NOTE_DTEMP11", "NOTE_DTEMP12", "NOTE_DTEMP13", "NOTE_DTEMP14",
                "NOTE_DTEMP15", "NOTE_DTEMP16"};


        btn_step6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(checkTempIP.this, mac1fvi2.class);
                i.putExtras(bundle);
                checkTempIP.this.startActivity(i);
            }
        });
        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(checkTempIP.this, LoginActivity.class);
                startActivity(i);
            }
        });
        Search_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog(Search_date);

            }
        });



        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    final NOTE_TEMP tpn = new NOTE_TEMP();
                    saveTempNote(tpn);

                    AlertDialog.Builder b = new AlertDialog.Builder(checkTempIP.this);
                    b.setTitle("Xác nhận");
                    b.setMessage("Bạn có chắc muốn Lưu dữ liệu không?");
                    b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //sendmail
                            try {
                                CallApi.insertTEMP_NOTE(factory, tpn, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();

                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            ToastCustom.message(checkTempIP.this, "Lưu Thành Công!", Color.GREEN);
                        }
                    });
                    b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog al = b.create();
                    al.show();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        for(EditText ed : edtl){
            ed.setEnabled(false);
        }
        for(EditText note : edtl_Note ){
            note.setEnabled(false);
        }
        for (CheckBox checkBox : chbox1) {
            checkBox.setEnabled(false);
        }

        spr_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] shift1 = spr_shift.getSelectedItem().toString().split("±");
                String shift1a = shift1[1];
                a1 = Integer.parseInt(shift1a);
                edtTcndT.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String ndtieuchuantrai = edtTcndT.getText().toString();
                        final int b1 = Integer.parseInt(ndtieuchuantrai);
                        c1 = b1 + a1;
                        d1 = b1 - a1;
                        for (int i = 0; i < edtl.length; i++){
                            final int temp = i;
                            edtl[temp].addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                }
                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    String abc1 = edtl[temp].getText().toString();
                                    if (!edtl[temp].getText().toString().equals("")){
                                        int value1 = Integer.parseInt(abc1);
                                        if ( c1 < value1 || value1 < d1 ) {
                                            imbtn[temp].setBackgroundColor(Color.YELLOW);
                                        }
                                        else imbtn[temp].setBackgroundColor(Color.WHITE);
                                    }else imbtn[temp].setBackgroundColor(Color.WHITE);
                                }
                                @Override
                                public void afterTextChanged(Editable s) {
                                }
                            });
                        }
                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getIdModelTemp();
            }
        });

        spn_id.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                for (int i = 0; i < edtl.length; i++){
//                    final int temp = i;
//                    edtl[temp].addTextChangedListener(new TextWatcher() {
//                        @Override
//                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                        }
//                        @Override
//                        public void onTextChanged(CharSequence s, int start, int before, int count) {
//                            chbox1[temp].setEnabled(!edtl_Note[temp].getText().toString().isEmpty());
//                                String abc1 = edtl_Note[temp].getText().toString();
//                                String abc2 = edtl[temp].getText().toString();
//                                if (edtl[temp].length()>0){
//                                    int b1 = Integer.parseInt(abc2);
////                                    if ( value2 > b1 || b1 > value1 ) {
////                                        imbtn[temp].setBackgroundColor(Color.YELLOW);
////                                    }
//                                    if ( b1 > value1 ) {
//                                        imbtn[temp].setBackgroundColor(Color.YELLOW);
//                                    }
//                                    else
//                                        imbtn[temp].setBackgroundColor(Color.WHITE);
//                            }
//                        }
//                        @Override
//                        public void afterTextChanged(Editable s) {
//                        }
//                    });
//                }
                try {
                    getListTemp();
                    getListTempNOTE();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        chk_AllTempUp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CheckBox[] chkl = new CheckBox[]{chk_temp1, chk_temp2, chk_temp3, chk_temp4, chk_temp5, chk_temp6, chk_temp7, chk_temp8, chk_temp9, chk_temp10, chk_temp11, chk_temp12, chk_temp13,
                        chk_temp14, chk_temp15, chk_temp16};
                if (isChecked) {
                    for (CheckBox checkBox : chkl) {
                        if (checkBox.isEnabled()) {
                            checkBox.setChecked(true);
                        }
                    }
                } else {
                    for (CheckBox checkBox : chkl) {
                        if (checkBox.isEnabled()) {
                            checkBox.setChecked(false);
                        }
                    }
                }
            }
        });

        chk_AllTempDown.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CheckBox[] chkl = new CheckBox[]{chk_Dtemp1, chk_Dtemp2, chk_Dtemp3, chk_Dtemp4, chk_Dtemp5, chk_Dtemp6, chk_Dtemp7, chk_Dtemp8, chk_Dtemp9, chk_Dtemp10, chk_Dtemp11, chk_Dtemp12, chk_Dtemp13,
                        chk_Dtemp14, chk_Dtemp15, chk_Dtemp16};
                if (isChecked) {
                    for (CheckBox checkBox : chkl) {
                        if (checkBox.isEnabled()) {
                            checkBox.setChecked(true);
                        }
                    }
                } else {
                    for (CheckBox checkBox : chkl) {
                        if (checkBox.isEnabled()) {
                            checkBox.setChecked(false);
                        }
                    }
                }
            }
        });


        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                creMode = true;
                try {
                    final NOTE_TEMP tpn = new NOTE_TEMP();
                    saveTempNote(tpn);
                    try {
                        CallApi.update_tempnote(factory, tpn, spn_id.getSelectedItem().toString(), new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();

                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ToastCustom.message(checkTempIP.this, "Update Thành Công!", Color.GREEN);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


        btn_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                creMode = true;
                btn_save.setEnabled(true);
                btnSearch.setEnabled(true);
                btnClear.setEnabled(true);
                btn_edit.setEnabled(true);
                Search_May.setEnabled(true);
                Search_Ca.setEnabled(true);
                Search_date.setEnabled(true);

                for(EditText ed : edtl){
                    ed.setEnabled(false);
                }
                for(EditText note : edtl_Note ){
                    note.setEnabled(false);
                }
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(EditText ed : edtl){
                    ed.setEnabled(false);
                    ed.setText("");
                }
                for(EditText note : edtl_Note){
                    note.setEnabled(false);
                    note.setText("");
                }
                btn_edit.setEnabled(false);
                btn_save.setEnabled(false);
                btn_new.setEnabled(true);
                btn_step6.setEnabled(true);
                btnSearch.setEnabled(true);
                ib_logout.setEnabled(true);
                btnClear.setEnabled(true);
            }
        });

    }



    public void getIdModelTemp() {//String date, String ca, String may
        try {
            CallApi.getIdModel(factory, Search_date.getText().toString(), Search_Ca.getText().toString(), Search_May.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    IDmodel iDmodel = gson.fromJson(response.toString(), IDmodel.class);
                    listIDmodel = iDmodel.getItems() == null ? new ArrayList<String>() : iDmodel.getItems();
                    arrIdModel = new ArrayAdapter(checkTempIP.this, R.layout.spn_layout, listIDmodel);
                    arrIdModel.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                    spn_id.setAdapter(arrIdModel);
//                    System.out.println("----------------------------------------------------------------");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getIdModelTemp2() {//String date, String ca, String may
        try {
            CallApi.getIdModel2(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    IDmodel iDmodel = gson.fromJson(response.toString(), IDmodel.class);
                    listIDmodel = iDmodel.getItems() == null ? new ArrayList<String>() : iDmodel.getItems();
                    arrIdModel = new ArrayAdapter(checkTempIP.this, R.layout.spn_layout, listIDmodel);
                    arrIdModel.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
//                    spn_id2.setAdapter(arrIdModel);
                }
            }, new Response.ErrorListener() {
                @Override
                 public void onErrorResponse(VolleyError error) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void saveTempNote(NOTE_TEMP tpn){
        tpn.setMAC(Search_May.getText().toString().equals("") ? "" : Search_May.getText().toString());
        tpn.setSHIFT(Search_Ca.getText().toString().equals("") ? "" : Search_Ca.getText().toString());
        tpn.setUP_DATE(Search_date.getText().toString());
        tpn.setID(spn_id.getSelectedItem().toString());
        tpn.setNOTEMP1(edt_Notemp1.getText().toString().equals("") ? "" : edt_Notemp1.getText().toString());
        tpn.setNOTEMP2(edt_Notemp2.getText().toString().equals("") ? "" : edt_Notemp2.getText().toString());
        tpn.setNOTEMP3(edt_Notemp3.getText().toString().equals("") ? "" : edt_Notemp3.getText().toString());
        tpn.setNOTEMP4(edt_Notemp4.getText().toString().equals("") ? "" : edt_Notemp4.getText().toString());
        tpn.setNOTEMP5(edt_Notemp5.getText().toString().equals("") ? "" : edt_Notemp5.getText().toString());
        tpn.setNOTEMP6(edt_Notemp6.getText().toString().equals("") ? "" : edt_Notemp6.getText().toString());
        tpn.setNOTEMP7(edt_Notemp7.getText().toString().equals("") ? "" : edt_Notemp7.getText().toString());
        tpn.setNOTEMP8(edt_Notemp8.getText().toString().equals("") ? "" : edt_Notemp8.getText().toString());
        tpn.setNOTEMP9(edt_Notemp9.getText().toString().equals("") ? "" : edt_Notemp9.getText().toString());
        tpn.setNOTEMP10(edt_Notemp10.getText().toString().equals("") ? "" : edt_Notemp10.getText().toString());
        tpn.setNOTEMP11(edt_Notemp11.getText().toString().equals("") ? "" : edt_Notemp11.getText().toString());
        tpn.setNOTEMP12(edt_Notemp12.getText().toString().equals("") ? "" : edt_Notemp12.getText().toString());
        tpn.setNOTEMP13(edt_Notemp13.getText().toString().equals("") ? "" : edt_Notemp13.getText().toString());
        tpn.setNOTEMP14(edt_Notemp14.getText().toString().equals("") ? "" : edt_Notemp14.getText().toString());
        tpn.setNOTEMP15(edt_Notemp15.getText().toString().equals("") ? "" : edt_Notemp15.getText().toString());
        tpn.setNOTEMP16(edt_Notemp16.getText().toString().equals("") ? "" : edt_Notemp16.getText().toString());
        tpn.setDNOTEMP1(edt_DNotemp1.getText().toString().equals("") ? "" : edt_DNotemp1.getText().toString());
        tpn.setDNOTEMP2(edt_DNotemp2.getText().toString().equals("") ? "" : edt_DNotemp2.getText().toString());
        tpn.setDNOTEMP3(edt_DNotemp3.getText().toString().equals("") ? "" : edt_DNotemp3.getText().toString());
        tpn.setDNOTEMP4(edt_DNotemp4.getText().toString().equals("") ? "" : edt_DNotemp4.getText().toString());
        tpn.setDNOTEMP5(edt_DNotemp5.getText().toString().equals("") ? "" : edt_DNotemp5.getText().toString());
        tpn.setDNOTEMP6(edt_DNotemp6.getText().toString().equals("") ? "" : edt_DNotemp6.getText().toString());
        tpn.setDNOTEMP7(edt_DNotemp7.getText().toString().equals("") ? "" : edt_DNotemp7.getText().toString());
        tpn.setDNOTEMP8(edt_DNotemp8.getText().toString().equals("") ? "" : edt_DNotemp8.getText().toString());
        tpn.setDNOTEMP9(edt_DNotemp9.getText().toString().equals("") ? "" : edt_DNotemp9.getText().toString());
        tpn.setDNOTEMP10(edt_DNotemp10.getText().toString().equals("") ? "" : edt_DNotemp10.getText().toString());
        tpn.setDNOTEMP11(edt_DNotemp11.getText().toString().equals("") ? "" : edt_DNotemp11.getText().toString());
        tpn.setDNOTEMP12(edt_DNotemp12.getText().toString().equals("") ? "" : edt_DNotemp12.getText().toString());
        tpn.setDNOTEMP13(edt_DNotemp13.getText().toString().equals("") ? "" : edt_DNotemp13.getText().toString());
        tpn.setDNOTEMP14(edt_DNotemp14.getText().toString().equals("") ? "" : edt_DNotemp14.getText().toString());
        tpn.setDNOTEMP15(edt_DNotemp15.getText().toString().equals("") ? "" : edt_DNotemp15.getText().toString());
        tpn.setDNOTEMP16(edt_DNotemp16.getText().toString().equals("") ? "" : edt_DNotemp16.getText().toString());
//        try {
//            CallApi.insertTEMP_NOTE(factory, tpn, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//
//                }
//            });
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        

    }


    public void getListTemp() {
        try {
            CallApi.getTempND(factory, spn_id.getSelectedItem().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    listTND listTND = gson.fromJson(response.toString(), checkTempIP.listTND.class);
                    listID = listTND.getItems() == null ? new ArrayList<TEMP>() : listTND.getItems();

                    edtTcndP.setText(listID.get(0).getNDTCP());
                    edtTcndT.setText(listID.get(0).getNDTCT());

                    listIDx = new String[]{listID.get(0).getEDTNDT1(),listID.get(0).getEDTNDT2(),
                            listID.get(0).getEDTNDT3(),listID.get(0).getEDTNDT4(),
                            listID.get(0).getEDTNDT5(),listID.get(0).getEDTNDT6(),
                            listID.get(0).getEDTNDT7(),listID.get(0).getEDTNDT8(),
                            listID.get(0).getEDTNDT9(),listID.get(0).getEDTNDT10(),
                            listID.get(0).getEDTNDT11(),listID.get(0).getEDTNDT12(),
                            listID.get(0).getEDTNDT13(),listID.get(0).getEDTNDT14(),
                            listID.get(0).getEDTNDT15(),listID.get(0).getEDTNDT16(),

                            listID.get(0).getEDTNDD1(),listID.get(0).getEDTNDD2(),
                            listID.get(0).getEDTNDD3(),listID.get(0).getEDTNDD4(),
                            listID.get(0).getEDTNDD5(),listID.get(0).getEDTNDD6(),
                            listID.get(0).getEDTNDD7(),listID.get(0).getEDTNDD8(),
                            listID.get(0).getEDTNDD9(),listID.get(0).getEDTNDD10(),
                            listID.get(0).getEDTNDD11(),listID.get(0).getEDTNDD12(),
                            listID.get(0).getEDTNDD13(),listID.get(0).getEDTNDD14(),
                            listID.get(0).getEDTNDD15(),listID.get(0).getEDTNDD16(),

                    };

                    for (int i = 0; i < listIDx.length; i++) {
                            edtl[i].setText(listIDx[i]);
                        for (int j = 0; j < edtl.length; j++){
                            edtl_Note[j].setEnabled(!edtl[j].getText().toString().isEmpty());
                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });

            System.out.println("da chay ham");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getListTempNOTE() {
        try {
            CallApi.getTempNOTE(factory, spn_id.getSelectedItem().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    listNOTE listNOTE = gson.fromJson(response.toString(), checkTempIP.listNOTE.class);
                    listID2 = listNOTE.getItems() == null ? new ArrayList<NOTE_TEMP>() : listNOTE.getItems();
                    if (listID2.size() != 0){
                        listIDx2 = new String[]{listID2.get(0).getNOTEMP1(),listID2.get(0).getNOTEMP2(),
                                listID2.get(0).getNOTEMP3(),listID2.get(0).getNOTEMP4(),
                                listID2.get(0).getNOTEMP5(),listID2.get(0).getNOTEMP6(),
                                listID2.get(0).getNOTEMP7(),listID2.get(0).getNOTEMP8(),
                                listID2.get(0).getNOTEMP9(),listID2.get(0).getNOTEMP10(),
                                listID2.get(0).getNOTEMP11(),listID2.get(0).getNOTEMP12(),
                                listID2.get(0).getNOTEMP13(),listID2.get(0).getNOTEMP14(),
                                listID2.get(0).getNOTEMP15(),listID2.get(0).getNOTEMP16(),

                                listID2.get(0).getDNOTEMP1(),listID2.get(0).getDNOTEMP2(),
                                listID2.get(0).getDNOTEMP3(),listID2.get(0).getDNOTEMP4(),
                                listID2.get(0).getDNOTEMP5(),listID2.get(0).getDNOTEMP6(),
                                listID2.get(0).getDNOTEMP7(),listID2.get(0).getDNOTEMP8(),
                                listID2.get(0).getDNOTEMP9(),listID2.get(0).getDNOTEMP10(),
                                listID2.get(0).getDNOTEMP11(),listID2.get(0).getDNOTEMP12(),
                                listID2.get(0).getDNOTEMP13(),listID2.get(0).getDNOTEMP14(),
                                listID2.get(0).getDNOTEMP15(),listID2.get(0).getDNOTEMP16(),
                        };
                        for (int i = 0; i < listIDx2.length; i++) {
                            edtl_Note[i].setText(listIDx2[i]);
                            if (listIDx2[i] != null) {
                                imbtn[i].setBackgroundResource(R.drawable.mybutton);
                            } else imbtn[i].setBackgroundResource(R.drawable.white_button);
                        }
                        for (int i = 0; i < imbtn.length; i++) {
                            final int col = i;
                            imbtn[col].setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    note_dialog(posNote[col],listIDx2[col],spn_id.getSelectedItem().toString(),imbtn[col], col);
                                }
                            });
                        }

                    } else {
                        edt_Notemp1.setText("");edt_Notemp2.setText("");
                        edt_Notemp3.setText("");edt_Notemp4.setText("");
                        edt_Notemp5.setText("");edt_Notemp6.setText("");
                        edt_Notemp7.setText("");edt_Notemp8.setText("");
                        edt_Notemp9.setText("");edt_Notemp10.setText("");
                        edt_Notemp11.setText("");edt_Notemp12.setText("");
                        edt_Notemp13.setText("");edt_Notemp14.setText("");
                        edt_Notemp15.setText("");edt_Notemp16.setText("");

                        edt_DNotemp1.setText("");edt_DNotemp2.setText("");
                        edt_DNotemp3.setText("");edt_DNotemp4.setText("");
                        edt_DNotemp5.setText("");edt_DNotemp6.setText("");
                        edt_DNotemp7.setText("");edt_DNotemp8.setText("");
                        edt_DNotemp9.setText("");edt_DNotemp10.setText("");
                        edt_DNotemp11.setText("");edt_DNotemp12.setText("");
                        edt_DNotemp13.setText("");edt_DNotemp14.setText("");
                        edt_DNotemp15.setText("");edt_DNotemp16.setText("");
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });

            System.out.println("da chay ham");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void note_dialog(final String position, final String value, final String note_no, final ImageButton imbtna, final int dain){
        LayoutInflater inflater = getLayoutInflater();
        final View noteLayout = inflater.inflate(R.layout.dialog_note, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(checkTempIP.this);
        alert.setView(noteLayout);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
//        final ImageButton ibtnClear = noteLayout.findViewById(R.id.btn_clear);
        final Button save = noteLayout.findViewById(R.id.btn_save);
        final Button close = noteLayout.findViewById(R.id.btn_close);
        final EditText edtNote = noteLayout.findViewById(R.id.edt_note);

        edtNote.setText(value);
        save.setEnabled(false);
        edtNote.setEnabled(false);
//        if (creMode==true){
//            save.setEnabled(true);
//            edtNote.setEnabled(true);
//            edtNote.setFocusable(true);
//        }
//        else {
//            edtNote.setEnabled(false);
//            edtNote.setFocusable(false);
//            save.setEnabled(false);
//        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    CallApi.updatenote_temp(factory, position, note_no, edtNote.getText().toString(), new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            listIDx2[dain]=edtNote.getText().toString();
                            ToastCustom.message(checkTempIP.this, "Lưu thành công!", Color.GREEN);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
                }catch (JSONException e){
                    e.printStackTrace();
                    ToastCustom.message(checkTempIP.this, "Lưu thất bại!", Color.RED);
                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (edtNote.getText().toString().equals("")){
//                    imbtna.setBackgroundResource(R.drawable.white_button);
//                }else imbtna.setBackgroundResource(R.drawable.mybutton);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void FindID() {
        btnClear = findViewById(R.id.btnClear);
        btn_save = findViewById(R.id.btn_save);
        btn_new = findViewById(R.id.btn_new);
        btn_edit = findViewById(R.id.btn_edit);
        chk_AllTempUp = findViewById(R.id.chk_AllTempUp);
        chk_AllTempDown = findViewById(R.id.chk_AllTempDown);

        btnSearch = findViewById(R.id.btnSearch);
        Search_date = findViewById(R.id.Search_date);
        Search_May = findViewById(R.id.Search_May);
        Search_Ca = findViewById(R.id.Search_Ca);
        spn_id = findViewById(R.id.spn_id);
        spr_shift = findViewById(R.id.spr_shift);
        spr_shift2 = findViewById(R.id.spr_shift2);
        ib_logout = findViewById(R.id.ib_logout);
        btn_step6 = findViewById(R.id.btn_step6);
        edt_Temp1 = findViewById(R.id.edt_Temp1);
        edt_Temp2 = findViewById(R.id.edt_Temp2);
        edt_Temp3 = findViewById(R.id.edt_Temp3);
        edt_Temp4 = findViewById(R.id.edt_Temp4);
        edt_Temp5 = findViewById(R.id.edt_Temp5);
        edt_Temp6 = findViewById(R.id.edt_Temp6);
        edt_Temp7 = findViewById(R.id.edt_Temp7);
        edt_Temp8 = findViewById(R.id.edt_Temp8);
        edt_Temp9 = findViewById(R.id.edt_Temp9);
        edt_Temp10 = findViewById(R.id.edt_Temp10);
        edt_Temp11 = findViewById(R.id.edt_Temp11);
        edt_Temp12 = findViewById(R.id.edt_Temp12);
        edt_Temp13 = findViewById(R.id.edt_Temp13);
        edt_Temp14 = findViewById(R.id.edt_Temp14);
        edt_Temp15 = findViewById(R.id.edt_Temp15);
        edt_Temp16 = findViewById(R.id.edt_Temp16);

        edt_Dtemp1 = findViewById(R.id.edt_Dtemp1);
        edt_Dtemp2 = findViewById(R.id.edt_Dtemp2);
        edt_Dtemp3 = findViewById(R.id.edt_Dtemp3);
        edt_Dtemp4 = findViewById(R.id.edt_Dtemp4);
        edt_Dtemp5 = findViewById(R.id.edt_Dtemp5);
        edt_Dtemp6 = findViewById(R.id.edt_Dtemp6);
        edt_Dtemp7 = findViewById(R.id.edt_Dtemp7);
        edt_Dtemp8 = findViewById(R.id.edt_Dtemp8);
        edt_Dtemp9 = findViewById(R.id.edt_Dtemp9);
        edt_Dtemp10 = findViewById(R.id.edt_Dtemp10);
        edt_Dtemp11 = findViewById(R.id.edt_Dtemp11);
        edt_Dtemp12 = findViewById(R.id.edt_Dtemp12);
        edt_Dtemp13 = findViewById(R.id.edt_Dtemp13);
        edt_Dtemp14 = findViewById(R.id.edt_Dtemp14);
        edt_Dtemp15 = findViewById(R.id.edt_Dtemp15);
        edt_Dtemp16 = findViewById(R.id.edt_Dtemp16);

        edt_Notemp1 = findViewById(R.id.edt_Notemp1);
        edt_Notemp2 = findViewById(R.id.edt_Notemp2);
        edt_Notemp3 = findViewById(R.id.edt_Notemp3);
        edt_Notemp4 = findViewById(R.id.edt_Notemp4);
        edt_Notemp5 = findViewById(R.id.edt_Notemp5);
        edt_Notemp6 = findViewById(R.id.edt_Notemp6);
        edt_Notemp7 = findViewById(R.id.edt_Notemp7);
        edt_Notemp8 = findViewById(R.id.edt_Notemp8);
        edt_Notemp9 = findViewById(R.id.edt_Notemp9);
        edt_Notemp10 = findViewById(R.id.edt_Notemp10);
        edt_Notemp11 = findViewById(R.id.edt_Notemp11);
        edt_Notemp12 = findViewById(R.id.edt_Notemp12);
        edt_Notemp13 = findViewById(R.id.edt_Notemp13);
        edt_Notemp14 = findViewById(R.id.edt_Notemp14);
        edt_Notemp15 = findViewById(R.id.edt_Notemp15);
        edt_Notemp16 = findViewById(R.id.edt_Notemp16);

        edt_DNotemp1 = findViewById(R.id.edt_DNotemp1);
        edt_DNotemp2 = findViewById(R.id.edt_DNotemp2);
        edt_DNotemp3 = findViewById(R.id.edt_DNotemp3);
        edt_DNotemp4 = findViewById(R.id.edt_DNotemp4);
        edt_DNotemp5 = findViewById(R.id.edt_DNotemp5);
        edt_DNotemp6 = findViewById(R.id.edt_DNotemp6);
        edt_DNotemp7 = findViewById(R.id.edt_DNotemp7);
        edt_DNotemp8 = findViewById(R.id.edt_DNotemp8);
        edt_DNotemp9 = findViewById(R.id.edt_DNotemp9);
        edt_DNotemp10 = findViewById(R.id.edt_DNotemp10);
        edt_DNotemp11 = findViewById(R.id.edt_DNotemp11);
        edt_DNotemp12 = findViewById(R.id.edt_DNotemp12);
        edt_DNotemp13 = findViewById(R.id.edt_DNotemp13);
        edt_DNotemp14 = findViewById(R.id.edt_DNotemp14);
        edt_DNotemp15 = findViewById(R.id.edt_DNotemp15);
        edt_DNotemp16 = findViewById(R.id.edt_DNotemp16);

        chk_temp1 = findViewById(R.id.chk_temp1);
        chk_temp2 = findViewById(R.id.chk_temp2);
        chk_temp3 = findViewById(R.id.chk_temp3);
        chk_temp4 = findViewById(R.id.chk_temp4);
        chk_temp5 = findViewById(R.id.chk_temp5);
        chk_temp6 = findViewById(R.id.chk_temp6);
        chk_temp7 = findViewById(R.id.chk_temp7);
        chk_temp8 = findViewById(R.id.chk_temp8);
        chk_temp9 = findViewById(R.id.chk_temp9);
        chk_temp10 = findViewById(R.id.chk_temp10);
        chk_temp11 = findViewById(R.id.chk_temp11);
        chk_temp12 = findViewById(R.id.chk_temp12);
        chk_temp13 = findViewById(R.id.chk_temp13);
        chk_temp14 = findViewById(R.id.chk_temp14);
        chk_temp15 = findViewById(R.id.chk_temp15);
        chk_temp16 = findViewById(R.id.chk_temp16);

        chk_Dtemp1 = findViewById(R.id.chk_Dtemp1);
        chk_Dtemp2 = findViewById(R.id.chk_Dtemp2);
        chk_Dtemp3 = findViewById(R.id.chk_Dtemp3);
        chk_Dtemp4 = findViewById(R.id.chk_Dtemp4);
        chk_Dtemp5 = findViewById(R.id.chk_Dtemp5);
        chk_Dtemp6 = findViewById(R.id.chk_Dtemp6);
        chk_Dtemp7 = findViewById(R.id.chk_Dtemp7);
        chk_Dtemp8 = findViewById(R.id.chk_Dtemp8);
        chk_Dtemp9 = findViewById(R.id.chk_Dtemp9);
        chk_Dtemp10 = findViewById(R.id.chk_Dtemp10);
        chk_Dtemp11 = findViewById(R.id.chk_Dtemp11);
        chk_Dtemp12 = findViewById(R.id.chk_Dtemp12);
        chk_Dtemp13 = findViewById(R.id.chk_Dtemp13);
        chk_Dtemp14 = findViewById(R.id.chk_Dtemp14);
        chk_Dtemp15 = findViewById(R.id.chk_Dtemp15);
        chk_Dtemp16 = findViewById(R.id.chk_Dtemp16);

        ibtn_temp1 = findViewById(R.id.ibtn_temp1);
        ibtn_temp2 = findViewById(R.id.ibtn_temp2);
        ibtn_temp3 = findViewById(R.id.ibtn_temp3);
        ibtn_temp4 = findViewById(R.id.ibtn_temp4);
        ibtn_temp5 = findViewById(R.id.ibtn_temp5);
        ibtn_temp6 = findViewById(R.id.ibtn_temp6);
        ibtn_temp7 = findViewById(R.id.ibtn_temp7);
        ibtn_temp8 = findViewById(R.id.ibtn_temp8);
        ibtn_temp9 = findViewById(R.id.ibtn_temp9);
        ibtn_temp10 = findViewById(R.id.ibtn_temp10);
        ibtn_temp11 = findViewById(R.id.ibtn_temp11);
        ibtn_temp12 = findViewById(R.id.ibtn_temp12);
        ibtn_temp13 = findViewById(R.id.ibtn_temp13);
        ibtn_temp14 = findViewById(R.id.ibtn_temp14);
        ibtn_temp15 = findViewById(R.id.ibtn_temp15);
        ibtn_temp16 = findViewById(R.id.ibtn_temp16);

        ibtn_Dtemp1 = findViewById(R.id.ibtn_Dtemp1);
        ibtn_Dtemp2 = findViewById(R.id.ibtn_Dtemp2);
        ibtn_Dtemp3 = findViewById(R.id.ibtn_Dtemp3);
        ibtn_Dtemp4 = findViewById(R.id.ibtn_Dtemp4);
        ibtn_Dtemp5 = findViewById(R.id.ibtn_Dtemp5);
        ibtn_Dtemp6 = findViewById(R.id.ibtn_Dtemp6);
        ibtn_Dtemp7 = findViewById(R.id.ibtn_Dtemp7);
        ibtn_Dtemp8 = findViewById(R.id.ibtn_Dtemp8);
        ibtn_Dtemp9 = findViewById(R.id.ibtn_Dtemp9);
        ibtn_Dtemp10 = findViewById(R.id.ibtn_Dtemp10);
        ibtn_Dtemp11 = findViewById(R.id.ibtn_Dtemp11);
        ibtn_Dtemp12 = findViewById(R.id.ibtn_Dtemp12);
        ibtn_Dtemp13 = findViewById(R.id.ibtn_Dtemp13);
        ibtn_Dtemp14 = findViewById(R.id.ibtn_Dtemp14);
        ibtn_Dtemp15 = findViewById(R.id.ibtn_Dtemp15);
        ibtn_Dtemp16 = findViewById(R.id.ibtn_Dtemp16);

        edtTcndT = findViewById(R.id.edtTcndT);
        edtTcndP = findViewById(R.id.edtTcndP);


    }

    public class listTND extends ApiReturnModel<ArrayList<TEMP>> {
    }
    public class listNOTE extends ApiReturnModel<ArrayList<NOTE_TEMP>> {
    }

}
