package com.deanshoes.AppQAM.step2.fvi;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.IpPrefix;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.ListAMTAdapter;
import com.deanshoes.AppQAM.adapter.ListMacAdapter;
import com.deanshoes.AppQAM.fragment.IP_BatchFragment;
import com.deanshoes.AppQAM.model.QAM.MCS;
import com.deanshoes.AppQAM.model.QAM.QAM_AMT;
import com.deanshoes.AppQAM.model.QAM.QAM_INS;
import com.deanshoes.AppQAM.model.QAM.QAM_MAC;
import com.deanshoes.AppQAM.model.QAM.QAM_NOTE;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_HAND;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_MAC1;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_MAC2;
import com.deanshoes.AppQAM.model.QAM.QAM_TEMP_MODEL;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step1.ChemicalActivity;
import com.deanshoes.AppQAM.step1.MaterialActivity;
import com.deanshoes.AppQAM.step3.FVI.Kneader_Controller;
import com.deanshoes.AppQAM.step4.Temp1;
import com.deanshoes.AppQAM.step5.mac1fvi2;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.poi.hssf.util.HSSFColor;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class IPBatchActivity_FVI extends AppCompatActivity {
    private Spinner spr_groupID,spr_shift;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private List<MCS> mcsinfor;
    QAM_AMT amt;

    List<QAM_INS> insl;

    ArrayList<String> arrGroupID;

    ArrayList<QAM_INS> arrINS;

    private ArrayList<QAM_AMT> amtlst;
    public  ArrayList<QAM_SCALE_MAC1> lstMac1;
    public  ArrayList<QAM_SCALE_MAC2> lstMac2;
    public  ArrayList<QAM_SCALE_HAND> lstMachand;

    private ArrayList<QAM_NOTE> notelst;
    DatePickerDialog datePickerDialog;
    String factory = "",user="";
    String mcs_no,line,ins_no,mcs_noID, mcs;

    EditText edt_date,edt_batchno,edt_style,edt_color,edt_hard,tv_mcs; //,tv_mcs

    EditText edt_Mraw1,edt_Mraw2,edt_Mraw3,edt_Mraw4,edt_Mraw5,edt_Mraw6,edt_Mraw7,edt_Mraw8,edt_Mraw9,edt_Mraw10;
    EditText edt_Mchem1,edt_Mchem2,edt_Mchem3,edt_Mchem4,edt_Mchem5,edt_Mchem6,edt_Mchem7,edt_Mchem8,edt_Mchem9,edt_Mchem10;
    EditText edt_Mchem11,edt_Mchem12,edt_Mchem13,edt_Mchem14,edt_Mchem15,edt_Mchem16,edt_Mchem17,edt_Mchem18,edt_Mchem19,edt_Mchem20;
    EditText edt_Mpig1,edt_Mpig2,edt_Mpig3,edt_Mpig4,edt_Mpig5,edt_Mpig6,edt_Mpig7,edt_Mpig8,edt_Mpig9,edt_Mpig10;

    EditText edt_raw1,edt_raw2,edt_raw3,edt_raw4,edt_raw5,edt_raw6,edt_raw7,edt_raw8,edt_raw9,edt_raw10;
    EditText edt_chem1,edt_chem2,edt_chem3,edt_chem4,edt_chem5,edt_chem6,edt_chem7,edt_chem8,edt_chem9,edt_chem10;
    EditText edt_chem11,edt_chem12,edt_chem13,edt_chem14,edt_chem15,edt_chem16,edt_chem17,edt_chem18,edt_chem19,edt_chem20;
    EditText edt_pig1,edt_pig2,edt_pig3,edt_pig4,edt_pig5,edt_pig6,edt_pig7,edt_pig8,edt_pig9,edt_pig10;

    CheckBox chk_raw1,chk_raw2,chk_raw3,chk_raw4,chk_raw5,chk_raw6,chk_raw7,chk_raw8,chk_raw9,chk_raw10;
    CheckBox chk_chem1,chk_chem2,chk_chem3,chk_chem4,chk_chem5,chk_chem6,chk_chem7,chk_chem8,chk_chem9,chk_chem10,
            chk_chem11,chk_chem12,chk_chem13,chk_chem14,chk_chem15,chk_chem16,chk_chem17,chk_chem18,chk_chem19,chk_chem20;
    CheckBox chk_pig1,chk_pig2,chk_pig3,chk_pig4,chk_pig5,chk_pig6,chk_pig7,chk_pig8,chk_pig9,chk_pig10;
    CheckBox chk_Allraw,chk_Allchem,chk_Allpig, chk_can1,chk_can2,chk_cantay;

    ImageButton ibtn_raw1,ibtn_raw2,ibtn_raw3,ibtn_raw4,ibtn_raw5,ibtn_raw6, ibtn_raw7,ibtn_raw8,ibtn_raw9,ibtn_raw10;
    ImageButton ibtn_chem1,ibtn_chem2,ibtn_chem3,ibtn_chem4,ibtn_chem5,ibtn_chem6,ibtn_chem7,ibtn_chem8,ibtn_chem9,ibtn_chem10,
            ibtn_chem11,ibtn_chem12,ibtn_chem13,ibtn_chem14,ibtn_chem15,ibtn_chem16,ibtn_chem17,ibtn_chem18,ibtn_chem19,ibtn_chem20;
    ImageButton ibtn_pig1,ibtn_pig2,ibtn_pig3,ibtn_pig4,ibtn_pig5,ibtn_pig6,ibtn_pig7,ibtn_pig8,ibtn_pig9,ibtn_pig10;
    ImageButton ib_logout;

    TextView tv_raw11111, tv_ngay, tv_rawtong1, tv_rawtong2, tv_rawtongtay, tv_rawtong,
            tv_chemtong1, tv_chemtong2, tv_chemtongtay, tv_chemtong,
            tv_pigtong1, tv_pigtong2, tv_pigtongtay, tv_pigtong, tv_may1, tv_may2;

    Button btn_edit,btn_save,btn_cancel,btn_step1,btn_step2,btn_step4,btn_part1,btn_part4;

    private ArrayList<String> listShift,listGroupID;
    private ArrayAdapter mcsAdapter,ShiftAdapter,MacAdapter;
    ListAMTAdapter listAMTAdapter;
    ListView lv_amt;
    int posAMT=-1;
    CheckBox[] chkl, chklcan;
    EditText[] edtl, edtlcan;
    TextView[] tvl;
    ImageButton[] ibtn;
    String[] strl2,strl,posNote,valNote, strl3;
    boolean creMode=false;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ipbatch_fvi);

        Intent intent = getIntent();
        final Bundle bundle = intent.getExtras();
        if (bundle != null) {
            user = bundle.getString("user", "");
            factory = bundle.getString("fac");
        }

        fintViewbyId();
        chkl = new CheckBox[]{chk_raw1, chk_raw2, chk_raw3, chk_raw4, chk_raw5, chk_raw6, chk_raw7, chk_raw8, chk_raw9, chk_raw10,
                chk_chem1, chk_chem2, chk_chem3, chk_chem4, chk_chem5, chk_chem6, chk_chem7, chk_chem8, chk_chem9, chk_chem10,
                chk_chem11, chk_chem12, chk_chem13, chk_chem14, chk_chem15, chk_chem16, chk_chem17, chk_chem18, chk_chem19, chk_chem20,
                chk_pig1, chk_pig2, chk_pig3, chk_pig4, chk_pig5, chk_pig6, chk_pig7, chk_pig8, chk_pig9, chk_pig10};

        chklcan = new CheckBox[]{chk_raw1, chk_raw2, chk_raw3, chk_raw4, chk_raw5,
                chk_raw6, chk_raw7, chk_raw8, chk_raw9, chk_raw10,
                chk_pig1, chk_pig2, chk_pig3, chk_pig4, chk_pig5, chk_pig6, chk_pig7, chk_pig8, chk_pig9, chk_pig10,
                chk_chem1, chk_chem2, chk_chem3, chk_chem4, chk_chem5, chk_chem6, chk_chem7, chk_chem8, chk_chem9, chk_chem10,
                chk_chem11, chk_chem12, chk_chem13, chk_chem14, chk_chem15, chk_chem16, chk_chem17, chk_chem18, chk_chem19, chk_chem20
        };

        edtl = new EditText[]{edt_Mraw1, edt_Mraw2, edt_Mraw3, edt_Mraw4, edt_Mraw5, edt_Mraw6, edt_Mraw7, edt_Mraw8, edt_Mraw9, edt_Mraw10,
                edt_Mchem1, edt_Mchem2, edt_Mchem3, edt_Mchem4, edt_Mchem5, edt_Mchem6, edt_Mchem7, edt_Mchem8, edt_Mchem9, edt_Mchem10,
                edt_Mchem11, edt_Mchem12, edt_Mchem13, edt_Mchem14, edt_Mchem15, edt_Mchem16, edt_Mchem17, edt_Mchem18, edt_Mchem19, edt_Mchem20,
                edt_Mpig1, edt_Mpig2, edt_Mpig3, edt_Mpig4, edt_Mpig5, edt_Mpig6, edt_Mpig7, edt_Mpig8, edt_Mpig9, edt_Mpig10,
                edt_raw1, edt_raw2, edt_raw3, edt_raw4, edt_raw5, edt_raw6, edt_raw7, edt_raw8, edt_raw9, edt_raw10,
                edt_chem1, edt_chem2, edt_chem3, edt_chem4, edt_chem5, edt_chem6, edt_chem7, edt_chem8, edt_chem9, edt_chem10,
                edt_chem11, edt_chem12, edt_chem13, edt_chem14, edt_chem15, edt_chem16, edt_chem17, edt_chem18, edt_chem19, edt_chem20,
                edt_pig1, edt_pig2, edt_pig3, edt_pig4, edt_pig5, edt_pig6, edt_pig7, edt_pig8, edt_pig9, edt_pig10};

        tvl = new TextView[]{tv_rawtong1, tv_rawtong2, tv_rawtongtay, tv_may1,
                tv_chemtong1, tv_chemtong2, tv_chemtongtay, tv_may2,
                tv_pigtong1, tv_pigtong2, tv_pigtongtay, tv_pigtong};

        edtlcan = new EditText[]{edt_Mraw1, edt_Mraw2, edt_Mraw3, edt_Mraw4, edt_Mraw5,
                edt_Mraw6, edt_Mraw7, edt_Mraw8, edt_Mraw9, edt_Mraw10,
                edt_Mpig1, edt_Mpig2, edt_Mpig3, edt_Mpig4, edt_Mpig5, edt_Mpig6, edt_Mpig7, edt_Mpig8, edt_Mpig9, edt_Mpig10,
                edt_Mchem1, edt_Mchem2, edt_Mchem3, edt_Mchem4, edt_Mchem5, edt_Mchem6, edt_Mchem7, edt_Mchem8, edt_Mchem9, edt_Mchem10,
                edt_Mchem11, edt_Mchem12, edt_Mchem13, edt_Mchem14, edt_Mchem15, edt_Mchem16, edt_Mchem17, edt_Mchem18, edt_Mchem19, edt_Mchem20
        };

        ibtn = new ImageButton[]{ibtn_raw1, ibtn_raw2, ibtn_raw3, ibtn_raw4, ibtn_raw5, ibtn_raw6, ibtn_raw7, ibtn_raw8, ibtn_raw9, ibtn_raw10,
                ibtn_chem1, ibtn_chem2, ibtn_chem3, ibtn_chem4, ibtn_chem5, ibtn_chem6, ibtn_chem7, ibtn_chem8, ibtn_chem9, ibtn_chem10,
                ibtn_chem11, ibtn_chem12, ibtn_chem13, ibtn_chem14, ibtn_chem15, ibtn_chem16, ibtn_chem17, ibtn_chem18, ibtn_chem19, ibtn_chem20,
                ibtn_pig1, ibtn_pig2, ibtn_pig3, ibtn_pig4, ibtn_pig5, ibtn_pig6, ibtn_pig7, ibtn_pig8, ibtn_pig9, ibtn_pig10};

        posNote = new String[]{"NOTE_MTR1", "NOTE_MTR2", "NOTE_MTR3", "NOTE_MTR4", "NOTE_MTR5", "NOTE_MTR6", "NOTE_MTR7", "NOTE_MTR8", "NOTE_MTR9", "NOTE_MTR10",
                "NOTE_CHEM1", "NOTE_CHEM2", "NOTE_CHEM3", "NOTE_CHEM4", "NOTE_CHEM5", "NOTE_CHEM6", "NOTE_CHEM7", "NOTE_CHEM8", "NOTE_CHEM9", "NOTE_CHEM10",
                "NOTE_CHEM11", "NOTE_CHEM12", "NOTE_CHEM13", "NOTE_CHEM14", "NOTE_CHEM15", "NOTE_CHEM16", "NOTE_CHEM17", "NOTE_CHEM18", "NOTE_CHEM19", "NOTE_CHEM20",
                "NOTE_PIG1", "NOTE_PIG2", "NOTE_PIG3", "NOTE_PIG4", "NOTE_PIG5", "NOTE_PIG6", "NOTE_PIG7", "NOTE_PIG8", "NOTE_PIG9", "NOTE_PIG10"};

        defaultSpinner();
        setDisable();

        edt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog(edt_date);
            }
        });

        spr_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (!edt_date.getText().toString().isEmpty()) {
                        spr_groupID.setEnabled(true);
                        getGroupID(spr_shift.getSelectedItem().toString(), edt_date.getText().toString());

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(IPBatchActivity_FVI.this, LoginActivity.class);
                startActivity(i);
            }
        });


        spr_groupID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = spr_groupID.getSelectedItem().toString();
                String[] ins_no1 = spr_groupID.getSelectedItem().toString().split("_");
                ins_no = ins_no1[0];
                mcs = ins_no1[2];
                for (int i = 0; i < arrGroupID.size(); i++) {
                    if (item.equals(arrINS.get(i).getINS_NO()) && position == i) {
                        mcs_no = arrINS.get(i).getMCS_NO();
                        line = arrINS.get(i).getINS_LINE();
                        getInfor(mcs_no);
                        getAMT(ins_no, edt_date.getText().toString(), spr_shift.getSelectedItem().toString(), line);
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        for (ImageView ibtnl : ibtn) {
            ibtnl.setEnabled(false);
        }

        lv_amt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                if (viewId == R.id.rdo_check) {
                    for (ImageView ibtnl : ibtn) {
                        ibtnl.setEnabled(true);
                    }
                    btn_edit.setEnabled(true);
                    posAMT = position;
                    clear();
                    edt_batchno.setText(amtlst.get(posAMT).getBTC_BATCH_NO());
                    if (amtlst.get(posAMT).getAMT_CHECK().equals("Y")) btn_edit.setEnabled(false);

                    strl = new String[]{amtlst.get(posAMT).getMAC_MTR1(), amtlst.get(posAMT).getMAC_MTR2(),
                            amtlst.get(posAMT).getMAC_MTR3(), amtlst.get(posAMT).getMAC_MTR4(),
                            amtlst.get(posAMT).getMAC_MTR5(), amtlst.get(posAMT).getMAC_MTR6(),
                            amtlst.get(posAMT).getMAC_MTR7(), amtlst.get(posAMT).getMAC_MTR8(),
                            amtlst.get(posAMT).getMAC_MTR9(), amtlst.get(posAMT).getMAC_MTR10(),

                            amtlst.get(posAMT).getMAC_CHEM1(), amtlst.get(posAMT).getMAC_CHEM2(),
                            amtlst.get(posAMT).getMAC_CHEM3(), amtlst.get(posAMT).getMAC_CHEM4(),
                            amtlst.get(posAMT).getMAC_CHEM5(), amtlst.get(posAMT).getMAC_CHEM6(),
                            amtlst.get(posAMT).getMAC_CHEM7(), amtlst.get(posAMT).getMAC_CHEM8(),
                            amtlst.get(posAMT).getMAC_CHEM9(), amtlst.get(posAMT).getMAC_CHEM10(),
                            amtlst.get(posAMT).getMAC_CHEM11(), amtlst.get(posAMT).getMAC_CHEM12(),
                            amtlst.get(posAMT).getMAC_CHEM13(), amtlst.get(posAMT).getMAC_CHEM14(),
                            amtlst.get(posAMT).getMAC_CHEM15(), amtlst.get(posAMT).getMAC_CHEM16(),
                            amtlst.get(posAMT).getMAC_CHEM17(), amtlst.get(posAMT).getMAC_CHEM18(),
                            amtlst.get(posAMT).getMAC_CHEM19(), amtlst.get(posAMT).getMAC_CHEM20(),

                            amtlst.get(posAMT).getMAC_PIG1(), amtlst.get(posAMT).getMAC_PIG2(),
                            amtlst.get(posAMT).getMAC_PIG3(), amtlst.get(posAMT).getMAC_PIG4(),
                            amtlst.get(posAMT).getMAC_PIG5(), amtlst.get(posAMT).getMAC_PIG6(),
                            amtlst.get(posAMT).getMAC_PIG7(), amtlst.get(posAMT).getMAC_PIG8(),
                            amtlst.get(posAMT).getMAC_PIG9(), amtlst.get(posAMT).getMAC_PIG10(),


                            amtlst.get(posAMT).getSUB_MTR1(), amtlst.get(posAMT).getSUB_MTR2(), amtlst.get(posAMT).getSUB_MTR3(),
                            amtlst.get(posAMT).getSUB_MTR4(), amtlst.get(posAMT).getSUB_MTR5(), amtlst.get(posAMT).getSUB_MTR6(), amtlst.get(posAMT).getSUB_MTR7(),
                            amtlst.get(posAMT).getSUB_MTR8(), amtlst.get(posAMT).getSUB_MTR9(), amtlst.get(posAMT).getSUB_MTR10(), amtlst.get(posAMT).getSUB_CHEM1(),
                            amtlst.get(posAMT).getSUB_CHEM2(), amtlst.get(posAMT).getSUB_CHEM3(), amtlst.get(posAMT).getSUB_CHEM4(), amtlst.get(posAMT).getSUB_CHEM5(),
                            amtlst.get(posAMT).getSUB_CHEM6(), amtlst.get(posAMT).getSUB_CHEM7(), amtlst.get(posAMT).getSUB_CHEM8(), amtlst.get(posAMT).getSUB_CHEM9(),
                            amtlst.get(posAMT).getSUB_CHEM10(), amtlst.get(posAMT).getSUB_CHEM11(), amtlst.get(posAMT).getSUB_CHEM12(), amtlst.get(posAMT).getSUB_CHEM13(),
                            amtlst.get(posAMT).getSUB_CHEM14(), amtlst.get(posAMT).getSUB_CHEM15(), amtlst.get(posAMT).getSUB_CHEM16(), amtlst.get(posAMT).getSUB_CHEM17(),
                            amtlst.get(posAMT).getSUB_CHEM18(), amtlst.get(posAMT).getSUB_CHEM19(), amtlst.get(posAMT).getSUB_CHEM20(), amtlst.get(posAMT).getSUB_PIG1(),
                            amtlst.get(posAMT).getSUB_PIG2(), amtlst.get(posAMT).getSUB_PIG3(), amtlst.get(posAMT).getSUB_PIG4(), amtlst.get(posAMT).getSUB_PIG5(),
                            amtlst.get(posAMT).getSUB_PIG6(), amtlst.get(posAMT).getSUB_PIG7(), amtlst.get(posAMT).getSUB_PIG8(), amtlst.get(posAMT).getSUB_PIG9(),
                            amtlst.get(posAMT).getSUB_PIG10()};

                    for (int i = 0; i < edtl.length; i++) {
                        if (strl[i] != null) {
                            edtl[i].setVisibility(View.VISIBLE);
                            edtl[i].setText(strl[i]);
                        }
                        else {
                            edtl[i].setVisibility(View.INVISIBLE);
                        }
                    }

                    strl2 = new String[]{amtlst.get(posAMT).getCHK_MTR1(), amtlst.get(posAMT).getCHK_MTR2(), amtlst.get(posAMT).getCHK_MTR3(),
                            amtlst.get(posAMT).getCHK_MTR4(), amtlst.get(posAMT).getCHK_MTR5(), amtlst.get(posAMT).getCHK_MTR6(), amtlst.get(posAMT).getCHK_MTR7(),
                            amtlst.get(posAMT).getCHK_MTR8(), amtlst.get(posAMT).getCHK_MTR9(), amtlst.get(posAMT).getCHK_MTR10(), amtlst.get(posAMT).getCHK_CHEM1(),
                            amtlst.get(posAMT).getCHK_CHEM2(), amtlst.get(posAMT).getCHK_CHEM3(), amtlst.get(posAMT).getCHK_CHEM4(), amtlst.get(posAMT).getCHK_CHEM5(),
                            amtlst.get(posAMT).getCHK_CHEM6(), amtlst.get(posAMT).getCHK_CHEM7(), amtlst.get(posAMT).getCHK_CHEM8(), amtlst.get(posAMT).getCHK_CHEM9(),
                            amtlst.get(posAMT).getCHK_CHEM10(), amtlst.get(posAMT).getCHK_CHEM11(), amtlst.get(posAMT).getCHK_CHEM12(), amtlst.get(posAMT).getCHK_CHEM13(),
                            amtlst.get(posAMT).getCHK_CHEM14(), amtlst.get(posAMT).getCHK_CHEM15(), amtlst.get(posAMT).getCHK_CHEM16(), amtlst.get(posAMT).getCHK_CHEM17(),
                            amtlst.get(posAMT).getCHK_CHEM18(), amtlst.get(posAMT).getCHK_CHEM19(), amtlst.get(posAMT).getCHK_CHEM20(), amtlst.get(posAMT).getCHK_PIG1(),
                            amtlst.get(posAMT).getCHK_PIG2(), amtlst.get(posAMT).getCHK_PIG3(), amtlst.get(posAMT).getCHK_PIG4(), amtlst.get(posAMT).getCHK_PIG5(),
                            amtlst.get(posAMT).getCHK_PIG6(), amtlst.get(posAMT).getCHK_PIG7(), amtlst.get(posAMT).getCHK_PIG8(), amtlst.get(posAMT).getCHK_PIG9(),
                            amtlst.get(posAMT).getCHK_PIG10()};

                    valNote = new String[]{amtlst.get(posAMT).getNOTE_MTR1(), amtlst.get(posAMT).getNOTE_MTR2(), amtlst.get(posAMT).getNOTE_MTR3(),
                            amtlst.get(posAMT).getNOTE_MTR4(), amtlst.get(posAMT).getNOTE_MTR5(), amtlst.get(posAMT).getNOTE_MTR6(), amtlst.get(posAMT).getNOTE_MTR7(),
                            amtlst.get(posAMT).getNOTE_MTR8(), amtlst.get(posAMT).getNOTE_MTR9(), amtlst.get(posAMT).getNOTE_MTR10(), amtlst.get(posAMT).getNOTE_CHEM1(),
                            amtlst.get(posAMT).getNOTE_CHEM2(), amtlst.get(posAMT).getNOTE_CHEM3(), amtlst.get(posAMT).getNOTE_CHEM4(), amtlst.get(posAMT).getNOTE_CHEM5(),
                            amtlst.get(posAMT).getNOTE_CHEM6(), amtlst.get(posAMT).getNOTE_CHEM7(), amtlst.get(posAMT).getNOTE_CHEM8(), amtlst.get(posAMT).getNOTE_CHEM9(),
                            amtlst.get(posAMT).getNOTE_CHEM10(), amtlst.get(posAMT).getNOTE_CHEM11(), amtlst.get(posAMT).getNOTE_CHEM12(), amtlst.get(posAMT).getNOTE_CHEM13(),
                            amtlst.get(posAMT).getNOTE_CHEM14(), amtlst.get(posAMT).getNOTE_CHEM15(), amtlst.get(posAMT).getNOTE_CHEM16(), amtlst.get(posAMT).getNOTE_CHEM17(),
                            amtlst.get(posAMT).getNOTE_CHEM18(), amtlst.get(posAMT).getNOTE_CHEM19(), amtlst.get(posAMT).getNOTE_CHEM20(), amtlst.get(posAMT).getNOTE_PIG1(),
                            amtlst.get(posAMT).getNOTE_PIG2(), amtlst.get(posAMT).getNOTE_PIG3(), amtlst.get(posAMT).getNOTE_PIG4(), amtlst.get(posAMT).getNOTE_PIG5(),
                            amtlst.get(posAMT).getNOTE_PIG6(), amtlst.get(posAMT).getNOTE_PIG7(), amtlst.get(posAMT).getNOTE_PIG8(), amtlst.get(posAMT).getNOTE_PIG9(),
                            amtlst.get(posAMT).getNOTE_PIG10()};

                    for (int i = 0; i < strl2.length; i++) {
                        if (strl[i] != null) {
                            chkl[i].setVisibility(View.VISIBLE);
                            ibtn[i].setVisibility(View.VISIBLE);

                        } else {
                            chkl[i].setVisibility(View.INVISIBLE);
                            ibtn[i].setVisibility(View.INVISIBLE);
                        }
                        if (amtlst.get(posAMT).getAMT_CHECK().equals("Y")) {
                            chk_Allraw.setChecked(true);
                            chk_Allchem.setChecked(true);
                            chk_Allpig.setChecked(true);
                        }
                        if (strl2[i] != null) {
                            if (strl2[i].equals("Y")) {
                                chkl[i].setChecked(true);
                            } else {
                                chkl[i].setChecked(false);
                            }
                        }
                        if (valNote[i] != null) {
                            ibtn[i].setBackgroundResource(R.drawable.mybutton);
                        } else ibtn[i].setBackgroundResource(R.drawable.white_button);
                    }

                    strl3 = new String[]{
                            amtlst.get(posAMT).getMAC_MTR11(), amtlst.get(posAMT).getMAC_MTR12(),
                            amtlst.get(posAMT).getMAC_MTR13(), amtlst.get(posAMT).getMAC_MTR14(),
                            amtlst.get(posAMT).getMAC_MTR15(), amtlst.get(posAMT).getMAC_MTR16(),
                            amtlst.get(posAMT).getMAC_MTR17(), amtlst.get(posAMT).getMAC_MTR18(),
                            amtlst.get(posAMT).getMAC_MTR19(), amtlst.get(posAMT).getMAC_MTR20(),
                            amtlst.get(posAMT).getMAC_MTR21(), amtlst.get(posAMT).getMAC_MTR22(),
                    };

                    for (int i = 0; i < tvl.length; i++) {
                        if (strl3[i] != null) {
                            tvl[i].setVisibility(View.VISIBLE);
                            tvl[i].setText(strl3[i]);
                        } else {
                            tvl[i].setVisibility(View.INVISIBLE);
                        }
                    }

                    for (CheckBox checkBox : chkl) {
                        if (checkBox.isChecked()) {
                            chk_Allraw.setChecked(true);
                            chk_Allchem.setChecked(true);
                            chk_Allpig.setChecked(true);
                        }
                    }


                }
            }
        });

        for (int i = 0; i < ibtn.length; i++) {
            final int col = i;
            ibtn[col].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    note_dialog(posNote[col],  valNote[col], amtlst.get(posAMT).getNOTE_NO(), ibtn[col], col);
                }
            });
        }

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                creMode = true;
                btn_edit.setEnabled(false);
                btn_save.setEnabled(true);
                btn_cancel.setEnabled(true);
                btn_step2.setEnabled(false);
                btn_step4.setEnabled(false);
                btn_step1.setEnabled(false);
                btn_part1.setEnabled(false);
                btn_part4.setEnabled(false);
                ib_logout.setEnabled(false);

                edt_date.setEnabled(false);
                spr_shift.setEnabled(false);
                spr_groupID.setEnabled(false);

                chk_can1.setEnabled(true);
                listAMTAdapter.disableitem(false);

            }
        });


        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAMT();
                ib_logout.setEnabled(true);
                listAMTAdapter.disableitem(true);

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAMT(ins_no, edt_date.getText().toString(), spr_shift.getSelectedItem().toString(), line);
                creMode = false;
                btn_edit.setEnabled(true);
                btn_save.setEnabled(false);
                btn_cancel.setEnabled(false);
                btn_step2.setEnabled(true);
                btn_step4.setEnabled(true);
                btn_step1.setEnabled(true);
                btn_part1.setEnabled(true);
                btn_part4.setEnabled(true);
                ib_logout.setEnabled(true);

//                edt_line.setEnabled(true);
                edt_date.setEnabled(true);
                spr_shift.setEnabled(true);
                spr_groupID.setEnabled(true);
                setDisable();
                listAMTAdapter.disableitem(true);
            }
        });



        chk_Allraw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CheckBox[] chkl = new CheckBox[]{chk_raw1, chk_raw2, chk_raw3, chk_raw4, chk_raw5, chk_raw6, chk_raw7,
                        chk_raw8, chk_raw9, chk_raw10, chk_chem1, chk_chem2, chk_chem3, chk_chem4, chk_chem5, chk_chem6, chk_chem7,
                        chk_chem8, chk_chem9, chk_chem10, chk_chem11, chk_chem12, chk_chem13, chk_chem14, chk_chem15, chk_chem16, chk_chem17,
                        chk_chem18, chk_chem19, chk_chem20, chk_pig1, chk_pig2, chk_pig3, chk_pig4, chk_pig5, chk_pig6, chk_pig7,
                        chk_pig8, chk_pig9, chk_pig10};
                if (isChecked) {
                    for (int i = 0; i < chkl.length; i++) {
                        if (chkl[i].isEnabled()) {
                            chkl[i].setChecked(true);
                        }
                    }
                } else {
                    for (int i = 0; i < chkl.length; i++) {
                        if (chkl[i].isEnabled()) {
                            chkl[i].setChecked(false);
                        }
                    }
                }
            }
        });

        chk_can1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    check_can1(factory, ins_no, spr_shift.getSelectedItem().toString(), edt_date.getText().toString()); ;
                    chk_can2.setEnabled(false);
                    chk_cantay.setEnabled(false);
                }
                else {
                    chk_Allraw.setEnabled(false);
                    if (chk_Allraw.isChecked()){
                        chk_can2.setEnabled(true);
                        chk_can1.setEnabled(false);
                    }
                    if (!chk_Allraw.isChecked()){
                        chk_Allraw.setEnabled(false);
                    }
                    for (EditText editText : edtl) {
                        editText.setEnabled(false);
                    }
                    for (CheckBox checkBox : chkl) {
                        checkBox.setEnabled(false);
                    }
                }
            }
        });

        chk_can2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    check_can2(factory, ins_no, spr_shift.getSelectedItem().toString(), edt_date.getText().toString()); ;
                    chk_can1.setEnabled(false);
                    chk_cantay.setEnabled(false);
                }
                else {
                    chk_Allchem.setEnabled(false);
                    if (chk_Allchem.isChecked()){
                        chk_cantay.setEnabled(true);
                        chk_can2.setEnabled(false);
                    }
                    if (!chk_Allchem.isChecked()){
                        chk_Allchem.setEnabled(false);
                        chk_cantay.setEnabled(false);
                    }
                    for (EditText editText : edtl) {
                        editText.setEnabled(false);
                    }
                    for (CheckBox checkBox : chkl) {
                        checkBox.setEnabled(false);
                    }
                }
            }
        });

        chk_cantay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    check_cantay(factory, ins_no, spr_shift.getSelectedItem().toString(), edt_date.getText().toString()); ;
                    chk_can1.setEnabled(false);
                    chk_can2.setEnabled(false);
                }
                else {
                    chk_Allpig.setEnabled(false);
                    if (chk_Allpig.isChecked()){
                        chk_can2.setEnabled(false);
                        chk_can1.setEnabled(false);
                        chk_cantay.setEnabled(false);
                    }
                    if (!chk_Allpig.isChecked()){
                        chk_Allpig.setEnabled(false);
                    }
                    for (EditText editText : edtl) {
                        editText.setEnabled(false);
                    }
                    for (CheckBox checkBox : chkl) {
                        checkBox.setEnabled(false);
                    }
                }
            }
        });



        chk_Allchem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CheckBox[] chkl = new CheckBox[]{chk_raw1, chk_raw2, chk_raw3, chk_raw4, chk_raw5, chk_raw6, chk_raw7,
                        chk_raw8, chk_raw9, chk_raw10, chk_chem1, chk_chem2, chk_chem3, chk_chem4, chk_chem5, chk_chem6, chk_chem7,
                        chk_chem8, chk_chem9, chk_chem10, chk_chem11, chk_chem12, chk_chem13, chk_chem14, chk_chem15, chk_chem16, chk_chem17,
                        chk_chem18, chk_chem19, chk_chem20, chk_pig1, chk_pig2, chk_pig3, chk_pig4, chk_pig5, chk_pig6, chk_pig7,
                        chk_pig8, chk_pig9, chk_pig10};
                if (isChecked) {
                    for (int i = 0; i < chkl.length; i++) {
                        if (chkl[i].isEnabled()) {
                            chkl[i].setChecked(true);
                        }
                    }
                } else {
                    for (int i = 0; i < chkl.length; i++) {
                        if (chkl[i].isEnabled()) {
                            chkl[i].setChecked(false);
                        }
                    }
                }
            }
        });

        chk_Allpig.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CheckBox[] chkl = new CheckBox[]{chk_raw1, chk_raw2, chk_raw3, chk_raw4, chk_raw5, chk_raw6, chk_raw7,
                        chk_raw8, chk_raw9, chk_raw10, chk_chem1, chk_chem2, chk_chem3, chk_chem4, chk_chem5, chk_chem6, chk_chem7,
                        chk_chem8, chk_chem9, chk_chem10, chk_chem11, chk_chem12, chk_chem13, chk_chem14, chk_chem15, chk_chem16, chk_chem17,
                        chk_chem18, chk_chem19, chk_chem20, chk_pig1, chk_pig2, chk_pig3, chk_pig4, chk_pig5, chk_pig6, chk_pig7,
                        chk_pig8, chk_pig9, chk_pig10};
                if (isChecked) {
                    for (int i = 0; i < chkl.length; i++) {
                        if (chkl[i].isEnabled()) {
                            chkl[i].setChecked(true);
                        }
                    }
                } else {
                    for (int i = 0; i < chkl.length; i++) {
                        if (chkl[i].isEnabled()) {
                            chkl[i].setChecked(false);
                        }
                    }
                }
            }
        });

        btn_step1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(IPBatchActivity_FVI.this, Mac1Activity_FVI.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        btn_step2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (factory.equals("FVI_II_305")) {
                    Intent i = new Intent(IPBatchActivity_FVI.this, Mac2Activity_FVI2.class);
                    i.putExtras(bundle);
                    startActivity(i);
                } else {
                    Intent i = new Intent(IPBatchActivity_FVI.this, Mac2Activity_FVI2.class);
                    i.putExtras(bundle);
                    startActivity(i);
                }
            }

        });

        btn_step4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(IPBatchActivity_FVI.this, Mac3Activity_FVI.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        btn_part1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(IPBatchActivity_FVI.this, ChemicalActivity.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        btn_part4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(IPBatchActivity_FVI.this, Kneader_Controller.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

    }


    public void check_can1(String factory, String mac1_id, String shift, String date) {//String date, String ca, String may
        try {
            CallApi.check_can1(factory, mac1_id, spr_shift.getSelectedItem().toString() ,edt_date.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    QSMac1 qsmac1 = gson.fromJson(response.toString(), QSMac1.class);
                    lstMac1 = qsmac1.getItems() == null ? new ArrayList<QAM_SCALE_MAC1>() : qsmac1.getItems();
                    if (lstMac1.size() != 0) {
                        edt_Mraw1.setText(lstMac1.get(0).getSC1_RAW1());
                        edt_Mraw2.setText(lstMac1.get(0).getSC1_RAW2());
                        edt_Mraw3.setText(lstMac1.get(0).getSC1_RAW3());
                        edt_Mraw4.setText(lstMac1.get(0).getSC1_RAW4());
                        edt_Mraw5.setText(lstMac1.get(0).getSC1_RAW5());
                        edt_Mraw6.setText(lstMac1.get(0).getSC1_RAW6());
                        edt_Mraw7.setText(lstMac1.get(0).getSC1_RAW7());
                        edt_Mraw8.setText(lstMac1.get(0).getSC1_RAW8());
                        edt_Mraw9.setText(lstMac1.get(0).getSC1_RAW9());
                        edt_Mraw10.setText(lstMac1.get(0).getSC1_RAW10());

                        edt_Mchem1.setText(lstMac1.get(0).getSC1_CHEM1());
                        edt_Mchem2.setText(lstMac1.get(0).getSC1_CHEM2());
                        edt_Mchem3.setText(lstMac1.get(0).getSC1_CHEM3());
                        edt_Mchem4.setText(lstMac1.get(0).getSC1_CHEM4());
                        edt_Mchem5.setText(lstMac1.get(0).getSC1_CHEM5());
                        edt_Mchem6.setText(lstMac1.get(0).getSC1_CHEM6());
                        edt_Mchem7.setText(lstMac1.get(0).getSC1_CHEM7());
                        edt_Mchem8.setText(lstMac1.get(0).getSC1_CHEM8());
                        edt_Mchem9.setText(lstMac1.get(0).getSC1_CHEM9());
                        edt_Mchem10.setText(lstMac1.get(0).getSC1_CHEM10());
                        edt_Mchem11.setText(lstMac1.get(0).getSC1_CHEM11());
                        edt_Mchem12.setText(lstMac1.get(0).getSC1_CHEM12());
                        edt_Mchem13.setText(lstMac1.get(0).getSC1_CHEM13());
                        edt_Mchem14.setText(lstMac1.get(0).getSC1_CHEM14());
                        edt_Mchem15.setText(lstMac1.get(0).getSC1_CHEM15());
                        edt_Mchem16.setText(lstMac1.get(0).getSC1_CHEM16());
                        edt_Mchem17.setText(lstMac1.get(0).getSC1_CHEM17());
                        edt_Mchem18.setText(lstMac1.get(0).getSC1_CHEM18());
                        edt_Mchem19.setText(lstMac1.get(0).getSC1_CHEM19());
                        edt_Mchem20.setText(lstMac1.get(0).getSC1_CHEM20());

                        edt_Mpig1.setText(lstMac1.get(0).getSC1_PIG1());
                        edt_Mpig2.setText(lstMac1.get(0).getSC1_PIG2());
                        edt_Mpig3.setText(lstMac1.get(0).getSC1_PIG3());
                        edt_Mpig4.setText(lstMac1.get(0).getSC1_PIG4());
                        edt_Mpig5.setText(lstMac1.get(0).getSC1_PIG5());
                        edt_Mpig6.setText(lstMac1.get(0).getSC1_PIG6());
                        edt_Mpig7.setText(lstMac1.get(0).getSC1_PIG7());
                        edt_Mpig8.setText(lstMac1.get(0).getSC1_PIG8());
                        edt_Mpig9.setText(lstMac1.get(0).getSC1_PIG9());
                        edt_Mpig10.setText(lstMac1.get(0).getSC1_PIG10());
                    }

                    String[] str1 = new String[]{

                            lstMac1.get(0).getSC1_RAW1(),lstMac1.get(0).getSC1_RAW2(),
                            lstMac1.get(0).getSC1_RAW3(),lstMac1.get(0).getSC1_RAW4(),
                            lstMac1.get(0).getSC1_RAW5(),lstMac1.get(0).getSC1_RAW6(),
                            lstMac1.get(0).getSC1_RAW7(),lstMac1.get(0).getSC1_RAW8(),
                            lstMac1.get(0).getSC1_RAW9(),lstMac1.get(0).getSC1_RAW10(),

                            lstMac1.get(0).getSC1_PIG1(),lstMac1.get(0).getSC1_PIG2(),
                            lstMac1.get(0).getSC1_PIG3(),lstMac1.get(0).getSC1_PIG4(),
                            lstMac1.get(0).getSC1_PIG5(),lstMac1.get(0).getSC1_PIG6(),
                            lstMac1.get(0).getSC1_PIG7(),lstMac1.get(0).getSC1_PIG8(),
                            lstMac1.get(0).getSC1_PIG9(),lstMac1.get(0).getSC1_PIG10(),
//
                            lstMac1.get(0).getSC1_CHEM1(),lstMac1.get(0).getSC1_CHEM2(),
                            lstMac1.get(0).getSC1_CHEM3(),lstMac1.get(0).getSC1_CHEM4(),
                            lstMac1.get(0).getSC1_CHEM5(),lstMac1.get(0).getSC1_CHEM6(),
                            lstMac1.get(0).getSC1_CHEM7(),lstMac1.get(0).getSC1_CHEM8(),
                            lstMac1.get(0).getSC1_CHEM9(),lstMac1.get(0).getSC1_CHEM10(),
                            lstMac1.get(0).getSC1_CHEM11(),lstMac1.get(0).getSC1_CHEM12(),
                            lstMac1.get(0).getSC1_CHEM13(),lstMac1.get(0).getSC1_CHEM14(),
                            lstMac1.get(0).getSC1_CHEM15(),lstMac1.get(0).getSC1_CHEM16(),
                            lstMac1.get(0).getSC1_CHEM17(),lstMac1.get(0).getSC1_CHEM18(),
                            lstMac1.get(0).getSC1_CHEM19(),lstMac1.get(0).getSC1_CHEM20()

                    };

                    for (int i = 0; i < edtlcan.length; i++) {
                        if (str1[i] != null) {
                            edtlcan[i].setText(str1[i]);
                            edtlcan[i].setEnabled(true);
                            for (int j = 0; j < edtlcan.length; j++){
                                if (edtlcan[i] != null){
                                    chklcan[i].setEnabled(true);
                                }
                            }
                            chk_Allraw.setEnabled(true);
                        }
                        else {
                            edtlcan[i].setEnabled(false);
                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });

            System.out.println("da chay ham");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void check_can2(String factory, String mac2_id, String shift, String date) {//String date, String ca, String may
        try {
            CallApi.check_can2(factory, mac2_id, spr_shift.getSelectedItem().toString() ,edt_date.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    QSMac2 qsmac2 = gson.fromJson(response.toString(), QSMac2.class);
                    lstMac2 = qsmac2.getItems() == null ? new ArrayList<QAM_SCALE_MAC2>() : qsmac2.getItems();
                    if (lstMac2.size() != 0) {
                        edt_Mraw1.setText(lstMac2.get(0).getSC2_RAW1());
                        edt_Mraw2.setText(lstMac2.get(0).getSC2_RAW2());
                        edt_Mraw3.setText(lstMac2.get(0).getSC2_RAW3());
                        edt_Mraw4.setText(lstMac2.get(0).getSC2_RAW4());
                        edt_Mraw5.setText(lstMac2.get(0).getSC2_RAW5());
                        edt_Mraw6.setText(lstMac2.get(0).getSC2_RAW6());
                        edt_Mraw7.setText(lstMac2.get(0).getSC2_RAW7());
                        edt_Mraw8.setText(lstMac2.get(0).getSC2_RAW8());
                        edt_Mraw9.setText(lstMac2.get(0).getSC2_RAW9());
                        edt_Mraw10.setText(lstMac2.get(0).getSC2_RAW10());

                        edt_Mchem1.setText(lstMac2.get(0).getSC2_CHEM1());
                        edt_Mchem2.setText(lstMac2.get(0).getSC2_CHEM2());
                        edt_Mchem3.setText(lstMac2.get(0).getSC2_CHEM3());
                        edt_Mchem4.setText(lstMac2.get(0).getSC2_CHEM4());
                        edt_Mchem5.setText(lstMac2.get(0).getSC2_CHEM5());
                        edt_Mchem6.setText(lstMac2.get(0).getSC2_CHEM6());
                        edt_Mchem7.setText(lstMac2.get(0).getSC2_CHEM7());
                        edt_Mchem8.setText(lstMac2.get(0).getSC2_CHEM8());
                        edt_Mchem9.setText(lstMac2.get(0).getSC2_CHEM9());
                        edt_Mchem10.setText(lstMac2.get(0).getSC2_CHEM10());
                        edt_Mchem11.setText(lstMac2.get(0).getSC2_CHEM11());
                        edt_Mchem12.setText(lstMac2.get(0).getSC2_CHEM12());
                        edt_Mchem13.setText(lstMac2.get(0).getSC2_CHEM13());
                        edt_Mchem14.setText(lstMac2.get(0).getSC2_CHEM14());
                        edt_Mchem15.setText(lstMac2.get(0).getSC2_CHEM15());
                        edt_Mchem16.setText(lstMac2.get(0).getSC2_CHEM16());
                        edt_Mchem17.setText(lstMac2.get(0).getSC2_CHEM17());
                        edt_Mchem18.setText(lstMac2.get(0).getSC2_CHEM18());
                        edt_Mchem19.setText(lstMac2.get(0).getSC2_CHEM19());
                        edt_Mchem20.setText(lstMac2.get(0).getSC2_CHEM20());

                        edt_Mpig1.setText(lstMac2.get(0).getSC2_PIG1());
                        edt_Mpig2.setText(lstMac2.get(0).getSC2_PIG2());
                        edt_Mpig3.setText(lstMac2.get(0).getSC2_PIG3());
                        edt_Mpig4.setText(lstMac2.get(0).getSC2_PIG4());
                        edt_Mpig5.setText(lstMac2.get(0).getSC2_PIG5());
                        edt_Mpig6.setText(lstMac2.get(0).getSC2_PIG6());
                        edt_Mpig7.setText(lstMac2.get(0).getSC2_PIG7());
                        edt_Mpig8.setText(lstMac2.get(0).getSC2_PIG8());
                        edt_Mpig9.setText(lstMac2.get(0).getSC2_PIG9());
                        edt_Mpig10.setText(lstMac2.get(0).getSC2_PIG10());
                    }

                    String[] str1 = new String[]{

                            lstMac2.get(0).getSC2_RAW1(),lstMac2.get(0).getSC2_RAW2(),
                            lstMac2.get(0).getSC2_RAW3(),lstMac2.get(0).getSC2_RAW4(),
                            lstMac2.get(0).getSC2_RAW5(),lstMac2.get(0).getSC2_RAW6(),
                            lstMac2.get(0).getSC2_RAW7(),lstMac2.get(0).getSC2_RAW8(),
                            lstMac2.get(0).getSC2_RAW9(),lstMac2.get(0).getSC2_RAW10(),

                            lstMac2.get(0).getSC2_PIG1(),lstMac2.get(0).getSC2_PIG2(),
                            lstMac2.get(0).getSC2_PIG3(),lstMac2.get(0).getSC2_PIG4(),
                            lstMac2.get(0).getSC2_PIG5(),lstMac2.get(0).getSC2_PIG6(),
                            lstMac2.get(0).getSC2_PIG7(),lstMac2.get(0).getSC2_PIG8(),
                            lstMac2.get(0).getSC2_PIG9(),lstMac2.get(0).getSC2_PIG10(),
//
                            lstMac2.get(0).getSC2_CHEM1(),lstMac2.get(0).getSC2_CHEM2(),
                            lstMac2.get(0).getSC2_CHEM3(),lstMac2.get(0).getSC2_CHEM4(),
                            lstMac2.get(0).getSC2_CHEM5(),lstMac2.get(0).getSC2_CHEM6(),
                            lstMac2.get(0).getSC2_CHEM7(),lstMac2.get(0).getSC2_CHEM8(),
                            lstMac2.get(0).getSC2_CHEM9(),lstMac2.get(0).getSC2_CHEM10(),
                            lstMac2.get(0).getSC2_CHEM11(),lstMac2.get(0).getSC2_CHEM12(),
                            lstMac2.get(0).getSC2_CHEM13(),lstMac2.get(0).getSC2_CHEM14(),
                            lstMac2.get(0).getSC2_CHEM15(),lstMac2.get(0).getSC2_CHEM16(),
                            lstMac2.get(0).getSC2_CHEM17(),lstMac2.get(0).getSC2_CHEM18(),
                            lstMac2.get(0).getSC2_CHEM19(),lstMac2.get(0).getSC2_CHEM20()

                    };

                    for (int i = 0; i < edtlcan.length; i++) {
                        if (str1[i] != null) {
                            edtlcan[i].setText(str1[i]);
                            edtlcan[i].setEnabled(true);
                            for (int j = 0; j < edtlcan.length; j++){
                                if (edtlcan[i] != null){
                                    chklcan[i].setEnabled(true);
                                }
                            }
                            chk_Allchem.setEnabled(true);
                        }
                        else {
                            edtlcan[i].setEnabled(false);
                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });

            System.out.println("da chay ham");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void check_cantay(String factory, String hand_id, String shift, String date) {//String date, String ca, String may
        try {
            CallApi.check_cantay(factory, hand_id, spr_shift.getSelectedItem().toString() ,edt_date.getText().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    QSMachand qsmachand = gson.fromJson(response.toString(), QSMachand.class);
                    lstMachand = qsmachand.getItems() == null ? new ArrayList<QAM_SCALE_HAND>() : qsmachand.getItems();
                    if (lstMachand.size() != 0) {
                        edt_Mraw1.setText(lstMachand.get(0).getSCH_RAW1());
                        edt_Mraw2.setText(lstMachand.get(0).getSCH_RAW2());
                        edt_Mraw3.setText(lstMachand.get(0).getSCH_RAW3());
                        edt_Mraw4.setText(lstMachand.get(0).getSCH_RAW4());
                        edt_Mraw5.setText(lstMachand.get(0).getSCH_RAW5());
                        edt_Mraw6.setText(lstMachand.get(0).getSCH_RAW6());
                        edt_Mraw7.setText(lstMachand.get(0).getSCH_RAW7());
                        edt_Mraw8.setText(lstMachand.get(0).getSCH_RAW8());
                        edt_Mraw9.setText(lstMachand.get(0).getSCH_RAW9());
                        edt_Mraw10.setText(lstMachand.get(0).getSCH_RAW10());

                        edt_Mchem1.setText(lstMachand.get(0).getSCH_CHEM1());
                        edt_Mchem2.setText(lstMachand.get(0).getSCH_CHEM2());
                        edt_Mchem3.setText(lstMachand.get(0).getSCH_CHEM3());
                        edt_Mchem4.setText(lstMachand.get(0).getSCH_CHEM4());
                        edt_Mchem5.setText(lstMachand.get(0).getSCH_CHEM5());
                        edt_Mchem6.setText(lstMachand.get(0).getSCH_CHEM6());
                        edt_Mchem7.setText(lstMachand.get(0).getSCH_CHEM7());
                        edt_Mchem8.setText(lstMachand.get(0).getSCH_CHEM8());
                        edt_Mchem9.setText(lstMachand.get(0).getSCH_CHEM9());
                        edt_Mchem10.setText(lstMachand.get(0).getSCH_CHEM10());
                        edt_Mchem11.setText(lstMachand.get(0).getSCH_CHEM11());
                        edt_Mchem12.setText(lstMachand.get(0).getSCH_CHEM12());
                        edt_Mchem13.setText(lstMachand.get(0).getSCH_CHEM13());
                        edt_Mchem14.setText(lstMachand.get(0).getSCH_CHEM14());
                        edt_Mchem15.setText(lstMachand.get(0).getSCH_CHEM15());
                        edt_Mchem16.setText(lstMachand.get(0).getSCH_CHEM16());
                        edt_Mchem17.setText(lstMachand.get(0).getSCH_CHEM17());
                        edt_Mchem18.setText(lstMachand.get(0).getSCH_CHEM18());
                        edt_Mchem19.setText(lstMachand.get(0).getSCH_CHEM19());
                        edt_Mchem20.setText(lstMachand.get(0).getSCH_CHEM20());

                        edt_Mpig1.setText(lstMachand.get(0).getSCH_PIG1());
                        edt_Mpig2.setText(lstMachand.get(0).getSCH_PIG2());
                        edt_Mpig3.setText(lstMachand.get(0).getSCH_PIG3());
                        edt_Mpig4.setText(lstMachand.get(0).getSCH_PIG4());
                        edt_Mpig5.setText(lstMachand.get(0).getSCH_PIG5());
                        edt_Mpig6.setText(lstMachand.get(0).getSCH_PIG6());
                        edt_Mpig7.setText(lstMachand.get(0).getSCH_PIG7());
                        edt_Mpig8.setText(lstMachand.get(0).getSCH_PIG8());
                        edt_Mpig9.setText(lstMachand.get(0).getSCH_PIG9());
                        edt_Mpig10.setText(lstMachand.get(0).getSCH_PIG10());
                    }

                    String[] str3 = new String[]{

                            lstMachand.get(0).getSCH_RAW1(),lstMachand.get(0).getSCH_RAW2(),
                            lstMachand.get(0).getSCH_RAW3(),lstMachand.get(0).getSCH_RAW4(),
                            lstMachand.get(0).getSCH_RAW5(),lstMachand.get(0).getSCH_RAW6(),
                            lstMachand.get(0).getSCH_RAW7(),lstMachand.get(0).getSCH_RAW8(),
                            lstMachand.get(0).getSCH_RAW9(),lstMachand.get(0).getSCH_RAW10(),

                            lstMachand.get(0).getSCH_PIG1(),lstMachand.get(0).getSCH_PIG2(),
                            lstMachand.get(0).getSCH_PIG3(),lstMachand.get(0).getSCH_PIG4(),
                            lstMachand.get(0).getSCH_PIG5(),lstMachand.get(0).getSCH_PIG6(),
                            lstMachand.get(0).getSCH_PIG7(),lstMachand.get(0).getSCH_PIG8(),
                            lstMachand.get(0).getSCH_PIG9(),lstMachand.get(0).getSCH_PIG10(),
//
                            lstMachand.get(0).getSCH_CHEM1(),lstMachand.get(0).getSCH_CHEM2(),
                            lstMachand.get(0).getSCH_CHEM3(),lstMachand.get(0).getSCH_CHEM4(),
                            lstMachand.get(0).getSCH_CHEM5(),lstMachand.get(0).getSCH_CHEM6(),
                            lstMachand.get(0).getSCH_CHEM7(),lstMachand.get(0).getSCH_CHEM8(),
                            lstMachand.get(0).getSCH_CHEM9(),lstMachand.get(0).getSCH_CHEM10(),
                            lstMachand.get(0).getSCH_CHEM11(),lstMachand.get(0).getSCH_CHEM12(),
                            lstMachand.get(0).getSCH_CHEM13(),lstMachand.get(0).getSCH_CHEM14(),
                            lstMachand.get(0).getSCH_CHEM15(),lstMachand.get(0).getSCH_CHEM16(),
                            lstMachand.get(0).getSCH_CHEM17(),lstMachand.get(0).getSCH_CHEM18(),
                            lstMachand.get(0).getSCH_CHEM19(),lstMachand.get(0).getSCH_CHEM20()

                    };

                    for (int i = 0; i < edtlcan.length; i++) {
                        if (str3[i] != null) {
                            edtlcan[i].setText(str3[i]);
                            edtlcan[i].setEnabled(true);
                            for (int j = 0; j < edtlcan.length; j++){
                                if (edtlcan[i] != null){
                                    chklcan[i].setEnabled(true);
                                }
                            }
                            chk_Allpig.setEnabled(true);
                        }
                        else {
                            edtlcan[i].setEnabled(false);
                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });

            System.out.println("da chay ham");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void note_dialog(final String position, final String value, final String note_no, final ImageButton imgbtn, final int dain){
        LayoutInflater inflater = getLayoutInflater();
        final View noteLayout = inflater.inflate(R.layout.dialog_note, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(IPBatchActivity_FVI.this);
        alert.setView(noteLayout);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
//        final ImageButton ibtnClear = noteLayout.findViewById(R.id.btn_clear);
        final Button save = noteLayout.findViewById(R.id.btn_save);
        final Button close = noteLayout.findViewById(R.id.btn_close);
        final EditText edtNote = noteLayout.findViewById(R.id.edt_note);

        edtNote.setText(value);

        if (creMode==true){
            save.setEnabled(true);
            edtNote.setEnabled(true);
            edtNote.setFocusable(true);
        }
        else {
            edtNote.setEnabled(false);
            edtNote.setFocusable(false);
            save.setEnabled(true);
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    CallApi.updateNOTE(factory, position, note_no, edtNote.getText().toString(), new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            valNote[dain]=edtNote.getText().toString();
                            ToastCustom.message(IPBatchActivity_FVI.this, "Lưu thành công!", Color.GREEN);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
                }catch (JSONException e){
                    e.printStackTrace();
                    ToastCustom.message(IPBatchActivity_FVI.this, "Lưu thất bại!", Color.RED);
                }

            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtNote.getText().toString().equals("")){
                    imgbtn.setBackgroundResource(R.drawable.white_button);
                }else imgbtn.setBackgroundResource(R.drawable.mybutton);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void defaultSpinner() {

        listGroupID = new ArrayList<>();
//        listGroupID.add(" Group ID");
        mcsAdapter = new ArrayAdapter(IPBatchActivity_FVI.this, R.layout.spn_layout, listGroupID);
        spr_groupID.setAdapter(mcsAdapter);

        listShift = new ArrayList<>();
        listShift.add("1");
        listShift.add("2");
        listShift.add("3");
        ShiftAdapter = new ArrayAdapter(IPBatchActivity_FVI.this, R.layout.spn_layout, listShift);
        spr_shift.setAdapter(ShiftAdapter);

    }

    public void datePickerDialog(final EditText edt_date) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(IPBatchActivity_FVI.this,
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int monthcurrent=monthOfYear + 1;
                        String month="";
                        String day="";
                        if(dayOfMonth>9) {
                            day=String.valueOf(dayOfMonth);

                        }
                        else
                        {
                            day="0"+String.valueOf(dayOfMonth);
                        }
                        if(monthcurrent>9)
                        {
                            month=String.valueOf(monthcurrent);
                        }
                        else
                        {
                            month="0"+String.valueOf(monthcurrent);
                        }
                        edt_date.setText(month + "/" + day + "/" + year);

                        spr_groupID.setEnabled(true);
                        getGroupID(spr_shift.getSelectedItem().toString(), edt_date.getText().toString());

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void getGroupID(String mcs_shift, String mcs_date) {
        try {
            CallApi.getGroupID2(factory, mcs_shift, mcs_date, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    InsArm insArm = gson.fromJson(response.toString(), InsArm.class);
                    insl = insArm.getItems() == null ? new ArrayList<QAM_INS>() : insArm.getItems();
                    arrINS = new ArrayList<>();
                    arrGroupID = new ArrayList<>();

                    if (insl.size()>0){
                        for (QAM_INS ob:insl) {
                            arrINS.add(ob);
                            arrGroupID.add(ob.getINS_NO());
                        }
                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(getApplicationContext(), R.layout.spn_layout, arrGroupID);
//                        adapter.setDropDownViewResource(R.layout.spn_layout);
                        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                        spr_groupID.setAdapter(adapter);
                    }
                    else {
                        btn_edit.setEnabled(false);
                        spr_groupID.setAdapter(mcsAdapter);
//                        tv_mcs.setText("");
                        edt_style.setText("");
                        edt_color.setText("");
                        edt_hard.setText("");
                        listAMTAdapter = new ListAMTAdapter(getApplicationContext(),R.layout.list_amt_adapter,new ArrayList<QAM_AMT>());
                        lv_amt.setAdapter(listAMTAdapter);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void getInfor(String mcs_no1){
        try {
            CallApi.getInforMcs(factory, mcs_no1, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    McsArm mcsArm = gson.fromJson(response.toString(), McsArm.class);
                    mcsinfor = mcsArm.getItems() == null ? new ArrayList<MCS>() : mcsArm.getItems();
                    if(mcsinfor.size()!=0) {
//                        tv_mcs.setText(mcsinfor.get(0).getMSC());
                        edt_style.setText(mcsinfor.get(0).getMCS_STYLE());
                        edt_color.setText(mcsinfor.get(0).getMCS_COLOR());
                        edt_hard.setText(mcsinfor.get(0).getMCS_HARD());
                    }
                    else{
//                        tv_mcs.setText("");
                        edt_color.setText("");
                        edt_hard.setText("");
                        edt_style.setText("");
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getAMT(String ins_no,String mcs_date,String mcs_shift, String mcs_line){
        try{
            CallApi.getAMT_FVI(factory, ins_no, mcs_date, mcs_shift, mcs_line, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    amtlst=new ArrayList<>();
                    AmtArm amtArm = gson.fromJson(response.toString(), AmtArm.class);
                    amtlst = amtArm.getItems()==null ? new ArrayList<QAM_AMT>():amtArm.getItems();
                    listAMTAdapter = new ListAMTAdapter(getApplicationContext(),R.layout.list_amt_adapter,amtlst);
                    lv_amt.setAdapter(listAMTAdapter);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });

        }catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void updateAMT(){
        boolean flag = true;
        ArrayList<Integer> locate= new ArrayList<>();
        for (int i=0;i<chkl.length;i++){
            if (chkl[i].isEnabled())
                locate.add(i);
        }

        for (int i=0;i<chkl.length;i++){
            if (chkl[i].isEnabled() && !chkl[i].isChecked()){
                flag = false;
                if (i==locate.get(locate.size()-1) && !chkl[i].isChecked()){
                    ToastCustom.message(getApplicationContext(), "Vui lòng đánh dấu chọn dữ liệu " +
                            "Liệu chủ/Hóa chất/Liệu màu", Color.RED);
                    break;
                }

            }else if (chkl[i].isEnabled() && chkl[i].isChecked()){
                flag = true;
                break;
            }
        }
        if (flag==true){
            amt = new QAM_AMT();
            amt.setAMT_NO(amtlst.get(posAMT).getAMT_NO());
            amt.setINS_NO(amtlst.get(posAMT).getINS_NO());

            amt.setCHK_MTR1(edt_Mraw1.isShown() ? (chk_raw1.isChecked() ? "Y" : "N") : "");
            amt.setCHK_MTR2(edt_Mraw2.isShown() ? (chk_raw2.isChecked() ? "Y" : "N") : "");
            amt.setCHK_MTR3(edt_Mraw3.isShown() ? (chk_raw3.isChecked() ? "Y" : "N") : "");
            amt.setCHK_MTR4(edt_Mraw4.isShown() ? (chk_raw4.isChecked() ? "Y" : "N") : "");
            amt.setCHK_MTR5(edt_Mraw5.isShown() ? (chk_raw5.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_MTR6(edt_Mraw6.isShown() ? (chk_raw6.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_MTR7(edt_Mraw7.isShown() ? (chk_raw7.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_MTR8(edt_Mraw8.isShown() ? (chk_raw8.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_MTR9(edt_Mraw9.isShown() ? (chk_raw9.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_MTR10(edt_Mraw10.isShown() ? (chk_raw10.isChecked()  ? "Y" : "N") : "");

            amt.setCHK_CHEM1(edt_Mchem1.isShown()  ? (chk_chem1.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM2(edt_Mchem2.isShown()  ? (chk_chem2.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM3(edt_Mchem3.isShown()  ? (chk_chem3.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM4(edt_Mchem4.isShown()  ? (chk_chem4.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM5(edt_Mchem5.isShown()  ? (chk_chem5.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM6(edt_Mchem6.isShown()  ? (chk_chem6.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM7(edt_Mchem7.isShown()  ? (chk_chem7.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM8(edt_Mchem8.isShown()  ? (chk_chem8.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM9(edt_Mchem9.isShown()  ? (chk_chem9.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM10(edt_Mchem10.isShown()  ? (chk_chem10.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM11(edt_Mchem11.isShown()  ? (chk_chem11.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM12(edt_Mchem12.isShown()  ? (chk_chem12.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM13(edt_Mchem13.isShown()  ? (chk_chem13.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM14(edt_Mchem14.isShown()  ? (chk_chem14.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM15(edt_Mchem15.isShown()  ? (chk_chem15.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM16(edt_Mchem16.isShown()  ? (chk_chem16.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM17(edt_Mchem17.isShown()  ? (chk_chem17.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM18(edt_Mchem18.isShown()  ? (chk_chem18.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM19(edt_Mchem19.isShown()  ? (chk_chem19.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_CHEM20(edt_Mchem20.isShown()  ? (chk_chem20.isChecked()  ? "Y" : "N") : "");

            amt.setCHK_PIG1(edt_Mpig1.isShown()  ? (chk_pig1.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_PIG2(edt_Mpig2.isShown()  ? (chk_pig2.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_PIG3(edt_Mpig3.isShown()  ? (chk_pig3.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_PIG4(edt_Mpig4.isShown()  ? (chk_pig4.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_PIG5(edt_Mpig5.isShown()  ? (chk_pig5.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_PIG6(edt_Mpig6.isShown()  ? (chk_pig6.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_PIG7(edt_Mpig7.isShown()  ? (chk_pig7.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_PIG8(edt_Mpig8.isShown()  ? (chk_pig8.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_PIG9(edt_Mpig9.isShown()  ? (chk_pig9.isChecked()  ? "Y" : "N") : "");
            amt.setCHK_PIG10(edt_Mpig10.isShown()  ? (chk_pig10.isChecked()  ? "Y" : "N") : "");
            try{
                CallApi.updateAMT(factory, amt, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        btn_edit.setEnabled(false);
                        btn_save.setEnabled(false);
                        btn_cancel.setEnabled(false);
                        btn_step2.setEnabled(true);
                        btn_step4.setEnabled(true);
                        btn_step1.setEnabled(true);
                        btn_part1.setEnabled(true);
                        btn_part4.setEnabled(true);

//                        edt_line.setEnabled(true);
                        edt_date.setEnabled(true);
                        spr_shift.setEnabled(true);
                        spr_groupID.setEnabled(true);
                        ToastCustom.message(getApplicationContext(), "Cập nhật dữ liệu thành công", Color.GREEN);
                        getAMT(ins_no,edt_date.getText().toString(), spr_shift.getSelectedItem().toString(), line);
                        creMode=false;
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }catch (JSONException e){
                e.printStackTrace();
            }
            setDisable();
        }

    }

    private void setDisable(){
        chk_Allraw.setEnabled(false);
        chk_Allchem.setEnabled(false);
        chk_Allpig.setEnabled(false);
        chk_can1.setEnabled(false);
        chk_can2.setEnabled(false);
        chk_cantay.setEnabled(false);

        for (CheckBox chk : chkl){
            chk.setEnabled(false);
        }

        edt_style.setEnabled(false);
        edt_color.setEnabled(false);
        edt_hard.setEnabled(false);

        for (EditText ed:edtl){
            ed.setEnabled(false);
        }
    }

    public void clear(){

        for (EditText ed:edtl){
            ed.setText("");
        }

        chk_Allraw.setChecked(false);
        chk_Allchem.setChecked(false);
        chk_Allpig.setChecked(false);
        chk_can1.setChecked(false);
        chk_can2.setChecked(false);
        chk_cantay.setChecked(false);
        for(int i=0;i<chkl.length;i++){
            chkl[i].setChecked(false);
        }

    }

    private void fintViewbyId(){
        lv_amt=findViewById(R.id.lv_mac);
        btn_edit= findViewById(R.id.btn_edit);
        btn_save= findViewById(R.id.btn_save);
        btn_cancel= findViewById(R.id.btn_cancel);
        btn_step1= findViewById(R.id.btn_step1);
        btn_step2= findViewById(R.id.btn_step2);
        btn_step4= findViewById(R.id.btn_step4);
        btn_part1= findViewById(R.id.btn_part1);
        btn_part4= findViewById(R.id.btn_part4);
        ib_logout = findViewById(R.id.ib_logout);

//        edt_line=findViewById(R.id.edt_line);
        edt_date=findViewById(R.id.edt_date);
        edt_batchno=findViewById(R.id.edt_batchno);
        edt_style=findViewById(R.id.edt_style);
        edt_color=findViewById(R.id.edt_color);
        edt_hard=findViewById(R.id.edt_hard);

        spr_groupID=findViewById(R.id.spr_groupID);
        spr_shift=findViewById(R.id.spr_shift);
//        tv_mcs=findViewById(R.id.tv_mcs);

        chk_Allraw=findViewById(R.id.chk_Allraw);
        chk_can1=findViewById(R.id.chk_can1);
        chk_can2=findViewById(R.id.chk_can2);
        chk_cantay=findViewById(R.id.chk_cantay);
        chk_Allchem=findViewById(R.id.chk_Allchem);
        chk_Allpig=findViewById(R.id.chk_Allpig);

        chk_raw1=findViewById(R.id.chk_raw1);
        chk_raw2=findViewById(R.id.chk_raw2);
        chk_raw3=findViewById(R.id.chk_raw3);
        chk_raw4=findViewById(R.id.chk_raw4);
        chk_raw5=findViewById(R.id.chk_raw5);
        chk_raw6=findViewById(R.id.chk_raw6);
        chk_raw7=findViewById(R.id.chk_raw7);
        chk_raw8=findViewById(R.id.chk_raw8);
        chk_raw9=findViewById(R.id.chk_raw9);
        chk_raw10=findViewById(R.id.chk_raw10);

        chk_chem1=findViewById(R.id.chk_chem1);
        chk_chem2=findViewById(R.id.chk_chem2);
        chk_chem3=findViewById(R.id.chk_chem3);
        chk_chem4=findViewById(R.id.chk_chem4);
        chk_chem5=findViewById(R.id.chk_chem5);
        chk_chem6=findViewById(R.id.chk_chem6);
        chk_chem7=findViewById(R.id.chk_chem7);
        chk_chem8=findViewById(R.id.chk_chem8);
        chk_chem9=findViewById(R.id.chk_chem9);
        chk_chem10=findViewById(R.id.chk_chem10);
        chk_chem11=findViewById(R.id.chk_chem11);
        chk_chem12=findViewById(R.id.chk_chem12);
        chk_chem13=findViewById(R.id.chk_chem13);
        chk_chem14=findViewById(R.id.chk_chem14);
        chk_chem15=findViewById(R.id.chk_chem15);
        chk_chem16=findViewById(R.id.chk_chem16);
        chk_chem17=findViewById(R.id.chk_chem17);
        chk_chem18=findViewById(R.id.chk_chem18);
        chk_chem19=findViewById(R.id.chk_chem19);
        chk_chem20=findViewById(R.id.chk_chem20);

        chk_pig1=findViewById(R.id.chk_pig1);
        chk_pig2=findViewById(R.id.chk_pig2);
        chk_pig3=findViewById(R.id.chk_pig3);
        chk_pig4=findViewById(R.id.chk_pig4);
        chk_pig5=findViewById(R.id.chk_pig5);
        chk_pig6=findViewById(R.id.chk_pig6);
        chk_pig7=findViewById(R.id.chk_pig7);
        chk_pig8=findViewById(R.id.chk_pig8);
        chk_pig9=findViewById(R.id.chk_pig9);
        chk_pig10=findViewById(R.id.chk_pig10);

        ibtn_raw1=findViewById(R.id.ibtn_raw1);
        ibtn_raw2=findViewById(R.id.ibtn_raw2);
        ibtn_raw3=findViewById(R.id.ibtn_raw3);
        ibtn_raw4=findViewById(R.id.ibtn_raw4);
        ibtn_raw5=findViewById(R.id.ibtn_raw5);
        ibtn_raw6=findViewById(R.id.ibtn_raw6);
        ibtn_raw7=findViewById(R.id.ibtn_raw7);
        ibtn_raw8=findViewById(R.id.ibtn_raw8);
        ibtn_raw9=findViewById(R.id.ibtn_raw9);
        ibtn_raw10=findViewById(R.id.ibtn_raw10);

        ibtn_chem1=findViewById(R.id.ibtn_chem1);
        ibtn_chem2=findViewById(R.id.ibtn_chem2);
        ibtn_chem3=findViewById(R.id.ibtn_chem3);
        ibtn_chem4=findViewById(R.id.ibtn_chem4);
        ibtn_chem5=findViewById(R.id.ibtn_chem5);
        ibtn_chem6=findViewById(R.id.ibtn_chem6);
        ibtn_chem7=findViewById(R.id.ibtn_chem7);
        ibtn_chem8=findViewById(R.id.ibtn_chem8);
        ibtn_chem9=findViewById(R.id.ibtn_chem9);
        ibtn_chem10=findViewById(R.id.ibtn_chem10);
        ibtn_chem11=findViewById(R.id.ibtn_chem11);
        ibtn_chem12=findViewById(R.id.ibtn_chem12);
        ibtn_chem13=findViewById(R.id.ibtn_chem13);
        ibtn_chem14=findViewById(R.id.ibtn_chem14);
        ibtn_chem15=findViewById(R.id.ibtn_chem15);
        ibtn_chem16=findViewById(R.id.ibtn_chem16);
        ibtn_chem17=findViewById(R.id.ibtn_chem17);
        ibtn_chem18=findViewById(R.id.ibtn_chem18);
        ibtn_chem19=findViewById(R.id.ibtn_chem19);
        ibtn_chem20=findViewById(R.id.ibtn_chem20);

        ibtn_pig1=findViewById(R.id.ibtn_pig1);
        ibtn_pig2=findViewById(R.id.ibtn_pig2);
        ibtn_pig3=findViewById(R.id.ibtn_pig3);
        ibtn_pig4=findViewById(R.id.ibtn_pig4);
        ibtn_pig5=findViewById(R.id.ibtn_pig5);
        ibtn_pig6=findViewById(R.id.ibtn_pig6);
        ibtn_pig7=findViewById(R.id.ibtn_pig7);
        ibtn_pig8=findViewById(R.id.ibtn_pig8);
        ibtn_pig9=findViewById(R.id.ibtn_pig9);
        ibtn_pig10=findViewById(R.id.ibtn_pig10);

        edt_raw1=findViewById(R.id.edt_raw1);
        edt_raw2=findViewById(R.id.edt_raw2);
        edt_raw3=findViewById(R.id.edt_raw3);
        edt_raw4=findViewById(R.id.edt_raw4);
        edt_raw5=findViewById(R.id.edt_raw5);
        edt_raw6=findViewById(R.id.edt_raw6);
        edt_raw7=findViewById(R.id.edt_raw7);
        edt_raw8=findViewById(R.id.edt_raw8);
        edt_raw9=findViewById(R.id.edt_raw9);
        edt_raw10=findViewById(R.id.edt_raw10);

        edt_chem1=findViewById(R.id.edt_chem1);
        edt_chem2=findViewById(R.id.edt_chem2);
        edt_chem3=findViewById(R.id.edt_chem3);
        edt_chem4=findViewById(R.id.edt_chem4);
        edt_chem5=findViewById(R.id.edt_chem5);
        edt_chem6=findViewById(R.id.edt_chem6);
        edt_chem7=findViewById(R.id.edt_chem7);
        edt_chem8=findViewById(R.id.edt_chem8);
        edt_chem9=findViewById(R.id.edt_chem9);
        edt_chem10=findViewById(R.id.edt_chem10);
        edt_chem11=findViewById(R.id.edt_chem11);
        edt_chem12=findViewById(R.id.edt_chem12);
        edt_chem13=findViewById(R.id.edt_chem13);
        edt_chem14=findViewById(R.id.edt_chem14);
        edt_chem15=findViewById(R.id.edt_chem15);
        edt_chem16=findViewById(R.id.edt_chem16);
        edt_chem17=findViewById(R.id.edt_chem17);
        edt_chem18=findViewById(R.id.edt_chem18);
        edt_chem19=findViewById(R.id.edt_chem19);
        edt_chem20=findViewById(R.id.edt_chem20);

        edt_pig1=findViewById(R.id.edt_pig1);
        edt_pig2=findViewById(R.id.edt_pig2);
        edt_pig3=findViewById(R.id.edt_pig3);
        edt_pig4=findViewById(R.id.edt_pig4);
        edt_pig5=findViewById(R.id.edt_pig5);
        edt_pig6=findViewById(R.id.edt_pig6);
        edt_pig7=findViewById(R.id.edt_pig7);
        edt_pig8=findViewById(R.id.edt_pig8);
        edt_pig9=findViewById(R.id.edt_pig9);
        edt_pig10=findViewById(R.id.edt_pig10);

        edt_Mraw1=findViewById(R.id.edt_Mraw1);
        edt_Mraw2=findViewById(R.id.edt_Mraw2);
        edt_Mraw3=findViewById(R.id.edt_Mraw3);
        edt_Mraw4=findViewById(R.id.edt_Mraw4);
        edt_Mraw5=findViewById(R.id.edt_Mraw5);
        edt_Mraw6=findViewById(R.id.edt_Mraw6);
        edt_Mraw7=findViewById(R.id.edt_Mraw7);
        edt_Mraw8=findViewById(R.id.edt_Mraw8);
        edt_Mraw9=findViewById(R.id.edt_Mraw9);
        edt_Mraw10=findViewById(R.id.edt_Mraw10);

        edt_Mchem1=findViewById(R.id.edt_Mchem1);
        edt_Mchem2=findViewById(R.id.edt_Mchem2);
        edt_Mchem3=findViewById(R.id.edt_Mchem3);
        edt_Mchem4=findViewById(R.id.edt_Mchem4);
        edt_Mchem5=findViewById(R.id.edt_Mchem5);
        edt_Mchem6=findViewById(R.id.edt_Mchem6);
        edt_Mchem7=findViewById(R.id.edt_Mchem7);
        edt_Mchem8=findViewById(R.id.edt_Mchem8);
        edt_Mchem9=findViewById(R.id.edt_Mchem9);
        edt_Mchem10=findViewById(R.id.edt_Mchem10);
        edt_Mchem11=findViewById(R.id.edt_Mchem11);
        edt_Mchem12=findViewById(R.id.edt_Mchem12);
        edt_Mchem13=findViewById(R.id.edt_Mchem13);
        edt_Mchem14=findViewById(R.id.edt_Mchem14);
        edt_Mchem15=findViewById(R.id.edt_Mchem15);
        edt_Mchem16=findViewById(R.id.edt_Mchem16);
        edt_Mchem17=findViewById(R.id.edt_Mchem17);
        edt_Mchem18=findViewById(R.id.edt_Mchem18);
        edt_Mchem19=findViewById(R.id.edt_Mchem19);
        edt_Mchem20=findViewById(R.id.edt_Mchem20);

        edt_Mpig1=findViewById(R.id.edt_Mpig1);
        edt_Mpig2=findViewById(R.id.edt_Mpig2);
        edt_Mpig3=findViewById(R.id.edt_Mpig3);
        edt_Mpig4=findViewById(R.id.edt_Mpig4);
        edt_Mpig5=findViewById(R.id.edt_Mpig5);
        edt_Mpig6=findViewById(R.id.edt_Mpig6);
        edt_Mpig7=findViewById(R.id.edt_Mpig7);
        edt_Mpig8=findViewById(R.id.edt_Mpig8);
        edt_Mpig9=findViewById(R.id.edt_Mpig9);
        edt_Mpig10=findViewById(R.id.edt_Mpig10);

        tv_raw11111 = findViewById(R.id.tv_raw11111);
        tv_ngay = findViewById(R.id.tv_ngay);
        tv_may1 = findViewById(R.id.tv_may1);
        tv_may2 = findViewById(R.id.tv_may2);

        tv_rawtong1 = findViewById(R.id.tv_rawtong1);
        tv_rawtong2 = findViewById(R.id.tv_rawtong2);
        tv_rawtongtay = findViewById(R.id.tv_rawtongtay);
        tv_rawtong = findViewById(R.id.tv_rawtong);

        tv_chemtong1 = findViewById(R.id.tv_chemtong1);
        tv_chemtong2 = findViewById(R.id.tv_chemtong2);
        tv_chemtongtay = findViewById(R.id.tv_chemtongtay);
        tv_chemtong = findViewById(R.id.tv_chemtong);

        tv_pigtong1 = findViewById(R.id.tv_pigtong1);
        tv_pigtong2 = findViewById(R.id.tv_pigtong2);
        tv_pigtongtay = findViewById(R.id.tv_pigtongtay);
        tv_pigtong = findViewById(R.id.tv_pigtong);
    }

    //fix height list view
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height=0;
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            view.measure(0, 0);
            totalHeight += view.getMeasuredHeight()-20;
        }
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }


    public class  QSMac1 extends ApiReturnModel<ArrayList<QAM_SCALE_MAC1>> {}

    public class  QSMac2 extends ApiReturnModel<ArrayList<QAM_SCALE_MAC2>> {}

    public class  QSMachand extends ApiReturnModel<ArrayList<QAM_SCALE_HAND>> {}

    public class McsArm extends ApiReturnModel<ArrayList<MCS>> {
    }

    public class  NoteArm extends ApiReturnModel<ArrayList<QAM_NOTE>>{}

    public class  AmtArm extends ApiReturnModel<ArrayList<QAM_AMT>>{}

    public class InsArm extends ApiReturnModel<ArrayList<QAM_INS>> {}

    public class IdColorMCS extends ApiReturnModel<ArrayList<Temp2>> {}




}
