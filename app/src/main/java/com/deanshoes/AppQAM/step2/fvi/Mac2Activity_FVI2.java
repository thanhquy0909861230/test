package com.deanshoes.AppQAM.step2.fvi;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.BuildConfig;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.ListBatchAdapter;
import com.deanshoes.AppQAM.model.QAM.MCS;
import com.deanshoes.AppQAM.model.QAM.MODEL_SEND_MAIL;
import com.deanshoes.AppQAM.model.QAM.QAM_BTC;
import com.deanshoes.AppQAM.model.QAM.QAM_INS;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step1.ChemicalActivity;
import com.deanshoes.AppQAM.step2.ER;
import com.deanshoes.AppQAM.step3.FVI.Kneader_Controller;
import com.deanshoes.AppQAM.step4.SendMail;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Mac2Activity_FVI2 extends AppCompatActivity {
    private Spinner spr_groupid,spr_shift2,spr_shiftBatch;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private EditText edt_date2,edt_batchno2,edt_style2,edt_color2,edt_hard2,tv_mcs;
    private ArrayAdapter mcsAdapter,ShiftAdapter;
    //    TextView tv_mcs;
    private List<MCS> mcsinfor;
    private List<QAM_INS> insl;
    private ArrayList<QAM_BTC> btclst;
    String sub;
    com.deanshoes.AppQAM.adapter.ListBatchAdapter ListBatchAdapter;
    ListView lv_batch_no;
    ImageButton ib_logout;

    ArrayList<String> arrGroupID;
    ArrayList<QAM_INS> arrINS;
    boolean creMode=false;

    Button btn_insert2,btn_save2,btn_cancel2,btn_step1,btn_step2,btn_step3,btn_step4,
            btn_part1,btn_part4,btn_delete,btn_print;

    public static final String FONTF = "assets/NotoSansCJKsc-Regular.otf";
    SimpleDateFormat datefilename = new SimpleDateFormat("yyyyMMdd");
    String NAMEPDF = "";

    DatePickerDialog datePickerDialog;
    private ArrayList<String> listShift,listMCS;
    String factory = "",user="";
    String ins_no,mcs_no,line,mcs_name;
    int locate=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mac2_fvi2);
        spr_groupid=findViewById(R.id.spr_groupid);
        edt_date2=findViewById(R.id.edt_date2);
        spr_shift2=findViewById(R.id.spr_shift2);
        edt_style2=findViewById(R.id.edt_style2);
        edt_color2=findViewById(R.id.edt_color2);
        edt_batchno2=findViewById(R.id.edt_batchno2);
//        edt_hard2=findViewById(R.id.edt_hard2);
        lv_batch_no=findViewById(R.id.lv_batch_no);
        tv_mcs = findViewById(R.id.tv_mcs);
        spr_shiftBatch=findViewById(R.id.spr_shiftBatch);

        btn_insert2= findViewById(R.id.btn_insert2);
        btn_delete = findViewById(R.id.btn_delete);
        btn_save2= findViewById(R.id.btn_save2);
        btn_cancel2= findViewById(R.id.btn_cancel2);
        btn_step1= findViewById(R.id.btn_step1);
        btn_step2= findViewById(R.id.btn_step2);
        btn_step3= findViewById(R.id.btn_step3);
        btn_step4= findViewById(R.id.btn_step4);
        btn_part1=findViewById(R.id.btn_part1);
        btn_part4=findViewById(R.id.btn_part4);
        ib_logout=findViewById(R.id.ib_logout);

        btn_insert2.setEnabled(false);
        btn_delete.setEnabled(false);
        btn_print = findViewById(R.id.btn_print);
        createfolder();

        Intent intent=getIntent();
        final Bundle bundle=intent.getExtras();
        if (bundle!=null){
            user=bundle.getString("user","ADMIN");
            factory=bundle.getString("fac");
        }

        defaultSpinner();

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Mac2Activity_FVI2.this, LoginActivity.class);
                startActivity(i);
            }
        });

        edt_date2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog(edt_date2);
            }
        });

        spr_shift2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try{
                    if(edt_date2.getText().toString().isEmpty()){

                    }else{
                        spr_groupid.setEnabled(true);
                        getGROUP_ID(spr_shift2.getSelectedItem().toString(), edt_date2.getText().toString());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spr_groupid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = spr_groupid.getSelectedItem().toString();
                ins_no=spr_groupid.getSelectedItem().toString();
                String[] ins_no1 = spr_groupid.getSelectedItem().toString().split("_");
                ins_no = ins_no1[0];
                NAMEPDF = spr_groupid.getSelectedItem().toString();
                for (int i=0;i<arrGroupID.size();i++){
                    if (item.equals(arrINS.get(i).getINS_NO()) && position==i){
                        mcs_no=arrINS.get(i).getMCS_NO();
                        line=arrINS.get(i).getINS_LINE();
                        getInfor(mcs_no);
                        getListBatchNo(ins_no,edt_date2.getText().toString(),spr_shift2.getSelectedItem().toString());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btn_insert2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                creMode=true;
                btn_delete.setEnabled(false);
                btn_insert2.setEnabled(false);
                btn_step1.setEnabled(false);
                btn_step4.setEnabled(false);
                btn_step3.setEnabled(false);
                btn_part1.setEnabled(false);
                btn_part4.setEnabled(false);
                ib_logout.setEnabled(false);
                edt_date2.setEnabled(false);
                spr_shift2.setEnabled(false);
                spr_groupid.setEnabled(false);
                btn_print.setEnabled(false);
                getMaxBatch(ins_no, edt_date2.getText().toString(), spr_shiftBatch.getSelectedItem().toString(),line,mcs_name);
//                edt_date2.getText().toString()
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_dialog(btclst.get(locate).getBTC_NO());
            }
        });

        btn_save2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveBTC();
                edt_batchno2.setText("");
                ib_logout.setEnabled(true);
            }
        });

        btn_cancel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_insert2.setEnabled(true);
                btn_save2.setEnabled(false);
                btn_cancel2.setEnabled(false);
                btn_print.setEnabled(true);
                btn_step1.setEnabled(true);
                btn_step4.setEnabled(true);
                btn_step3.setEnabled(true);
                btn_part1.setEnabled(true);
                btn_part4.setEnabled(true);
                ib_logout.setEnabled(true);
                edt_date2.setEnabled(true);
                spr_shift2.setEnabled(true);
                spr_groupid.setEnabled(true);
                edt_batchno2.setText("");
            }
        });

        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String d =edt_date2.getText().toString().substring(0,2);
                String m =edt_date2.getText().toString().substring(3,5);
                String y =edt_date2.getText().toString().substring(6);
                String name="IP-BATCH TRACKING CARD.1_"+d+"-"+m+"-"+y+"_"+
                        spr_shift2.getSelectedItem().toString()+".pdf";
                String DEST = Environment.getExternalStorageDirectory().toString() + "/QAM_BATCH/" + name;
                File file = new File(DEST);
                file.getParentFile().mkdirs();
                try {
                    createPdf(DEST,btclst);
                }catch (Exception e){

                }
            }
        });

        btn_step1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Mac2Activity_FVI2.this, Mac1Activity_FVI.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        btn_step3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Mac2Activity_FVI2.this, IPBatchActivity_FVI.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        btn_step4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Mac2Activity_FVI2.this, Mac3Activity_FVI.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        btn_part1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Mac2Activity_FVI2.this, ChemicalActivity.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        btn_part4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Mac2Activity_FVI2.this, Kneader_Controller.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        lv_batch_no.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();
                if (viewId == R.id.rdo_check) {
                    edt_batchno2.setText(btclst.get(position).getBTC_BATCH_NO());
                    if (position==btclst.size()-1 && btclst.get(position).getCHECK().equals("N")) {
                        btn_delete.setEnabled(true);
                        locate = position;
                    }else{
                        btn_delete.setEnabled(false);
                    }
                }
            }
        });

    }

    public void SelectMaDon(String mcs, String color){
        if (!mcs.equals("") && !color.equals("")){
            //goi ham
        }

    }

    public void defaultSpinner() {
        listMCS = new ArrayList<>();
        mcsAdapter = new ArrayAdapter(Mac2Activity_FVI2.this, R.layout.spn_layout, listMCS);
        spr_groupid.setAdapter(mcsAdapter);

        listShift = new ArrayList<>();
        listShift.add("1");
        listShift.add("2");
        listShift.add("3");
        ShiftAdapter = new ArrayAdapter(Mac2Activity_FVI2.this, R.layout.spn_layout, listShift);
        spr_shift2.setAdapter(ShiftAdapter);
        spr_shiftBatch.setAdapter(ShiftAdapter);
    }

    public void delete_dialog(final String btc_no){
        AlertDialog.Builder b = new AlertDialog.Builder(Mac2Activity_FVI2.this);
        b.setTitle("Xác nhận");
        b.setMessage("Bạn có muốn xóa mã đợt này?");
        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                try {
                    CallApi.deleteBatchno(factory, btc_no, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            ToastCustom.message(getApplicationContext(), "Xóa thành công!", Color.GREEN);
                            getListBatchNo(ins_no,edt_date2.getText().toString(),spr_shift2.getSelectedItem().toString());
                            btn_delete.setEnabled(false);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
                } catch (JSONException e) {
                    ToastCustom.message(getApplicationContext(), "Không thể xóa!", Color.RED);
                    e.printStackTrace();
                }
            }
        });
        b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog al = b.create();
        al.show();
    }

    public void datePickerDialog(final EditText edt_date) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(Mac2Activity_FVI2.this,
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int monthcurrent=monthOfYear + 1;
                        String month="";
                        String day="";
                        if(dayOfMonth>9) {
                            day=String.valueOf(dayOfMonth);

                        }
                        else
                        {
                            day="0"+String.valueOf(dayOfMonth);
                        }
                        if(monthcurrent>9)
                        {
                            month=String.valueOf(monthcurrent);
                        }
                        else
                        {
                            month="0"+String.valueOf(monthcurrent);

                        }
                        edt_date.setText(month + "/" + day + "/" + year);

                        spr_groupid.setEnabled(true);
                        getGROUP_ID(spr_shift2.getSelectedItem().toString(), edt_date.getText().toString());

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void getGROUP_ID( String mcs_shift, String mcs_date) {
        try {
            CallApi.getGroupID(factory,mcs_shift, mcs_date, new Response.Listener<JSONObject>()
            //CallApi.getGroupIDpdf(factory,mcs_shift, mcs_date, new Response.Listener<JSONObject>()
            {
                @Override
                public void onResponse(JSONObject response) {
                    Mac2Activity_FVI2.InsArm insArm = gson.fromJson(response.toString(), Mac2Activity_FVI2.InsArm.class);
                    insl = insArm.getItems() == null ? new ArrayList<QAM_INS>() : insArm.getItems();
                    arrINS = new ArrayList<>();
                    arrGroupID = new ArrayList<>();
                    if (insl.size()>0){
                        for (QAM_INS ob:insl) {
                            arrINS.add(ob);
                            arrGroupID.add(ob.getINS_NO());
                        }
                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(getApplicationContext(), R.layout.spn_layout, arrGroupID);
//                        adapter.setDropDownViewResource(R.layout.spn_layout);
                        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                        spr_groupid.setAdapter(adapter);
                        btn_insert2.setEnabled(true);
                    }else {
                        btn_insert2.setEnabled(false);
                        btn_print.setEnabled(false);
                        spr_groupid.setAdapter(mcsAdapter);
                        tv_mcs.setText("");
                        edt_style2.setText("");
                        edt_color2.setText("");
//                        edt_hard2.setText("");
                        ListBatchAdapter = new ListBatchAdapter(getApplicationContext(),R.layout.list_batch_no_adapter,new ArrayList<QAM_BTC>());
                        lv_batch_no.setAdapter(ListBatchAdapter);
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getInfor(String mcs_no){
        try {
            CallApi.getInforMcs(factory, mcs_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Mac1Activity_FVI.McsArm mcsArm = gson.fromJson(response.toString(), Mac1Activity_FVI.McsArm.class);
                    mcsinfor = mcsArm.getItems() == null ? new ArrayList<MCS>() : mcsArm.getItems();
                    if(mcsinfor.size()!=0) {
                        tv_mcs.setText(mcsinfor.get(0).getMSC());
                        String str = mcsinfor.get(0).getMSC();
                        if (str.indexOf("CR")!=-1){
                            mcs_name = str.substring(str.indexOf("CR")+2);
                        }else{
                            if (str.indexOf("|")!=-1){
                                mcs_name=str.substring(str.indexOf("|")+1);
                            }else mcs_name=str;
                        }

                        edt_style2.setText(mcsinfor.get(0).getMCS_STYLE());
                        edt_color2.setText(mcsinfor.get(0).getMCS_COLOR());
//                        edt_hard2.setText(mcsinfor.get(0).getMCS_HARD());
                    }else{
                        tv_mcs.setText("");
                        edt_color2.setText("");
//                        edt_hard2.setText("");
                        edt_style2.setText("");
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getListBatchNo(String ins_no,String date,String shift){
        try{
            CallApi.getListBTC(factory, ins_no, date, shift , new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    btclst=new ArrayList<>();
                    Mac2Activity_FVI.BtcArm btcArm= gson.fromJson(response.toString(), Mac2Activity_FVI.BtcArm.class);
                    btclst = btcArm.getItems()== null ? new ArrayList<QAM_BTC>() : btcArm.getItems();
                    ListBatchAdapter = new ListBatchAdapter(getApplicationContext(),R.layout.list_batch_no_adapter,btclst);
                    lv_batch_no.setAdapter(ListBatchAdapter);
                    if (btclst.size()>0){
                        btn_print.setEnabled(true);
                    }else btn_print.setEnabled(false);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                }
            });

        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getMaxBatch(String ins_no,String date,String shift, String line, String mcs_name){
        sub="";
        try{
            CallApi.getMaxBatchNo(factory, ins_no, date, shift, line, mcs_name, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Mac2Activity_FVI.BatchArm batchArm= gson.fromJson(response.toString(), Mac2Activity_FVI.BatchArm.class);
                    if (batchArm.getItems() != null){
                        sub=batchArm.getItems() == null ? ""  : batchArm.getItems();
                        edt_batchno2.setText(sub);
                        btn_save2.setEnabled(true);
                        btn_cancel2.setEnabled(true);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void saveBTC(){
        boolean flag=true;
//        if(edt_line2.getText().toString().isEmpty()){
//            ToastCustom.message(getApplicationContext(), "Chuyền không thể để trống!", Color.RED);
//            flag=false;
//        }
        if (edt_date2.getText().toString().isEmpty()){
            ToastCustom.message(getApplicationContext(), "Ngày không thể để trống!", Color.RED);
            flag=false;
        }

        if (spr_groupid.getSelectedItem().toString().isEmpty()){
            ToastCustom.message(getApplicationContext(), "Mã đơn không thể để trống!", Color.RED);
            flag=false;
        }

        if (edt_batchno2.getText().toString().isEmpty()){
            ToastCustom.message(getApplicationContext(), "Mã đợt không thể để trống!", Color.RED);
            flag=false;
        }

        if (flag==true){
            final QAM_BTC btc=new QAM_BTC();
            btc.setMCS_NO(mcs_no);
            btc.setBTC_STYLE(edt_style2.getText().toString());
            btc.setBTC_COLOR(edt_color2.getText().toString());
            btc.setBTC_HARD(mcsinfor.get(0).getMCS_HARD() == null ? "" : mcsinfor.get(0).getMCS_HARD());
            btc.setBTC_DATE(edt_date2.getText().toString());
            btc.setBTC_SHIFT(spr_shiftBatch.getSelectedItem().toString());
            btc.setBTC_LINE(line);
            btc.setBTC_BATCH_NO(edt_batchno2.getText().toString());
            btc.setUP_USER(user);
//            btc.setINS_NO(spr_groupid.getSelectedItem().toString());
            btc.setINS_NO(ins_no);
            try{
                CallApi.insertBTC(factory, btc, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        btn_insert2.setEnabled(true);
                        btn_save2.setEnabled(false);
                        btn_cancel2.setEnabled(false);
                        btn_print.setEnabled(true);
                        btn_step1.setEnabled(true);
                        btn_step4.setEnabled(true);
                        btn_step3.setEnabled(true);
                        btn_part1.setEnabled(true);
                        btn_part4.setEnabled(true);
                        edt_date2.setEnabled(true);
                        spr_shift2.setEnabled(true);
                        spr_groupid.setEnabled(true);
                        ToastCustom.message(getApplicationContext(), "Thêm dữ liệu thành công", Color.GREEN);
                        getListBatchNo(ins_no,edt_date2.getText().toString(),spr_shift2.getSelectedItem().toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    MODEL_SEND_MAIL model;
    ArrayList<String> batch;
    public void createPdf(String dest, ArrayList<QAM_BTC> btc) throws IOException, DocumentException {

        Document document = new Document();
        File f = new File(dest);
        if (f.exists()){
            f.delete();
        }
        try {
            Font font = FontFactory.getFont(FONTF, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            font.setSize(6);
            Font font2 = FontFactory.getFont(FONTF, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            font2.setStyle(Font.BOLD);
            font2.setSize(12);
            Font font3 = FontFactory.getFont(FONTF, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            font3.setSize(5);
            Font font4 = FontFactory.getFont(FONTF, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            font4.setSize(10);
            Font fontdynamic = FontFactory.getFont(FONTF, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            fontdynamic.setSize(11);
            fontdynamic.setColor(BaseColor.RED);


            PdfWriter.getInstance(document, new FileOutputStream(dest));
            document.open();
            if (btc.size()>0){
                model = new MODEL_SEND_MAIL();
                model.setMSC(tv_mcs.getText().toString());
                model.setDATE(btc.get(0).getBTC_DATE());
                model.setSHIFT(btc.get(0).getBTC_SHIFT());
                model.setSTYLE(edt_style2.getText().toString());
                model.setCOLOR(edt_color2.getText().toString());
                PdfPTable outertable = new PdfPTable(2);
                outertable.setWidthPercentage(110);
                batch = new ArrayList<>();

                for (int i=0;i<btc.size();i++){
                    batch.add(btc.get(i).getBTC_BATCH_NO());
                    if(btc.size()%2!=0){
                        if (i==btc.size()-1){
                            PdfPTable ntable = new PdfPTable(2);
                            ntable.setWidthPercentage(50);

                            //head
                            PdfPCell ncellhead = new PdfPCell(new Paragraph("    ", font2));
                            ncellhead.setColspan(2);
                            ncellhead.setHorizontalAlignment(Element.ALIGN_CENTER);
                            ncellhead.setFixedHeight(45);
                            ncellhead.setBorder(Rectangle.NO_BORDER);
                            ntable.addCell(ncellhead);

                            //dong 1
                            PdfPCell ncell1 = new PdfPCell(new Paragraph(" " , font4));
                            ncell1.setColspan(2);
                            ncell1.setBorder(Rectangle.NO_BORDER);
                            ncell1.setFixedHeight(30);
                            ntable.addCell(ncell1);

                            //dong 2
                            PdfPCell ncell2 = new PdfPCell(new Paragraph(" " , font4));
                            ncell2.setColspan(2);
                            ncell2.setBorder(Rectangle.NO_BORDER);
                            ncell2.setFixedHeight(30);
                            ncell2.setVerticalAlignment(Element.ALIGN_CENTER);
                            ntable.addCell(ncell2);

                            //dong 3
                            PdfPCell ncell3 = new PdfPCell(new Paragraph(" " , font4));
                            ncell3.setColspan(2);
                            ncell3.setFixedHeight(30);
                            ncell3.setBorder(Rectangle.NO_BORDER);
                            ntable.addCell(ncell3);
                            // //dong 4
                            PdfPCell ncell4 = new PdfPCell(new Paragraph(" " , font4));
                            ncell4.setColspan(1);
                            ncell4.setFixedHeight(30);
                            ncell4.setBorder(Rectangle.NO_BORDER);
                            PdfPCell ncell5 = new PdfPCell(new Paragraph("  " , font4));
                            ncell5.setColspan(1);
                            ncell5.setFixedHeight(30);
                            ncell5.setBorder(Rectangle.NO_BORDER);
                            ntable.addCell(ncell4);
                            ntable.addCell(ncell5);

                            // //dong 5
                            PdfPCell ncell6 = new PdfPCell(new Paragraph(" " , font4));
                            ncell6.setColspan(2);
                            ncell6.setFixedHeight(30);
                            ncell6.setBorder(Rectangle.NO_BORDER);
                            ntable.addCell(ncell6);
                            //dong 6
                            PdfPCell ncell7 = new PdfPCell(new Paragraph(" ", font4));
                            ncell7.setColspan(2);
                            ncell7.setFixedHeight(30);
                            ncell7.setBorder(Rectangle.NO_BORDER);
                            ntable.addCell(ncell7);

                            PdfPCell ncell8 = new PdfPCell(new Paragraph(" ", font4));
                            ncell8.setColspan(2);
                            ncell8.setFixedHeight(30);
                            ncell8.setBorder(Rectangle.NO_BORDER);
                            ntable.addCell(ncell8);
                            //dong 9 tgsd
                            PdfPCell ncell9 = new PdfPCell(new Paragraph("" , font4));
                            ncell9.setColspan(2);
                            ncell9.setFixedHeight(30);
                            ncell9.setBorder(Rectangle.NO_BORDER);
                            ntable.addCell(ncell9);
                            PdfPCell outercell1 = new PdfPCell(ntable);
                            outercell1.setPadding(15);
                            outercell1.setBorder(Rectangle.NO_BORDER);
                            outertable.addCell(outercell1);
                        }
                    }
                    PdfPTable table = new PdfPTable(2);
                    table.setWidthPercentage(50);

                    //head

                    if(factory.equals("FVI")){
                        PdfPCell cellhead = new PdfPCell(new Paragraph("IP - BATCH TRACKING CARD .1 \n \n THẺ THEO DÕI IP -", font2));
                        cellhead.setColspan(2);
                        cellhead.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cellhead.setFixedHeight(45);
                        table.addCell(cellhead);
                    }else if (factory.equals("FVI_II_305")){
                        PdfPCell cellhead = new PdfPCell(new Paragraph("ID - BATCH TRACKING CARD .1 \n \n THẺ THEO DÕI ID -", font2));
                        cellhead.setColspan(2);
                        cellhead.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cellhead.setFixedHeight(45);
                        table.addCell(cellhead);
                    }


                    //dong 1
                    PdfPCell cell1 = new PdfPCell(new Paragraph("批號/Batch No/Số Đợt: " + btc.get(i).getBTC_BATCH_NO() , font4));
                    cell1.setColspan(2);
                    cell1.setFixedHeight(30);
                    table.addCell(cell1);

                    //dong 2
                    PdfPCell cell2 = new PdfPCell(new Paragraph("日期/Date/Ngày: " + btc.get(i).getBTC_DATE(), font4));
                    cell2.setColspan(2);
                    cell2.setFixedHeight(30);
                    cell2.setVerticalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell2);

                    //dong 3
                    PdfPCell cell3 = new PdfPCell(new Paragraph("班別/Shift/Ca biệt: " + btc.get(i).getBTC_SHIFT(), font4));
                    cell3.setColspan(2);
                    cell3.setFixedHeight(30);
                    table.addCell(cell3);
                    // //dong 4
                    PdfPCell cell4 = new PdfPCell(new Paragraph("MCS#: " + tv_mcs.getText().toString(), font4));
                    cell4.setColspan(1);
                    cell4.setFixedHeight(30);
                    String str = btc.get(i).getER();
                    if (str!=null && !str.equals("")){
                        PdfPCell cell5 = new PdfPCell(new Paragraph("ER :  " +
                                str.substring(0,str.indexOf(":"))+"±"+str.substring(str.indexOf(":")+1)+"%", font4));
                        cell5.setColspan(1);
                        cell5.setFixedHeight(30);
                        table.addCell(cell4);
                        table.addCell(cell5);
                    }else{
                        PdfPCell cell5 = new PdfPCell(new Paragraph("ER :  "));
                        cell5.setColspan(1);
                        cell5.setFixedHeight(30);
                        table.addCell(cell4);
                        table.addCell(cell5);
                    }



                    // //dong 5
                    PdfPCell cell6 = new PdfPCell(new Paragraph("型體/Model/Hình thể: " + edt_style2.getText().toString(), font4));
                    cell6.setColspan(2);
                    cell6.setFixedHeight(30);
                    table.addCell(cell6);
                    //dong 6

                    if (factory.equals("FVI_II_305")){
                        String date = edt_date2.getText().toString().substring(3,5)+edt_date2.getText().toString().substring(0,2);
                        String batch = btc.get(i).getBTC_BATCH_NO();
                        String lot_no = batch.substring(batch.indexOf(date)+4,batch.indexOf(date)+7);
                        PdfPCell cell7 = new PdfPCell(new Paragraph("流水號/Lot No/Tem Số: " + lot_no, font4));
                        cell7.setColspan(2);
                        cell7.setFixedHeight(30);
                        table.addCell(cell7);
                    }

                    PdfPCell cell8 = new PdfPCell(new Paragraph("顏色/Color/Màu Sắc: " + edt_color2.getText().toString(), font4));
                    cell8.setColspan(2);
                    cell8.setFixedHeight(30);
                    table.addCell(cell8);
                    //dong 9 tgsd
                    PdfPCell cell9 = new PdfPCell(new Paragraph("配方檢查員/Inspector/Người Kiểm Tra Phối Phương:  " , font4));
                    cell9.setColspan(2);
                    cell9.setFixedHeight(30);
                    table.addCell(cell9);

                    PdfPCell outercell = new PdfPCell(table);
                    outercell.setPadding(15);
                    outertable.addCell(outercell);
                    //add Table into PDF
                }
                document.add(outertable);
            }

            document.close();
            try {

                SendMail sendmail = new SendMail();
                sendmail.sendmail_card1(factory,dest,model,batch);
                View toastView = getLayoutInflater().inflate(R.layout.activity_toast_custom_view, null);
//
//                // Initiate the Toast instance.
                Toast toast = new Toast(getApplicationContext());
                // Set custom view in toast.
                toast.setView(toastView);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

            File file = new File(dest);
            Uri photoURI = FileProvider.getUriForFile(Mac2Activity_FVI2.this, BuildConfig.APPLICATION_ID + ".provider", file);
            Intent intent = ShareCompat.IntentBuilder.from(this)
                    .setStream(photoURI) // uri from FileProvider
                    .setType("text/html")
                    .getIntent()
                    .setAction(Intent.ACTION_VIEW) //Change if needed
                    .setDataAndType(photoURI, "application/pdf")
                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void createfolder() {
        File folder = new File(Environment.getExternalStorageDirectory() + "/QAM_BATCH");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            // Do something on success
        } else {
            // Do something else on failure
        }
    }

    public class BtcArm extends ApiReturnModel<ArrayList<QAM_BTC>> {
    }

    public class BatchArm extends ApiReturnModel<String> {
    }
    public class InsArm extends ApiReturnModel<ArrayList<QAM_INS>> {}

}
