package com.deanshoes.AppQAM.step2;

public class ER {
    public static String GET_ER(String mcs) {
        String er = "";

        if (mcs.contains("CR150")){
            er="154±2%";
        }else if(mcs.contains("CR150-4")){
            er="150±2%";
        }else if(mcs.contains("CR150-5")){
            er="156±2%";
        }else if(mcs.contains("CR150-6")){
            er="150±2%";
        }else if(mcs.contains("CR350")){
            er="151±2%";
        }else if(mcs.contains("CR250")){
            er="160±2%";
        }else if(mcs.contains("CR250-1")){
            er="158±2%";
        }else if(mcs.contains("CR350-2")){
            er="151±2%";
        }else if(mcs.contains("CR350-3")){
            er="151±2%";
        }else if(mcs.contains("CR140")){
            er="152±2%";
        }else if(mcs.contains("CR140-1")){
            er="152±2%";
        }else if(mcs.contains("CR163")){
            er="145±2%";
        }else if(mcs.contains("CR163-2")){
            er="145±2%";
        }else if(mcs.contains("CR137-1")){
            er="178±2%";
        }else if(mcs.contains("CR137-2")){
            er="172±2%";
        }else if(mcs.contains("CR154")){
            er="152±2%";
        }else if(mcs.contains("CR154-2")){
            er="152±2%";
        }else if(mcs.contains("CR145")){
            er="172±2%";
        }else if(mcs.contains("CR128-2")){
            er="172±2%";
        }else if(mcs.contains("CR128-3")){
            er="170±2%";
        }else if(mcs.contains("CR150-1")){
            er="150±2%";
        }else if(mcs.contains("CR150-2")){
            er="152±2%";
        }else if(mcs.contains("CR150-3")){
            er="153±2%";
        }else if(mcs.contains("S")){
            er="100±2%";
        }else if(mcs.contains("L")){
            er="200±2%";
        }

        return er;
    }
}
