package com.deanshoes.AppQAM.step2.fvi;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.LoginActivity;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.ListMac3Adapter;
import com.deanshoes.AppQAM.fragment.IP_BatchFragment;
import com.deanshoes.AppQAM.model.QAM.MCS;
import com.deanshoes.AppQAM.model.QAM.QAM_AMT;
import com.deanshoes.AppQAM.model.QAM.QAM_BTC;
import com.deanshoes.AppQAM.model.QAM.QAM_INS;
import com.deanshoes.AppQAM.model.QAM.QAM_MAC;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_MAC1;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step1.ChemicalActivity;
import com.deanshoes.AppQAM.step3.FVI.Kneader_Controller;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

public class Mac3Activity_FVI extends AppCompatActivity {
    private Spinner spr_groupid,spr_shift;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private List<MCS> mcsl,mcsinfor;
    private ArrayList<QAM_MAC> maclst;
    public  ArrayList<QAM_MAC> list_mac1;
    static String[] scale1;
    ListMac3Adapter listMac3Adapter;
    ListView lv_mac;

    private ArrayList<String> rawl, cheml,pigl;
    ArrayAdapter RawAdapter,ChemAdapter,PigAdapter;

    Spinner sp_raw1,sp_raw2,sp_raw3,sp_raw4,sp_raw5,sp_raw6,sp_raw7,sp_raw8,sp_raw9,sp_raw10;
    Spinner sp_chem1,sp_chem2,sp_chem3,sp_chem4,sp_chem5,sp_chem6,sp_chem7,sp_chem8,sp_chem9,sp_chem10;
    Spinner sp_chem11,sp_chem12,sp_chem13,sp_chem14,sp_chem15,sp_chem16,sp_chem17,sp_chem18,sp_chem19,sp_chem20;
    Spinner sp_pig1,sp_pig2,sp_pig3,sp_pig4,sp_pig5,sp_pig6,sp_pig7,sp_pig8,sp_pig9,sp_pig10;

    EditText edt_raw1,edt_raw2,edt_raw3,edt_raw4,edt_raw5,edt_raw6,edt_raw7,edt_raw8,edt_raw9,edt_raw10;
    EditText edt_chem1,edt_chem2,edt_chem3,edt_chem4,edt_chem5,edt_chem6,edt_chem7,edt_chem8,edt_chem9,edt_chem10;
    EditText edt_chem11,edt_chem12,edt_chem13,edt_chem14,edt_chem15,edt_chem16,edt_chem17,edt_chem18,edt_chem19,edt_chem20;
    EditText edt_pig1,edt_pig2,edt_pig3,edt_pig4,edt_pig5,edt_pig6,edt_pig7,edt_pig8,edt_pig9,edt_pig10;

    EditText edt_date,edt_style,edt_color,edt_hard,tv_count;
    TextView[] tvl;
    TextView tv_rawtong1, tv_rawtong2, tv_rawtongtay, tv_rawtong,
    tv_chemtong1, tv_chemtong2, tv_chemtongtay, tv_chemtong,
    tv_pigtong1, tv_pigtong2, tv_pigtongtay, tv_pigtong, tv_may1, tv_may2;

    DatePickerDialog datePickerDialog;
    ImageButton ib_logout;
    TextView tv_mcs;
    Button btn_edit,btn_save,btn_cancel,btn_step1,btn_step2,btn_step3,btn_step4,btn_part1,btn_part4, btn_new;
    String factory = "",user="";
    String mcs_no,ins_no;
    String[] ins_no1;
    private List<QAM_INS> insl;
    ArrayList<QAM_INS> arrINS;
    QAM_MAC MAC_CUR;
    private ArrayList<QAM_AMT> amtlst;

    private ArrayList<String> listShift,arrGroupID;
    private ArrayAdapter InsAdapter,ShiftAdapter;
    Spinner[] spnl;
    EditText[] edtl;
    boolean creMode=false;
    String [] strl3;
    int posAMT=-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mac3_fvi);

        spr_groupid=findViewById(R.id.spr_groupid);
        edt_date=findViewById(R.id.edt_date);
        spr_shift=findViewById(R.id.spr_shift);
        edt_style=findViewById(R.id.edt_style);
        edt_color=findViewById(R.id.edt_color);
        edt_hard=findViewById(R.id.edt_hard);
//        lv_mac=findViewById(R.id.lv_mac);
        ib_logout=findViewById(R.id.ib_logout);
        tv_mcs=findViewById(R.id.tv_mcs);
        tv_count = findViewById(R.id.tv_count);

        tv_rawtong1 = findViewById(R.id.tv_rawtong1);
        tv_rawtong2 = findViewById(R.id.tv_rawtong2);
        tv_rawtongtay = findViewById(R.id.tv_rawtongtay);
        tv_rawtong = findViewById(R.id.tv_rawtong);
        tv_may1 = findViewById(R.id.tv_may1);
        tv_may2 = findViewById(R.id.tv_may2);

        tv_chemtong1 = findViewById(R.id.tv_chemtong1);
        tv_chemtong2 = findViewById(R.id.tv_chemtong2);
        tv_chemtongtay = findViewById(R.id.tv_chemtongtay);
        tv_chemtong = findViewById(R.id.tv_chemtong);

        tv_pigtong1 = findViewById(R.id.tv_pigtong1);
        tv_pigtong2 = findViewById(R.id.tv_pigtong2);
        tv_pigtongtay = findViewById(R.id.tv_pigtongtay);
        tv_pigtong = findViewById(R.id.tv_pigtong);
        
        final Intent intent=getIntent();
        final Bundle bundle=intent.getExtras();
        if (bundle!=null){
            user=bundle.getString("user","ADMIN");
            factory=bundle.getString("fac");
        }
        
        fintViewbyId();
        spnl=new Spinner[]{sp_raw1, sp_raw2, sp_raw3,sp_raw4,sp_raw5,sp_raw6,sp_raw7,sp_raw8,sp_raw9,sp_raw10,
                sp_chem1,sp_chem2,sp_chem3,sp_chem4,sp_chem5,sp_chem6,sp_chem7,sp_chem8,sp_chem9,sp_chem10,
                sp_chem11,sp_chem12,sp_chem13,sp_chem14,sp_chem15,sp_chem16,sp_chem17,sp_chem18,sp_chem19,sp_chem20,
                sp_pig1,sp_pig2,sp_pig3,sp_pig4,sp_pig5,sp_pig6,sp_pig7,sp_pig8,sp_pig9,sp_pig10};

        edtl=new EditText[]{edt_raw1, edt_raw2, edt_raw3,edt_raw4,edt_raw5,edt_raw6,edt_raw7,edt_raw8,edt_raw9,edt_raw10,
                edt_chem1,edt_chem2,edt_chem3,edt_chem4,edt_chem5,edt_chem6,edt_chem7,edt_chem8,edt_chem9,edt_chem10,
                edt_chem11,edt_chem12,edt_chem13,edt_chem14,edt_chem15,edt_chem16,edt_chem17,edt_chem18,edt_chem19,edt_chem20,
                edt_pig1,edt_pig2,edt_pig3,edt_pig4,edt_pig5,edt_pig6,edt_pig7,edt_pig8,edt_pig9,edt_pig10};

        tvl = new TextView[]{tv_rawtong1, tv_rawtong2, tv_rawtongtay, tv_may1,
                tv_chemtong1, tv_chemtong2, tv_chemtongtay, tv_may2,
                tv_pigtong1, tv_pigtong2, tv_pigtongtay, tv_pigtong};

        setDisable();
        defaultSpinner();
        getRaw_Chem_Pig();

        for (int i=0;i<spnl.length;i++){
            setEnableTextview(spnl[i],edtl[i]);
        }

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Mac3Activity_FVI.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btn_edit.setEnabled(false);

        edt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog(edt_date);
            }
        });


        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                creMode=true;
                btn_edit.setEnabled(false);
                btn_save.setEnabled(true);
                btn_cancel.setEnabled(true);
                btn_new.setEnabled(true);
                btn_step1.setEnabled(false);
                btn_step2.setEnabled(false);
                btn_step3.setEnabled(false);
                btn_part1.setEnabled(false);
                btn_part4.setEnabled(false);
                edt_date.setEnabled(false);
                spr_shift.setEnabled(false);
                spr_groupid.setEnabled(false);
                ib_logout.setEnabled(false);
                //enable spiner
//                setEnable();
                //enable edittext
                enbEdittext();
//                listMac3Adapter.disableitem(false);

            }
        });
        spr_shift.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try{
                    if(edt_date.getText().toString().isEmpty()){

                    }else{
                        spr_groupid.setEnabled(true);
                        getGROUP_ID(edt_date.getText().toString(), spr_shift.getSelectedItem().toString());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spr_groupid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = spr_groupid.getSelectedItem().toString();
                ins_no1 = spr_groupid.getSelectedItem().toString().split("_");
                ins_no = ins_no1[0];
                for (int i=0;i<arrINS.size();i++){
                    if (item.equals(arrINS.get(i).getINS_NO()) && position==i){
                        mcs_no=arrINS.get(i).getMCS_NO();
                        getInfor(mcs_no);
                        getMac(ins_no,spr_shift.getSelectedItem().toString(),edt_date.getText().toString());

                    }
                }
                getsum(factory,ins_no);
//                if (item.equals("8020322101_FFN")){
//                    getsum(factory,ins_no);
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    saveMAC();
                    ib_logout.setEnabled(true);
//                    listMac3Adapter.disableitem(true);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                creMode=false;
                btn_edit.setEnabled(true);
                btn_save.setEnabled(false);
                btn_cancel.setEnabled(false);
                btn_step1.setEnabled(true);
                btn_step2.setEnabled(true);
                btn_step3.setEnabled(true);
                btn_part1.setEnabled(true);
                btn_part4.setEnabled(true);
                edt_date.setEnabled(true);
                spr_shift.setEnabled(true);
                spr_groupid.setEnabled(true);
                ib_logout.setEnabled(true);
                clear();
                setDisable();
//                listMac3Adapter.disableitem(true);
            }
        });

//        lv_mac.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                long viewId = view.getId();
//                if (viewId == R.id.rdo_check) {
//                    btn_edit.setEnabled(true);
//                    btn_save.setEnabled(false);
//                    btn_cancel.setEnabled(false);
//                    selectMac=true;
//                    posMac=position;
//                    clear();
//                    
//                }
//            }
//        });

        btn_step2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (factory.equals("FVI_II_305")) {
                    Intent i = new Intent(Mac3Activity_FVI.this, Mac2Activity_FVI2.class);
                    i.putExtras(bundle);
                    startActivity(i);
                } else {
                    Intent i = new Intent(Mac3Activity_FVI.this, Mac2Activity_FVI.class);
                    i.putExtras(bundle);
                    startActivity(i);
                }
            }
        });

        btn_step3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Mac3Activity_FVI.this, IPBatchActivity_FVI.class);
                i.putExtras(bundle);
                startActivity(i);

            }
        });

        btn_step1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Mac3Activity_FVI.this, Mac1Activity_FVI.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });


        btn_part1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Mac3Activity_FVI.this, ChemicalActivity.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        btn_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getsum(factory, ins_no);
            }
        });

        btn_part4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Mac3Activity_FVI.this,Kneader_Controller.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });
    }



    public void getInfor(String mcs_no){
        try {
            CallApi.getInforMcs(factory, mcs_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    McsArm mcsArm = gson.fromJson(response.toString(), McsArm.class);
                    mcsinfor = mcsArm.getItems() == null ? new ArrayList<MCS>() : mcsArm.getItems();
                    if(mcsinfor.size()!=0) {
                        tv_mcs.setText(mcsinfor.get(0).getMSC());
                        edt_style.setText(mcsinfor.get(0).getMCS_STYLE());
                        edt_color.setText(mcsinfor.get(0).getMCS_COLOR());
                        edt_hard.setText(mcsinfor.get(0).getMCS_HARD());
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getRaw_Chem_Pig(){
        try {
            CallApi.getRaw_MTR(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    rawl=new ArrayList<>();
                    RawArm rawArm = gson.fromJson(response.toString(), RawArm.class);
                    rawl = rawArm.getItems() == null ? new ArrayList<String>() : rawArm.getItems();
                    rawl.add(0,"");
                    RawAdapter = new ArrayAdapter(getApplicationContext(), R.layout.spn_layout, rawl);
                    RawAdapter.setDropDownViewResource(R.layout.spn_layout);
                    Spinner[] spnRaw = new Spinner[]{sp_raw1, sp_raw2, sp_raw3,sp_raw4,sp_raw5,sp_raw6,sp_raw7,sp_raw8,sp_raw9,sp_raw10};
                    for(Spinner spn:spnRaw){
                        spn.setAdapter(RawAdapter);
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });

            CallApi.getChem(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    cheml=new ArrayList<>();
                    ChemArm chemArm = gson.fromJson(response.toString(), ChemArm.class);
                    cheml = chemArm.getItems() == null ? new ArrayList<String>() : chemArm.getItems();
                    cheml.add(0,"");
                    ChemAdapter = new ArrayAdapter(getApplicationContext(), R.layout.spn_layout, cheml);
                    ChemAdapter.setDropDownViewResource(R.layout.spn_layout);
                    Spinner[] spnChem = new Spinner[]{sp_chem1,sp_chem2,sp_chem3,sp_chem4,sp_chem5,sp_chem6,sp_chem7,sp_chem8,sp_chem9,sp_chem10,
                            sp_chem11,sp_chem12,sp_chem13,sp_chem14,sp_chem15,sp_chem16,sp_chem17,sp_chem18,sp_chem19,sp_chem20};
                    for(Spinner spn:spnChem){
                        spn.setAdapter(ChemAdapter);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });

            CallApi.getPig(factory, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    pigl=new ArrayList<>();
                    PigArm pigArm = gson.fromJson(response.toString(), PigArm.class);
                    pigl = pigArm.getItems() == null ? new ArrayList<String>() : pigArm.getItems();
                    pigl.add(0,"");
                    PigAdapter = new ArrayAdapter(getApplicationContext(), R.layout.spn_layout, pigl);
                    PigAdapter.setDropDownViewResource(R.layout.spn_layout);
                    Spinner[] spnPig = new Spinner[]{sp_pig1,sp_pig2,sp_pig3,sp_pig4,sp_pig5,sp_pig6,sp_pig7,sp_pig8,sp_pig9,sp_pig10};
                    for(Spinner sp:spnPig){
                        sp.setAdapter(PigAdapter);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void getMac(String ins_no, String shift, String date){
        try{
            CallApi.getMac3(factory, ins_no, shift, date,new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    maclst=new ArrayList<>();
                    MAC_CUR = new QAM_MAC();
                    MacArm macArm= gson.fromJson(response.toString(), MacArm.class);
                    maclst = macArm.getItems()== null ? new ArrayList<QAM_MAC>() : macArm.getItems();
                    clear();
                    if (maclst.size()>0){
                        tv_count.setText(maclst.get(0).getKEY());
                        Integer[] positionl=new Integer[]{rawl.indexOf(maclst.get(0).getMAC_MTR1()),rawl.indexOf(maclst.get(0).getMAC_MTR2()),rawl.indexOf(maclst.get(0).getMAC_MTR3()),
                                rawl.indexOf(maclst.get(0).getMAC_MTR4()),rawl.indexOf(maclst.get(0).getMAC_MTR5()),rawl.indexOf(maclst.get(0).getMAC_MTR6()),rawl.indexOf(maclst.get(0).getMAC_MTR7()),
                                rawl.indexOf(maclst.get(0).getMAC_MTR8()),rawl.indexOf(maclst.get(0).getMAC_MTR9()),rawl.indexOf(maclst.get(0).getMAC_MTR10()),cheml.indexOf(maclst.get(0).getMAC_CHEM1()),
                                cheml.indexOf(maclst.get(0).getMAC_CHEM2()),cheml.indexOf(maclst.get(0).getMAC_CHEM3()),cheml.indexOf(maclst.get(0).getMAC_CHEM4()),cheml.indexOf(maclst.get(0).getMAC_CHEM5()),
                                cheml.indexOf(maclst.get(0).getMAC_CHEM6()),cheml.indexOf(maclst.get(0).getMAC_CHEM7()),cheml.indexOf(maclst.get(0).getMAC_CHEM8()),cheml.indexOf(maclst.get(0).getMAC_CHEM9()),
                                cheml.indexOf(maclst.get(0).getMAC_CHEM10()),cheml.indexOf(maclst.get(0).getMAC_CHEM11()),cheml.indexOf(maclst.get(0).getMAC_CHEM12()),cheml.indexOf(maclst.get(0).getMAC_CHEM13()),
                                cheml.indexOf(maclst.get(0).getMAC_CHEM14()),cheml.indexOf(maclst.get(0).getMAC_CHEM15()),cheml.indexOf(maclst.get(0).getMAC_CHEM16()),cheml.indexOf(maclst.get(0).getMAC_CHEM17()),
                                cheml.indexOf(maclst.get(0).getMAC_CHEM18()),cheml.indexOf(maclst.get(0).getMAC_CHEM19()),cheml.indexOf(maclst.get(0).getMAC_CHEM20()),pigl.indexOf(maclst.get(0).getMAC_PIG1()),
                                pigl.indexOf(maclst.get(0).getMAC_PIG2()),pigl.indexOf(maclst.get(0).getMAC_PIG3()),pigl.indexOf(maclst.get(0).getMAC_PIG4()),pigl.indexOf(maclst.get(0).getMAC_PIG5()),
                                pigl.indexOf(maclst.get(0).getMAC_PIG6()),pigl.indexOf(maclst.get(0).getMAC_PIG7()),pigl.indexOf(maclst.get(0).getMAC_PIG8()),pigl.indexOf(maclst.get(0).getMAC_PIG9()),
                                pigl.indexOf(maclst.get(0).getMAC_PIG10())};

                        for (int i=0;i<spnl.length;i++){
                            spnl[i].setSelection(positionl[i]== -1 ? 0 : positionl[i]);
                        }

                        String[] strl=new String[]{maclst.get(0).getSMAC_MTR1(),maclst.get(0).getSMAC_MTR2(),maclst.get(0).getSMAC_MTR3(),
                                maclst.get(0).getSMAC_MTR4(),maclst.get(0).getSMAC_MTR5(),maclst.get(0).getSMAC_MTR6(),maclst.get(0).getSMAC_MTR7(),
                                maclst.get(0).getSMAC_MTR8(),maclst.get(0).getSMAC_MTR9(),maclst.get(0).getSMAC_MTR10(),maclst.get(0).getSMAC_CHEM1(),
                                maclst.get(0).getSMAC_CHEM2(),maclst.get(0).getSMAC_CHEM3(),maclst.get(0).getSMAC_CHEM4(),maclst.get(0).getSMAC_CHEM5(),
                                maclst.get(0).getSMAC_CHEM6(),maclst.get(0).getSMAC_CHEM7(),maclst.get(0).getSMAC_CHEM8(),maclst.get(0).getSMAC_CHEM9(),
                                maclst.get(0).getSMAC_CHEM10(),maclst.get(0).getSMAC_CHEM11(),maclst.get(0).getSMAC_CHEM12(),maclst.get(0).getSMAC_CHEM13(),
                                maclst.get(0).getSMAC_CHEM14(),maclst.get(0).getSMAC_CHEM15(),maclst.get(0).getSMAC_CHEM16(),maclst.get(0).getSMAC_CHEM17(),
                                maclst.get(0).getSMAC_CHEM18(),maclst.get(0).getSMAC_CHEM19(),maclst.get(0).getSMAC_CHEM20(),maclst.get(0).getSMAC_PIG1(),
                                maclst.get(0).getSMAC_PIG2(),maclst.get(0).getSMAC_PIG3(),maclst.get(0).getSMAC_PIG4(),maclst.get(0).getSMAC_PIG5(),
                                maclst.get(0).getSMAC_PIG6(),maclst.get(0).getSMAC_PIG7(),maclst.get(0).getSMAC_PIG8(),maclst.get(0).getSMAC_PIG9(),
                                maclst.get(0).getSMAC_PIG10()};
                        for (int i=0;i<edtl.length;i++){
                            edtl[i].setText(strl[i]);
                        }
                        if (Integer.parseInt(maclst.get(0).getKEY())>0){
                            btn_edit.setEnabled(true);
                        }else{
                            btn_edit.setEnabled(false);
                        }

                        MAC_CUR.setSMAC_MTR1(strl[0]);
                        MAC_CUR.setSMAC_MTR2(strl[1]);
                        MAC_CUR.setSMAC_MTR3(strl[2]);
                        MAC_CUR.setSMAC_MTR4(strl[3]);
                        MAC_CUR.setSMAC_MTR5(strl[4]);
                        MAC_CUR.setSMAC_MTR6(strl[5]);
                        MAC_CUR.setSMAC_MTR7(strl[6]);
                        MAC_CUR.setSMAC_MTR8(strl[7]);
                        MAC_CUR.setSMAC_MTR9(strl[8]);
                        MAC_CUR.setSMAC_MTR10(strl[9]);

                        MAC_CUR.setSMAC_CHEM1(strl[10]);
                        MAC_CUR.setSMAC_CHEM2(strl[11]);
                        MAC_CUR.setSMAC_CHEM3(strl[12]);
                        MAC_CUR.setSMAC_CHEM4(strl[13]);
                        MAC_CUR.setSMAC_CHEM5(strl[14]);
                        MAC_CUR.setSMAC_CHEM6(strl[15]);
                        MAC_CUR.setSMAC_CHEM7(strl[16]);
                        MAC_CUR.setSMAC_CHEM8(strl[17]);
                        MAC_CUR.setSMAC_CHEM9(strl[18]);
                        MAC_CUR.setSMAC_CHEM10(strl[19]);
                        MAC_CUR.setSMAC_CHEM11(strl[20]);
                        MAC_CUR.setSMAC_CHEM12(strl[21]);
                        MAC_CUR.setSMAC_CHEM13(strl[22]);
                        MAC_CUR.setSMAC_CHEM14(strl[23]);
                        MAC_CUR.setSMAC_CHEM15(strl[24]);
                        MAC_CUR.setSMAC_CHEM16(strl[25]);
                        MAC_CUR.setSMAC_CHEM17(strl[26]);
                        MAC_CUR.setSMAC_CHEM18(strl[27]);
                        MAC_CUR.setSMAC_CHEM19(strl[28]);
                        MAC_CUR.setSMAC_CHEM20(strl[29]);

                        MAC_CUR.setSMAC_PIG1(strl[30]);
                        MAC_CUR.setSMAC_PIG2(strl[31]);
                        MAC_CUR.setSMAC_PIG3(strl[32]);
                        MAC_CUR.setSMAC_PIG4(strl[33]);
                        MAC_CUR.setSMAC_PIG5(strl[34]);
                        MAC_CUR.setSMAC_PIG6(strl[35]);
                        MAC_CUR.setSMAC_PIG7(strl[36]);
                        MAC_CUR.setSMAC_PIG8(strl[37]);
                        MAC_CUR.setSMAC_PIG9(strl[38]);
                        MAC_CUR.setSMAC_PIG10(strl[39]);




                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        }catch (JSONException e){
            e.fillInStackTrace();
        }
    }

    public boolean checkValueSpn(){
        Spinner[] spnRAW = new Spinner[]{sp_raw1, sp_raw2, sp_raw3,sp_raw4,sp_raw5,sp_raw6,sp_raw7,sp_raw8,sp_raw9,sp_raw10};
        for (int i=0;i<-1;i++){
            if (spnRAW[i].getSelectedItemPosition()==0 && spnRAW[i+1].getSelectedItemPosition()!=0){
                return true;
            }
        }

        Spinner[] spnCHEM = new Spinner[]{sp_chem1,sp_chem2,sp_chem3,sp_chem4,sp_chem5,sp_chem6,sp_chem7,sp_chem8,sp_chem9,sp_chem10,
                sp_chem11,sp_chem12,sp_chem13,sp_chem14,sp_chem15,sp_chem16,sp_chem17,sp_chem18,sp_chem19,sp_chem20};
        for (int i=0;i<spnCHEM.length-1;i++){
            if (spnCHEM[i].getSelectedItemPosition()==0 && spnCHEM[i+1].getSelectedItemPosition()!=0){
                return true;
            }
        }

        Spinner[] spnPIG = new Spinner[]{sp_pig1,sp_pig2,sp_pig3,sp_pig4,sp_pig5,sp_pig6,sp_pig7,sp_pig8,sp_pig9,sp_pig10};
        for (int i=0;i<spnPIG.length-1;i++){
            if (spnPIG[i].getSelectedItemPosition()==0 && spnPIG[i+1].getSelectedItemPosition()!=0){
                return true;
            }
        }
        return false;
    }

    public boolean checkDuplicateMTR(){
        ArrayList<String> arrMTR = new ArrayList<>();
        Spinner[] spnRaw = new Spinner[]{sp_raw1, sp_raw2, sp_raw3,sp_raw4,sp_raw5,sp_raw6,sp_raw7,sp_raw8,sp_raw9,sp_raw10};
        for(Spinner sp:spnRaw){
            if (!sp.getSelectedItem().toString().equals("")){
                arrMTR.add(sp.getSelectedItem().toString());
            }
        }

        HashSet<String> hash=new HashSet<>(arrMTR);

        if (arrMTR.size()>hash.size()){
            return true;
        }
        return false;
    }

    public boolean checkDuplicateCHEM(){
        ArrayList<String> arrMTR = new ArrayList<>();
        Spinner[] spnChem = new Spinner[]{sp_chem1,sp_chem2,sp_chem3,sp_chem4,sp_chem5,sp_chem6,sp_chem7,sp_chem8,sp_chem9,sp_chem10,
                sp_chem11,sp_chem12,sp_chem13,sp_chem14,sp_chem15,sp_chem16,sp_chem17,sp_chem18,sp_chem19,sp_chem20};
        for(Spinner sp:spnChem){
            if (!sp.getSelectedItem().toString().equals("")){
                arrMTR.add(sp.getSelectedItem().toString());
            }
        }

        HashSet<String> hash=new HashSet<>(arrMTR);

        if (arrMTR.size()>hash.size()){
            return true;
        }

        return false;
    }

    public boolean checkDuplicatePIG(){
        ArrayList<String> arrMTR = new ArrayList<>();
        Spinner[] spnPig = new Spinner[]{sp_pig1,sp_pig2,sp_pig3,sp_pig4,sp_pig5,sp_pig6,sp_pig7,sp_pig8,sp_pig9,sp_pig10};
        for(Spinner sp:spnPig){
            if (!sp.getSelectedItem().toString().equals("")){
                arrMTR.add(sp.getSelectedItem().toString());
            }
        }

        HashSet<String> hash=new HashSet<>(arrMTR);

        if (arrMTR.size()>hash.size()){
            return true;
        }

        return false;
    }

    public String getSpn(Spinner spn){
        String value="";
        if(spn.isEnabled()){
            if (spn.getSelectedItemPosition()!=0){
                value=spn.getSelectedItem().toString().trim();
            }
        }
        return value;
    }

    public String getEdt(EditText edt){
        String value="";
        if(edt.isEnabled()){
            if (!edt.getText().toString().isEmpty()){
                value=edt.getText().toString().trim();
            }
        }
        return value;
    }

    public void saveMAC(){
        boolean flag=true;

        Spinner[] spnRaw = new Spinner[]{sp_raw1, sp_raw2, sp_raw3,sp_raw4,sp_raw5,sp_raw6,sp_raw7,sp_raw8,sp_raw9,sp_raw10};
        int ra=0;
        for(Spinner sp : spnRaw){
            if(sp.getSelectedItemPosition()!=0){
                ra=1;
            }
        }
        if(ra==0){
            ToastCustom.message(getApplicationContext(), "Liệu chủ không thể để trống!", Color.RED);
            flag=false;
        }

        Spinner[] spnChem = new Spinner[]{sp_chem1,sp_chem2,sp_chem3,sp_chem4,sp_chem5,sp_chem6,sp_chem7,sp_chem8,sp_chem9,sp_chem10,
                sp_chem11,sp_chem12,sp_chem13,sp_chem14,sp_chem15,sp_chem16,sp_chem17,sp_chem18,sp_chem19,sp_chem20};
        int ch=0;
        for(Spinner sp : spnChem){
            if(sp.getSelectedItemPosition()!=0){
                ch=1;
            }
        }
        if(ch==0){
            ToastCustom.message(getApplicationContext(), "Hóa chất không thể để trống!", Color.RED);
            flag=false;
        }

//        Spinner[] spnPig = new Spinner[]{sp_pig1,sp_pig2,sp_pig3,sp_pig4,sp_pig5,sp_pig6,sp_pig7,sp_pig8,sp_pig9,sp_pig10};
//        int pi=0;
//        for(Spinner sp : spnPig){
//            if(sp.getSelectedItemPosition()!=0){
//                pi=1;
//            }
//        }
//        if (pi==0){
//            ToastCustom.message(getApplicationContext(), "Liệu màu không thể để trống!", Color.RED);
//            flag=false;
//        }
        if (checkValueSpn()==true){
            flag=false;
            ToastCustom.message(getApplicationContext(), "Liệu chủ/Hóa chất/Liệu màu phải nhập liên tiếp, không thể bỏ trống!", Color.RED);
        }

        if(checkDuplicateMTR()==true){
            flag=false;
            ToastCustom.message(getApplicationContext(), "Không thể chọn trùng lặp liệu chủ!", Color.RED);
        }
        if(checkDuplicateCHEM()==true){
            flag=false;
            ToastCustom.message(getApplicationContext(), "Không thể chọn trùng lặp Hoá chất!", Color.RED);
        }
        if(checkDuplicatePIG()==true){
            flag=false;
            ToastCustom.message(getApplicationContext(), "Không thể chọn trùng lặp Liệu màu!", Color.RED);
        }

        for (EditText edt : edtl){
            if(edt.isEnabled()){
                if (edt.getText().toString().isEmpty()){
                    ToastCustom.message(getApplicationContext(), "Khối lượng không thể để trống!", Color.RED);
                    flag=false;
                }
            }
        }

        for (EditText edt : edtl){
            if(edt.isEnabled()){
                if (Integer.parseInt(edt.getText().toString())<=0){
                    ToastCustom.message(getApplicationContext(), "Khối lượng không thể nhở hơn 0!", Color.RED);
                    flag=false;
                }
            }
        }

        if (flag==true){
            final QAM_MAC mac=new QAM_MAC();
//            mac.setMAC_MTR1(getSpn(sp_raw1));
//            mac.setMAC_MTR2(getSpn(sp_raw2));
//            mac.setMAC_MTR3(getSpn(sp_raw3));
//            mac.setMAC_MTR4(getSpn(sp_raw4));
//            mac.setMAC_MTR5(getSpn(sp_raw5));
//            mac.setMAC_MTR6(getSpn(sp_raw6));
//            mac.setMAC_MTR7(getSpn(sp_raw7));
//            mac.setMAC_MTR8(getSpn(sp_raw8));
//            mac.setMAC_MTR9(getSpn(sp_raw9));
//            mac.setMAC_MTR10(getSpn(sp_raw10));
//
//            mac.setMAC_CHEM1(getSpn(sp_chem1));
//            mac.setMAC_CHEM2(getSpn(sp_chem2));
//            mac.setMAC_CHEM3(getSpn(sp_chem3));
//            mac.setMAC_CHEM4(getSpn(sp_chem4));
//            mac.setMAC_CHEM5(getSpn(sp_chem5));
//            mac.setMAC_CHEM6(getSpn(sp_chem6));
//            mac.setMAC_CHEM7(getSpn(sp_chem7));
//            mac.setMAC_CHEM8(getSpn(sp_chem8));
//            mac.setMAC_CHEM9(getSpn(sp_chem9));
//            mac.setMAC_CHEM10(getSpn(sp_chem10));
//            mac.setMAC_CHEM11(getSpn(sp_chem11));
//            mac.setMAC_CHEM12(getSpn(sp_chem12));
//            mac.setMAC_CHEM13(getSpn(sp_chem13));
//            mac.setMAC_CHEM14(getSpn(sp_chem14));
//            mac.setMAC_CHEM15(getSpn(sp_chem15));
//            mac.setMAC_CHEM16(getSpn(sp_chem16));
//            mac.setMAC_CHEM17(getSpn(sp_chem17));
//            mac.setMAC_CHEM18(getSpn(sp_chem18));
//            mac.setMAC_CHEM19(getSpn(sp_chem19));
//            mac.setMAC_CHEM20(getSpn(sp_chem20));
//
//            mac.setMAC_PIG1(getSpn(sp_pig1));
//            mac.setMAC_PIG2(getSpn(sp_pig2));
//            mac.setMAC_PIG3(getSpn(sp_pig3));
//            mac.setMAC_PIG4(getSpn(sp_pig4));
//            mac.setMAC_PIG5(getSpn(sp_pig5));
//            mac.setMAC_PIG6(getSpn(sp_pig6));
//            mac.setMAC_PIG7(getSpn(sp_pig7));
//            mac.setMAC_PIG8(getSpn(sp_pig8));
//            mac.setMAC_PIG9(getSpn(sp_pig9));
//            mac.setMAC_PIG10(getSpn(sp_pig10));
            mac.setUP_USER(user);

            mac.setSMAC_MTR1(getEdt(edt_raw1).equals(MAC_CUR.getSMAC_MTR1()) == true ? "" : getEdt(edt_raw1));
            mac.setSMAC_MTR2(getEdt(edt_raw2).equals(MAC_CUR.getSMAC_MTR2()) == true ? "" : getEdt(edt_raw1));
            mac.setSMAC_MTR3(getEdt(edt_raw3).equals(MAC_CUR.getSMAC_MTR3()) == true ? "" : getEdt(edt_raw3));
            mac.setSMAC_MTR4(getEdt(edt_raw4).equals(MAC_CUR.getSMAC_MTR4()) == true ? "" : getEdt(edt_raw4));
            mac.setSMAC_MTR5(getEdt(edt_raw5).equals(MAC_CUR.getSMAC_MTR5()) == true ? "" : getEdt(edt_raw5));
            mac.setSMAC_MTR6(getEdt(edt_raw6).equals(MAC_CUR.getSMAC_MTR6()) == true ? "" : getEdt(edt_raw6));
            mac.setSMAC_MTR7(getEdt(edt_raw7).equals(MAC_CUR.getSMAC_MTR7()) == true ? "" : getEdt(edt_raw7));
            mac.setSMAC_MTR8(getEdt(edt_raw8).equals(MAC_CUR.getSMAC_MTR8()) == true ? "" : getEdt(edt_raw8));
            mac.setSMAC_MTR9(getEdt(edt_raw9).equals(MAC_CUR.getSMAC_MTR9()) == true ? "" : getEdt(edt_raw9));
            mac.setSMAC_MTR10(getEdt(edt_raw10).equals(MAC_CUR.getSMAC_MTR10()) == true ? "" : getEdt(edt_raw10));

            mac.setSMAC_CHEM1(getEdt(edt_chem1).equals(MAC_CUR.getSMAC_CHEM1()) == true ? "" :  getEdt(edt_chem1));
            mac.setSMAC_CHEM2(getEdt(edt_chem2).equals(MAC_CUR.getSMAC_CHEM2()) == true ? "" :  getEdt(edt_chem2));
            mac.setSMAC_CHEM3(getEdt(edt_chem3).equals(MAC_CUR.getSMAC_CHEM3()) == true ? "" :  getEdt(edt_chem3));
            mac.setSMAC_CHEM4(getEdt(edt_chem4).equals(MAC_CUR.getSMAC_CHEM4()) == true ? "" :  getEdt(edt_chem4));
            mac.setSMAC_CHEM5(getEdt(edt_chem5).equals(MAC_CUR.getSMAC_CHEM5()) == true ? "" :  getEdt(edt_chem5));
            mac.setSMAC_CHEM6(getEdt(edt_chem6).equals(MAC_CUR.getSMAC_CHEM6()) == true ? "" :  getEdt(edt_chem6));
            mac.setSMAC_CHEM7(getEdt(edt_chem7).equals(MAC_CUR.getSMAC_CHEM7()) == true ? "" :  getEdt(edt_chem7));
            mac.setSMAC_CHEM8(getEdt(edt_chem8).equals(MAC_CUR.getSMAC_CHEM8()) == true ? "" :  getEdt(edt_chem8));
            mac.setSMAC_CHEM9(getEdt(edt_chem9).equals(MAC_CUR.getSMAC_CHEM9()) == true ? "" :  getEdt(edt_chem9));
            mac.setSMAC_CHEM10(getEdt(edt_chem10).equals(MAC_CUR.getSMAC_CHEM10()) == true ? "" :  getEdt(edt_chem10));
            mac.setSMAC_CHEM11(getEdt(edt_chem11).equals(MAC_CUR.getSMAC_CHEM11()) == true ? "" :  getEdt(edt_chem11));
            mac.setSMAC_CHEM12(getEdt(edt_chem12).equals(MAC_CUR.getSMAC_CHEM12()) == true ? "" :  getEdt(edt_chem12));
            mac.setSMAC_CHEM13(getEdt(edt_chem13).equals(MAC_CUR.getSMAC_CHEM13()) == true ? "" :  getEdt(edt_chem13));
            mac.setSMAC_CHEM14(getEdt(edt_chem14).equals(MAC_CUR.getSMAC_CHEM14()) == true ? "" :  getEdt(edt_chem14));
            mac.setSMAC_CHEM15(getEdt(edt_chem15).equals(MAC_CUR.getSMAC_CHEM15()) == true ? "" :  getEdt(edt_chem15));
            mac.setSMAC_CHEM16(getEdt(edt_chem16).equals(MAC_CUR.getSMAC_CHEM16()) == true ? "" :  getEdt(edt_chem16));
            mac.setSMAC_CHEM17(getEdt(edt_chem17).equals(MAC_CUR.getSMAC_CHEM17()) == true ? "" :  getEdt(edt_chem17));
            mac.setSMAC_CHEM18(getEdt(edt_chem18).equals(MAC_CUR.getSMAC_CHEM18()) == true ? "" :  getEdt(edt_chem18));
            mac.setSMAC_CHEM19(getEdt(edt_chem19).equals(MAC_CUR.getSMAC_CHEM19()) == true ? "" :  getEdt(edt_chem19));
            mac.setSMAC_CHEM20(getEdt(edt_chem20).equals(MAC_CUR.getSMAC_CHEM20()) == true ? "" :  getEdt(edt_chem20));

            mac.setSMAC_PIG1(getEdt(edt_pig1).equals(MAC_CUR.getSMAC_PIG1()) == true ? "" :  getEdt(edt_pig1));
            mac.setSMAC_PIG2(getEdt(edt_pig2).equals(MAC_CUR.getSMAC_PIG2()) == true ? "" :  getEdt(edt_pig2));
            mac.setSMAC_PIG3(getEdt(edt_pig3).equals(MAC_CUR.getSMAC_PIG3()) == true ? "" :  getEdt(edt_pig3));
            mac.setSMAC_PIG4(getEdt(edt_pig4).equals(MAC_CUR.getSMAC_PIG4()) == true ? "" :  getEdt(edt_pig4));
            mac.setSMAC_PIG5(getEdt(edt_pig5).equals(MAC_CUR.getSMAC_PIG5()) == true ? "" :  getEdt(edt_pig5));
            mac.setSMAC_PIG6(getEdt(edt_pig6).equals(MAC_CUR.getSMAC_PIG6()) == true ? "" :  getEdt(edt_pig6));
            mac.setSMAC_PIG7(getEdt(edt_pig7).equals(MAC_CUR.getSMAC_PIG7()) == true ? "" :  getEdt(edt_pig7));
            mac.setSMAC_PIG8(getEdt(edt_pig8).equals(MAC_CUR.getSMAC_PIG8()) == true ? "" :  getEdt(edt_pig8));
            mac.setSMAC_PIG9(getEdt(edt_pig9).equals(MAC_CUR.getSMAC_PIG9()) == true ? "" :  getEdt(edt_pig9));
            mac.setSMAC_PIG10(getEdt(edt_pig10).equals(MAC_CUR.getSMAC_PIG10()) == true ? "" :  getEdt(edt_pig10));

            try {
                CallApi.insertMAC3(factory, mac, ins_no, spr_shift.getSelectedItem().toString(), edt_date.getText().toString(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        btn_edit.setEnabled(false);
                        btn_save.setEnabled(false);
                        btn_cancel.setEnabled(false);
                        btn_step1.setEnabled(true);
                        btn_step2.setEnabled(true);
                        btn_step3.setEnabled(true);
                        btn_part1.setEnabled(true);
                        btn_part4.setEnabled(true);

                        edt_date.setEnabled(true);
                        spr_shift.setEnabled(true);
                        spr_groupid.setEnabled(true);
                        ToastCustom.message(getApplicationContext(), "Lưu dữ liệu thành công", Color.GREEN);
                        setDisable();
//                        getMac(ins_no, spr_shift.getSelectedItem().toString(), edt_date.getText().toString());
                        creMode = false;
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void getsum(String factory, String ins_no) {//String date, String ca, String may
        try {
            CallApi.getsum(factory, ins_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    QSM1 qsm1 = gson.fromJson(response.toString(),  QSM1.class);
                    list_mac1 = qsm1.getItems() == null ? new ArrayList<QAM_MAC>() : qsm1.getItems();
                    if(list_mac1.size() != 0){

                        tv_rawtong1.setText(list_mac1.get(0).getMAC_MTR11());
                        tv_rawtong2.setText(list_mac1.get(0).getMAC_MTR12());
                        tv_rawtongtay.setText(list_mac1.get(0).getMAC_MTR13());
                        tv_may1.setText(list_mac1.get(0).getMAC_MTR14());

                        tv_chemtong1.setText(list_mac1.get(0).getMAC_MTR15());
                        tv_chemtong2.setText(list_mac1.get(0).getMAC_MTR16());
                        tv_chemtongtay.setText(list_mac1.get(0).getMAC_MTR17());
                        tv_may2.setText(list_mac1.get(0).getMAC_MTR18());

                        tv_pigtong1.setText(list_mac1.get(0).getMAC_MTR19());
                        tv_pigtong2.setText(list_mac1.get(0).getMAC_MTR20());
                        tv_pigtongtay.setText(list_mac1.get(0).getMAC_MTR21());
                        tv_pigtong.setText(list_mac1.get(0).getMAC_MTR22());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setDisable(){
        spr_groupid.setEnabled(false);
        edt_style.setEnabled(false);
        edt_color.setEnabled(false);
        edt_hard.setEnabled(false);

        for (Spinner sp:spnl){
            sp.setEnabled(false);
        }
        for (EditText edt: edtl){
            edt.setEnabled(false);
        }
    }

    private void setEnable(){
        for (Spinner sp:spnl){
            sp.setEnabled(true);
        }
    }

    private void fintViewbyId(){
        btn_edit=(Button) findViewById(R.id.btn_edit);
        btn_save=(Button) findViewById(R.id.btn_save);
        btn_cancel=(Button) findViewById(R.id.btn_cancel);
        btn_new=(Button) findViewById(R.id.btn_new);
        btn_step1=(Button) findViewById(R.id.btn_step1);
        btn_step2=(Button) findViewById(R.id.btn_step2);
        btn_step3=(Button) findViewById(R.id.btn_step3);
        btn_step4=(Button) findViewById(R.id.btn_step4);
        btn_part1=findViewById(R.id.btn_part1);
        btn_part4=findViewById(R.id.btn_part4);

        sp_raw1=findViewById(R.id.sp_raw1);
        sp_raw2=findViewById(R.id.sp_raw2);
        sp_raw3=findViewById(R.id.sp_raw3);
        sp_raw4=findViewById(R.id.sp_raw4);
        sp_raw5=findViewById(R.id.sp_raw5);
        sp_raw6=findViewById(R.id.sp_raw6);
        sp_raw7=findViewById(R.id.sp_raw7);
        sp_raw8=findViewById(R.id.sp_raw8);
        sp_raw9=findViewById(R.id.sp_raw9);
        sp_raw10=findViewById(R.id.sp_raw10);

        sp_chem1=findViewById(R.id.sp_chem1);
        sp_chem2=findViewById(R.id.sp_chem2);
        sp_chem3=findViewById(R.id.sp_chem3);
        sp_chem4=findViewById(R.id.sp_chem4);
        sp_chem5=findViewById(R.id.sp_chem5);
        sp_chem6=findViewById(R.id.sp_chem6);
        sp_chem7=findViewById(R.id.sp_chem7);
        sp_chem8=findViewById(R.id.sp_chem8);
        sp_chem9=findViewById(R.id.sp_chem9);
        sp_chem10=findViewById(R.id.sp_chem10);
        sp_chem11=findViewById(R.id.sp_chem11);
        sp_chem12=findViewById(R.id.sp_chem12);
        sp_chem13=findViewById(R.id.sp_chem13);
        sp_chem14=findViewById(R.id.sp_chem14);
        sp_chem15=findViewById(R.id.sp_chem15);
        sp_chem16=findViewById(R.id.sp_chem16);
        sp_chem17=findViewById(R.id.sp_chem17);
        sp_chem18=findViewById(R.id.sp_chem18);
        sp_chem19=findViewById(R.id.sp_chem19);
        sp_chem20=findViewById(R.id.sp_chem20);

        sp_pig1=findViewById(R.id.sp_pig1);
        sp_pig2=findViewById(R.id.sp_pig2);
        sp_pig3=findViewById(R.id.sp_pig3);
        sp_pig4=findViewById(R.id.sp_pig4);
        sp_pig5=findViewById(R.id.sp_pig5);
        sp_pig6=findViewById(R.id.sp_pig6);
        sp_pig7=findViewById(R.id.sp_pig7);
        sp_pig8=findViewById(R.id.sp_pig8);
        sp_pig9=findViewById(R.id.sp_pig9);
        sp_pig10=findViewById(R.id.sp_pig10);

        edt_raw1=findViewById(R.id.edt_raw1);
        edt_raw2=findViewById(R.id.edt_raw2);
        edt_raw3=findViewById(R.id.edt_raw3);
        edt_raw4=findViewById(R.id.edt_raw4);
        edt_raw5=findViewById(R.id.edt_raw5);
        edt_raw6=findViewById(R.id.edt_raw6);
        edt_raw7=findViewById(R.id.edt_raw7);
        edt_raw8=findViewById(R.id.edt_raw8);
        edt_raw9=findViewById(R.id.edt_raw9);
        edt_raw10=findViewById(R.id.edt_raw10);

        edt_chem1=findViewById(R.id.edt_chem1);
        edt_chem2=findViewById(R.id.edt_chem2);
        edt_chem3=findViewById(R.id.edt_chem3);
        edt_chem4=findViewById(R.id.edt_chem4);
        edt_chem5=findViewById(R.id.edt_chem5);
        edt_chem6=findViewById(R.id.edt_chem6);
        edt_chem7=findViewById(R.id.edt_chem7);
        edt_chem8=findViewById(R.id.edt_chem8);
        edt_chem9=findViewById(R.id.edt_chem9);
        edt_chem10=findViewById(R.id.edt_chem10);
        edt_chem11=findViewById(R.id.edt_chem11);
        edt_chem12=findViewById(R.id.edt_chem12);
        edt_chem13=findViewById(R.id.edt_chem13);
        edt_chem14=findViewById(R.id.edt_chem14);
        edt_chem15=findViewById(R.id.edt_chem15);
        edt_chem16=findViewById(R.id.edt_chem16);
        edt_chem17=findViewById(R.id.edt_chem17);
        edt_chem18=findViewById(R.id.edt_chem18);
        edt_chem19=findViewById(R.id.edt_chem19);
        edt_chem20=findViewById(R.id.edt_chem20);

        edt_pig1=findViewById(R.id.edt_pig1);
        edt_pig2=findViewById(R.id.edt_pig2);
        edt_pig3=findViewById(R.id.edt_pig3);
        edt_pig4=findViewById(R.id.edt_pig4);
        edt_pig5=findViewById(R.id.edt_pig5);
        edt_pig6=findViewById(R.id.edt_pig6);
        edt_pig7=findViewById(R.id.edt_pig7);
        edt_pig8=findViewById(R.id.edt_pig8);
        edt_pig9=findViewById(R.id.edt_pig9);
        edt_pig10=findViewById(R.id.edt_pig10);
    }

    public void defaultSpinner() {

        arrGroupID = new ArrayList<>();
        InsAdapter = new ArrayAdapter(Mac3Activity_FVI.this, R.layout.spn_layout, arrGroupID);
        spr_groupid.setAdapter(InsAdapter);

        listShift = new ArrayList<>();
        listShift.add("1");
        listShift.add("2");
        listShift.add("3");
        ShiftAdapter = new ArrayAdapter(Mac3Activity_FVI.this, R.layout.spn_layout, listShift);
        spr_shift.setAdapter(ShiftAdapter);

    }

    public void clear(){

        for (int i=0;i<spnl.length;i++){
            spnl[i].setSelection(0);
        }

        for(int i=0;i<edtl.length;i++){
            edtl[i].setText("");
        }
        tv_mcs.setText("");
    }

    public void datePickerDialog(final EditText edt_date) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(Mac3Activity_FVI.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int monthcurrent=monthOfYear + 1;
                        String month="";
                        String day="";
                        if(dayOfMonth>9) {
                            day=String.valueOf(dayOfMonth);

                        }
                        else
                        {
                            day="0"+String.valueOf(dayOfMonth);
                        }
                        if(monthcurrent>9)
                        {
                            month=String.valueOf(monthcurrent);
                        }
                        else
                        {
                            month="0"+String.valueOf(monthcurrent);

                        }
                        edt_date.setText(month + "/" + day + "/" + year);

                        spr_groupid.setEnabled(true);
                        getGROUP_ID(edt_date.getText().toString(), spr_shift.getSelectedItem().toString());

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void getGROUP_ID(String mcs_date, String mcs_shift) {
        try {
            CallApi.getGroupID(factory,mcs_shift, mcs_date, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    InsArm insArm = gson.fromJson(response.toString(), InsArm.class);
                    insl = insArm.getItems() == null ? new ArrayList<QAM_INS>() : insArm.getItems();
                    arrINS = new ArrayList<>();
                    arrGroupID = new ArrayList<>();
                    if (insl.size()>0){
                        for (QAM_INS ob:insl) {
                            arrINS.add(ob);
                            arrGroupID.add(ob.getINS_NO());
                        }
                        ArrayAdapter<String> adapter =
                                new ArrayAdapter<String>(getApplicationContext(), R.layout.spn_layout, arrGroupID);
                        adapter.setDropDownViewResource(R.layout.spn_layout);
                        spr_groupid.setAdapter(adapter);

                    }else {
                        btn_edit.setEnabled(false);
                        spr_groupid.setAdapter(InsAdapter);
                        tv_mcs.setText("");
                        edt_style.setText("");
                        edt_color.setText("");
                        edt_hard.setText("");
                        clear();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastCustom.message(getApplicationContext(), error.toString(), Color.RED);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void enbEdittext(){
        for (int i=0;i<spnl.length;i++){
            if (spnl[i].getSelectedItemPosition()!=0){
                edtl[i].setEnabled(true);
            }
        }
    }

    private void setEnableTextview(final Spinner spr,final EditText tv){
        spr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (creMode==true){
                    if(position!=0){
                        tv.setEnabled(true);
                    }else{
                        tv.setText(null);
                        tv.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    //fix height list view
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height=0;
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            view.measure(0, 0);
            totalHeight += view.getMeasuredHeight()+10;
        }
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public class  QSM1 extends ApiReturnModel<ArrayList<QAM_MAC>> {}

    public class InsArm extends ApiReturnModel<ArrayList<QAM_INS>> {}

    public class McsArm extends ApiReturnModel<ArrayList<MCS>> {
    }
    public class MacArm extends ApiReturnModel<ArrayList<QAM_MAC>> {
    }
    public class RawArm extends ApiReturnModel<ArrayList<String>> {
    }
    public class ChemArm extends ApiReturnModel<ArrayList<String>> {
    }
    public class PigArm extends ApiReturnModel<ArrayList<String>> {
    }
    public class CheckArm extends ApiReturnModel<String> {
    }
}
