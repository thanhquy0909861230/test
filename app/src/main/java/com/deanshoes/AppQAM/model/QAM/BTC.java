package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class BTC implements Serializable {
    private static final long serialVersionUID = 1L;

    private String KN_NO;
    private String KN_TEMP_F;
    private String KN_TIME_F;
    private String KN_TEMP_1;
    private String KN_TIME_1;
    private String KN_TEMP_2;
    private String KN_TIME_2;
    private String KN_TEMP_3;
    private String KN_TIME_3;
    private String KN_TEMP_4;
    private String KN_TIME_4;
    private String KN_TEMP_END;
    private String KN_TIME_END;
    private String KN_NOTE;

    private String BTC_NO;
    private String BTC_STYLE;
    private String BTC_COLOR;
    private String BTC_HARD;
    private String BTC_BATCH_NO;

    private String UP_USER;

    private String KN_TEMP_F_NOTE;
    private String KN_TEMP_1_NOTE;
    private String KN_TEMP_2_NOTE;
    private String KN_TEMP_3_NOTE;
    private String KN_TEMP_4_NOTE;
    private String KN_TEMP_END_NOTE;

    private String KN_TEMP_F_RENOTE;
    private String KN_TEMP_1_RENOTE;
    private String KN_TEMP_2_RENOTE;
    private String KN_TEMP_3_RENOTE;
    private String KN_TEMP_4_RENOTE;
    private String KN_TEMP_END_RENOTE;

    private String KN_FSTD;
    private String KN_1STD;
    private String KN_2STD;
    private String KN_3STD;
    private String KN_4STD;
    private String KN_ESTD;

    private String AMT_NOTE;

    public String getAMT_NOTE() {
        return AMT_NOTE;
    }

    public void setAMT_NOTE(String AMT_NOTE) {
        this.AMT_NOTE = AMT_NOTE;
    }

    public String getKN_TEMP_F_NOTE() {
        return KN_TEMP_F_NOTE;
    }
    public void setKN_TEMP_F_NOTE(String kN_TEMP_F_NOTE) {
        KN_TEMP_F_NOTE = kN_TEMP_F_NOTE;
    }
    public String getKN_TEMP_1_NOTE() {
        return KN_TEMP_1_NOTE;
    }
    public void setKN_TEMP_1_NOTE(String kN_TEMP_1_NOTE) {
        KN_TEMP_1_NOTE = kN_TEMP_1_NOTE;
    }
    public String getKN_TEMP_2_NOTE() {
        return KN_TEMP_2_NOTE;
    }
    public void setKN_TEMP_2_NOTE(String kN_TEMP_2_NOTE) {
        KN_TEMP_2_NOTE = kN_TEMP_2_NOTE;
    }
    public String getKN_TEMP_3_NOTE() {
        return KN_TEMP_3_NOTE;
    }
    public void setKN_TEMP_3_NOTE(String kN_TEMP_3_NOTE) {
        KN_TEMP_3_NOTE = kN_TEMP_3_NOTE;
    }
    public String getKN_TEMP_4_NOTE() {
        return KN_TEMP_4_NOTE;
    }
    public void setKN_TEMP_4_NOTE(String kN_TEMP_4_NOTE) {
        KN_TEMP_4_NOTE = kN_TEMP_4_NOTE;
    }
    public String getKN_TEMP_END_NOTE() {
        return KN_TEMP_END_NOTE;
    }
    public void setKN_TEMP_END_NOTE(String kN_TEMP_END_NOTE) {
        KN_TEMP_END_NOTE = kN_TEMP_END_NOTE;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getKN_TEMP_F_RENOTE() {
        return KN_TEMP_F_RENOTE;
    }

    public void setKN_TEMP_F_RENOTE(String KN_TEMP_F_RENOTE) {
        this.KN_TEMP_F_RENOTE = KN_TEMP_F_RENOTE;
    }

    public String getKN_TEMP_1_RENOTE() {
        return KN_TEMP_1_RENOTE;
    }

    public void setKN_TEMP_1_RENOTE(String KN_TEMP_1_RENOTE) {
        this.KN_TEMP_1_RENOTE = KN_TEMP_1_RENOTE;
    }

    public String getKN_TEMP_2_RENOTE() {
        return KN_TEMP_2_RENOTE;
    }

    public void setKN_TEMP_2_RENOTE(String KN_TEMP_2_RENOTE) {
        this.KN_TEMP_2_RENOTE = KN_TEMP_2_RENOTE;
    }

    public String getKN_TEMP_3_RENOTE() {
        return KN_TEMP_3_RENOTE;
    }

    public void setKN_TEMP_3_RENOTE(String KN_TEMP_3_RENOTE) {
        this.KN_TEMP_3_RENOTE = KN_TEMP_3_RENOTE;
    }

    public String getKN_TEMP_4_RENOTE() {
        return KN_TEMP_4_RENOTE;
    }

    public void setKN_TEMP_4_RENOTE(String KN_TEMP_4_RENOTE) {
        this.KN_TEMP_4_RENOTE = KN_TEMP_4_RENOTE;
    }

    public String getKN_TEMP_END_RENOTE() {
        return KN_TEMP_END_RENOTE;
    }

    public void setKN_TEMP_END_RENOTE(String KN_TEMP_END_RENOTE) {
        this.KN_TEMP_END_RENOTE = KN_TEMP_END_RENOTE;
    }

    public String getKN_FSTD() {
        return KN_FSTD;
    }

    public void setKN_FSTD(String KN_FSTD) {
        this.KN_FSTD = KN_FSTD;
    }

    public String getKN_1STD() {
        return KN_1STD;
    }

    public void setKN_1STD(String KN_1STD) {
        this.KN_1STD = KN_1STD;
    }

    public String getKN_2STD() {
        return KN_2STD;
    }

    public void setKN_2STD(String KN_2STD) {
        this.KN_2STD = KN_2STD;
    }

    public String getKN_3STD() {
        return KN_3STD;
    }

    public void setKN_3STD(String KN_3STD) {
        this.KN_3STD = KN_3STD;
    }

    public String getKN_4STD() {
        return KN_4STD;
    }

    public void setKN_4STD(String KN_4STD) {
        this.KN_4STD = KN_4STD;
    }

    public String getKN_ESTD() {
        return KN_ESTD;
    }

    public void setKN_ESTD(String KN_ESTD) {
        this.KN_ESTD = KN_ESTD;
    }

    public String getUP_USER() {
        return UP_USER;
    }
    public void setUP_USER(String uP_USER) {
        UP_USER = uP_USER;
    }
    public String getKN_TEMP_F() {
        return KN_TEMP_F;
    }
    public void setKN_TEMP_F(String kN_TEMP_F) {
        KN_TEMP_F = kN_TEMP_F;
    }
    public String getKN_TIME_F() {
        return KN_TIME_F;
    }
    public void setKN_TIME_F(String kN_TIME_F) {
        KN_TIME_F = kN_TIME_F;
    }
    public String getKN_TEMP_1() {
        return KN_TEMP_1;
    }
    public void setKN_TEMP_1(String kN_TEMP_1) {
        KN_TEMP_1 = kN_TEMP_1;
    }
    public String getKN_TIME_1() {
        return KN_TIME_1;
    }
    public void setKN_TIME_1(String kN_TIME_1) {
        KN_TIME_1 = kN_TIME_1;
    }
    public String getKN_TEMP_2() {
        return KN_TEMP_2;
    }
    public void setKN_TEMP_2(String kN_TEMP_2) {
        KN_TEMP_2 = kN_TEMP_2;
    }
    public String getKN_TIME_2() {
        return KN_TIME_2;
    }
    public void setKN_TIME_2(String kN_TIME_2) {
        KN_TIME_2 = kN_TIME_2;
    }
    public String getKN_TEMP_3() {
        return KN_TEMP_3;
    }
    public void setKN_TEMP_3(String kN_TEMP_3) {
        KN_TEMP_3 = kN_TEMP_3;
    }
    public String getKN_TIME_3() {
        return KN_TIME_3;
    }
    public void setKN_TIME_3(String kN_TIME_3) {
        KN_TIME_3 = kN_TIME_3;
    }
    public String getKN_TEMP_4() {
        return KN_TEMP_4;
    }
    public void setKN_TEMP_4(String kN_TEMP_4) {
        KN_TEMP_4 = kN_TEMP_4;
    }
    public String getKN_TIME_4() {
        return KN_TIME_4;
    }
    public void setKN_TIME_4(String kN_TIME_4) {
        KN_TIME_4 = kN_TIME_4;
    }
    public String getKN_TEMP_END() {
        return KN_TEMP_END;
    }
    public void setKN_TEMP_END(String kN_TEMP_END) {
        KN_TEMP_END = kN_TEMP_END;
    }
    public String getKN_TIME_END() {
        return KN_TIME_END;
    }
    public void setKN_TIME_END(String kN_TIME_END) {
        KN_TIME_END = kN_TIME_END;
    }
    public String getKN_NOTE() {
        return KN_NOTE;
    }
    public void setKN_NOTE(String kN_NOTE) {
        KN_NOTE = kN_NOTE;
    }
    public String getBTC_NO() {
        return BTC_NO;
    }
    public void setBTC_NO(String bTC_NO) {
        BTC_NO = bTC_NO;
    }
    public String getBTC_STYLE() {
        return BTC_STYLE;
    }
    public void setBTC_STYLE(String bTC_STYLE) {
        BTC_STYLE = bTC_STYLE;
    }
    public String getBTC_COLOR() {
        return BTC_COLOR;
    }
    public void setBTC_COLOR(String bTC_COLOR) {
        BTC_COLOR = bTC_COLOR;
    }
    public String getBTC_HARD() {
        return BTC_HARD;
    }
    public void setBTC_HARD(String bTC_HARD) {
        BTC_HARD = bTC_HARD;
    }

    public String getBTC_BATCH_NO() {
        return BTC_BATCH_NO;
    }
    public void setBTC_BATCH_NO(String bTC_BATCH_NO) {
        BTC_BATCH_NO = bTC_BATCH_NO;
    }
    public String getKN_NO() {
        return KN_NO;
    }
    public void setKN_NO(String kN_NO) {
        KN_NO = kN_NO;
    }


}