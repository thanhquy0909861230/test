package com.deanshoes.AppQAM.model.QAM;

public class EXCEL {
    private String LINE;
    private String MCS;
    private String SHIFT;
    private String DATE;
    private String STYLE;
    private String COLOR;
    private String HARD;

    private String[] MTR;
    private String[] MTR_W;

    private String[] CHEM;
    private String[] CHEM_W;

    private String[] PIG;
    private String[] PIG_W;

    public String getLINE() {
        return LINE;
    }

    public void setLINE(String LINE) {
        this.LINE = LINE;
    }

    public String getMCS() {
        return MCS;
    }

    public void setMCS(String MCS) {
        this.MCS = MCS;
    }

    public String getSHIFT() {
        return SHIFT;
    }

    public void setSHIFT(String SHIFT) {
        this.SHIFT = SHIFT;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getSTYLE() {
        return STYLE;
    }

    public void setSTYLE(String STYLE) {
        this.STYLE = STYLE;
    }

    public String getCOLOR() {
        return COLOR;
    }

    public void setCOLOR(String COLOR) {
        this.COLOR = COLOR;
    }

    public String getHARD() {
        return HARD;
    }

    public void setHARD(String HARD) {
        this.HARD = HARD;
    }

    public String[] getMTR() {
        return MTR;
    }

    public void setMTR(String[] MTR) {
        this.MTR = MTR;
    }

    public String[] getMTR_W() {
        return MTR_W;
    }

    public void setMTR_W(String[] MTR_W) {
        this.MTR_W = MTR_W;
    }

    public String[] getCHEM() {
        return CHEM;
    }

    public void setCHEM(String[] CHEM) {
        this.CHEM = CHEM;
    }

    public String[] getCHEM_W() {
        return CHEM_W;
    }

    public void setCHEM_W(String[] CHEM_W) {
        this.CHEM_W = CHEM_W;
    }

    public String[] getPIG() {
        return PIG;
    }

    public void setPIG(String[] PIG) {
        this.PIG = PIG;
    }

    public String[] getPIG_W() {
        return PIG_W;
    }

    public void setPIG_W(String[] PIG_W) {
        this.PIG_W = PIG_W;
    }
}
