package com.deanshoes.AppQAM.model.QAM;

public class QAM_MODEL {
    private String STYLE_NO;
    private String STYLE;
    private String COLOR;
    private String UP_DATE;
    private String UP_USER;
    public String getSTYLE() {
        return STYLE;
    }

    public void setSTYLE(String STYLE) {
        this.STYLE = STYLE;
    }

    public String getCOLOR() {
        return COLOR;
    }

    public void setCOLOR(String COLOR) {
        this.COLOR = COLOR;
    }

    public String getSTYLE_NO() {
        return STYLE_NO;
    }

    public void setSTYLE_NO(String STYLE_NO) {
        this.STYLE_NO = STYLE_NO;
    }

    public String getUP_DATE() {
        return UP_DATE;
    }

    public void setUP_DATE(String UP_DATE) {
        this.UP_DATE = UP_DATE;
    }

    public String getUP_USER() {
        return UP_USER;
    }

    public void setUP_USER(String UP_USER) {
        this.UP_USER = UP_USER;
    }
}
