package com.deanshoes.AppQAM.model.QAM;

public class OGD_FILE {
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    private String filename;
    private String filepath;
}
