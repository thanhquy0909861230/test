package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class BTC_OP implements Serializable {
    private static final long serialVersionUID = 1L;

    private String MCS;
    private String OP_NO;
    private String BTC_NO;
    private String OP_TEMP_1;
    private String OP_THICK_1;
    private String OP_TEMP_THIN;
    private String OP_THICK_THIN;
    private String OP_TEMP_2;
    private String OP_THICK_2;


    private String BTC_STYLE;
    private String BTC_COLOR;
    private String BTC_BATCH_NO;
    private String UP_USER;
    private String OP_TEMP_1_NOTE;
    private String OP_TEMP_THIN_NOTE;
    private String OP_TEMP_2_NOTE;

    private String OP_STD_TEMP_D;
    private String OP_STD_TEMP_L;
    private String OP_STD_THICK1_D;
    private String OP_STD_THICK1_L;
    private String OP_STD_THIN_D;
    private String OP_STD_THIN_L;
    private String OP_STD_THICK2_D;
    private String OP_STD_THICK2_L;
    private String OP_THICK1_NOTE;
    private String OP_THIN_NOTE;
    private String OP_THICK2_NOTE;

    private String AMT_NOTE;


    public String getMCS() {
        return MCS;
    }

    public void setMCS(String MCS) {
        this.MCS = MCS;
    }

    public String getOP_TEMP_1_NOTE() {
        return OP_TEMP_1_NOTE;
    }
    public void setOP_TEMP_1_NOTE(String oP_TEMP_1_NOTE) {
        OP_TEMP_1_NOTE = oP_TEMP_1_NOTE;
    }
    public String getOP_TEMP_THIN_NOTE() {
        return OP_TEMP_THIN_NOTE;
    }
    public void setOP_TEMP_THIN_NOTE(String oP_TEMP_THIN_NOTE) {
        OP_TEMP_THIN_NOTE = oP_TEMP_THIN_NOTE;
    }
    public String getOP_TEMP_2_NOTE() {
        return OP_TEMP_2_NOTE;
    }
    public void setOP_TEMP_2_NOTE(String oP_TEMP_2_NOTE) {
        OP_TEMP_2_NOTE = oP_TEMP_2_NOTE;
    }


    public String getUP_USER() {
        return UP_USER;
    }
    public void setUP_USER(String uP_USER) {
        UP_USER = uP_USER;
    }



    public String getOP_NO() {
        return OP_NO;
    }
    public void setOP_NO(String oP_NO) {
        OP_NO = oP_NO;
    }
    public String getBTC_NO() {
        return BTC_NO;
    }
    public void setBTC_NO(String bTC_NO) {
        BTC_NO = bTC_NO;
    }
    public String getOP_TEMP_1() {
        return OP_TEMP_1;
    }
    public void setOP_TEMP_1(String oP_TEMP_1) {
        OP_TEMP_1 = oP_TEMP_1;
    }
    public String getOP_THICK_1() {
        return OP_THICK_1;
    }
    public void setOP_THICK_1(String oP_THICK_1) {
        OP_THICK_1 = oP_THICK_1;
    }
    public String getOP_TEMP_THIN() {
        return OP_TEMP_THIN;
    }
    public void setOP_TEMP_THIN(String oP_TEMP_THIN) {
        OP_TEMP_THIN = oP_TEMP_THIN;
    }
    public String getOP_THICK_THIN() {
        return OP_THICK_THIN;
    }
    public void setOP_THICK_THIN(String oP_THICK_THIN) {
        OP_THICK_THIN = oP_THICK_THIN;
    }
    public String getOP_TEMP_2() {
        return OP_TEMP_2;
    }
    public void setOP_TEMP_2(String oP_TEMP_2) {
        OP_TEMP_2 = oP_TEMP_2;
    }
    public String getOP_THICK_2() {
        return OP_THICK_2;
    }
    public void setOP_THICK_2(String oP_THICK_2) {
        OP_THICK_2 = oP_THICK_2;
    }
    public String getBTC_STYLE() {
        return BTC_STYLE;
    }
    public void setBTC_STYLE(String bTC_STYLE) {
        BTC_STYLE = bTC_STYLE;
    }
    public String getBTC_COLOR() {
        return BTC_COLOR;
    }
    public void setBTC_COLOR(String bTC_COLOR) {
        BTC_COLOR = bTC_COLOR;
    }


    public String getBTC_BATCH_NO() {
        return BTC_BATCH_NO;
    }
    public void setBTC_BATCH_NO(String bTC_BATCH_NO) {
        BTC_BATCH_NO = bTC_BATCH_NO;
    }

    public String getOP_STD_TEMP_D() {
        return OP_STD_TEMP_D;
    }

    public void setOP_STD_TEMP_D(String OP_STD_TEMP_D) {
        this.OP_STD_TEMP_D = OP_STD_TEMP_D;
    }

    public String getOP_STD_TEMP_L() {
        return OP_STD_TEMP_L;
    }

    public void setOP_STD_TEMP_L(String OP_STD_TEMP_L) {
        this.OP_STD_TEMP_L = OP_STD_TEMP_L;
    }

    public String getOP_STD_THICK1_D() {
        return OP_STD_THICK1_D;
    }

    public void setOP_STD_THICK1_D(String OP_STD_THICK1_D) {
        this.OP_STD_THICK1_D = OP_STD_THICK1_D;
    }

    public String getOP_STD_THICK1_L() {
        return OP_STD_THICK1_L;
    }

    public void setOP_STD_THICK1_L(String OP_STD_THICK1_L) {
        this.OP_STD_THICK1_L = OP_STD_THICK1_L;
    }

    public String getOP_STD_THIN_D() {
        return OP_STD_THIN_D;
    }

    public void setOP_STD_THIN_D(String OP_STD_THIN_D) {
        this.OP_STD_THIN_D = OP_STD_THIN_D;
    }

    public String getOP_STD_THIN_L() {
        return OP_STD_THIN_L;
    }

    public void setOP_STD_THIN_L(String OP_STD_THIN_L) {
        this.OP_STD_THIN_L = OP_STD_THIN_L;
    }

    public String getOP_STD_THICK2_D() {
        return OP_STD_THICK2_D;
    }

    public void setOP_STD_THICK2_D(String OP_STD_THICK2_D) {
        this.OP_STD_THICK2_D = OP_STD_THICK2_D;
    }

    public String getOP_STD_THICK2_L() {
        return OP_STD_THICK2_L;
    }

    public void setOP_STD_THICK2_L(String OP_STD_THICK2_L) {
        this.OP_STD_THICK2_L = OP_STD_THICK2_L;
    }

    public String getOP_THICK1_NOTE() {
        return OP_THICK1_NOTE;
    }

    public void setOP_THICK1_NOTE(String OP_THICK1_NOTE) {
        this.OP_THICK1_NOTE = OP_THICK1_NOTE;
    }

    public String getOP_THIN_NOTE() {
        return OP_THIN_NOTE;
    }

    public void setOP_THIN_NOTE(String OP_THIN_NOTE) {
        this.OP_THIN_NOTE = OP_THIN_NOTE;
    }

    public String getOP_THICK2_NOTE() {
        return OP_THICK2_NOTE;
    }

    public void setOP_THICK2_NOTE(String OP_THICK2_NOTE) {
        this.OP_THICK2_NOTE = OP_THICK2_NOTE;
    }

    public String getAMT_NOTE() {
        return AMT_NOTE;
    }

    public void setAMT_NOTE(String AMT_NOTE) {
        this.AMT_NOTE = AMT_NOTE;
    }
}
