package com.deanshoes.AppQAM.model.response;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

public class ToastCustom {
    public static void message(Context context,String mes,int color){
        Toast ToastMessage = Toast.makeText(context, mes, Toast.LENGTH_LONG);
        TextView v = (TextView) ToastMessage.getView().findViewById(android.R.id.message);
        v.setTextColor(color);
        ToastMessage.show();
    }
}
