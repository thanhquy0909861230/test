package com.deanshoes.AppQAM.model.response;

/**
 * 錯誤碼<br>
 * EX: ErrorCode.CODE_0 代表執行成功
 * @author bin.chang
 *
 */
public enum ErrorCode implements ErrorCodeToCode {

	/** 執行成功 */
	ERROR_CODE_SYSTEM_SUCCESS(0, "執行成功"), 
	/** 系統發生錯誤 */
	ERROR_CODE_SYSTEM_ERROR(-1, "系統發生錯誤"),
	/** 資料已存在 */
	ERROR_CODE_DATA_EXISTS(10, "資料已存在"),
	/** 新增失敗 */
	ERROR_CODE_CREATE_FAILED(11, "新增失敗"),
	/** 更新失敗 */
	ERROR_CODE_UPDATE_FAILED(12, "更新失敗"),
	/** 刪除失敗 */
	ERROR_CODE_DELETE_FAILED(13, "刪除失敗"),
	/** 資料不存在 */
	ERROR_CODE_DATA_NOT_EXISTS(14, "資料不存在"),
	/** 使用者不存在 */
	ERROR_CODE_USER_NOT_EXIST(50, "使用者不存在"),
	/** 密碼錯誤 */
	ERROR_CODE_WRONG_PASSWORD(51, "密碼錯誤"),
	/** 帳號已存在 */
	ERROR_CODE_ACCOUNT_EXISTS(52, "帳號已存在"),
	/** 角色編號已存在 */
	ERROR_CODE_ROLE_ID_EXISTS(53, "角色編號已存在"),
	/** App編號已存在 */
	ERROR_CODE_APP_ID_EXISTS(54, "App編號已存在"),
	/** App 子功能編號已存在 */
	ERROR_CODE_APP_SUB_ID_EXISTS(55, "App 子功能編號已存在");
	
	/** 內存碼 */
	private Integer code;
	/** 預設的錯誤簡要說明 */
	private String desc;
	
	/**
	 * 建構時一並設定內存碼
	 * @param code
	 */
	private ErrorCode(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	/**
	 * 轉換為內存碼
	 */
	@Override
	public Integer toCode() {
		// TODO Auto-generated method stub
		return code;
	}

	@Override
	public String toDesc() {
		// TODO Auto-generated method stub
		return desc;
	}
}
