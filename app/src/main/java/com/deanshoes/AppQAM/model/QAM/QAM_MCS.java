package com.deanshoes.AppQAM.model.QAM;

public class QAM_MCS {
    private static final long serialVersionUID = 1L;
    private String MCS_NO;
    private String MCS;
    private String UP_DATE;
    private String UP_USER;
    private String MCS_SHIFT;
    private String MCS_DATE;
    private String MCS_STYLE;
    private String MCS_COLOR;
    private String MCS_HARD;
    private String MCS_NAME;
    private String ER;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    public String getMCS_NO() {
        return MCS_NO;
    }

    public void setMCS_NO(String MCS_NO) {
        this.MCS_NO = MCS_NO;
    }

    public String getMCS() {
        return MCS;
    }

    public void setMCS(String MCS) {
        this.MCS = MCS;
    }

    public String getUP_DATE() {
        return UP_DATE;
    }

    public void setUP_DATE(String UP_DATE) {
        this.UP_DATE = UP_DATE;
    }

    public String getUP_USER() {
        return UP_USER;
    }

    public void setUP_USER(String UP_USER) {
        this.UP_USER = UP_USER;
    }

    public String getMCS_SHIFT() {
        return MCS_SHIFT;
    }

    public void setMCS_SHIFT(String MCS_SHIFT) {
        this.MCS_SHIFT = MCS_SHIFT;
    }

    public String getMCS_DATE() {
        return MCS_DATE;
    }

    public void setMCS_DATE(String MCS_DATE) {
        this.MCS_DATE = MCS_DATE;
    }

    public String getMCS_STYLE() {
        return MCS_STYLE;
    }

    public void setMCS_STYLE(String MCS_STYLE) {
        this.MCS_STYLE = MCS_STYLE;
    }

    public String getMCS_COLOR() {
        return MCS_COLOR;
    }

    public void setMCS_COLOR(String MCS_COLOR) {
        this.MCS_COLOR = MCS_COLOR;
    }

    public String getMCS_HARD() {
        return MCS_HARD;
    }

    public void setMCS_HARD(String MCS_HARD) {
        this.MCS_HARD = MCS_HARD;
    }

    public String getMCS_NAME() {
        return MCS_NAME;
    }

    public void setMCS_NAME(String MCS_NAME) {
        this.MCS_NAME = MCS_NAME;
    }

    public String getER() {
        return ER;
    }

    public void setER(String ER) {
        this.ER = ER;
    }
}
