package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class TEMP implements Serializable {
    private static final long serialVersionUID = 1L;

    private String EDTNDT1;
    private String EDTNDT2;
    private String EDTNDT3;
    private String EDTNDT4;
    private String EDTNDT5;
    private String EDTNDT6;
    private String EDTNDT7;
    private String EDTNDT8;
    private String EDTNDT9;
    private String EDTNDT10;
    private String EDTNDT11;
    private String EDTNDT12;
    private String EDTNDT13;
    private String EDTNDT14;
    private String EDTNDT15;
    private String EDTNDT16;

    private String EDTNDD1;
    private String EDTNDD2;
    private String EDTNDD3;
    private String EDTNDD4;
    private String EDTNDD5;
    private String EDTNDD6;
    private String EDTNDD7;
    private String EDTNDD8;
    private String EDTNDD9;
    private String EDTNDD10;
    private String EDTNDD11;
    private String EDTNDD12;
    private String EDTNDD13;
    private String EDTNDD14;
    private String EDTNDD15;
    private String EDTNDD16;
    private String ID;

    private String NDTCT;
    private String NDTCP;

    public String getNDTCT() {
        return NDTCT;
    }

    public void setNDTCT(String NDTCT) {
        this.NDTCT = NDTCT;
    }

    public String getNDTCP() {
        return NDTCP;
    }

    public void setNDTCP(String NDTCP) {
        this.NDTCP = NDTCP;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getEDTNDT1() {
        return EDTNDT1;
    }

    public void setEDTNDT1(String EDTNDT1) {
        this.EDTNDT1 = EDTNDT1;
    }

    public String getEDTNDT2() {
        return EDTNDT2;
    }

    public void setEDTNDT2(String EDTNDT2) {
        this.EDTNDT2 = EDTNDT2;
    }

    public String getEDTNDT3() {
        return EDTNDT3;
    }

    public void setEDTNDT3(String EDTNDT3) {
        this.EDTNDT3 = EDTNDT3;
    }

    public String getEDTNDT4() {
        return EDTNDT4;
    }

    public void setEDTNDT4(String EDTNDT4) {
        this.EDTNDT4 = EDTNDT4;
    }

    public String getEDTNDT5() {
        return EDTNDT5;
    }

    public void setEDTNDT5(String EDTNDT5) {
        this.EDTNDT5 = EDTNDT5;
    }

    public String getEDTNDT6() {
        return EDTNDT6;
    }

    public void setEDTNDT6(String EDTNDT6) {
        this.EDTNDT6 = EDTNDT6;
    }

    public String getEDTNDT7() {
        return EDTNDT7;
    }

    public void setEDTNDT7(String EDTNDT7) {
        this.EDTNDT7 = EDTNDT7;
    }

    public String getEDTNDT8() {
        return EDTNDT8;
    }

    public void setEDTNDT8(String EDTNDT8) {
        this.EDTNDT8 = EDTNDT8;
    }

    public String getEDTNDT9() {
        return EDTNDT9;
    }

    public void setEDTNDT9(String EDTNDT9) {
        this.EDTNDT9 = EDTNDT9;
    }

    public String getEDTNDT10() {
        return EDTNDT10;
    }

    public void setEDTNDT10(String EDTNDT10) {
        this.EDTNDT10 = EDTNDT10;
    }

    public String getEDTNDT11() {
        return EDTNDT11;
    }

    public void setEDTNDT11(String EDTNDT11) {
        this.EDTNDT11 = EDTNDT11;
    }

    public String getEDTNDT12() {
        return EDTNDT12;
    }

    public void setEDTNDT12(String EDTNDT12) {
        this.EDTNDT12 = EDTNDT12;
    }

    public String getEDTNDT13() {
        return EDTNDT13;
    }

    public void setEDTNDT13(String EDTNDT13) {
        this.EDTNDT13 = EDTNDT13;
    }

    public String getEDTNDT14() {
        return EDTNDT14;
    }

    public void setEDTNDT14(String EDTNDT14) {
        this.EDTNDT14 = EDTNDT14;
    }

    public String getEDTNDT15() {
        return EDTNDT15;
    }

    public void setEDTNDT15(String EDTNDT15) {
        this.EDTNDT15 = EDTNDT15;
    }

    public String getEDTNDT16() {
        return EDTNDT16;
    }

    public void setEDTNDT16(String EDTNDT16) {
        this.EDTNDT16 = EDTNDT16;
    }

    public String getEDTNDD1() {
        return EDTNDD1;
    }

    public void setEDTNDD1(String EDTNDD1) {
        this.EDTNDD1 = EDTNDD1;
    }

    public String getEDTNDD2() {
        return EDTNDD2;
    }

    public void setEDTNDD2(String EDTNDD2) {
        this.EDTNDD2 = EDTNDD2;
    }

    public String getEDTNDD3() {
        return EDTNDD3;
    }

    public void setEDTNDD3(String EDTNDD3) {
        this.EDTNDD3 = EDTNDD3;
    }

    public String getEDTNDD4() {
        return EDTNDD4;
    }

    public void setEDTNDD4(String EDTNDD4) {
        this.EDTNDD4 = EDTNDD4;
    }

    public String getEDTNDD5() {
        return EDTNDD5;
    }

    public void setEDTNDD5(String EDTNDD5) {
        this.EDTNDD5 = EDTNDD5;
    }

    public String getEDTNDD6() {
        return EDTNDD6;
    }

    public void setEDTNDD6(String EDTNDD6) {
        this.EDTNDD6 = EDTNDD6;
    }

    public String getEDTNDD7() {
        return EDTNDD7;
    }

    public void setEDTNDD7(String EDTNDD7) {
        this.EDTNDD7 = EDTNDD7;
    }

    public String getEDTNDD8() {
        return EDTNDD8;
    }

    public void setEDTNDD8(String EDTNDD8) {
        this.EDTNDD8 = EDTNDD8;
    }

    public String getEDTNDD9() {
        return EDTNDD9;
    }

    public void setEDTNDD9(String EDTNDD9) {
        this.EDTNDD9 = EDTNDD9;
    }

    public String getEDTNDD10() {
        return EDTNDD10;
    }

    public void setEDTNDD10(String EDTNDD10) {
        this.EDTNDD10 = EDTNDD10;
    }

    public String getEDTNDD11() {
        return EDTNDD11;
    }

    public void setEDTNDD11(String EDTNDD11) {
        this.EDTNDD11 = EDTNDD11;
    }

    public String getEDTNDD12() {
        return EDTNDD12;
    }

    public void setEDTNDD12(String EDTNDD12) {
        this.EDTNDD12 = EDTNDD12;
    }

    public String getEDTNDD13() {
        return EDTNDD13;
    }

    public void setEDTNDD13(String EDTNDD13) {
        this.EDTNDD13 = EDTNDD13;
    }

    public String getEDTNDD14() {
        return EDTNDD14;
    }

    public void setEDTNDD14(String EDTNDD14) {
        this.EDTNDD14 = EDTNDD14;
    }

    public String getEDTNDD15() {
        return EDTNDD15;
    }

    public void setEDTNDD15(String EDTNDD15) {
        this.EDTNDD15 = EDTNDD15;
    }

    public String getEDTNDD16() {
        return EDTNDD16;
    }

    public void setEDTNDD16(String EDTNDD16) {
        this.EDTNDD16 = EDTNDD16;
    }



}