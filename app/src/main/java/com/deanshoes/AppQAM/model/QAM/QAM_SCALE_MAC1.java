package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class QAM_SCALE_MAC1 implements Serializable {

    private String SC1_SHIFT;
    private String SC1_DATE;
    private String SC1_MCS;
    private String KEY;

    private String SC1_RAW1;
    private String SC1_RAW2;
    private String SC1_RAW3;
    private String SC1_RAW4;
    private String SC1_RAW5;
    private String SC1_RAW6;
    private String SC1_RAW7;
    private String SC1_RAW8;
    private String SC1_RAW9;
    private String SC1_RAW10;
    private String SC1_CHEM1;
    private String SC1_CHEM2;
    private String SC1_CHEM3;
    private String SC1_CHEM4;
    private String SC1_CHEM5;
    private String SC1_CHEM6;
    private String SC1_CHEM7;
    private String SC1_CHEM8;
    private String SC1_CHEM9;
    private String SC1_CHEM10;
    private String SC1_CHEM11;
    private String SC1_CHEM12;
    private String SC1_CHEM13;
    private String SC1_CHEM14;
    private String SC1_CHEM15;
    private String SC1_CHEM16;
    private String SC1_CHEM17;
    private String SC1_CHEM18;
    private String SC1_CHEM19;
    private String SC1_CHEM20;
    private String SC1_PIG1;
    private String SC1_PIG2;
    private String SC1_PIG3;
    private String SC1_PIG4;
    private String SC1_PIG5;
    private String SC1_PIG6;
    private String SC1_PIG7;
    private String SC1_PIG8;
    private String SC1_PIG9;
    private String SC1_PIG10;
    private String SC1_WRAW1;
    private String SC1_WRAW2;
    private String SC1_WRAW3;
    private String SC1_WRAW4;
    private String SC1_WRAW5;
    private String SC1_WRAW6;
    private String SC1_WRAW7;
    private String SC1_WRAW8;
    private String SC1_WRAW9;
    private String SC1_WRAW10;
    private String SC1_WCHEM1;
    private String SC1_WCHEM2;
    private String SC1_WCHEM3;
    private String SC1_WCHEM4;
    private String SC1_WCHEM5;
    private String SC1_WCHEM6;
    private String SC1_WCHEM7;
    private String SC1_WCHEM8;
    private String SC1_WCHEM9;
    private String SC1_WCHEM10;
    private String SC1_WCHEM11;
    private String SC1_WCHEM12;
    private String SC1_WCHEM13;
    private String SC1_WCHEM14;
    private String SC1_WCHEM15;
    private String SC1_WCHEM16;
    private String SC1_WCHEM17;
    private String SC1_WCHEM18;
    private String SC1_WCHEM19;
    private String SC1_WCHEM20;
    private String SC1_WPIG1;
    private String SC1_WPIG2;
    private String SC1_WPIG3;
    private String SC1_WPIG4;
    private String SC1_WPIG5;
    private String SC1_WPIG6;
    private String SC1_WPIG7;
    private String SC1_WPIG8;
    private String SC1_WPIG9;
    private String SC1_WPIG10;

    private String MAC1_ID;

    public String getKEY() {
        return KEY;
    }

    public void setKEY(String KEY) {
        this.KEY = KEY;
    }

    public String getSC1_SHIFT() {
        return SC1_SHIFT;
    }

    public void setSC1_SHIFT(String SC1_SHIFT) {
        this.SC1_SHIFT = SC1_SHIFT;
    }

    public String getSC1_DATE() {
        return SC1_DATE;
    }

    public void setSC1_DATE(String SC1_DATE) {
        this.SC1_DATE = SC1_DATE;
    }

    public String getSC1_MCS() {
        return SC1_MCS;
    }

    public void setSC1_MCS(String SC1_MCS) {
        this.SC1_MCS = SC1_MCS;
    }

    public String getSC1_RAW1() {
        return SC1_RAW1;
    }

    public void setSC1_RAW1(String SC1_RAW1) {
        this.SC1_RAW1 = SC1_RAW1;
    }

    public String getSC1_RAW2() {
        return SC1_RAW2;
    }

    public void setSC1_RAW2(String SC1_RAW2) {
        this.SC1_RAW2 = SC1_RAW2;
    }

    public String getSC1_RAW3() {
        return SC1_RAW3;
    }

    public void setSC1_RAW3(String SC1_RAW3) {
        this.SC1_RAW3 = SC1_RAW3;
    }

    public String getSC1_RAW4() {
        return SC1_RAW4;
    }

    public void setSC1_RAW4(String SC1_RAW4) {
        this.SC1_RAW4 = SC1_RAW4;
    }

    public String getSC1_RAW5() {
        return SC1_RAW5;
    }

    public void setSC1_RAW5(String SC1_RAW5) {
        this.SC1_RAW5 = SC1_RAW5;
    }

    public String getSC1_RAW6() {
        return SC1_RAW6;
    }

    public void setSC1_RAW6(String SC1_RAW6) {
        this.SC1_RAW6 = SC1_RAW6;
    }

    public String getSC1_RAW7() {
        return SC1_RAW7;
    }

    public void setSC1_RAW7(String SC1_RAW7) {
        this.SC1_RAW7 = SC1_RAW7;
    }

    public String getSC1_RAW8() {
        return SC1_RAW8;
    }

    public void setSC1_RAW8(String SC1_RAW8) {
        this.SC1_RAW8 = SC1_RAW8;
    }

    public String getSC1_RAW9() {
        return SC1_RAW9;
    }

    public void setSC1_RAW9(String SC1_RAW9) {
        this.SC1_RAW9 = SC1_RAW9;
    }

    public String getSC1_RAW10() {
        return SC1_RAW10;
    }

    public void setSC1_RAW10(String SC1_RAW10) {
        this.SC1_RAW10 = SC1_RAW10;
    }

    public String getSC1_CHEM1() {
        return SC1_CHEM1;
    }

    public void setSC1_CHEM1(String SC1_CHEM1) {
        this.SC1_CHEM1 = SC1_CHEM1;
    }

    public String getSC1_CHEM2() {
        return SC1_CHEM2;
    }

    public void setSC1_CHEM2(String SC1_CHEM2) {
        this.SC1_CHEM2 = SC1_CHEM2;
    }

    public String getSC1_CHEM3() {
        return SC1_CHEM3;
    }

    public void setSC1_CHEM3(String SC1_CHEM3) {
        this.SC1_CHEM3 = SC1_CHEM3;
    }

    public String getSC1_CHEM4() {
        return SC1_CHEM4;
    }

    public void setSC1_CHEM4(String SC1_CHEM4) {
        this.SC1_CHEM4 = SC1_CHEM4;
    }

    public String getSC1_CHEM5() {
        return SC1_CHEM5;
    }

    public void setSC1_CHEM5(String SC1_CHEM5) {
        this.SC1_CHEM5 = SC1_CHEM5;
    }

    public String getSC1_CHEM6() {
        return SC1_CHEM6;
    }

    public void setSC1_CHEM6(String SC1_CHEM6) {
        this.SC1_CHEM6 = SC1_CHEM6;
    }

    public String getSC1_CHEM7() {
        return SC1_CHEM7;
    }

    public void setSC1_CHEM7(String SC1_CHEM7) {
        this.SC1_CHEM7 = SC1_CHEM7;
    }

    public String getSC1_CHEM8() {
        return SC1_CHEM8;
    }

    public void setSC1_CHEM8(String SC1_CHEM8) {
        this.SC1_CHEM8 = SC1_CHEM8;
    }

    public String getSC1_CHEM9() {
        return SC1_CHEM9;
    }

    public void setSC1_CHEM9(String SC1_CHEM9) {
        this.SC1_CHEM9 = SC1_CHEM9;
    }

    public String getSC1_CHEM10() {
        return SC1_CHEM10;
    }

    public void setSC1_CHEM10(String SC1_CHEM10) {
        this.SC1_CHEM10 = SC1_CHEM10;
    }

    public String getSC1_CHEM11() {
        return SC1_CHEM11;
    }

    public void setSC1_CHEM11(String SC1_CHEM11) {
        this.SC1_CHEM11 = SC1_CHEM11;
    }

    public String getSC1_CHEM12() {
        return SC1_CHEM12;
    }

    public void setSC1_CHEM12(String SC1_CHEM12) {
        this.SC1_CHEM12 = SC1_CHEM12;
    }

    public String getSC1_CHEM13() {
        return SC1_CHEM13;
    }

    public void setSC1_CHEM13(String SC1_CHEM13) {
        this.SC1_CHEM13 = SC1_CHEM13;
    }

    public String getSC1_CHEM14() {
        return SC1_CHEM14;
    }

    public void setSC1_CHEM14(String SC1_CHEM14) {
        this.SC1_CHEM14 = SC1_CHEM14;
    }

    public String getSC1_CHEM15() {
        return SC1_CHEM15;
    }

    public void setSC1_CHEM15(String SC1_CHEM15) {
        this.SC1_CHEM15 = SC1_CHEM15;
    }

    public String getSC1_CHEM16() {
        return SC1_CHEM16;
    }

    public void setSC1_CHEM16(String SC1_CHEM16) {
        this.SC1_CHEM16 = SC1_CHEM16;
    }

    public String getSC1_CHEM17() {
        return SC1_CHEM17;
    }

    public void setSC1_CHEM17(String SC1_CHEM17) {
        this.SC1_CHEM17 = SC1_CHEM17;
    }

    public String getSC1_CHEM18() {
        return SC1_CHEM18;
    }

    public void setSC1_CHEM18(String SC1_CHEM18) {
        this.SC1_CHEM18 = SC1_CHEM18;
    }

    public String getSC1_CHEM19() {
        return SC1_CHEM19;
    }

    public void setSC1_CHEM19(String SC1_CHEM19) {
        this.SC1_CHEM19 = SC1_CHEM19;
    }

    public String getSC1_CHEM20() {
        return SC1_CHEM20;
    }

    public void setSC1_CHEM20(String SC1_CHEM20) {
        this.SC1_CHEM20 = SC1_CHEM20;
    }

    public String getSC1_PIG1() {
        return SC1_PIG1;
    }

    public void setSC1_PIG1(String SC1_PIG1) {
        this.SC1_PIG1 = SC1_PIG1;
    }

    public String getSC1_PIG2() {
        return SC1_PIG2;
    }

    public void setSC1_PIG2(String SC1_PIG2) {
        this.SC1_PIG2 = SC1_PIG2;
    }

    public String getSC1_PIG3() {
        return SC1_PIG3;
    }

    public void setSC1_PIG3(String SC1_PIG3) {
        this.SC1_PIG3 = SC1_PIG3;
    }

    public String getSC1_PIG4() {
        return SC1_PIG4;
    }

    public void setSC1_PIG4(String SC1_PIG4) {
        this.SC1_PIG4 = SC1_PIG4;
    }

    public String getSC1_PIG5() {
        return SC1_PIG5;
    }

    public void setSC1_PIG5(String SC1_PIG5) {
        this.SC1_PIG5 = SC1_PIG5;
    }

    public String getSC1_PIG6() {
        return SC1_PIG6;
    }

    public void setSC1_PIG6(String SC1_PIG6) {
        this.SC1_PIG6 = SC1_PIG6;
    }

    public String getSC1_PIG7() {
        return SC1_PIG7;
    }

    public void setSC1_PIG7(String SC1_PIG7) {
        this.SC1_PIG7 = SC1_PIG7;
    }

    public String getSC1_PIG8() {
        return SC1_PIG8;
    }

    public void setSC1_PIG8(String SC1_PIG8) {
        this.SC1_PIG8 = SC1_PIG8;
    }

    public String getSC1_PIG9() {
        return SC1_PIG9;
    }

    public void setSC1_PIG9(String SC1_PIG9) {
        this.SC1_PIG9 = SC1_PIG9;
    }

    public String getSC1_PIG10() {
        return SC1_PIG10;
    }

    public void setSC1_PIG10(String SC1_PIG10) {
        this.SC1_PIG10 = SC1_PIG10;
    }

    public String getSC1_WRAW1() {
        return SC1_WRAW1;
    }

    public void setSC1_WRAW1(String SC1_WRAW1) {
        this.SC1_WRAW1 = SC1_WRAW1;
    }

    public String getSC1_WRAW2() {
        return SC1_WRAW2;
    }

    public void setSC1_WRAW2(String SC1_WRAW2) {
        this.SC1_WRAW2 = SC1_WRAW2;
    }

    public String getSC1_WRAW3() {
        return SC1_WRAW3;
    }

    public void setSC1_WRAW3(String SC1_WRAW3) {
        this.SC1_WRAW3 = SC1_WRAW3;
    }

    public String getSC1_WRAW4() {
        return SC1_WRAW4;
    }

    public void setSC1_WRAW4(String SC1_WRAW4) {
        this.SC1_WRAW4 = SC1_WRAW4;
    }

    public String getSC1_WRAW5() {
        return SC1_WRAW5;
    }

    public void setSC1_WRAW5(String SC1_WRAW5) {
        this.SC1_WRAW5 = SC1_WRAW5;
    }

    public String getSC1_WRAW6() {
        return SC1_WRAW6;
    }

    public void setSC1_WRAW6(String SC1_WRAW6) {
        this.SC1_WRAW6 = SC1_WRAW6;
    }

    public String getSC1_WRAW7() {
        return SC1_WRAW7;
    }

    public void setSC1_WRAW7(String SC1_WRAW7) {
        this.SC1_WRAW7 = SC1_WRAW7;
    }

    public String getSC1_WRAW8() {
        return SC1_WRAW8;
    }

    public void setSC1_WRAW8(String SC1_WRAW8) {
        this.SC1_WRAW8 = SC1_WRAW8;
    }

    public String getSC1_WRAW9() {
        return SC1_WRAW9;
    }

    public void setSC1_WRAW9(String SC1_WRAW9) {
        this.SC1_WRAW9 = SC1_WRAW9;
    }

    public String getSC1_WRAW10() {
        return SC1_WRAW10;
    }

    public void setSC1_WRAW10(String SC1_WRAW10) {
        this.SC1_WRAW10 = SC1_WRAW10;
    }

    public String getSC1_WCHEM1() {
        return SC1_WCHEM1;
    }

    public void setSC1_WCHEM1(String SC1_WCHEM1) {
        this.SC1_WCHEM1 = SC1_WCHEM1;
    }

    public String getSC1_WCHEM2() {
        return SC1_WCHEM2;
    }

    public void setSC1_WCHEM2(String SC1_WCHEM2) {
        this.SC1_WCHEM2 = SC1_WCHEM2;
    }

    public String getSC1_WCHEM3() {
        return SC1_WCHEM3;
    }

    public void setSC1_WCHEM3(String SC1_WCHEM3) {
        this.SC1_WCHEM3 = SC1_WCHEM3;
    }

    public String getSC1_WCHEM4() {
        return SC1_WCHEM4;
    }

    public void setSC1_WCHEM4(String SC1_WCHEM4) {
        this.SC1_WCHEM4 = SC1_WCHEM4;
    }

    public String getSC1_WCHEM5() {
        return SC1_WCHEM5;
    }

    public void setSC1_WCHEM5(String SC1_WCHEM5) {
        this.SC1_WCHEM5 = SC1_WCHEM5;
    }

    public String getSC1_WCHEM6() {
        return SC1_WCHEM6;
    }

    public void setSC1_WCHEM6(String SC1_WCHEM6) {
        this.SC1_WCHEM6 = SC1_WCHEM6;
    }

    public String getSC1_WCHEM7() {
        return SC1_WCHEM7;
    }

    public void setSC1_WCHEM7(String SC1_WCHEM7) {
        this.SC1_WCHEM7 = SC1_WCHEM7;
    }

    public String getSC1_WCHEM8() {
        return SC1_WCHEM8;
    }

    public void setSC1_WCHEM8(String SC1_WCHEM8) {
        this.SC1_WCHEM8 = SC1_WCHEM8;
    }

    public String getSC1_WCHEM9() {
        return SC1_WCHEM9;
    }

    public void setSC1_WCHEM9(String SC1_WCHEM9) {
        this.SC1_WCHEM9 = SC1_WCHEM9;
    }

    public String getSC1_WCHEM10() {
        return SC1_WCHEM10;
    }

    public void setSC1_WCHEM10(String SC1_WCHEM10) {
        this.SC1_WCHEM10 = SC1_WCHEM10;
    }

    public String getSC1_WCHEM11() {
        return SC1_WCHEM11;
    }

    public void setSC1_WCHEM11(String SC1_WCHEM11) {
        this.SC1_WCHEM11 = SC1_WCHEM11;
    }

    public String getSC1_WCHEM12() {
        return SC1_WCHEM12;
    }

    public void setSC1_WCHEM12(String SC1_WCHEM12) {
        this.SC1_WCHEM12 = SC1_WCHEM12;
    }

    public String getSC1_WCHEM13() {
        return SC1_WCHEM13;
    }

    public void setSC1_WCHEM13(String SC1_WCHEM13) {
        this.SC1_WCHEM13 = SC1_WCHEM13;
    }

    public String getSC1_WCHEM14() {
        return SC1_WCHEM14;
    }

    public void setSC1_WCHEM14(String SC1_WCHEM14) {
        this.SC1_WCHEM14 = SC1_WCHEM14;
    }

    public String getSC1_WCHEM15() {
        return SC1_WCHEM15;
    }

    public void setSC1_WCHEM15(String SC1_WCHEM15) {
        this.SC1_WCHEM15 = SC1_WCHEM15;
    }

    public String getSC1_WCHEM16() {
        return SC1_WCHEM16;
    }

    public void setSC1_WCHEM16(String SC1_WCHEM16) {
        this.SC1_WCHEM16 = SC1_WCHEM16;
    }

    public String getSC1_WCHEM17() {
        return SC1_WCHEM17;
    }

    public void setSC1_WCHEM17(String SC1_WCHEM17) {
        this.SC1_WCHEM17 = SC1_WCHEM17;
    }

    public String getSC1_WCHEM18() {
        return SC1_WCHEM18;
    }

    public void setSC1_WCHEM18(String SC1_WCHEM18) {
        this.SC1_WCHEM18 = SC1_WCHEM18;
    }

    public String getSC1_WCHEM19() {
        return SC1_WCHEM19;
    }

    public void setSC1_WCHEM19(String SC1_WCHEM19) {
        this.SC1_WCHEM19 = SC1_WCHEM19;
    }

    public String getSC1_WCHEM20() {
        return SC1_WCHEM20;
    }

    public void setSC1_WCHEM20(String SC1_WCHEM20) {
        this.SC1_WCHEM20 = SC1_WCHEM20;
    }

    public String getSC1_WPIG1() {
        return SC1_WPIG1;
    }

    public void setSC1_WPIG1(String SC1_WPIG1) {
        this.SC1_WPIG1 = SC1_WPIG1;
    }

    public String getSC1_WPIG2() {
        return SC1_WPIG2;
    }

    public void setSC1_WPIG2(String SC1_WPIG2) {
        this.SC1_WPIG2 = SC1_WPIG2;
    }

    public String getSC1_WPIG3() {
        return SC1_WPIG3;
    }

    public void setSC1_WPIG3(String SC1_WPIG3) {
        this.SC1_WPIG3 = SC1_WPIG3;
    }

    public String getSC1_WPIG4() {
        return SC1_WPIG4;
    }

    public void setSC1_WPIG4(String SC1_WPIG4) {
        this.SC1_WPIG4 = SC1_WPIG4;
    }

    public String getSC1_WPIG5() {
        return SC1_WPIG5;
    }

    public void setSC1_WPIG5(String SC1_WPIG5) {
        this.SC1_WPIG5 = SC1_WPIG5;
    }

    public String getSC1_WPIG6() {
        return SC1_WPIG6;
    }

    public void setSC1_WPIG6(String SC1_WPIG6) {
        this.SC1_WPIG6 = SC1_WPIG6;
    }

    public String getSC1_WPIG7() {
        return SC1_WPIG7;
    }

    public void setSC1_WPIG7(String SC1_WPIG7) {
        this.SC1_WPIG7 = SC1_WPIG7;
    }

    public String getSC1_WPIG8() {
        return SC1_WPIG8;
    }

    public void setSC1_WPIG8(String SC1_WPIG8) {
        this.SC1_WPIG8 = SC1_WPIG8;
    }

    public String getSC1_WPIG9() {
        return SC1_WPIG9;
    }

    public void setSC1_WPIG9(String SC1_WPIG9) {
        this.SC1_WPIG9 = SC1_WPIG9;
    }

    public String getSC1_WPIG10() {
        return SC1_WPIG10;
    }

    public void setSC1_WPIG10(String SC1_WPIG10) {
        this.SC1_WPIG10 = SC1_WPIG10;
    }

    public String getMAC1_ID() {
        return MAC1_ID;
    }

    public void setMAC1_ID(String MAC1_ID) {
        this.MAC1_ID = MAC1_ID;
    }


}