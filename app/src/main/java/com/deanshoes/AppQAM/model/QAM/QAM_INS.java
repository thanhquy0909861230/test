package com.deanshoes.AppQAM.model.QAM;

public class QAM_INS {
    private String INS_NO;
    private String MCS_NO;
    private String MAC_NO;
    private String INS_SHIFT;
    private String INS_DATE;
    private String UP_DATE;
    private String UP_USER;
    private String NEW_BATCH_NO;
    private String INS_LINE;
    private String Info;

    public String getInfo() {
        return Info;
    }

    public void setInfo(String Info) {
        this.Info = Info;
    }



    public String getINS_NO() {
        return INS_NO;
    }

    public void setINS_NO(String INS_NO) {
        this.INS_NO = INS_NO;
    }

    public String getMCS_NO() {
        return MCS_NO;
    }

    public void setMCS_NO(String MCS_NO) {
        this.MCS_NO = MCS_NO;
    }

    public String getMAC_NO() {
        return MAC_NO;
    }

    public void setMAC_NO(String MAC_NO) {
        this.MAC_NO = MAC_NO;
    }

    public String getINS_SHIFT() {
        return INS_SHIFT;
    }

    public void setINS_SHIFT(String INS_SHIFT) {
        this.INS_SHIFT = INS_SHIFT;
    }

    public String getINS_DATE() {
        return INS_DATE;
    }

    public void setINS_DATE(String INS_DATE) {
        this.INS_DATE = INS_DATE;
    }

    public String getUP_DATE() {
        return UP_DATE;
    }

    public void setUP_DATE(String UP_DATE) {
        this.UP_DATE = UP_DATE;
    }

    public String getUP_USER() {
        return UP_USER;
    }

    public void setUP_USER(String UP_USER) {
        this.UP_USER = UP_USER;
    }

    public String getNEW_BATCH_NO() {
        return NEW_BATCH_NO;
    }

    public void setNEW_BATCH_NO(String NEW_BATCH_NO) {
        this.NEW_BATCH_NO = NEW_BATCH_NO;
    }

    public String getINS_LINE() {
        return INS_LINE;
    }

    public void setINS_LINE(String INS_LINE) {
        this.INS_LINE = INS_LINE;
    }


    @Override
    public String toString() {
        return
                this.INS_NO
                ;
    }

}
