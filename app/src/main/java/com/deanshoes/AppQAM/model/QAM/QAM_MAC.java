package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;
import java.util.Date;

public class QAM_MAC implements Serializable {
    private static final long serialVersionUID = 1L;
    private String MAC_MTR1;
    private String MAC_MTR2;
    private String MAC_MTR3;
    private String MAC_MTR4;
    private String MAC_MTR5;
    private String MAC_MTR6;
    private String MAC_MTR7;
    private String MAC_MTR8;
    private String MAC_MTR9;
    private String MAC_MTR10;

    public String getMAC_MTR12() {
        return MAC_MTR12;
    }

    public void setMAC_MTR12(String MAC_MTR12) {
        this.MAC_MTR12 = MAC_MTR12;
    }

    public String getSMAC_MTR12() {
        return SMAC_MTR12;
    }

    public void setSMAC_MTR12(String SMAC_MTR12) {
        this.SMAC_MTR12 = SMAC_MTR12;
    }

    private String MAC_MTR11;
    private String MAC_MTR12;
    private String MAC_MTR13;
    private String MAC_MTR14;
    private String MAC_MTR15;
    private String MAC_MTR16;
    private String MAC_MTR17;
    private String MAC_MTR18;
    private String MAC_MTR19;
    private String MAC_MTR20;
    private String MAC_MTR21;

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    private String DATE;

    public String getMAC_MTR13() {
        return MAC_MTR13;
    }

    public void setMAC_MTR13(String MAC_MTR13) {
        this.MAC_MTR13 = MAC_MTR13;
    }

    public String getMAC_MTR14() {
        return MAC_MTR14;
    }

    public void setMAC_MTR14(String MAC_MTR14) {
        this.MAC_MTR14 = MAC_MTR14;
    }

    public String getMAC_MTR15() {
        return MAC_MTR15;
    }

    public void setMAC_MTR15(String MAC_MTR15) {
        this.MAC_MTR15 = MAC_MTR15;
    }

    public String getMAC_MTR16() {
        return MAC_MTR16;
    }

    public void setMAC_MTR16(String MAC_MTR16) {
        this.MAC_MTR16 = MAC_MTR16;
    }

    public String getMAC_MTR17() {
        return MAC_MTR17;
    }

    public void setMAC_MTR17(String MAC_MTR17) {
        this.MAC_MTR17 = MAC_MTR17;
    }

    public String getMAC_MTR18() {
        return MAC_MTR18;
    }

    public void setMAC_MTR18(String MAC_MTR18) {
        this.MAC_MTR18 = MAC_MTR18;
    }

    public String getMAC_MTR19() {
        return MAC_MTR19;
    }

    public void setMAC_MTR19(String MAC_MTR19) {
        this.MAC_MTR19 = MAC_MTR19;
    }

    public String getMAC_MTR20() {
        return MAC_MTR20;
    }

    public void setMAC_MTR20(String MAC_MTR20) {
        this.MAC_MTR20 = MAC_MTR20;
    }

    public String getMAC_MTR21() {
        return MAC_MTR21;
    }

    public void setMAC_MTR21(String MAC_MTR21) {
        this.MAC_MTR21 = MAC_MTR21;
    }

    public String getMAC_MTR22() {
        return MAC_MTR22;
    }

    public void setMAC_MTR22(String MAC_MTR22) {
        this.MAC_MTR22 = MAC_MTR22;
    }

    private String MAC_MTR22;

    public String getMAC_MTR11() {
        return MAC_MTR11;
    }

    public void setMAC_MTR11(String MAC_MTR11) {
        this.MAC_MTR11 = MAC_MTR11;
    }

    private String MAC_CHEM1;
    private String MAC_CHEM2;
    private String MAC_CHEM3;
    private String MAC_CHEM4;
    private String MAC_CHEM5;
    private String MAC_CHEM6;
    private String MAC_CHEM7;
    private String MAC_CHEM8;
    private String MAC_CHEM9;
    private String MAC_CHEM10;
    private String MAC_CHEM11;
    private String MAC_CHEM12;
    private String MAC_CHEM13;
    private String MAC_CHEM14;
    private String MAC_CHEM15;
    private String MAC_CHEM16;
    private String MAC_CHEM17;
    private String MAC_CHEM18;
    private String MAC_CHEM19;
    private String MAC_CHEM20;

    private String MAC_PIG1;
    private String MAC_PIG2;
    private String MAC_PIG3;
    private String MAC_PIG4;
    private String MAC_PIG5;
    private String MAC_PIG6;
    private String MAC_PIG7;
    private String MAC_PIG8;
    private String MAC_PIG9;
    private String MAC_PIG10;

    private String KEY;
    private String UP_USER;
    private String MAC_NO;
    private String INS_NO;

    private String SMAC_MTR1;
    private String SMAC_MTR2;
    private String SMAC_MTR3;
    private String SMAC_MTR4;
    private String SMAC_MTR5;
    private String SMAC_MTR6;
    private String SMAC_MTR7;
    private String SMAC_MTR8;
    private String SMAC_MTR9;
    private String SMAC_MTR10;
    private String SMAC_MTR11;
    private String SMAC_MTR12;

    public String getSMAC_MTR11() {
        return SMAC_MTR11;
    }

    public void setSMAC_MTR11(String SMAC_MTR11) {
        this.SMAC_MTR11 = SMAC_MTR11;
    }

    private String SMAC_CHEM1;
    private String SMAC_CHEM2;
    private String SMAC_CHEM3;
    private String SMAC_CHEM4;
    private String SMAC_CHEM5;
    private String SMAC_CHEM6;
    private String SMAC_CHEM7;
    private String SMAC_CHEM8;
    private String SMAC_CHEM9;
    private String SMAC_CHEM10;
    private String SMAC_CHEM11;
    private String SMAC_CHEM12;
    private String SMAC_CHEM13;
    private String SMAC_CHEM14;
    private String SMAC_CHEM15;
    private String SMAC_CHEM16;
    private String SMAC_CHEM17;
    private String SMAC_CHEM18;
    private String SMAC_CHEM19;
    private String SMAC_CHEM20;

    private String SMAC_PIG1;
    private String SMAC_PIG2;
    private String SMAC_PIG3;
    private String SMAC_PIG4;
    private String SMAC_PIG5;
    private String SMAC_PIG6;
    private String SMAC_PIG7;
    private String SMAC_PIG8;
    private String SMAC_PIG9;
    private String SMAC_PIG10;



    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getMAC_MTR1() {
        return MAC_MTR1;
    }

    public void setMAC_MTR1(String MAC_MTR1) {
        this.MAC_MTR1 = MAC_MTR1;
    }

    public String getMAC_MTR2() {
        return MAC_MTR2;
    }

    public void setMAC_MTR2(String MAC_MTR2) {
        this.MAC_MTR2 = MAC_MTR2;
    }

    public String getMAC_MTR3() {
        return MAC_MTR3;
    }

    public void setMAC_MTR3(String MAC_MTR3) {
        this.MAC_MTR3 = MAC_MTR3;
    }

    public String getMAC_MTR4() {
        return MAC_MTR4;
    }

    public void setMAC_MTR4(String MAC_MTR4) {
        this.MAC_MTR4 = MAC_MTR4;
    }

    public String getMAC_MTR5() {
        return MAC_MTR5;
    }

    public void setMAC_MTR5(String MAC_MTR5) {
        this.MAC_MTR5 = MAC_MTR5;
    }

    public String getMAC_MTR6() {
        return MAC_MTR6;
    }

    public void setMAC_MTR6(String MAC_MTR6) {
        this.MAC_MTR6 = MAC_MTR6;
    }

    public String getMAC_MTR7() {
        return MAC_MTR7;
    }

    public void setMAC_MTR7(String MAC_MTR7) {
        this.MAC_MTR7 = MAC_MTR7;
    }

    public String getMAC_MTR8() {
        return MAC_MTR8;
    }

    public void setMAC_MTR8(String MAC_MTR8) {
        this.MAC_MTR8 = MAC_MTR8;
    }

    public String getMAC_MTR9() {
        return MAC_MTR9;
    }

    public void setMAC_MTR9(String MAC_MTR9) {
        this.MAC_MTR9 = MAC_MTR9;
    }

    public String getMAC_MTR10() {
        return MAC_MTR10;
    }

    public void setMAC_MTR10(String MAC_MTR10) {
        this.MAC_MTR10 = MAC_MTR10;
    }

    public String getMAC_CHEM1() {
        return MAC_CHEM1;
    }

    public void setMAC_CHEM1(String MAC_CHEM1) {
        this.MAC_CHEM1 = MAC_CHEM1;
    }

    public String getMAC_CHEM2() {
        return MAC_CHEM2;
    }

    public void setMAC_CHEM2(String MAC_CHEM2) {
        this.MAC_CHEM2 = MAC_CHEM2;
    }

    public String getMAC_CHEM3() {
        return MAC_CHEM3;
    }

    public void setMAC_CHEM3(String MAC_CHEM3) {
        this.MAC_CHEM3 = MAC_CHEM3;
    }

    public String getMAC_CHEM4() {
        return MAC_CHEM4;
    }

    public void setMAC_CHEM4(String MAC_CHEM4) {
        this.MAC_CHEM4 = MAC_CHEM4;
    }

    public String getMAC_CHEM5() {
        return MAC_CHEM5;
    }

    public void setMAC_CHEM5(String MAC_CHEM5) {
        this.MAC_CHEM5 = MAC_CHEM5;
    }

    public String getMAC_CHEM6() {
        return MAC_CHEM6;
    }

    public void setMAC_CHEM6(String MAC_CHEM6) {
        this.MAC_CHEM6 = MAC_CHEM6;
    }

    public String getMAC_CHEM7() {
        return MAC_CHEM7;
    }

    public void setMAC_CHEM7(String MAC_CHEM7) {
        this.MAC_CHEM7 = MAC_CHEM7;
    }

    public String getMAC_CHEM8() {
        return MAC_CHEM8;
    }

    public void setMAC_CHEM8(String MAC_CHEM8) {
        this.MAC_CHEM8 = MAC_CHEM8;
    }

    public String getMAC_CHEM9() {
        return MAC_CHEM9;
    }

    public void setMAC_CHEM9(String MAC_CHEM9) {
        this.MAC_CHEM9 = MAC_CHEM9;
    }

    public String getMAC_CHEM10() {
        return MAC_CHEM10;
    }

    public void setMAC_CHEM10(String MAC_CHEM10) {
        this.MAC_CHEM10 = MAC_CHEM10;
    }

    public String getMAC_CHEM11() {
        return MAC_CHEM11;
    }

    public void setMAC_CHEM11(String MAC_CHEM11) {
        this.MAC_CHEM11 = MAC_CHEM11;
    }

    public String getMAC_CHEM12() {
        return MAC_CHEM12;
    }

    public void setMAC_CHEM12(String MAC_CHEM12) {
        this.MAC_CHEM12 = MAC_CHEM12;
    }

    public String getMAC_CHEM13() {
        return MAC_CHEM13;
    }

    public void setMAC_CHEM13(String MAC_CHEM13) {
        this.MAC_CHEM13 = MAC_CHEM13;
    }

    public String getMAC_CHEM14() {
        return MAC_CHEM14;
    }

    public void setMAC_CHEM14(String MAC_CHEM14) {
        this.MAC_CHEM14 = MAC_CHEM14;
    }

    public String getMAC_CHEM15() {
        return MAC_CHEM15;
    }

    public void setMAC_CHEM15(String MAC_CHEM15) {
        this.MAC_CHEM15 = MAC_CHEM15;
    }

    public String getMAC_CHEM16() {
        return MAC_CHEM16;
    }

    public void setMAC_CHEM16(String MAC_CHEM16) {
        this.MAC_CHEM16 = MAC_CHEM16;
    }

    public String getMAC_CHEM17() {
        return MAC_CHEM17;
    }

    public void setMAC_CHEM17(String MAC_CHEM17) {
        this.MAC_CHEM17 = MAC_CHEM17;
    }

    public String getMAC_CHEM18() {
        return MAC_CHEM18;
    }

    public void setMAC_CHEM18(String MAC_CHEM18) {
        this.MAC_CHEM18 = MAC_CHEM18;
    }

    public String getMAC_CHEM19() {
        return MAC_CHEM19;
    }

    public void setMAC_CHEM19(String MAC_CHEM19) {
        this.MAC_CHEM19 = MAC_CHEM19;
    }

    public String getMAC_CHEM20() {
        return MAC_CHEM20;
    }

    public void setMAC_CHEM20(String MAC_CHEM20) {
        this.MAC_CHEM20 = MAC_CHEM20;
    }

    public String getMAC_PIG1() {
        return MAC_PIG1;
    }

    public void setMAC_PIG1(String MAC_PIG1) {
        this.MAC_PIG1 = MAC_PIG1;
    }

    public String getMAC_PIG2() {
        return MAC_PIG2;
    }

    public void setMAC_PIG2(String MAC_PIG2) {
        this.MAC_PIG2 = MAC_PIG2;
    }

    public String getMAC_PIG3() {
        return MAC_PIG3;
    }

    public void setMAC_PIG3(String MAC_PIG3) {
        this.MAC_PIG3 = MAC_PIG3;
    }

    public String getMAC_PIG4() {
        return MAC_PIG4;
    }

    public void setMAC_PIG4(String MAC_PIG4) {
        this.MAC_PIG4 = MAC_PIG4;
    }

    public String getMAC_PIG5() {
        return MAC_PIG5;
    }

    public void setMAC_PIG5(String MAC_PIG5) {
        this.MAC_PIG5 = MAC_PIG5;
    }

    public String getMAC_PIG6() {
        return MAC_PIG6;
    }

    public void setMAC_PIG6(String MAC_PIG6) {
        this.MAC_PIG6 = MAC_PIG6;
    }

    public String getMAC_PIG7() {
        return MAC_PIG7;
    }

    public void setMAC_PIG7(String MAC_PIG7) {
        this.MAC_PIG7 = MAC_PIG7;
    }

    public String getMAC_PIG8() {
        return MAC_PIG8;
    }

    public void setMAC_PIG8(String MAC_PIG8) {
        this.MAC_PIG8 = MAC_PIG8;
    }

    public String getMAC_PIG9() {
        return MAC_PIG9;
    }

    public void setMAC_PIG9(String MAC_PIG9) {
        this.MAC_PIG9 = MAC_PIG9;
    }

    public String getMAC_PIG10() {
        return MAC_PIG10;
    }

    public void setMAC_PIG10(String MAC_PIG10) {
        this.MAC_PIG10 = MAC_PIG10;
    }

    public String getMAC_NO() {
        return MAC_NO;
    }

    public void setMAC_NO(String MAC_NO) {
        this.MAC_NO = MAC_NO;
    }

    public String getINS_NO() {
        return INS_NO;
    }

    public void setINS_NO(String INS_NO) {
        this.INS_NO = INS_NO;
    }

    public String getKEY() {
        return KEY;
    }

    public void setKEY(String KEY) {
        this.KEY = KEY;
    }

    public String getUP_USER() {
        return UP_USER;
    }

    public void setUP_USER(String UP_USER) {
        this.UP_USER = UP_USER;
    }

    public String getSMAC_MTR1() {
        return SMAC_MTR1;
    }

    public void setSMAC_MTR1(String SMAC_MTR1) {
        this.SMAC_MTR1 = SMAC_MTR1;
    }

    public String getSMAC_MTR2() {
        return SMAC_MTR2;
    }

    public void setSMAC_MTR2(String SMAC_MTR2) {
        this.SMAC_MTR2 = SMAC_MTR2;
    }

    public String getSMAC_MTR3() {
        return SMAC_MTR3;
    }

    public void setSMAC_MTR3(String SMAC_MTR3) {
        this.SMAC_MTR3 = SMAC_MTR3;
    }

    public String getSMAC_MTR4() {
        return SMAC_MTR4;
    }

    public void setSMAC_MTR4(String SMAC_MTR4) {
        this.SMAC_MTR4 = SMAC_MTR4;
    }

    public String getSMAC_MTR5() {
        return SMAC_MTR5;
    }

    public void setSMAC_MTR5(String SMAC_MTR5) {
        this.SMAC_MTR5 = SMAC_MTR5;
    }

    public String getSMAC_MTR6() {
        return SMAC_MTR6;
    }

    public void setSMAC_MTR6(String SMAC_MTR6) {
        this.SMAC_MTR6 = SMAC_MTR6;
    }

    public String getSMAC_MTR7() {
        return SMAC_MTR7;
    }

    public void setSMAC_MTR7(String SMAC_MTR7) {
        this.SMAC_MTR7 = SMAC_MTR7;
    }

    public String getSMAC_MTR8() {
        return SMAC_MTR8;
    }

    public void setSMAC_MTR8(String SMAC_MTR8) {
        this.SMAC_MTR8 = SMAC_MTR8;
    }

    public String getSMAC_MTR9() {
        return SMAC_MTR9;
    }

    public void setSMAC_MTR9(String SMAC_MTR9) {
        this.SMAC_MTR9 = SMAC_MTR9;
    }

    public String getSMAC_MTR10() {
        return SMAC_MTR10;
    }

    public void setSMAC_MTR10(String SMAC_MTR10) {
        this.SMAC_MTR10 = SMAC_MTR10;
    }

    public String getSMAC_CHEM1() {
        return SMAC_CHEM1;
    }

    public void setSMAC_CHEM1(String SMAC_CHEM1) {
        this.SMAC_CHEM1 = SMAC_CHEM1;
    }

    public String getSMAC_CHEM2() {
        return SMAC_CHEM2;
    }

    public void setSMAC_CHEM2(String SMAC_CHEM2) {
        this.SMAC_CHEM2 = SMAC_CHEM2;
    }

    public String getSMAC_CHEM3() {
        return SMAC_CHEM3;
    }

    public void setSMAC_CHEM3(String SMAC_CHEM3) {
        this.SMAC_CHEM3 = SMAC_CHEM3;
    }

    public String getSMAC_CHEM4() {
        return SMAC_CHEM4;
    }

    public void setSMAC_CHEM4(String SMAC_CHEM4) {
        this.SMAC_CHEM4 = SMAC_CHEM4;
    }

    public String getSMAC_CHEM5() {
        return SMAC_CHEM5;
    }

    public void setSMAC_CHEM5(String SMAC_CHEM5) {
        this.SMAC_CHEM5 = SMAC_CHEM5;
    }

    public String getSMAC_CHEM6() {
        return SMAC_CHEM6;
    }

    public void setSMAC_CHEM6(String SMAC_CHEM6) {
        this.SMAC_CHEM6 = SMAC_CHEM6;
    }

    public String getSMAC_CHEM7() {
        return SMAC_CHEM7;
    }

    public void setSMAC_CHEM7(String SMAC_CHEM7) {
        this.SMAC_CHEM7 = SMAC_CHEM7;
    }

    public String getSMAC_CHEM8() {
        return SMAC_CHEM8;
    }

    public void setSMAC_CHEM8(String SMAC_CHEM8) {
        this.SMAC_CHEM8 = SMAC_CHEM8;
    }

    public String getSMAC_CHEM9() {
        return SMAC_CHEM9;
    }

    public void setSMAC_CHEM9(String SMAC_CHEM9) {
        this.SMAC_CHEM9 = SMAC_CHEM9;
    }

    public String getSMAC_CHEM10() {
        return SMAC_CHEM10;
    }

    public void setSMAC_CHEM10(String SMAC_CHEM10) {
        this.SMAC_CHEM10 = SMAC_CHEM10;
    }

    public String getSMAC_CHEM11() {
        return SMAC_CHEM11;
    }

    public void setSMAC_CHEM11(String SMAC_CHEM11) {
        this.SMAC_CHEM11 = SMAC_CHEM11;
    }

    public String getSMAC_CHEM12() {
        return SMAC_CHEM12;
    }

    public void setSMAC_CHEM12(String SMAC_CHEM12) {
        this.SMAC_CHEM12 = SMAC_CHEM12;
    }

    public String getSMAC_CHEM13() {
        return SMAC_CHEM13;
    }

    public void setSMAC_CHEM13(String SMAC_CHEM13) {
        this.SMAC_CHEM13 = SMAC_CHEM13;
    }

    public String getSMAC_CHEM14() {
        return SMAC_CHEM14;
    }

    public void setSMAC_CHEM14(String SMAC_CHEM14) {
        this.SMAC_CHEM14 = SMAC_CHEM14;
    }

    public String getSMAC_CHEM15() {
        return SMAC_CHEM15;
    }

    public void setSMAC_CHEM15(String SMAC_CHEM15) {
        this.SMAC_CHEM15 = SMAC_CHEM15;
    }

    public String getSMAC_CHEM16() {
        return SMAC_CHEM16;
    }

    public void setSMAC_CHEM16(String SMAC_CHEM16) {
        this.SMAC_CHEM16 = SMAC_CHEM16;
    }

    public String getSMAC_CHEM17() {
        return SMAC_CHEM17;
    }

    public void setSMAC_CHEM17(String SMAC_CHEM17) {
        this.SMAC_CHEM17 = SMAC_CHEM17;
    }

    public String getSMAC_CHEM18() {
        return SMAC_CHEM18;
    }

    public void setSMAC_CHEM18(String SMAC_CHEM18) {
        this.SMAC_CHEM18 = SMAC_CHEM18;
    }

    public String getSMAC_CHEM19() {
        return SMAC_CHEM19;
    }

    public void setSMAC_CHEM19(String SMAC_CHEM19) {
        this.SMAC_CHEM19 = SMAC_CHEM19;
    }

    public String getSMAC_CHEM20() {
        return SMAC_CHEM20;
    }

    public void setSMAC_CHEM20(String SMAC_CHEM20) {
        this.SMAC_CHEM20 = SMAC_CHEM20;
    }

    public String getSMAC_PIG1() {
        return SMAC_PIG1;
    }

    public void setSMAC_PIG1(String SMAC_PIG1) {
        this.SMAC_PIG1 = SMAC_PIG1;
    }

    public String getSMAC_PIG2() {
        return SMAC_PIG2;
    }

    public void setSMAC_PIG2(String SMAC_PIG2) {
        this.SMAC_PIG2 = SMAC_PIG2;
    }

    public String getSMAC_PIG3() {
        return SMAC_PIG3;
    }

    public void setSMAC_PIG3(String SMAC_PIG3) {
        this.SMAC_PIG3 = SMAC_PIG3;
    }

    public String getSMAC_PIG4() {
        return SMAC_PIG4;
    }

    public void setSMAC_PIG4(String SMAC_PIG4) {
        this.SMAC_PIG4 = SMAC_PIG4;
    }

    public String getSMAC_PIG5() {
        return SMAC_PIG5;
    }

    public void setSMAC_PIG5(String SMAC_PIG5) {
        this.SMAC_PIG5 = SMAC_PIG5;
    }

    public String getSMAC_PIG6() {
        return SMAC_PIG6;
    }

    public void setSMAC_PIG6(String SMAC_PIG6) {
        this.SMAC_PIG6 = SMAC_PIG6;
    }

    public String getSMAC_PIG7() {
        return SMAC_PIG7;
    }

    public void setSMAC_PIG7(String SMAC_PIG7) {
        this.SMAC_PIG7 = SMAC_PIG7;
    }

    public String getSMAC_PIG8() {
        return SMAC_PIG8;
    }

    public void setSMAC_PIG8(String SMAC_PIG8) {
        this.SMAC_PIG8 = SMAC_PIG8;
    }

    public String getSMAC_PIG9() {
        return SMAC_PIG9;
    }

    public void setSMAC_PIG9(String SMAC_PIG9) {
        this.SMAC_PIG9 = SMAC_PIG9;
    }

    public String getSMAC_PIG10() {
        return SMAC_PIG10;
    }

    public void setSMAC_PIG10(String SMAC_PIG10) {
        this.SMAC_PIG10 = SMAC_PIG10;
    }
}
