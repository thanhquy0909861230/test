package com.deanshoes.AppQAM.model.QAM;

public class UploadFile {
    private String UP_NO;
    private String UP_NAME;
    private String UP_LINK;
    private String UP_SIZE;
    private String UP_DATE;
    private String DOWNLOAD;

    public String getUP_NO() {
        return UP_NO;
    }

    public void setUP_NO(String UP_NO) {
        this.UP_NO = UP_NO;
    }

    public String getUP_NAME() {
        return UP_NAME;
    }

    public void setUP_NAME(String UP_NAME) {
        this.UP_NAME = UP_NAME;
    }

    public String getUP_LINK() {
        return UP_LINK;
    }

    public void setUP_LINK(String UP_LINK) {
        this.UP_LINK = UP_LINK;
    }

    public String getUP_SIZE() {
        return UP_SIZE;
    }

    public void setUP_SIZE(String UP_SIZE) {
        this.UP_SIZE = UP_SIZE;
    }

    public String getUP_DATE() {
        return UP_DATE;
    }

    public void setUP_DATE(String UP_DATE) {
        this.UP_DATE = UP_DATE;
    }

    public String getDOWNLOAD() {
        return DOWNLOAD;
    }

    public void setDOWNLOAD(String DOWNLOAD) {
        this.DOWNLOAD = DOWNLOAD;
    }
}
