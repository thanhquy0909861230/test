package com.deanshoes.AppQAM.model.QAM;

public class QAM_CR {
    private String CR_ID;
    private String CR_MCS;
    private String CR_NAME;
    private String CR_FULLNAME;

    public String getCR_ID() {
        return CR_ID;
    }

    public void setCR_ID(String CR_ID) {
        this.CR_ID = CR_ID;
    }

    public String getCR_MCS() {
        return CR_MCS;
    }

    public void setCR_MCS(String CR_MCS) {
        this.CR_MCS = CR_MCS;
    }

    public String getCR_NAME() {
        return CR_NAME;
    }

    public void setCR_NAME(String CR_NAME) {
        this.CR_NAME = CR_NAME;
    }

    public String getCR_FULLNAME() {
        return CR_FULLNAME;
    }

    public void setCR_FULLNAME(String CR_FULLNAME) {
        this.CR_FULLNAME = CR_FULLNAME;
    }
}
