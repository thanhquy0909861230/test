package com.deanshoes.AppQAM.model.QAM;

public class QAM_MTR {
    private String MTR_NO;
    private String MTR;
    private String MTR_WEIGHT;
    private String UP_DATE;
    private String UP_USER;

    public String getMTR_NO() {
        return MTR_NO;
    }

    public void setMTR_NO(String MTR_NO) {
        this.MTR_NO = MTR_NO;
    }

    public String getMTR() {
        return MTR;
    }

    public void setMTR(String MTR) {
        this.MTR = MTR;
    }

    public String getMTR_WEIGHT() {
        return MTR_WEIGHT;
    }

    public void setMTR_WEIGHT(String MTR_WEIGHT) {
        this.MTR_WEIGHT = MTR_WEIGHT;
    }

    public String getUP_DATE() {
        return UP_DATE;
    }

    public void setUP_DATE(String UP_DATE) {
        this.UP_DATE = UP_DATE;
    }

    public String getUP_USER() {
        return UP_USER;
    }

    public void setUP_USER(String UP_USER) {
        this.UP_USER = UP_USER;
    }
}
