package com.deanshoes.AppQAM.model.QAM;

public class MODEL_SEND_MAIL {
    private String MSC;
    private String DATE;
    private String SHIFT;
    private String STYLE;
    private String COLOR;
    private String INS_NO;
    private String BTC_NO;
    private String ER;
    private String TL;
    private String EXDATE;

    public String getER() {
        return ER;
    }

    public void setER(String ER) {
        this.ER = ER;
    }

    public String getTL() {
        return TL;
    }

    public void setTL(String TL) {
        this.TL = TL;
    }

    public String getEXDATE() {
        return EXDATE;
    }

    public void setEXDATE(String EXDATE) {
        this.EXDATE = EXDATE;
    }

    public String getBTC_NO() {
        return BTC_NO;
    }

    public void setBTC_NO(String BTC_NO) {
        this.BTC_NO = BTC_NO;
    }

    public String getMSC() {
        return MSC;
    }

    public void setMSC(String MSC) {
        this.MSC = MSC;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getSHIFT() {
        return SHIFT;
    }

    public void setSHIFT(String SHIFT) {
        this.SHIFT = SHIFT;
    }

    public String getSTYLE() {
        return STYLE;
    }

    public void setSTYLE(String STYLE) {
        this.STYLE = STYLE;
    }

    public String getCOLOR() {
        return COLOR;
    }

    public void setCOLOR(String COLOR) {
        this.COLOR = COLOR;
    }

    public String getINS_NO() {
        return INS_NO;
    }

    public void setINS_NO(String INS_NO) {
        this.INS_NO = INS_NO;
    }
}
