package com.deanshoes.AppQAM.model.QAM;

public class QAM_CHEM {
    private String CHEM_NO;
    private String CHEM;
    private String CHEM_WEIGHT;
    private String UP_DATE;
    private String UP_USER;

    public String getCHEM_NO() {
        return CHEM_NO;
    }

    public void setCHEM_NO(String CHEM_NO) {
        this.CHEM_NO = CHEM_NO;
    }

    public String getCHEM() {
        return CHEM;
    }

    public void setCHEM(String CHEM) {
        this.CHEM = CHEM;
    }

    public String getCHEM_WEIGHT() {
        return CHEM_WEIGHT;
    }

    public void setCHEM_WEIGHT(String CHEM_WEIGHT) {
        this.CHEM_WEIGHT = CHEM_WEIGHT;
    }

    public String getUP_DATE() {
        return UP_DATE;
    }

    public void setUP_DATE(String UP_DATE) {
        this.UP_DATE = UP_DATE;
    }

    public String getUP_USER() {
        return UP_USER;
    }

    public void setUP_USER(String UP_USER) {
        this.UP_USER = UP_USER;
    }
}
