package com.deanshoes.AppQAM.model.QAM;

public class RP_LIST_BATCH {
    private String INS_NO;
    private String MCS_NO;
    private String BTC_NO;
    private String STYLE;
    private String COLOR;
    private String HARD;
    private String BTC_BATCH_NO;

    private String AMT_NO;
    private String AMT_CHECK;
    private String CHK_MTR1;
    private String CHK_MTR2;
    private String CHK_MTR3;
    private String CHK_MTR4;
    private String CHK_MTR5;
    private String CHK_MTR6;
    private String CHK_MTR7;
    private String CHK_MTR8;
    private String CHK_MTR9;
    private String CHK_MTR10;

    private String CHK_CHEM1;
    private String CHK_CHEM2;
    private String CHK_CHEM3;
    private String CHK_CHEM4;
    private String CHK_CHEM5;
    private String CHK_CHEM6;
    private String CHK_CHEM7;
    private String CHK_CHEM8;
    private String CHK_CHEM9;
    private String CHK_CHEM10;
    private String CHK_CHEM11;
    private String CHK_CHEM12;
    private String CHK_CHEM13;
    private String CHK_CHEM14;
    private String CHK_CHEM15;
    private String CHK_CHEM16;
    private String CHK_CHEM17;
    private String CHK_CHEM18;
    private String CHK_CHEM19;
    private String CHK_CHEM20;

    private String CHK_PIG1;
    private String CHK_PIG2;
    private String CHK_PIG3;
    private String CHK_PIG4;
    private String CHK_PIG5;
    private String CHK_PIG6;
    private String CHK_PIG7;
    private String CHK_PIG8;
    private String CHK_PIG9;
    private String CHK_PIG10;

    private String NOTE_MTR1;
    private String NOTE_MTR2;
    private String NOTE_MTR3;
    private String NOTE_MTR4;
    private String NOTE_MTR5;
    private String NOTE_MTR6;
    private String NOTE_MTR7;
    private String NOTE_MTR8;
    private String NOTE_MTR9;
    private String NOTE_MTR10;

    private String NOTE_CHEM1;
    private String NOTE_CHEM2;
    private String NOTE_CHEM3;
    private String NOTE_CHEM4;
    private String NOTE_CHEM5;
    private String NOTE_CHEM6;
    private String NOTE_CHEM7;
    private String NOTE_CHEM8;
    private String NOTE_CHEM9;
    private String NOTE_CHEM10;
    private String NOTE_CHEM11;
    private String NOTE_CHEM12;
    private String NOTE_CHEM13;
    private String NOTE_CHEM14;
    private String NOTE_CHEM15;
    private String NOTE_CHEM16;
    private String NOTE_CHEM17;
    private String NOTE_CHEM18;
    private String NOTE_CHEM19;
    private String NOTE_CHEM20;

    private String NOTE_PIG1;
    private String NOTE_PIG2;
    private String NOTE_PIG3;
    private String NOTE_PIG4;
    private String NOTE_PIG5;
    private String NOTE_PIG6;
    private String NOTE_PIG7;
    private String NOTE_PIG8;
    private String NOTE_PIG9;
    private String NOTE_PIG10;

    public String getINS_NO() {
        return INS_NO;
    }

    public String getMCS_NO() {
        return MCS_NO;
    }

    public String getBTC_NO() {
        return BTC_NO;
    }

    public String getSTYLE() {
        return STYLE;
    }

    public String getCOLOR() {
        return COLOR;
    }

    public String getHARD() {
        return HARD;
    }

    public String getBTC_BATCH_NO() {
        return BTC_BATCH_NO;
    }

    public String getAMT_NO() {
        return AMT_NO;
    }

    public String getAMT_CHECK() {
        return AMT_CHECK;
    }

    public String getCHK_MTR1() {
        return CHK_MTR1;
    }

    public String getCHK_MTR2() {
        return CHK_MTR2;
    }

    public String getCHK_MTR3() {
        return CHK_MTR3;
    }

    public String getCHK_MTR4() {
        return CHK_MTR4;
    }

    public String getCHK_MTR5() {
        return CHK_MTR5;
    }

    public String getCHK_MTR6() {
        return CHK_MTR6;
    }

    public String getCHK_MTR7() {
        return CHK_MTR7;
    }

    public String getCHK_MTR8() {
        return CHK_MTR8;
    }

    public String getCHK_MTR9() {
        return CHK_MTR9;
    }

    public String getCHK_MTR10() {
        return CHK_MTR10;
    }

    public String getCHK_CHEM1() {
        return CHK_CHEM1;
    }

    public String getCHK_CHEM2() {
        return CHK_CHEM2;
    }

    public String getCHK_CHEM3() {
        return CHK_CHEM3;
    }

    public String getCHK_CHEM4() {
        return CHK_CHEM4;
    }

    public String getCHK_CHEM5() {
        return CHK_CHEM5;
    }

    public String getCHK_CHEM6() {
        return CHK_CHEM6;
    }

    public String getCHK_CHEM7() {
        return CHK_CHEM7;
    }

    public String getCHK_CHEM8() {
        return CHK_CHEM8;
    }

    public String getCHK_CHEM9() {
        return CHK_CHEM9;
    }

    public String getCHK_CHEM10() {
        return CHK_CHEM10;
    }

    public String getCHK_CHEM11() {
        return CHK_CHEM11;
    }

    public String getCHK_CHEM12() {
        return CHK_CHEM12;
    }

    public String getCHK_CHEM13() {
        return CHK_CHEM13;
    }

    public String getCHK_CHEM14() {
        return CHK_CHEM14;
    }

    public String getCHK_CHEM15() {
        return CHK_CHEM15;
    }

    public String getCHK_CHEM16() {
        return CHK_CHEM16;
    }

    public String getCHK_CHEM17() {
        return CHK_CHEM17;
    }

    public String getCHK_CHEM18() {
        return CHK_CHEM18;
    }

    public String getCHK_CHEM19() {
        return CHK_CHEM19;
    }

    public String getCHK_CHEM20() {
        return CHK_CHEM20;
    }

    public String getCHK_PIG1() {
        return CHK_PIG1;
    }

    public String getCHK_PIG2() {
        return CHK_PIG2;
    }

    public String getCHK_PIG3() {
        return CHK_PIG3;
    }

    public String getCHK_PIG4() {
        return CHK_PIG4;
    }

    public String getCHK_PIG5() {
        return CHK_PIG5;
    }

    public String getCHK_PIG6() {
        return CHK_PIG6;
    }

    public String getCHK_PIG7() {
        return CHK_PIG7;
    }

    public String getCHK_PIG8() {
        return CHK_PIG8;
    }

    public String getCHK_PIG9() {
        return CHK_PIG9;
    }

    public String getCHK_PIG10() {
        return CHK_PIG10;
    }

    public String getNOTE_MTR1() {
        return NOTE_MTR1;
    }

    public String getNOTE_MTR2() {
        return NOTE_MTR2;
    }

    public String getNOTE_MTR3() {
        return NOTE_MTR3;
    }

    public String getNOTE_MTR4() {
        return NOTE_MTR4;
    }

    public String getNOTE_MTR5() {
        return NOTE_MTR5;
    }

    public String getNOTE_MTR6() {
        return NOTE_MTR6;
    }

    public String getNOTE_MTR7() {
        return NOTE_MTR7;
    }

    public String getNOTE_MTR8() {
        return NOTE_MTR8;
    }

    public String getNOTE_MTR9() {
        return NOTE_MTR9;
    }

    public String getNOTE_MTR10() {
        return NOTE_MTR10;
    }

    public String getNOTE_CHEM1() {
        return NOTE_CHEM1;
    }

    public String getNOTE_CHEM2() {
        return NOTE_CHEM2;
    }

    public String getNOTE_CHEM3() {
        return NOTE_CHEM3;
    }

    public String getNOTE_CHEM4() {
        return NOTE_CHEM4;
    }

    public String getNOTE_CHEM5() {
        return NOTE_CHEM5;
    }

    public String getNOTE_CHEM6() {
        return NOTE_CHEM6;
    }

    public String getNOTE_CHEM7() {
        return NOTE_CHEM7;
    }

    public String getNOTE_CHEM8() {
        return NOTE_CHEM8;
    }

    public String getNOTE_CHEM9() {
        return NOTE_CHEM9;
    }

    public String getNOTE_CHEM10() {
        return NOTE_CHEM10;
    }

    public String getNOTE_CHEM11() {
        return NOTE_CHEM11;
    }

    public String getNOTE_CHEM12() {
        return NOTE_CHEM12;
    }

    public String getNOTE_CHEM13() {
        return NOTE_CHEM13;
    }

    public String getNOTE_CHEM14() {
        return NOTE_CHEM14;
    }

    public String getNOTE_CHEM15() {
        return NOTE_CHEM15;
    }

    public String getNOTE_CHEM16() {
        return NOTE_CHEM16;
    }

    public String getNOTE_CHEM17() {
        return NOTE_CHEM17;
    }

    public String getNOTE_CHEM18() {
        return NOTE_CHEM18;
    }

    public String getNOTE_CHEM19() {
        return NOTE_CHEM19;
    }

    public String getNOTE_CHEM20() {
        return NOTE_CHEM20;
    }

    public String getNOTE_PIG1() {
        return NOTE_PIG1;
    }

    public String getNOTE_PIG2() {
        return NOTE_PIG2;
    }

    public String getNOTE_PIG3() {
        return NOTE_PIG3;
    }

    public String getNOTE_PIG4() {
        return NOTE_PIG4;
    }

    public String getNOTE_PIG5() {
        return NOTE_PIG5;
    }

    public String getNOTE_PIG6() {
        return NOTE_PIG6;
    }

    public String getNOTE_PIG7() {
        return NOTE_PIG7;
    }

    public String getNOTE_PIG8() {
        return NOTE_PIG8;
    }

    public String getNOTE_PIG9() {
        return NOTE_PIG9;
    }

    public String getNOTE_PIG10() {
        return NOTE_PIG10;
    }
}
