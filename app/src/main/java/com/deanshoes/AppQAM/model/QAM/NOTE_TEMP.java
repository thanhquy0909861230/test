package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class NOTE_TEMP implements Serializable {
    private static final long serialVersionUID = 1L;

    private String MAC;
    private String SHIFT;
    private String UP_DATE;
    private String NOTEMP1;
    private String NOTEMP2;
    private String NOTEMP3;
    private String NOTEMP4;
    private String NOTEMP5;
    private String NOTEMP6;
    private String NOTEMP7;
    private String NOTEMP8;
    private String NOTEMP9;
    private String NOTEMP10;
    private String NOTEMP11;
    private String NOTEMP12;
    private String NOTEMP13;
    private String NOTEMP14;
    private String NOTEMP15;
    private String NOTEMP16;

    private String DNOTEMP1;
    private String DNOTEMP2;
    private String DNOTEMP3;
    private String DNOTEMP4;
    private String DNOTEMP5;
    private String DNOTEMP6;
    private String DNOTEMP7;
    private String DNOTEMP8;
    private String DNOTEMP9;
    private String DNOTEMP10;
    private String DNOTEMP11;
    private String DNOTEMP12;
    private String DNOTEMP13;
    private String DNOTEMP14;
    private String DNOTEMP15;
    private String DNOTEMP16;
    private String ID;


    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    public String getSHIFT() {
        return SHIFT;
    }

    public void setSHIFT(String SHIFT) {
        this.SHIFT = SHIFT;
    }

    public String getUP_DATE() {
        return UP_DATE;
    }

    public void setUP_DATE(String UP_DATE) {
        this.UP_DATE = UP_DATE;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNOTEMP1() {
        return NOTEMP1;
    }

    public void setNOTEMP1(String NOTEMP1) {
        this.NOTEMP1 = NOTEMP1;
    }

    public String getNOTEMP2() {
        return NOTEMP2;
    }

    public void setNOTEMP2(String NOTEMP2) {
        this.NOTEMP2 = NOTEMP2;
    }

    public String getNOTEMP3() {
        return NOTEMP3;
    }

    public void setNOTEMP3(String NOTEMP3) {
        this.NOTEMP3 = NOTEMP3;
    }

    public String getNOTEMP4() {
        return NOTEMP4;
    }

    public void setNOTEMP4(String NOTEMP4) {
        this.NOTEMP4 = NOTEMP4;
    }

    public String getNOTEMP5() {
        return NOTEMP5;
    }

    public void setNOTEMP5(String NOTEMP5) {
        this.NOTEMP5 = NOTEMP5;
    }

    public String getNOTEMP6() {
        return NOTEMP6;
    }

    public void setNOTEMP6(String NOTEMP6) {
        this.NOTEMP6 = NOTEMP6;
    }

    public String getNOTEMP7() {
        return NOTEMP7;
    }

    public void setNOTEMP7(String NOTEMP7) {
        this.NOTEMP7 = NOTEMP7;
    }

    public String getNOTEMP8() {
        return NOTEMP8;
    }

    public void setNOTEMP8(String NOTEMP8) {
        this.NOTEMP8 = NOTEMP8;
    }

    public String getNOTEMP9() {
        return NOTEMP9;
    }

    public void setNOTEMP9(String NOTEMP9) {
        this.NOTEMP9 = NOTEMP9;
    }

    public String getNOTEMP10() {
        return NOTEMP10;
    }

    public void setNOTEMP10(String NOTEMP10) {
        this.NOTEMP10 = NOTEMP10;
    }

    public String getNOTEMP11() {
        return NOTEMP11;
    }

    public void setNOTEMP11(String NOTEMP11) {
        this.NOTEMP11 = NOTEMP11;
    }

    public String getNOTEMP12() {
        return NOTEMP12;
    }

    public void setNOTEMP12(String NOTEMP12) {
        this.NOTEMP12 = NOTEMP12;
    }

    public String getNOTEMP13() {
        return NOTEMP13;
    }

    public void setNOTEMP13(String NOTEMP13) {
        this.NOTEMP13 = NOTEMP13;
    }

    public String getNOTEMP14() {
        return NOTEMP14;
    }

    public void setNOTEMP14(String NOTEMP14) {
        this.NOTEMP14 = NOTEMP14;
    }

    public String getNOTEMP15() {
        return NOTEMP15;
    }

    public void setNOTEMP15(String NOTEMP15) {
        this.NOTEMP15 = NOTEMP15;
    }

    public String getNOTEMP16() {
        return NOTEMP16;
    }

    public void setNOTEMP16(String NOTEMP16) {
        this.NOTEMP16 = NOTEMP16;
    }

    public String getDNOTEMP1() {
        return DNOTEMP1;
    }

    public void setDNOTEMP1(String DNOTEMP1) {
        this.DNOTEMP1 = DNOTEMP1;
    }

    public String getDNOTEMP2() {
        return DNOTEMP2;
    }

    public void setDNOTEMP2(String DNOTEMP2) {
        this.DNOTEMP2 = DNOTEMP2;
    }

    public String getDNOTEMP3() {
        return DNOTEMP3;
    }

    public void setDNOTEMP3(String DNOTEMP3) {
        this.DNOTEMP3 = DNOTEMP3;
    }

    public String getDNOTEMP4() {
        return DNOTEMP4;
    }

    public void setDNOTEMP4(String DNOTEMP4) {
        this.DNOTEMP4 = DNOTEMP4;
    }

    public String getDNOTEMP5() {
        return DNOTEMP5;
    }

    public void setDNOTEMP5(String DNOTEMP5) {
        this.DNOTEMP5 = DNOTEMP5;
    }

    public String getDNOTEMP6() {
        return DNOTEMP6;
    }

    public void setDNOTEMP6(String DNOTEMP6) {
        this.DNOTEMP6 = DNOTEMP6;
    }

    public String getDNOTEMP7() {
        return DNOTEMP7;
    }

    public void setDNOTEMP7(String DNOTEMP7) {
        this.DNOTEMP7 = DNOTEMP7;
    }

    public String getDNOTEMP8() {
        return DNOTEMP8;
    }

    public void setDNOTEMP8(String DNOTEMP8) {
        this.DNOTEMP8 = DNOTEMP8;
    }

    public String getDNOTEMP9() {
        return DNOTEMP9;
    }

    public void setDNOTEMP9(String DNOTEMP9) {
        this.DNOTEMP9 = DNOTEMP9;
    }

    public String getDNOTEMP10() {
        return DNOTEMP10;
    }

    public void setDNOTEMP10(String DNOTEMP10) {
        this.DNOTEMP10 = DNOTEMP10;
    }

    public String getDNOTEMP11() {
        return DNOTEMP11;
    }

    public void setDNOTEMP11(String DNOTEMP11) {
        this.DNOTEMP11 = DNOTEMP11;
    }

    public String getDNOTEMP12() {
        return DNOTEMP12;
    }

    public void setDNOTEMP12(String DNOTEMP12) {
        this.DNOTEMP12 = DNOTEMP12;
    }

    public String getDNOTEMP13() {
        return DNOTEMP13;
    }

    public void setDNOTEMP13(String DNOTEMP13) {
        this.DNOTEMP13 = DNOTEMP13;
    }

    public String getDNOTEMP14() {
        return DNOTEMP14;
    }

    public void setDNOTEMP14(String DNOTEMP14) {
        this.DNOTEMP14 = DNOTEMP14;
    }

    public String getDNOTEMP15() {
        return DNOTEMP15;
    }

    public void setDNOTEMP15(String DNOTEMP15) {
        this.DNOTEMP15 = DNOTEMP15;
    }

    public String getDNOTEMP16() {
        return DNOTEMP16;
    }

    public void setDNOTEMP16(String DNOTEMP16) {
        this.DNOTEMP16 = DNOTEMP16;
    }
}