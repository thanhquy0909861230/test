package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class QAM_SCALE_HAND implements Serializable {

    private String SCH_SHIFT;
    private String SCH_DATE;
    private String SCH_MCS;
    private String KEY;

    private String SCH_RAW1;
    private String SCH_RAW2;
    private String SCH_RAW3;
    private String SCH_RAW4;
    private String SCH_RAW5;
    private String SCH_RAW6;
    private String SCH_RAW7;
    private String SCH_RAW8;
    private String SCH_RAW9;
    private String SCH_RAW10;
    private String SCH_CHEM1;
    private String SCH_CHEM2;
    private String SCH_CHEM3;
    private String SCH_CHEM4;
    private String SCH_CHEM5;
    private String SCH_CHEM6;
    private String SCH_CHEM7;
    private String SCH_CHEM8;
    private String SCH_CHEM9;
    private String SCH_CHEM10;
    private String SCH_CHEM11;
    private String SCH_CHEM12;
    private String SCH_CHEM13;
    private String SCH_CHEM14;
    private String SCH_CHEM15;
    private String SCH_CHEM16;
    private String SCH_CHEM17;
    private String SCH_CHEM18;
    private String SCH_CHEM19;
    private String SCH_CHEM20;
    private String SCH_PIG1;
    private String SCH_PIG2;
    private String SCH_PIG3;
    private String SCH_PIG4;
    private String SCH_PIG5;
    private String SCH_PIG6;
    private String SCH_PIG7;
    private String SCH_PIG8;
    private String SCH_PIG9;
    private String SCH_PIG10;
    private String SCH_WRAW1;
    private String SCH_WRAW2;
    private String SCH_WRAW3;
    private String SCH_WRAW4;
    private String SCH_WRAW5;
    private String SCH_WRAW6;
    private String SCH_WRAW7;
    private String SCH_WRAW8;
    private String SCH_WRAW9;
    private String SCH_WRAW10;
    private String SCH_WCHEM1;
    private String SCH_WCHEM2;
    private String SCH_WCHEM3;
    private String SCH_WCHEM4;
    private String SCH_WCHEM5;
    private String SCH_WCHEM6;
    private String SCH_WCHEM7;
    private String SCH_WCHEM8;
    private String SCH_WCHEM9;
    private String SCH_WCHEM10;
    private String SCH_WCHEM11;
    private String SCH_WCHEM12;
    private String SCH_WCHEM13;
    private String SCH_WCHEM14;
    private String SCH_WCHEM15;
    private String SCH_WCHEM16;
    private String SCH_WCHEM17;
    private String SCH_WCHEM18;
    private String SCH_WCHEM19;
    private String SCH_WCHEM20;
    private String SCH_WPIG1;
    private String SCH_WPIG2;
    private String SCH_WPIG3;
    private String SCH_WPIG4;
    private String SCH_WPIG5;
    private String SCH_WPIG6;
    private String SCH_WPIG7;
    private String SCH_WPIG8;
    private String SCH_WPIG9;
    private String SCH_WPIG10;

    private String HAND_ID;

    public String getKEY() {
        return KEY;
    }

    public void setKEY(String KEY) {
        this.KEY = KEY;
    }

    public String getHAND_ID() {
        return HAND_ID;
    }

    public void setHAND_ID(String HAND_ID) {
        this.HAND_ID = HAND_ID;
    }

    public String getSCH_SHIFT() {
        return SCH_SHIFT;
    }

    public void setSCH_SHIFT(String SCH_SHIFT) {
        this.SCH_SHIFT = SCH_SHIFT;
    }

    public String getSCH_DATE() {
        return SCH_DATE;
    }

    public void setSCH_DATE(String SCH_DATE) {
        this.SCH_DATE = SCH_DATE;
    }

    public String getSCH_MCS() {
        return SCH_MCS;
    }

    public void setSCH_MCS(String SCH_MCS) {
        this.SCH_MCS = SCH_MCS;
    }

    public String getSCH_RAW1() {
        return SCH_RAW1;
    }

    public void setSCH_RAW1(String SCH_RAW1) {
        this.SCH_RAW1 = SCH_RAW1;
    }

    public String getSCH_RAW2() {
        return SCH_RAW2;
    }

    public void setSCH_RAW2(String SCH_RAW2) {
        this.SCH_RAW2 = SCH_RAW2;
    }

    public String getSCH_RAW3() {
        return SCH_RAW3;
    }

    public void setSCH_RAW3(String SCH_RAW3) {
        this.SCH_RAW3 = SCH_RAW3;
    }

    public String getSCH_RAW4() {
        return SCH_RAW4;
    }

    public void setSCH_RAW4(String SCH_RAW4) {
        this.SCH_RAW4 = SCH_RAW4;
    }

    public String getSCH_RAW5() {
        return SCH_RAW5;
    }

    public void setSCH_RAW5(String SCH_RAW5) {
        this.SCH_RAW5 = SCH_RAW5;
    }

    public String getSCH_RAW6() {
        return SCH_RAW6;
    }

    public void setSCH_RAW6(String SCH_RAW6) {
        this.SCH_RAW6 = SCH_RAW6;
    }

    public String getSCH_RAW7() {
        return SCH_RAW7;
    }

    public void setSCH_RAW7(String SCH_RAW7) {
        this.SCH_RAW7 = SCH_RAW7;
    }

    public String getSCH_RAW8() {
        return SCH_RAW8;
    }

    public void setSCH_RAW8(String SCH_RAW8) {
        this.SCH_RAW8 = SCH_RAW8;
    }

    public String getSCH_RAW9() {
        return SCH_RAW9;
    }

    public void setSCH_RAW9(String SCH_RAW9) {
        this.SCH_RAW9 = SCH_RAW9;
    }

    public String getSCH_RAW10() {
        return SCH_RAW10;
    }

    public void setSCH_RAW10(String SCH_RAW10) {
        this.SCH_RAW10 = SCH_RAW10;
    }

    public String getSCH_CHEM1() {
        return SCH_CHEM1;
    }

    public void setSCH_CHEM1(String SCH_CHEM1) {
        this.SCH_CHEM1 = SCH_CHEM1;
    }

    public String getSCH_CHEM2() {
        return SCH_CHEM2;
    }

    public void setSCH_CHEM2(String SCH_CHEM2) {
        this.SCH_CHEM2 = SCH_CHEM2;
    }

    public String getSCH_CHEM3() {
        return SCH_CHEM3;
    }

    public void setSCH_CHEM3(String SCH_CHEM3) {
        this.SCH_CHEM3 = SCH_CHEM3;
    }

    public String getSCH_CHEM4() {
        return SCH_CHEM4;
    }

    public void setSCH_CHEM4(String SCH_CHEM4) {
        this.SCH_CHEM4 = SCH_CHEM4;
    }

    public String getSCH_CHEM5() {
        return SCH_CHEM5;
    }

    public void setSCH_CHEM5(String SCH_CHEM5) {
        this.SCH_CHEM5 = SCH_CHEM5;
    }

    public String getSCH_CHEM6() {
        return SCH_CHEM6;
    }

    public void setSCH_CHEM6(String SCH_CHEM6) {
        this.SCH_CHEM6 = SCH_CHEM6;
    }

    public String getSCH_CHEM7() {
        return SCH_CHEM7;
    }

    public void setSCH_CHEM7(String SCH_CHEM7) {
        this.SCH_CHEM7 = SCH_CHEM7;
    }

    public String getSCH_CHEM8() {
        return SCH_CHEM8;
    }

    public void setSCH_CHEM8(String SCH_CHEM8) {
        this.SCH_CHEM8 = SCH_CHEM8;
    }

    public String getSCH_CHEM9() {
        return SCH_CHEM9;
    }

    public void setSCH_CHEM9(String SCH_CHEM9) {
        this.SCH_CHEM9 = SCH_CHEM9;
    }

    public String getSCH_CHEM10() {
        return SCH_CHEM10;
    }

    public void setSCH_CHEM10(String SCH_CHEM10) {
        this.SCH_CHEM10 = SCH_CHEM10;
    }

    public String getSCH_CHEM11() {
        return SCH_CHEM11;
    }

    public void setSCH_CHEM11(String SCH_CHEM11) {
        this.SCH_CHEM11 = SCH_CHEM11;
    }

    public String getSCH_CHEM12() {
        return SCH_CHEM12;
    }

    public void setSCH_CHEM12(String SCH_CHEM12) {
        this.SCH_CHEM12 = SCH_CHEM12;
    }

    public String getSCH_CHEM13() {
        return SCH_CHEM13;
    }

    public void setSCH_CHEM13(String SCH_CHEM13) {
        this.SCH_CHEM13 = SCH_CHEM13;
    }

    public String getSCH_CHEM14() {
        return SCH_CHEM14;
    }

    public void setSCH_CHEM14(String SCH_CHEM14) {
        this.SCH_CHEM14 = SCH_CHEM14;
    }

    public String getSCH_CHEM15() {
        return SCH_CHEM15;
    }

    public void setSCH_CHEM15(String SCH_CHEM15) {
        this.SCH_CHEM15 = SCH_CHEM15;
    }

    public String getSCH_CHEM16() {
        return SCH_CHEM16;
    }

    public void setSCH_CHEM16(String SCH_CHEM16) {
        this.SCH_CHEM16 = SCH_CHEM16;
    }

    public String getSCH_CHEM17() {
        return SCH_CHEM17;
    }

    public void setSCH_CHEM17(String SCH_CHEM17) {
        this.SCH_CHEM17 = SCH_CHEM17;
    }

    public String getSCH_CHEM18() {
        return SCH_CHEM18;
    }

    public void setSCH_CHEM18(String SCH_CHEM18) {
        this.SCH_CHEM18 = SCH_CHEM18;
    }

    public String getSCH_CHEM19() {
        return SCH_CHEM19;
    }

    public void setSCH_CHEM19(String SCH_CHEM19) {
        this.SCH_CHEM19 = SCH_CHEM19;
    }

    public String getSCH_CHEM20() {
        return SCH_CHEM20;
    }

    public void setSCH_CHEM20(String SCH_CHEM20) {
        this.SCH_CHEM20 = SCH_CHEM20;
    }

    public String getSCH_PIG1() {
        return SCH_PIG1;
    }

    public void setSCH_PIG1(String SCH_PIG1) {
        this.SCH_PIG1 = SCH_PIG1;
    }

    public String getSCH_PIG2() {
        return SCH_PIG2;
    }

    public void setSCH_PIG2(String SCH_PIG2) {
        this.SCH_PIG2 = SCH_PIG2;
    }

    public String getSCH_PIG3() {
        return SCH_PIG3;
    }

    public void setSCH_PIG3(String SCH_PIG3) {
        this.SCH_PIG3 = SCH_PIG3;
    }

    public String getSCH_PIG4() {
        return SCH_PIG4;
    }

    public void setSCH_PIG4(String SCH_PIG4) {
        this.SCH_PIG4 = SCH_PIG4;
    }

    public String getSCH_PIG5() {
        return SCH_PIG5;
    }

    public void setSCH_PIG5(String SCH_PIG5) {
        this.SCH_PIG5 = SCH_PIG5;
    }

    public String getSCH_PIG6() {
        return SCH_PIG6;
    }

    public void setSCH_PIG6(String SCH_PIG6) {
        this.SCH_PIG6 = SCH_PIG6;
    }

    public String getSCH_PIG7() {
        return SCH_PIG7;
    }

    public void setSCH_PIG7(String SCH_PIG7) {
        this.SCH_PIG7 = SCH_PIG7;
    }

    public String getSCH_PIG8() {
        return SCH_PIG8;
    }

    public void setSCH_PIG8(String SCH_PIG8) {
        this.SCH_PIG8 = SCH_PIG8;
    }

    public String getSCH_PIG9() {
        return SCH_PIG9;
    }

    public void setSCH_PIG9(String SCH_PIG9) {
        this.SCH_PIG9 = SCH_PIG9;
    }

    public String getSCH_PIG10() {
        return SCH_PIG10;
    }

    public void setSCH_PIG10(String SCH_PIG10) {
        this.SCH_PIG10 = SCH_PIG10;
    }

    public String getSCH_WRAW1() {
        return SCH_WRAW1;
    }

    public void setSCH_WRAW1(String SCH_WRAW1) {
        this.SCH_WRAW1 = SCH_WRAW1;
    }

    public String getSCH_WRAW2() {
        return SCH_WRAW2;
    }

    public void setSCH_WRAW2(String SCH_WRAW2) {
        this.SCH_WRAW2 = SCH_WRAW2;
    }

    public String getSCH_WRAW3() {
        return SCH_WRAW3;
    }

    public void setSCH_WRAW3(String SCH_WRAW3) {
        this.SCH_WRAW3 = SCH_WRAW3;
    }

    public String getSCH_WRAW4() {
        return SCH_WRAW4;
    }

    public void setSCH_WRAW4(String SCH_WRAW4) {
        this.SCH_WRAW4 = SCH_WRAW4;
    }

    public String getSCH_WRAW5() {
        return SCH_WRAW5;
    }

    public void setSCH_WRAW5(String SCH_WRAW5) {
        this.SCH_WRAW5 = SCH_WRAW5;
    }

    public String getSCH_WRAW6() {
        return SCH_WRAW6;
    }

    public void setSCH_WRAW6(String SCH_WRAW6) {
        this.SCH_WRAW6 = SCH_WRAW6;
    }

    public String getSCH_WRAW7() {
        return SCH_WRAW7;
    }

    public void setSCH_WRAW7(String SCH_WRAW7) {
        this.SCH_WRAW7 = SCH_WRAW7;
    }

    public String getSCH_WRAW8() {
        return SCH_WRAW8;
    }

    public void setSCH_WRAW8(String SCH_WRAW8) {
        this.SCH_WRAW8 = SCH_WRAW8;
    }

    public String getSCH_WRAW9() {
        return SCH_WRAW9;
    }

    public void setSCH_WRAW9(String SCH_WRAW9) {
        this.SCH_WRAW9 = SCH_WRAW9;
    }

    public String getSCH_WRAW10() {
        return SCH_WRAW10;
    }

    public void setSCH_WRAW10(String SCH_WRAW10) {
        this.SCH_WRAW10 = SCH_WRAW10;
    }

    public String getSCH_WCHEM1() {
        return SCH_WCHEM1;
    }

    public void setSCH_WCHEM1(String SCH_WCHEM1) {
        this.SCH_WCHEM1 = SCH_WCHEM1;
    }

    public String getSCH_WCHEM2() {
        return SCH_WCHEM2;
    }

    public void setSCH_WCHEM2(String SCH_WCHEM2) {
        this.SCH_WCHEM2 = SCH_WCHEM2;
    }

    public String getSCH_WCHEM3() {
        return SCH_WCHEM3;
    }

    public void setSCH_WCHEM3(String SCH_WCHEM3) {
        this.SCH_WCHEM3 = SCH_WCHEM3;
    }

    public String getSCH_WCHEM4() {
        return SCH_WCHEM4;
    }

    public void setSCH_WCHEM4(String SCH_WCHEM4) {
        this.SCH_WCHEM4 = SCH_WCHEM4;
    }

    public String getSCH_WCHEM5() {
        return SCH_WCHEM5;
    }

    public void setSCH_WCHEM5(String SCH_WCHEM5) {
        this.SCH_WCHEM5 = SCH_WCHEM5;
    }

    public String getSCH_WCHEM6() {
        return SCH_WCHEM6;
    }

    public void setSCH_WCHEM6(String SCH_WCHEM6) {
        this.SCH_WCHEM6 = SCH_WCHEM6;
    }

    public String getSCH_WCHEM7() {
        return SCH_WCHEM7;
    }

    public void setSCH_WCHEM7(String SCH_WCHEM7) {
        this.SCH_WCHEM7 = SCH_WCHEM7;
    }

    public String getSCH_WCHEM8() {
        return SCH_WCHEM8;
    }

    public void setSCH_WCHEM8(String SCH_WCHEM8) {
        this.SCH_WCHEM8 = SCH_WCHEM8;
    }

    public String getSCH_WCHEM9() {
        return SCH_WCHEM9;
    }

    public void setSCH_WCHEM9(String SCH_WCHEM9) {
        this.SCH_WCHEM9 = SCH_WCHEM9;
    }

    public String getSCH_WCHEM10() {
        return SCH_WCHEM10;
    }

    public void setSCH_WCHEM10(String SCH_WCHEM10) {
        this.SCH_WCHEM10 = SCH_WCHEM10;
    }

    public String getSCH_WCHEM11() {
        return SCH_WCHEM11;
    }

    public void setSCH_WCHEM11(String SCH_WCHEM11) {
        this.SCH_WCHEM11 = SCH_WCHEM11;
    }

    public String getSCH_WCHEM12() {
        return SCH_WCHEM12;
    }

    public void setSCH_WCHEM12(String SCH_WCHEM12) {
        this.SCH_WCHEM12 = SCH_WCHEM12;
    }

    public String getSCH_WCHEM13() {
        return SCH_WCHEM13;
    }

    public void setSCH_WCHEM13(String SCH_WCHEM13) {
        this.SCH_WCHEM13 = SCH_WCHEM13;
    }

    public String getSCH_WCHEM14() {
        return SCH_WCHEM14;
    }

    public void setSCH_WCHEM14(String SCH_WCHEM14) {
        this.SCH_WCHEM14 = SCH_WCHEM14;
    }

    public String getSCH_WCHEM15() {
        return SCH_WCHEM15;
    }

    public void setSCH_WCHEM15(String SCH_WCHEM15) {
        this.SCH_WCHEM15 = SCH_WCHEM15;
    }

    public String getSCH_WCHEM16() {
        return SCH_WCHEM16;
    }

    public void setSCH_WCHEM16(String SCH_WCHEM16) {
        this.SCH_WCHEM16 = SCH_WCHEM16;
    }

    public String getSCH_WCHEM17() {
        return SCH_WCHEM17;
    }

    public void setSCH_WCHEM17(String SCH_WCHEM17) {
        this.SCH_WCHEM17 = SCH_WCHEM17;
    }

    public String getSCH_WCHEM18() {
        return SCH_WCHEM18;
    }

    public void setSCH_WCHEM18(String SCH_WCHEM18) {
        this.SCH_WCHEM18 = SCH_WCHEM18;
    }

    public String getSCH_WCHEM19() {
        return SCH_WCHEM19;
    }

    public void setSCH_WCHEM19(String SCH_WCHEM19) {
        this.SCH_WCHEM19 = SCH_WCHEM19;
    }

    public String getSCH_WCHEM20() {
        return SCH_WCHEM20;
    }

    public void setSCH_WCHEM20(String SCH_WCHEM20) {
        this.SCH_WCHEM20 = SCH_WCHEM20;
    }

    public String getSCH_WPIG1() {
        return SCH_WPIG1;
    }

    public void setSCH_WPIG1(String SCH_WPIG1) {
        this.SCH_WPIG1 = SCH_WPIG1;
    }

    public String getSCH_WPIG2() {
        return SCH_WPIG2;
    }

    public void setSCH_WPIG2(String SCH_WPIG2) {
        this.SCH_WPIG2 = SCH_WPIG2;
    }

    public String getSCH_WPIG3() {
        return SCH_WPIG3;
    }

    public void setSCH_WPIG3(String SCH_WPIG3) {
        this.SCH_WPIG3 = SCH_WPIG3;
    }

    public String getSCH_WPIG4() {
        return SCH_WPIG4;
    }

    public void setSCH_WPIG4(String SCH_WPIG4) {
        this.SCH_WPIG4 = SCH_WPIG4;
    }

    public String getSCH_WPIG5() {
        return SCH_WPIG5;
    }

    public void setSCH_WPIG5(String SCH_WPIG5) {
        this.SCH_WPIG5 = SCH_WPIG5;
    }

    public String getSCH_WPIG6() {
        return SCH_WPIG6;
    }

    public void setSCH_WPIG6(String SCH_WPIG6) {
        this.SCH_WPIG6 = SCH_WPIG6;
    }

    public String getSCH_WPIG7() {
        return SCH_WPIG7;
    }

    public void setSCH_WPIG7(String SCH_WPIG7) {
        this.SCH_WPIG7 = SCH_WPIG7;
    }

    public String getSCH_WPIG8() {
        return SCH_WPIG8;
    }

    public void setSCH_WPIG8(String SCH_WPIG8) {
        this.SCH_WPIG8 = SCH_WPIG8;
    }

    public String getSCH_WPIG9() {
        return SCH_WPIG9;
    }

    public void setSCH_WPIG9(String SCH_WPIG9) {
        this.SCH_WPIG9 = SCH_WPIG9;
    }

    public String getSCH_WPIG10() {
        return SCH_WPIG10;
    }

    public void setSCH_WPIG10(String SCH_WPIG10) {
        this.SCH_WPIG10 = SCH_WPIG10;
    }
}