package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class QAM_SCALE_MAC2 implements Serializable {

    private String SC2_SHIFT;
    private String SC2_DATE;
    private String SC2_MCS;
    private String KEY;

    private String SC2_RAW1;
    private String SC2_RAW2;
    private String SC2_RAW3;
    private String SC2_RAW4;
    private String SC2_RAW5;
    private String SC2_RAW6;
    private String SC2_RAW7;
    private String SC2_RAW8;
    private String SC2_RAW9;
    private String SC2_RAW10;
    private String SC2_CHEM1;
    private String SC2_CHEM2;
    private String SC2_CHEM3;
    private String SC2_CHEM4;
    private String SC2_CHEM5;
    private String SC2_CHEM6;
    private String SC2_CHEM7;
    private String SC2_CHEM8;
    private String SC2_CHEM9;
    private String SC2_CHEM10;
    private String SC2_CHEM11;
    private String SC2_CHEM12;
    private String SC2_CHEM13;
    private String SC2_CHEM14;
    private String SC2_CHEM15;
    private String SC2_CHEM16;
    private String SC2_CHEM17;
    private String SC2_CHEM18;
    private String SC2_CHEM19;
    private String SC2_CHEM20;
    private String SC2_PIG1;
    private String SC2_PIG2;
    private String SC2_PIG3;
    private String SC2_PIG4;
    private String SC2_PIG5;
    private String SC2_PIG6;
    private String SC2_PIG7;
    private String SC2_PIG8;
    private String SC2_PIG9;
    private String SC2_PIG10;
    private String SC2_WRAW1;
    private String SC2_WRAW2;
    private String SC2_WRAW3;
    private String SC2_WRAW4;
    private String SC2_WRAW5;
    private String SC2_WRAW6;
    private String SC2_WRAW7;
    private String SC2_WRAW8;
    private String SC2_WRAW9;
    private String SC2_WRAW10;
    private String SC2_WCHEM1;
    private String SC2_WCHEM2;
    private String SC2_WCHEM3;
    private String SC2_WCHEM4;
    private String SC2_WCHEM5;
    private String SC2_WCHEM6;
    private String SC2_WCHEM7;
    private String SC2_WCHEM8;
    private String SC2_WCHEM9;
    private String SC2_WCHEM10;
    private String SC2_WCHEM11;
    private String SC2_WCHEM12;
    private String SC2_WCHEM13;
    private String SC2_WCHEM14;
    private String SC2_WCHEM15;
    private String SC2_WCHEM16;
    private String SC2_WCHEM17;
    private String SC2_WCHEM18;
    private String SC2_WCHEM19;
    private String SC2_WCHEM20;
    private String SC2_WPIG1;
    private String SC2_WPIG2;
    private String SC2_WPIG3;
    private String SC2_WPIG4;
    private String SC2_WPIG5;
    private String SC2_WPIG6;
    private String SC2_WPIG7;
    private String SC2_WPIG8;
    private String SC2_WPIG9;
    private String SC2_WPIG10;

    private String MAC2_ID;

    public String getKEY() {
        return KEY;
    }

    public void setKEY(String KEY) {
        this.KEY = KEY;
    }

    public String getSC2_SHIFT() {
        return SC2_SHIFT;
    }

    public void setSC2_SHIFT(String SC2_SHIFT) {
        this.SC2_SHIFT = SC2_SHIFT;
    }

    public String getSC2_DATE() {
        return SC2_DATE;
    }

    public void setSC2_DATE(String SC2_DATE) {
        this.SC2_DATE = SC2_DATE;
    }

    public String getSC2_MCS() {
        return SC2_MCS;
    }

    public void setSC2_MCS(String SC2_MCS) {
        this.SC2_MCS = SC2_MCS;
    }

    public String getSC2_RAW1() {
        return SC2_RAW1;
    }

    public void setSC2_RAW1(String SC2_RAW1) {
        this.SC2_RAW1 = SC2_RAW1;
    }

    public String getSC2_RAW2() {
        return SC2_RAW2;
    }

    public void setSC2_RAW2(String SC2_RAW2) {
        this.SC2_RAW2 = SC2_RAW2;
    }

    public String getSC2_RAW3() {
        return SC2_RAW3;
    }

    public void setSC2_RAW3(String SC2_RAW3) {
        this.SC2_RAW3 = SC2_RAW3;
    }

    public String getSC2_RAW4() {
        return SC2_RAW4;
    }

    public void setSC2_RAW4(String SC2_RAW4) {
        this.SC2_RAW4 = SC2_RAW4;
    }

    public String getSC2_RAW5() {
        return SC2_RAW5;
    }

    public void setSC2_RAW5(String SC2_RAW5) {
        this.SC2_RAW5 = SC2_RAW5;
    }

    public String getSC2_RAW6() {
        return SC2_RAW6;
    }

    public void setSC2_RAW6(String SC2_RAW6) {
        this.SC2_RAW6 = SC2_RAW6;
    }

    public String getSC2_RAW7() {
        return SC2_RAW7;
    }

    public void setSC2_RAW7(String SC2_RAW7) {
        this.SC2_RAW7 = SC2_RAW7;
    }

    public String getSC2_RAW8() {
        return SC2_RAW8;
    }

    public void setSC2_RAW8(String SC2_RAW8) {
        this.SC2_RAW8 = SC2_RAW8;
    }

    public String getSC2_RAW9() {
        return SC2_RAW9;
    }

    public void setSC2_RAW9(String SC2_RAW9) {
        this.SC2_RAW9 = SC2_RAW9;
    }

    public String getSC2_RAW10() {
        return SC2_RAW10;
    }

    public void setSC2_RAW10(String SC2_RAW10) {
        this.SC2_RAW10 = SC2_RAW10;
    }

    public String getSC2_CHEM1() {
        return SC2_CHEM1;
    }

    public void setSC2_CHEM1(String SC2_CHEM1) {
        this.SC2_CHEM1 = SC2_CHEM1;
    }

    public String getSC2_CHEM2() {
        return SC2_CHEM2;
    }

    public void setSC2_CHEM2(String SC2_CHEM2) {
        this.SC2_CHEM2 = SC2_CHEM2;
    }

    public String getSC2_CHEM3() {
        return SC2_CHEM3;
    }

    public void setSC2_CHEM3(String SC2_CHEM3) {
        this.SC2_CHEM3 = SC2_CHEM3;
    }

    public String getSC2_CHEM4() {
        return SC2_CHEM4;
    }

    public void setSC2_CHEM4(String SC2_CHEM4) {
        this.SC2_CHEM4 = SC2_CHEM4;
    }

    public String getSC2_CHEM5() {
        return SC2_CHEM5;
    }

    public void setSC2_CHEM5(String SC2_CHEM5) {
        this.SC2_CHEM5 = SC2_CHEM5;
    }

    public String getSC2_CHEM6() {
        return SC2_CHEM6;
    }

    public void setSC2_CHEM6(String SC2_CHEM6) {
        this.SC2_CHEM6 = SC2_CHEM6;
    }

    public String getSC2_CHEM7() {
        return SC2_CHEM7;
    }

    public void setSC2_CHEM7(String SC2_CHEM7) {
        this.SC2_CHEM7 = SC2_CHEM7;
    }

    public String getSC2_CHEM8() {
        return SC2_CHEM8;
    }

    public void setSC2_CHEM8(String SC2_CHEM8) {
        this.SC2_CHEM8 = SC2_CHEM8;
    }

    public String getSC2_CHEM9() {
        return SC2_CHEM9;
    }

    public void setSC2_CHEM9(String SC2_CHEM9) {
        this.SC2_CHEM9 = SC2_CHEM9;
    }

    public String getSC2_CHEM10() {
        return SC2_CHEM10;
    }

    public void setSC2_CHEM10(String SC2_CHEM10) {
        this.SC2_CHEM10 = SC2_CHEM10;
    }

    public String getSC2_CHEM11() {
        return SC2_CHEM11;
    }

    public void setSC2_CHEM11(String SC2_CHEM11) {
        this.SC2_CHEM11 = SC2_CHEM11;
    }

    public String getSC2_CHEM12() {
        return SC2_CHEM12;
    }

    public void setSC2_CHEM12(String SC2_CHEM12) {
        this.SC2_CHEM12 = SC2_CHEM12;
    }

    public String getSC2_CHEM13() {
        return SC2_CHEM13;
    }

    public void setSC2_CHEM13(String SC2_CHEM13) {
        this.SC2_CHEM13 = SC2_CHEM13;
    }

    public String getSC2_CHEM14() {
        return SC2_CHEM14;
    }

    public void setSC2_CHEM14(String SC2_CHEM14) {
        this.SC2_CHEM14 = SC2_CHEM14;
    }

    public String getSC2_CHEM15() {
        return SC2_CHEM15;
    }

    public void setSC2_CHEM15(String SC2_CHEM15) {
        this.SC2_CHEM15 = SC2_CHEM15;
    }

    public String getSC2_CHEM16() {
        return SC2_CHEM16;
    }

    public void setSC2_CHEM16(String SC2_CHEM16) {
        this.SC2_CHEM16 = SC2_CHEM16;
    }

    public String getSC2_CHEM17() {
        return SC2_CHEM17;
    }

    public void setSC2_CHEM17(String SC2_CHEM17) {
        this.SC2_CHEM17 = SC2_CHEM17;
    }

    public String getSC2_CHEM18() {
        return SC2_CHEM18;
    }

    public void setSC2_CHEM18(String SC2_CHEM18) {
        this.SC2_CHEM18 = SC2_CHEM18;
    }

    public String getSC2_CHEM19() {
        return SC2_CHEM19;
    }

    public void setSC2_CHEM19(String SC2_CHEM19) {
        this.SC2_CHEM19 = SC2_CHEM19;
    }

    public String getSC2_CHEM20() {
        return SC2_CHEM20;
    }

    public void setSC2_CHEM20(String SC2_CHEM20) {
        this.SC2_CHEM20 = SC2_CHEM20;
    }

    public String getSC2_PIG1() {
        return SC2_PIG1;
    }

    public void setSC2_PIG1(String SC2_PIG1) {
        this.SC2_PIG1 = SC2_PIG1;
    }

    public String getSC2_PIG2() {
        return SC2_PIG2;
    }

    public void setSC2_PIG2(String SC2_PIG2) {
        this.SC2_PIG2 = SC2_PIG2;
    }

    public String getSC2_PIG3() {
        return SC2_PIG3;
    }

    public void setSC2_PIG3(String SC2_PIG3) {
        this.SC2_PIG3 = SC2_PIG3;
    }

    public String getSC2_PIG4() {
        return SC2_PIG4;
    }

    public void setSC2_PIG4(String SC2_PIG4) {
        this.SC2_PIG4 = SC2_PIG4;
    }

    public String getSC2_PIG5() {
        return SC2_PIG5;
    }

    public void setSC2_PIG5(String SC2_PIG5) {
        this.SC2_PIG5 = SC2_PIG5;
    }

    public String getSC2_PIG6() {
        return SC2_PIG6;
    }

    public void setSC2_PIG6(String SC2_PIG6) {
        this.SC2_PIG6 = SC2_PIG6;
    }

    public String getSC2_PIG7() {
        return SC2_PIG7;
    }

    public void setSC2_PIG7(String SC2_PIG7) {
        this.SC2_PIG7 = SC2_PIG7;
    }

    public String getSC2_PIG8() {
        return SC2_PIG8;
    }

    public void setSC2_PIG8(String SC2_PIG8) {
        this.SC2_PIG8 = SC2_PIG8;
    }

    public String getSC2_PIG9() {
        return SC2_PIG9;
    }

    public void setSC2_PIG9(String SC2_PIG9) {
        this.SC2_PIG9 = SC2_PIG9;
    }

    public String getSC2_PIG10() {
        return SC2_PIG10;
    }

    public void setSC2_PIG10(String SC2_PIG10) {
        this.SC2_PIG10 = SC2_PIG10;
    }

    public String getSC2_WRAW1() {
        return SC2_WRAW1;
    }

    public void setSC2_WRAW1(String SC2_WRAW1) {
        this.SC2_WRAW1 = SC2_WRAW1;
    }

    public String getSC2_WRAW2() {
        return SC2_WRAW2;
    }

    public void setSC2_WRAW2(String SC2_WRAW2) {
        this.SC2_WRAW2 = SC2_WRAW2;
    }

    public String getSC2_WRAW3() {
        return SC2_WRAW3;
    }

    public void setSC2_WRAW3(String SC2_WRAW3) {
        this.SC2_WRAW3 = SC2_WRAW3;
    }

    public String getSC2_WRAW4() {
        return SC2_WRAW4;
    }

    public void setSC2_WRAW4(String SC2_WRAW4) {
        this.SC2_WRAW4 = SC2_WRAW4;
    }

    public String getSC2_WRAW5() {
        return SC2_WRAW5;
    }

    public void setSC2_WRAW5(String SC2_WRAW5) {
        this.SC2_WRAW5 = SC2_WRAW5;
    }

    public String getSC2_WRAW6() {
        return SC2_WRAW6;
    }

    public void setSC2_WRAW6(String SC2_WRAW6) {
        this.SC2_WRAW6 = SC2_WRAW6;
    }

    public String getSC2_WRAW7() {
        return SC2_WRAW7;
    }

    public void setSC2_WRAW7(String SC2_WRAW7) {
        this.SC2_WRAW7 = SC2_WRAW7;
    }

    public String getSC2_WRAW8() {
        return SC2_WRAW8;
    }

    public void setSC2_WRAW8(String SC2_WRAW8) {
        this.SC2_WRAW8 = SC2_WRAW8;
    }

    public String getSC2_WRAW9() {
        return SC2_WRAW9;
    }

    public void setSC2_WRAW9(String SC2_WRAW9) {
        this.SC2_WRAW9 = SC2_WRAW9;
    }

    public String getSC2_WRAW10() {
        return SC2_WRAW10;
    }

    public void setSC2_WRAW10(String SC2_WRAW10) {
        this.SC2_WRAW10 = SC2_WRAW10;
    }

    public String getSC2_WCHEM1() {
        return SC2_WCHEM1;
    }

    public void setSC2_WCHEM1(String SC2_WCHEM1) {
        this.SC2_WCHEM1 = SC2_WCHEM1;
    }

    public String getSC2_WCHEM2() {
        return SC2_WCHEM2;
    }

    public void setSC2_WCHEM2(String SC2_WCHEM2) {
        this.SC2_WCHEM2 = SC2_WCHEM2;
    }

    public String getSC2_WCHEM3() {
        return SC2_WCHEM3;
    }

    public void setSC2_WCHEM3(String SC2_WCHEM3) {
        this.SC2_WCHEM3 = SC2_WCHEM3;
    }

    public String getSC2_WCHEM4() {
        return SC2_WCHEM4;
    }

    public void setSC2_WCHEM4(String SC2_WCHEM4) {
        this.SC2_WCHEM4 = SC2_WCHEM4;
    }

    public String getSC2_WCHEM5() {
        return SC2_WCHEM5;
    }

    public void setSC2_WCHEM5(String SC2_WCHEM5) {
        this.SC2_WCHEM5 = SC2_WCHEM5;
    }

    public String getSC2_WCHEM6() {
        return SC2_WCHEM6;
    }

    public void setSC2_WCHEM6(String SC2_WCHEM6) {
        this.SC2_WCHEM6 = SC2_WCHEM6;
    }

    public String getSC2_WCHEM7() {
        return SC2_WCHEM7;
    }

    public void setSC2_WCHEM7(String SC2_WCHEM7) {
        this.SC2_WCHEM7 = SC2_WCHEM7;
    }

    public String getSC2_WCHEM8() {
        return SC2_WCHEM8;
    }

    public void setSC2_WCHEM8(String SC2_WCHEM8) {
        this.SC2_WCHEM8 = SC2_WCHEM8;
    }

    public String getSC2_WCHEM9() {
        return SC2_WCHEM9;
    }

    public void setSC2_WCHEM9(String SC2_WCHEM9) {
        this.SC2_WCHEM9 = SC2_WCHEM9;
    }

    public String getSC2_WCHEM10() {
        return SC2_WCHEM10;
    }

    public void setSC2_WCHEM10(String SC2_WCHEM10) {
        this.SC2_WCHEM10 = SC2_WCHEM10;
    }

    public String getSC2_WCHEM11() {
        return SC2_WCHEM11;
    }

    public void setSC2_WCHEM11(String SC2_WCHEM11) {
        this.SC2_WCHEM11 = SC2_WCHEM11;
    }

    public String getSC2_WCHEM12() {
        return SC2_WCHEM12;
    }

    public void setSC2_WCHEM12(String SC2_WCHEM12) {
        this.SC2_WCHEM12 = SC2_WCHEM12;
    }

    public String getSC2_WCHEM13() {
        return SC2_WCHEM13;
    }

    public void setSC2_WCHEM13(String SC2_WCHEM13) {
        this.SC2_WCHEM13 = SC2_WCHEM13;
    }

    public String getSC2_WCHEM14() {
        return SC2_WCHEM14;
    }

    public void setSC2_WCHEM14(String SC2_WCHEM14) {
        this.SC2_WCHEM14 = SC2_WCHEM14;
    }

    public String getSC2_WCHEM15() {
        return SC2_WCHEM15;
    }

    public void setSC2_WCHEM15(String SC2_WCHEM15) {
        this.SC2_WCHEM15 = SC2_WCHEM15;
    }

    public String getSC2_WCHEM16() {
        return SC2_WCHEM16;
    }

    public void setSC2_WCHEM16(String SC2_WCHEM16) {
        this.SC2_WCHEM16 = SC2_WCHEM16;
    }

    public String getSC2_WCHEM17() {
        return SC2_WCHEM17;
    }

    public void setSC2_WCHEM17(String SC2_WCHEM17) {
        this.SC2_WCHEM17 = SC2_WCHEM17;
    }

    public String getSC2_WCHEM18() {
        return SC2_WCHEM18;
    }

    public void setSC2_WCHEM18(String SC2_WCHEM18) {
        this.SC2_WCHEM18 = SC2_WCHEM18;
    }

    public String getSC2_WCHEM19() {
        return SC2_WCHEM19;
    }

    public void setSC2_WCHEM19(String SC2_WCHEM19) {
        this.SC2_WCHEM19 = SC2_WCHEM19;
    }

    public String getSC2_WCHEM20() {
        return SC2_WCHEM20;
    }

    public void setSC2_WCHEM20(String SC2_WCHEM20) {
        this.SC2_WCHEM20 = SC2_WCHEM20;
    }

    public String getSC2_WPIG1() {
        return SC2_WPIG1;
    }

    public void setSC2_WPIG1(String SC2_WPIG1) {
        this.SC2_WPIG1 = SC2_WPIG1;
    }

    public String getSC2_WPIG2() {
        return SC2_WPIG2;
    }

    public void setSC2_WPIG2(String SC2_WPIG2) {
        this.SC2_WPIG2 = SC2_WPIG2;
    }

    public String getSC2_WPIG3() {
        return SC2_WPIG3;
    }

    public void setSC2_WPIG3(String SC2_WPIG3) {
        this.SC2_WPIG3 = SC2_WPIG3;
    }

    public String getSC2_WPIG4() {
        return SC2_WPIG4;
    }

    public void setSC2_WPIG4(String SC2_WPIG4) {
        this.SC2_WPIG4 = SC2_WPIG4;
    }

    public String getSC2_WPIG5() {
        return SC2_WPIG5;
    }

    public void setSC2_WPIG5(String SC2_WPIG5) {
        this.SC2_WPIG5 = SC2_WPIG5;
    }

    public String getSC2_WPIG6() {
        return SC2_WPIG6;
    }

    public void setSC2_WPIG6(String SC2_WPIG6) {
        this.SC2_WPIG6 = SC2_WPIG6;
    }

    public String getSC2_WPIG7() {
        return SC2_WPIG7;
    }

    public void setSC2_WPIG7(String SC2_WPIG7) {
        this.SC2_WPIG7 = SC2_WPIG7;
    }

    public String getSC2_WPIG8() {
        return SC2_WPIG8;
    }

    public void setSC2_WPIG8(String SC2_WPIG8) {
        this.SC2_WPIG8 = SC2_WPIG8;
    }

    public String getSC2_WPIG9() {
        return SC2_WPIG9;
    }

    public void setSC2_WPIG9(String SC2_WPIG9) {
        this.SC2_WPIG9 = SC2_WPIG9;
    }

    public String getSC2_WPIG10() {
        return SC2_WPIG10;
    }

    public void setSC2_WPIG10(String SC2_WPIG10) {
        this.SC2_WPIG10 = SC2_WPIG10;
    }

    public String getMAC2_ID() {
        return MAC2_ID;
    }

    public void setMAC2_ID(String MAC2_ID) {
        this.MAC2_ID = MAC2_ID;
    }
}