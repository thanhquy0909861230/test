package com.deanshoes.AppQAM.model.QAM;

public class RP_MAC {
    private String INS_NO;
    private String MCS;
    private String STYLE;
    private String COLOR;
    private String HARD;

    private String MAC_MTR1;
    private String MAC_MTR2;
    private String MAC_MTR3;
    private String MAC_MTR4;
    private String MAC_MTR5;
    private String MAC_MTR6;
    private String MAC_MTR7;
    private String MAC_MTR8;
    private String MAC_MTR9;
    private String MAC_MTR10;

    private String MAC_CHEM1;
    private String MAC_CHEM2;
    private String MAC_CHEM3;
    private String MAC_CHEM4;
    private String MAC_CHEM5;
    private String MAC_CHEM6;
    private String MAC_CHEM7;
    private String MAC_CHEM8;
    private String MAC_CHEM9;
    private String MAC_CHEM10;
    private String MAC_CHEM11;
    private String MAC_CHEM12;
    private String MAC_CHEM13;
    private String MAC_CHEM14;
    private String MAC_CHEM15;
    private String MAC_CHEM16;
    private String MAC_CHEM17;
    private String MAC_CHEM18;
    private String MAC_CHEM19;
    private String MAC_CHEM20;

    private String MAC_PIG1;
    private String MAC_PIG2;
    private String MAC_PIG3;
    private String MAC_PIG4;
    private String MAC_PIG5;
    private String MAC_PIG6;
    private String MAC_PIG7;
    private String MAC_PIG8;
    private String MAC_PIG9;
    private String MAC_PIG10;

    private String MAC_NO;

    private String SMAC_MTR1;
    private String SMAC_MTR2;
    private String SMAC_MTR3;
    private String SMAC_MTR4;
    private String SMAC_MTR5;
    private String SMAC_MTR6;
    private String SMAC_MTR7;
    private String SMAC_MTR8;
    private String SMAC_MTR9;
    private String SMAC_MTR10;

    private String SMAC_CHEM1;
    private String SMAC_CHEM2;
    private String SMAC_CHEM3;
    private String SMAC_CHEM4;
    private String SMAC_CHEM5;
    private String SMAC_CHEM6;
    private String SMAC_CHEM7;
    private String SMAC_CHEM8;
    private String SMAC_CHEM9;
    private String SMAC_CHEM10;
    private String SMAC_CHEM11;
    private String SMAC_CHEM12;
    private String SMAC_CHEM13;
    private String SMAC_CHEM14;
    private String SMAC_CHEM15;
    private String SMAC_CHEM16;
    private String SMAC_CHEM17;
    private String SMAC_CHEM18;
    private String SMAC_CHEM19;
    private String SMAC_CHEM20;

    private String SMAC_PIG1;
    private String SMAC_PIG2;
    private String SMAC_PIG3;
    private String SMAC_PIG4;
    private String SMAC_PIG5;
    private String SMAC_PIG6;
    private String SMAC_PIG7;
    private String SMAC_PIG8;
    private String SMAC_PIG9;
    private String SMAC_PIG10;

    public String getINS_NO() {
        return INS_NO;
    }

    public String getMCS() {
        return MCS;
    }

    public String getSTYLE() {
        return STYLE;
    }

    public String getCOLOR() {
        return COLOR;
    }

    public String getHARD() {
        return HARD;
    }

    public String getMAC_MTR1() {
        return MAC_MTR1;
    }

    public String getMAC_MTR2() {
        return MAC_MTR2;
    }

    public String getMAC_MTR3() {
        return MAC_MTR3;
    }

    public String getMAC_MTR4() {
        return MAC_MTR4;
    }

    public String getMAC_MTR5() {
        return MAC_MTR5;
    }

    public String getMAC_MTR6() {
        return MAC_MTR6;
    }

    public String getMAC_MTR7() {
        return MAC_MTR7;
    }

    public String getMAC_MTR8() {
        return MAC_MTR8;
    }

    public String getMAC_MTR9() {
        return MAC_MTR9;
    }

    public String getMAC_MTR10() {
        return MAC_MTR10;
    }

    public String getMAC_CHEM1() {
        return MAC_CHEM1;
    }

    public String getMAC_CHEM2() {
        return MAC_CHEM2;
    }

    public String getMAC_CHEM3() {
        return MAC_CHEM3;
    }

    public String getMAC_CHEM4() {
        return MAC_CHEM4;
    }

    public String getMAC_CHEM5() {
        return MAC_CHEM5;
    }

    public String getMAC_CHEM6() {
        return MAC_CHEM6;
    }

    public String getMAC_CHEM7() {
        return MAC_CHEM7;
    }

    public String getMAC_CHEM8() {
        return MAC_CHEM8;
    }

    public String getMAC_CHEM9() {
        return MAC_CHEM9;
    }

    public String getMAC_CHEM10() {
        return MAC_CHEM10;
    }

    public String getMAC_CHEM11() {
        return MAC_CHEM11;
    }

    public String getMAC_CHEM12() {
        return MAC_CHEM12;
    }

    public String getMAC_CHEM13() {
        return MAC_CHEM13;
    }

    public String getMAC_CHEM14() {
        return MAC_CHEM14;
    }

    public String getMAC_CHEM15() {
        return MAC_CHEM15;
    }

    public String getMAC_CHEM16() {
        return MAC_CHEM16;
    }

    public String getMAC_CHEM17() {
        return MAC_CHEM17;
    }

    public String getMAC_CHEM18() {
        return MAC_CHEM18;
    }

    public String getMAC_CHEM19() {
        return MAC_CHEM19;
    }

    public String getMAC_CHEM20() {
        return MAC_CHEM20;
    }

    public String getMAC_PIG1() {
        return MAC_PIG1;
    }

    public String getMAC_PIG2() {
        return MAC_PIG2;
    }

    public String getMAC_PIG3() {
        return MAC_PIG3;
    }

    public String getMAC_PIG4() {
        return MAC_PIG4;
    }

    public String getMAC_PIG5() {
        return MAC_PIG5;
    }

    public String getMAC_PIG6() {
        return MAC_PIG6;
    }

    public String getMAC_PIG7() {
        return MAC_PIG7;
    }

    public String getMAC_PIG8() {
        return MAC_PIG8;
    }

    public String getMAC_PIG9() {
        return MAC_PIG9;
    }

    public String getMAC_PIG10() {
        return MAC_PIG10;
    }

    public String getMAC_NO() {
        return MAC_NO;
    }

    public String getSMAC_MTR1() {
        return SMAC_MTR1;
    }

    public String getSMAC_MTR2() {
        return SMAC_MTR2;
    }

    public String getSMAC_MTR3() {
        return SMAC_MTR3;
    }

    public String getSMAC_MTR4() {
        return SMAC_MTR4;
    }

    public String getSMAC_MTR5() {
        return SMAC_MTR5;
    }

    public String getSMAC_MTR6() {
        return SMAC_MTR6;
    }

    public String getSMAC_MTR7() {
        return SMAC_MTR7;
    }

    public String getSMAC_MTR8() {
        return SMAC_MTR8;
    }

    public String getSMAC_MTR9() {
        return SMAC_MTR9;
    }

    public String getSMAC_MTR10() {
        return SMAC_MTR10;
    }

    public String getSMAC_CHEM1() {
        return SMAC_CHEM1;
    }

    public String getSMAC_CHEM2() {
        return SMAC_CHEM2;
    }

    public String getSMAC_CHEM3() {
        return SMAC_CHEM3;
    }

    public String getSMAC_CHEM4() {
        return SMAC_CHEM4;
    }

    public String getSMAC_CHEM5() {
        return SMAC_CHEM5;
    }

    public String getSMAC_CHEM6() {
        return SMAC_CHEM6;
    }

    public String getSMAC_CHEM7() {
        return SMAC_CHEM7;
    }

    public String getSMAC_CHEM8() {
        return SMAC_CHEM8;
    }

    public String getSMAC_CHEM9() {
        return SMAC_CHEM9;
    }

    public String getSMAC_CHEM10() {
        return SMAC_CHEM10;
    }

    public String getSMAC_CHEM11() {
        return SMAC_CHEM11;
    }

    public String getSMAC_CHEM12() {
        return SMAC_CHEM12;
    }

    public String getSMAC_CHEM13() {
        return SMAC_CHEM13;
    }

    public String getSMAC_CHEM14() {
        return SMAC_CHEM14;
    }

    public String getSMAC_CHEM15() {
        return SMAC_CHEM15;
    }

    public String getSMAC_CHEM16() {
        return SMAC_CHEM16;
    }

    public String getSMAC_CHEM17() {
        return SMAC_CHEM17;
    }

    public String getSMAC_CHEM18() {
        return SMAC_CHEM18;
    }

    public String getSMAC_CHEM19() {
        return SMAC_CHEM19;
    }

    public String getSMAC_CHEM20() {
        return SMAC_CHEM20;
    }

    public String getSMAC_PIG1() {
        return SMAC_PIG1;
    }

    public String getSMAC_PIG2() {
        return SMAC_PIG2;
    }

    public String getSMAC_PIG3() {
        return SMAC_PIG3;
    }

    public String getSMAC_PIG4() {
        return SMAC_PIG4;
    }

    public String getSMAC_PIG5() {
        return SMAC_PIG5;
    }

    public String getSMAC_PIG6() {
        return SMAC_PIG6;
    }

    public String getSMAC_PIG7() {
        return SMAC_PIG7;
    }

    public String getSMAC_PIG8() {
        return SMAC_PIG8;
    }

    public String getSMAC_PIG9() {
        return SMAC_PIG9;
    }

    public String getSMAC_PIG10() {
        return SMAC_PIG10;
    }
}
