package com.deanshoes.AppQAM.model.response;

/**
 * 回傳的相關資料Model<br>
 * 用Gson解析JSON回Model前，先繼承此Model並指定items的Model(T)，就可直接解析<br><br>
 *
 * ex
 * 準備Model：public class Fcpg54Arm extends ApiReturnModel<Fcpg54> {}
 * 解析JSON：Fcpg54Arm fcpg54Arm = gson.fromJson(json, Fcpg54Arm.class);
 *
 * @author bin.chang
 *
 */
public class ApiReturnModel<T> {

	/** 是否發生錯誤 */
	private Boolean error;
	/** 錯誤碼 */
	private Integer error_code;
	/** 錯誤簡要說明 */
	private String error_desc;
	/** 回傳的資料 */
	private T items;

	/**
	 * 取得是否發生錯誤
	 * @return the error
	 */
	public Boolean getError() {
		return error;
	}

	/**
	 * 設置是否發生錯誤
	 * @param error the error to set
	 */
	public void setError(Boolean error) {
		this.error = error;
	}

	/**
	 * 取得錯誤碼
	 * @return the error_code
	 */
	public Integer getError_code() {
		return error_code;
	}

	/**
	 * 設置錯誤碼(會一並填入預設的設置錯誤簡要說明)
	 * @param error_code the error_code to set
	 */
	public void setError_code(ErrorCode error_code) {
		this.error_code = error_code.toCode();
		this.error_desc = error_code.toDesc();
	}

	/**
	 * 取得錯誤簡要說明
	 * @return the error_desc
	 */
	public String getError_desc() {
		return error_desc;
	}

	/**
	 * 設置錯誤簡要說明
	 * @param error_desc the error_desc to set
	 */
	public void setError_desc(String error_desc) {
		this.error_desc = error_desc;
	}

	/**
	 * 取得回傳資料
	 * @return the items
	 */
	public T getItems() {
		return items;
	}

	/**
	 * 設置回傳資料
	 * @param items the items to set
	 */
	public void setItems(T items) {
		this.items = items;
	}
}
