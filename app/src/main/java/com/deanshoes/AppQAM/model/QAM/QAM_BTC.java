package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class QAM_BTC implements Serializable {
    private static final long serialVersionUID = 1L;
    private String BTC_NO;
    private String MCS_NO;
    private String BTC_STYLE;
    private String BTC_COLOR;
    private String BTC_HARD;
    private String BTC_DATE;
    private String BTC_SHIFT;
    private String BTC_LINE;
    private String BTC_BATCH_NO;
    private String MAC_NO;

    private String UP_USER;
    private String INS_NO;
    private String CHECK;
    private String ER;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getBTC_NO() {
        return BTC_NO;
    }

    public void setBTC_NO(String BTC_NO) {
        this.BTC_NO = BTC_NO;
    }

    public String getMCS_NO() {
        return MCS_NO;
    }

    public void setMCS_NO(String MCS_NO) {
        this.MCS_NO = MCS_NO;
    }

    public String getBTC_STYLE() {
        return BTC_STYLE;
    }

    public void setBTC_STYLE(String BTC_STYLE) {
        this.BTC_STYLE = BTC_STYLE;
    }

    public String getBTC_COLOR() {
        return BTC_COLOR;
    }

    public void setBTC_COLOR(String BTC_COLOR) {
        this.BTC_COLOR = BTC_COLOR;
    }

    public String getBTC_HARD() {
        return BTC_HARD;
    }

    public void setBTC_HARD(String BTC_HARD) {
        this.BTC_HARD = BTC_HARD;
    }

    public String getBTC_DATE() {
        return BTC_DATE;
    }

    public void setBTC_DATE(String BTC_DATE) {
        this.BTC_DATE = BTC_DATE;
    }

    public String getBTC_SHIFT() {
        return BTC_SHIFT;
    }

    public void setBTC_SHIFT(String BTC_SHIFT) {
        this.BTC_SHIFT = BTC_SHIFT;
    }

    public String getBTC_LINE() {
        return BTC_LINE;
    }

    public void setBTC_LINE(String BTC_LINE) {
        this.BTC_LINE = BTC_LINE;
    }

    public String getBTC_BATCH_NO() {
        return BTC_BATCH_NO;
    }

    public void setBTC_BATCH_NO(String BTC_BATCH_NO) {
        this.BTC_BATCH_NO = BTC_BATCH_NO;
    }

    public String getMAC_NO() {
        return MAC_NO;
    }

    public void setMAC_NO(String MAC_NO) {
        this.MAC_NO = MAC_NO;
    }

    public String getUP_USER() {
        return UP_USER;
    }

    public void setUP_USER(String UP_USER) {
        this.UP_USER = UP_USER;
    }

    public String getINS_NO() {
        return INS_NO;
    }

    public void setINS_NO(String INS_NO) {
        this.INS_NO = INS_NO;
    }

    public String getCHECK() {
        return CHECK;
    }

    public void setCHECK(String CHECK) {
        this.CHECK = CHECK;
    }

    public String getER() {
        return ER;
    }

    public void setER(String ER) {
        this.ER = ER;
    }
}
