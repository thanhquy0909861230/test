package com.deanshoes.AppQAM.model.QAM;

public class MCS {

    private String MSC;
    private String MSC_NO;
    private String MCS_STYLE;
    private String MCS_COLOR;
    private String MCS_HARD;
    private String ER;

    public String getER() {
        return ER;
    }

    public void setER(String ER) {
        this.ER = ER;
    }

    public String getMSC() {
        return MSC;
    }
    public void setMSC(String mSC) {
        MSC = mSC;
    }
    public String getMSC_NO() {
        return MSC_NO;
    }
    public void setMSC_NO(String mSC_NO) {
        MSC_NO = mSC_NO;
    }

    public String getMCS_STYLE() {
        return MCS_STYLE;
    }

    public void setMCS_STYLE(String MCS_STYLE) {
        this.MCS_STYLE = MCS_STYLE;
    }

    public String getMCS_COLOR() {
        return MCS_COLOR;
    }

    public void setMCS_COLOR(String MCS_COLOR) {
        this.MCS_COLOR = MCS_COLOR;
    }

    public String getMCS_HARD() {
        return MCS_HARD;
    }

    public void setMCS_HARD(String MCS_HARD) {
        this.MCS_HARD = MCS_HARD;
    }

    @Override
    public String toString() {
        return
                this.MSC+"-"+MSC_NO
                ;
    }

}
