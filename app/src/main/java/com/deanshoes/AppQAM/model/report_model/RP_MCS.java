package com.deanshoes.AppQAM.model.report_model;

public class RP_MCS {
    private String MCS;

    public String getMCS() {
        return MCS;
    }

    public void setMCS(String MCS) {
        this.MCS = MCS;
    }
}
