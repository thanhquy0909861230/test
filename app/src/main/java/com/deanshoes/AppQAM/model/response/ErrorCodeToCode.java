package com.deanshoes.AppQAM.model.response;

/**
 * 將ErrorCode enum轉換為內存碼
 * @author bin.chang
 *
 */
public interface ErrorCodeToCode {

	Integer toCode();
	String toDesc();
}
