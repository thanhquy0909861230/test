package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class QAM_SUB_MAC implements Serializable {
    private static final long serialVersionUID = 1L;
    private String MAC_MTR1;
    private String MAC_MTR2;
    private String MAC_MTR3;
    private String MAC_MTR4;
    private String MAC_MTR5;
    private String MAC_MTR6;
    private String MAC_MTR7;
    private String MAC_MTR8;
    private String MAC_MTR9;
    private String MAC_MTR10;

    private String MAC_CHEM1;
    private String MAC_CHEM2;
    private String MAC_CHEM3;
    private String MAC_CHEM4;
    private String MAC_CHEM5;
    private String MAC_CHEM6;
    private String MAC_CHEM7;
    private String MAC_CHEM8;
    private String MAC_CHEM9;
    private String MAC_CHEM10;
    private String MAC_CHEM11;
    private String MAC_CHEM12;
    private String MAC_CHEM13;
    private String MAC_CHEM14;
    private String MAC_CHEM15;
    private String MAC_CHEM16;
    private String MAC_CHEM17;
    private String MAC_CHEM18;
    private String MAC_CHEM19;
    private String MAC_CHEM20;

    private String MAC_PIG1;
    private String MAC_PIG2;
    private String MAC_PIG3;
    private String MAC_PIG4;
    private String MAC_PIG5;
    private String MAC_PIG6;
    private String MAC_PIG7;
    private String MAC_PIG8;
    private String MAC_PIG9;
    private String MAC_PIG10;

    private String UP_USER;

    private String MAC_NO;
    private String INS_NO;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getMAC_MTR1() {
        return MAC_MTR1;
    }

    public void setMAC_MTR1(String MAC_MTR1) {
        this.MAC_MTR1 = MAC_MTR1;
    }

    public String getMAC_MTR2() {
        return MAC_MTR2;
    }

    public void setMAC_MTR2(String MAC_MTR2) {
        this.MAC_MTR2 = MAC_MTR2;
    }

    public String getMAC_MTR3() {
        return MAC_MTR3;
    }

    public void setMAC_MTR3(String MAC_MTR3) {
        this.MAC_MTR3 = MAC_MTR3;
    }

    public String getMAC_MTR4() {
        return MAC_MTR4;
    }

    public void setMAC_MTR4(String MAC_MTR4) {
        this.MAC_MTR4 = MAC_MTR4;
    }

    public String getMAC_MTR5() {
        return MAC_MTR5;
    }

    public void setMAC_MTR5(String MAC_MTR5) {
        this.MAC_MTR5 = MAC_MTR5;
    }

    public String getMAC_MTR6() {
        return MAC_MTR6;
    }

    public void setMAC_MTR6(String MAC_MTR6) {
        this.MAC_MTR6 = MAC_MTR6;
    }

    public String getMAC_MTR7() {
        return MAC_MTR7;
    }

    public void setMAC_MTR7(String MAC_MTR7) {
        this.MAC_MTR7 = MAC_MTR7;
    }

    public String getMAC_MTR8() {
        return MAC_MTR8;
    }

    public void setMAC_MTR8(String MAC_MTR8) {
        this.MAC_MTR8 = MAC_MTR8;
    }

    public String getMAC_MTR9() {
        return MAC_MTR9;
    }

    public void setMAC_MTR9(String MAC_MTR9) {
        this.MAC_MTR9 = MAC_MTR9;
    }

    public String getMAC_MTR10() {
        return MAC_MTR10;
    }

    public void setMAC_MTR10(String MAC_MTR10) {
        this.MAC_MTR10 = MAC_MTR10;
    }

    public String getMAC_CHEM1() {
        return MAC_CHEM1;
    }

    public void setMAC_CHEM1(String MAC_CHEM1) {
        this.MAC_CHEM1 = MAC_CHEM1;
    }

    public String getMAC_CHEM2() {
        return MAC_CHEM2;
    }

    public void setMAC_CHEM2(String MAC_CHEM2) {
        this.MAC_CHEM2 = MAC_CHEM2;
    }

    public String getMAC_CHEM3() {
        return MAC_CHEM3;
    }

    public void setMAC_CHEM3(String MAC_CHEM3) {
        this.MAC_CHEM3 = MAC_CHEM3;
    }

    public String getMAC_CHEM4() {
        return MAC_CHEM4;
    }

    public void setMAC_CHEM4(String MAC_CHEM4) {
        this.MAC_CHEM4 = MAC_CHEM4;
    }

    public String getMAC_CHEM5() {
        return MAC_CHEM5;
    }

    public void setMAC_CHEM5(String MAC_CHEM5) {
        this.MAC_CHEM5 = MAC_CHEM5;
    }

    public String getMAC_CHEM6() {
        return MAC_CHEM6;
    }

    public void setMAC_CHEM6(String MAC_CHEM6) {
        this.MAC_CHEM6 = MAC_CHEM6;
    }

    public String getMAC_CHEM7() {
        return MAC_CHEM7;
    }

    public void setMAC_CHEM7(String MAC_CHEM7) {
        this.MAC_CHEM7 = MAC_CHEM7;
    }

    public String getMAC_CHEM8() {
        return MAC_CHEM8;
    }

    public void setMAC_CHEM8(String MAC_CHEM8) {
        this.MAC_CHEM8 = MAC_CHEM8;
    }

    public String getMAC_CHEM9() {
        return MAC_CHEM9;
    }

    public void setMAC_CHEM9(String MAC_CHEM9) {
        this.MAC_CHEM9 = MAC_CHEM9;
    }

    public String getMAC_CHEM10() {
        return MAC_CHEM10;
    }

    public void setMAC_CHEM10(String MAC_CHEM10) {
        this.MAC_CHEM10 = MAC_CHEM10;
    }

    public String getMAC_CHEM11() {
        return MAC_CHEM11;
    }

    public void setMAC_CHEM11(String MAC_CHEM11) {
        this.MAC_CHEM11 = MAC_CHEM11;
    }

    public String getMAC_CHEM12() {
        return MAC_CHEM12;
    }

    public void setMAC_CHEM12(String MAC_CHEM12) {
        this.MAC_CHEM12 = MAC_CHEM12;
    }

    public String getMAC_CHEM13() {
        return MAC_CHEM13;
    }

    public void setMAC_CHEM13(String MAC_CHEM13) {
        this.MAC_CHEM13 = MAC_CHEM13;
    }

    public String getMAC_CHEM14() {
        return MAC_CHEM14;
    }

    public void setMAC_CHEM14(String MAC_CHEM14) {
        this.MAC_CHEM14 = MAC_CHEM14;
    }

    public String getMAC_CHEM15() {
        return MAC_CHEM15;
    }

    public void setMAC_CHEM15(String MAC_CHEM15) {
        this.MAC_CHEM15 = MAC_CHEM15;
    }

    public String getMAC_CHEM16() {
        return MAC_CHEM16;
    }

    public void setMAC_CHEM16(String MAC_CHEM16) {
        this.MAC_CHEM16 = MAC_CHEM16;
    }

    public String getMAC_CHEM17() {
        return MAC_CHEM17;
    }

    public void setMAC_CHEM17(String MAC_CHEM17) {
        this.MAC_CHEM17 = MAC_CHEM17;
    }

    public String getMAC_CHEM18() {
        return MAC_CHEM18;
    }

    public void setMAC_CHEM18(String MAC_CHEM18) {
        this.MAC_CHEM18 = MAC_CHEM18;
    }

    public String getMAC_CHEM19() {
        return MAC_CHEM19;
    }

    public void setMAC_CHEM19(String MAC_CHEM19) {
        this.MAC_CHEM19 = MAC_CHEM19;
    }

    public String getMAC_CHEM20() {
        return MAC_CHEM20;
    }

    public void setMAC_CHEM20(String MAC_CHEM20) {
        this.MAC_CHEM20 = MAC_CHEM20;
    }

    public String getMAC_PIG1() {
        return MAC_PIG1;
    }

    public void setMAC_PIG1(String MAC_PIG1) {
        this.MAC_PIG1 = MAC_PIG1;
    }

    public String getMAC_PIG2() {
        return MAC_PIG2;
    }

    public void setMAC_PIG2(String MAC_PIG2) {
        this.MAC_PIG2 = MAC_PIG2;
    }

    public String getMAC_PIG3() {
        return MAC_PIG3;
    }

    public void setMAC_PIG3(String MAC_PIG3) {
        this.MAC_PIG3 = MAC_PIG3;
    }

    public String getMAC_PIG4() {
        return MAC_PIG4;
    }

    public void setMAC_PIG4(String MAC_PIG4) {
        this.MAC_PIG4 = MAC_PIG4;
    }

    public String getMAC_PIG5() {
        return MAC_PIG5;
    }

    public void setMAC_PIG5(String MAC_PIG5) {
        this.MAC_PIG5 = MAC_PIG5;
    }

    public String getMAC_PIG6() {
        return MAC_PIG6;
    }

    public void setMAC_PIG6(String MAC_PIG6) {
        this.MAC_PIG6 = MAC_PIG6;
    }

    public String getMAC_PIG7() {
        return MAC_PIG7;
    }

    public void setMAC_PIG7(String MAC_PIG7) {
        this.MAC_PIG7 = MAC_PIG7;
    }

    public String getMAC_PIG8() {
        return MAC_PIG8;
    }

    public void setMAC_PIG8(String MAC_PIG8) {
        this.MAC_PIG8 = MAC_PIG8;
    }

    public String getMAC_PIG9() {
        return MAC_PIG9;
    }

    public void setMAC_PIG9(String MAC_PIG9) {
        this.MAC_PIG9 = MAC_PIG9;
    }

    public String getMAC_PIG10() {
        return MAC_PIG10;
    }

    public void setMAC_PIG10(String MAC_PIG10) {
        this.MAC_PIG10 = MAC_PIG10;
    }

    public String getMAC_NO() {
        return MAC_NO;
    }

    public void setMAC_NO(String MAC_NO) {
        this.MAC_NO = MAC_NO;
    }

    public String getUP_USER() {
        return UP_USER;
    }

    public void setUP_USER(String UP_USER) {
        this.UP_USER = UP_USER;
    }

    public String getINS_NO() {
        return INS_NO;
    }

    public void setINS_NO(String INS_NO) {
        this.INS_NO = INS_NO;
    }
}
