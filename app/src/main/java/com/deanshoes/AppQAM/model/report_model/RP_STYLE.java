package com.deanshoes.AppQAM.model.report_model;

public class RP_STYLE {
    private String BTC_STYLE;

    public String getBTC_STYLE() {
        return BTC_STYLE;
    }

    public void setBTC_STYLE(String BTC_STYLE) {
        this.BTC_STYLE = BTC_STYLE;
    }
}
