package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class BTC_PELL implements Serializable {
    private static final long serialVersionUID = 1L;

    private String MCS;
    private String PELL_NO;
    private String BTC_NO;
    private String PELL_GRAN;
    private String PELL_TEMP_1;
    private String PELL_TEMP_2;
    private String PELL_TEMP_3;

    public String getPELL_TEMP_5() {
        return PELL_TEMP_5;
    }

    public void setPELL_TEMP_5(String PELL_TEMP_5) {
        this.PELL_TEMP_5 = PELL_TEMP_5;
    }

    private String PELL_TEMP_5;
    private String PELL_TEMP_HEAD;
    private String NEW_BATCH_NO;
    private String PELL_MIXING_TIME;
    private String PELL_OUT_TEMP;
    private String PELL_OUT_TIME;
    private String PELL_CLEAN_TIME;
    private String BTC_STYLE;
    private String BTC_COLOR;
    private String BTC_BATCH_NO;
    private String UP_USER;
    private String PELL_TEMP_1_NOTE;
    private String PELL_TEMP_2_NOTE;
    private String PELL_TEMP_3_NOTE;
    private String PELL_TEMP_6_NOTE;
    private String PELL_OUT_TEMP_NOTE;
    private String PELL_TEMP_HEAD_NOTE;

    private String AMT_NOTE;

    public String getPELL_TEMP_6_NOTE() {
        return PELL_TEMP_6_NOTE;
    }

    public void setPELL_TEMP_6_NOTE(String PELL_TEMP_6_NOTE) {
        this.PELL_TEMP_6_NOTE = PELL_TEMP_6_NOTE;
    }

    public String getMCS() {
        return MCS;
    }

    public void setMCS(String MCS) {
        this.MCS = MCS;
    }

    public String getAMT_NOTE() {
        return AMT_NOTE;
    }

    public void setAMT_NOTE(String AMT_NOTE) {
        this.AMT_NOTE = AMT_NOTE;
    }

    public String getPELL_TEMP_1_NOTE() {
        return PELL_TEMP_1_NOTE;
    }
    public void setPELL_TEMP_1_NOTE(String pELL_TEMP_1_NOTE) {
        PELL_TEMP_1_NOTE = pELL_TEMP_1_NOTE;
    }
    public String getPELL_TEMP_2_NOTE() {
        return PELL_TEMP_2_NOTE;
    }
    public void setPELL_TEMP_2_NOTE(String pELL_TEMP_2_NOTE) {
        PELL_TEMP_2_NOTE = pELL_TEMP_2_NOTE;
    }
    public String getPELL_TEMP_3_NOTE() {
        return PELL_TEMP_3_NOTE;
    }
    public void setPELL_TEMP_3_NOTE(String pELL_TEMP_3_NOTE) {
        PELL_TEMP_3_NOTE = pELL_TEMP_3_NOTE;
    }
    public String getPELL_OUT_TEMP_NOTE() {
        return PELL_OUT_TEMP_NOTE;
    }
    public void setPELL_OUT_TEMP_NOTE(String pELL_OUT_TEMP_NOTE) {
        PELL_OUT_TEMP_NOTE = pELL_OUT_TEMP_NOTE;
    }
    public String getPELL_TEMP_HEAD_NOTE() {
        return PELL_TEMP_HEAD_NOTE;
    }
    public void setPELL_TEMP_HEAD_NOTE(String pELL_TEMP_HEAD_NOTE) {
        PELL_TEMP_HEAD_NOTE = pELL_TEMP_HEAD_NOTE;
    }


    public String getUP_USER() {
        return UP_USER;
    }
    public void setUP_USER(String uP_USER) {
        UP_USER = uP_USER;
    }
    public String getPELL_NO() {
        return PELL_NO;
    }
    public void setPELL_NO(String pELL_NO) {
        PELL_NO = pELL_NO;
    }
    public String getBTC_NO() {
        return BTC_NO;
    }
    public void setBTC_NO(String bTC_NO) {
        BTC_NO = bTC_NO;
    }
    public String getPELL_GRAN() {
        return PELL_GRAN;
    }
    public void setPELL_GRAN(String pELL_GRAN) {
        PELL_GRAN = pELL_GRAN;
    }
    public String getPELL_TEMP_1() {
        return PELL_TEMP_1;
    }
    public void setPELL_TEMP_1(String pELL_TEMP_1) {
        PELL_TEMP_1 = pELL_TEMP_1;
    }
    public String getPELL_TEMP_2() {
        return PELL_TEMP_2;
    }
    public void setPELL_TEMP_2(String pELL_TEMP_2) {
        PELL_TEMP_2 = pELL_TEMP_2;
    }
    public String getPELL_TEMP_3() {
        return PELL_TEMP_3;
    }
    public void setPELL_TEMP_3(String pELL_TEMP_3) {
        PELL_TEMP_3 = pELL_TEMP_3;
    }
    public String getPELL_TEMP_HEAD() {
        return PELL_TEMP_HEAD;
    }
    public void setPELL_TEMP_HEAD(String pELL_TEMP_HEAD) {
        PELL_TEMP_HEAD = pELL_TEMP_HEAD;
    }
    public String getNEW_BATCH_NO() {
        return NEW_BATCH_NO;
    }
    public void setNEW_BATCH_NO(String nEW_BATCH_NO) {
        NEW_BATCH_NO = nEW_BATCH_NO;
    }
    public String getPELL_MIXING_TIME() {
        return PELL_MIXING_TIME;
    }
    public void setPELL_MIXING_TIME(String pELL_MIXING_TIME) {
        PELL_MIXING_TIME = pELL_MIXING_TIME;
    }
    public String getPELL_OUT_TEMP() {
        return PELL_OUT_TEMP;
    }
    public void setPELL_OUT_TEMP(String pELL_OUT_TEMP) {
        PELL_OUT_TEMP = pELL_OUT_TEMP;
    }
    public String getPELL_OUT_TIME() {
        return PELL_OUT_TIME;
    }
    public void setPELL_OUT_TIME(String pELL_OUT_TIME) {
        PELL_OUT_TIME = pELL_OUT_TIME;
    }
    public String getPELL_CLEAN_TIME() {
        return PELL_CLEAN_TIME;
    }
    public void setPELL_CLEAN_TIME(String pELL_CLEAN_TIME) {
        PELL_CLEAN_TIME = pELL_CLEAN_TIME;
    }
    public String getBTC_STYLE() {
        return BTC_STYLE;
    }
    public void setBTC_STYLE(String bTC_STYLE) {
        BTC_STYLE = bTC_STYLE;
    }
    public String getBTC_COLOR() {
        return BTC_COLOR;
    }
    public void setBTC_COLOR(String bTC_COLOR) {
        BTC_COLOR = bTC_COLOR;
    }
    public String getBTC_BATCH_NO() {
        return BTC_BATCH_NO;
    }
    public void setBTC_BATCH_NO(String bTC_BATCH_NO) {
        BTC_BATCH_NO = bTC_BATCH_NO;
    }









}
