package com.deanshoes.AppQAM.model.QAM;

public class QAM_CHECK {
    private String MCS = "";

    private String RAW1 = "";
    private String RAW2 = "";
    private String RAW3  = "";
    private String RAW4 = "";
    private String RAW5 = "";
    private String RAW6 = "";
    private String RAW7 = "";
    private String RAW8 = "";
    private String RAW9 = "";
    private String RAW10 = "";

    private String CHEM1 = "";
    private String CHEM2 = "";
    private String CHEM3 = "";
    private String CHEM4 = "";
    private String CHEM5 = "";
    private String CHEM6 = "";
    private String CHEM7 = "";
    private String CHEM8 = "";
    private String CHEM9 = "";
    private String CHEM10 = "";
    private String CHEM11 = "";
    private String CHEM12 = "";
    private String CHEM13 = "";
    private String CHEM14 = "";
    private String CHEM15 = "";
    private String CHEM16 = "";
    private String CHEM17 = "";
    private String CHEM18 = "";
    private String CHEM19 = "";
    private String CHEM20 = "";

    private String PIG1  = "";
    private String PIG2 = "";
    private String PIG3 = "";
    private String PIG4 = "";
    private String PIG5 = "";
    private String PIG6 = "";
    private String PIG7 = "";
    private String PIG8 = "";
    private String PIG9 = "";
    private String PIG10 = "";

    public String getMCS() {
        return MCS;
    }

    public void setMCS(String MCS) {
        if(MCS != null)
            this.MCS = MCS;
    }

    public String getRAW1() {
        return RAW1;
    }

    public void setRAW1(String RAW1) {
        if(RAW1 != null)
            this.RAW1 = RAW1;
    }

    public String getRAW2() {
        return RAW2;
    }

    public void setRAW2(String RAW2) {
        if(RAW2 != null)
            this.RAW2 = RAW2;
    }

    public String getRAW3() {
        return RAW3;
    }

    public void setRAW3(String RAW3) {
        if(RAW3 != null)
            this.RAW3 = RAW3;
    }

    public String getRAW4() {
        return RAW4;
    }

    public void setRAW4(String RAW4) {
        if(RAW4 != null)
            this.RAW4 = RAW4;
    }

    public String getRAW5() {
        return RAW5;
    }

    public void setRAW5(String RAW5) {
        if(RAW5 != null)
            this.RAW5 = RAW5;
    }

    public String getRAW6() {
        return RAW6;
    }

    public void setRAW6(String RAW6) {
        if(RAW6 != null)
            this.RAW6 = RAW6;
    }

    public String getRAW7() {
        return RAW7;
    }

    public void setRAW7(String RAW7) {
        if(RAW7 != null)
            this.RAW7 = RAW7;
    }

    public String getRAW8() {
        return RAW8;
    }

    public void setRAW8(String RAW8) {
        if(RAW8 != null)
            this.RAW8 = RAW8;
    }

    public String getRAW9() {
        return RAW9;
    }

    public void setRAW9(String RAW9) {
        if(RAW9 != null)
            this.RAW9 = RAW9;
    }

    public String getRAW10() {
        return RAW10;
    }

    public void setRAW10(String RAW10) {
        if(RAW10 != null)
            this.RAW10 = RAW10;
    }

    public String getCHEM1() {
        return CHEM1;
    }

    public void setCHEM1(String CHEM1) {
        if(CHEM1 != null)
            this.CHEM1 = CHEM1;
    }

    public String getCHEM2() {
        return CHEM2;
    }

    public void setCHEM2(String CHEM2) {
        if(CHEM2 != null)
            this.CHEM2 = CHEM2;
    }

    public String getCHEM3() {
        return CHEM3;
    }

    public void setCHEM3(String CHEM3) {
        if(CHEM3 != null)
            this.CHEM3 = CHEM3;
    }

    public String getCHEM4() {
        return CHEM4;
    }

    public void setCHEM4(String CHEM4) {
        if(CHEM4 != null)
            this.CHEM4 = CHEM4;
    }

    public String getCHEM5() {
        return CHEM5;
    }

    public void setCHEM5(String CHEM5) {
        if(CHEM5 != null)
            this.CHEM5 = CHEM5;
    }

    public String getCHEM6() {
        return CHEM6;
    }

    public void setCHEM6(String CHEM6) {
        if(CHEM6 != null)
            this.CHEM6 = CHEM6;
    }

    public String getCHEM7() {
        return CHEM7;
    }

    public void setCHEM7(String CHEM7) {
        if(CHEM7 != null)
            this.CHEM7 = CHEM7;
    }

    public String getCHEM8() {
        return CHEM8;
    }

    public void setCHEM8(String CHEM8) {
        if(CHEM8 != null)
            this.CHEM8 = CHEM8;
    }

    public String getCHEM9() {
        return CHEM9;
    }

    public void setCHEM9(String CHEM9) {
        if(CHEM9 != null)
            this.CHEM9 = CHEM9;
    }

    public String getCHEM10() {
        return CHEM10;
    }

    public void setCHEM10(String CHEM10) {
        if(CHEM10 != null)
            this.CHEM10 = CHEM10;
    }

    public String getCHEM11() {
        return CHEM11;
    }

    public void setCHEM11(String CHEM11) {
        if(CHEM11 != null)
            this.CHEM11 = CHEM11;
    }

    public String getCHEM12() {
        return CHEM12;
    }

    public void setCHEM12(String CHEM12) {
        if(CHEM12 != null)
            this.CHEM12 = CHEM12;
    }

    public String getCHEM13() {
        return CHEM13;
    }

    public void setCHEM13(String CHEM13) {
        if(CHEM13 != null)
            this.CHEM13 = CHEM13;
    }

    public String getCHEM14() {
        return CHEM14;
    }

    public void setCHEM14(String CHEM14) {
        if(CHEM14 != null)
            this.CHEM14 = CHEM14;
    }

    public String getCHEM15() {
        return CHEM15;
    }

    public void setCHEM15(String CHEM15) {
        if(CHEM15 != null)
            this.CHEM15 = CHEM15;
    }

    public String getCHEM16() {
        return CHEM16;
    }

    public void setCHEM16(String CHEM16) {
        if(CHEM16 != null)
            this.CHEM16 = CHEM16;
    }

    public String getCHEM17() {
        return CHEM17;
    }

    public void setCHEM17(String CHEM17) {
        if(CHEM17 != null)
            this.CHEM17 = CHEM17;
    }

    public String getCHEM18() {
        return CHEM18;
    }

    public void setCHEM18(String CHEM18) {
        if(CHEM18 != null)
            this.CHEM18 = CHEM18;
    }

    public String getCHEM19() {
        return CHEM19;
    }

    public void setCHEM19(String CHEM19) {
        if(CHEM19 != null)
            this.CHEM19 = CHEM19;
    }

    public String getCHEM20() {
        return CHEM20;
    }

    public void setCHEM20(String CHEM20) {
        if(CHEM20 != null)
            this.CHEM20 = CHEM20;
    }

    public String getPIG1() {
        return PIG1;
    }

    public void setPIG1(String PIG1) {
        if(PIG1 != null)
            this.PIG1 = PIG1;
    }

    public String getPIG2() {
        return PIG2;
    }

    public void setPIG2(String PIG2) {
        if(PIG2 != null)
            this.PIG2 = PIG2;
    }

    public String getPIG3() {
        return PIG3;
    }

    public void setPIG3(String PIG3) {
        if(PIG3 != null)
            this.PIG3 = PIG3;
    }

    public String getPIG4() {
        return PIG4;
    }

    public void setPIG4(String PIG4) {
        if(PIG4 != null)
            this.PIG4 = PIG4;
    }

    public String getPIG5() {
        return PIG5;
    }

    public void setPIG5(String PIG5) {
        if(PIG5 != null)
            this.PIG5 = PIG5;
    }

    public String getPIG6() {
        return PIG6;
    }

    public void setPIG6(String PIG6) {
        if(PIG6 != null)
            this.PIG6 = PIG6;
    }

    public String getPIG7() {
        return PIG7;
    }

    public void setPIG7(String PIG7) {
        if(PIG7 != null)
            this.PIG7 = PIG7;
    }

    public String getPIG8() {
        return PIG8;
    }

    public void setPIG8(String PIG8) {
        if(PIG8 != null)
            this.PIG8 = PIG8;
    }

    public String getPIG9() {
        return PIG9;
    }

    public void setPIG9(String PIG9) {
        if(PIG9 != null)
            this.PIG9 = PIG9;
    }

    public String getPIG10() {
        return PIG10;
    }

    public void setPIG10(String PIG10) {
        if(PIG10 != null)
            this.PIG10 = PIG10;
    }
}
