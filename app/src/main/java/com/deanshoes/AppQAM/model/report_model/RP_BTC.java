package com.deanshoes.AppQAM.model.report_model;

public class RP_BTC {
    private String BTC_NO;
    private String BTC_BATCH_NO;

    public String getBTC_NO() {
        return BTC_NO;
    }

    public void setBTC_NO(String BTC_NO) {
        this.BTC_NO = BTC_NO;
    }

    public String getBTC_BATCH_NO() {
        return BTC_BATCH_NO;
    }

    public void setBTC_BATCH_NO(String BTC_BATCH_NO) {
        this.BTC_BATCH_NO = BTC_BATCH_NO;
    }
}
