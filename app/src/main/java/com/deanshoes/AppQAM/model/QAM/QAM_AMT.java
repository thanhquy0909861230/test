package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class QAM_AMT implements Serializable {
    private static final long serialVersionUID = 1L;
    private String AMT_NO;
    private String AMT_CHECK;
    private String MAC_MTR1;
    private String MAC_MTR2;
    private String MAC_MTR3;
    private String MAC_MTR4;
    private String MAC_MTR5;
    private String MAC_MTR6;
    private String MAC_MTR7;
    private String MAC_MTR8;
    private String MAC_MTR9;
    private String MAC_MTR10;

    private String MAC_MTR11;
    private String MAC_MTR12;
    private String MAC_MTR13;
    private String MAC_MTR14;

    private String MAC_MTR15;
    private String MAC_MTR16;
    private String MAC_MTR17;
    private String MAC_MTR18;
    private String MAC_MTR19;
    private String MAC_MTR20;
    private String MAC_MTR21;
    private String MAC_MTR22;

    public String getMAC_MTR22() {
        return MAC_MTR22;
    }

    public void setMAC_MTR22(String MAC_MTR22) {
        this.MAC_MTR22 = MAC_MTR22;
    }

    public String getMAC_MTR11() {
        return MAC_MTR11;
    }

    public void setMAC_MTR11(String MAC_MTR11) {
        this.MAC_MTR11 = MAC_MTR11;
    }

    public String getMAC_MTR12() {
        return MAC_MTR12;
    }

    public void setMAC_MTR12(String MAC_MTR12) {
        this.MAC_MTR12 = MAC_MTR12;
    }

    public String getMAC_MTR13() {
        return MAC_MTR13;
    }

    public void setMAC_MTR13(String MAC_MTR13) {
        this.MAC_MTR13 = MAC_MTR13;
    }

    public String getMAC_MTR14() {
        return MAC_MTR14;
    }

    public void setMAC_MTR14(String MAC_MTR14) {
        this.MAC_MTR14 = MAC_MTR14;
    }

    public String getMAC_MTR15() {
        return MAC_MTR15;
    }

    public void setMAC_MTR15(String MAC_MTR15) {
        this.MAC_MTR15 = MAC_MTR15;
    }

    public String getMAC_MTR16() {
        return MAC_MTR16;
    }

    public void setMAC_MTR16(String MAC_MTR16) {
        this.MAC_MTR16 = MAC_MTR16;
    }

    public String getMAC_MTR17() {
        return MAC_MTR17;
    }

    public void setMAC_MTR17(String MAC_MTR17) {
        this.MAC_MTR17 = MAC_MTR17;
    }

    public String getMAC_MTR18() {
        return MAC_MTR18;
    }

    public void setMAC_MTR18(String MAC_MTR18) {
        this.MAC_MTR18 = MAC_MTR18;
    }

    public String getMAC_MTR19() {
        return MAC_MTR19;
    }

    public void setMAC_MTR19(String MAC_MTR19) {
        this.MAC_MTR19 = MAC_MTR19;
    }

    public String getMAC_MTR20() {
        return MAC_MTR20;
    }

    public void setMAC_MTR20(String MAC_MTR20) {
        this.MAC_MTR20 = MAC_MTR20;
    }

    public String getMAC_MTR21() {
        return MAC_MTR21;
    }

    public void setMAC_MTR21(String MAC_MTR21) {
        this.MAC_MTR21 = MAC_MTR21;
    }

    private String MAC_CHEM1;
    private String MAC_CHEM2;
    private String MAC_CHEM3;
    private String MAC_CHEM4;
    private String MAC_CHEM5;
    private String MAC_CHEM6;
    private String MAC_CHEM7;
    private String MAC_CHEM8;
    private String MAC_CHEM9;
    private String MAC_CHEM10;
    private String MAC_CHEM11;
    private String MAC_CHEM12;
    private String MAC_CHEM13;
    private String MAC_CHEM14;
    private String MAC_CHEM15;
    private String MAC_CHEM16;
    private String MAC_CHEM17;
    private String MAC_CHEM18;
    private String MAC_CHEM19;
    private String MAC_CHEM20;

    private String MAC_PIG1;
    private String MAC_PIG2;
    private String MAC_PIG3;
    private String MAC_PIG4;
    private String MAC_PIG5;
    private String MAC_PIG6;
    private String MAC_PIG7;
    private String MAC_PIG8;
    private String MAC_PIG9;
    private String MAC_PIG10;

    private String SUB_MTR1;
    private String SUB_MTR2;
    private String SUB_MTR3;
    private String SUB_MTR4;
    private String SUB_MTR5;
    private String SUB_MTR6;
    private String SUB_MTR7;
    private String SUB_MTR8;
    private String SUB_MTR9;
    private String SUB_MTR10;

    private String SUB_CHEM1;
    private String SUB_CHEM2;
    private String SUB_CHEM3;
    private String SUB_CHEM4;
    private String SUB_CHEM5;
    private String SUB_CHEM6;
    private String SUB_CHEM7;
    private String SUB_CHEM8;
    private String SUB_CHEM9;
    private String SUB_CHEM10;
    private String SUB_CHEM11;
    private String SUB_CHEM12;
    private String SUB_CHEM13;
    private String SUB_CHEM14;
    private String SUB_CHEM15;
    private String SUB_CHEM16;
    private String SUB_CHEM17;
    private String SUB_CHEM18;
    private String SUB_CHEM19;
    private String SUB_CHEM20;

    private String SUB_PIG1;
    private String SUB_PIG2;
    private String SUB_PIG3;
    private String SUB_PIG4;
    private String SUB_PIG5;
    private String SUB_PIG6;
    private String SUB_PIG7;
    private String SUB_PIG8;
    private String SUB_PIG9;
    private String SUB_PIG10;

    private String CHK_MTR1;
    private String CHK_MTR2;
    private String CHK_MTR3;
    private String CHK_MTR4;
    private String CHK_MTR5;
    private String CHK_MTR6;
    private String CHK_MTR7;
    private String CHK_MTR8;
    private String CHK_MTR9;
    private String CHK_MTR10;

    private String CHK_CHEM1;
    private String CHK_CHEM2;
    private String CHK_CHEM3;
    private String CHK_CHEM4;
    private String CHK_CHEM5;
    private String CHK_CHEM6;
    private String CHK_CHEM7;
    private String CHK_CHEM8;
    private String CHK_CHEM9;
    private String CHK_CHEM10;
    private String CHK_CHEM11;
    private String CHK_CHEM12;
    private String CHK_CHEM13;
    private String CHK_CHEM14;
    private String CHK_CHEM15;
    private String CHK_CHEM16;
    private String CHK_CHEM17;
    private String CHK_CHEM18;
    private String CHK_CHEM19;
    private String CHK_CHEM20;

    private String CHK_PIG1;
    private String CHK_PIG2;
    private String CHK_PIG3;
    private String CHK_PIG4;
    private String CHK_PIG5;
    private String CHK_PIG6;
    private String CHK_PIG7;
    private String CHK_PIG8;
    private String CHK_PIG9;
    private String CHK_PIG10;

    private String NOTE_NO;
    private String NOTE_MTR1;
    private String NOTE_MTR2;
    private String NOTE_MTR3;
    private String NOTE_MTR4;
    private String NOTE_MTR5;
    private String NOTE_MTR6;
    private String NOTE_MTR7;
    private String NOTE_MTR8;
    private String NOTE_MTR9;
    private String NOTE_MTR10;

    private String NOTE_CHEM1;
    private String NOTE_CHEM2;
    private String NOTE_CHEM3;
    private String NOTE_CHEM4;
    private String NOTE_CHEM5;
    private String NOTE_CHEM6;
    private String NOTE_CHEM7;
    private String NOTE_CHEM8;
    private String NOTE_CHEM9;
    private String NOTE_CHEM10;
    private String NOTE_CHEM11;
    private String NOTE_CHEM12;
    private String NOTE_CHEM13;
    private String NOTE_CHEM14;
    private String NOTE_CHEM15;
    private String NOTE_CHEM16;
    private String NOTE_CHEM17;
    private String NOTE_CHEM18;
    private String NOTE_CHEM19;
    private String NOTE_CHEM20;

    private String NOTE_PIG1;
    private String NOTE_PIG2;
    private String NOTE_PIG3;
    private String NOTE_PIG4;
    private String NOTE_PIG5;
    private String NOTE_PIG6;
    private String NOTE_PIG7;
    private String NOTE_PIG8;
    private String NOTE_PIG9;
    private String NOTE_PIG10;

    private String UP_DATE;
    private String UP_USER;
    private String INS_NO;
    private String MCS_NO;
    private String BTC_NO;
    private String BTC_BATCH_NO;

    private String MAC_SCALERAW1;
    private String MAC_SCALERAW2;

    public String getMAC_SCALERAW1() {
        return MAC_SCALERAW1;
    }

    public void setMAC_SCALERAW1(String MAC_SCALERAW1) {
        this.MAC_SCALERAW1 = MAC_SCALERAW1;
    }

    public String getMAC_SCALERAW2() {
        return MAC_SCALERAW2;
    }

    public void setMAC_SCALERAW2(String MAC_SCALERAW2) {
        this.MAC_SCALERAW2 = MAC_SCALERAW2;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getAMT_NO() {
        return AMT_NO;
    }

    public void setAMT_NO(String AMT_NO) {
        this.AMT_NO = AMT_NO;
    }

    public String getAMT_CHECK() {
        return AMT_CHECK;
    }

    public void setAMT_CHECK(String AMT_CHECK) {
        this.AMT_CHECK = AMT_CHECK;
    }

    public String getMAC_MTR1() {
        return MAC_MTR1;
    }

    public void setMAC_MTR1(String MAC_MTR1) {
        this.MAC_MTR1 = MAC_MTR1;
    }

    public String getMAC_MTR2() {
        return MAC_MTR2;
    }

    public void setMAC_MTR2(String MAC_MTR2) {
        this.MAC_MTR2 = MAC_MTR2;
    }

    public String getMAC_MTR3() {
        return MAC_MTR3;
    }

    public void setMAC_MTR3(String MAC_MTR3) {
        this.MAC_MTR3 = MAC_MTR3;
    }

    public String getMAC_MTR4() {
        return MAC_MTR4;
    }

    public void setMAC_MTR4(String MAC_MTR4) {
        this.MAC_MTR4 = MAC_MTR4;
    }

    public String getMAC_MTR5() {
        return MAC_MTR5;
    }

    public void setMAC_MTR5(String MAC_MTR5) {
        this.MAC_MTR5 = MAC_MTR5;
    }

    public String getMAC_MTR6() {
        return MAC_MTR6;
    }

    public void setMAC_MTR6(String MAC_MTR6) {
        this.MAC_MTR6 = MAC_MTR6;
    }

    public String getMAC_MTR7() {
        return MAC_MTR7;
    }

    public void setMAC_MTR7(String MAC_MTR7) {
        this.MAC_MTR7 = MAC_MTR7;
    }

    public String getMAC_MTR8() {
        return MAC_MTR8;
    }

    public void setMAC_MTR8(String MAC_MTR8) {
        this.MAC_MTR8 = MAC_MTR8;
    }

    public String getMAC_MTR9() {
        return MAC_MTR9;
    }

    public void setMAC_MTR9(String MAC_MTR9) {
        this.MAC_MTR9 = MAC_MTR9;
    }

    public String getMAC_MTR10() {
        return MAC_MTR10;
    }

    public void setMAC_MTR10(String MAC_MTR10) {
        this.MAC_MTR10 = MAC_MTR10;
    }

    public String getMAC_CHEM1() {
        return MAC_CHEM1;
    }

    public void setMAC_CHEM1(String MAC_CHEM1) {
        this.MAC_CHEM1 = MAC_CHEM1;
    }

    public String getMAC_CHEM2() {
        return MAC_CHEM2;
    }

    public void setMAC_CHEM2(String MAC_CHEM2) {
        this.MAC_CHEM2 = MAC_CHEM2;
    }

    public String getMAC_CHEM3() {
        return MAC_CHEM3;
    }

    public void setMAC_CHEM3(String MAC_CHEM3) {
        this.MAC_CHEM3 = MAC_CHEM3;
    }

    public String getMAC_CHEM4() {
        return MAC_CHEM4;
    }

    public void setMAC_CHEM4(String MAC_CHEM4) {
        this.MAC_CHEM4 = MAC_CHEM4;
    }

    public String getMAC_CHEM5() {
        return MAC_CHEM5;
    }

    public void setMAC_CHEM5(String MAC_CHEM5) {
        this.MAC_CHEM5 = MAC_CHEM5;
    }

    public String getMAC_CHEM6() {
        return MAC_CHEM6;
    }

    public void setMAC_CHEM6(String MAC_CHEM6) {
        this.MAC_CHEM6 = MAC_CHEM6;
    }

    public String getMAC_CHEM7() {
        return MAC_CHEM7;
    }

    public void setMAC_CHEM7(String MAC_CHEM7) {
        this.MAC_CHEM7 = MAC_CHEM7;
    }

    public String getMAC_CHEM8() {
        return MAC_CHEM8;
    }

    public void setMAC_CHEM8(String MAC_CHEM8) {
        this.MAC_CHEM8 = MAC_CHEM8;
    }

    public String getMAC_CHEM9() {
        return MAC_CHEM9;
    }

    public void setMAC_CHEM9(String MAC_CHEM9) {
        this.MAC_CHEM9 = MAC_CHEM9;
    }

    public String getMAC_CHEM10() {
        return MAC_CHEM10;
    }

    public void setMAC_CHEM10(String MAC_CHEM10) {
        this.MAC_CHEM10 = MAC_CHEM10;
    }

    public String getMAC_CHEM11() {
        return MAC_CHEM11;
    }

    public void setMAC_CHEM11(String MAC_CHEM11) {
        this.MAC_CHEM11 = MAC_CHEM11;
    }

    public String getMAC_CHEM12() {
        return MAC_CHEM12;
    }

    public void setMAC_CHEM12(String MAC_CHEM12) {
        this.MAC_CHEM12 = MAC_CHEM12;
    }

    public String getMAC_CHEM13() {
        return MAC_CHEM13;
    }

    public void setMAC_CHEM13(String MAC_CHEM13) {
        this.MAC_CHEM13 = MAC_CHEM13;
    }

    public String getMAC_CHEM14() {
        return MAC_CHEM14;
    }

    public void setMAC_CHEM14(String MAC_CHEM14) {
        this.MAC_CHEM14 = MAC_CHEM14;
    }

    public String getMAC_CHEM15() {
        return MAC_CHEM15;
    }

    public void setMAC_CHEM15(String MAC_CHEM15) {
        this.MAC_CHEM15 = MAC_CHEM15;
    }

    public String getMAC_CHEM16() {
        return MAC_CHEM16;
    }

    public void setMAC_CHEM16(String MAC_CHEM16) {
        this.MAC_CHEM16 = MAC_CHEM16;
    }

    public String getMAC_CHEM17() {
        return MAC_CHEM17;
    }

    public void setMAC_CHEM17(String MAC_CHEM17) {
        this.MAC_CHEM17 = MAC_CHEM17;
    }

    public String getMAC_CHEM18() {
        return MAC_CHEM18;
    }

    public void setMAC_CHEM18(String MAC_CHEM18) {
        this.MAC_CHEM18 = MAC_CHEM18;
    }

    public String getMAC_CHEM19() {
        return MAC_CHEM19;
    }

    public void setMAC_CHEM19(String MAC_CHEM19) {
        this.MAC_CHEM19 = MAC_CHEM19;
    }

    public String getMAC_CHEM20() {
        return MAC_CHEM20;
    }

    public void setMAC_CHEM20(String MAC_CHEM20) {
        this.MAC_CHEM20 = MAC_CHEM20;
    }

    public String getMAC_PIG1() {
        return MAC_PIG1;
    }

    public void setMAC_PIG1(String MAC_PIG1) {
        this.MAC_PIG1 = MAC_PIG1;
    }

    public String getMAC_PIG2() {
        return MAC_PIG2;
    }

    public void setMAC_PIG2(String MAC_PIG2) {
        this.MAC_PIG2 = MAC_PIG2;
    }

    public String getMAC_PIG3() {
        return MAC_PIG3;
    }

    public void setMAC_PIG3(String MAC_PIG3) {
        this.MAC_PIG3 = MAC_PIG3;
    }

    public String getMAC_PIG4() {
        return MAC_PIG4;
    }

    public void setMAC_PIG4(String MAC_PIG4) {
        this.MAC_PIG4 = MAC_PIG4;
    }

    public String getMAC_PIG5() {
        return MAC_PIG5;
    }

    public void setMAC_PIG5(String MAC_PIG5) {
        this.MAC_PIG5 = MAC_PIG5;
    }

    public String getMAC_PIG6() {
        return MAC_PIG6;
    }

    public void setMAC_PIG6(String MAC_PIG6) {
        this.MAC_PIG6 = MAC_PIG6;
    }

    public String getMAC_PIG7() {
        return MAC_PIG7;
    }

    public void setMAC_PIG7(String MAC_PIG7) {
        this.MAC_PIG7 = MAC_PIG7;
    }

    public String getMAC_PIG8() {
        return MAC_PIG8;
    }

    public void setMAC_PIG8(String MAC_PIG8) {
        this.MAC_PIG8 = MAC_PIG8;
    }

    public String getMAC_PIG9() {
        return MAC_PIG9;
    }

    public void setMAC_PIG9(String MAC_PIG9) {
        this.MAC_PIG9 = MAC_PIG9;
    }

    public String getMAC_PIG10() {
        return MAC_PIG10;
    }

    public void setMAC_PIG10(String MAC_PIG10) {
        this.MAC_PIG10 = MAC_PIG10;
    }

    public String getSUB_MTR1() {
        return SUB_MTR1;
    }

    public void setSUB_MTR1(String SUB_MTR1) {
        this.SUB_MTR1 = SUB_MTR1;
    }

    public String getSUB_MTR2() {
        return SUB_MTR2;
    }

    public void setSUB_MTR2(String SUB_MTR2) {
        this.SUB_MTR2 = SUB_MTR2;
    }

    public String getSUB_MTR3() {
        return SUB_MTR3;
    }

    public void setSUB_MTR3(String SUB_MTR3) {
        this.SUB_MTR3 = SUB_MTR3;
    }

    public String getSUB_MTR4() {
        return SUB_MTR4;
    }

    public void setSUB_MTR4(String SUB_MTR4) {
        this.SUB_MTR4 = SUB_MTR4;
    }

    public String getSUB_MTR5() {
        return SUB_MTR5;
    }

    public void setSUB_MTR5(String SUB_MTR5) {
        this.SUB_MTR5 = SUB_MTR5;
    }

    public String getSUB_MTR6() {
        return SUB_MTR6;
    }

    public void setSUB_MTR6(String SUB_MTR6) {
        this.SUB_MTR6 = SUB_MTR6;
    }

    public String getSUB_MTR7() {
        return SUB_MTR7;
    }

    public void setSUB_MTR7(String SUB_MTR7) {
        this.SUB_MTR7 = SUB_MTR7;
    }

    public String getSUB_MTR8() {
        return SUB_MTR8;
    }

    public void setSUB_MTR8(String SUB_MTR8) {
        this.SUB_MTR8 = SUB_MTR8;
    }

    public String getSUB_MTR9() {
        return SUB_MTR9;
    }

    public void setSUB_MTR9(String SUB_MTR9) {
        this.SUB_MTR9 = SUB_MTR9;
    }

    public String getSUB_MTR10() {
        return SUB_MTR10;
    }

    public void setSUB_MTR10(String SUB_MTR10) {
        this.SUB_MTR10 = SUB_MTR10;
    }

    public String getSUB_CHEM1() {
        return SUB_CHEM1;
    }

    public void setSUB_CHEM1(String SUB_CHEM1) {
        this.SUB_CHEM1 = SUB_CHEM1;
    }

    public String getSUB_CHEM2() {
        return SUB_CHEM2;
    }

    public void setSUB_CHEM2(String SUB_CHEM2) {
        this.SUB_CHEM2 = SUB_CHEM2;
    }

    public String getSUB_CHEM3() {
        return SUB_CHEM3;
    }

    public void setSUB_CHEM3(String SUB_CHEM3) {
        this.SUB_CHEM3 = SUB_CHEM3;
    }

    public String getSUB_CHEM4() {
        return SUB_CHEM4;
    }

    public void setSUB_CHEM4(String SUB_CHEM4) {
        this.SUB_CHEM4 = SUB_CHEM4;
    }

    public String getSUB_CHEM5() {
        return SUB_CHEM5;
    }

    public void setSUB_CHEM5(String SUB_CHEM5) {
        this.SUB_CHEM5 = SUB_CHEM5;
    }

    public String getSUB_CHEM6() {
        return SUB_CHEM6;
    }

    public void setSUB_CHEM6(String SUB_CHEM6) {
        this.SUB_CHEM6 = SUB_CHEM6;
    }

    public String getSUB_CHEM7() {
        return SUB_CHEM7;
    }

    public void setSUB_CHEM7(String SUB_CHEM7) {
        this.SUB_CHEM7 = SUB_CHEM7;
    }

    public String getSUB_CHEM8() {
        return SUB_CHEM8;
    }

    public void setSUB_CHEM8(String SUB_CHEM8) {
        this.SUB_CHEM8 = SUB_CHEM8;
    }

    public String getSUB_CHEM9() {
        return SUB_CHEM9;
    }

    public void setSUB_CHEM9(String SUB_CHEM9) {
        this.SUB_CHEM9 = SUB_CHEM9;
    }

    public String getSUB_CHEM10() {
        return SUB_CHEM10;
    }

    public void setSUB_CHEM10(String SUB_CHEM10) {
        this.SUB_CHEM10 = SUB_CHEM10;
    }

    public String getSUB_CHEM11() {
        return SUB_CHEM11;
    }

    public void setSUB_CHEM11(String SUB_CHEM11) {
        this.SUB_CHEM11 = SUB_CHEM11;
    }

    public String getSUB_CHEM12() {
        return SUB_CHEM12;
    }

    public void setSUB_CHEM12(String SUB_CHEM12) {
        this.SUB_CHEM12 = SUB_CHEM12;
    }

    public String getSUB_CHEM13() {
        return SUB_CHEM13;
    }

    public void setSUB_CHEM13(String SUB_CHEM13) {
        this.SUB_CHEM13 = SUB_CHEM13;
    }

    public String getSUB_CHEM14() {
        return SUB_CHEM14;
    }

    public void setSUB_CHEM14(String SUB_CHEM14) {
        this.SUB_CHEM14 = SUB_CHEM14;
    }

    public String getSUB_CHEM15() {
        return SUB_CHEM15;
    }

    public void setSUB_CHEM15(String SUB_CHEM15) {
        this.SUB_CHEM15 = SUB_CHEM15;
    }

    public String getSUB_CHEM16() {
        return SUB_CHEM16;
    }

    public void setSUB_CHEM16(String SUB_CHEM16) {
        this.SUB_CHEM16 = SUB_CHEM16;
    }

    public String getSUB_CHEM17() {
        return SUB_CHEM17;
    }

    public void setSUB_CHEM17(String SUB_CHEM17) {
        this.SUB_CHEM17 = SUB_CHEM17;
    }

    public String getSUB_CHEM18() {
        return SUB_CHEM18;
    }

    public void setSUB_CHEM18(String SUB_CHEM18) {
        this.SUB_CHEM18 = SUB_CHEM18;
    }

    public String getSUB_CHEM19() {
        return SUB_CHEM19;
    }

    public void setSUB_CHEM19(String SUB_CHEM19) {
        this.SUB_CHEM19 = SUB_CHEM19;
    }

    public String getSUB_CHEM20() {
        return SUB_CHEM20;
    }

    public void setSUB_CHEM20(String SUB_CHEM20) {
        this.SUB_CHEM20 = SUB_CHEM20;
    }

    public String getSUB_PIG1() {
        return SUB_PIG1;
    }

    public void setSUB_PIG1(String SUB_PIG1) {
        this.SUB_PIG1 = SUB_PIG1;
    }

    public String getSUB_PIG2() {
        return SUB_PIG2;
    }

    public void setSUB_PIG2(String SUB_PIG2) {
        this.SUB_PIG2 = SUB_PIG2;
    }

    public String getSUB_PIG3() {
        return SUB_PIG3;
    }

    public void setSUB_PIG3(String SUB_PIG3) {
        this.SUB_PIG3 = SUB_PIG3;
    }

    public String getSUB_PIG4() {
        return SUB_PIG4;
    }

    public void setSUB_PIG4(String SUB_PIG4) {
        this.SUB_PIG4 = SUB_PIG4;
    }

    public String getSUB_PIG5() {
        return SUB_PIG5;
    }

    public void setSUB_PIG5(String SUB_PIG5) {
        this.SUB_PIG5 = SUB_PIG5;
    }

    public String getSUB_PIG6() {
        return SUB_PIG6;
    }

    public void setSUB_PIG6(String SUB_PIG6) {
        this.SUB_PIG6 = SUB_PIG6;
    }

    public String getSUB_PIG7() {
        return SUB_PIG7;
    }

    public void setSUB_PIG7(String SUB_PIG7) {
        this.SUB_PIG7 = SUB_PIG7;
    }

    public String getSUB_PIG8() {
        return SUB_PIG8;
    }

    public void setSUB_PIG8(String SUB_PIG8) {
        this.SUB_PIG8 = SUB_PIG8;
    }

    public String getSUB_PIG9() {
        return SUB_PIG9;
    }

    public void setSUB_PIG9(String SUB_PIG9) {
        this.SUB_PIG9 = SUB_PIG9;
    }

    public String getSUB_PIG10() {
        return SUB_PIG10;
    }

    public void setSUB_PIG10(String SUB_PIG10) {
        this.SUB_PIG10 = SUB_PIG10;
    }

    public String getCHK_MTR1() {
        return CHK_MTR1;
    }

    public void setCHK_MTR1(String CHK_MTR1) {
        this.CHK_MTR1 = CHK_MTR1;
    }

    public String getCHK_MTR2() {
        return CHK_MTR2;
    }

    public void setCHK_MTR2(String CHK_MTR2) {
        this.CHK_MTR2 = CHK_MTR2;
    }

    public String getCHK_MTR3() {
        return CHK_MTR3;
    }

    public void setCHK_MTR3(String CHK_MTR3) {
        this.CHK_MTR3 = CHK_MTR3;
    }

    public String getCHK_MTR4() {
        return CHK_MTR4;
    }

    public void setCHK_MTR4(String CHK_MTR4) {
        this.CHK_MTR4 = CHK_MTR4;
    }

    public String getCHK_MTR5() {
        return CHK_MTR5;
    }

    public void setCHK_MTR5(String CHK_MTR5) {
        this.CHK_MTR5 = CHK_MTR5;
    }

    public String getCHK_MTR6() {
        return CHK_MTR6;
    }

    public void setCHK_MTR6(String CHK_MTR6) {
        this.CHK_MTR6 = CHK_MTR6;
    }

    public String getCHK_MTR7() {
        return CHK_MTR7;
    }

    public void setCHK_MTR7(String CHK_MTR7) {
        this.CHK_MTR7 = CHK_MTR7;
    }

    public String getCHK_MTR8() {
        return CHK_MTR8;
    }

    public void setCHK_MTR8(String CHK_MTR8) {
        this.CHK_MTR8 = CHK_MTR8;
    }

    public String getCHK_MTR9() {
        return CHK_MTR9;
    }

    public void setCHK_MTR9(String CHK_MTR9) {
        this.CHK_MTR9 = CHK_MTR9;
    }

    public String getCHK_MTR10() {
        return CHK_MTR10;
    }

    public void setCHK_MTR10(String CHK_MTR10) {
        this.CHK_MTR10 = CHK_MTR10;
    }

    public String getCHK_CHEM1() {
        return CHK_CHEM1;
    }

    public void setCHK_CHEM1(String CHK_CHEM1) {
        this.CHK_CHEM1 = CHK_CHEM1;
    }

    public String getCHK_CHEM2() {
        return CHK_CHEM2;
    }

    public void setCHK_CHEM2(String CHK_CHEM2) {
        this.CHK_CHEM2 = CHK_CHEM2;
    }

    public String getCHK_CHEM3() {
        return CHK_CHEM3;
    }

    public void setCHK_CHEM3(String CHK_CHEM3) {
        this.CHK_CHEM3 = CHK_CHEM3;
    }

    public String getCHK_CHEM4() {
        return CHK_CHEM4;
    }

    public void setCHK_CHEM4(String CHK_CHEM4) {
        this.CHK_CHEM4 = CHK_CHEM4;
    }

    public String getCHK_CHEM5() {
        return CHK_CHEM5;
    }

    public void setCHK_CHEM5(String CHK_CHEM5) {
        this.CHK_CHEM5 = CHK_CHEM5;
    }

    public String getCHK_CHEM6() {
        return CHK_CHEM6;
    }

    public void setCHK_CHEM6(String CHK_CHEM6) {
        this.CHK_CHEM6 = CHK_CHEM6;
    }

    public String getCHK_CHEM7() {
        return CHK_CHEM7;
    }

    public void setCHK_CHEM7(String CHK_CHEM7) {
        this.CHK_CHEM7 = CHK_CHEM7;
    }

    public String getCHK_CHEM8() {
        return CHK_CHEM8;
    }

    public void setCHK_CHEM8(String CHK_CHEM8) {
        this.CHK_CHEM8 = CHK_CHEM8;
    }

    public String getCHK_CHEM9() {
        return CHK_CHEM9;
    }

    public void setCHK_CHEM9(String CHK_CHEM9) {
        this.CHK_CHEM9 = CHK_CHEM9;
    }

    public String getCHK_CHEM10() {
        return CHK_CHEM10;
    }

    public void setCHK_CHEM10(String CHK_CHEM10) {
        this.CHK_CHEM10 = CHK_CHEM10;
    }

    public String getCHK_CHEM11() {
        return CHK_CHEM11;
    }

    public void setCHK_CHEM11(String CHK_CHEM11) {
        this.CHK_CHEM11 = CHK_CHEM11;
    }

    public String getCHK_CHEM12() {
        return CHK_CHEM12;
    }

    public void setCHK_CHEM12(String CHK_CHEM12) {
        this.CHK_CHEM12 = CHK_CHEM12;
    }

    public String getCHK_CHEM13() {
        return CHK_CHEM13;
    }

    public void setCHK_CHEM13(String CHK_CHEM13) {
        this.CHK_CHEM13 = CHK_CHEM13;
    }

    public String getCHK_CHEM14() {
        return CHK_CHEM14;
    }

    public void setCHK_CHEM14(String CHK_CHEM14) {
        this.CHK_CHEM14 = CHK_CHEM14;
    }

    public String getCHK_CHEM15() {
        return CHK_CHEM15;
    }

    public void setCHK_CHEM15(String CHK_CHEM15) {
        this.CHK_CHEM15 = CHK_CHEM15;
    }

    public String getCHK_CHEM16() {
        return CHK_CHEM16;
    }

    public void setCHK_CHEM16(String CHK_CHEM16) {
        this.CHK_CHEM16 = CHK_CHEM16;
    }

    public String getCHK_CHEM17() {
        return CHK_CHEM17;
    }

    public void setCHK_CHEM17(String CHK_CHEM17) {
        this.CHK_CHEM17 = CHK_CHEM17;
    }

    public String getCHK_CHEM18() {
        return CHK_CHEM18;
    }

    public void setCHK_CHEM18(String CHK_CHEM18) {
        this.CHK_CHEM18 = CHK_CHEM18;
    }

    public String getCHK_CHEM19() {
        return CHK_CHEM19;
    }

    public void setCHK_CHEM19(String CHK_CHEM19) {
        this.CHK_CHEM19 = CHK_CHEM19;
    }

    public String getCHK_CHEM20() {
        return CHK_CHEM20;
    }

    public void setCHK_CHEM20(String CHK_CHEM20) {
        this.CHK_CHEM20 = CHK_CHEM20;
    }

    public String getCHK_PIG1() {
        return CHK_PIG1;
    }

    public void setCHK_PIG1(String CHK_PIG1) {
        this.CHK_PIG1 = CHK_PIG1;
    }

    public String getCHK_PIG2() {
        return CHK_PIG2;
    }

    public void setCHK_PIG2(String CHK_PIG2) {
        this.CHK_PIG2 = CHK_PIG2;
    }

    public String getCHK_PIG3() {
        return CHK_PIG3;
    }

    public void setCHK_PIG3(String CHK_PIG3) {
        this.CHK_PIG3 = CHK_PIG3;
    }

    public String getCHK_PIG4() {
        return CHK_PIG4;
    }

    public void setCHK_PIG4(String CHK_PIG4) {
        this.CHK_PIG4 = CHK_PIG4;
    }

    public String getCHK_PIG5() {
        return CHK_PIG5;
    }

    public void setCHK_PIG5(String CHK_PIG5) {
        this.CHK_PIG5 = CHK_PIG5;
    }

    public String getCHK_PIG6() {
        return CHK_PIG6;
    }

    public void setCHK_PIG6(String CHK_PIG6) {
        this.CHK_PIG6 = CHK_PIG6;
    }

    public String getCHK_PIG7() {
        return CHK_PIG7;
    }

    public void setCHK_PIG7(String CHK_PIG7) {
        this.CHK_PIG7 = CHK_PIG7;
    }

    public String getCHK_PIG8() {
        return CHK_PIG8;
    }

    public void setCHK_PIG8(String CHK_PIG8) {
        this.CHK_PIG8 = CHK_PIG8;
    }

    public String getCHK_PIG9() {
        return CHK_PIG9;
    }

    public void setCHK_PIG9(String CHK_PIG9) {
        this.CHK_PIG9 = CHK_PIG9;
    }

    public String getCHK_PIG10() {
        return CHK_PIG10;
    }

    public void setCHK_PIG10(String CHK_PIG10) {
        this.CHK_PIG10 = CHK_PIG10;
    }

    public String getUP_DATE() {
        return UP_DATE;
    }

    public void setUP_DATE(String UP_DATE) {
        this.UP_DATE = UP_DATE;
    }

    public String getUP_USER() {
        return UP_USER;
    }

    public void setUP_USER(String UP_USER) {
        this.UP_USER = UP_USER;
    }

    public String getINS_NO() {
        return INS_NO;
    }

    public void setINS_NO(String INS_NO) {
        this.INS_NO = INS_NO;
    }

    public String getMCS_NO() {
        return MCS_NO;
    }

    public void setMCS_NO(String MCS_NO) {
        this.MCS_NO = MCS_NO;
    }

    public String getBTC_NO() {
        return BTC_NO;
    }

    public void setBTC_NO(String BTC_NO) {
        this.BTC_NO = BTC_NO;
    }

    public String getBTC_BATCH_NO() {
        return BTC_BATCH_NO;
    }

    public void setBTC_BATCH_NO(String BTC_BATCH_NO) {
        this.BTC_BATCH_NO = BTC_BATCH_NO;
    }

    public String getNOTE_NO() {
        return NOTE_NO;
    }

    public void setNOTE_NO(String NOTE_NO) {
        this.NOTE_NO = NOTE_NO;
    }

    public String getNOTE_MTR1() {
        return NOTE_MTR1;
    }

    public void setNOTE_MTR1(String NOTE_MTR1) {
        this.NOTE_MTR1 = NOTE_MTR1;
    }

    public String getNOTE_MTR2() {
        return NOTE_MTR2;
    }

    public void setNOTE_MTR2(String NOTE_MTR2) {
        this.NOTE_MTR2 = NOTE_MTR2;
    }

    public String getNOTE_MTR3() {
        return NOTE_MTR3;
    }

    public void setNOTE_MTR3(String NOTE_MTR3) {
        this.NOTE_MTR3 = NOTE_MTR3;
    }

    public String getNOTE_MTR4() {
        return NOTE_MTR4;
    }

    public void setNOTE_MTR4(String NOTE_MTR4) {
        this.NOTE_MTR4 = NOTE_MTR4;
    }

    public String getNOTE_MTR5() {
        return NOTE_MTR5;
    }

    public void setNOTE_MTR5(String NOTE_MTR5) {
        this.NOTE_MTR5 = NOTE_MTR5;
    }

    public String getNOTE_MTR6() {
        return NOTE_MTR6;
    }

    public void setNOTE_MTR6(String NOTE_MTR6) {
        this.NOTE_MTR6 = NOTE_MTR6;
    }

    public String getNOTE_MTR7() {
        return NOTE_MTR7;
    }

    public void setNOTE_MTR7(String NOTE_MTR7) {
        this.NOTE_MTR7 = NOTE_MTR7;
    }

    public String getNOTE_MTR8() {
        return NOTE_MTR8;
    }

    public void setNOTE_MTR8(String NOTE_MTR8) {
        this.NOTE_MTR8 = NOTE_MTR8;
    }

    public String getNOTE_MTR9() {
        return NOTE_MTR9;
    }

    public void setNOTE_MTR9(String NOTE_MTR9) {
        this.NOTE_MTR9 = NOTE_MTR9;
    }

    public String getNOTE_MTR10() {
        return NOTE_MTR10;
    }

    public void setNOTE_MTR10(String NOTE_MTR10) {
        this.NOTE_MTR10 = NOTE_MTR10;
    }

    public String getNOTE_CHEM1() {
        return NOTE_CHEM1;
    }

    public void setNOTE_CHEM1(String NOTE_CHEM1) {
        this.NOTE_CHEM1 = NOTE_CHEM1;
    }

    public String getNOTE_CHEM2() {
        return NOTE_CHEM2;
    }

    public void setNOTE_CHEM2(String NOTE_CHEM2) {
        this.NOTE_CHEM2 = NOTE_CHEM2;
    }

    public String getNOTE_CHEM3() {
        return NOTE_CHEM3;
    }

    public void setNOTE_CHEM3(String NOTE_CHEM3) {
        this.NOTE_CHEM3 = NOTE_CHEM3;
    }

    public String getNOTE_CHEM4() {
        return NOTE_CHEM4;
    }

    public void setNOTE_CHEM4(String NOTE_CHEM4) {
        this.NOTE_CHEM4 = NOTE_CHEM4;
    }

    public String getNOTE_CHEM5() {
        return NOTE_CHEM5;
    }

    public void setNOTE_CHEM5(String NOTE_CHEM5) {
        this.NOTE_CHEM5 = NOTE_CHEM5;
    }

    public String getNOTE_CHEM6() {
        return NOTE_CHEM6;
    }

    public void setNOTE_CHEM6(String NOTE_CHEM6) {
        this.NOTE_CHEM6 = NOTE_CHEM6;
    }

    public String getNOTE_CHEM7() {
        return NOTE_CHEM7;
    }

    public void setNOTE_CHEM7(String NOTE_CHEM7) {
        this.NOTE_CHEM7 = NOTE_CHEM7;
    }

    public String getNOTE_CHEM8() {
        return NOTE_CHEM8;
    }

    public void setNOTE_CHEM8(String NOTE_CHEM8) {
        this.NOTE_CHEM8 = NOTE_CHEM8;
    }

    public String getNOTE_CHEM9() {
        return NOTE_CHEM9;
    }

    public void setNOTE_CHEM9(String NOTE_CHEM9) {
        this.NOTE_CHEM9 = NOTE_CHEM9;
    }

    public String getNOTE_CHEM10() {
        return NOTE_CHEM10;
    }

    public void setNOTE_CHEM10(String NOTE_CHEM10) {
        this.NOTE_CHEM10 = NOTE_CHEM10;
    }

    public String getNOTE_CHEM11() {
        return NOTE_CHEM11;
    }

    public void setNOTE_CHEM11(String NOTE_CHEM11) {
        this.NOTE_CHEM11 = NOTE_CHEM11;
    }

    public String getNOTE_CHEM12() {
        return NOTE_CHEM12;
    }

    public void setNOTE_CHEM12(String NOTE_CHEM12) {
        this.NOTE_CHEM12 = NOTE_CHEM12;
    }

    public String getNOTE_CHEM13() {
        return NOTE_CHEM13;
    }

    public void setNOTE_CHEM13(String NOTE_CHEM13) {
        this.NOTE_CHEM13 = NOTE_CHEM13;
    }

    public String getNOTE_CHEM14() {
        return NOTE_CHEM14;
    }

    public void setNOTE_CHEM14(String NOTE_CHEM14) {
        this.NOTE_CHEM14 = NOTE_CHEM14;
    }

    public String getNOTE_CHEM15() {
        return NOTE_CHEM15;
    }

    public void setNOTE_CHEM15(String NOTE_CHEM15) {
        this.NOTE_CHEM15 = NOTE_CHEM15;
    }

    public String getNOTE_CHEM16() {
        return NOTE_CHEM16;
    }

    public void setNOTE_CHEM16(String NOTE_CHEM16) {
        this.NOTE_CHEM16 = NOTE_CHEM16;
    }

    public String getNOTE_CHEM17() {
        return NOTE_CHEM17;
    }

    public void setNOTE_CHEM17(String NOTE_CHEM17) {
        this.NOTE_CHEM17 = NOTE_CHEM17;
    }

    public String getNOTE_CHEM18() {
        return NOTE_CHEM18;
    }

    public void setNOTE_CHEM18(String NOTE_CHEM18) {
        this.NOTE_CHEM18 = NOTE_CHEM18;
    }

    public String getNOTE_CHEM19() {
        return NOTE_CHEM19;
    }

    public void setNOTE_CHEM19(String NOTE_CHEM19) {
        this.NOTE_CHEM19 = NOTE_CHEM19;
    }

    public String getNOTE_CHEM20() {
        return NOTE_CHEM20;
    }

    public void setNOTE_CHEM20(String NOTE_CHEM20) {
        this.NOTE_CHEM20 = NOTE_CHEM20;
    }

    public String getNOTE_PIG1() {
        return NOTE_PIG1;
    }

    public void setNOTE_PIG1(String NOTE_PIG1) {
        this.NOTE_PIG1 = NOTE_PIG1;
    }

    public String getNOTE_PIG2() {
        return NOTE_PIG2;
    }

    public void setNOTE_PIG2(String NOTE_PIG2) {
        this.NOTE_PIG2 = NOTE_PIG2;
    }

    public String getNOTE_PIG3() {
        return NOTE_PIG3;
    }

    public void setNOTE_PIG3(String NOTE_PIG3) {
        this.NOTE_PIG3 = NOTE_PIG3;
    }

    public String getNOTE_PIG4() {
        return NOTE_PIG4;
    }

    public void setNOTE_PIG4(String NOTE_PIG4) {
        this.NOTE_PIG4 = NOTE_PIG4;
    }

    public String getNOTE_PIG5() {
        return NOTE_PIG5;
    }

    public void setNOTE_PIG5(String NOTE_PIG5) {
        this.NOTE_PIG5 = NOTE_PIG5;
    }

    public String getNOTE_PIG6() {
        return NOTE_PIG6;
    }

    public void setNOTE_PIG6(String NOTE_PIG6) {
        this.NOTE_PIG6 = NOTE_PIG6;
    }

    public String getNOTE_PIG7() {
        return NOTE_PIG7;
    }

    public void setNOTE_PIG7(String NOTE_PIG7) {
        this.NOTE_PIG7 = NOTE_PIG7;
    }

    public String getNOTE_PIG8() {
        return NOTE_PIG8;
    }

    public void setNOTE_PIG8(String NOTE_PIG8) {
        this.NOTE_PIG8 = NOTE_PIG8;
    }

    public String getNOTE_PIG9() {
        return NOTE_PIG9;
    }

    public void setNOTE_PIG9(String NOTE_PIG9) {
        this.NOTE_PIG9 = NOTE_PIG9;
    }

    public String getNOTE_PIG10() {
        return NOTE_PIG10;
    }

    public void setNOTE_PIG10(String NOTE_PIG10) {
        this.NOTE_PIG10 = NOTE_PIG10;
    }
}
