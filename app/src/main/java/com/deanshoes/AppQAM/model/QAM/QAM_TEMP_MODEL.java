package com.deanshoes.AppQAM.model.QAM;

import java.io.Serializable;

public class QAM_TEMP_MODEL implements Serializable {

    private String MAY;
    private String CHECK_HOURS;
    private String CA ;
    //private String UP_DATE;
    private String CHECK_DATE;
    private String NDTCT;
    private String TCTGT ;
    private String NDTCP ;
    private String TCTGP;
    private String HTT ;
    private String HTP ;
    private String HT;

    private String HT1 ;
    private String HT2 ;
    private String HT3 ;
    private String HT4 ;
    private String HT5 ;
    private String HT6 ;
    private String HT7;



    private String HT8;
    private String HT9;
    private String HT10;
    private String HT11 ;
    private String HT12 ;
    private String HT13 ;
    private String HT14 ;
    private String HT15;
    private String HT16 ;


    private String SM1T ;
    private String SM1D ;
    private String SM2T ;
    private String SM2D ;
    private String SM3T ;
    private String SM3D ;
    private String SM4T;
    private String SM4D;
    private String SM5T;
    private String SM5D;
    private String SM6T ;
    private String SM6D ;
    private String SM7T ;
    private String SM7D ;
    private String SM8T ;
    private String SM8D ;

    private String NDM1TT ;
    private String NDM1TP  ;
    private String NDM1DT ;
    private String NDM1DP ;
    private String NDM2TT ;

    private String NDM2TP ;
    private String NDM2DT ;
    private String NDM2DP ;
    private String NDM3TT ;
    private String NDM3TP ;
    private String NDM3DT ;
    private String NDM3DP ;
    private String NDM4TT ;
    private String NDM4TP ;
    private String NDM4DT ;
    private String NDM4DP  ;

    private String NDM5TT  ;
    private String NDM5TP  ;
    private String NDM5DT  ;
    private String NDM5DP  ;
    private String NDM6TT ;
    private String NDM6TP;
    private String NDM6DT ;
    private String NDM6DP ;
    private String NDM7TT  ;
    private String NDM7TP  ;
    private String NDM7DT  ;
    private String NDM7DP  ;
    private String NDM8TT  ;
    private String NDM8TP  ;
    private String NDM8DT  ;
    private String NDM8DP   ;
    private String SET_TIMEM1T   ;
    private String SET_TIMEM1D   ;
    private String SET_TIMEM2T   ;

    private String SET_TIMEM2D    ;
    private String SET_TIMEM3T    ;
    private String SET_TIMEM3D    ;
    private String SET_TIMEM4T    ;
    private String SET_TIMEM4D    ;
    private String SET_TIMEM5T ;
    private String SET_TIMEM5D  ;
    private String SET_TIMEM6T   ;
    private String SET_TIMEM6D   ;
    private String SET_TIMEM7T    ;
    private String SET_TIMEM7D  ;
    private String SET_TIMEM8T  ;
    private String SET_TIMEM8D    ;

    private String ID    ;

    public String getCHECK_HOURS() {
        return CHECK_HOURS;
    }

    public void setCHECK_HOURS(String CHECK_HOURS) {
        this.CHECK_HOURS = CHECK_HOURS;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMAY() {
        return MAY;
    }

    public void setMAY(String MAY) {
        this.MAY = MAY;
    }

    public String getCA() {
        return CA;
    }

    public void setCA(String CA) {
        this.CA = CA;
    }

    public String getHT() {
        return HT;
    }

    public void setHT(String HT) {
        this.HT = HT;
    }

//    public String getUP_DATE() {
//        return UP_DATE;
//    }
//
//    public void setUP_DATE(String UP_DATE) {
//        this.UP_DATE = UP_DATE;
//    }

    public String getCHECK_DATE() {
        return CHECK_DATE;
    }

    public void setCHECK_DATE(String CHECK_DATE) {
        this.CHECK_DATE = CHECK_DATE;
    }

    public String getNDTCT() {
        return NDTCT;
    }

    public void setNDTCT(String NDTCT) {
        this.NDTCT = NDTCT;
    }

    public String getTCTGT() {
        return TCTGT;
    }

    public void setTCTGT(String TCTGT) {
        this.TCTGT = TCTGT;
    }

    public String getNDTCP() {
        return NDTCP;
    }

    public void setNDTCP(String NDTCP) {
        this.NDTCP = NDTCP;
    }

    public String getTCTGP() {
        return TCTGP;
    }

    public void setTCTGP(String TCTGP) {
        this.TCTGP = TCTGP;
    }

    public String getHTT() {
        return HTT;
    }

    public void setHTT(String HTT) {
        this.HTT = HTT;
    }

    public String getHTP() {
        return HTP;
    }

    public void setHTP(String HTP) {
        this.HTP = HTP;
    }



    public String getHT1() {
        return HT1;
    }

    public void setHT1(String HT1) {
        this.HT1 = HT1;
    }

    public String getHT2() {
        return HT2;
    }

    public void setHT2(String HT2) {
        this.HT2 = HT2;
    }

    public String getHT3() {
        return HT3;
    }

    public void setHT3(String HT3) {
        this.HT3 = HT3;
    }

    public String getHT4() {
        return HT4;
    }

    public void setHT4(String HT4) {
        this.HT4 = HT4;
    }

    public String getHT5() {
        return HT5;
    }

    public void setHT5(String HT5) {
        this.HT5 = HT5;
    }

    public String getHT6() {
        return HT6;
    }

    public void setHT6(String HT6) {
        this.HT6 = HT6;
    }

    public String getHT7() {
        return HT7;
    }

    public void setHT7(String HT7) {
        this.HT7 = HT7;
    }

    public String getHT8() {
        return HT8;
    }

    public void setHT8(String HT8) {
        this.HT8 = HT8;
    }

    public String getHT9() {
        return HT9;
    }

    public void setHT9(String HT9) {
        this.HT9 = HT9;
    }

    public String getHT10() {
        return HT10;
    }

    public void setHT10(String HT10) {
        this.HT10 = HT10;
    }

    public String getHT11() {
        return HT11;
    }

    public void setHT11(String HT11) {
        this.HT11 = HT11;
    }

    public String getHT12() {
        return HT12;
    }

    public void setHT12(String HT12) {
        this.HT12 = HT12;
    }

    public String getHT13() {
        return HT13;
    }

    public void setHT13(String HT13) {
        this.HT13 = HT13;
    }

    public String getHT14() {
        return HT14;
    }

    public void setHT14(String HT14) {
        this.HT14 = HT14;
    }

    public String getHT15() {
        return HT15;
    }

    public void setHT15(String HT15) {
        this.HT15 = HT15;
    }

    public String getHT16() {
        return HT16;
    }

    public void setHT16(String HT16) {
        this.HT16 = HT16;
    }

    public String getSM1T() {
        return SM1T;
    }

    public void setSM1T(String SM1T) {
        this.SM1T = SM1T;
    }

    public String getSM1D() {
        return SM1D;
    }

    public void setSM1D(String SM1D) {
        this.SM1D = SM1D;
    }

    public String getSM2T() {
        return SM2T;
    }

    public void setSM2T(String SM2T) {
        this.SM2T = SM2T;
    }

    public String getSM2D() {
        return SM2D;
    }

    public void setSM2D(String SM2D) {
        this.SM2D = SM2D;
    }

    public String getSM3T() {
        return SM3T;
    }

    public void setSM3T(String SM3T) {
        this.SM3T = SM3T;
    }

    public String getSM3D() {
        return SM3D;
    }

    public void setSM3D(String SM3D) {
        this.SM3D = SM3D;
    }

    public String getSM4T() {
        return SM4T;
    }

    public void setSM4T(String SM4T) {
        this.SM4T = SM4T;
    }

    public String getSM4D() {
        return SM4D;
    }

    public void setSM4D(String SM4D) {
        this.SM4D = SM4D;
    }

    public String getSM5T() {
        return SM5T;
    }

    public void setSM5T(String SM5T) {
        this.SM5T = SM5T;
    }

    public String getSM5D() {
        return SM5D;
    }

    public void setSM5D(String SM5D) {
        this.SM5D = SM5D;
    }

    public String getSM6T() {
        return SM6T;
    }

    public void setSM6T(String SM6T) {
        this.SM6T = SM6T;
    }

    public String getSM6D() {
        return SM6D;
    }

    public void setSM6D(String SM6D) {
        this.SM6D = SM6D;
    }

    public String getSM7T() {
        return SM7T;
    }

    public void setSM7T(String SM7T) {
        this.SM7T = SM7T;
    }

    public String getSM7D() {
        return SM7D;
    }

    public void setSM7D(String SM7D) {
        this.SM7D = SM7D;
    }

    public String getSM8T() {
        return SM8T;
    }

    public void setSM8T(String SM8T) {
        this.SM8T = SM8T;
    }

    public String getSM8D() {
        return SM8D;
    }

    public void setSM8D(String SM8D) {
        this.SM8D = SM8D;
    }

    public String getNDM1TT() {
        return NDM1TT;
    }

    public void setNDM1TT(String NDM1TT) {
        this.NDM1TT = NDM1TT;
    }

    public String getNDM1TP() {
        return NDM1TP;
    }

    public void setNDM1TP(String NDM1TP) {
        this.NDM1TP = NDM1TP;
    }

    public String getNDM1DT() {
        return NDM1DT;
    }

    public void setNDM1DT(String NDM1DT) {
        this.NDM1DT = NDM1DT;
    }

    public String getNDM1DP() {
        return NDM1DP;
    }

    public void setNDM1DP(String NDM1DP) {
        this.NDM1DP = NDM1DP;
    }

    public String getNDM2TT() {
        return NDM2TT;
    }

    public void setNDM2TT(String NDM2TT) {
        this.NDM2TT = NDM2TT;
    }

    public String getNDM2TP() {
        return NDM2TP;
    }

    public void setNDM2TP(String NDM2TP) {
        this.NDM2TP = NDM2TP;
    }

    public String getNDM2DT() {
        return NDM2DT;
    }

    public void setNDM2DT(String NDM2DT) {
        this.NDM2DT = NDM2DT;
    }

    public String getNDM2DP() {
        return NDM2DP;
    }

    public void setNDM2DP(String NDM2DP) {
        this.NDM2DP = NDM2DP;
    }

    public String getNDM3TT() {
        return NDM3TT;
    }

    public void setNDM3TT(String NDM3TT) {
        this.NDM3TT = NDM3TT;
    }

    public String getNDM3TP() {
        return NDM3TP;
    }

    public void setNDM3TP(String NDM3TP) {
        this.NDM3TP = NDM3TP;
    }

    public String getNDM3DT() {
        return NDM3DT;
    }

    public void setNDM3DT(String NDM3DT) {
        this.NDM3DT = NDM3DT;
    }

    public String getNDM3DP() {
        return NDM3DP;
    }

    public void setNDM3DP(String NDM3DP) {
        this.NDM3DP = NDM3DP;
    }

    public String getNDM4TT() {
        return NDM4TT;
    }

    public void setNDM4TT(String NDM4TT) {
        this.NDM4TT = NDM4TT;
    }

    public String getNDM4TP() {
        return NDM4TP;
    }

    public void setNDM4TP(String NDM4TP) {
        this.NDM4TP = NDM4TP;
    }

    public String getNDM4DT() {
        return NDM4DT;
    }

    public void setNDM4DT(String NDM4DT) {
        this.NDM4DT = NDM4DT;
    }

    public String getNDM4DP() {
        return NDM4DP;
    }

    public void setNDM4DP(String NDM4DP) {
        this.NDM4DP = NDM4DP;
    }

    public String getNDM5TT() {
        return NDM5TT;
    }

    public void setNDM5TT(String NDM5TT) {
        this.NDM5TT = NDM5TT;
    }

    public String getNDM5TP() {
        return NDM5TP;
    }

    public void setNDM5TP(String NDM5TP) {
        this.NDM5TP = NDM5TP;
    }

    public String getNDM5DT() {
        return NDM5DT;
    }

    public void setNDM5DT(String NDM5DT) {
        this.NDM5DT = NDM5DT;
    }

    public String getNDM5DP() {
        return NDM5DP;
    }

    public void setNDM5DP(String NDM5DP) {
        this.NDM5DP = NDM5DP;
    }

    public String getNDM6TT() {
        return NDM6TT;
    }

    public void setNDM6TT(String NDM6TT) {
        this.NDM6TT = NDM6TT;
    }

    public String getNDM6TP() {
        return NDM6TP;
    }

    public void setNDM6TP(String NDM6TP) {
        this.NDM6TP = NDM6TP;
    }

    public String getNDM6DT() {
        return NDM6DT;
    }

    public void setNDM6DT(String NDM6DT) {
        this.NDM6DT = NDM6DT;
    }

    public String getNDM6DP() {
        return NDM6DP;
    }

    public void setNDM6DP(String NDM6DP) {
        this.NDM6DP = NDM6DP;
    }

    public String getNDM7TT() {
        return NDM7TT;
    }

    public void setNDM7TT(String NDM7TT) {
        this.NDM7TT = NDM7TT;
    }

    public String getNDM7TP() {
        return NDM7TP;
    }

    public void setNDM7TP(String NDM7TP) {
        this.NDM7TP = NDM7TP;
    }

    public String getNDM7DT() {
        return NDM7DT;
    }

    public void setNDM7DT(String NDM7DT) {
        this.NDM7DT = NDM7DT;
    }

    public String getNDM7DP() {
        return NDM7DP;
    }

    public void setNDM7DP(String NDM7DP) {
        this.NDM7DP = NDM7DP;
    }

    public String getNDM8TT() {
        return NDM8TT;
    }

    public void setNDM8TT(String NDM8TT) {
        this.NDM8TT = NDM8TT;
    }

    public String getNDM8TP() {
        return NDM8TP;
    }

    public void setNDM8TP(String NDM8TP) {
        this.NDM8TP = NDM8TP;
    }

    public String getNDM8DT() {
        return NDM8DT;
    }

    public void setNDM8DT(String NDM8DT) {
        this.NDM8DT = NDM8DT;
    }

    public String getNDM8DP() {
        return NDM8DP;
    }

    public void setNDM8DP(String NDM8DP) {
        this.NDM8DP = NDM8DP;
    }

    public String getSET_TIMEM1T() {
        return SET_TIMEM1T;
    }

    public void setSET_TIMEM1T(String SET_TIMEM1T) {
        this.SET_TIMEM1T = SET_TIMEM1T;
    }

    public String getSET_TIMEM1D() {
        return SET_TIMEM1D;
    }

    public void setSET_TIMEM1D(String SET_TIMEM1D) {
        this.SET_TIMEM1D = SET_TIMEM1D;
    }

    public String getSET_TIMEM2T() {
        return SET_TIMEM2T;
    }

    public void setSET_TIMEM2T(String SET_TIMEM2T) {
        this.SET_TIMEM2T = SET_TIMEM2T;
    }

    public String getSET_TIMEM2D() {
        return SET_TIMEM2D;
    }

    public void setSET_TIMEM2D(String SET_TIMEM2D) {
        this.SET_TIMEM2D = SET_TIMEM2D;
    }

    public String getSET_TIMEM3T() {
        return SET_TIMEM3T;
    }

    public void setSET_TIMEM3T(String SET_TIMEM3T) {
        this.SET_TIMEM3T = SET_TIMEM3T;
    }

    public String getSET_TIMEM3D() {
        return SET_TIMEM3D;
    }

    public void setSET_TIMEM3D(String SET_TIMEM3D) {
        this.SET_TIMEM3D = SET_TIMEM3D;
    }

    public String getSET_TIMEM4T() {
        return SET_TIMEM4T;
    }

    public void setSET_TIMEM4T(String SET_TIMEM4T) {
        this.SET_TIMEM4T = SET_TIMEM4T;
    }

    public String getSET_TIMEM4D() {
        return SET_TIMEM4D;
    }

    public void setSET_TIMEM4D(String SET_TIMEM4D) {
        this.SET_TIMEM4D = SET_TIMEM4D;
    }

    public String getSET_TIMEM5T() {
        return SET_TIMEM5T;
    }

    public void setSET_TIMEM5T(String SET_TIMEM5T) {
        this.SET_TIMEM5T = SET_TIMEM5T;
    }

    public String getSET_TIMEM5D() {
        return SET_TIMEM5D;
    }

    public void setSET_TIMEM5D(String SET_TIMEM5D) {
        this.SET_TIMEM5D = SET_TIMEM5D;
    }

    public String getSET_TIMEM6T() {
        return SET_TIMEM6T;
    }

    public void setSET_TIMEM6T(String SET_TIMEM6T) {
        this.SET_TIMEM6T = SET_TIMEM6T;
    }

    public String getSET_TIMEM6D() {
        return SET_TIMEM6D;
    }

    public void setSET_TIMEM6D(String SET_TIMEM6D) {
        this.SET_TIMEM6D = SET_TIMEM6D;
    }

    public String getSET_TIMEM7T() {
        return SET_TIMEM7T;
    }

    public void setSET_TIMEM7T(String SET_TIMEM7T) {
        this.SET_TIMEM7T = SET_TIMEM7T;
    }

    public String getSET_TIMEM7D() {
        return SET_TIMEM7D;
    }

    public void setSET_TIMEM7D(String SET_TIMEM7D) {
        this.SET_TIMEM7D = SET_TIMEM7D;
    }

    public String getSET_TIMEM8T() {
        return SET_TIMEM8T;
    }

    public void setSET_TIMEM8T(String SET_TIMEM8T) {
        this.SET_TIMEM8T = SET_TIMEM8T;
    }

    public String getSET_TIMEM8D() {
        return SET_TIMEM8D;
    }

    public void setSET_TIMEM8D(String SET_TIMEM8D) {
        this.SET_TIMEM8D = SET_TIMEM8D;
    }

}