package com.deanshoes.AppQAM.model.QAM;

public class QAM_PIG {
    private String PIG_NO;
    private String PIG;
    private String PIG_WEIGHT;
    private String UP_DATE;
    private String UP_USER;

    public String getPIG_NO() {
        return PIG_NO;
    }

    public void setPIG_NO(String PIG_NO) {
        this.PIG_NO = PIG_NO;
    }

    public String getPIG() {
        return PIG;
    }

    public void setPIG(String PIG) {
        this.PIG = PIG;
    }

    public String getPIG_WEIGHT() {
        return PIG_WEIGHT;
    }

    public void setPIG_WEIGHT(String PIG_WEIGHT) {
        this.PIG_WEIGHT = PIG_WEIGHT;
    }

    public String getUP_DATE() {
        return UP_DATE;
    }

    public void setUP_DATE(String UP_DATE) {
        this.UP_DATE = UP_DATE;
    }

    public String getUP_USER() {
        return UP_USER;
    }

    public void setUP_USER(String UP_USER) {
        this.UP_USER = UP_USER;
    }
}
