package com.deanshoes.AppQAM.model.QAM;

public class QAM_NOTE {
    private String NOTE_NO;
    private String INS_NO;
    private String BTC_NO;
    private String NOTE_MTR1;
    private String NOTE_MTR2;
    private String NOTE_MTR3;
    private String NOTE_MTR4;
    private String NOTE_MTR5;
    private String NOTE_MTR6;
    private String NOTE_MTR7;
    private String NOTE_MTR8;
    private String NOTE_MTR9;
    private String NOTE_MTR10;
    private String NOTE_CHEM1;
    private String NOTE_CHEM2;
    private String NOTE_CHEM3;
    private String NOTE_CHEM4;
    private String NOTE_CHEM5;
    private String NOTE_CHEM6;
    private String NOTE_CHEM7;
    private String NOTE_CHEM8;
    private String NOTE_CHEM9;
    private String NOTE_CHEM10;
    private String NOTE_CHEM11;
    private String NOTE_CHEM12;
    private String NOTE_CHEM13;
    private String NOTE_CHEM14;
    private String NOTE_CHEM15;
    private String NOTE_CHEM16;
    private String NOTE_CHEM17;
    private String NOTE_CHEM18;
    private String NOTE_CHEM19;
    private String NOTE_CHEM20;
    private String NOTE_PIG1;
    private String NOTE_PIG2;
    private String NOTE_PIG3;
    private String NOTE_PIG4;
    private String NOTE_PIG5;
    private String NOTE_PIG6;
    private String NOTE_PIG7;
    private String NOTE_PIG8;
    private String NOTE_PIG9;
    private String NOTE_PIG10;
    private String UP_USER;
    private String AMT_NO;

    public String getNOTE_NO() {
        return NOTE_NO;
    }

    public void setNOTE_NO(String NOTE_NO) {
        this.NOTE_NO = NOTE_NO;
    }

    public String getINS_NO() {
        return INS_NO;
    }

    public void setINS_NO(String INS_NO) {
        this.INS_NO = INS_NO;
    }

    public String getBTC_NO() {
        return BTC_NO;
    }

    public void setBTC_NO(String BTC_NO) {
        this.BTC_NO = BTC_NO;
    }

    public String getNOTE_MTR1() {
        return NOTE_MTR1;
    }

    public void setNOTE_MTR1(String NOTE_MTR1) {
        this.NOTE_MTR1 = NOTE_MTR1;
    }

    public String getNOTE_MTR2() {
        return NOTE_MTR2;
    }

    public void setNOTE_MTR2(String NOTE_MTR2) {
        this.NOTE_MTR2 = NOTE_MTR2;
    }

    public String getNOTE_MTR3() {
        return NOTE_MTR3;
    }

    public void setNOTE_MTR3(String NOTE_MTR3) {
        this.NOTE_MTR3 = NOTE_MTR3;
    }

    public String getNOTE_MTR4() {
        return NOTE_MTR4;
    }

    public void setNOTE_MTR4(String NOTE_MTR4) {
        this.NOTE_MTR4 = NOTE_MTR4;
    }

    public String getNOTE_MTR5() {
        return NOTE_MTR5;
    }

    public void setNOTE_MTR5(String NOTE_MTR5) {
        this.NOTE_MTR5 = NOTE_MTR5;
    }

    public String getNOTE_MTR6() {
        return NOTE_MTR6;
    }

    public void setNOTE_MTR6(String NOTE_MTR6) {
        this.NOTE_MTR6 = NOTE_MTR6;
    }

    public String getNOTE_MTR7() {
        return NOTE_MTR7;
    }

    public void setNOTE_MTR7(String NOTE_MTR7) {
        this.NOTE_MTR7 = NOTE_MTR7;
    }

    public String getNOTE_MTR8() {
        return NOTE_MTR8;
    }

    public void setNOTE_MTR8(String NOTE_MTR8) {
        this.NOTE_MTR8 = NOTE_MTR8;
    }

    public String getNOTE_MTR9() {
        return NOTE_MTR9;
    }

    public void setNOTE_MTR9(String NOTE_MTR9) {
        this.NOTE_MTR9 = NOTE_MTR9;
    }

    public String getNOTE_MTR10() {
        return NOTE_MTR10;
    }

    public void setNOTE_MTR10(String NOTE_MTR10) {
        this.NOTE_MTR10 = NOTE_MTR10;
    }

    public String getNOTE_CHEM1() {
        return NOTE_CHEM1;
    }

    public void setNOTE_CHEM1(String NOTE_CHEM1) {
        this.NOTE_CHEM1 = NOTE_CHEM1;
    }

    public String getNOTE_CHEM2() {
        return NOTE_CHEM2;
    }

    public void setNOTE_CHEM2(String NOTE_CHEM2) {
        this.NOTE_CHEM2 = NOTE_CHEM2;
    }

    public String getNOTE_CHEM3() {
        return NOTE_CHEM3;
    }

    public void setNOTE_CHEM3(String NOTE_CHEM3) {
        this.NOTE_CHEM3 = NOTE_CHEM3;
    }

    public String getNOTE_CHEM4() {
        return NOTE_CHEM4;
    }

    public void setNOTE_CHEM4(String NOTE_CHEM4) {
        this.NOTE_CHEM4 = NOTE_CHEM4;
    }

    public String getNOTE_CHEM5() {
        return NOTE_CHEM5;
    }

    public void setNOTE_CHEM5(String NOTE_CHEM5) {
        this.NOTE_CHEM5 = NOTE_CHEM5;
    }

    public String getNOTE_CHEM6() {
        return NOTE_CHEM6;
    }

    public void setNOTE_CHEM6(String NOTE_CHEM6) {
        this.NOTE_CHEM6 = NOTE_CHEM6;
    }

    public String getNOTE_CHEM7() {
        return NOTE_CHEM7;
    }

    public void setNOTE_CHEM7(String NOTE_CHEM7) {
        this.NOTE_CHEM7 = NOTE_CHEM7;
    }

    public String getNOTE_CHEM8() {
        return NOTE_CHEM8;
    }

    public void setNOTE_CHEM8(String NOTE_CHEM8) {
        this.NOTE_CHEM8 = NOTE_CHEM8;
    }

    public String getNOTE_CHEM9() {
        return NOTE_CHEM9;
    }

    public void setNOTE_CHEM9(String NOTE_CHEM9) {
        this.NOTE_CHEM9 = NOTE_CHEM9;
    }

    public String getNOTE_CHEM10() {
        return NOTE_CHEM10;
    }

    public void setNOTE_CHEM10(String NOTE_CHEM10) {
        this.NOTE_CHEM10 = NOTE_CHEM10;
    }

    public String getNOTE_CHEM11() {
        return NOTE_CHEM11;
    }

    public void setNOTE_CHEM11(String NOTE_CHEM11) {
        this.NOTE_CHEM11 = NOTE_CHEM11;
    }

    public String getNOTE_CHEM12() {
        return NOTE_CHEM12;
    }

    public void setNOTE_CHEM12(String NOTE_CHEM12) {
        this.NOTE_CHEM12 = NOTE_CHEM12;
    }

    public String getNOTE_CHEM13() {
        return NOTE_CHEM13;
    }

    public void setNOTE_CHEM13(String NOTE_CHEM13) {
        this.NOTE_CHEM13 = NOTE_CHEM13;
    }

    public String getNOTE_CHEM14() {
        return NOTE_CHEM14;
    }

    public void setNOTE_CHEM14(String NOTE_CHEM14) {
        this.NOTE_CHEM14 = NOTE_CHEM14;
    }

    public String getNOTE_CHEM15() {
        return NOTE_CHEM15;
    }

    public void setNOTE_CHEM15(String NOTE_CHEM15) {
        this.NOTE_CHEM15 = NOTE_CHEM15;
    }

    public String getNOTE_CHEM16() {
        return NOTE_CHEM16;
    }

    public void setNOTE_CHEM16(String NOTE_CHEM16) {
        this.NOTE_CHEM16 = NOTE_CHEM16;
    }

    public String getNOTE_CHEM17() {
        return NOTE_CHEM17;
    }

    public void setNOTE_CHEM17(String NOTE_CHEM17) {
        this.NOTE_CHEM17 = NOTE_CHEM17;
    }

    public String getNOTE_CHEM18() {
        return NOTE_CHEM18;
    }

    public void setNOTE_CHEM18(String NOTE_CHEM18) {
        this.NOTE_CHEM18 = NOTE_CHEM18;
    }

    public String getNOTE_CHEM19() {
        return NOTE_CHEM19;
    }

    public void setNOTE_CHEM19(String NOTE_CHEM19) {
        this.NOTE_CHEM19 = NOTE_CHEM19;
    }

    public String getNOTE_CHEM20() {
        return NOTE_CHEM20;
    }

    public void setNOTE_CHEM20(String NOTE_CHEM20) {
        this.NOTE_CHEM20 = NOTE_CHEM20;
    }

    public String getNOTE_PIG1() {
        return NOTE_PIG1;
    }

    public void setNOTE_PIG1(String NOTE_PIG1) {
        this.NOTE_PIG1 = NOTE_PIG1;
    }

    public String getNOTE_PIG2() {
        return NOTE_PIG2;
    }

    public void setNOTE_PIG2(String NOTE_PIG2) {
        this.NOTE_PIG2 = NOTE_PIG2;
    }

    public String getNOTE_PIG3() {
        return NOTE_PIG3;
    }

    public void setNOTE_PIG3(String NOTE_PIG3) {
        this.NOTE_PIG3 = NOTE_PIG3;
    }

    public String getNOTE_PIG4() {
        return NOTE_PIG4;
    }

    public void setNOTE_PIG4(String NOTE_PIG4) {
        this.NOTE_PIG4 = NOTE_PIG4;
    }

    public String getNOTE_PIG5() {
        return NOTE_PIG5;
    }

    public void setNOTE_PIG5(String NOTE_PIG5) {
        this.NOTE_PIG5 = NOTE_PIG5;
    }

    public String getNOTE_PIG6() {
        return NOTE_PIG6;
    }

    public void setNOTE_PIG6(String NOTE_PIG6) {
        this.NOTE_PIG6 = NOTE_PIG6;
    }

    public String getNOTE_PIG7() {
        return NOTE_PIG7;
    }

    public void setNOTE_PIG7(String NOTE_PIG7) {
        this.NOTE_PIG7 = NOTE_PIG7;
    }

    public String getNOTE_PIG8() {
        return NOTE_PIG8;
    }

    public void setNOTE_PIG8(String NOTE_PIG8) {
        this.NOTE_PIG8 = NOTE_PIG8;
    }

    public String getNOTE_PIG9() {
        return NOTE_PIG9;
    }

    public void setNOTE_PIG9(String NOTE_PIG9) {
        this.NOTE_PIG9 = NOTE_PIG9;
    }

    public String getNOTE_PIG10() {
        return NOTE_PIG10;
    }

    public void setNOTE_PIG10(String NOTE_PIG10) {
        this.NOTE_PIG10 = NOTE_PIG10;
    }

    public String getUP_USER() {
        return UP_USER;
    }

    public void setUP_USER(String UP_USER) {
        this.UP_USER = UP_USER;
    }

    public String getAMT_NO() {
        return AMT_NO;
    }

    public void setAMT_NO(String AMT_NO) {
        this.AMT_NO = AMT_NO;
    }
}
