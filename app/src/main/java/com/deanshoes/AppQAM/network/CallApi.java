package com.deanshoes.AppQAM.network;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.deanshoes.AppQAM.model.QAM.BTC;
import com.deanshoes.AppQAM.model.QAM.BTC_OP;
import com.deanshoes.AppQAM.model.QAM.BTC_PELL;
import com.deanshoes.AppQAM.model.QAM.NOTE_TEMP;
import com.deanshoes.AppQAM.model.QAM.QAM_AMT;
import com.deanshoes.AppQAM.model.QAM.QAM_BTC;
import com.deanshoes.AppQAM.model.QAM.QAM_CHEM;
import com.deanshoes.AppQAM.model.QAM.QAM_CR;
import com.deanshoes.AppQAM.model.QAM.QAM_MAC;
import com.deanshoes.AppQAM.model.QAM.QAM_MCS;
import com.deanshoes.AppQAM.model.QAM.QAM_MODEL;
import com.deanshoes.AppQAM.model.QAM.QAM_MTR;
import com.deanshoes.AppQAM.model.QAM.QAM_PIG;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_HAND;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_MAC1;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_MAC2;
import com.deanshoes.AppQAM.model.QAM.QAM_TEMP_MODEL;
import com.deanshoes.dsapptools.tools.helper.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;


public class CallApi {

    /**
     * Timeout 時間
     */
    public static int SOCKET_TIMEOUT_MS = 30000;

    private static void timeoutSetup(JsonObjectRequest jsonObjReq) {

        // 加入 Timeout 設定
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    public static void getmcs(String factory, String mcs_date, String mcs_shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getmcs(factory, mcs_date, mcs_shift);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getCurrentTime(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = String.format(Locale.ENGLISH, "%s?factory=%s", ApiUrl.getCurrentTime(factory), factory);
        JSONObject params = new JSONObject();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }


    public static void getbtc(String factory, String mcs_no, String mcs_date, String mcs_shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getbtc(factory, mcs_no, mcs_date, mcs_shift);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getbtc2(String factory, String mcs_no, String mcs_shift, String mcs_date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getbtc2(factory, mcs_no, mcs_shift, mcs_date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);
        params.put("mcs_shift", mcs_shift);
        params.put("mcs_date", mcs_date);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertKneader(String factory, BTC btc, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertKneader(factory, btc);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("btc_no", btc.getBTC_NO());
        params.put("kn_temp_f", btc.getKN_TEMP_F());
        params.put("kn_time_f", btc.getKN_TIME_F());
        params.put("kn_temp_f1", btc.getKN_TEMP_1());
        params.put("kn_time_f1", btc.getKN_TIME_1());
        params.put("kn_temp_f2", btc.getKN_TEMP_2());
        params.put("kn_time_f2", btc.getKN_TIME_2());
        params.put("kn_temp_f3", btc.getKN_TEMP_3());
        params.put("kn_time_f3", btc.getKN_TIME_3());
        params.put("kn_temp_f4", btc.getKN_TEMP_4());
        params.put("kn_time_f4", btc.getKN_TIME_4());
        params.put("kn_temp_end", btc.getKN_TEMP_END());
        params.put("kn_time_end", btc.getKN_TIME_END());
        params.put("kn_note", btc.getKN_NOTE());
        params.put("up_user", btc.getUP_USER());


        params.put("kn_temp_f_note", btc.getKN_TEMP_F_NOTE());
        params.put("kn_temp_f1_note", btc.getKN_TEMP_1_NOTE());
        params.put("kn_temp_f2_note", btc.getKN_TEMP_2_NOTE());
        params.put("kn_temp_f3_note", btc.getKN_TEMP_3_NOTE());
        params.put("kn_temp_f4_note", btc.getKN_TEMP_4_NOTE());
        params.put("kn_temp_end_note", btc.getKN_TEMP_END_NOTE());

        params.put("kn_temp_f_renote", btc.getKN_TEMP_F_RENOTE());
        params.put("kn_temp_f1_renote", btc.getKN_TEMP_1_RENOTE());
        params.put("kn_temp_f2_renote", btc.getKN_TEMP_2_RENOTE());
        params.put("kn_temp_f3_renote", btc.getKN_TEMP_3_RENOTE());
        params.put("kn_temp_f4_renote", btc.getKN_TEMP_4_RENOTE());
        params.put("kn_temp_end_renote", btc.getKN_TEMP_END_RENOTE());

        params.put("kn_fstd", btc.getKN_FSTD());
        params.put("kn_1std", btc.getKN_1STD());
        params.put("kn_2std", btc.getKN_2STD());
        params.put("kn_3std", btc.getKN_3STD());
        params.put("kn_4std", btc.getKN_4STD());
        params.put("kn_estd", btc.getKN_ESTD());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getbtc_op(String factory, String mcs_no, String mcs_date, String mcs_shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getbtc_op(factory, mcs_no, mcs_date, mcs_shift);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertOpenmill(String factory, BTC_OP btc_op, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertOpenmill(factory, btc_op);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("btc_no", btc_op.getBTC_NO());
        params.put("op_temp_1", btc_op.getOP_TEMP_1());
        params.put("op_thick_1", btc_op.getOP_THICK_1());
//        params.put("op_temp_thin", btc_op.getOP_TEMP_THIN());
        params.put("op_thick_thin", btc_op.getOP_THICK_THIN());
//        params.put("op_temp_2", btc_op.getOP_TEMP_2());
        params.put("op_thick_2", btc_op.getOP_THICK_2());
        params.put("up_user", btc_op.getUP_USER());


        params.put("op_temp_1_note", btc_op.getOP_TEMP_1_NOTE());
//        params.put("op_temp_thin_note", btc_op.getOP_TEMP_THIN_NOTE());
//        params.put("op_temp_2_note", btc_op.getOP_TEMP_2_NOTE());
        params.put("op_std_temp_d", btc_op.getOP_STD_TEMP_D());
        params.put("op_std_temp_l", btc_op.getOP_STD_TEMP_L());
        params.put("op_std_thick1_d", btc_op.getOP_STD_THICK1_D());
        params.put("op_std_thick1_l", btc_op.getOP_STD_THICK1_L());
        params.put("op_std_thin_d", btc_op.getOP_STD_THIN_D());
        params.put("op_std_thin_l", btc_op.getOP_STD_THIN_L());
        params.put("op_std_thick2_d", btc_op.getOP_STD_THICK2_D());
        params.put("op_std_thick2_l", btc_op.getOP_STD_THICK2_L());
        params.put("op_thick1_note", btc_op.getOP_THICK1_NOTE());
        params.put("op_thin_note", btc_op.getOP_THIN_NOTE());
        params.put("op_thick2_note", btc_op.getOP_THICK2_NOTE());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getbtc_pell(String factory, String ins_no, String mcs_date, String mcs_shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getbtc_pell(factory, ins_no, mcs_date, mcs_shift);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertPellezation(String factory, BTC_PELL btc_pell, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertPelletization(factory, btc_pell);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("btc_no", btc_pell.getBTC_NO());
        params.put("pell_temp_1", btc_pell.getPELL_TEMP_1());
        params.put("pell_temp_2", btc_pell.getPELL_TEMP_2());
        params.put("pell_temp_3", btc_pell.getPELL_TEMP_3());
        params.put("pell_temp_head", btc_pell.getPELL_TEMP_HEAD());
        params.put("new_batch_no", btc_pell.getNEW_BATCH_NO());
        params.put("pell_mixing_time", btc_pell.getPELL_MIXING_TIME());
        params.put("pell_out_temp", btc_pell.getPELL_OUT_TEMP());
        params.put("pell_out_time", btc_pell.getPELL_OUT_TIME());
        params.put("pell_clean_time", btc_pell.getPELL_CLEAN_TIME());

        params.put("pell_temp_1_note", btc_pell.getPELL_TEMP_1_NOTE());
        params.put("pell_temp_2_note", btc_pell.getPELL_TEMP_2_NOTE());
        params.put("pell_temp_3_note", btc_pell.getPELL_TEMP_3_NOTE());
        params.put("pell_temp_head_note", btc_pell.getPELL_TEMP_HEAD_NOTE());
        params.put("pell_out_temp_note", btc_pell.getPELL_OUT_TEMP_NOTE());


        params.put("up_user", btc_pell.getUP_USER());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertPellezation_NEW(String factory, BTC_PELL btc_pell, String mcs_no, String mcs_date, String mcs_shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertPelletization_NEW(factory, btc_pell, mcs_no, mcs_date, mcs_shift);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);

        params.put("new_batch_no", btc_pell.getNEW_BATCH_NO());

        params.put("btc_no", btc_pell.getBTC_NO());
        params.put("pell_gran", btc_pell.getPELL_GRAN());
        params.put("pell_temp_1", btc_pell.getPELL_TEMP_1());
        params.put("pell_temp_2", btc_pell.getPELL_TEMP_2());
        params.put("pell_temp_3", btc_pell.getPELL_TEMP_3());
        params.put("pell_temp_5", btc_pell.getPELL_TEMP_5());
        params.put("pell_temp_head", btc_pell.getPELL_TEMP_HEAD());
        params.put("new_batch_no", btc_pell.getNEW_BATCH_NO());
        params.put("pell_mixing_time", btc_pell.getPELL_MIXING_TIME());
        params.put("pell_out_temp", btc_pell.getPELL_OUT_TEMP());
        params.put("pell_out_time", btc_pell.getPELL_OUT_TIME());
        params.put("pell_clean_time", btc_pell.getPELL_CLEAN_TIME());
        params.put("up_user", btc_pell.getUP_USER());

        params.put("pell_temp_1_note", btc_pell.getPELL_TEMP_1_NOTE());
        params.put("pell_temp_2_note", btc_pell.getPELL_TEMP_2_NOTE());
        params.put("pell_temp_3_note", btc_pell.getPELL_TEMP_3_NOTE());
        params.put("pell_temp_6_note", btc_pell.getPELL_TEMP_6_NOTE());
        params.put("pell_temp_head_note", btc_pell.getPELL_TEMP_HEAD_NOTE());
        params.put("pell_out_temp_note", btc_pell.getPELL_OUT_TEMP_NOTE());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertNEW_BATCH(String factory, BTC_PELL btc_pell, String ins_no, String mcs_date, String mcs_shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertNEW_BATCH(factory, btc_pell, ins_no, mcs_date, mcs_shift);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);

        params.put("new_batch_no", btc_pell.getNEW_BATCH_NO());

        params.put("btc_no", btc_pell.getBTC_NO());
        params.put("pell_gran", btc_pell.getPELL_GRAN());
        params.put("pell_temp_1", btc_pell.getPELL_TEMP_1());
        params.put("pell_temp_2", btc_pell.getPELL_TEMP_2());
        params.put("pell_temp_3", btc_pell.getPELL_TEMP_3());
        params.put("pell_temp_5", btc_pell.getPELL_TEMP_5());
        params.put("pell_mixing_time", btc_pell.getPELL_MIXING_TIME());
        params.put("pell_temp_head", btc_pell.getPELL_TEMP_HEAD());
        params.put("new_batch_no", btc_pell.getNEW_BATCH_NO());
        params.put("pell_out_temp", btc_pell.getPELL_OUT_TEMP());
        params.put("pell_out_time", btc_pell.getPELL_OUT_TIME());
        params.put("pell_clean_time", btc_pell.getPELL_CLEAN_TIME());
        params.put("up_user", btc_pell.getUP_USER());

        params.put("pell_temp_1_note", btc_pell.getPELL_TEMP_1_NOTE());
        params.put("pell_temp_2_note", btc_pell.getPELL_TEMP_2_NOTE());
        params.put("pell_temp_3_note", btc_pell.getPELL_TEMP_3_NOTE());
        params.put("pell_temp_6_note", btc_pell.getPELL_TEMP_6_NOTE());
        params.put("pell_temp_head_note", btc_pell.getPELL_TEMP_HEAD_NOTE());
        params.put("pell_out_temp_note", btc_pell.getPELL_OUT_TEMP_NOTE());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getnewbatch(String factory, String mcs_date, String mcs_shift, String mcs_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getnewbatch(factory, mcs_date, mcs_shift, mcs_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);
        params.put("mcs_no", mcs_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    //Dain begin
    public static void getFileName(String factory, String mind, String maxd, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getFileName(factory, mind, maxd);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mind", mind);
        params.put("maxd", maxd);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getListMcs(String factory, String mcs_date, String mcs_shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListMCS(factory, mcs_date, mcs_shift);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getInforMcs(String factory, String mcs_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getInforMCS(factory, mcs_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getRaw_MTR(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListRaw_MTR(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getcolor(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getcolor(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getChem(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListChem(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getPig(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListPig(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    //get groupID- qam_ins
    public static void getGroupID(String factory, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getGroupID(factory, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("shift", shift);
        params.put("date", date);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getGroupIDpdf(String factory, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getGroupIDpdf(factory, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("shift", shift);
        params.put("date", date);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getGroupID2(String factory, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getGroupID2(factory, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("shift", shift);
        params.put("date", date);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getGroupIDstep3(String factory, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getGroupIDstep3(factory, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("shift", shift);
        params.put("date", date);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getListBTC(String factory, String ins_no, String mcs_date, String mcs_shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListBTC(factory, ins_no, mcs_date, mcs_shift);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);
//        params.put("mcs_line", mcs_line);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void checkBatch_no(String factory, String ins_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkBatch_no(factory, ins_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }


    public static void deleteMAC(String factory, String ins_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.deleteMAC(factory, ins_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }


    public static void deleteBatchno(String factory, String btc_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.deleteBatchno(factory, btc_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("btc_no", btc_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getMaxBatchNo(String factory, String ins_no, String mcs_date, String mcs_shift, String mcs_line, String mcs_name, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getMaxBatchNo(factory, ins_no, mcs_date, mcs_shift, mcs_line, mcs_name);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);
        params.put("ins_no", ins_no);
        params.put("mcs_line", mcs_line);
        params.put("mcs_name", mcs_name);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getListMacNo(String factory, String mcs_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListMacNo(factory, mcs_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertBTC(String factory, QAM_BTC btc, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertBTC(factory, btc);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", btc.getMCS_NO());
        params.put("btc_style", btc.getBTC_STYLE());
        params.put("btc_color", btc.getBTC_COLOR());
        params.put("btc_hard", btc.getBTC_HARD());
        params.put("btc_date", btc.getBTC_DATE());
        params.put("btc_shift", btc.getBTC_SHIFT());
        params.put("btc_line", btc.getBTC_LINE());
        params.put("btc_batchno", btc.getBTC_BATCH_NO());
        params.put("up_user", btc.getUP_USER());
        params.put("ins_no", btc.getINS_NO());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getMac(String factory, String mcs_no, String shift, String date, String line, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListMac(factory, mcs_no, shift, date, line);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);
        params.put("shift", shift);
        params.put("date", date);
        params.put("line", line);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void check_can1(String factory, String mac1_id, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.check_can1(factory, mac1_id, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mac1_id", mac1_id);
        params.put("shift", shift);
        params.put("date", date);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }


    public static void check_can2(String factory, String mac2_id, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.check_can2(factory, mac2_id, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mac2_id", mac2_id);
        params.put("shift", shift);
        params.put("date", date);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void check_cantay(String factory, String hand_id, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.check_cantay(factory, hand_id, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("hand_id", hand_id);
        params.put("shift", shift);
        params.put("date", date);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void checkMCS_Excel(String factory, String date1, String shift, String mcs, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkMCS_Excel(factory, date1, shift, mcs);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("date1", date1);
//        params.put("date2", date2);
        params.put("shift", shift);
        params.put("mcs", mcs);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void checkINS(String factory, QAM_MAC mac, String mcs_no, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkINS(factory, mac, mcs_no, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);
        params.put("shift", shift);
        params.put("date", date);
        params.put("mac_mtr1", mac.getMAC_MTR1());
        params.put("mac_mtr2", mac.getMAC_MTR2());
        params.put("mac_mtr3", mac.getMAC_MTR3());
        params.put("mac_mtr4", mac.getMAC_MTR4());
        params.put("mac_mtr5", mac.getMAC_MTR5());
        params.put("mac_mtr6", mac.getMAC_MTR6());
        params.put("mac_mtr7", mac.getMAC_MTR7());
        params.put("mac_mtr8", mac.getMAC_MTR8());
        params.put("mac_mtr9", mac.getMAC_MTR9());
        params.put("mac_mtr10", mac.getMAC_MTR10());
        params.put("mac_chem1", mac.getMAC_CHEM1());
        params.put("mac_chem2", mac.getMAC_CHEM2());
        params.put("mac_chem3", mac.getMAC_CHEM3());
        params.put("mac_chem4", mac.getMAC_CHEM4());
        params.put("mac_chem5", mac.getMAC_CHEM5());
        params.put("mac_chem6", mac.getMAC_CHEM6());
        params.put("mac_chem7", mac.getMAC_CHEM7());
        params.put("mac_chem8", mac.getMAC_CHEM8());
        params.put("mac_chem9", mac.getMAC_CHEM9());
        params.put("mac_chem10", mac.getMAC_CHEM10());
        params.put("mac_chem11", mac.getMAC_CHEM11());
        params.put("mac_chem12", mac.getMAC_CHEM12());
        params.put("mac_chem13", mac.getMAC_CHEM13());
        params.put("mac_chem14", mac.getMAC_CHEM14());
        params.put("mac_chem15", mac.getMAC_CHEM15());
        params.put("mac_chem16", mac.getMAC_CHEM16());
        params.put("mac_chem17", mac.getMAC_CHEM17());
        params.put("mac_chem18", mac.getMAC_CHEM18());
        params.put("mac_chem19", mac.getMAC_CHEM19());
        params.put("mac_chem20", mac.getMAC_CHEM20());
        params.put("mac_pig1", mac.getMAC_PIG1());
        params.put("mac_pig2", mac.getMAC_PIG2());
        params.put("mac_pig3", mac.getMAC_PIG3());
        params.put("mac_pig4", mac.getMAC_PIG4());
        params.put("mac_pig5", mac.getMAC_PIG5());
        params.put("mac_pig6", mac.getMAC_PIG6());
        params.put("mac_pig7", mac.getMAC_PIG7());
        params.put("mac_pig8", mac.getMAC_PIG8());
        params.put("mac_pig9", mac.getMAC_PIG9());
        params.put("mac_pig10", mac.getMAC_PIG10());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertMAC(String factory, QAM_MAC mac, String mcs_no, String shift, String date, String line, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertMAC(factory, mac, mcs_no, shift, date, line);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);
        params.put("shift", shift);
        params.put("date", date);
        params.put("line", line);
        params.put("mac_mtr1", mac.getMAC_MTR1());
        params.put("mac_mtr2", mac.getMAC_MTR2());
        params.put("mac_mtr3", mac.getMAC_MTR3());
        params.put("mac_mtr4", mac.getMAC_MTR4());
        params.put("mac_mtr5", mac.getMAC_MTR5());
        params.put("mac_mtr6", mac.getMAC_MTR6());
        params.put("mac_mtr7", mac.getMAC_MTR7());
        params.put("mac_mtr8", mac.getMAC_MTR8());
        params.put("mac_mtr9", mac.getMAC_MTR9());
        params.put("mac_mtr10", mac.getMAC_MTR10());

        params.put("mac_mtr11", mac.getMAC_MTR11());
        params.put("mac_mtr12", mac.getMAC_MTR12());
        params.put("mac_mtr13", mac.getMAC_MTR13());
        params.put("mac_mtr14", mac.getMAC_MTR14());
        params.put("mac_mtr15", mac.getMAC_MTR15());
        params.put("mac_mtr16", mac.getMAC_MTR16());
        params.put("mac_mtr17", mac.getMAC_MTR17());
        params.put("mac_mtr18", mac.getMAC_MTR18());
        params.put("mac_mtr19", mac.getMAC_MTR19());
        params.put("mac_mtr20", mac.getMAC_MTR20());
        params.put("mac_mtr21", mac.getMAC_MTR21());
        params.put("mac_mtr22", mac.getMAC_MTR22());

        params.put("mac_chem1", mac.getMAC_CHEM1());
        params.put("mac_chem2", mac.getMAC_CHEM2());
        params.put("mac_chem3", mac.getMAC_CHEM3());
        params.put("mac_chem4", mac.getMAC_CHEM4());
        params.put("mac_chem5", mac.getMAC_CHEM5());
        params.put("mac_chem6", mac.getMAC_CHEM6());
        params.put("mac_chem7", mac.getMAC_CHEM7());
        params.put("mac_chem8", mac.getMAC_CHEM8());
        params.put("mac_chem9", mac.getMAC_CHEM9());
        params.put("mac_chem10", mac.getMAC_CHEM10());
        params.put("mac_chem11", mac.getMAC_CHEM11());
        params.put("mac_chem12", mac.getMAC_CHEM12());
        params.put("mac_chem13", mac.getMAC_CHEM13());
        params.put("mac_chem14", mac.getMAC_CHEM14());
        params.put("mac_chem15", mac.getMAC_CHEM15());
        params.put("mac_chem16", mac.getMAC_CHEM16());
        params.put("mac_chem17", mac.getMAC_CHEM17());
        params.put("mac_chem18", mac.getMAC_CHEM18());
        params.put("mac_chem19", mac.getMAC_CHEM19());
        params.put("mac_chem20", mac.getMAC_CHEM20());
        params.put("mac_pig1", mac.getMAC_PIG1());
        params.put("mac_pig2", mac.getMAC_PIG2());
        params.put("mac_pig3", mac.getMAC_PIG3());
        params.put("mac_pig4", mac.getMAC_PIG4());
        params.put("mac_pig5", mac.getMAC_PIG5());
        params.put("mac_pig6", mac.getMAC_PIG6());
        params.put("mac_pig7", mac.getMAC_PIG7());
        params.put("mac_pig8", mac.getMAC_PIG8());
        params.put("mac_pig9", mac.getMAC_PIG9());
        params.put("mac_pig10", mac.getMAC_PIG10());

        params.put("up_user", mac.getUP_USER());
        params.put("key", mac.getKEY());

        params.put("smac_mtr1", mac.getSMAC_MTR1());
        params.put("smac_mtr2", mac.getSMAC_MTR2());
        params.put("smac_mtr3", mac.getSMAC_MTR3());
        params.put("smac_mtr4", mac.getSMAC_MTR4());
        params.put("smac_mtr5", mac.getSMAC_MTR5());
        params.put("smac_mtr6", mac.getSMAC_MTR6());
        params.put("smac_mtr7", mac.getSMAC_MTR7());
        params.put("smac_mtr8", mac.getSMAC_MTR8());
        params.put("smac_mtr9", mac.getSMAC_MTR9());
        params.put("smac_mtr10", mac.getSMAC_MTR10());

        params.put("smac_chem1", mac.getSMAC_CHEM1());
        params.put("smac_chem2", mac.getSMAC_CHEM2());
        params.put("smac_chem3", mac.getSMAC_CHEM3());
        params.put("smac_chem4", mac.getSMAC_CHEM4());
        params.put("smac_chem5", mac.getSMAC_CHEM5());
        params.put("smac_chem6", mac.getSMAC_CHEM6());
        params.put("smac_chem7", mac.getSMAC_CHEM7());
        params.put("smac_chem8", mac.getSMAC_CHEM8());
        params.put("smac_chem9", mac.getSMAC_CHEM9());
        params.put("smac_chem10", mac.getSMAC_CHEM10());
        params.put("smac_chem11", mac.getSMAC_CHEM11());
        params.put("smac_chem12", mac.getSMAC_CHEM12());
        params.put("smac_chem13", mac.getSMAC_CHEM13());
        params.put("smac_chem14", mac.getSMAC_CHEM14());
        params.put("smac_chem15", mac.getSMAC_CHEM15());
        params.put("smac_chem16", mac.getSMAC_CHEM16());
        params.put("smac_chem17", mac.getSMAC_CHEM17());
        params.put("smac_chem18", mac.getSMAC_CHEM18());
        params.put("smac_chem19", mac.getSMAC_CHEM19());
        params.put("smac_chem20", mac.getSMAC_CHEM20());
        params.put("smac_pig1", mac.getSMAC_PIG1());
        params.put("smac_pig2", mac.getSMAC_PIG2());
        params.put("smac_pig3", mac.getSMAC_PIG3());
        params.put("smac_pig4", mac.getSMAC_PIG4());
        params.put("smac_pig5", mac.getSMAC_PIG5());
        params.put("smac_pig6", mac.getSMAC_PIG6());
        params.put("smac_pig7", mac.getSMAC_PIG7());
        params.put("smac_pig8", mac.getSMAC_PIG8());
        params.put("smac_pig9", mac.getSMAC_PIG9());
        params.put("smac_pig10", mac.getSMAC_PIG10());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertMAC2(String factory, QAM_MAC mac, String mcs_no, String shift, String date, String line, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertMAC2(factory, mac, mcs_no, shift, date, line);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);
        params.put("shift", shift);
        params.put("date", date);
        params.put("line", line);
        params.put("mac_mtr1", mac.getMAC_MTR1());
        params.put("mac_mtr2", mac.getMAC_MTR2());
        params.put("mac_mtr3", mac.getMAC_MTR3());
        params.put("mac_mtr4", mac.getMAC_MTR4());
        params.put("mac_mtr5", mac.getMAC_MTR5());
        params.put("mac_mtr6", mac.getMAC_MTR6());
        params.put("mac_mtr7", mac.getMAC_MTR7());
        params.put("mac_mtr8", mac.getMAC_MTR8());
        params.put("mac_mtr9", mac.getMAC_MTR9());
        params.put("mac_mtr10", mac.getMAC_MTR10());

        params.put("mac_mtr11", mac.getMAC_MTR11());
        params.put("mac_mtr12", mac.getMAC_MTR12());
        params.put("mac_mtr13", mac.getMAC_MTR13());
        params.put("mac_mtr14", mac.getMAC_MTR14());
        params.put("mac_mtr15", mac.getMAC_MTR15());
        params.put("mac_mtr16", mac.getMAC_MTR16());
        params.put("mac_mtr17", mac.getMAC_MTR17());
        params.put("mac_mtr18", mac.getMAC_MTR18());
        params.put("mac_mtr19", mac.getMAC_MTR19());
        params.put("mac_mtr20", mac.getMAC_MTR20());
        params.put("mac_mtr21", mac.getMAC_MTR21());
        params.put("mac_mtr22", mac.getMAC_MTR22());

        params.put("mac_chem1", mac.getMAC_CHEM1());
        params.put("mac_chem2", mac.getMAC_CHEM2());
        params.put("mac_chem3", mac.getMAC_CHEM3());
        params.put("mac_chem4", mac.getMAC_CHEM4());
        params.put("mac_chem5", mac.getMAC_CHEM5());
        params.put("mac_chem6", mac.getMAC_CHEM6());
        params.put("mac_chem7", mac.getMAC_CHEM7());
        params.put("mac_chem8", mac.getMAC_CHEM8());
        params.put("mac_chem9", mac.getMAC_CHEM9());
        params.put("mac_chem10", mac.getMAC_CHEM10());
        params.put("mac_chem11", mac.getMAC_CHEM11());
        params.put("mac_chem12", mac.getMAC_CHEM12());
        params.put("mac_chem13", mac.getMAC_CHEM13());
        params.put("mac_chem14", mac.getMAC_CHEM14());
        params.put("mac_chem15", mac.getMAC_CHEM15());
        params.put("mac_chem16", mac.getMAC_CHEM16());
        params.put("mac_chem17", mac.getMAC_CHEM17());
        params.put("mac_chem18", mac.getMAC_CHEM18());
        params.put("mac_chem19", mac.getMAC_CHEM19());
        params.put("mac_chem20", mac.getMAC_CHEM20());
        params.put("mac_pig1", mac.getMAC_PIG1());
        params.put("mac_pig2", mac.getMAC_PIG2());
        params.put("mac_pig3", mac.getMAC_PIG3());
        params.put("mac_pig4", mac.getMAC_PIG4());
        params.put("mac_pig5", mac.getMAC_PIG5());
        params.put("mac_pig6", mac.getMAC_PIG6());
        params.put("mac_pig7", mac.getMAC_PIG7());
        params.put("mac_pig8", mac.getMAC_PIG8());
        params.put("mac_pig9", mac.getMAC_PIG9());
        params.put("mac_pig10", mac.getMAC_PIG10());

        params.put("up_user", mac.getUP_USER());
        params.put("key", mac.getKEY());

        params.put("smac_mtr1", mac.getSMAC_MTR1());
        params.put("smac_mtr2", mac.getSMAC_MTR2());
        params.put("smac_mtr3", mac.getSMAC_MTR3());
        params.put("smac_mtr4", mac.getSMAC_MTR4());
        params.put("smac_mtr5", mac.getSMAC_MTR5());
        params.put("smac_mtr6", mac.getSMAC_MTR6());
        params.put("smac_mtr7", mac.getSMAC_MTR7());
        params.put("smac_mtr8", mac.getSMAC_MTR8());
        params.put("smac_mtr9", mac.getSMAC_MTR9());
        params.put("smac_mtr10", mac.getSMAC_MTR10());

        params.put("smac_chem1", mac.getSMAC_CHEM1());
        params.put("smac_chem2", mac.getSMAC_CHEM2());
        params.put("smac_chem3", mac.getSMAC_CHEM3());
        params.put("smac_chem4", mac.getSMAC_CHEM4());
        params.put("smac_chem5", mac.getSMAC_CHEM5());
        params.put("smac_chem6", mac.getSMAC_CHEM6());
        params.put("smac_chem7", mac.getSMAC_CHEM7());
        params.put("smac_chem8", mac.getSMAC_CHEM8());
        params.put("smac_chem9", mac.getSMAC_CHEM9());
        params.put("smac_chem10", mac.getSMAC_CHEM10());
        params.put("smac_chem11", mac.getSMAC_CHEM11());
        params.put("smac_chem12", mac.getSMAC_CHEM12());
        params.put("smac_chem13", mac.getSMAC_CHEM13());
        params.put("smac_chem14", mac.getSMAC_CHEM14());
        params.put("smac_chem15", mac.getSMAC_CHEM15());
        params.put("smac_chem16", mac.getSMAC_CHEM16());
        params.put("smac_chem17", mac.getSMAC_CHEM17());
        params.put("smac_chem18", mac.getSMAC_CHEM18());
        params.put("smac_chem19", mac.getSMAC_CHEM19());
        params.put("smac_chem20", mac.getSMAC_CHEM20());
        params.put("smac_pig1", mac.getSMAC_PIG1());
        params.put("smac_pig2", mac.getSMAC_PIG2());
        params.put("smac_pig3", mac.getSMAC_PIG3());
        params.put("smac_pig4", mac.getSMAC_PIG4());
        params.put("smac_pig5", mac.getSMAC_PIG5());
        params.put("smac_pig6", mac.getSMAC_PIG6());
        params.put("smac_pig7", mac.getSMAC_PIG7());
        params.put("smac_pig8", mac.getSMAC_PIG8());
        params.put("smac_pig9", mac.getSMAC_PIG9());
        params.put("smac_pig10", mac.getSMAC_PIG10());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertMAC_EXCEL(String factory, QAM_MAC mac, String mcs, String shift, String date, String line, String style, String color, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertMAC_EXCEL(factory, mac, mcs, shift, date, line, style);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs", mcs);
        params.put("shift", shift);
        params.put("date", date);
        params.put("line", line);
        params.put("style", style);
        params.put("color", color);

        params.put("mac_mtr1", mac.getMAC_MTR1());
        params.put("mac_mtr2", mac.getMAC_MTR2());
        params.put("mac_mtr3", mac.getMAC_MTR3());
        params.put("mac_mtr4", mac.getMAC_MTR4());
        params.put("mac_mtr5", mac.getMAC_MTR5());
        params.put("mac_mtr6", mac.getMAC_MTR6());
        params.put("mac_mtr7", mac.getMAC_MTR7());
        params.put("mac_mtr8", mac.getMAC_MTR8());
        params.put("mac_mtr9", mac.getMAC_MTR9());
        params.put("mac_mtr10", mac.getMAC_MTR10());
        params.put("mac_chem1", mac.getMAC_CHEM1());
        params.put("mac_chem2", mac.getMAC_CHEM2());
        params.put("mac_chem3", mac.getMAC_CHEM3());
        params.put("mac_chem4", mac.getMAC_CHEM4());
        params.put("mac_chem5", mac.getMAC_CHEM5());
        params.put("mac_chem6", mac.getMAC_CHEM6());
        params.put("mac_chem7", mac.getMAC_CHEM7());
        params.put("mac_chem8", mac.getMAC_CHEM8());
        params.put("mac_chem9", mac.getMAC_CHEM9());
        params.put("mac_chem10", mac.getMAC_CHEM10());
        params.put("mac_chem11", mac.getMAC_CHEM11());
        params.put("mac_chem12", mac.getMAC_CHEM12());
        params.put("mac_chem13", mac.getMAC_CHEM13());
        params.put("mac_chem14", mac.getMAC_CHEM14());
        params.put("mac_chem15", mac.getMAC_CHEM15());
        params.put("mac_chem16", mac.getMAC_CHEM16());
        params.put("mac_chem17", mac.getMAC_CHEM17());
        params.put("mac_chem18", mac.getMAC_CHEM18());
        params.put("mac_chem19", mac.getMAC_CHEM19());
        params.put("mac_chem20", mac.getMAC_CHEM20());
        params.put("mac_pig1", mac.getMAC_PIG1());
        params.put("mac_pig2", mac.getMAC_PIG2());
        params.put("mac_pig3", mac.getMAC_PIG3());
        params.put("mac_pig4", mac.getMAC_PIG4());
        params.put("mac_pig5", mac.getMAC_PIG5());
        params.put("mac_pig6", mac.getMAC_PIG6());
        params.put("mac_pig7", mac.getMAC_PIG7());
        params.put("mac_pig8", mac.getMAC_PIG8());
        params.put("mac_pig9", mac.getMAC_PIG9());
        params.put("mac_pig10", mac.getMAC_PIG10());
        params.put("up_user", mac.getUP_USER());
        params.put("key", mac.getKEY());

        params.put("smac_mtr1", mac.getSMAC_MTR1());
        params.put("smac_mtr2", mac.getSMAC_MTR2());
        params.put("smac_mtr3", mac.getSMAC_MTR3());
        params.put("smac_mtr4", mac.getSMAC_MTR4());
        params.put("smac_mtr5", mac.getSMAC_MTR5());
        params.put("smac_mtr6", mac.getSMAC_MTR6());
        params.put("smac_mtr7", mac.getSMAC_MTR7());
        params.put("smac_mtr8", mac.getSMAC_MTR8());
        params.put("smac_mtr9", mac.getSMAC_MTR9());
        params.put("smac_mtr10", mac.getSMAC_MTR10());
        params.put("smac_chem1", mac.getSMAC_CHEM1());
        params.put("smac_chem2", mac.getSMAC_CHEM2());
        params.put("smac_chem3", mac.getSMAC_CHEM3());
        params.put("smac_chem4", mac.getSMAC_CHEM4());
        params.put("smac_chem5", mac.getSMAC_CHEM5());
        params.put("smac_chem6", mac.getSMAC_CHEM6());
        params.put("smac_chem7", mac.getSMAC_CHEM7());
        params.put("smac_chem8", mac.getSMAC_CHEM8());
        params.put("smac_chem9", mac.getSMAC_CHEM9());
        params.put("smac_chem10", mac.getSMAC_CHEM10());
        params.put("smac_chem11", mac.getSMAC_CHEM11());
        params.put("smac_chem12", mac.getSMAC_CHEM12());
        params.put("smac_chem13", mac.getSMAC_CHEM13());
        params.put("smac_chem14", mac.getSMAC_CHEM14());
        params.put("smac_chem15", mac.getSMAC_CHEM15());
        params.put("smac_chem16", mac.getSMAC_CHEM16());
        params.put("smac_chem17", mac.getSMAC_CHEM17());
        params.put("smac_chem18", mac.getSMAC_CHEM18());
        params.put("smac_chem19", mac.getSMAC_CHEM19());
        params.put("smac_chem20", mac.getSMAC_CHEM20());
        params.put("smac_pig1", mac.getSMAC_PIG1());
        params.put("smac_pig2", mac.getSMAC_PIG2());
        params.put("smac_pig3", mac.getSMAC_PIG3());
        params.put("smac_pig4", mac.getSMAC_PIG4());
        params.put("smac_pig5", mac.getSMAC_PIG5());
        params.put("smac_pig6", mac.getSMAC_PIG6());
        params.put("smac_pig7", mac.getSMAC_PIG7());
        params.put("smac_pig8", mac.getSMAC_PIG8());
        params.put("smac_pig9", mac.getSMAC_PIG9());
        params.put("smac_pig10", mac.getSMAC_PIG10());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getListAMT_FVI2(String factory, String ins_no, String mcs_date, String mcs_shift, String mcs_line, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListAMT_FVI2(factory, ins_no, mcs_date, mcs_shift, mcs_line);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);
        params.put("mcs_line", mcs_line);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getAMT_FVI(String factory, String ins_no, String mcs_date, String mcs_shift, String mcs_line, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListAMT_FVI(factory, ins_no, mcs_date, mcs_shift, mcs_line);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);
        params.put("mcs_date", mcs_date);
        params.put("mcs_shift", mcs_shift);
        params.put("mcs_line", mcs_line);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void updateAMT(String factory, QAM_AMT amt, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.updateAMT(factory, amt);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("amt_no", amt.getAMT_NO());
        params.put("ins_no", amt.getINS_NO());

        params.put("chk_mtr1", amt.getCHK_MTR1());
        params.put("chk_mtr2", amt.getCHK_MTR2());
        params.put("chk_mtr3", amt.getCHK_MTR3());
        params.put("chk_mtr4", amt.getCHK_MTR4());
        params.put("chk_mtr5", amt.getCHK_MTR5());
        params.put("chk_mtr6", amt.getCHK_MTR6());
        params.put("chk_mtr7", amt.getCHK_MTR7());
        params.put("chk_mtr8", amt.getCHK_MTR8());
        params.put("chk_mtr9", amt.getCHK_MTR9());
        params.put("chk_mtr10", amt.getCHK_MTR10());
        params.put("chk_chem1", amt.getCHK_CHEM1());
        params.put("chk_chem2", amt.getCHK_CHEM2());
        params.put("chk_chem3", amt.getCHK_CHEM3());
        params.put("chk_chem4", amt.getCHK_CHEM4());
        params.put("chk_chem5", amt.getCHK_CHEM5());
        params.put("chk_chem6", amt.getCHK_CHEM6());
        params.put("chk_chem7", amt.getCHK_CHEM7());
        params.put("chk_chem8", amt.getCHK_CHEM8());
        params.put("chk_chem9", amt.getCHK_CHEM9());
        params.put("chk_chem10", amt.getCHK_CHEM10());
        params.put("chk_chem11", amt.getCHK_CHEM11());
        params.put("chk_chem12", amt.getCHK_CHEM12());
        params.put("chk_chem13", amt.getCHK_CHEM13());
        params.put("chk_chem14", amt.getCHK_CHEM14());
        params.put("chk_chem15", amt.getCHK_CHEM15());
        params.put("chk_chem16", amt.getCHK_CHEM16());
        params.put("chk_chem17", amt.getCHK_CHEM17());
        params.put("chk_chem18", amt.getCHK_CHEM18());
        params.put("chk_chem19", amt.getCHK_CHEM19());
        params.put("chk_chem20", amt.getCHK_CHEM20());
        params.put("chk_pig1", amt.getCHK_PIG1());
        params.put("chk_pig2", amt.getCHK_PIG2());
        params.put("chk_pig3", amt.getCHK_PIG3());
        params.put("chk_pig4", amt.getCHK_PIG4());
        params.put("chk_pig5", amt.getCHK_PIG5());
        params.put("chk_pig6", amt.getCHK_PIG6());
        params.put("chk_pig7", amt.getCHK_PIG7());
        params.put("chk_pig8", amt.getCHK_PIG8());
        params.put("chk_pig9", amt.getCHK_PIG9());
        params.put("chk_pig10", amt.getCHK_PIG10());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

//    public static void getQAM_NOTE(String factory, String position, String sub_no, String btc_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
//        String url = ApiUrl.getQAM_NOTE(factory, position, sub_no, btc_no);
//        JSONObject params = new JSONObject();
//        params.put("factory", factory);
//        params.put("position", position);
//        params.put("sub_no", sub_no);
//        params.put("btc_no", btc_no);
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
//        timeoutSetup(jsonObjReq);
//        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
//    }

    public static void updateNOTE(String factory, String position, String key, String value, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.updateNOTE(factory, position, key, value);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("position", position);
        params.put("key", key);
        params.put("value", value);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void updatenote_temp(String factory, String position, String key, String value, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.updatenote_temp(factory, position, key, value);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("position", position);
        params.put("key", key);
        params.put("value", value);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getMac3(String factory, String ins_no, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListMac3(factory, ins_no, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);
        params.put("shift", shift);
        params.put("date", date);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertMAC3(String factory, QAM_MAC mac, String ins_no, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertMAC3(factory, mac, ins_no, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);
        params.put("shift", shift);
        params.put("date", date);
        params.put("mac_mtr1", mac.getMAC_MTR1());
        params.put("mac_mtr2", mac.getMAC_MTR2());
        params.put("mac_mtr3", mac.getMAC_MTR3());
        params.put("mac_mtr4", mac.getMAC_MTR4());
        params.put("mac_mtr5", mac.getMAC_MTR5());
        params.put("mac_mtr6", mac.getMAC_MTR6());
        params.put("mac_mtr7", mac.getMAC_MTR7());
        params.put("mac_mtr8", mac.getMAC_MTR8());
        params.put("mac_mtr9", mac.getMAC_MTR9());
        params.put("mac_mtr10", mac.getMAC_MTR10());
        params.put("mac_chem1", mac.getMAC_CHEM1());
        params.put("mac_chem2", mac.getMAC_CHEM2());
        params.put("mac_chem3", mac.getMAC_CHEM3());
        params.put("mac_chem4", mac.getMAC_CHEM4());
        params.put("mac_chem5", mac.getMAC_CHEM5());
        params.put("mac_chem6", mac.getMAC_CHEM6());
        params.put("mac_chem7", mac.getMAC_CHEM7());
        params.put("mac_chem8", mac.getMAC_CHEM8());
        params.put("mac_chem9", mac.getMAC_CHEM9());
        params.put("mac_chem10", mac.getMAC_CHEM10());
        params.put("mac_chem11", mac.getMAC_CHEM11());
        params.put("mac_chem12", mac.getMAC_CHEM12());
        params.put("mac_chem13", mac.getMAC_CHEM13());
        params.put("mac_chem14", mac.getMAC_CHEM14());
        params.put("mac_chem15", mac.getMAC_CHEM15());
        params.put("mac_chem16", mac.getMAC_CHEM16());
        params.put("mac_chem17", mac.getMAC_CHEM17());
        params.put("mac_chem18", mac.getMAC_CHEM18());
        params.put("mac_chem19", mac.getMAC_CHEM19());
        params.put("mac_chem20", mac.getMAC_CHEM20());
        params.put("mac_pig1", mac.getMAC_PIG1());
        params.put("mac_pig2", mac.getMAC_PIG2());
        params.put("mac_pig3", mac.getMAC_PIG3());
        params.put("mac_pig4", mac.getMAC_PIG4());
        params.put("mac_pig5", mac.getMAC_PIG5());
        params.put("mac_pig6", mac.getMAC_PIG6());
        params.put("mac_pig7", mac.getMAC_PIG7());
        params.put("mac_pig8", mac.getMAC_PIG8());
        params.put("mac_pig9", mac.getMAC_PIG9());
        params.put("mac_pig10", mac.getMAC_PIG10());
        params.put("up_user", mac.getUP_USER());

        params.put("smac_mtr1", mac.getSMAC_MTR1());
        params.put("smac_mtr2", mac.getSMAC_MTR2());
        params.put("smac_mtr3", mac.getSMAC_MTR3());
        params.put("smac_mtr4", mac.getSMAC_MTR4());
        params.put("smac_mtr5", mac.getSMAC_MTR5());
        params.put("smac_mtr6", mac.getSMAC_MTR6());
        params.put("smac_mtr7", mac.getSMAC_MTR7());
        params.put("smac_mtr8", mac.getSMAC_MTR8());
        params.put("smac_mtr9", mac.getSMAC_MTR9());
        params.put("smac_mtr10", mac.getSMAC_MTR10());
        params.put("smac_chem1", mac.getSMAC_CHEM1());
        params.put("smac_chem2", mac.getSMAC_CHEM2());
        params.put("smac_chem3", mac.getSMAC_CHEM3());
        params.put("smac_chem4", mac.getSMAC_CHEM4());
        params.put("smac_chem5", mac.getSMAC_CHEM5());
        params.put("smac_chem6", mac.getSMAC_CHEM6());
        params.put("smac_chem7", mac.getSMAC_CHEM7());
        params.put("smac_chem8", mac.getSMAC_CHEM8());
        params.put("smac_chem9", mac.getSMAC_CHEM9());
        params.put("smac_chem10", mac.getSMAC_CHEM10());
        params.put("smac_chem11", mac.getSMAC_CHEM11());
        params.put("smac_chem12", mac.getSMAC_CHEM12());
        params.put("smac_chem13", mac.getSMAC_CHEM13());
        params.put("smac_chem14", mac.getSMAC_CHEM14());
        params.put("smac_chem15", mac.getSMAC_CHEM15());
        params.put("smac_chem16", mac.getSMAC_CHEM16());
        params.put("smac_chem17", mac.getSMAC_CHEM17());
        params.put("smac_chem18", mac.getSMAC_CHEM18());
        params.put("smac_chem19", mac.getSMAC_CHEM19());
        params.put("smac_chem20", mac.getSMAC_CHEM20());
        params.put("smac_pig1", mac.getSMAC_PIG1());
        params.put("smac_pig2", mac.getSMAC_PIG2());
        params.put("smac_pig3", mac.getSMAC_PIG3());
        params.put("smac_pig4", mac.getSMAC_PIG4());
        params.put("smac_pig5", mac.getSMAC_PIG5());
        params.put("smac_pig6", mac.getSMAC_PIG6());
        params.put("smac_pig7", mac.getSMAC_PIG7());
        params.put("smac_pig8", mac.getSMAC_PIG8());
        params.put("smac_pig9", mac.getSMAC_PIG9());
        params.put("smac_pig10", mac.getSMAC_PIG10());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }
    //end cmnl

    //get list batch no - new batch no
    public static void getData(String factory, String ins_no, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
//         ApiUrl.testPOSTRequest(factory)：先從ApiUrl裡面取得URL
        String url = ApiUrl.getData(factory, ins_no, shift, date);
        // 參數用JSON傳遞
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);
        params.put("shift", shift);
        params.put("date", date);
        // 這邊使用Request.Method.POST
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);

        // 設定Time Out，當超過時間server還沒回應，則會觸發errorListener
        timeoutSetup(jsonObjReq);

        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }


    public static void getbtcno(String factory, String mcs, String style, String color, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
//         ApiUrl.testPOSTRequest(factory)：先從ApiUrl裡面取得URL
        String url = ApiUrl.getbtcno(factory, mcs, style, color, shift, date);
        // 參數用JSON傳遞
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs", mcs);
        params.put("style", style);
        params.put("color", color);
        params.put("shift", shift);
        params.put("date", date);


        // 這邊使用Request.Method.POST
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);

        // 設定Time Out，當超過時間server還沒回應，則會觸發errorListener
        timeoutSetup(jsonObjReq);

        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getbct1(String factory, String mcs, String style, String color, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
//         ApiUrl.testPOSTRequest(factory)：先從ApiUrl裡面取得URL
        String url = ApiUrl.getbtc1(factory, mcs, style, color, shift, date);

        // 參數用JSON傳遞
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs", mcs);
        params.put("style", style);
        params.put("color", color);
        params.put("shift", shift);
        params.put("date", date);
        // 這邊使用Request.Method.POST
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);

        // 設定Time Out，當超過時間server還沒回應，則會觸發errorListener
        timeoutSetup(jsonObjReq);

        AppController.getInstance().addToRequestQueue(jsonObjReq, url);

    }

    public static void getMcs(String factory, String date, String shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
//         ApiUrl.testPOSTRequest(factory)：先從ApiUrl裡面取得URL
        String url = ApiUrl.getMcs(factory, date, shift);

        // 參數用JSON傳遞
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("date", date);
        params.put("shift", shift);
        // 這邊使用Request.Method.POST
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);

        // 設定Time Out，當超過時間server還沒回應，則會觸發errorListener
        timeoutSetup(jsonObjReq);

        AppController.getInstance().addToRequestQueue(jsonObjReq, url);

    }

    public static void getNewBatchNo(String factory, String date, String shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getNewBatchNo(factory, date, shift);
        // 參數用JSON傳遞
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("date", date);
        params.put("shift", shift);
        // 這邊使用Request.Method.POST
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);

        // 設定Time Out，當超過時間server還沒回應，則會觸發errorListener
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);

    }

    public static void getstyle(String factory, String mcs, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
//         ApiUrl.testPOSTRequest(factory)：先從ApiUrl裡面取得URL
        String url = ApiUrl.getstyle(factory, mcs, shift, date);
        // 參數用JSON傳遞
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs", mcs);
        params.put("shift", shift);
        params.put("date", date);
        // 這邊使用Request.Method.POST
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);

        // 設定Time Out，當超過時間server還沒回應，則會觸發errorListener
        timeoutSetup(jsonObjReq);

        AppController.getInstance().addToRequestQueue(jsonObjReq, url);

    }

    public static void getcolor(String factory, String mcs, String shift, String date, String style, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
//         ApiUrl.testPOSTRequest(factory)：先從ApiUrl裡面取得URL
        String url = ApiUrl.getcolor(factory, mcs, shift, date, style);

        // 參數用JSON傳遞
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs", mcs);
        params.put("shift", shift);
        params.put("date", date);
        params.put("style", style);
        // 這邊使用Request.Method.POST
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);

        // 設定Time Out，當超過時間server還沒回應，則會觸發errorListener
        timeoutSetup(jsonObjReq);

        AppController.getInstance().addToRequestQueue(jsonObjReq, url);

    }


    public static void getbct2(String factory, String mcs, String style, String color, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
//         ApiUrl.testPOSTRequest(factory)：先從ApiUrl裡面取得URL
        String url = ApiUrl.getbtc2(factory, mcs, style, color, shift, date);

        // 參數用JSON傳遞
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs", mcs);
        params.put("style", style);
        params.put("color", color);
        params.put("shift", shift);
        params.put("date", date);
        // 這邊使用Request.Method.POST
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);

        // 設定Time Out，當超過時間server還沒回應，則會觸發errorListener
        timeoutSetup(jsonObjReq);

        AppController.getInstance().addToRequestQueue(jsonObjReq, url);

    }

    //step 1
    public static void getcrmane(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getcrname(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getcrmcs(String factory, String crname, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getcrmcs(factory, crname);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("crname", crname);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertmcs(String factory, QAM_MCS mcs, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertmcs(factory, mcs);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs", mcs.getMCS());
        params.put("up_user", mcs.getUP_USER());
        params.put("mcs_shift", mcs.getMCS_SHIFT());
        params.put("mcs_date", mcs.getMCS_DATE());
        params.put("mcs_style", mcs.getMCS_STYLE());
        params.put("mcs_color", mcs.getMCS_COLOR());
        params.put("mcs_hard", mcs.getMCS_HARD());
        params.put("mcs_name", "");
        params.put("er", mcs.getER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getmcslist(String factory, String date, String shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getmcslist(factory, date, shift);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("date", date);
        params.put("shift", shift);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void updatemcsList(String factory, QAM_MCS mcs, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.updateMcsList(factory, mcs);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs.getMCS_NO());
        params.put("mcs", mcs.getMCS());
        params.put("up_user", mcs.getUP_USER());
        params.put("mcs_shift", mcs.getMCS_SHIFT());
        params.put("mcs_date", mcs.getMCS_DATE());
        params.put("mcs_style", mcs.getMCS_STYLE());
        params.put("mcs_color", mcs.getMCS_COLOR());
        params.put("mcs_hard", mcs.getMCS_HARD());
        params.put("mcs_name", "");
        params.put("er", mcs.getER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void deletemcs(String factory, String mcs_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.deletemcs(factory, mcs_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getchemical(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getchemical(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getpig(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getpig(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getmtr(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getmtr(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getmodel(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getmodel(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void setColor(String factory, String style, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.setcolor(factory, style);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("style", style);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getlistmodel(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getlistmodel(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertQAM_MTR(String factory, QAM_MTR mtr, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertQAM_MTR(factory, mtr);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mtr", mtr.getMTR());
        params.put("weight", mtr.getMTR_WEIGHT());
        params.put("up_user", mtr.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void updatemtr(String factory, QAM_MTR mtr, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.updateQAM_MTR(factory, mtr);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mtr_no", mtr.getMTR_NO());
        params.put("mtr", mtr.getMTR());
        params.put("mtr_weight", mtr.getMTR_WEIGHT());
        params.put("up_user", mtr.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertQAM_PIG(String factory, QAM_PIG pig, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertQAM_PIG(factory, pig);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("pig", pig.getPIG());
        params.put("weight", pig.getPIG_WEIGHT());
        params.put("up_user", pig.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void updateQAM_PIG(String factory, QAM_PIG pig, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.updateQAM_PIG(factory, pig);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("pig_no", pig.getPIG_NO());
        params.put("pig", pig.getPIG());
        params.put("weight", pig.getPIG_WEIGHT());
        params.put("up_user", pig.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertQAM_MODEL(String factory, QAM_MODEL model, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertQAM_MODEL(factory, model);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("style", model.getSTYLE());
        params.put("color", model.getCOLOR());
        params.put("up_user", model.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertQAM_MODEL_STYLE(String factory, QAM_MODEL model, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertQAM_MODEL_STYLE(factory, model);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("style", model.getSTYLE());
        params.put("up_user", model.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertQAM_MODEL_COLOR(String factory, QAM_MODEL model, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertQAM_MODEL_COLOR(factory, model);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("color", model.getCOLOR());
        params.put("up_user", model.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void updateQAM_MODEL(String factory, QAM_MODEL model, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.updateQAM_MODEL_STYLE(factory, model);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("style_no", model.getSTYLE_NO());
        params.put("style", model.getSTYLE());
        params.put("color", model.getCOLOR());
        params.put("up_user", model.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void updateQAM_MODEL_STYLE(String factory, QAM_MODEL model, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.updateQAM_MODEL_STYLE(factory, model);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("style_no", model.getSTYLE_NO());
        params.put("style", model.getSTYLE());
        params.put("up_user", model.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void updateQAM_MODEL_COLOR(String factory, QAM_MODEL model, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.updateQAM_MODEL_COLOR(factory, model);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("style_no", model.getSTYLE_NO());
        params.put("color", model.getCOLOR());
        params.put("up_user", model.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    //chem
    public static void insertQAM_CHEM(String factory, QAM_CHEM chem, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertQAM_CHEM(factory, chem);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("chem", chem.getCHEM());
        params.put("weight", chem.getCHEM_WEIGHT());
        params.put("up_user", chem.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void updateQAM_CHEM(String factory, QAM_CHEM chem, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.updateQAM_CHEM(factory, chem);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("chem_no", chem.getCHEM_NO());
        params.put("chem", chem.getCHEM());
        params.put("weight", chem.getCHEM_WEIGHT());
        params.put("up_user", chem.getUP_USER());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    //cr
    public static void insertQAM_CR(String factory, QAM_CR cr, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertQAM_CR(factory, cr);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("cr_mcs", cr.getCR_MCS());
        params.put("cr_name", cr.getCR_NAME());
        params.put("cr_fullname", cr.getCR_FULLNAME());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void updateQAM_CR(String factory, QAM_CR cr, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.updateQAM_CR(factory, cr);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("cr_id", cr.getCR_ID());
        params.put("cr_mcs", cr.getCR_MCS());
        params.put("cr_name", cr.getCR_NAME());
        params.put("cr_fullname", cr.getCR_FULLNAME());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getcrlist(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getcrlist(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void deleteStyle(String factory, String style_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.deleteStyle(factory, style_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("style_no", style_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void checkStyle_fvi(String factory, String style, String color, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkStyle(factory, style, color);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("style", style);
        params.put("color", color);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void checkStyle_fvi2(String factory, String style, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkStyle_fvi2(factory, style);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("style", style);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void checkColor_fvi2(String factory, String color, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkColor_fvi2(factory, color);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("color", color);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void deleteCrocs(String factory, String crocs_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.deleteCrocs(factory, crocs_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("crocs_no", crocs_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void checkCrocs(String factory, String crocs, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkCrocs(factory, crocs);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("crocs", crocs);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void deleteMCS(String factory, String mcs_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.deleteMCS(factory, mcs_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void checkMCS(String factory, String mcs_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkMCS(factory, mcs_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mcs_no", mcs_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void deleteMTR(String factory, String mtr_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.deleteMTR(factory, mtr_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mtr_no", mtr_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void checkMTR(String factory, String mtr, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkMTR(factory, mtr);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mtr", mtr);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void deleteChemical(String factory, String chem_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.deleteChemical(factory, chem_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("chem_no", chem_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void checkChemical(String factory, String chem, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkChemical(factory, chem);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("chem", chem);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void deletePigment(String factory, String pig_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.deletePigment(factory, pig_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("pig_no", pig_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void checkPigment(String factory, String pig, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkPigment(factory, pig);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("pig", pig);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    //report - QAM
    public static void getRP_MAC(String factory, String ins_no, String date, String shift, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getRP_MAC(factory, ins_no, date, shift);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);
        params.put("date", date);
        params.put("shift", shift);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getListBatch(String factory, String ins_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListBatch(factory, ins_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getListKneader(String factory, String ins_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListKneader(factory, ins_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getListOpenMill(String factory, String ins_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListOpenMill(factory, ins_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getListPell(String factory, String ins_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListPell(factory, ins_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void gettimekneader(String factory, String btc_no, String ins_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.gettimekneader(factory, btc_no, ins_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("btc_no", btc_no);
        params.put("ins_no", ins_no);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getChem_checkFVI2(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.checkThong(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }
    public static void TempModel_FVI2(String factory,String may, String ca, String ngay, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.TempModel(factory, may,ca, ngay);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("may", may);
        params.put("ca", ca);
        params.put("ngay", ngay);
//        params.put("CHECK_DATE",qtm.getCHECK_DATE());
//        params.put("NDTCT", qtm.getNDTCT());
//        params.put("TCTGT", qtm.getTCTGT());
//        params.put("NDTCP", qtm.getNDTCP());
//        params.put("TCTGP", qtm.getTCTGP());
//        params.put("HTT",qtm.getHTT());
//
//        params.put("HTP", qtm.getHTP());
//        params.put("SM1T", qtm.getSM1T());
//        params.put("SM1D", qtm.getSM1D());
//        params.put("SM2T", qtm.getSM2T());
//        params.put("SM2D",qtm.getSM2D());
//
//
//        params.put("SM3T",qtm.getSM3T());
//        params.put("SM3D", qtm.getSM3D());
//        params.put("SM4T", qtm.getSM4T());
//        params.put("SM4D", qtm.getSM4D());
//        params.put("SM5T",qtm.getSM5T());
//        params.put("SM5D", qtm.getSM5D());
//        params.put("SM6T", qtm.getSM6T());
//        params.put("SM6D", qtm.getSM6D());
//        params.put("SM7T", qtm.getSM7T());
//        params.put("SM7D",qtm.getSM7D());
//
//        params.put("SM8T",qtm.getSM8T());
//        params.put("SM8D", qtm.getSM8D());
//        params.put("NDM1TT", qtm.getNDM1TT());
//        params.put("NDM1TP", qtm.getNDM1TP());
//        params.put("NDM1DT",qtm.getNDM1DT());
//        params.put("NDM1DP", qtm.getNDM1DP());
//        params.put("NDM2TT", qtm.getNDM2TT());
//        params.put("NDM2TP", qtm.getNDM2TP());
//        params.put("NDM2DT", qtm.getNDM2DT());
//        params.put("NDM2DP",qtm.getNDM2DP());
//
//        params.put("NDM3TT", qtm.getNDM3TT());
//        params.put("NDM3TP", qtm.getNDM3TP());
//        params.put("NDM3DT", qtm.getNDM3DT());
//        params.put("NDM3DP", qtm.getNDM3DP());
//        params.put("NDM4TT",qtm.getNDM4TT());
//        params.put("NDM4TP", qtm.getNDM4TP());
//        params.put("NDM4DT", qtm.getNDM4DT());
//        params.put("NDM4DP", qtm.getNDM4DP());
//        params.put("NDM5TT", qtm.getNDM5TT());
//        params.put("NDM5TP",qtm.getNDM5TP());
//        params.put("NDM5DT", qtm.getNDM5DT());
//        params.put("NDM5DP", qtm.getNDM5DP());
//
//        params.put("NDM6TT", qtm.getNDM6TT());
//        params.put("NDM6TP", qtm.getNDM6TP());
//        params.put("NDM6DT",qtm.getNDM6DT());
//        params.put("NDM6DP", qtm.getNDM6DP());
//        params.put("NDM7TT", qtm.getNDM7TT());
//        params.put("NDM7TP", qtm.getNDM7TP());
//        params.put("NDM7DT", qtm.getNDM7DT());
//        params.put("NDM7DP",qtm.getNDM7DP());
//        params.put("NDM8TT", qtm.getNDM8TT());
//        params.put("NDM8TP", qtm.getNDM8TP());
//        params.put("NDM8DT",qtm.getNDM8DT());
//        params.put("NDM8DP", qtm.getNDM8DP());
//
//        params.put("SET_TIMEM1T", qtm.getSET_TIMEM1T());
//        params.put("SET_TIMEM1D", qtm.getSET_TIMEM1D());
//        params.put("SET_TIMEM2T", qtm.getSET_TIMEM2T());
//        params.put("SET_TIMEM2D", qtm.getSET_TIMEM2D());
//        params.put("SET_TIMEM3T",qtm.getSET_TIMEM3T());
//        params.put("SET_TIMEM3D", qtm.getSET_TIMEM3D());
//        params.put("SET_TIMEM4T", qtm.getSET_TIMEM4T());
//        params.put("SET_TIMEM4D", qtm.getSET_TIMEM4D());
//        params.put("SET_TIMEM5T", qtm.getSET_TIMEM5T());
//        params.put("SET_TIMEM5D",qtm.getSET_TIMEM5D());
//        params.put("SET_TIMEM6T", qtm.getSET_TIMEM6T());
//        params.put("SET_TIMEM6D", qtm.getSET_TIMEM6D());
//
//        params.put("SET_TIMEM7T", qtm.getSET_TIMEM7T());
//        params.put("SET_TIMEM7D", qtm.getSET_TIMEM7D());
//        params.put("SET_TIMEM8T",qtm.getSET_TIMEM8T());
//        params.put("SET_TIMEM8D", qtm.getSET_TIMEM8D());
//        params.put("CHECK_TIMEM1T", qtm.getCHECK_TIMEM1T());
//        params.put("CHECK_TIMEM1D", qtm.getCHECK_TIMEM1D());
//        params.put("CHECK_TIMEM2T", qtm.getCHECK_TIMEM2T());
//        params.put("CHECK_TIMEM2D",qtm.getCHECK_TIMEM2D());
//        params.put("CHECK_TIMEM3T", qtm.getCHECK_TIMEM3T());
//        params.put("CHECK_TIMEM3D", qtm.getCHECK_TIMEM3D());
//        params.put("CHECK_TIMEM4T",qtm.getCHECK_TIMEM4T());
//        params.put("CHECK_TIMEM4D", qtm.getCHECK_TIMEM4D());
//
//        params.put("CHECK_TIMEM5T", qtm.getCHECK_TIMEM5T());
//        params.put("CHECK_TIMEM5D", qtm.getCHECK_TIMEM5D());
//        params.put("CHECK_TIMEM6T", qtm.getCHECK_TIMEM6T());
//        params.put("CHECK_TIMEM6D",qtm.getCHECK_TIMEM6D());
//        params.put("CHECK_TIMEM7T", qtm.getCHECK_TIMEM7T());
//        params.put("CHECK_TIMEM7D",qtm.getCHECK_TIMEM7D());
//        params.put("CHECK_TIMEM8T", qtm.getCHECK_TIMEM8T());
//        params.put("CHECK_TIMEM8D", qtm.getCHECK_TIMEM8D());
//        params.put("ID",qtm.getID());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void SaveTempModel_FVI2(String factory, QAM_TEMP_MODEL qtm, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.saveTempModel(factory, qtm);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("may", qtm.getMAY());
        params.put("ca", qtm.getCA());
        params.put("check_date", qtm.getCHECK_DATE());
        params.put("check_hours",qtm.getCHECK_HOURS());
        params.put("NDTCT", qtm.getNDTCT());
        params.put("TCTGT", qtm.getTCTGT());
        params.put("NDTCP", qtm.getNDTCP());
        params.put("TCTGP", qtm.getTCTGP());
        params.put("HTT",qtm.getHTT());

//        params.put("HTP", qtm.getHTP());
        params.put("SM1T", qtm.getSM1T());
        params.put("SM1D", qtm.getSM1D());
        params.put("SM2T", qtm.getSM2T());
        params.put("SM2D",qtm.getSM2D());


        params.put("SM3T",qtm.getSM3T());
        params.put("SM3D", qtm.getSM3D());
        params.put("SM4T", qtm.getSM4T());
        params.put("SM4D", qtm.getSM4D());
        params.put("SM5T",qtm.getSM5T());
        params.put("SM5D", qtm.getSM5D());
        params.put("SM6T", qtm.getSM6T());
        params.put("SM6D", qtm.getSM6D());
        params.put("SM7T", qtm.getSM7T());
        params.put("SM7D",qtm.getSM7D());

        params.put("SM8T",qtm.getSM8T());
        params.put("SM8D", qtm.getSM8D());
        params.put("NDM1TT", qtm.getNDM1TT());
        params.put("NDM1TP", qtm.getNDM1TP());
        params.put("NDM1DT",qtm.getNDM1DT());
        params.put("NDM1DP", qtm.getNDM1DP());
        params.put("NDM2TT", qtm.getNDM2TT());
        params.put("NDM2TP", qtm.getNDM2TP());
        params.put("NDM2DT", qtm.getNDM2DT());
        params.put("NDM2DP",qtm.getNDM2DP());

        params.put("NDM3TT", qtm.getNDM3TT());
        params.put("NDM3TP", qtm.getNDM3TP());
        params.put("NDM3DT", qtm.getNDM3DT());
        params.put("NDM3DP", qtm.getNDM3DP());
        params.put("NDM4TT",qtm.getNDM4TT());
        params.put("NDM4TP", qtm.getNDM4TP());
        params.put("NDM4DT", qtm.getNDM4DT());
        params.put("NDM4DP", qtm.getNDM4DP());
        params.put("NDM5TT", qtm.getNDM5TT());
        params.put("NDM5TP",qtm.getNDM5TP());
        params.put("NDM5DT", qtm.getNDM5DT());
        params.put("NDM5DP", qtm.getNDM5DP());

        params.put("NDM6TT", qtm.getNDM6TT());
        params.put("NDM6TP", qtm.getNDM6TP());
        params.put("NDM6DT",qtm.getNDM6DT());
        params.put("NDM6DP", qtm.getNDM6DP());
        params.put("NDM7TT", qtm.getNDM7TT());
        params.put("NDM7TP", qtm.getNDM7TP());
        params.put("NDM7DT", qtm.getNDM7DT());
        params.put("NDM7DP",qtm.getNDM7DP());
        params.put("NDM8TT", qtm.getNDM8TT());
        params.put("NDM8TP", qtm.getNDM8TP());
        params.put("NDM8DT",qtm.getNDM8DT());
        params.put("NDM8DP", qtm.getNDM8DP());

        params.put("SET_TIMEM1T", qtm.getSET_TIMEM1T());
        params.put("SET_TIMEM1D", qtm.getSET_TIMEM1D());
        params.put("SET_TIMEM2T", qtm.getSET_TIMEM2T());
        params.put("SET_TIMEM2D", qtm.getSET_TIMEM2D());
        params.put("SET_TIMEM3T",qtm.getSET_TIMEM3T());
        params.put("SET_TIMEM3D", qtm.getSET_TIMEM3D());
        params.put("SET_TIMEM4T", qtm.getSET_TIMEM4T());
        params.put("SET_TIMEM4D", qtm.getSET_TIMEM4D());
        params.put("SET_TIMEM5T", qtm.getSET_TIMEM5T());
        params.put("SET_TIMEM5D",qtm.getSET_TIMEM5D());
        params.put("SET_TIMEM6T", qtm.getSET_TIMEM6T());
        params.put("SET_TIMEM6D", qtm.getSET_TIMEM6D());

        params.put("SET_TIMEM7T", qtm.getSET_TIMEM7T());
        params.put("SET_TIMEM7D", qtm.getSET_TIMEM7D());
        params.put("SET_TIMEM8T",qtm.getSET_TIMEM8T());
        params.put("SET_TIMEM8D", qtm.getSET_TIMEM8D());

        params.put("ID",qtm.getID());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }


    public static void getMCS_color(String factory,String shift,String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getMCS_color(factory,shift,date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("shift", shift);
        params.put("date", date);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }
    public static void getMCS_FVI(String factory,String shift,String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getMCS_FVI(factory,shift,date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("shift", shift);
        params.put("date", date);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }
    public static void getMaDonINS_FVI(String factory,String date,String shift,String color, String mcs, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getMaDonINS_FVI(factory,date,shift,color,mcs);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("date",date );
        params.put("shift", shift);
        params.put("color", color);
        params.put("mcs", mcs);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertQAM_TEMP_MODEL(String factory, QAM_TEMP_MODEL qtm, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertQAM_TEMP_MODEL(factory, qtm);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("id", qtm.getID());
        params.put("may", qtm.getMAY());
        params.put("ca", qtm.getCA());
        params.put("check_date", qtm.getCHECK_DATE());
        params.put("check_hours", qtm.getCHECK_HOURS());
        params.put("NDTCT", qtm.getNDTCT());
        params.put("TCTGT", qtm.getTCTGT());
        params.put("NDTCP", qtm.getNDTCP());
        params.put("TCTGP", qtm.getTCTGP());
//        params.put("HT",qtm.getHT());


        params.put("HT1", qtm.getHT1());
        params.put("HT2", qtm.getHT2());
        params.put("HT3", qtm.getHT3());
        params.put("HT4",qtm.getHT4());
        params.put("HT5", qtm.getHT5());
        params.put("HT6", qtm.getHT6());
        params.put("HT7", qtm.getHT7());
        params.put("HT8",qtm.getHT8());
        params.put("HT9", qtm.getHT9());
        params.put("HT10", qtm.getHT10());
        params.put("HT11", qtm.getHT11());
        params.put("HT12", qtm.getHT12());
        params.put("HT13",qtm.getHT13());
        params.put("HT14",qtm.getHT14());
        params.put("HT15", qtm.getHT15());
        params.put("HT16", qtm.getHT16());



        params.put("SM1T", qtm.getSM1T());
        params.put("SM1D", qtm.getSM1D());
        params.put("SM2T", qtm.getSM2T());
        params.put("SM2D",qtm.getSM2D());
        params.put("SM3T",qtm.getSM3T());
        params.put("SM3D", qtm.getSM3D());
        params.put("SM4T", qtm.getSM4T());
        params.put("SM4D", qtm.getSM4D());
        params.put("SM5T",qtm.getSM5T());
        params.put("SM5D", qtm.getSM5D());
        params.put("SM6T", qtm.getSM6T());
        params.put("SM6D", qtm.getSM6D());
        params.put("SM7T", qtm.getSM7T());
        params.put("SM7D",qtm.getSM7D());
        params.put("SM8T",qtm.getSM8T());
        params.put("SM8D", qtm.getSM8D());
        params.put("NDM1TT", qtm.getNDM1TT());
        params.put("NDM1TP", qtm.getNDM1TP());
        params.put("NDM1DT",qtm.getNDM1DT());
        params.put("NDM1DP", qtm.getNDM1DP());
        params.put("NDM2TT", qtm.getNDM2TT());
        params.put("NDM2TP", qtm.getNDM2TP());
        params.put("NDM2DT", qtm.getNDM2DT());
        params.put("NDM2DP",qtm.getNDM2DP());
        params.put("NDM3TT", qtm.getNDM3TT());
        params.put("NDM3TP", qtm.getNDM3TP());
        params.put("NDM3DT", qtm.getNDM3DT());
        params.put("NDM3DP", qtm.getNDM3DP());
        params.put("NDM4TT",qtm.getNDM4TT());
        params.put("NDM4TP", qtm.getNDM4TP());
        params.put("NDM4DT", qtm.getNDM4DT());
        params.put("NDM4DP", qtm.getNDM4DP());
        params.put("NDM5TT", qtm.getNDM5TT());
        params.put("NDM5TP",qtm.getNDM5TP());
        params.put("NDM5DT", qtm.getNDM5DT());
        params.put("NDM5DP", qtm.getNDM5DP());
        params.put("NDM6TT", qtm.getNDM6TT());
        params.put("NDM6TP", qtm.getNDM6TP());
        params.put("NDM6DT",qtm.getNDM6DT());
        params.put("NDM6DP", qtm.getNDM6DP());
        params.put("NDM7TT", qtm.getNDM7TT());
        params.put("NDM7TP", qtm.getNDM7TP());
        params.put("NDM7DT", qtm.getNDM7DT());
        params.put("NDM7DP",qtm.getNDM7DP());
        params.put("NDM8TT", qtm.getNDM8TT());
        params.put("NDM8TP", qtm.getNDM8TP());
        params.put("NDM8DT",qtm.getNDM8DT());
        params.put("NDM8DP", qtm.getNDM8DP());
        params.put("SET_TIMEM1T", qtm.getSET_TIMEM1T());
        params.put("SET_TIMEM1D", qtm.getSET_TIMEM1D());
        params.put("SET_TIMEM2T", qtm.getSET_TIMEM2T());
        params.put("SET_TIMEM2D", qtm.getSET_TIMEM2D());
        params.put("SET_TIMEM3T",qtm.getSET_TIMEM3T());
        params.put("SET_TIMEM3D", qtm.getSET_TIMEM3D());
        params.put("SET_TIMEM4T", qtm.getSET_TIMEM4T());
        params.put("SET_TIMEM4D", qtm.getSET_TIMEM4D());
        params.put("SET_TIMEM5T", qtm.getSET_TIMEM5T());
        params.put("SET_TIMEM5D",qtm.getSET_TIMEM5D());
        params.put("SET_TIMEM6T", qtm.getSET_TIMEM6T());
        params.put("SET_TIMEM6D", qtm.getSET_TIMEM6D());

        params.put("SET_TIMEM7T", qtm.getSET_TIMEM7T());
        params.put("SET_TIMEM7D", qtm.getSET_TIMEM7D());
        params.put("SET_TIMEM8T",qtm.getSET_TIMEM8T());
        params.put("SET_TIMEM8D", qtm.getSET_TIMEM8D());

        params.put("ID",qtm.getID());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }


    public static void getIdModel(String factory,String date,String ca,String may, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getIdModel(factory,date,ca,may);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("date",date );
        params.put("ca", ca);
        params.put("may", may);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getIdModel2(String factory, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getIdModel2(factory);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getListQAM_TEMP_MODEL(String factory,String ID, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListQAM_TEMP_MODEL(factory,ID);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ID",ID );
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void deleteID(String factory,String ID, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.deleteID(factory,ID);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ID",ID );
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getIdColor(String factory, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getIdColor(factory, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("shift", shift);
        params.put("date", date);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertQAM_SCALE_MAC1(String factory, QAM_SCALE_MAC1 scm1, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertQAM_SCALE_MAC1(factory, scm1);
        JSONObject params = new JSONObject();
        params.put("factory", factory);

        params.put("key", scm1.getKEY());
        params.put("SC1_SHIFT", scm1.getSC1_SHIFT());
        params.put("SC1_DATE", scm1.getSC1_DATE());
        params.put("SC1_MCS", scm1.getSC1_MCS());
        params.put("SC1_RAW1", scm1.getSC1_RAW1());
        params.put("SC1_RAW2", scm1.getSC1_RAW2());
        params.put("SC1_RAW3", scm1.getSC1_RAW3());
        params.put("SC1_RAW4", scm1.getSC1_RAW4());
        params.put("SC1_RAW5", scm1.getSC1_RAW5());
        params.put("SC1_RAW6", scm1.getSC1_RAW6());
        params.put("SC1_RAW7", scm1.getSC1_RAW7());
        params.put("SC1_RAW8", scm1.getSC1_RAW8());
        params.put("SC1_RAW9", scm1.getSC1_RAW9());
        params.put("SC1_RAW10", scm1.getSC1_RAW10());
        params.put("SC1_CHEM1", scm1.getSC1_CHEM1());
        params.put("SC1_CHEM2", scm1.getSC1_CHEM2());
        params.put("SC1_CHEM3", scm1.getSC1_CHEM3());
        params.put("SC1_CHEM4", scm1.getSC1_CHEM4());
        params.put("SC1_CHEM5", scm1.getSC1_CHEM5());
        params.put("SC1_CHEM6", scm1.getSC1_CHEM6());
        params.put("SC1_CHEM7", scm1.getSC1_CHEM7());
        params.put("SC1_CHEM8", scm1.getSC1_CHEM8());
        params.put("SC1_CHEM9", scm1.getSC1_CHEM9());
        params.put("SC1_CHEM10", scm1.getSC1_CHEM10());
        params.put("SC1_CHEM11", scm1.getSC1_CHEM11());
        params.put("SC1_CHEM12", scm1.getSC1_CHEM12());
        params.put("SC1_CHEM13", scm1.getSC1_CHEM13());
        params.put("SC1_CHEM14", scm1.getSC1_CHEM14());
        params.put("SC1_CHEM15", scm1.getSC1_CHEM15());
        params.put("SC1_CHEM16", scm1.getSC1_CHEM16());
        params.put("SC1_CHEM17", scm1.getSC1_CHEM17());
        params.put("SC1_CHEM18", scm1.getSC1_CHEM18());
        params.put("SC1_CHEM19", scm1.getSC1_CHEM19());
        params.put("SC1_CHEM20", scm1.getSC1_CHEM20());
        params.put("SC1_PIG1", scm1.getSC1_PIG1());
        params.put("SC1_PIG2", scm1.getSC1_PIG2());
        params.put("SC1_PIG3", scm1.getSC1_PIG3());
        params.put("SC1_PIG4", scm1.getSC1_PIG4());
        params.put("SC1_PIG5", scm1.getSC1_PIG5());
        params.put("SC1_PIG6", scm1.getSC1_PIG6());
        params.put("SC1_PIG7", scm1.getSC1_PIG7());
        params.put("SC1_PIG8", scm1.getSC1_PIG8());
        params.put("SC1_PIG9", scm1.getSC1_PIG9());
        params.put("SC1_PIG10", scm1.getSC1_PIG10());
        params.put("SC1_WRAW1", scm1.getSC1_WRAW1());
        params.put("SC1_WRAW2", scm1.getSC1_WRAW2());
        params.put("SC1_WRAW3", scm1.getSC1_WRAW3());
        params.put("SC1_WRAW4", scm1.getSC1_WRAW4());
        params.put("SC1_WRAW5", scm1.getSC1_WRAW5());
        params.put("SC1_WRAW6", scm1.getSC1_WRAW6());
        params.put("SC1_WRAW7", scm1.getSC1_WRAW7());
        params.put("SC1_WRAW8", scm1.getSC1_WRAW8());
        params.put("SC1_WRAW9", scm1.getSC1_WRAW9());
        params.put("SC1_WRAW10", scm1.getSC1_WRAW10());
        params.put("SC1_WCHEM1", scm1.getSC1_WCHEM1());
        params.put("SC1_WCHEM2", scm1.getSC1_WCHEM2());
        params.put("SC1_WCHEM3", scm1.getSC1_WCHEM3());
        params.put("SC1_WCHEM4", scm1.getSC1_WCHEM4());
        params.put("SC1_WCHEM5", scm1.getSC1_WCHEM5());
        params.put("SC1_WCHEM6", scm1.getSC1_WCHEM6());
        params.put("SC1_WCHEM7", scm1.getSC1_WCHEM7());
        params.put("SC1_WCHEM8", scm1.getSC1_WCHEM8());
        params.put("SC1_WCHEM9", scm1.getSC1_WCHEM9());
        params.put("SC1_WCHEM10", scm1.getSC1_WCHEM10());
        params.put("SC1_WCHEM11", scm1.getSC1_WCHEM11());
        params.put("SC1_WCHEM12", scm1.getSC1_WCHEM12());
        params.put("SC1_WCHEM13", scm1.getSC1_WCHEM13());
        params.put("SC1_WCHEM14", scm1.getSC1_WCHEM14());
        params.put("SC1_WCHEM15", scm1.getSC1_WCHEM15());
        params.put("SC1_WCHEM16", scm1.getSC1_WCHEM16());
        params.put("SC1_WCHEM17", scm1.getSC1_WCHEM17());
        params.put("SC1_WCHEM18", scm1.getSC1_WCHEM18());
        params.put("SC1_WCHEM19", scm1.getSC1_WCHEM19());
        params.put("SC1_WCHEM20", scm1.getSC1_WCHEM20());
        params.put("SC1_WPIG1", scm1.getSC1_WPIG1());
        params.put("SC1_WPIG2", scm1.getSC1_WPIG2());
        params.put("SC1_WPIG3", scm1.getSC1_WPIG3());
        params.put("SC1_WPIG4", scm1.getSC1_WPIG4());
        params.put("SC1_WPIG5", scm1.getSC1_WPIG5());
        params.put("SC1_WPIG6", scm1.getSC1_WPIG6());
        params.put("SC1_WPIG7", scm1.getSC1_WPIG7());
        params.put("SC1_WPIG8", scm1.getSC1_WPIG8());
        params.put("SC1_WPIG9", scm1.getSC1_WPIG9());
        params.put("SC1_WPIG10", scm1.getSC1_WPIG10());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }
//    public static void insertQAM_SCALE_MAC2(String factory, QAM_SCALE_MAC2 scm2, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
//        String url = ApiUrl.insertQAM_SCALE_MAC2(factory, scm2);
//        JSONObject params = new JSONObject();
//        params.put("factory", factory);
//        params.put("SC2_SHIFT", scm2.getSC2_SHIFT());
//        params.put("SC2_DATE", scm2.getSC2_DATE());
//        params.put("SC2_MCS", scm2.getSC2_MCS());
//        params.put("SC2_RAW1", scm2.getSC2_RAW1());
//        params.put("SC2_RAW2", scm2.getSC2_RAW2());
//        params.put("SC2_RAW3", scm2.getSC2_RAW3());
//        params.put("SC2_RAW4", scm2.getSC2_RAW4());
//        params.put("SC2_RAW5", scm2.getSC2_RAW5());
//        params.put("SC2_RAW6", scm2.getSC2_RAW6());
//        params.put("SC2_RAW7", scm2.getSC2_RAW7());
//        params.put("SC2_RAW8", scm2.getSC2_RAW8());
//        params.put("SC2_RAW9", scm2.getSC2_RAW9());
//        params.put("SC2_RAW10", scm2.getSC2_RAW10());
//        params.put("SC2_CHEM1", scm2.getSC2_CHEM1());
//        params.put("SC2_CHEM2", scm2.getSC2_CHEM2());
//        params.put("SC2_CHEM3", scm2.getSC2_CHEM3());
//        params.put("SC2_CHEM4", scm2.getSC2_CHEM4());
//        params.put("SC2_CHEM5", scm2.getSC2_CHEM5());
//        params.put("SC2_CHEM6", scm2.getSC2_CHEM6());
//        params.put("SC2_CHEM7", scm2.getSC2_CHEM7());
//        params.put("SC2_CHEM8", scm2.getSC2_CHEM8());
//        params.put("SC2_CHEM9", scm2.getSC2_CHEM9());
//        params.put("SC2_CHEM10", scm2.getSC2_CHEM10());
//        params.put("SC2_CHEM11", scm2.getSC2_CHEM11());
//        params.put("SC2_CHEM12", scm2.getSC2_CHEM12());
//        params.put("SC2_CHEM13", scm2.getSC2_CHEM13());
//        params.put("SC2_CHEM14", scm2.getSC2_CHEM14());
//        params.put("SC2_CHEM15", scm2.getSC2_CHEM15());
//        params.put("SC2_CHEM16", scm2.getSC2_CHEM16());
//        params.put("SC2_CHEM17", scm2.getSC2_CHEM17());
//        params.put("SC2_CHEM18", scm2.getSC2_CHEM18());
//        params.put("SC2_CHEM19", scm2.getSC2_CHEM19());
//        params.put("SC2_CHEM20", scm2.getSC2_CHEM20());
//        params.put("SC2_PIG1", scm2.getSC2_PIG1());
//        params.put("SC2_PIG2", scm2.getSC2_PIG2());
//        params.put("SC2_PIG3", scm2.getSC2_PIG3());
//        params.put("SC2_PIG4", scm2.getSC2_PIG4());
//        params.put("SC2_PIG5", scm2.getSC2_PIG5());
//        params.put("SC2_PIG6", scm2.getSC2_PIG6());
//        params.put("SC2_PIG7", scm2.getSC2_PIG7());
//        params.put("SC2_PIG8", scm2.getSC2_PIG8());
//        params.put("SC2_PIG9", scm2.getSC2_PIG9());
//        params.put("SC2_PIG10", scm2.getSC2_PIG10());
//        params.put("SC2_WRAW1", scm2.getSC2_WRAW1());
//        params.put("SC2_WRAW2", scm2.getSC2_WRAW2());
//        params.put("SC2_WRAW3", scm2.getSC2_WRAW3());
//        params.put("SC2_WRAW4", scm2.getSC2_WRAW4());
//        params.put("SC2_WRAW5", scm2.getSC2_WRAW5());
//        params.put("SC2_WRAW6", scm2.getSC2_WRAW6());
//        params.put("SC2_WRAW7", scm2.getSC2_WRAW7());
//        params.put("SC2_WRAW8", scm2.getSC2_WRAW8());
//        params.put("SC2_WRAW9", scm2.getSC2_WRAW9());
//        params.put("SC2_WRAW10", scm2.getSC2_WRAW10());
//        params.put("SC2_WCHEM1", scm2.getSC2_WCHEM1());
//        params.put("SC2_WCHEM2", scm2.getSC2_WCHEM2());
//        params.put("SC2_WCHEM3", scm2.getSC2_WCHEM3());
//        params.put("SC2_WCHEM4", scm2.getSC2_WCHEM4());
//        params.put("SC2_WCHEM5", scm2.getSC2_WCHEM5());
//        params.put("SC2_WCHEM6", scm2.getSC2_WCHEM6());
//        params.put("SC2_WCHEM7", scm2.getSC2_WCHEM7());
//        params.put("SC2_WCHEM8", scm2.getSC2_WCHEM8());
//        params.put("SC2_WCHEM9", scm2.getSC2_WCHEM9());
//        params.put("SC2_WCHEM10", scm2.getSC2_WCHEM10());
//        params.put("SC2_WCHEM11", scm2.getSC2_WCHEM11());
//        params.put("SC2_WCHEM12", scm2.getSC2_WCHEM12());
//        params.put("SC2_WCHEM13", scm2.getSC2_WCHEM13());
//        params.put("SC2_WCHEM14", scm2.getSC2_WCHEM14());
//        params.put("SC2_WCHEM15", scm2.getSC2_WCHEM15());
//        params.put("SC2_WCHEM16", scm2.getSC2_WCHEM16());
//        params.put("SC2_WCHEM17", scm2.getSC2_WCHEM17());
//        params.put("SC2_WCHEM18", scm2.getSC2_WCHEM18());
//        params.put("SC2_WCHEM19", scm2.getSC2_WCHEM19());
//        params.put("SC2_WCHEM20", scm2.getSC2_WCHEM20());
//        params.put("SC2_WPIG1", scm2.getSC2_WPIG1());
//        params.put("SC2_WPIG2", scm2.getSC2_WPIG2());
//        params.put("SC2_WPIG3", scm2.getSC2_WPIG3());
//        params.put("SC2_WPIG4", scm2.getSC2_WPIG4());
//        params.put("SC2_WPIG5", scm2.getSC2_WPIG5());
//        params.put("SC2_WPIG6", scm2.getSC2_WPIG6());
//        params.put("SC2_WPIG7", scm2.getSC2_WPIG7());
//        params.put("SC2_WPIG8", scm2.getSC2_WPIG8());
//        params.put("SC2_WPIG9", scm2.getSC2_WPIG9());
//        params.put("SC2_WPIG10", scm2.getSC2_WPIG10());
//        params.put("MAC2_LINE", scm2.getMAC2_LINE());
//
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
//        timeoutSetup(jsonObjReq);
//        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
//    }


    public static void insertQAM_SCALE_MAC2_Test(String factory, QAM_SCALE_MAC2 scm2, QAM_SCALE_MAC1 scm1, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertQAM_SCALE_MAC2_Test(factory, scm2,scm1);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("key", scm2.getKEY());

        params.put("SC2_SHIFT", scm2.getSC2_SHIFT());
        params.put("SC2_DATE", scm2.getSC2_DATE());
        params.put("SC2_MCS", scm2.getSC2_MCS());
        params.put("SC2_RAW1", scm2.getSC2_RAW1());
        params.put("SC2_RAW2", scm2.getSC2_RAW2());
        params.put("SC2_RAW3", scm2.getSC2_RAW3());
        params.put("SC2_RAW4", scm2.getSC2_RAW4());
        params.put("SC2_RAW5", scm2.getSC2_RAW5());
        params.put("SC2_RAW6", scm2.getSC2_RAW6());
        params.put("SC2_RAW7", scm2.getSC2_RAW7());
        params.put("SC2_RAW8", scm2.getSC2_RAW8());
        params.put("SC2_RAW9", scm2.getSC2_RAW9());
        params.put("SC2_RAW10", scm2.getSC2_RAW10());
        params.put("SC2_CHEM1", scm2.getSC2_CHEM1());
        params.put("SC2_CHEM2", scm2.getSC2_CHEM2());
        params.put("SC2_CHEM3", scm2.getSC2_CHEM3());
        params.put("SC2_CHEM4", scm2.getSC2_CHEM4());
        params.put("SC2_CHEM5", scm2.getSC2_CHEM5());
        params.put("SC2_CHEM6", scm2.getSC2_CHEM6());
        params.put("SC2_CHEM7", scm2.getSC2_CHEM7());
        params.put("SC2_CHEM8", scm2.getSC2_CHEM8());
        params.put("SC2_CHEM9", scm2.getSC2_CHEM9());
        params.put("SC2_CHEM10", scm2.getSC2_CHEM10());
        params.put("SC2_CHEM11", scm2.getSC2_CHEM11());
        params.put("SC2_CHEM12", scm2.getSC2_CHEM12());
        params.put("SC2_CHEM13", scm2.getSC2_CHEM13());
        params.put("SC2_CHEM14", scm2.getSC2_CHEM14());
        params.put("SC2_CHEM15", scm2.getSC2_CHEM15());
        params.put("SC2_CHEM16", scm2.getSC2_CHEM16());
        params.put("SC2_CHEM17", scm2.getSC2_CHEM17());
        params.put("SC2_CHEM18", scm2.getSC2_CHEM18());
        params.put("SC2_CHEM19", scm2.getSC2_CHEM19());
        params.put("SC2_CHEM20", scm2.getSC2_CHEM20());
        params.put("SC2_PIG1", scm2.getSC2_PIG1());
        params.put("SC2_PIG2", scm2.getSC2_PIG2());
        params.put("SC2_PIG3", scm2.getSC2_PIG3());
        params.put("SC2_PIG4", scm2.getSC2_PIG4());
        params.put("SC2_PIG5", scm2.getSC2_PIG5());
        params.put("SC2_PIG6", scm2.getSC2_PIG6());
        params.put("SC2_PIG7", scm2.getSC2_PIG7());
        params.put("SC2_PIG8", scm2.getSC2_PIG8());
        params.put("SC2_PIG9", scm2.getSC2_PIG9());
        params.put("SC2_PIG10", scm2.getSC2_PIG10());
        params.put("SC2_WRAW1", scm2.getSC2_WRAW1());
        params.put("SC2_WRAW2", scm2.getSC2_WRAW2());
        params.put("SC2_WRAW3", scm2.getSC2_WRAW3());
        params.put("SC2_WRAW4", scm2.getSC2_WRAW4());
        params.put("SC2_WRAW5", scm2.getSC2_WRAW5());
        params.put("SC2_WRAW6", scm2.getSC2_WRAW6());
        params.put("SC2_WRAW7", scm2.getSC2_WRAW7());
        params.put("SC2_WRAW8", scm2.getSC2_WRAW8());
        params.put("SC2_WRAW9", scm2.getSC2_WRAW9());
        params.put("SC2_WRAW10", scm2.getSC2_WRAW10());
        params.put("SC2_WCHEM1", scm2.getSC2_WCHEM1());
        params.put("SC2_WCHEM2", scm2.getSC2_WCHEM2());
        params.put("SC2_WCHEM3", scm2.getSC2_WCHEM3());
        params.put("SC2_WCHEM4", scm2.getSC2_WCHEM4());
        params.put("SC2_WCHEM5", scm2.getSC2_WCHEM5());
        params.put("SC2_WCHEM6", scm2.getSC2_WCHEM6());
        params.put("SC2_WCHEM7", scm2.getSC2_WCHEM7());
        params.put("SC2_WCHEM8", scm2.getSC2_WCHEM8());
        params.put("SC2_WCHEM9", scm2.getSC2_WCHEM9());
        params.put("SC2_WCHEM10", scm2.getSC2_WCHEM10());
        params.put("SC2_WCHEM11", scm2.getSC2_WCHEM11());
        params.put("SC2_WCHEM12", scm2.getSC2_WCHEM12());
        params.put("SC2_WCHEM13", scm2.getSC2_WCHEM13());
        params.put("SC2_WCHEM14", scm2.getSC2_WCHEM14());
        params.put("SC2_WCHEM15", scm2.getSC2_WCHEM15());
        params.put("SC2_WCHEM16", scm2.getSC2_WCHEM16());
        params.put("SC2_WCHEM17", scm2.getSC2_WCHEM17());
        params.put("SC2_WCHEM18", scm2.getSC2_WCHEM18());
        params.put("SC2_WCHEM19", scm2.getSC2_WCHEM19());
        params.put("SC2_WCHEM20", scm2.getSC2_WCHEM20());
        params.put("SC2_WPIG1", scm2.getSC2_WPIG1());
        params.put("SC2_WPIG2", scm2.getSC2_WPIG2());
        params.put("SC2_WPIG3", scm2.getSC2_WPIG3());
        params.put("SC2_WPIG4", scm2.getSC2_WPIG4());
        params.put("SC2_WPIG5", scm2.getSC2_WPIG5());
        params.put("SC2_WPIG6", scm2.getSC2_WPIG6());
        params.put("SC2_WPIG7", scm2.getSC2_WPIG7());
        params.put("SC2_WPIG8", scm2.getSC2_WPIG8());
        params.put("SC2_WPIG9", scm2.getSC2_WPIG9());
        params.put("SC2_WPIG10", scm2.getSC2_WPIG10());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }


    public static void insertQAM_SCALE_HAND(String factory, QAM_SCALE_HAND sch, QAM_SCALE_MAC2 scm2, QAM_SCALE_MAC1 scm1, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertQAM_SCALE_HAND(factory, sch,scm2,scm1);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("key", sch.getKEY());
        params.put("SCH_SHIFT", sch.getSCH_SHIFT());
        params.put("SCH_DATE", sch.getSCH_DATE());
        params.put("SCH_MCS", sch.getSCH_MCS());
        params.put("SCH_RAW1", sch.getSCH_RAW1());
        params.put("SCH_RAW2", sch.getSCH_RAW2());
        params.put("SCH_RAW3", sch.getSCH_RAW3());
        params.put("SCH_RAW4", sch.getSCH_RAW4());
        params.put("SCH_RAW5", sch.getSCH_RAW5());
        params.put("SCH_RAW6", sch.getSCH_RAW6());
        params.put("SCH_RAW7", sch.getSCH_RAW7());
        params.put("SCH_RAW8", sch.getSCH_RAW8());
        params.put("SCH_RAW9", sch.getSCH_RAW9());
        params.put("SCH_RAW10", sch.getSCH_RAW10());
        params.put("SCH_CHEM1", sch.getSCH_CHEM1());
        params.put("SCH_CHEM2", sch.getSCH_CHEM2());
        params.put("SCH_CHEM3", sch.getSCH_CHEM3());
        params.put("SCH_CHEM4", sch.getSCH_CHEM4());
        params.put("SCH_CHEM5", sch.getSCH_CHEM5());
        params.put("SCH_CHEM6", sch.getSCH_CHEM6());
        params.put("SCH_CHEM7", sch.getSCH_CHEM7());
        params.put("SCH_CHEM8", sch.getSCH_CHEM8());
        params.put("SCH_CHEM9", sch.getSCH_CHEM9());
        params.put("SCH_CHEM10", sch.getSCH_CHEM10());
        params.put("SCH_CHEM11", sch.getSCH_CHEM11());
        params.put("SCH_CHEM12", sch.getSCH_CHEM12());
        params.put("SCH_CHEM13", sch.getSCH_CHEM13());
        params.put("SCH_CHEM14", sch.getSCH_CHEM14());
        params.put("SCH_CHEM15", sch.getSCH_CHEM15());
        params.put("SCH_CHEM16", sch.getSCH_CHEM16());
        params.put("SCH_CHEM17", sch.getSCH_CHEM17());
        params.put("SCH_CHEM18", sch.getSCH_CHEM18());
        params.put("SCH_CHEM19", sch.getSCH_CHEM19());
        params.put("SCH_CHEM20", sch.getSCH_CHEM20());
        params.put("SCH_PIG1", sch.getSCH_PIG1());
        params.put("SCH_PIG2", sch.getSCH_PIG2());
        params.put("SCH_PIG3", sch.getSCH_PIG3());
        params.put("SCH_PIG4", sch.getSCH_PIG4());
        params.put("SCH_PIG5", sch.getSCH_PIG5());
        params.put("SCH_PIG6", sch.getSCH_PIG6());
        params.put("SCH_PIG7", sch.getSCH_PIG7());
        params.put("SCH_PIG8", sch.getSCH_PIG8());
        params.put("SCH_PIG9", sch.getSCH_PIG9());
        params.put("SCH_PIG10", sch.getSCH_PIG10());
        params.put("SCH_WRAW1", sch.getSCH_WRAW1());
        params.put("SCH_WRAW2", sch.getSCH_WRAW2());
        params.put("SCH_WRAW3", sch.getSCH_WRAW3());
        params.put("SCH_WRAW4", sch.getSCH_WRAW4());
        params.put("SCH_WRAW5", sch.getSCH_WRAW5());
        params.put("SCH_WRAW6", sch.getSCH_WRAW6());
        params.put("SCH_WRAW7", sch.getSCH_WRAW7());
        params.put("SCH_WRAW8", sch.getSCH_WRAW8());
        params.put("SCH_WRAW9", sch.getSCH_WRAW9());
        params.put("SCH_WRAW10", sch.getSCH_WRAW10());
        params.put("SCH_WCHEM1", sch.getSCH_WCHEM1());
        params.put("SCH_WCHEM2", sch.getSCH_WCHEM2());
        params.put("SCH_WCHEM3", sch.getSCH_WCHEM3());
        params.put("SCH_WCHEM4", sch.getSCH_WCHEM4());
        params.put("SCH_WCHEM5", sch.getSCH_WCHEM5());
        params.put("SCH_WCHEM6", sch.getSCH_WCHEM6());
        params.put("SCH_WCHEM7", sch.getSCH_WCHEM7());
        params.put("SCH_WCHEM8", sch.getSCH_WCHEM8());
        params.put("SCH_WCHEM9", sch.getSCH_WCHEM9());
        params.put("SCH_WCHEM10", sch.getSCH_WCHEM10());
        params.put("SCH_WCHEM11", sch.getSCH_WCHEM11());
        params.put("SCH_WCHEM12", sch.getSCH_WCHEM12());
        params.put("SCH_WCHEM13", sch.getSCH_WCHEM13());
        params.put("SCH_WCHEM14", sch.getSCH_WCHEM14());
        params.put("SCH_WCHEM15", sch.getSCH_WCHEM15());
        params.put("SCH_WCHEM16", sch.getSCH_WCHEM16());
        params.put("SCH_WCHEM17", sch.getSCH_WCHEM17());
        params.put("SCH_WCHEM18", sch.getSCH_WCHEM18());
        params.put("SCH_WCHEM19", sch.getSCH_WCHEM19());
        params.put("SCH_WCHEM20", sch.getSCH_WCHEM20());
        params.put("SCH_WPIG1", sch.getSCH_WPIG1());
        params.put("SCH_WPIG2", sch.getSCH_WPIG2());
        params.put("SCH_WPIG3", sch.getSCH_WPIG3());
        params.put("SCH_WPIG4", sch.getSCH_WPIG4());
        params.put("SCH_WPIG5", sch.getSCH_WPIG5());
        params.put("SCH_WPIG6", sch.getSCH_WPIG6());
        params.put("SCH_WPIG7", sch.getSCH_WPIG7());
        params.put("SCH_WPIG8", sch.getSCH_WPIG8());
        params.put("SCH_WPIG9", sch.getSCH_WPIG9());
        params.put("SCH_WPIG10", sch.getSCH_WPIG10());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }


    public static void getListQAM_SCALE_MAC1(String factory, String mac1_id, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListQAM_SCALE_MAC1(factory, mac1_id, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mac1_id", mac1_id);
        params.put("shift", shift);
        params.put("date", date);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getsum(String factory, String ins_no, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getsum(factory, ins_no);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ins_no", ins_no);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getListQAM_SCALE_MAC2(String factory, String mac2_id, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListQAM_SCALE_MAC2(factory, mac2_id, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("mac2_id", mac2_id);
        params.put("shift", shift);
        params.put("date", date);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }
    public static void getListQAM_SCALE_HAND(String factory, String hand_id, String shift, String date, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getListQAM_SCALE_HAND(factory, hand_id, shift, date);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("hand_id", hand_id);
        params.put("shift", shift);
        params.put("date", date);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void insertTEMP_NOTE(String factory, NOTE_TEMP tpn, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.insertTEMP_NOTE(factory, tpn);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("MAC", tpn.getMAC());
        params.put("SHIFT", tpn.getSHIFT());
        params.put("UP_DATE", tpn.getUP_DATE());
        params.put("ID", tpn.getID());
        params.put("NOTEMP1",tpn.getNOTEMP1());
        params.put("NOTEMP2",tpn.getNOTEMP2());
        params.put("NOTEMP3",tpn.getNOTEMP3());
        params.put("NOTEMP4",tpn.getNOTEMP4());
        params.put("NOTEMP5",tpn.getNOTEMP5());
        params.put("NOTEMP6",tpn.getNOTEMP6());
        params.put("NOTEMP7",tpn.getNOTEMP7());
        params.put("NOTEMP8",tpn.getNOTEMP8());
        params.put("NOTEMP9",tpn.getNOTEMP9());
        params.put("NOTEMP10",tpn.getNOTEMP10());
        params.put("NOTEMP11",tpn.getNOTEMP11());
        params.put("NOTEMP12",tpn.getNOTEMP12());
        params.put("NOTEMP13",tpn.getNOTEMP13());
        params.put("NOTEMP14",tpn.getNOTEMP14());
        params.put("NOTEMP15",tpn.getNOTEMP15());
        params.put("NOTEMP16",tpn.getNOTEMP16());
        params.put("DNOTEMP1",tpn.getDNOTEMP1());
        params.put("DNOTEMP2",tpn.getDNOTEMP2());
        params.put("DNOTEMP3",tpn.getDNOTEMP3());
        params.put("DNOTEMP4",tpn.getDNOTEMP4());
        params.put("DNOTEMP5",tpn.getDNOTEMP5());
        params.put("DNOTEMP6",tpn.getDNOTEMP6());
        params.put("DNOTEMP7",tpn.getDNOTEMP7());
        params.put("DNOTEMP8",tpn.getDNOTEMP8());
        params.put("DNOTEMP9",tpn.getDNOTEMP9());
        params.put("DNOTEMP10",tpn.getDNOTEMP10());
        params.put("DNOTEMP11",tpn.getDNOTEMP11());
        params.put("DNOTEMP12",tpn.getDNOTEMP12());
        params.put("DNOTEMP13",tpn.getDNOTEMP13());
        params.put("DNOTEMP14",tpn.getDNOTEMP14());
        params.put("DNOTEMP15",tpn.getDNOTEMP15());
        params.put("DNOTEMP16",tpn.getDNOTEMP16());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void update_tempnote(String factory, NOTE_TEMP tpn, String ID ,Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.update_tempnote(factory, tpn, ID);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("MAC", tpn.getMAC());
        params.put("SHIFT", tpn.getSHIFT());
        params.put("UP_DATE", tpn.getUP_DATE());
//        params.put("ID", tpn.getID());
        params.put("ID",ID );
        params.put("NOTEMP1",tpn.getNOTEMP1());
        params.put("NOTEMP2",tpn.getNOTEMP2());
        params.put("NOTEMP3",tpn.getNOTEMP3());
        params.put("NOTEMP4",tpn.getNOTEMP4());
        params.put("NOTEMP5",tpn.getNOTEMP5());
        params.put("NOTEMP6",tpn.getNOTEMP6());
        params.put("NOTEMP7",tpn.getNOTEMP7());
        params.put("NOTEMP8",tpn.getNOTEMP8());
        params.put("NOTEMP9",tpn.getNOTEMP9());
        params.put("NOTEMP10",tpn.getNOTEMP10());
        params.put("NOTEMP11",tpn.getNOTEMP11());
        params.put("NOTEMP12",tpn.getNOTEMP12());
        params.put("NOTEMP13",tpn.getNOTEMP13());
        params.put("NOTEMP14",tpn.getNOTEMP14());
        params.put("NOTEMP15",tpn.getNOTEMP15());
        params.put("NOTEMP16",tpn.getNOTEMP16());
        params.put("DNOTEMP1",tpn.getDNOTEMP1());
        params.put("DNOTEMP2",tpn.getDNOTEMP2());
        params.put("DNOTEMP3",tpn.getDNOTEMP3());
        params.put("DNOTEMP4",tpn.getDNOTEMP4());
        params.put("DNOTEMP5",tpn.getDNOTEMP5());
        params.put("DNOTEMP6",tpn.getDNOTEMP6());
        params.put("DNOTEMP7",tpn.getDNOTEMP7());
        params.put("DNOTEMP8",tpn.getDNOTEMP8());
        params.put("DNOTEMP9",tpn.getDNOTEMP9());
        params.put("DNOTEMP10",tpn.getDNOTEMP10());
        params.put("DNOTEMP11",tpn.getDNOTEMP11());
        params.put("DNOTEMP12",tpn.getDNOTEMP12());
        params.put("DNOTEMP13",tpn.getDNOTEMP13());
        params.put("DNOTEMP14",tpn.getDNOTEMP14());
        params.put("DNOTEMP15",tpn.getDNOTEMP15());
        params.put("DNOTEMP16",tpn.getDNOTEMP16());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void update_tempmodel(String factory, QAM_TEMP_MODEL qtm, String ID , Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.update_tempmodel(factory, qtm, ID);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("id", qtm.getID());
        params.put("may", qtm.getMAY());
        params.put("ca", qtm.getCA());
        params.put("ID",ID );
        params.put("check_date", qtm.getCHECK_DATE());
        params.put("check_hours", qtm.getCHECK_HOURS());
        params.put("NDTCT", qtm.getNDTCT());
        params.put("TCTGT", qtm.getTCTGT());
        params.put("NDTCP", qtm.getNDTCP());
        params.put("TCTGP", qtm.getTCTGP());

        params.put("HT1", qtm.getHT1());
        params.put("HT2", qtm.getHT2());
        params.put("HT3", qtm.getHT3());
        params.put("HT4",qtm.getHT4());
        params.put("HT5", qtm.getHT5());
        params.put("HT6", qtm.getHT6());
        params.put("HT7", qtm.getHT7());
        params.put("HT8",qtm.getHT8());
        params.put("HT9", qtm.getHT9());
        params.put("HT10", qtm.getHT10());
        params.put("HT11", qtm.getHT11());
        params.put("HT12", qtm.getHT12());
        params.put("HT13",qtm.getHT13());
        params.put("HT14",qtm.getHT14());
        params.put("HT15", qtm.getHT15());
        params.put("HT16", qtm.getHT16());

        params.put("SM1T", qtm.getSM1T());
        params.put("SM1D", qtm.getSM1D());
        params.put("SM2T", qtm.getSM2T());
        params.put("SM2D",qtm.getSM2D());
        params.put("SM3T",qtm.getSM3T());
        params.put("SM3D", qtm.getSM3D());
        params.put("SM4T", qtm.getSM4T());
        params.put("SM4D", qtm.getSM4D());
        params.put("SM5T",qtm.getSM5T());
        params.put("SM5D", qtm.getSM5D());
        params.put("SM6T", qtm.getSM6T());
        params.put("SM6D", qtm.getSM6D());
        params.put("SM7T", qtm.getSM7T());
        params.put("SM7D",qtm.getSM7D());
        params.put("SM8T",qtm.getSM8T());
        params.put("SM8D", qtm.getSM8D());
        params.put("NDM1TT", qtm.getNDM1TT());
        params.put("NDM1TP", qtm.getNDM1TP());
        params.put("NDM1DT",qtm.getNDM1DT());
        params.put("NDM1DP", qtm.getNDM1DP());
        params.put("NDM2TT", qtm.getNDM2TT());
        params.put("NDM2TP", qtm.getNDM2TP());
        params.put("NDM2DT", qtm.getNDM2DT());
        params.put("NDM2DP",qtm.getNDM2DP());
        params.put("NDM3TT", qtm.getNDM3TT());
        params.put("NDM3TP", qtm.getNDM3TP());
        params.put("NDM3DT", qtm.getNDM3DT());
        params.put("NDM3DP", qtm.getNDM3DP());
        params.put("NDM4TT",qtm.getNDM4TT());
        params.put("NDM4TP", qtm.getNDM4TP());
        params.put("NDM4DT", qtm.getNDM4DT());
        params.put("NDM4DP", qtm.getNDM4DP());
        params.put("NDM5TT", qtm.getNDM5TT());
        params.put("NDM5TP",qtm.getNDM5TP());
        params.put("NDM5DT", qtm.getNDM5DT());
        params.put("NDM5DP", qtm.getNDM5DP());
        params.put("NDM6TT", qtm.getNDM6TT());
        params.put("NDM6TP", qtm.getNDM6TP());
        params.put("NDM6DT",qtm.getNDM6DT());
        params.put("NDM6DP", qtm.getNDM6DP());
        params.put("NDM7TT", qtm.getNDM7TT());
        params.put("NDM7TP", qtm.getNDM7TP());
        params.put("NDM7DT", qtm.getNDM7DT());
        params.put("NDM7DP",qtm.getNDM7DP());
        params.put("NDM8TT", qtm.getNDM8TT());
        params.put("NDM8TP", qtm.getNDM8TP());
        params.put("NDM8DT",qtm.getNDM8DT());
        params.put("NDM8DP", qtm.getNDM8DP());
        params.put("SET_TIMEM1T", qtm.getSET_TIMEM1T());
        params.put("SET_TIMEM1D", qtm.getSET_TIMEM1D());
        params.put("SET_TIMEM2T", qtm.getSET_TIMEM2T());
        params.put("SET_TIMEM2D", qtm.getSET_TIMEM2D());
        params.put("SET_TIMEM3T",qtm.getSET_TIMEM3T());
        params.put("SET_TIMEM3D", qtm.getSET_TIMEM3D());
        params.put("SET_TIMEM4T", qtm.getSET_TIMEM4T());
        params.put("SET_TIMEM4D", qtm.getSET_TIMEM4D());
        params.put("SET_TIMEM5T", qtm.getSET_TIMEM5T());
        params.put("SET_TIMEM5D",qtm.getSET_TIMEM5D());
        params.put("SET_TIMEM6T", qtm.getSET_TIMEM6T());
        params.put("SET_TIMEM6D", qtm.getSET_TIMEM6D());

        params.put("SET_TIMEM7T", qtm.getSET_TIMEM7T());
        params.put("SET_TIMEM7D", qtm.getSET_TIMEM7D());
        params.put("SET_TIMEM8T",qtm.getSET_TIMEM8T());
        params.put("SET_TIMEM8D", qtm.getSET_TIMEM8D());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getTempND(String factory,String ID, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getTempND(factory,ID);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ID",ID );
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

    public static void getTempNOTE(String factory,String ID, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) throws JSONException {
        String url = ApiUrl.getTempNOTE(factory,ID);
        JSONObject params = new JSONObject();
        params.put("factory", factory);
        params.put("ID",ID );
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, params, listener, errorListener);
        timeoutSetup(jsonObjReq);
        AppController.getInstance().addToRequestQueue(jsonObjReq, url);
    }

}