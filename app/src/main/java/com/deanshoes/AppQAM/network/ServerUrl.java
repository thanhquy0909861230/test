package com.deanshoes.AppQAM.network;

import com.deanshoes.AppQAM.BuildConfig;
import com.deanshoes.dsapptools.tools.attr.DSServerUrl;

public class ServerUrl {

    public static String SERVER_URL(String factory) {

        String url = DSServerUrl.SERVER_URL(factory);

        if (BuildConfig.DEBUG)
        {
            // 若需由自己本機的IP測試，修改下方設定
//            url = "http://5.1.103.200:8080/";
//            FVI
//                    url = "http://10.17.1.17:8080/";
//            FVI-II
                  url = "http://10.21.1.65:8080/";
 
//            url = "http://5.1.182.118:8081/"; //prince



        }
        return url;
    }

    public static String SERVER_URL1(String factory) {

        String url1 = DSServerUrl.SERVER_URL(factory);
        if (BuildConfig.DEBUG)
        {
            // 若需由自己本機的IP測試，修改下方設定
//            url = "http://5.1.103.200:8080/";
//            FVI
//                    url = "http://10.17.1.17:8080/";
//            FVI-II
//                  url = "http://10.21.1.65:8080/";

//            url = "http://5.1.182.118:8081/"; //prince

            url1 = "http://5.1.182.118:8081/";

        }
        return url1;
    }


}
