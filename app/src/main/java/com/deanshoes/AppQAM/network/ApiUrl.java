package com.deanshoes.AppQAM.network;

import com.deanshoes.AppQAM.model.QAM.BTC;
import com.deanshoes.AppQAM.model.QAM.BTC_OP;
import com.deanshoes.AppQAM.model.QAM.BTC_PELL;
import com.deanshoes.AppQAM.model.QAM.NOTE_TEMP;
import com.deanshoes.AppQAM.model.QAM.QAM_AMT;
import com.deanshoes.AppQAM.model.QAM.QAM_BTC;
import com.deanshoes.AppQAM.model.QAM.QAM_CHEM;
import com.deanshoes.AppQAM.model.QAM.QAM_CR;
import com.deanshoes.AppQAM.model.QAM.QAM_MAC;
import com.deanshoes.AppQAM.model.QAM.QAM_MCS;
import com.deanshoes.AppQAM.model.QAM.QAM_MODEL;
import com.deanshoes.AppQAM.model.QAM.QAM_MTR;
import com.deanshoes.AppQAM.model.QAM.QAM_PIG;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_HAND;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_MAC1;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_MAC2;
import com.deanshoes.AppQAM.model.QAM.QAM_TEMP_MODEL;

import static com.deanshoes.AppQAM.network.ServerUrl.SERVER_URL;
import static com.deanshoes.AppQAM.network.ServerUrl.SERVER_URL1;
public class ApiUrl {

    //TODO 修改為此App呼叫的主要 WebService
    public static String API_URL(String factory) {

        return SERVER_URL(factory) + "YourAppWebService/webapi/";

    }

    //此為通用 App Web Service
    public static String APP_API_URL(String factory) {

        return SERVER_URL(factory) + "appwebservice/webapi/";
    }


    public static String POST_TEST(String factory) {

        return APP_API_URL(factory) + "hello/postTest";
    }


    //TODO 修改為此App的APK檔案名稱
    public static String APP_UPDATE_FILE_URL(String factory) {
        return SERVER_URL(factory) + "apppool/AppQAM.apk";
    }

    public static String DOWNLOAD_APK(String factory, String fileName) {
        return SERVER_URL(factory) + "apppool/" + fileName;
    }

    public static String getmcs(String factory, String mcs_date,String mcs_shift){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getmcs";
        //return SERVER_URL1(factory) + "QualityAssurance/webapi/qam/getmcs";
    }
    public static String getCurrentTime(String factory){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/curenttime";
        //return SERVER_URL1(factory) + "QualityAssurance/webapi/qam/curenttime";
    }

    public static String getbtc(String factory, String mcs_no,String mcs_date,String mcs_shift){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getbtc";
        //return SERVER_URL1(factory) + "QualityAssurance/webapi/qam/getbtc";
    }
    public static String getbtc2(String factory, String mcs_no,String mcs_shift,String mcs_date){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getbtc";
        //return SERVER_URL1(factory) + "QualityAssurance/webapi/qam/getbtc";
    }
    public static String insertKneader(String factory, BTC btc){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/insertKN";
        //return SERVER_URL1(factory) + "QualityAssurance/webapi/qam/insertKN";
    }

    public static String getbtc_op(String factory, String mcs_no,String mcs_date,String mcs_shift){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getbtc_op";
        //return SERVER_URL1(factory) + "QualityAssurance/webapi/qam/getbtc_op";
    }
    public static String insertOpenmill(String factory, BTC_OP btc_op){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/insertOP";
        //return SERVER_URL1(factory) + "QualityAssurance/webapi/qam/insertOP";
    }

    public static String getbtc_pell(String factory, String ins_no,String mcs_date,String mcs_shift){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getbtc_pell";
        //return SERVER_URL1(factory) + "QualityAssurance/webapi/qam/getbtc_pell";
    }
    public static String insertPelletization(String factory, BTC_PELL btc_pell){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/insertPELL";
    }
    public static String insertPelletization_NEW(String factory, BTC_PELL btc_pell, String mcs_no, String mcs_date, String mcs_shift){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/insertPELL_NEW";
    }
    public static String insertNEW_BATCH(String factory, BTC_PELL btc_pell, String ins_no, String mcs_date, String mcs_shift){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/insertNEW_BATCH";
    }
    public static String getnewbatch(String factory, String mcs_date,String mcs_shift,String mcs_no){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getnewbatch";
    }

    //Dain - begin
    public static String getFileName(String factory,String mind, String maxd) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getFileName";
    }

    public static String getListMCS(String factory,String mcs_date, String mcs_shift) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getmcs1";
    }

    public static String getInforMCS(String factory,String mcs_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getinfor";
    }

    public static String getListBTC(String factory,String ins_no, String mcs_date, String mcs_shift) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getbtc1";
    }

    public static String checkBatch_no(String factory,String ins_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/checkBatch_no";
    }

    public static String deleteMAC(String factory,String ins_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/deleteMAC";
    }

    public static String deleteBatchno(String factory,String btc_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/deleteBatchno";
    }

    public static String getMaxBatchNo(String factory,String mcs_no, String mcs_date, String mcs_shift, String mcs_line, String mcs_name) {
            return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getmaxbatch";
    }

    public static String insertBTC(String factory, QAM_BTC btc){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/insertbtc";
    }

    public static String getListMacNo(String factory,String mcs_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getmacno";
    }

    public static String getListRaw_MTR(String factory) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getraw_mtr";
    }

    public static String getcolor(String factory) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getcolor";
    }

    public static String getListChem(String factory) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getchem";
    }

    public static String checkMCS_Excel(String factory, String date1,String shift, String mcs) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/checkMCS_Excel";
    }

    public static String getListPig(String factory) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getpigment";
    }

    public static String check_can11(String factory, String mcs, String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/check_can11";
    }

    public static String getListMac(String factory, String mcs_no, String shift, String date, String line ) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getmac";
    }

    public static String check_can1(String factory,  String mac1_id, String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/check_can1";
    }

    public static String check_can2(String factory,  String mac2_id, String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/check_can2";
    }

    public static String insertTEMP_NOTE(String factory, NOTE_TEMP qam_temp_note) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertqam_temp_note";
    }

    public static String getTempND(String factory,String ID) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getTempND";
    }

    public static String getTempNOTE(String factory,String ID) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getTempNOTE";
    }

    public static String check_cantay(String factory, String hand_id, String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/check_cantay";
    }

    public static String checkINS(String factory, QAM_MAC mac, String mcs_no, String shift, String date){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/checkINS";
    }

    public static String insertMAC(String factory, QAM_MAC mac, String mcs_no, String shift, String date, String line){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/insertmac";
    }

    public static String insertMAC2(String factory, QAM_MAC mac, String mcs_no, String shift, String date, String line){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/insertmac2";
    }

    public static String insertMAC_EXCEL(String factory, QAM_MAC mac, String mcs, String shift, String date, String line, String style){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/insertmac_excel";
    }

    public static String getGroupID(String factory, String shift, String date){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getGroupID";
    }

    public static String getGroupIDpdf(String factory, String shift, String date){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getGroupIDpdf";
    }

    public static String getGroupID2(String factory, String shift, String date){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getGroupID2";
        //return SERVER_URL1(factory) + "QualityAssurance/webapi/qam/getGroupID2";
    }

    public static String getGroupIDstep3(String factory, String shift, String date){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getGroupIDstep3";
    }

    public static String getListAMT_FVI2(String factory,String ins_no , String mcs_date, String mcs_shift,String mcs_line) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getamt_FVI2";
    }

    public static String getListAMT_FVI(String factory,String ins_no , String mcs_date, String mcs_shift,String mcs_line) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getamt_FVI";
    }

    public static String updateAMT(String factory, QAM_AMT amt){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/updateamt";
    }

//    public static String getQAM_NOTE(String factory,String position , String sub_no, String btc_no) {
//        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getnote";
//    }

    public static String updateNOTE(String factory, String position, String key, String value){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/updatenote";
    }

    public static String updatenote_temp(String factory, String position, String key, String value){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/updatenote_temp";
    }

    public static String getListMac3(String factory, String ins_no, String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/getmac3";
    }

    public static String insertMAC3(String factory, QAM_MAC mac, String ins_no, String shift, String date){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/insertmac3";
    }
    // end cmnl

    //rp

    public static String getNewBatchNo(String factory,String date, String shift) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getNewBatchNo";
    }
    public static String getMcs(String factory,String date, String shift) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/mcs";
    }
    public static String getstyle(String factory, String mcs, String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getstyle";
    }
    public static String getcolor(String factory, String mcs,String style,String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getcolor";
    }
    public static String getbtc1(String factory, String mcs,String style,String color,String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getbtc1";
    }

    public static String getbtc2(String factory, String mcs,String style,String color,String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getbtc2";
    }
    public static String getbtcno(String factory, String mcs, String style, String color,String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/rp";
    }

    public static String getData(String factory, String ins_no,String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getData";
    }

    public static String getcrname(String factory){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getcrname";}
    public static String getcrmcs(String factory,String crname){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getcrmcs";}
    public static String insertmcs(String factory, QAM_MCS mcs){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertmcs";}
    public static String getmcslist(String factory,String date, String shift){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getmcslist";}
    public static String updateMcsList(String factory, QAM_MCS mcs){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/updatemcs";}
    public static String deletemcs(String factory,String mcs_no ){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/deletemcs";}
    public static String getmtr(String factory){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getmtr";}
    public static String getpig(String factory){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getpig";}
    public static String getchemical(String factory){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getchemical";}
    public static String getmodel(String factory){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getmodel";}
    public static String setcolor(String factory,String style){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getqamcolor";}
    public static String getlistmodel(String factory){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getlistmodel";}
    public static String insertQAM_MTR(String factory, QAM_MTR mtr){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertQAM_MTR";}
    public static String updateQAM_MTR(String factory, QAM_MTR mtr){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/updateQAM_MTR";}
    public static String insertQAM_PIG(String factory, QAM_PIG pig){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertQAM_PIG";}
    public static String updateQAM_PIG(String factory, QAM_PIG pig){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/updateQAM_PIG";}
    public static String insertQAM_MODEL(String factory, QAM_MODEL model){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertQAM_MODEL";
    }
    public static String insertQAM_MODEL_STYLE(String factory, QAM_MODEL model){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertQAM_MODEL_STYLE";}
    public static String insertQAM_MODEL_COLOR(String factory, QAM_MODEL model){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertQAM_MODEL_COLOR";}
    public static String updateQAM_MODELE(String factory, QAM_MODEL model){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/updateQAM_model";
    }
    public static String updateQAM_MODEL_STYLE(String factory, QAM_MODEL model){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/updateQAM_model_style";}
    public static String updateQAM_MODEL_COLOR(String factory, QAM_MODEL model){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/updateQAM_model_color";}
    public static String insertQAM_CHEM(String factory, QAM_CHEM chem){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertQAM_CHEM";}
    public static String updateQAM_CHEM(String factory, QAM_CHEM chem){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/updateQAM_CHEM";}
    public static String insertQAM_CR(String factory, QAM_CR cr){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertQAM_CR";}
    public static String updateQAM_CR(String factory, QAM_CR cr){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/updateQAM_CR";}
    public static String getcrlist(String factory){ return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getcrosslist";}

    public static String deleteStyle(String factory,String style_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/deletestyle";
    }

    public static String checkStyle(String factory,String style, String color) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/checkStyle";
    }

    public static String checkStyle_fvi2(String factory,String style) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/checkStyle_fvi2";
    }

    public static String checkColor_fvi2(String factory, String color) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/checkColor_fvi2";
    }

    public static String deleteCrocs(String factory,String crocs_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/deletecross";
    }

    public static String checkCrocs(String factory,String crocs) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/checkCrocs";
    }

    public static String deleteMCS(String factory,String mcs_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/deleteMCS";
    }

    public static String checkMCS(String factory,String mcs_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/checkMCS";
    }

    public static String deleteMTR(String factory,String mtr_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/deletemtr";
    }

    public static String checkMTR(String factory,String mtr) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/checkMaterial";
    }

    public static String deleteChemical(String factory,String chem_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/deletechemical";
    }

    public static String checkChemical(String factory,String chem) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/checkChemical";
    }

    public static String deletePigment(String factory,String pig_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/deletepigment";
    }

    public static String checkPigment(String factory,String pig) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/checPigment";
    }

    //report - QAM
    public static String getRP_MAC(String factory,String ins_no, String date, String shift) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getrp_mac";
    }

    public static String getListBatch(String factory, String ins_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/get_listbatch";
    }

    public static String getListKneader(String factory, String ins_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/get_listkneader";
    }

    public static String getListOpenMill(String factory, String ins_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/get_listop";
    }

    public static String getListPell(String factory, String ins_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/get_listpell";
    }

    public static String gettimekneader(String factory, String btc_no, String ins_no) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam/gettimekneader";
    }
    public static String checkThong(String factory) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getChem_Check_FVI2";
    }
    public static String TempModel(String factory ,String may, String ca, String ngay) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/TempModel_FVI2";
    }

    public static String saveTempModel(String factory, QAM_TEMP_MODEL qtm) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/SaveTempModel_FVI2()";
    }
    public static String getMCS_color(String factory, String  shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getMCS_color";
    }
    public static String getMCS_FVI(String factory, String  shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getMCS_FVI";
    }
    public static String getMaDonINS_FVI(String factory,  String date,String  shift, String color, String mcs) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getMaDonINS_FVI";
    }

    public static String insertQAM_TEMP_MODEL(String factory, QAM_TEMP_MODEL qam_temp_model ) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertqam_temp_model";
      //  return "http://5.1.182.118:8081/" + "QualityAssurance/webapi/qam2/insertqam_temp_model";
    }

    public static String getIdModel(String factory,String date, String ca, String may) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getIdModel";
    }

    public static String getIdModel2(String factory) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getIdModel2";
    }

    public static String getListQAM_TEMP_MODEL(String factory, String ID) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getListQAM_TEMP_MODEL";
    }

    public static String deleteID(String factory, String ID) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/deleteID";
    }

    public static String update_tempnote(String factory, NOTE_TEMP qam_temp_note, String ID) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/update_tempnote";
    }

    public static String update_tempmodel(String factory, QAM_TEMP_MODEL qam_temp_model, String ID ) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/update_tempmodel";
    }

    public static String getIdColor(String factory,String shift, String date) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getIDColor";
    }

    public static String insertQAM_SCALE_MAC1(String factory, QAM_SCALE_MAC1 scm1 ) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertqam_scale_mac1";
    }
//    public static String insertQAM_SCALE_MAC2(String factory, QAM_SCALE_MAC2 scm2 ) {
//        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertqam_scale_mac2";
//    }

    public static String insertQAM_SCALE_MAC2_Test(String factory, QAM_SCALE_MAC2 scm2, QAM_SCALE_MAC1 scm1) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertqam_scale_mac2_test";
    }
    public static String insertQAM_SCALE_HAND(String factory, QAM_SCALE_HAND sch, QAM_SCALE_MAC2 scm2, QAM_SCALE_MAC1 scm1 ) {
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/insertqam_scale_hand";
    }

    public static String getListQAM_SCALE_MAC1(String factory, String mac1_id , String shift, String date){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getListQAM_SCALE_MAC1";
    }

    public static String getsum(String factory, String ins_no ){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getsum";
    }

    public static String getListQAM_SCALE_MAC2(String factory, String mcs , String shift, String date){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getListQAM_SCALE_MAC2";
    }

    public static String getListQAM_SCALE_HAND(String factory, String mcs , String shift, String date){
        return SERVER_URL(factory) + "QualityAssurance/webapi/qam2/getListQAM_SCALE_HAND";
    }


}
