package com.deanshoes.AppQAM;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.db.UserPermissionDAO;
import com.deanshoes.AppQAM.helper.AppAttr;
import com.deanshoes.AppQAM.helper.AppFactory;
import com.deanshoes.AppQAM.network.ApiUrl;
import com.deanshoes.AppQAM.service.GeneralService;
import com.deanshoes.dsapptools.tools.DSCommonTools;
import com.deanshoes.dsapptools.tools.DSLogTools;
import com.deanshoes.dsapptools.tools.DefaultExceptionHandler;
import com.deanshoes.dsapptools.tools.attr.DSAppSetup;
import com.deanshoes.dsapptools.tools.attr.DSFactoryID;
import com.deanshoes.dsapptools.tools.attr.DSTags;
import com.deanshoes.dsapptools.tools.interfaces.IOnApkDownloadDialogActionListener;
import com.deanshoes.dsapptools.tools.interfaces.IOnDialogActionListener;
import com.deanshoes.dsapptools.tools.interfaces.IOnSelectFactoryDialogActionListener;
import com.deanshoes.dsapptools.tools.model.AppUseHistoryModel;
import com.deanshoes.dsapptools.tools.model.UserModel;
import com.deanshoes.dsapptools.tools.model.UserPermissionModel;
import com.deanshoes.dsapptools.tools.network.ConnectionDetector;
import com.deanshoes.dsapptools.tools.network.DSCallApi;
import com.deanshoes.dsapptools.tools.ui.dialog.DownloadApkDialogFragment;
import com.deanshoes.dsapptools.tools.ui.dialog.MessageWithIconDialogFragment;
import com.deanshoes.dsapptools.tools.ui.dialog.SelectFactoryDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


/**
 * App 介紹頁，為App進入點，在此執行更新檢查，權限檢查
 *
 * 執行流程：
 * 讀取來自入口 App 所傳送過來的帳號資訊
 *      無帳號資料 --> 跳轉回入口App
 *      有帳號資料 --> 檢查請求手機的權限是否同意
 *                          不同意 --> 要求權限
 *                          同意   --> 取得帳號於此App的權限設定，並存至本機DB中 -->
 *                                          檢查 App 是否有更新檔 -->
 *                                              是 --> 下載更新
 *                                              否 --> 跳轉至其他頁面
 *
 */
public class IntroActivity extends AppCompatActivity {

    private final static String TAG = IntroActivity.class.getSimpleName();

    // 網路連結檢查工具
    ConnectionDetector mConnectionDetector;

    Handler mHandler;

    UserModel mCurUser;

    DownloadTask mDownloadTask;

    // 由此判斷此 App 是否需登入
    boolean mNeedLogin = true;


    //region UI

    TextView mTxtAppVersion;

    TextView mTxtAppMessage;

    ProgressDialog mProgressDialog;

    //endregion UI


    //region Inner class

    static class MyHandler extends Handler {

        private final WeakReference<IntroActivity> mThis;

        public MyHandler(IntroActivity activity) {
            mThis = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            IntroActivity activity = mThis.get();
            if (activity != null) {

                try {
                    switch (msg.what) {


                        default:
                            break;


                    }
                } catch (Exception e) {

                    DSLogTools.e(TAG, "Exception: " + e.getMessage());

                }

            }
        }
    }



    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {

            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(600000);
                connection.connect();


                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // 取得檔案大小
                int fileLength = connection.getContentLength();

                File root = Environment.getExternalStorageDirectory();

                String filePath = root.getAbsolutePath() + File.separator + getApplicationContext().getPackageName()
                        + File.separator + "update.apk";

                // 開始下載檔案
                input = connection.getInputStream();
                output = new FileOutputStream(filePath);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);

                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (result != null)
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            else {

                File root = Environment.getExternalStorageDirectory();

                String filePath = root.getAbsolutePath() + File.separator + getApplicationContext().getPackageName()
                        + File.separator + "update.apk";

                File file = new File(filePath);
                if(file.exists() && file.isFile()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        try {
                            Uri apkUri = FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID + ".fileprovider", file);
                            Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                            intent.setData(apkUri);
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Uri path = Uri.fromFile(file);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(path, "application/vnd.android.package-archive");

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
                        startActivity(intent);
                    }
                } else {
                    DSLogTools.d("RunStep", "No File");
                }

            }
        }


    }




    //endregion Inner class


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //region 非固定設定，可選用

        // 設定將未Catch的錯誤，記錄至檔案中
        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(getApplicationContext()));

        // 全螢幕
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // 無標題
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        //endregion

        setContentView(R.layout.activity_intro);

        initData();


        this.mTxtAppVersion = (TextView) findViewById(R.id.lbl_app_version);

        this.mTxtAppMessage = (TextView) findViewById(R.id.lbl_app_message);


        String ver = "";

        try {
            ver = getString(R.string.version) + "：" + getPackageManager().getPackageInfo(getPackageName(),0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        this.mTxtAppVersion.setText(ver);

        initUI();

        // 取得前導APP所送過來的使用者資訊
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            boolean isLogin = bundle.getBoolean("isLogin", false);
            String userID = bundle.getString("userID", "");
            String userName = bundle.getString("userName", "");
            String factory = bundle.getString("factory", "");

            // 有登入的資訊
            if (isLogin && !userID.isEmpty() && !userName.isEmpty() && !factory.isEmpty()) {

                UserModel user = new UserModel();

                user.mFactoryName = factory;
                user.mUserID = userID;
                user.mUserName = userName;
                user.mIsLock = false;

                DSCommonTools.saveAppUser(getApplicationContext(), user);

            }

        }

        // 開發模式且非經由App Portal進入，由此控制登入廠區
        if (BuildConfig.DEBUG && !DSCommonTools.isLogin(getApplicationContext())) {

            //region 開發模式

            // 假登入
            UserModel user = new UserModel();

            user.mFactoryName = AppFactory.APP_USE;
            user.mUserID = "0100000000";
            user.mUserName = "FVGIT";
            user.mIsLock = false;

            DSCommonTools.saveAppUser(getApplicationContext(), user);

            //endregion
        }

    }

    @Override
    protected void onResume() {

        super.onResume();


        // 若此App皆需要連網操作時，檢查是否有連網
        if (!mConnectionDetector.isConnectingToInternet()) {

            new AlertDialog.Builder(IntroActivity.this).setTitle(getString(R.string.network_error)).setMessage("此App需連至網路")
                    // 設定是否可以點擊螢幕其他地方或上一頁鍵取消
                    .setCancelable(true)
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();

                        }
                    }).show();

        } else {

            // 檢查手機權限
            checkPermission();
        }

    }


    /**
     * 初始化UI
     */
    private void initUI() {




    }


    /**
     * 初始化資料
     */
    private  void initData() {

        // 若需要登入，代表需由入口App傳送使用者資料來，所以需清除上一個使用者資料
        // 若不需登入，則會產生一個預設使用者，並提供選擇廠區的設定，不清除使用者資料
        if(mNeedLogin) {
            DSCommonTools.clearUser(getApplicationContext());
        }

        mConnectionDetector = new ConnectionDetector(getApplicationContext());

        mHandler = new MyHandler(this);

    }


    /**
     * 返回入口 App
     */
    private void return2IntoApp() {

        Intent intent = getPackageManager().getLaunchIntentForPackage(DSTags.DS_APP_PORTAL_PACKAGE_NAME);

        if (intent != null) {

            startActivity(intent);

            finish();

        } else {

            downloadAPK(DSAppSetup.PortalAppFileName);

        }

    }


    private void openSelectFactoryDialog() {

        SelectFactoryDialogFragment.newInstance(new IOnSelectFactoryDialogActionListener() {

            @Override
            public void onAction(String factory) {


                dismissSelectFactoryDialog();

                // 假登入
                UserModel user = new UserModel();

                user.mFactoryName = factory;
                user.mUserID = "0000000000";
                user.mUserName = "GUEST";
                user.mIsLock = false;

                DSCommonTools.saveAppUser(getApplicationContext(), user);

                mCurUser = DSCommonTools.getAppUser(getApplicationContext());

                // 檢查App更新
                checkAppUpdate();

            }

            @Override
            public void onCancel() {



            }

        }, BuildConfig.DEBUG, true, getString(com.deanshoes.dsapptools.R.string.ok), false, getString(com.deanshoes.dsapptools.R.string.cancel), "請先選擇廠區")
                .show(getSupportFragmentManager(), SelectFactoryDialogFragment.class.getSimpleName());

    }


    private void dismissSelectFactoryDialog() {

        DialogFragment fragment = (DialogFragment) getSupportFragmentManager().findFragmentByTag(SelectFactoryDialogFragment.class.getSimpleName());
        if (fragment != null) fragment.dismiss();

    }


    /**
     * 手機權限請求
     */
    private void checkPermission() {

        // 存取儲存空間的權限

       if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    DSTags.REQUEST_EXTERNAL_STORAGE
            );

        } else {
           //上列權限皆同意，進行App更新檢查
           checkLoginInfo();
       }




        /***
         * 新檢查 Android 權限方法, 需引用  compile 'com.github.karanchuri:PermissionManager:0.1.0'
         PermissionManager permission=new PermissionManager() {

            @Override
            public boolean checkAndRequestPermissions(Activity activity) {

                boolean isOK = super.checkAndRequestPermissions(activity);

                if(isOK) {
                    checkLoginInfo();
                }

                    return isOK;
                }
            };

         permission.checkAndRequestPermissions(this);

         */

    }

    /**
     * 檢查app是否有新版
     */
    private void checkAppUpdate() {


        try {

            // 取得版本號
            int appCode = DSCommonTools.getVersionCode(getApplicationContext());

            DSCallApi.checkAppUpdate(DSFactoryID.FVI_II_305, AppAttr.AppID, appCode, BuildConfig.DEBUG,

                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject jObj) {

                            boolean updatable = false;
                            boolean mustUpdate = false;
                            String updateMsg = "";

                            if(jObj != null) {

                                try {

                                    boolean isError = jObj.optBoolean("error", true);

                                    if(!isError) {

                                        //== 是否有更新
                                        updatable = jObj.optBoolean("need_to_update", false);

                                        updateMsg = jObj.optString("update_note", "");
                                    }


                                } catch (Exception error) {

                                    DSLogTools.e("AJAX", "Exception: " + error.getMessage());
                                    error.printStackTrace();
                                }

                            }

                            if(updatable) {

                                //== 如果有更新，進入更新程序流程
                                updateProcess();

                            } else {

                                //== 如果沒有更新，執行正常啟動流程
                                getUserPermission();

                            }


                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            DSLogTools.e("AJAX", "Response Error: " +error.getMessage());

                            // 發生錯誤，直接進入主畫面
                            getUserPermission();
                        }
                    });


        } catch (JSONException je) {

        }


    }

    /**
     * 更新程序
     */
    private void updateProcess() {

        if (Environment.getExternalStorageState()//確定SD卡可讀寫
                .equals(Environment.MEDIA_MOUNTED)) {

            File sdFile = android.os.Environment.getExternalStorageDirectory();
            String path = sdFile.getPath() + File.separator + getApplicationContext().getPackageName();

            File dirFile = new File(path);

            boolean isCreateOK = false;
            if (!dirFile.exists()) {//如果資料夾不存在
                isCreateOK = dirFile.mkdir();//建立資料夾
            }

            MessageWithIconDialogFragment fragment = (MessageWithIconDialogFragment) getSupportFragmentManager().findFragmentByTag(MessageWithIconDialogFragment.class.getSimpleName());
            if (fragment != null) fragment.dismiss();

            MessageWithIconDialogFragment.newInstance(new IOnDialogActionListener() {

                @Override
                public void onAction() {

                    MessageWithIconDialogFragment fragment = (MessageWithIconDialogFragment) getSupportFragmentManager().findFragmentByTag(MessageWithIconDialogFragment.class.getSimpleName());
                    if (fragment != null) fragment.dismiss();

                    downloadUpdate();

                }

                @Override
                public void onCancel() {
                    MessageWithIconDialogFragment fragment = (MessageWithIconDialogFragment) getSupportFragmentManager().findFragmentByTag(MessageWithIconDialogFragment.class.getSimpleName());
                    if (fragment != null) fragment.dismiss();
                }

            }, true, getString(R.string.ok), false, "", "", false,  getString(R.string.please_update_app), R.drawable.ic_download_outline, R.color.icon_default_color, true)
                    .show(getSupportFragmentManager(), MessageWithIconDialogFragment.class.getSimpleName());


        } else {

            Toast.makeText(getApplicationContext(), "無法讀寫儲存區", Toast.LENGTH_LONG).show();

        }

    }


    private void downloadUpdate() {
        mProgressDialog = new ProgressDialog(IntroActivity.this);
        mProgressDialog.setMessage("下載更新檔中");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);

        String factory = mCurUser.mFactoryName;

        if (mDownloadTask == null) {
            mDownloadTask = new DownloadTask(IntroActivity.this);
            mDownloadTask.execute(ApiUrl.APP_UPDATE_FILE_URL(factory));
        } else if (mDownloadTask.isCancelled() || mDownloadTask.getStatus().equals(AsyncTask.Status.FINISHED)) {
            mDownloadTask = new DownloadTask(IntroActivity.this);
            mDownloadTask.execute(ApiUrl.APP_UPDATE_FILE_URL(factory));
        }

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mDownloadTask.cancel(true);
            }
        });

    }

    /**
     * 取得使用者於此App的權限
     */
    private void getUserPermission() {


        try {

            DSCallApi.getAppPermissions(mCurUser.mFactoryName, AppAttr.AppID, AppAttr.AppKey, AppAttr.AppID, mCurUser.mUserID,

                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject jObj) {

                            if (jObj != null) {

                                try {

                                    boolean isError = jObj.optBoolean("error", true);

                                    if (!isError) {

                                        //region 更新使用者權限

                                        JSONArray ruleList = jObj.optJSONArray("rule_list");


                                        UserPermissionDAO permissionDAO = new UserPermissionDAO(getApplicationContext());

                                        permissionDAO.deleteAll();

                                        Calendar now = Calendar.getInstance();
                                        long updateTime = now.getTimeInMillis();
                                        ArrayList<UserPermissionModel> permissionList = new ArrayList<>();

                                        for (int i = 0; i < ruleList.length(); i++) {

                                            JSONObject rule = ruleList.getJSONObject(i);


                                            UserPermissionModel permission = new UserPermissionModel();

                                            permission.mUserID = mCurUser.mUserID;
                                            permission.mAppID = rule.optString("APP_ID", "");
                                            permission.mFunctionID = rule.optInt("SUB_FUN_ID", -1);
                                            permission.mIsOpen = rule.optBoolean("IS_OPEN", false);
                                            permission.mCanInquiry = rule.optBoolean("CAN_INQUIRY", false);
                                            permission.mCanCreate = rule.optBoolean("CAN_CREATE", false);
                                            permission.mCanEdit = rule.optBoolean("CAN_EDIT", false);
                                            permission.mCanDelete = rule.optBoolean("CAN_DELETE", false);

                                            permissionList.add(permission);


                                        }

                                        permissionDAO.insert(permissionList);

                                        permissionDAO.close();

                                        //endregion 更新使用者權限
                                    }


                                } catch (Exception error) {

                                    DSLogTools.e("AJAX", "Exception: " + error.getMessage());
                                    DSLogTools.e(getApplicationContext(), "Exception: " + error.getMessage());

                                    error.printStackTrace();
                                }

                            }

                            initProcess();

                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {


                            DSLogTools.e("AJAX", "Response Error: " + error.getMessage());
                            DSLogTools.e(getApplicationContext(), "Response Error: " + error.getMessage());

                            if (error.networkResponse != null) {

                                DSLogTools.e("AJAX", "Response statusCode: " + Integer.toString(error.networkResponse.statusCode));

                                DSLogTools.e(getApplicationContext(), "Response statusCode: " + Integer.toString(error.networkResponse.statusCode));
                            }

                            initProcess();

                        }
                    });


        } catch (JSONException je) {

        }


    }

    /**
     * 傳送設備資訊與該App的資料，由此記錄是否有設備沒有更新App
     */
    private void saveAppUseHistory() {


        try {

            AppUseHistoryModel appHistory = new AppUseHistoryModel();
            appHistory.DEVICE_TOKEN = "";// TODO 若有使用推播，此處用來存放推播TOKEN
            appHistory.APP_ID = AppAttr.AppID;
            appHistory.FACTORY = mCurUser.mFactoryName;
            appHistory.PLATFORM = "android";
            appHistory.SDK_VER = String.format(Locale.getDefault(),"%d", Build.VERSION.SDK_INT);
            appHistory.APP_VER_CODE = DSCommonTools.getVersionCode(getApplicationContext());

            DSCallApi.saveAppInfo(getApplicationContext(), mCurUser.mFactoryName, AppAttr.AppID, AppAttr.AppKey,  mCurUser.mUserID,  appHistory,

                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject jObj) {


                            //TODO


                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            //TODO

                        }
                    });


        } catch (Exception je) {

            //TODO
        }

    }


    /**
     * 啟動程序
     */
    private void initProcess() {

        //== 延遲1秒後 進入主畫面
        this.mHandler.postDelayed(new Runnable() {

            public void run() {


                // TODO 若需在背景處理一些事情，可在此 Service 中寫入需要的程式
                Intent serviceIntent = new Intent(getApplicationContext(), GeneralService.class);
                startService(serviceIntent);


                // TODO 修改為此 App 的實際啟始頁

                Intent intent = new Intent();
                intent.setClass(IntroActivity.this, LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                finish();


            }
        }, 1000);

    }

    /**
     * 檢查登入資訊
     */
    private void checkLoginInfo() {


        if (DSCommonTools.isLogin(getApplicationContext())) {

            //region 有登入資訊

            mCurUser = DSCommonTools.getAppUser(getApplicationContext());

            // 檢查App更新
            checkAppUpdate();

            //endregion

        } else {

            // 若App需要登入
            if(mNeedLogin) {

                // 無登入資訊，呼叫入口App
                return2IntoApp();

            } else {

                //TODO 若App不需登入，要讓使用者選擇廠區，且此 App 需於提供操作者重新選擇廠區的功能，請自行撰寫

                //不需登入，要選擇現在操作者的廠區
                openSelectFactoryDialog();

            }

        }

    }

    private void dismissDownloadApkDialog() {

        DownloadApkDialogFragment fragment = (DownloadApkDialogFragment) getSupportFragmentManager().findFragmentByTag(DownloadApkDialogFragment.class.getSimpleName());
        if (fragment != null) fragment.dismiss();
    }

    private void downloadAPK(String fileName){

        try {

            if (Environment.getExternalStorageState()//確定SD卡可讀寫
                    .equals(Environment.MEDIA_MOUNTED)) {

                File sdFile = android.os.Environment.getExternalStorageDirectory();
                String path = sdFile.getPath() + File.separator + getApplicationContext().getPackageName();

                File dirFile = new File(path);

                boolean isCreateOK = false;
                if (!dirFile.exists()) {//如果資料夾不存在
                    isCreateOK = dirFile.mkdir();//建立資料夾
                }

                dismissDownloadApkDialog();

                DownloadApkDialogFragment.newInstance(new IOnApkDownloadDialogActionListener() {
                    @Override
                    public void onAction(String factory, String fileName) {

                        dismissDownloadApkDialog();

                        downloadPortalApp(factory, fileName);
                    }

                    @Override
                    public void onCancel() {
                        dismissDownloadApkDialog();
                    }

                }, BuildConfig.DEBUG, fileName, true,getString(R.string.ok), false, getString(R.string.cancel), getString(R.string.need_to_download_portal_app), true, R.drawable.ic_download_outline, R.color.icon_default_color)
                        .show(getSupportFragmentManager(), DownloadApkDialogFragment.class.getSimpleName());

            } else {

                Toast.makeText(getApplicationContext(), "無法讀寫儲存區", Toast.LENGTH_LONG).show();

            }



        } catch (Exception e) {

            Log.d("RunStep", e.getMessage());
        }

    }

    private void downloadPortalApp(String factory, String fileName) {

        mProgressDialog = new ProgressDialog(IntroActivity.this);
        mProgressDialog.setMessage("下載檔案中");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);

        if (mDownloadTask == null) {
            mDownloadTask = new DownloadTask(IntroActivity.this);
            mDownloadTask.execute(ApiUrl.DOWNLOAD_APK(factory, fileName));
        } else if (mDownloadTask.isCancelled() || mDownloadTask.getStatus().equals(AsyncTask.Status.FINISHED)) {
            mDownloadTask = new DownloadTask(IntroActivity.this);
            mDownloadTask.execute(ApiUrl.DOWNLOAD_APK(factory, fileName));
        }

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mDownloadTask.cancel(true);
            }
        });

    }




}
