package com.deanshoes.AppQAM;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step1.ChemicalActivity;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.deanshoes.AppQAM.step2.fvi.Mac2Activity_FVI2;
import com.deanshoes.AppQAM.step3.FVI.Kneader_Controller;
import com.deanshoes.AppQAM.step4.pdfActivity;
import com.deanshoes.AppQAM.step5.checkTempIP;
import com.deanshoes.AppQAM.step5.mac1fvi2;
import com.deanshoes.dsapptools.tools.DSCommonTools;
import com.deanshoes.dsapptools.tools.model.UserModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {
    private Spinner spr_part,spr_fac,spn_id2;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private EditText edt_user;
    private Button btn_login, btn_login2;
    private ArrayList<String> listPart,listFac;
    private ArrayAdapter PartAdapter, FacAdapter;
    public static String factory = "FVI_II_305";
    ArrayList<String> listIDmodel;
    private ArrayAdapter arrIdModel;
    UserModel user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        spr_part=findViewById(R.id.spr_part);
        spn_id2=findViewById(R.id.spn_id2);
        spr_fac= findViewById(R.id.spr_fac);
        edt_user=findViewById(R.id.edt_user);
        edt_user.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        btn_login=findViewById(R.id.btn_login);
        btn_login2=findViewById(R.id.btn_login2);
        defaultSpinner();

        spr_part.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_login2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    CallApi.getIdModel2(factory, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            IDmodel iDmodel = gson.fromJson(response.toString(), IDmodel.class);
                            listIDmodel = iDmodel.getItems() == null ? new ArrayList<String>() : iDmodel.getItems();
                            arrIdModel = new ArrayAdapter(LoginActivity.this, R.layout.spn_layout, listIDmodel);
                            arrIdModel.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
                            spn_id2.setAdapter(arrIdModel);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean flag=true;
                Bundle bundle=new Bundle();
                bundle.putString("user",edt_user.getText().toString());
                if (spr_fac.getSelectedItem().toString().equals("FVI")){
                    bundle.putString("fac","FVI");
                }else if (spr_fac.getSelectedItem().toString().equals("FVI_II_305")){
                    bundle.putString("fac","FVI_II_305");
                }

                if(edt_user.getText().toString().isEmpty()){
                    ToastCustom.message(getApplicationContext(), "Vui lòng nhập tên user!", Color.RED);
                    flag=false;
                }

                if (flag==true){
                    if (spr_part.getSelectedItemPosition()==0){
                        Intent intent =new Intent(LoginActivity.this, ChemicalActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }

                    if (spr_part.getSelectedItemPosition()==1){
//                        if(spr_fac.getSelectedItem().toString().equals("FVI_II_305")){
//                            Intent intent =new Intent(LoginActivity.this, Mac2Activity_FVI2.class);
//                            intent.putExtras(bundle);
//                            startActivity(intent);
//                        }
//                        else {
//                            Intent intent =new Intent(LoginActivity.this, Mac1Activity_FVI.class);
//                            intent.putExtras(bundle);
//                            startActivity(intent);
//                        }
                        Intent intent =new Intent(LoginActivity.this, Mac1Activity_FVI.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                    if (spr_part.getSelectedItemPosition()==2){
                        Intent intent =new Intent(LoginActivity.this, Kneader_Controller.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                    if (spr_part.getSelectedItemPosition()==3){
                        Intent intent =new Intent(LoginActivity.this, pdfActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }


                    if (spr_part.getSelectedItemPosition()==4){
                        Intent intent =new Intent(LoginActivity.this, mac1fvi2.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                    if (spr_part.getSelectedItemPosition()==5){
                        Intent intent =new Intent(LoginActivity.this, checkTempIP.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                }

            }
        });
    }

    public void defaultSpinner() {
        user = DSCommonTools.getAppUser(getApplicationContext());

        System.out.println("fac ne:  " +user.mFactoryName);

//        listFac = new ArrayList<>();
//        listFac.add("FVI");


        listFac = new ArrayList<>();
        listFac.add("FVI_II_305");

        FacAdapter = new ArrayAdapter(LoginActivity.this, R.layout.login_spiner, listFac);
        FacAdapter.setDropDownViewResource(R.layout.login_spiner);
        spr_fac.setAdapter(FacAdapter);

        listPart = new ArrayList<>();
        listPart.add("Thông Tin Cơ Bản");
        listPart.add("Biểu Kiểm Tra Phối Liệu");
        listPart.add("Lý Nả/Máy Cán/Tạo Hạt");
        listPart.add("In file PDF");
        if (spr_fac.getSelectedItem().toString().equals("FVI_II_305")){
            listPart.add("Báo Kiểm Nhiệt Độ Khuôn IP");
            listPart.add("Đo Kiểm Khuôn");

        }



        PartAdapter = new ArrayAdapter(LoginActivity.this, R.layout.login_spiner, listPart){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return true;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.BLACK);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        PartAdapter.setDropDownViewResource(R.layout.login_spiner);
        spr_part.setAdapter(PartAdapter);
        spr_part.setSelection(0, false);

    }

    public class IDmodel extends ApiReturnModel<ArrayList<String>> {}
}
