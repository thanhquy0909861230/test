package com.deanshoes.AppQAM.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.R;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.adapter.reportadapter.ListIPBatchAdapter;
import com.deanshoes.AppQAM.adapter.reportadapter.ListIPBatchFVI2Adapter;
import com.deanshoes.AppQAM.model.QAM.MODEL_SEND_MAIL;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_HAND;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_MAC1;
import com.deanshoes.AppQAM.model.QAM.QAM_SCALE_MAC2;
import com.deanshoes.AppQAM.model.QAM.RP_LIST_BATCH;
import com.deanshoes.AppQAM.model.QAM.RP_MAC;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step4.SendMail;
import com.deanshoes.AppQAM.step4.pdfActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.ss.util.RegionUtil;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;


public class IP_BatchFragment extends Fragment {

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    ArrayList<RP_MAC> RP_MAC;
    TextView tv_1,tv_2,tv_3,tv_4,tv_5,tv_6,tv_7,tv_8,tv_9,tv_10;
    TextView tv_11,tv_12,tv_13,tv_14,tv_15,tv_16,tv_17,tv_18,tv_19,tv_20;
    TextView tv_21,tv_22,tv_23,tv_24,tv_25,tv_26,tv_27,tv_28,tv_29,tv_30;
    TextView tv_31,tv_32,tv_33,tv_34,tv_35,tv_36,tv_37,tv_38,tv_39,tv_40;

    TextView tv_s1,tv_s2,tv_s3,tv_s4,tv_s5,tv_s6,tv_s7,tv_s8,tv_s9,tv_s10;
    TextView tv_s11,tv_s12,tv_s13,tv_s14,tv_s15,tv_s16,tv_s17,tv_s18,tv_s19,tv_s20;
    TextView tv_s21,tv_s22,tv_s23,tv_s24,tv_s25,tv_s26,tv_s27,tv_s28,tv_s29,tv_s30;
    TextView tv_s31,tv_s32,tv_s33,tv_s34,tv_s35,tv_s36,tv_s37,tv_s38,tv_s39,tv_s40;

    TextView tv_mcs, tv_shift,tv_date;
    static TextView tv_sum1,tv_sum2;
    ListView rv_ipbatch;
    public static String[] scale1;
    static String[] wscale1;
    public static String[] scale2;
    static String[] wscale2;
    public static String[] scale3;
    static String[] wscale3;


    ImageButton ib_sendmail;
    String Filename="";
    public static String date="", shift ="", ins_no = "",MCS="";
    String factory="";
    String FILE_NAME="";
    public static int mtr_num=0,chem_num=0,pig_num=0;

    TextView[] list_tv, list_w;
    public static String[] list_vl, list_wvl;
    public static ArrayList<RP_LIST_BATCH> RP_LIST_BATCH;
    public  ArrayList<QAM_SCALE_MAC1> list_mac1;
    public  ArrayList<QAM_SCALE_MAC2> list_mac2;
    public  ArrayList<QAM_SCALE_HAND> list_hand;




    public static IP_BatchFragment newInstance(String title) {
        IP_BatchFragment fragment = new IP_BatchFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ip__batch, container, false);
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        RP_LIST_BATCH = new ArrayList<>();
        factory=pdfActivity.factory;
        ins_no = pdfActivity.ins_no;
        date= pdfActivity.date;
        shift =pdfActivity.shift;
        MCS = pdfActivity.MCS;
        //anh ta vốn dĩ giàu sang còn anh thì bữa cơm manh áo, cơ hàn
        //đổi tình đổi áo đổi anh, em cần phồn hoa đến thế, thôi anh cũng đành
        //cuộc đời tha hương, lưu lạc nơi xứ người
        //bao nhiêu thứ lo toan nên chẳng có thời gian
        //chăm sóc em tận tình như người kia mới đến
        //anh dành cả sáng đêm cũng chẳng thể giữ được em
        //

//        scale1 = new String[]{"TAIC","AZO-805","AR316","EVA8502","LS3123","TLA-142","LS1655","UP-55","MD-3","1/10 LS1132",
//                "1/10 LS3123","1/10 EVA8502","LS5005","SQ116"};
//        scale2 = new String[]{"LS1132","VT-35","STA-1801","EVA5502","EVA4643","LS2506","AC3000","LS5013","LS3125","R103",
//                "SK-901","LS1186","9353"};



//        list_vl = new String[]{};
        findbyID();
        tv_mcs.setText("MCS# "+ MCS);
        tv_shift.setText(shift);
        tv_date.setText(date);

//        FILE_NAME = "IP-BATCH_"+ ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
//                +"_" + shift ;

        FILE_NAME = ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
                +"_" + shift ;

        list_tv = new TextView[]{tv_1,tv_2,tv_3,tv_4,tv_5,tv_6,tv_7,tv_8,tv_9,tv_10,
                tv_11,tv_12,tv_13,tv_14,tv_15,tv_16,tv_17,tv_18,tv_19,tv_20,
                tv_21,tv_22,tv_23,tv_24,tv_25,tv_26,tv_27,tv_28,tv_29,tv_30,
                tv_31,tv_32,tv_33,tv_34,tv_35,tv_36,tv_37,tv_38,tv_39,tv_40};

        list_w = new TextView[]{tv_s1,tv_s2,tv_s3,tv_s4,tv_s5,tv_s6,tv_s7,tv_s8,tv_s9,tv_s10,
                tv_s11,tv_s12,tv_s13,tv_s14,tv_s15,tv_s16,tv_s17,tv_s18,tv_s19,tv_s20,
                tv_s21,tv_s22,tv_s23,tv_s24,tv_s25,tv_s26,tv_s27,tv_s28,tv_s29,tv_s30,
                tv_s31,tv_s32,tv_s33,tv_s34,tv_s35,tv_s36,tv_s37,tv_s38,tv_s39,tv_s40};



        getListQAM_SCALE_MAC1(factory, ins_no , shift, date);
        getListQAM_SCALE_MAC2(factory, ins_no, shift, date);
        getListQAM_SCALE_HAND();


//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                ib_sendmail.performClick();
//            }
//        }, 10000);
//
//        ib_sendmail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sendmail_dialog();
//            }
//        });

        getList_MAC();

        getList_BATCH();




    }



    MODEL_SEND_MAIL model;
    String[] ListFile;
    String DEST = Environment.getExternalStorageDirectory().toString() + "/QAM_EXCEL/";
    public void sendmail_dialog(){
//        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
//        b.setTitle("Xác nhận");
//        b.setMessage("Bạn có muốn gửi báo biểu về Mail?");
//        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
        //sendmail
        String name1 = DEST+ ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
                +"_" + shift + ".xls" ;
//                String name2 = DEST+ ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
//                        +"_" + shift + ".xls";
//                String name3 = DEST+ ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
//                        +"_" + shift + ".xls";
//                String name4 = DEST+FILE_NAME + ".xls";

        model = new MODEL_SEND_MAIL();
        model.setMSC(MCS);
        model.setDATE(date);
        model.setSHIFT(shift);
        model.setSTYLE(pdfActivity.style);
        model.setCOLOR(pdfActivity.color);
        model.setINS_NO(ins_no);

        ListFile = new String[]{name1};
        SendMail sendmail = new SendMail();
        sendmail.sendmail_report(factory,ListFile,model,Filename);

        ToastCustom.message(getActivity(), "Gửi thành công!", Color.GREEN);
//            }
//        });
//        b.setNegativeButton("Không",  new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.cancel();
//            }
//        });
//        AlertDialog al = b.create();
//        al.show();
    }

    public void getList_MAC(){


        try{
            CallApi.getRP_MAC(factory, ins_no, date, shift, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    RP_MAC = new ArrayList<>();
                    RP_MACArm rp_macArm = gson.fromJson(response.toString(), RP_MACArm.class);
                    RP_MAC = rp_macArm.getItems() == null ? new ArrayList<RP_MAC>() : rp_macArm.getItems();

                    if (RP_MAC.size() > 0) {
                        list_vl = new String[]{RP_MAC.get(0).getMAC_MTR1(), RP_MAC.get(0).getMAC_MTR2(), RP_MAC.get(0).getMAC_MTR3(), RP_MAC.get(0).getMAC_MTR4(),
                                RP_MAC.get(0).getMAC_MTR5(), RP_MAC.get(0).getMAC_MTR6(), RP_MAC.get(0).getMAC_MTR7(), RP_MAC.get(0).getMAC_MTR8(), RP_MAC.get(0).getMAC_MTR9(),
                                RP_MAC.get(0).getMAC_MTR10(), RP_MAC.get(0).getMAC_CHEM1(), RP_MAC.get(0).getMAC_CHEM2(), RP_MAC.get(0).getMAC_CHEM3(), RP_MAC.get(0).getMAC_CHEM4(),
                                RP_MAC.get(0).getMAC_CHEM5(), RP_MAC.get(0).getMAC_CHEM6(), RP_MAC.get(0).getMAC_CHEM7(), RP_MAC.get(0).getMAC_CHEM8(), RP_MAC.get(0).getMAC_CHEM9(),
                                RP_MAC.get(0).getMAC_CHEM10(), RP_MAC.get(0).getMAC_CHEM11(), RP_MAC.get(0).getMAC_CHEM12(), RP_MAC.get(0).getMAC_CHEM13(), RP_MAC.get(0).getMAC_CHEM14(),
                                RP_MAC.get(0).getMAC_CHEM15(), RP_MAC.get(0).getMAC_CHEM16(), RP_MAC.get(0).getMAC_CHEM17(), RP_MAC.get(0).getMAC_CHEM18(), RP_MAC.get(0).getMAC_CHEM19(),
                                RP_MAC.get(0).getMAC_CHEM20(), RP_MAC.get(0).getMAC_PIG1(), RP_MAC.get(0).getMAC_PIG2(), RP_MAC.get(0).getMAC_PIG3(), RP_MAC.get(0).getMAC_PIG4(),
                                RP_MAC.get(0).getMAC_PIG5(), RP_MAC.get(0).getMAC_PIG6(), RP_MAC.get(0).getMAC_PIG7(), RP_MAC.get(0).getMAC_PIG8(), RP_MAC.get(0).getMAC_PIG9(),
                                RP_MAC.get(0).getMAC_PIG10()};

                        list_wvl = new String[]{RP_MAC.get(0).getSMAC_MTR1(), RP_MAC.get(0).getSMAC_MTR2(), RP_MAC.get(0).getSMAC_MTR3(), RP_MAC.get(0).getSMAC_MTR4(),
                                RP_MAC.get(0).getSMAC_MTR5(), RP_MAC.get(0).getSMAC_MTR6(), RP_MAC.get(0).getSMAC_MTR7(), RP_MAC.get(0).getSMAC_MTR8(), RP_MAC.get(0).getSMAC_MTR9(),
                                RP_MAC.get(0).getSMAC_MTR10(), RP_MAC.get(0).getSMAC_CHEM1(), RP_MAC.get(0).getSMAC_CHEM2(), RP_MAC.get(0).getSMAC_CHEM3(), RP_MAC.get(0).getSMAC_CHEM4(),
                                RP_MAC.get(0).getSMAC_CHEM5(), RP_MAC.get(0).getSMAC_CHEM6(), RP_MAC.get(0).getSMAC_CHEM7(), RP_MAC.get(0).getSMAC_CHEM8(), RP_MAC.get(0).getSMAC_CHEM9(),
                                RP_MAC.get(0).getSMAC_CHEM10(), RP_MAC.get(0).getSMAC_CHEM11(), RP_MAC.get(0).getSMAC_CHEM12(), RP_MAC.get(0).getSMAC_CHEM13(), RP_MAC.get(0).getSMAC_CHEM14(),
                                RP_MAC.get(0).getSMAC_CHEM15(), RP_MAC.get(0).getSMAC_CHEM16(), RP_MAC.get(0).getSMAC_CHEM17(), RP_MAC.get(0).getSMAC_CHEM18(), RP_MAC.get(0).getSMAC_CHEM19(),
                                RP_MAC.get(0).getSMAC_CHEM20(), RP_MAC.get(0).getSMAC_PIG1(), RP_MAC.get(0).getSMAC_PIG2(), RP_MAC.get(0).getSMAC_PIG3(), RP_MAC.get(0).getSMAC_PIG4(),
                                RP_MAC.get(0).getSMAC_PIG5(), RP_MAC.get(0).getSMAC_PIG6(), RP_MAC.get(0).getSMAC_PIG7(), RP_MAC.get(0).getSMAC_PIG8(), RP_MAC.get(0).getSMAC_PIG9(),
                                RP_MAC.get(0).getSMAC_PIG10()};

                        setValue(list_tv, list_vl,list_w,list_wvl);
                    }



//
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getList_BATCH(){
        try{
            CallApi.getListBatch(factory, ins_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    RP_BATCHArm rp_batchArm = gson.fromJson(response.toString(), RP_BATCHArm.class);
                    RP_LIST_BATCH = rp_batchArm.getItems() == null ? new ArrayList<RP_LIST_BATCH>() : rp_batchArm.getItems();
                    if (factory.equals("FVI")){
                        ListIPBatchAdapter listIPBatchAdapter =
                                new ListIPBatchAdapter(getActivity(),R.layout.rp_ipbatch_adapter, RP_LIST_BATCH,list_vl);
                        rv_ipbatch.setAdapter(listIPBatchAdapter);
                    }else if (factory.equals("FVI_II_305")){
                        ListIPBatchFVI2Adapter listIPBatchAdapter = new ListIPBatchFVI2Adapter(getActivity(),R.layout.rp_ipbatch_adapter_fvi2, RP_LIST_BATCH);
                        rv_ipbatch.setAdapter(listIPBatchAdapter);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    public static boolean checkValue(String str){
        if (str !=null && !str.equals("")){
            return true;
        }
        return false;
    }

    public void getListQAM_SCALE_MAC1(String factory, String ins_no, String shift, String date) {//String date, String ca, String may

        try {

            CallApi.getListQAM_SCALE_MAC1(factory, ins_no , shift, date, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    QSM1 qsm1 = gson.fromJson(response.toString(),  QSM1.class);
                    chem_num=0;
                    list_mac1 = qsm1.getItems() == null ? new ArrayList<QAM_SCALE_MAC1>() : qsm1.getItems();
                    if(list_mac1.size() > 0){
                        scale1 = new String[]{
                                list_mac1.get(0).getSC1_RAW1(), list_mac1.get(0).getSC1_RAW2(), list_mac1.get(0).getSC1_RAW3(), list_mac1.get(0).getSC1_RAW4(),
                                list_mac1.get(0).getSC1_RAW5(), list_mac1.get(0).getSC1_RAW6(), list_mac1.get(0).getSC1_RAW7(), list_mac1.get(0).getSC1_RAW8(), list_mac1.get(0).getSC1_RAW9(),
                                list_mac1.get(0).getSC1_RAW10(), list_mac1.get(0).getSC1_CHEM1(), list_mac1.get(0).getSC1_CHEM2(), list_mac1.get(0).getSC1_CHEM3(), list_mac1.get(0).getSC1_CHEM4(),
                                list_mac1.get(0).getSC1_CHEM5(), list_mac1.get(0).getSC1_CHEM6(), list_mac1.get(0).getSC1_CHEM7(), list_mac1.get(0).getSC1_CHEM8(), list_mac1.get(0).getSC1_CHEM9(),
                                list_mac1.get(0).getSC1_CHEM10(), list_mac1.get(0).getSC1_CHEM11(), list_mac1.get(0).getSC1_CHEM12(), list_mac1.get(0).getSC1_CHEM13(), list_mac1.get(0).getSC1_CHEM14(),
                                list_mac1.get(0).getSC1_CHEM15(), list_mac1.get(0).getSC1_CHEM16(), list_mac1.get(0).getSC1_CHEM17(), list_mac1.get(0).getSC1_CHEM18(), list_mac1.get(0).getSC1_CHEM19(),
                                list_mac1.get(0).getSC1_CHEM20(), list_mac1.get(0).getSC1_PIG1(), list_mac1.get(0).getSC1_PIG2(), list_mac1.get(0).getSC1_PIG3(), list_mac1.get(0).getSC1_PIG4(),
                                list_mac1.get(0).getSC1_PIG5(), list_mac1.get(0).getSC1_PIG6(), list_mac1.get(0).getSC1_PIG7(), list_mac1.get(0).getSC1_PIG8(), list_mac1.get(0).getSC1_PIG9(),
                                list_mac1.get(0).getSC1_PIG10()

                        };
                        wscale1 = new String[]{
                                list_mac1.get(0).getSC1_WRAW1(), list_mac1.get(0).getSC1_WRAW2(), list_mac1.get(0).getSC1_WRAW3(), list_mac1.get(0).getSC1_WRAW4(),
                                list_mac1.get(0).getSC1_WRAW5(), list_mac1.get(0).getSC1_WRAW6(), list_mac1.get(0).getSC1_WRAW7(), list_mac1.get(0).getSC1_WRAW8(), list_mac1.get(0).getSC1_WRAW9(),
                                list_mac1.get(0).getSC1_WRAW10(), list_mac1.get(0).getSC1_WCHEM1(), list_mac1.get(0).getSC1_WCHEM2(), list_mac1.get(0).getSC1_WCHEM3(), list_mac1.get(0).getSC1_WCHEM4(),
                                list_mac1.get(0).getSC1_WCHEM5(), list_mac1.get(0).getSC1_WCHEM6(), list_mac1.get(0).getSC1_WCHEM7(), list_mac1.get(0).getSC1_WCHEM8(), list_mac1.get(0).getSC1_WCHEM9(),
                                list_mac1.get(0).getSC1_WCHEM10(), list_mac1.get(0).getSC1_WCHEM11(), list_mac1.get(0).getSC1_WCHEM12(), list_mac1.get(0).getSC1_WCHEM13(), list_mac1.get(0).getSC1_WCHEM14(),
                                list_mac1.get(0).getSC1_WCHEM15(), list_mac1.get(0).getSC1_WCHEM16(), list_mac1.get(0).getSC1_WCHEM17(), list_mac1.get(0).getSC1_WCHEM18(), list_mac1.get(0).getSC1_WCHEM19(),
                                list_mac1.get(0).getSC1_WCHEM20(), list_mac1.get(0).getSC1_WPIG1(), list_mac1.get(0).getSC1_WPIG2(), list_mac1.get(0).getSC1_WPIG3(), list_mac1.get(0).getSC1_WPIG4(),
                                list_mac1.get(0).getSC1_WPIG5(), list_mac1.get(0).getSC1_WPIG6(), list_mac1.get(0).getSC1_WPIG7(), list_mac1.get(0).getSC1_WPIG8(), list_mac1.get(0).getSC1_WPIG9(),
                                list_mac1.get(0).getSC1_WPIG10()
                        };

                        for(int i=0;i< scale1.length;i++){
                            if(scale1[i]!=null ){
                                if (checkValue(scale1[i]) && scale1[i].equals(scale1[i])){
                                    chem_num++;

                                }

                            }
                        }

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getListQAM_SCALE_MAC2(String factory, String ins_no, String shift,String date) {

        try {

            CallApi.getListQAM_SCALE_MAC2(factory, ins_no, shift, date, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    QSM2 qsm2 = gson.fromJson(response.toString(),  QSM2.class);
                    pig_num=0;
                    list_mac2 = qsm2.getItems() == null ? new ArrayList<QAM_SCALE_MAC2>() : qsm2.getItems();
                    if(list_mac2.size() > 0){
                        scale2 = new String[]{
                                list_mac2.get(0).getSC2_RAW1(), list_mac2.get(0).getSC2_RAW2(), list_mac2.get(0).getSC2_RAW3(), list_mac2.get(0).getSC2_RAW4(),
                                list_mac2.get(0).getSC2_RAW5(), list_mac2.get(0).getSC2_RAW6(), list_mac2.get(0).getSC2_RAW7(), list_mac2.get(0).getSC2_RAW8(), list_mac2.get(0).getSC2_RAW9(),
                                list_mac2.get(0).getSC2_RAW10(), list_mac2.get(0).getSC2_CHEM1(), list_mac2.get(0).getSC2_CHEM2(), list_mac2.get(0).getSC2_CHEM3(), list_mac2.get(0).getSC2_CHEM4(),
                                list_mac2.get(0).getSC2_CHEM5(), list_mac2.get(0).getSC2_CHEM6(), list_mac2.get(0).getSC2_CHEM7(), list_mac2.get(0).getSC2_CHEM8(), list_mac2.get(0).getSC2_CHEM9(),
                                list_mac2.get(0).getSC2_CHEM10(), list_mac2.get(0).getSC2_CHEM11(), list_mac2.get(0).getSC2_CHEM12(), list_mac2.get(0).getSC2_CHEM13(), list_mac2.get(0).getSC2_CHEM14(),
                                list_mac2.get(0).getSC2_CHEM15(), list_mac2.get(0).getSC2_CHEM16(), list_mac2.get(0).getSC2_CHEM17(), list_mac2.get(0).getSC2_CHEM18(), list_mac2.get(0).getSC2_CHEM19(),
                                list_mac2.get(0).getSC2_CHEM20(), list_mac2.get(0).getSC2_PIG1(), list_mac2.get(0).getSC2_PIG2(), list_mac2.get(0).getSC2_PIG3(), list_mac2.get(0).getSC2_PIG4(),
                                list_mac2.get(0).getSC2_PIG5(), list_mac2.get(0).getSC2_PIG6(), list_mac2.get(0).getSC2_PIG7(), list_mac2.get(0).getSC2_PIG8(), list_mac2.get(0).getSC2_PIG9(),
                                list_mac2.get(0).getSC2_PIG10()
                        };
                        wscale2 = new String[]{
                                list_mac2.get(0).getSC2_WRAW1(), list_mac2.get(0).getSC2_WRAW2(), list_mac2.get(0).getSC2_WRAW3(), list_mac2.get(0).getSC2_WRAW4(),
                                list_mac2.get(0).getSC2_WRAW5(), list_mac2.get(0).getSC2_WRAW6(), list_mac2.get(0).getSC2_WRAW7(), list_mac2.get(0).getSC2_WRAW8(), list_mac2.get(0).getSC2_WRAW9(),
                                list_mac2.get(0).getSC2_WRAW10(), list_mac2.get(0).getSC2_WCHEM1(), list_mac2.get(0).getSC2_WCHEM2(), list_mac2.get(0).getSC2_WCHEM3(), list_mac2.get(0).getSC2_WCHEM4(),
                                list_mac2.get(0).getSC2_WCHEM5(), list_mac2.get(0).getSC2_WCHEM6(), list_mac2.get(0).getSC2_WCHEM7(), list_mac2.get(0).getSC2_WCHEM8(), list_mac2.get(0).getSC2_WCHEM9(),
                                list_mac2.get(0).getSC2_WCHEM10(), list_mac2.get(0).getSC2_WCHEM11(), list_mac2.get(0).getSC2_WCHEM12(), list_mac2.get(0).getSC2_WCHEM13(), list_mac2.get(0).getSC2_WCHEM14(),
                                list_mac2.get(0).getSC2_WCHEM15(), list_mac2.get(0).getSC2_WCHEM16(), list_mac2.get(0).getSC2_WCHEM17(), list_mac2.get(0).getSC2_WCHEM18(), list_mac2.get(0).getSC2_WCHEM19(),
                                list_mac2.get(0).getSC2_WCHEM20(), list_mac2.get(0).getSC2_WPIG1(), list_mac2.get(0).getSC2_WPIG2(), list_mac2.get(0).getSC2_WPIG3(), list_mac2.get(0).getSC2_WPIG4(),
                                list_mac2.get(0).getSC2_WPIG5(), list_mac2.get(0).getSC2_WPIG6(), list_mac2.get(0).getSC2_WPIG7(), list_mac2.get(0).getSC2_WPIG8(), list_mac2.get(0).getSC2_WPIG9(),
                                list_mac2.get(0).getSC2_WPIG10()
                        };


                        for(int i=0; i<scale2.length;i++){
                            if(scale2[i]!= null){
                                if(checkValue(scale2[i]) && scale2[i].equals(scale2[i])){

                                    pig_num++;

                                }

                            }
                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public void getListQAM_SCALE_HAND(){

        try{
            CallApi.getListQAM_SCALE_HAND(factory, ins_no, shift, date, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    QSM3 qsm3 = gson.fromJson(response.toString(), QSM3.class);
                    mtr_num =0;
                    list_hand = qsm3.getItems() == null ? new ArrayList<QAM_SCALE_HAND>() : qsm3.getItems();

                    if (list_hand.size() > 0) {
                        scale3 = new String[]{list_hand.get(0).getSCH_RAW1(), list_hand.get(0).getSCH_RAW2(), list_hand.get(0).getSCH_RAW3(), list_hand.get(0).getSCH_RAW4(),
                                list_hand.get(0).getSCH_RAW5(), list_hand.get(0).getSCH_RAW6(), list_hand.get(0).getSCH_RAW7(), list_hand.get(0).getSCH_RAW8(), list_hand.get(0).getSCH_RAW9(),
                                list_hand.get(0).getSCH_RAW10(), list_hand.get(0).getSCH_CHEM1(), list_hand.get(0).getSCH_CHEM2(), list_hand.get(0).getSCH_CHEM3(), list_hand.get(0).getSCH_CHEM4(),
                                list_hand.get(0).getSCH_CHEM5(), list_hand.get(0).getSCH_CHEM6(), list_hand.get(0).getSCH_CHEM7(), list_hand.get(0).getSCH_CHEM8(), list_hand.get(0).getSCH_CHEM9(),
                                list_hand.get(0).getSCH_CHEM10(), list_hand.get(0).getSCH_CHEM11(), list_hand.get(0).getSCH_CHEM12(), list_hand.get(0).getSCH_CHEM13(), list_hand.get(0).getSCH_CHEM14(),
                                list_hand.get(0).getSCH_CHEM15(), list_hand.get(0).getSCH_CHEM16(), list_hand.get(0).getSCH_CHEM17(), list_hand.get(0).getSCH_CHEM18(), list_hand.get(0).getSCH_CHEM19(),
                                list_hand.get(0).getSCH_CHEM20(), list_hand.get(0).getSCH_PIG1(), list_hand.get(0).getSCH_PIG2(), list_hand.get(0).getSCH_PIG3(), list_hand.get(0).getSCH_PIG4(),
                                list_hand.get(0).getSCH_PIG5(), list_hand.get(0).getSCH_PIG6(), list_hand.get(0).getSCH_PIG7(), list_hand.get(0).getSCH_PIG8(), list_hand.get(0).getSCH_PIG9(),
                                list_hand.get(0).getSCH_PIG10()};

                        wscale3= new String[]{list_hand.get(0).getSCH_WRAW1(), list_hand.get(0).getSCH_WRAW2(), list_hand.get(0).getSCH_WRAW3(), list_hand.get(0).getSCH_WRAW4(),
                                list_hand.get(0).getSCH_WRAW5(), list_hand.get(0).getSCH_WRAW6(), list_hand.get(0).getSCH_WRAW7(), list_hand.get(0).getSCH_WRAW8(), list_hand.get(0).getSCH_WRAW9(),
                                list_hand.get(0).getSCH_WRAW10(), list_hand.get(0).getSCH_WCHEM1(), list_hand.get(0).getSCH_WCHEM2(), list_hand.get(0).getSCH_WCHEM3(), list_hand.get(0).getSCH_WCHEM4(),
                                list_hand.get(0).getSCH_WCHEM5(), list_hand.get(0).getSCH_WCHEM6(), list_hand.get(0).getSCH_WCHEM7(), list_hand.get(0).getSCH_WCHEM8(), list_hand.get(0).getSCH_WCHEM9(),
                                list_hand.get(0).getSCH_WCHEM10(), list_hand.get(0).getSCH_WCHEM11(), list_hand.get(0).getSCH_WCHEM12(), list_hand.get(0).getSCH_WCHEM13(), list_hand.get(0).getSCH_WCHEM14(),
                                list_hand.get(0).getSCH_WCHEM15(), list_hand.get(0).getSCH_WCHEM16(), list_hand.get(0).getSCH_WCHEM17(), list_hand.get(0).getSCH_WCHEM18(), list_hand.get(0).getSCH_WCHEM19(),
                                list_hand.get(0).getSCH_WCHEM20(), list_hand.get(0).getSCH_WPIG1(), list_hand.get(0).getSCH_WPIG2(), list_hand.get(0).getSCH_WPIG3(), list_hand.get(0).getSCH_WPIG4(),
                                list_hand.get(0).getSCH_WPIG5(), list_hand.get(0).getSCH_WPIG6(), list_hand.get(0).getSCH_WPIG7(), list_hand.get(0).getSCH_WPIG8(), list_hand.get(0).getSCH_WPIG9(),
                                list_hand.get(0).getSCH_WPIG10()};

                        for(int i=0;i<scale3.length;i++){
                            if (scale3[i]!=null){
                                if(checkValue(scale3[i]) && scale3[i].equals(scale3[i])){
                                    mtr_num++;
                                }

                            }
                        }

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    public static void setValue(TextView[] list_tv,String[] str,TextView[] list_stv,String[] wei){
        int pos=0,sum1=0,sum2=0;
        ArrayList<String> list1 = new ArrayList<String>(); //list scale 1
        ArrayList<String> list2 = new ArrayList<>();//list scale 1
        ArrayList<String> list3 = new ArrayList<>();//list no scale

        ArrayList<String> wei1 = new ArrayList<>(); //list scale 1
        ArrayList<String> wei2 = new ArrayList<String>();//list scale 1
        ArrayList<String> wei3 = new ArrayList<String>();//list no scale
        //set value raw
        for (int i =0;i<list_tv.length-30;i++){
            if (checkValue(str[i])){
                list_tv[pos].setText(str[pos]);
                list_tv[pos].setVisibility(View.VISIBLE);

                list_stv[pos].setText(wei[pos]);
                list_stv[pos].setVisibility(View.VISIBLE);
                pos=pos+1;
            }else{
                list_tv[pos].setVisibility(View.GONE);
            }
        }
        for (int i =0;i<list_tv.length;i++){
            if (checkValue(str[i])){
                //               if (checkValue(scale1[i]) && scale1[i].equals(str[i])) {
                list1.add(str[i]);
                wei1.add(wei[i]);
                //               }
//                else if (checkValue(scale2[i]) && scale2[i].equals(str[i])) {
                list2.add(str[i]);
                wei2.add(wei[i]);
//
//                }
                //else {
                list3.add(str[i]);
                wei3.add(wei[i]);

//                    if(checkValue(scale3[i]) && scale3[i].equals(str[i]))
                //  }

            }
        }
        //set value chem no scale
        for (int i=0;i<list3.size();i++){
            list_tv[pos].setText(list3.get(i).toString());
            list_tv[pos].setVisibility(View.VISIBLE);

            list_stv[pos].setText(wei3.get(i).toString());
            list_stv[pos].setVisibility(View.VISIBLE);
            pos=pos+1;
        }

        //set value chem in scale1
        pos=13;
        for (int i=0;i<list1.size();i++){
            list_tv[pos].setText(list1.get(i).toString());
            list_tv[pos].setVisibility(View.VISIBLE);

            list_stv[pos].setText(wei1.get(i).toString());
            list_stv[pos].setVisibility(View.VISIBLE);
            pos=pos+1;
            sum1=sum1+Integer.parseInt(wei1.get(i).toString());
//            System.out.println(i+" 1- "+sum1);
        }
        tv_sum1.setText(String.valueOf(sum1));
        //set value chem in scale 2
        pos=26;
        for (int i=26;i<list2.size();i++){
//            list_tv[pos].setText(list2.get(i).toString());
            list_tv[pos].setVisibility(View.VISIBLE);
//            list_stv[pos].setText(wei2.get(i).toString());
            list_stv[pos].setVisibility(View.VISIBLE);
            pos=pos+1;
            sum2=sum2+Integer.parseInt(wei2.get(i).toString());
        }
        tv_sum2.setText(String.valueOf(sum2));


    }

    public static void setValueScale1(TextView[] list_tv,String[] str,TextView[] list_stv,String[] wei){
        int pos=0,sum1=0;
        ArrayList<String> list1 = new ArrayList<String>(); //list scale 1

        ArrayList<String> wei1 = new ArrayList<>(); //list scale 1

        //set value raw
        for (int i =0;i<list_tv.length-30;i++){
            if (checkValue(str[i])){
                list_tv[pos].setText(str[pos]);
                list_tv[pos].setVisibility(View.VISIBLE);

                list_stv[pos].setText(wei[pos]);
                list_stv[pos].setVisibility(View.VISIBLE);
                pos=pos+1;
            }else{
                list_tv[pos].setVisibility(View.GONE);
            }
        }
        for (int i =0;i<list_tv.length;i++){
            if (checkValue(str[i])){
                if (checkValue(scale1[i]) && scale1[i].equals(str[i])) {
                    list1.add(str[i]);
                    wei1.add(wei[i]);

                }

            }
        }




        //set value chem in scale1
        pos=13;
        for (int i=0;i<list1.size();i++){
            list_tv[pos].setText(list1.get(i).toString());
            list_tv[pos].setVisibility(View.VISIBLE);

            list_stv[pos].setText(wei1.get(i).toString());
            list_stv[pos].setVisibility(View.VISIBLE);
            pos=pos+1;
            sum1=sum1+Integer.parseInt(wei1.get(i).toString());
//            System.out.println(i+" 1- "+sum1);
        }
        tv_sum1.setText(String.valueOf(sum1));


    }


    public static void setValueScale2(TextView[] list_tv,String[] str,TextView[] list_stv,String[] wei){
        int pos=0,sum2=0;
        ArrayList<String> list2 = new ArrayList<>();//list scale 1

        ArrayList<String> wei2 = new ArrayList<String>();//list scale 1
        //set value raw
        for (int i =0;i<list_tv.length-30;i++){
            if (checkValue(str[i])){
                list_tv[pos].setText(str[pos]);
                list_tv[pos].setVisibility(View.VISIBLE);

                list_stv[pos].setText(wei[pos]);
                list_stv[pos].setVisibility(View.VISIBLE);
                pos=pos+1;
            }else{
                list_tv[pos].setVisibility(View.GONE);
            }
        }
        for (int i =0;i<list_tv.length;i++){
            if (checkValue(str[i])){

                if(checkValue(scale2[i]) && scale2[i].equals(str[i])) {
                    list2.add(str[i]);
                    wei2.add(wei[i]);

                }


            }
        }


        //set value chem no scale

        pos=26;
        for (int i=0;i<list2.size();i++){
            list_tv[pos].setText(list2.get(i).toString());
            list_tv[pos].setVisibility(View.VISIBLE);
            list_stv[pos].setText(wei2.get(i).toString());
            list_stv[pos].setVisibility(View.VISIBLE);
            pos=pos+1;
            sum2=sum2+Integer.parseInt(wei2.get(i).toString());
//            System.out.println(i+" 2- "+sum2);
        }
        tv_sum2.setText(String.valueOf(sum2));

    }





    public static boolean checkScale(String[] scale, String val){
        for (String str : scale){
            if (str.toUpperCase().equals(val.toUpperCase())){
                return true;
            }
        }
        return false;
    }





    public static void setValueScaleHand(TextView[] list_tv,String[] str,TextView[] list_stv,String[] wei){
        int pos=0,sum1=0,sum2=0;

        ArrayList<String> list3 = new ArrayList<>();//list no scale


        ArrayList<String> wei3 = new ArrayList<String>();//list no scale
        //set value raw
        for (int i =0;i<list_tv.length-30;i++){
            if (checkValue(str[i])){
                list_tv[pos].setText(str[pos]);
                list_tv[pos].setVisibility(View.VISIBLE);

                list_stv[pos].setText(wei[pos]);
                list_stv[pos].setVisibility(View.VISIBLE);
                pos=pos+1;
            }else{
                list_tv[pos].setVisibility(View.GONE);
            }
        }
        for (int i =0;i<list_tv.length;i++){
            if (checkValue(str[i])){
                if (checkValue(scale3[i]) && scale3[i].equals(str[i])) {
                    list3.add(str[i]);
                    wei3.add(wei[i]);

                }

            }
        }


        //set value chem no scale
        for (int i=0;i<list3.size();i++){
            list_tv[pos].setText(list3.get(i).toString());
            list_tv[pos].setVisibility(View.VISIBLE);

            list_stv[pos].setText(wei3.get(i).toString());
            list_stv[pos].setVisibility(View.VISIBLE);
            pos=pos+1;
        }





    }

    public static void createExcel(Workbook workbook, ArrayList<RP_LIST_BATCH> listBTC,String[] name, String [] weight) throws FileNotFoundException {
        if (list_vl.length>0 && list_wvl.length>0){

            try{
                Sheet sheet = workbook.getSheetAt(0);
                Cell cell;
                Row row;
                int rownum=6,col=0;
                CellRangeAddress mergedCell;

                CellStyle style = createStyleForTitle(workbook);
                CellStyle styletitle =  createStyleBold(workbook);

                mergedCell = new CellRangeAddress(0,0,0,3+mtr_num+chem_num+pig_num+2);
                sheet.addMergedRegion(mergedCell);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                row = sheet.getRow(0);
                // Creates the cell
                cell = CellUtil.getCell(row,0);

                // Sets the allignment to the created cell
                CellUtil.setAlignment(cell, workbook, CellStyle.ALIGN_CENTER);

                row = sheet.getRow(1);
                //set value standard
                cell = row.createCell(1);
                cell.setCellValue(MCS);
                cell.setCellStyle(style);

                //set shift
//                if (mtr_num>7){
//                    col=4+mtr_num;
//                }else{
//                    col=11;
//                }
                col=3+mtr_num;
                mergedCell = new CellRangeAddress(1,1,4,col);
                sheet.addMergedRegion(mergedCell);
                cell = CellUtil.getCell(row,4);
                cell.setCellValue(pdfActivity.shift);
                cell.setCellStyle(style);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                //set title date
                mergedCell = new CellRangeAddress(1,1,col+1,col+2);
                sheet.addMergedRegion(mergedCell);
                cell = row.createCell(col+1);
                cell.setCellValue("日期/Ngày");
                cell.setCellStyle(styletitle);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                cell = row.createCell(col+3);
                cell.setCellValue(pdfActivity.date);
                cell.setCellStyle(style);
                mergedCell = new CellRangeAddress(1,1,col+3,col+2+chem_num+pig_num);
                sheet.addMergedRegion(mergedCell);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);


                //set title Raw mtr/Chemical/Pigment
                row = sheet.getRow(2);
                cell = row.createCell(4);
                cell.setCellValue("主料(g) 藥品(g)/LIỆU CHỦ(g) HÓA CHẤT(g)");
                cell.setCellStyle(styletitle);
                mergedCell = new CellRangeAddress(2,2,4,col);
                sheet.addMergedRegion(mergedCell);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                cell = row.createCell(col+1);
                cell.setCellValue("藥品(g) 色粒(g)/HÓA CHẤT(g) LIỆU MÀU(g)");
                cell.setCellStyle(styletitle);
//                int last_col=0;
//                if (chem_num>9){
//                    last_col=last_col+col+chem_num;
//                }else last_col=last_col+col+9;
//
//                if (pig_num>9){
//                    last_col=last_col+pig_num;
//                }else last_col=last_col+9;

                mergedCell = new CellRangeAddress(2,2,col+1,col+chem_num+pig_num+2);
                sheet.addMergedRegion(mergedCell);

                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

//                cell = row.createCell(col);
//                cell.setCellValue("色粒(g) LIỆU MÀU(g)");
//                cell.setCellStyle(styletitle);
//                if (pig_num>6){
//                    mergedCell = new CellRangeAddress(2,2,col,col+pig_num-1);
//                    sheet.addMergedRegion(mergedCell);
//                }else{
//                    mergedCell = new CellRangeAddress(2,2,col,col+5);
//                    sheet.addMergedRegion(mergedCell);
//                }
//                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
//                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
//                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
//                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                //set title scale
                row = sheet.getRow(3);
                cell = row.createCell(4);
                cell.setCellValue("手工承重 CÂN TAY");
                cell.setCellStyle(styletitle);
                mergedCell = new CellRangeAddress(3,3,4,col);
                sheet.addMergedRegion(mergedCell);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                cell = row.createCell(col+1);
                cell.setCellValue("一號機承重 CÂN MÁY 1");
                cell.setCellStyle(styletitle);
//                if (chem_num>9){
////
////                    col=col+chem_num;
////                }else {
////                    mergedCell = new CellRangeAddress(3,3,col,col+8);
////                    sheet.addMergedRegion(mergedCell);
////                    col=col+9;
////                }
                mergedCell = new CellRangeAddress(3,3,col+1,col+chem_num+1);
                sheet.addMergedRegion(mergedCell);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                cell = row.createCell(col+chem_num+2);
                cell.setCellValue("二號機承重 CÂN MÁY 2");
                cell.setCellStyle(styletitle);
//                if (pig_num>9){
//                    mergedCell = new CellRangeAddress(3,3,col+1,col+pig_num+1);
//                    sheet.addMergedRegion(mergedCell);
//                }else {
//                    mergedCell = new CellRangeAddress(3,3,col+1,col+9);
//                    sheet.addMergedRegion(mergedCell);
//                }
                mergedCell = new CellRangeAddress(3,3,col+chem_num+2,col+chem_num+pig_num+2);
                sheet.addMergedRegion(mergedCell);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                //set value Raw mtr/Chemical/Pigment
                int colnum=4;
                ArrayList<String> lst1 = new ArrayList<>();
                ArrayList<String> lst2 = new ArrayList<>();
                ArrayList<String> lst3 = new ArrayList<>();

                ArrayList<String> weight1 = new ArrayList<>();
                ArrayList<String> weight2 = new ArrayList<>();
                ArrayList<String> weight3 = new ArrayList<>();
                int sum1=0,sum2=0;

//                for(int i=0;i<name.length-30;i++){
//                    if (checkValue(name[i])){
//                        lst3.add(name[i]);
//                        weight3.add(weight[i]);
//                    }
//                }

//                for (int i=0;i<name.size();i++){
//                    if (checkValue(name.get(i))){
////                        if (checkScale(scale1,name[i])){
//                        if (checkValue(scale1[i]) && scale1[i].equals(name.get(i))){
//
//                            lst1.add(name.get(i));
//                            weight1.add(weight.get(i));
////                        }else if (checkScale(scale2,name[i])){
//                        }else if (checkValue(scale2[i]) && scale2[i].equals(name.get(i))){
//                            lst2.add(name.get(i));
//                            weight2.add(weight.get(i));
//                        }else{
//                            lst3.add(name.get(i));
//                            weight3.add(weight.get(i));
//
//                        }
//                    }
//                }
//

                for(int i=0;i<scale1.length;i++) {
                    if (scale1[i] != null) {
                        lst1.add(scale1[i]);
//                         weight1.add(weight[i]);                     }
                        weight1.add(wscale1[i]);
                    }
                }

                // weight2 không lấy được dữ liệu cânnnawgj
                for(int i=0; i<scale2.length;i++){
                    if(scale2[i]!= null){
                        lst2.add(scale2[i]);
//                         weight2.add(weight[i]);
                        weight2.add(wscale2[i]);
                    }
                }
                for(int i=0; i<scale3.length;i++){
                    if(scale3[i]!=null){
                        lst3.add(scale3[i]);
//                        weight3.add(weight[i]);
                        weight3.add(wscale3[i]);
                    }

                }

                for (int i=0;i<lst3.size();i++){
                    row = sheet.getRow(4);
                    cell = row.createCell(colnum);
                    cell.setCellValue(lst3.get(i));
                    cell.setCellStyle(style);

                    row = sheet.getRow(5);
                    cell = row.createCell(colnum);
                    cell.setCellValue(weight3.get(i));
                    cell.setCellStyle(style);
                    colnum++;
//                    if (lst3.size()<=7){
//                        row = sheet.getRow(4);
//                        cell = row.createCell(colnum);
//                        cell.setCellValue(lst3.get(i));
//                        cell.setCellStyle(style);
//
//                        row = sheet.getRow(5);
//                        cell = row.createCell(colnum);
//                        cell.setCellValue(weight3.get(i));
//                        cell.setCellStyle(style);
//                        colnum++;
//                        if (i==6){
//                            colnum=11;
//                            break;
//                        }
//                    }else{
//                        if (lst3.get(i)!=null){
//                            row = sheet.getRow(4);
//                            cell = row.createCell(colnum);
//                            cell.setCellValue(lst3.get(i));
//                            cell.setCellStyle(style);
//
//                            row = sheet.getRow(5);
//                            cell = row.createCell(colnum);
//                            cell.setCellValue(weight3.get(i));
//                            cell.setCellStyle(style);
//                            colnum++;
//
//                        }
//                    }
                }


                for (int i=0;i<lst1.size();i++){
                    row = sheet.getRow(4);
                    cell = row.createCell(colnum);
                    cell.setCellValue(lst1.get(i));
                    cell.setCellStyle(style);

                    row = sheet.getRow(5);
                    cell = row.createCell(colnum);
                    cell.setCellValue(weight1.get(i));
                    cell.setCellStyle(style);
                    colnum++;
                    sum1=sum1+Integer.parseInt(weight1.get(i));
//                    if (lst1.size()<=9){
//                        row = sheet.getRow(4);
//                        cell = row.createCell(colnum);
//                        cell.setCellValue(lst1.get(i));
//                        cell.setCellStyle(style);
//
//                        row = sheet.getRow(5);
//                        cell = row.createCell(colnum);
//                        cell.setCellValue(weight1.get(i));
//                        cell.setCellStyle(style);
//                        colnum++;
//                        sum1=sum1+Integer.parseInt(weight1.get(i));
//                        if (i==8){
//                            colnum=21;
//                            break;
//                        }
//                    }else{
//                        if (lst1.get(i)!=null){
//                            row = sheet.getRow(4);
//                            cell = row.createCell(colnum);
//                            cell.setCellValue(lst1.get(i));
//                            cell.setCellStyle(style);
//
//                            row = sheet.getRow(5);
//                            cell = row.createCell(colnum);
//                            cell.setCellValue(weight1.get(i));
//                            cell.setCellStyle(style);
//                            colnum++;
//                            sum1=sum1+Integer.parseInt(weight1.get(i));
//                        }
//                    }
                }
                //set sum 1
                row = sheet.getRow(4);
                cell = row.createCell(colnum);
                cell.setCellValue("TỔNG");
                cell.setCellStyle(style);

                row = sheet.getRow(5);
                cell = row.createCell(colnum);
                cell.setCellValue(sum1);
                cell.setCellStyle(style);
                colnum++;

                for (int i=0;i<lst2.size();i++){
                    row = sheet.getRow(4);
                    cell = row.createCell(colnum);
                    cell.setCellValue(lst2.get(i));
                    cell.setCellStyle(style);

                    row = sheet.getRow(5);
                    cell = row.createCell(colnum);
                    cell.setCellValue(weight2.get(i));
                    cell.setCellStyle(style);
                    colnum++;
                    sum2=sum2+Integer.parseInt(weight2.get(i));
//                    if (lst2.size()<=9){
//                        row = sheet.getRow(4);
//                        cell = row.createCell(colnum);
//                        cell.setCellValue(lst2.get(i));
//                        cell.setCellStyle(style);
//
//                        row = sheet.getRow(5);
//                        cell = row.createCell(colnum);
//                        cell.setCellValue(weight2.get(i));
//                        cell.setCellStyle(style);
//                        colnum++;
//                        sum2=sum2+Integer.parseInt(weight2.get(i));
//                        if (i==8){
//                            break;
//                        }
//                    }else {
//                        if (lst2.get(i)!=null){
//                            row = sheet.getRow(4);
//                            cell = row.createCell(colnum);
//                            cell.setCellValue(lst2.get(i));
//                            cell.setCellStyle(style);
//
//                            row = sheet.getRow(5);
//                            cell = row.createCell(colnum);
//                            cell.setCellValue(weight2.get(i));
//                            cell.setCellStyle(style);
//                            colnum++;
//                            sum2=sum2+Integer.parseInt(weight2.get(i));
//                        }
//                    }

                }
                //set sum 1
                row = sheet.getRow(4);
                cell = row.createCell(colnum);
                cell.setCellValue("TỔNG");
                cell.setCellStyle(style);

                row = sheet.getRow(5);
                cell = row.createCell(colnum);
                cell.setCellValue(sum2);
                cell.setCellStyle(style);

                //set list IP-Batch

                String[] note;
                ArrayList<String> note1;
                ArrayList<String> note2;
                ArrayList<String> note3;
                for (int i = 0;i<listBTC.size();i++){
                    note1 = new ArrayList<>();
                    note2 = new ArrayList<>();
                    note3 = new ArrayList<>();
                    int column=4;
                    row = sheet.createRow(rownum);
                    cell = row.createCell(0);
                    cell.setCellValue(listBTC.get(i).getBTC_BATCH_NO());
                    cell.setCellStyle(style);

                    cell = row.createCell(1);
                    cell.setCellValue(listBTC.get(i).getSTYLE());
                    cell.setCellStyle(style);

                    cell = row.createCell(2);
                    cell.setCellValue(listBTC.get(i).getCOLOR());
                    cell.setCellStyle(style);

                    cell = row.createCell(3);
                    cell.setCellValue(listBTC.get(i).getHARD());
                    cell.setCellStyle(style);

                    note = new String[]{listBTC.get(i).getNOTE_MTR1(),listBTC.get(i).getNOTE_MTR2(),listBTC.get(i).getNOTE_MTR3(),
                            listBTC.get(i).getNOTE_MTR4(),listBTC.get(i).getNOTE_MTR5(),listBTC.get(i).getNOTE_MTR6(),
                            listBTC.get(i).getNOTE_MTR7(),listBTC.get(i).getNOTE_MTR8(),listBTC.get(i).getNOTE_MTR9(),
                            listBTC.get(i).getNOTE_MTR10(),listBTC.get(i).getNOTE_CHEM1(),listBTC.get(i).getNOTE_CHEM2(),
                            listBTC.get(i).getNOTE_CHEM3(),listBTC.get(i).getNOTE_CHEM4(),listBTC.get(i).getNOTE_CHEM5(),
                            listBTC.get(i).getNOTE_CHEM6(),listBTC.get(i).getNOTE_CHEM7(),listBTC.get(i).getNOTE_CHEM8(),
                            listBTC.get(i).getNOTE_CHEM9(),listBTC.get(i).getNOTE_CHEM10(),listBTC.get(i).getNOTE_CHEM11(),
                            listBTC.get(i).getNOTE_CHEM12(),listBTC.get(i).getNOTE_CHEM13(),listBTC.get(i).getNOTE_CHEM14(),
                            listBTC.get(i).getNOTE_CHEM15(),listBTC.get(i).getNOTE_CHEM16(),listBTC.get(i).getNOTE_CHEM17(),
                            listBTC.get(i).getNOTE_CHEM18(),listBTC.get(i).getNOTE_CHEM19(),listBTC.get(i).getNOTE_CHEM20(),
                            listBTC.get(i).getNOTE_PIG1(),listBTC.get(i).getNOTE_PIG2(),listBTC.get(i).getNOTE_PIG3(),
                            listBTC.get(i).getNOTE_PIG4(),listBTC.get(i).getNOTE_PIG5(),listBTC.get(i).getNOTE_PIG6(),
                            listBTC.get(i).getNOTE_PIG7(),listBTC.get(i).getNOTE_PIG8(),listBTC.get(i).getNOTE_PIG9(),
                            listBTC.get(i).getNOTE_PIG10()};

//                    for(int j=0;j<name.length-30;j++){
//                        if (checkValue(name[j])){
//                            note3.add(note[j]);
//                            System.out.println("no scale--- " + note[j]);
//                        }
//                    }


                    //test 3 mac
//                    for (int j=0;j<name.length;j++){
//                        if (checkValue(name[j])){
////                            if (checkScale(scale1,name[j])){
//                            if(checkValue(scale1[j]) && scale1[j].equals(name[j])){
//                                note1.add(note[j]);
//                                System.out.println(" scale 1--- " + note[j]);
////                            }else if (checkScale(scale2,name[j])){
//                            }else if (checkValue(scale2[j]) && scale2[j].equals(name[j])){
//                                note2.add(note[j]);
//                                System.out.println(" scale 2--- " + note[j]);
//
//                            }else{
//                                note3.add(note[j]);
//                                System.out.println("no scale--- " + note[j]);
//                            }
//                        }
//                    }

                    for(int j =0;j<scale1.length;j++){
                        if (checkValue(scale1[j])){
                            if(checkValue(scale1[j]) && scale1[j].equals(scale1[j])){
                                note1.add(note[j]);
                            }
                        }
                    }

                    for(int j =0;j<scale2.length;j++){
                        if (checkValue(scale2[j])){
                            if(checkValue(scale2[j]) && scale2[j].equals(scale2[j])){
                                note2.add(note[j]);
                            }
                        }
                    }

                    for(int j =0;j<scale3.length;j++){
                        if (checkValue(scale3[j])){
                            if(checkValue(scale3[j]) && scale3[j].equals(scale3[j])){
                                note3.add(note[j]);
                            }
                        }
                    }


                    for (int j=0;j<lst3.size();j++){
                        cell = row.createCell(column);
                        cell.setCellValue(note3.get(j) == null ? "V" : note3.get(j).toString());
                        cell.setCellStyle(style);
                        column++;

//                        if (lst3.size()<=7){
//                            if (lst3.get(j)!=null){
//                                cell.setCellValue(note3.get(j).toString() == null ? "V" : note3.get(j).toString());
//                                cell.setCellStyle(style);
//                                column++;
//                            }else {
//                                cell.setCellValue("");
//                                cell.setCellStyle(style);
//                                column++;
//                            }
//                            if (mtr_num<=7 && j==6){
//                                column=8;
//                                break;
//                            }
//
//                        }else{
//                            if (lst3.get(j)!=null){
//                                cell.setCellValue(note3.get(j).toString() == null ? "V" : note3.get(j).toString());
//                                cell.setCellStyle(style);
//                                column++;
//                            }
//                        }



                    }

                    for (int j=0;j<lst1.size();j++){
                        cell = row.createCell(column);
                        cell.setCellValue(note1.get(j) == null ? "V" : note1.get(j).toString());
                        cell.setCellStyle(style);
                        column++;
//                        if (lst1.size()<=9){
//                            if (lst1.get(j)!=null){
//                                cell.setCellValue(note1.get(j) == null ? "V" : note1.get(j).toString());
//                                cell.setCellStyle(style);
//                                column++;
//                            }else {
//                                cell.setCellValue("");
//                                cell.setCellStyle(style);
//                                column++;
//                            }
//                            if (j==8){
//                                column=20;
//                                break;
//                            }
//                        }else{
//                            if (lst1.get(j)!=null){
//                                cell.setCellValue(note1.get(j).toString() == null ? "V" : note1.get(j).toString());
//                                cell.setCellStyle(style);
//                                column++;
//                            }
//                        }
                    }

                    cell = row.createCell(column);
                    cell.setCellValue("V");
                    cell.setCellStyle(style);
                    column++;

                    for (int j=0;j<lst2.size();j++){
                        cell = row.createCell(column);
                        cell.setCellValue(note2.get(j)== null ? "V" : note2.get(j).toString());
                        cell.setCellStyle(style);
                        column++;
//                        if(lst2.size()<=9){
//                            if (lst2.get(j)!=null){
//                                cell.setCellValue(note2.get(j)== null ? "V" : note2.get(j).toString());
//                                cell.setCellStyle(style);
//                                column++;
//                            }else {
//                                cell.setCellValue("");
//                                cell.setCellStyle(style);
//                                column++;
//                            }
//                            if (j==8){
//                                column=30;
//                                break;
//                            }
//                        }else{
//                            if (lst2.get(j)!=null){
//                                cell.setCellValue(note2.get(j).toString() == null ? "V" : note2.get(j).toString());
//                                cell.setCellStyle(style);
//                                column++;
//                            }
//                        }

                    }

                    cell = row.createCell(column);
                    cell.setCellValue("V");
                    cell.setCellStyle(style);
                    rownum++;
                }

                System.out.println("Created sheet IP-Batch " );

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static CellStyle createStyleForTitle(Workbook workbook) {

        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        // Font Color
        font.setColor(IndexedColors.BLACK.getIndex());
        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setBorderLeft((short)1);
        style.setBorderRight((short)1);
        style.setBorderTop((short)1);
        style.setBorderBottom((short)1);

        return style;
    }

    public static CellStyle createStyleBold(Workbook workbook) {

        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setFontName("Times New Roman");
        // Font Color
        font.setColor(IndexedColors.BLACK.getIndex());
        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setBorderLeft((short)1);
        style.setBorderRight((short)1);
        style.setBorderTop((short)1);
        style.setBorderBottom((short)1);
        style.setWrapText(true);

        return style;
    }

    private void findbyID(){

        tv_mcs = getActivity().findViewById(R.id.tv_mcs);
        tv_shift = getActivity().findViewById(R.id.tv_shift);
        tv_date = getActivity().findViewById(R.id.tv_date);

        tv_sum1 = getActivity().findViewById(R.id.tv_sum1);
        tv_sum2 = getActivity().findViewById(R.id.tv_sum2);

        tv_1 = getActivity().findViewById(R.id.tv_1);
        tv_2 = getActivity().findViewById(R.id.tv_2);
        tv_3 = getActivity().findViewById(R.id.tv_3);
        tv_4 = getActivity().findViewById(R.id.tv_4);
        tv_5 = getActivity().findViewById(R.id.tv_5);
        tv_6 = getActivity().findViewById(R.id.tv_6);
        tv_7 = getActivity().findViewById(R.id.tv_7);
        tv_8 = getActivity().findViewById(R.id.tv_8);
        tv_9 = getActivity().findViewById(R.id.tv_9);
        tv_10 = getActivity().findViewById(R.id.tv_10);

        tv_11 = getActivity().findViewById(R.id.tv_11);
        tv_12 = getActivity().findViewById(R.id.tv_12);
        tv_13 = getActivity().findViewById(R.id.tv_13);
        tv_14 = getActivity().findViewById(R.id.tv_14);
        tv_15 = getActivity().findViewById(R.id.tv_15);
        tv_16 = getActivity().findViewById(R.id.tv_16);
        tv_17 = getActivity().findViewById(R.id.tv_17);
        tv_18 = getActivity().findViewById(R.id.tv_18);
        tv_19 = getActivity().findViewById(R.id.tv_19);
        tv_20 = getActivity().findViewById(R.id.tv_20);
        tv_21 = getActivity().findViewById(R.id.tv_21);
        tv_22 = getActivity().findViewById(R.id.tv_22);
        tv_23 = getActivity().findViewById(R.id.tv_23);
        tv_24 = getActivity().findViewById(R.id.tv_24);
        tv_25 = getActivity().findViewById(R.id.tv_25);
        tv_26 = getActivity().findViewById(R.id.tv_26);
        tv_27 = getActivity().findViewById(R.id.tv_27);
        tv_28 = getActivity().findViewById(R.id.tv_28);
        tv_29 = getActivity().findViewById(R.id.tv_29);
        tv_30 = getActivity().findViewById(R.id.tv_30);

        tv_31 = getActivity().findViewById(R.id.tv_31);
        tv_32 = getActivity().findViewById(R.id.tv_32);
        tv_33 = getActivity().findViewById(R.id.tv_33);
        tv_34 = getActivity().findViewById(R.id.tv_34);
        tv_35 = getActivity().findViewById(R.id.tv_35);
        tv_36 = getActivity().findViewById(R.id.tv_36);
        tv_37 = getActivity().findViewById(R.id.tv_37);
        tv_38 = getActivity().findViewById(R.id.tv_38);
        tv_39 = getActivity().findViewById(R.id.tv_39);
        tv_40 = getActivity().findViewById(R.id.tv_40);

        tv_s1 = getActivity().findViewById(R.id.tv_s1);
        tv_s2 = getActivity().findViewById(R.id.tv_s2);
        tv_s3 = getActivity().findViewById(R.id.tv_s3);
        tv_s4 = getActivity().findViewById(R.id.tv_s4);
        tv_s5 = getActivity().findViewById(R.id.tv_s5);
        tv_s6 = getActivity().findViewById(R.id.tv_s6);
        tv_s7 = getActivity().findViewById(R.id.tv_s7);
        tv_s8 = getActivity().findViewById(R.id.tv_s8);
        tv_s9 = getActivity().findViewById(R.id.tv_s9);
        tv_s10 = getActivity().findViewById(R.id.tv_s10);

        tv_s11 = getActivity().findViewById(R.id.tv_s11);
        tv_s12 = getActivity().findViewById(R.id.tv_s12);
        tv_s13 = getActivity().findViewById(R.id.tv_s13);
        tv_s14 = getActivity().findViewById(R.id.tv_s14);
        tv_s15 = getActivity().findViewById(R.id.tv_s15);
        tv_s16 = getActivity().findViewById(R.id.tv_s16);
        tv_s17 = getActivity().findViewById(R.id.tv_s17);
        tv_s18 = getActivity().findViewById(R.id.tv_s18);
        tv_s19 = getActivity().findViewById(R.id.tv_s19);
        tv_s20 = getActivity().findViewById(R.id.tv_s20);
        tv_s21 = getActivity().findViewById(R.id.tv_s21);
        tv_s22 = getActivity().findViewById(R.id.tv_s22);
        tv_s23 = getActivity().findViewById(R.id.tv_s23);
        tv_s24 = getActivity().findViewById(R.id.tv_s24);
        tv_s25 = getActivity().findViewById(R.id.tv_s25);
        tv_s26 = getActivity().findViewById(R.id.tv_s26);
        tv_s27 = getActivity().findViewById(R.id.tv_s27);
        tv_s28 = getActivity().findViewById(R.id.tv_s28);
        tv_s29 = getActivity().findViewById(R.id.tv_s29);
        tv_s30 = getActivity().findViewById(R.id.tv_s30);

        tv_s31 = getActivity().findViewById(R.id.tv_s31);
        tv_s32 = getActivity().findViewById(R.id.tv_s32);
        tv_s33 = getActivity().findViewById(R.id.tv_s33);
        tv_s34 = getActivity().findViewById(R.id.tv_s34);
        tv_s35 = getActivity().findViewById(R.id.tv_s35);
        tv_s36 = getActivity().findViewById(R.id.tv_s36);
        tv_s37 = getActivity().findViewById(R.id.tv_s37);
        tv_s38 = getActivity().findViewById(R.id.tv_s38);
        tv_s39 = getActivity().findViewById(R.id.tv_s39);
        tv_s40 = getActivity().findViewById(R.id.tv_s40);

        rv_ipbatch =getActivity().findViewById(R.id.rv_ipbatch);
    }

    public class  RP_MACArm extends ApiReturnModel<ArrayList<RP_MAC>> {}

    public class  RP_BATCHArm extends ApiReturnModel<ArrayList<RP_LIST_BATCH>> {}
    public class  QSM1 extends ApiReturnModel<ArrayList<QAM_SCALE_MAC1>> {}
    public class  QSM2 extends ApiReturnModel<ArrayList<QAM_SCALE_MAC2>> {}
    public class  QSM3 extends ApiReturnModel<ArrayList<QAM_SCALE_HAND>> {}




}
