package com.deanshoes.AppQAM.fragment;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.reportadapter.ListKneaderAdapter;
import com.deanshoes.AppQAM.model.QAM.BTC;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step4.ReportActivity;
import com.deanshoes.AppQAM.step4.pdfActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class KneaderFragment extends Fragment {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    TextView tv_shift,tv_date;
    TextView tv_temp, tv_time,tv_temp1,tv_time1,tv_temp2,tv_time2,tv_temp3,tv_time3,tv_temp4,
            tv_time4,tv_tempout,tv_timeout, tv_note;

    ListView rv_kneader;
    String factory="";
    public static String date="", shift ="", ins_no = "",MCS="";

    public static ArrayList<BTC> BTC = new ArrayList<>();
    public static String[] STD_170FS_min, STD_XL4MB_min, STD_ALL_min;
    public static String[] TIME_170FS, TIME_XL4MB, TIME_ALL;
    String FILE_NAME="";



    public static KneaderFragment newInstance(String param1) {
        KneaderFragment fragment = new KneaderFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_kneader, container, false);
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        STD_170FS_min = new String[]{"75-85","95-105","105-115","115-125","125-130","122-126"};
        STD_XL4MB_min = new String[]{"75-85","100-110","110-120","115-125","125-135","130-135"};
        STD_ALL_min = new String[]{"75-85","95-110","105-115","110-120","115-125","110-120"};

        TIME_170FS = new String[]{"5'","3'","3'","3'"};
        TIME_XL4MB = new String[]{"6'30'","2'40","2'40'","2'40"};
        TIME_ALL = new String[]{"5'","2'","1'30","1'30"};

        Intent intent = getActivity().getIntent();
        final Bundle bundle=intent.getExtras();
        bundle.putString("fac",ReportActivity.factory);
        bundle.putString("user","QAM");

        findbyid();

        factory= pdfActivity.factory;

        MCS = pdfActivity.MCS;

        ins_no = pdfActivity.ins_no;
        shift = pdfActivity.shift;
        date = pdfActivity.date;

        //

            FILE_NAME = ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
                    +"_" + shift ;


        CheckType(MCS);
        tv_shift.setText(shift);
        tv_date.setText(date);

        getList_Kneader();

    }

    public void getList_Kneader(){
        try{
            CallApi.getListKneader(factory, ins_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    BTC = new ArrayList<>();
                    RP_BTCArm rp_btcArm = gson.fromJson(response.toString(), RP_BTCArm.class);
                    BTC = rp_btcArm.getItems() == null ? new ArrayList<BTC>() : rp_btcArm.getItems();
                    ListKneaderAdapter listKneaderAdapter = new ListKneaderAdapter(getActivity(),R.layout.rp_kneader_adapter, BTC);
                    rv_kneader.setAdapter(listKneaderAdapter);


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public static void createExcel(Workbook workbook, ArrayList<BTC> listBTC) throws FileNotFoundException {
        String[] STD, TIME;

        try{
//            File ex = new File(Environment.getExternalStorageDirectory().toString() +
//                    FILE_NAME + ".xls");
//            if (!ex.exists()){
//                inputStream = am.open("excel_qam.xls");
//                workbook =getWorkbook(inputStream,"excel_qam.xls");
//            }else{
//                inputStream = new FileInputStream(ex);
//                workbook =getWorkbook(inputStream,FILE_NAME + ".xls");
//            }
            Sheet sheet = workbook.getSheetAt(1);
            Cell cell;
            Row row;
            int rownum=5;

            CellStyle style = createStyleForTitle(workbook);
            row = sheet.getRow(1);
            cell = row.getCell(1);
            cell.setCellValue(pdfActivity.shift);
            cell.setCellStyle(style);

            cell = row.getCell(11);
            cell.setCellValue(pdfActivity.date);
            cell.setCellStyle(style);


            if (MCS.contains("170FS")){
                STD=STD_170FS_min;
                TIME=TIME_170FS;
            }else if(MCS.contains("XL4MB")){
                STD=STD_XL4MB_min;
                TIME=TIME_XL4MB;
            }else {
                STD=STD_ALL_min;
                TIME=TIME_ALL;
            }
            row = sheet.getRow(4);
            //set value standard
            cell = row.createCell(4);
            cell.setCellValue(STD[0]);
            cell.setCellStyle(style);

            cell = row.createCell(6);
            cell.setCellValue(STD[1]);
            cell.setCellStyle(style);
            cell = row.createCell(7);
            cell.setCellValue(TIME[0]);
            cell.setCellStyle(style);

            cell = row.createCell(8);
            cell.setCellValue(STD[2]);
            cell.setCellStyle(style);
            cell = row.createCell(9);
            cell.setCellValue(TIME[1]);
            cell.setCellStyle(style);

            cell = row.createCell(10);
            cell.setCellValue(STD[3]);
            cell.setCellStyle(style);
            cell = row.createCell(11);
            cell.setCellValue(TIME[2]);
            cell.setCellStyle(style);

            cell = row.createCell(12);
            cell.setCellValue(STD[4]);
            cell.setCellStyle(style);
            cell = row.createCell(13);
            cell.setCellValue(TIME[3]);
            cell.setCellStyle(style);

            cell = row.createCell(14);
            cell.setCellValue(STD[5]);
            cell.setCellStyle(style);

            //set list kneader
            for (int i = 0;i<listBTC.size();i++){
                row = sheet.createRow(rownum);
                cell = row.createCell(0);
                cell.setCellValue(listBTC.get(i).getBTC_BATCH_NO());
                cell.setCellStyle(style);

                cell = row.createCell(1);
                cell.setCellValue(listBTC.get(i).getBTC_STYLE());
                cell.setCellStyle(style);

                cell = row.createCell(2);
                cell.setCellValue(listBTC.get(i).getBTC_COLOR());
                cell.setCellStyle(style);

                cell = row.createCell(3);
                cell.setCellValue(listBTC.get(i).getBTC_HARD());
                cell.setCellStyle(style);

                cell = row.createCell(4);
                cell.setCellValue(listBTC.get(i).getKN_TEMP_F_NOTE() == null ? listBTC.get(i).getKN_TEMP_F() : listBTC.get(i).getKN_TEMP_F_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(5);
                cell.setCellValue(listBTC.get(i).getKN_TIME_F());
                cell.setCellStyle(style);

                cell = row.createCell(6);
                cell.setCellValue(listBTC.get(i).getKN_TEMP_1_NOTE() == null ? listBTC.get(i).getKN_TEMP_1() : listBTC.get(i).getKN_TEMP_1_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(7);
                cell.setCellValue(listBTC.get(i).getKN_TIME_1());
                cell.setCellStyle(style);

                cell = row.createCell(8);
                cell.setCellValue(listBTC.get(i).getKN_TEMP_2_NOTE() == null ? listBTC.get(i).getKN_TEMP_2() : listBTC.get(i).getKN_TEMP_2_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(9);
                cell.setCellValue(listBTC.get(i).getKN_TIME_2());
                cell.setCellStyle(style);

                cell = row.createCell(10);
                cell.setCellValue(listBTC.get(i).getKN_TEMP_3_NOTE() == null ? listBTC.get(i).getKN_TEMP_3() : listBTC.get(i).getKN_TEMP_3_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(11);
                cell.setCellValue(listBTC.get(i).getKN_TIME_3());
                cell.setCellStyle(style);

                cell = row.createCell(12);
                cell.setCellValue(listBTC.get(i).getKN_TEMP_4_NOTE() == null ? listBTC.get(i).getKN_TEMP_4() : listBTC.get(i).getKN_TEMP_4_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(13);
                cell.setCellValue(listBTC.get(i).getKN_TIME_4());
                cell.setCellStyle(style);

                cell = row.createCell(14);
                cell.setCellValue(listBTC.get(i).getKN_TEMP_END_NOTE() == null ? listBTC.get(i).getKN_TEMP_END() : listBTC.get(i).getKN_TEMP_END_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(15);
                cell.setCellValue(listBTC.get(i).getKN_TIME_END());
                cell.setCellStyle(style);

                cell = row.createCell(16);
                cell.setCellValue(listBTC.get(i).getKN_NOTE());
                cell.setCellStyle(style);

                rownum++;
            }


//            File check = new File(Environment.getExternalStorageDirectory().toString() +
//                    "/QAM_EXCEL/" + FILE_NAME + ".xls");
//            if (check.exists()){
//                check.delete();
//            }
//            outFile = new FileOutputStream(check);
//            workbook.write(outFile);
//
//            System.out.println("Created file: " + check.getAbsolutePath());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            if (outFile != null) {
//                try {
//                    outFile.flush();
//                    outFile.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
        }
    }

    private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
        Workbook workbook = null;
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }

    private static CellStyle createStyleForTitle(Workbook workbook) {

        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        // Font Color
        font.setColor(IndexedColors.BLACK.getIndex());
        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setBorderLeft((short)1);
        style.setBorderRight((short)1);
        style.setBorderTop((short)1);
        style.setBorderBottom((short)1);
        style.setLocked(true);

        return style;
    }

    public void CheckType(String name){
        String[] STD, TIME;
        if (name.indexOf("170FS")!=-1){
            STD=STD_170FS_min;
            TIME=TIME_170FS;
        }else if(name.indexOf("XL4MB")!=-1){
            STD=STD_XL4MB_min;
            TIME=TIME_XL4MB;
        }else {
            STD=STD_ALL_min;
            TIME=TIME_ALL;
        }

        tv_temp.setText(STD[0]);

        tv_temp1.setText(STD[1]);
        tv_time1.setText(TIME[0]);
        tv_temp2.setText(STD[2]);
        tv_time2.setText(TIME[1]);
        tv_temp3.setText(STD[3]);
        tv_time3.setText(TIME[2]);
        tv_temp4.setText(STD[4]);
        tv_time4.setText(TIME[3]);

        tv_tempout.setText(STD[5]);

    }

    public void findbyid(){
        rv_kneader = getActivity().findViewById(R.id.rv_kneader);
        tv_shift = getActivity().findViewById(R.id.tv_shift_kn);
        tv_date = getActivity().findViewById(R.id.tv_date_kn);
        tv_temp = getActivity().findViewById(R.id.tv_temp);
        tv_time = getActivity().findViewById(R.id.tv_time);
        tv_temp1 = getActivity().findViewById(R.id.tv_temp1);
        tv_time1 = getActivity().findViewById(R.id.tv_time1);
        tv_temp2 = getActivity().findViewById(R.id.tv_temp2);
        tv_time2 = getActivity().findViewById(R.id.tv_time2);
        tv_temp3 = getActivity().findViewById(R.id.tv_temp3);
        tv_time3 = getActivity().findViewById(R.id.tv_time3);
        tv_temp4 = getActivity().findViewById(R.id.tv_temp4);
        tv_time4 = getActivity().findViewById(R.id.tv_time4);
        tv_tempout = getActivity().findViewById(R.id.tv_tempout);
        tv_timeout = getActivity().findViewById(R.id.tv_timeout);
        tv_note = getActivity().findViewById(R.id.tv_note);
    }

    public class  RP_BTCArm extends ApiReturnModel<ArrayList<BTC>> {}
}
