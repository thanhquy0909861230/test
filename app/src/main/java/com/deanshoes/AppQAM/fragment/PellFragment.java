package com.deanshoes.AppQAM.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.reportadapter.ListPellAdapter;
import com.deanshoes.AppQAM.adapter.rpadapter.BTCAdapter;
import com.deanshoes.AppQAM.model.QAM.BTC_PELL;
import com.deanshoes.AppQAM.model.QAM.MODEL_SEND_MAIL;
import com.deanshoes.AppQAM.model.report_model.RP_BTC;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.model.response.ToastCustom;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step2.fvi.Mac1Activity_FVI;
import com.deanshoes.AppQAM.step4.ReportActivity;
import com.deanshoes.AppQAM.step4.SendMail;
import com.deanshoes.AppQAM.step4.pdfActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.deanshoes.AppQAM.step4.pdfActivity.style;

public class PellFragment extends Fragment {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    TextView tv_shift,tv_date;
    ListView rv_pell;
    String factory="";
    private String ins_no = "",MCS="",date="",shift="",  TL = "", EXDATE="";
    //new
    private String STYLE = "", COLOR = "", ER="";
    String FILE_NAME="";
    ImageButton ib_logout,ib_sendmail;
    String Filename="";
    ArrayList<RP_BTC> arrBTC;
    BTCAdapter btcAdapter;
    ArrayList<BTC_PELL> BTC_PELL = new ArrayList<>();
    ArrayList<String> btc_no1ArrayList;

    public static PellFragment newInstance(String param1) {
        PellFragment fragment = new PellFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pell, container, false);
    }
    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        factory= pdfActivity.factory;
        MCS = pdfActivity.MCS;
        ER = pdfActivity.er;
        TL = pdfActivity.TL;
        EXDATE = pdfActivity.EXDATE;
        ins_no = pdfActivity.ins_no;
        date = pdfActivity.date;
        shift = pdfActivity.shift;
        //new
        STYLE = pdfActivity.style;
        COLOR = pdfActivity.color;

        Intent intent = getActivity().getIntent();
        final Bundle bundle=intent.getExtras();
        bundle.putString("fac", ReportActivity.factory);
        bundle.putString("user","QAM");

        FILE_NAME = ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
                +"_" + shift ;

        tv_shift = getActivity().findViewById(R.id.tv_shift_pell);
        tv_date = getActivity().findViewById(R.id.tv_date_pell);
        rv_pell = getActivity().findViewById(R.id.rv_pell);
        ib_sendmail = getActivity().findViewById(R.id.ib_sendmail);
        ib_logout = getActivity().findViewById(R.id.ib_logout);
        tv_shift.setText(shift);
        tv_date.setText(date);

        DeteleFileBefore7days();
        getList_PELL();
        getData(ins_no,shift,date);

        ib_sendmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendmail_dialog();
            }
        });
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                ib_sendmail.performClick();
//            }
//        }, 900000);

        ib_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), pdfActivity.class);
                myIntent.putExtras(bundle);
                startActivity(myIntent);
            }
        });

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                ib_logout.performClick();
//            }
//        }, 901000);
    }

    public void getList_PELL(){
        try{
            CallApi.getListPell(factory, ins_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    BTC_PELL = new ArrayList<>();
                    RP_BTCPELLArm rp_btcpellArm = gson.fromJson(response.toString(), RP_BTCPELLArm.class);
                    BTC_PELL = rp_btcpellArm.getItems() == null ? new ArrayList<BTC_PELL>() : rp_btcpellArm.getItems();
                    for (int i=0;i<BTC_PELL.size();i++){
                        BTC_PELL.get(i).setMCS(MCS);
                        Filename = BTC_PELL.get(0).getNEW_BATCH_NO();
                    }
                    ListPellAdapter listPellAdapter = new ListPellAdapter(getActivity(),R.layout.rp_pell_adapter,BTC_PELL);
                    rv_pell.setAdapter(listPellAdapter);
                    try {
                        createExcel(BTC_PELL);
 //                      getData(ins_no,shift,date);
 //                       createExcel_FVI_II(arrBTC);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getData(String ins_no, String shift, String date) {
//        arrBTC = new ArrayList<>();
        try {
            CallApi.getData(factory, ins_no, shift, date, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    pdfActivity.BtcArm btcArm = gson.fromJson(response.toString(), pdfActivity.BtcArm.class);
                    arrBTC = btcArm.getItems() == null ? new ArrayList<RP_BTC>() : btcArm.getItems();
                    btcAdapter = new BTCAdapter(getActivity(), R.layout.btc_adapter, arrBTC);
//                  lv_btc.setAdapter(btcAdapter);
                    btc_no1ArrayList = new ArrayList<>();
                    for (int i = 0; i < arrBTC.size(); i++) {
                        btc_no1ArrayList.add(arrBTC.get(i).getBTC_BATCH_NO());
                    }
                }
            },
                    new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("LOGCAT", "Exception", e);
        }
    }
//    public void getData(String ins_no, String shift, String date) {
//        try {
//            CallApi.getData(factory, ins_no, shift, date, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//
//                    pdfActivity.BtcArm btcArm = gson.fromJson(response.toString(), pdfActivity.BtcArm.class);
//                    arrBTC = btcArm.getItems() == null ? new ArrayList<RP_BTC>() : btcArm.getItems();
//                    btcAdapter = new BTCAdapter(getActivity(), R.layout.btc_adapter, arrBTC);
//                    lv_btc.setAdapter(btcAdapter);
//                    btc_no1ArrayList = new ArrayList<>();
//                    for (int i = 0; i < arrBTC.size(); i++) {
//                        btc_no1ArrayList.add(arrBTC.get(i).getBTC_BATCH_NO());
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    error.printStackTrace();
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e("LOGCAT", "Exception", e);
//        }
//    }

    MODEL_SEND_MAIL model;
    String[] ListFile;
    String DEST = Environment.getExternalStorageDirectory().toString() + "/QAM_EXCEL/";
    public void sendmail_dialog(){
        //for FVI-2
//        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
//        b.setTitle("Xác nhận");
//        b.setMessage("Bạn có muốn gửi báo biểu về Mail?");
//        b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
                //sendmail
                String name1 = DEST+ ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
                        +"_" + shift + ".xls" ;
//                String name2 = DEST+ ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
//                        +"_" + shift + ".xls";
//                String name3 = DEST+ ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
//                        +"_" + shift + ".xls";
//                String name4 = DEST+FILE_NAME + ".xls";
                model = new MODEL_SEND_MAIL();
                model.setMSC(MCS);
                model.setDATE(date);
                model.setSHIFT(shift);
                model.setER(pdfActivity.er);
                model.setTL(pdfActivity.TL);
                model.setEXDATE(pdfActivity.EXDATE);
                model.setSTYLE(pdfActivity.style);
                model.setCOLOR(pdfActivity.color);
                model.setINS_NO(ins_no);

                ListFile = new String[]{name1};
                SendMail sendmail = new SendMail();
                sendmail.sendmail_report(factory,ListFile,model,Filename);
                ToastCustom.message(getActivity(), "Gửi thành công!", Color.GREEN);
                //for FVI-2
     //       }
//        });
//        b.setNegativeButton("Không",  new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.cancel();
//            }
//        });
//        AlertDialog al = b.create();
//        al.show();
    }

    public void createExcel(ArrayList<BTC_PELL> listBTC) throws FileNotFoundException {
        AssetManager am = getActivity().getAssets();
        FileOutputStream outFile = null;
        InputStream inputStream = null;
        Workbook workbook = null;
        try{
            File ex = new File(Environment.getExternalStorageDirectory().toString() +
                    FILE_NAME + ".xls");
            if (!ex.exists()){
                if (factory.equals("FVI")){
                    inputStream = am.open("excel_qam_fvi.xls");
                    workbook =getWorkbook(inputStream,"excel_qam_fvi.xls");
                }else if (factory.equals("FVI_II_305")){
                    inputStream = am.open("excel_qam_fvi2.xls");
                    workbook =getWorkbook(inputStream,"excel_qam_fvi2.xls");
                }
            }else{
                inputStream = new FileInputStream(ex);
                workbook =getWorkbook(inputStream,FILE_NAME + ".xls");
            }
            if (factory.equals("FVI")){
                IP_BatchFragment.createExcel(workbook,IP_BatchFragment.RP_LIST_BATCH, IP_BatchFragment.list_vl, IP_BatchFragment.list_wvl);
            }else if (factory.equals("FVI_II_305")){
                IP_BatchFragment.createExcel(workbook,IP_BatchFragment.RP_LIST_BATCH, IP_BatchFragment.list_vl, IP_BatchFragment.list_wvl);

//                IP_BatchFVI2Fragment.createExcel(workbook,IP_BatchFVI2Fragment.RP_LIST_BATCH, IP_BatchFVI2Fragment.list_vl, IP_BatchFVI2Fragment.list_wvl);

            }

            KneaderFragment.createExcel(workbook,KneaderFragment.BTC);
            OpenMillFragment.createExcel(workbook,OpenMillFragment.BTC_OP);
            getData(ins_no,shift,date);
//            createExcel_FVI_II(workbook,arrBTC);
//            Card2Fragment.createExcel_FVI_II(workbook,Card2Fragment.arrBTC);
            Sheet sheet = workbook.getSheetAt(3);

            //FVI-2  IB-BATCH NEW
            Sheet sheet1 = workbook.getSheetAt(4);

            Cell cell;
            Row row;
            int rownum=4;

            CellStyle style = createStyleForTitle(workbook);

            //FVI-2 IB-BATCH 2
            row = sheet1.getRow(15);
            cell = row.getCell(0);
            cell.setCellValue(listBTC.get(0).getNEW_BATCH_NO());
            sheet.addMergedRegion(new CellRangeAddress(15, 15, 0, 3));
            cell.setCellStyle(style);

            //FVI-2 mcs IB-BATCH 2
            row = sheet1.getRow(2);
            cell = row.getCell(1);
            String a1 = MCS;
            String a2 = a1.replace('|', '/');
            cell.setCellValue(a2);
            cell.setCellStyle(style);

            //ER
            row = sheet1.getRow(2);
            cell = row.getCell(3);
            String q1 = ER;
            cell.setCellValue(q1);
            cell.setCellStyle(style);

            //ngay het han vs trong luong
            row = sheet1.getRow(16);
            cell = row.getCell(3);
            cell.setCellValue(pdfActivity.TL);
            cell.setCellStyle(style);

            row = sheet1.getRow(17);
            cell = row.getCell(3);
            cell.setCellValue(pdfActivity.EXDATE);
            cell.setCellStyle(style);

            //FVI-2 model/hinhthe IB-BATCH 2
            row = sheet1.getRow(3);
            cell = row.getCell(2);
            String b1 = (STYLE);
            cell.setCellValue(b1);
            cell.setCellValue(STYLE);

            //FVI-2 color IB-BATCH 2
            row = sheet1.getRow(4);
            cell = row.getCell(2);
            String c1 = COLOR;
            cell.setCellValue(c1);
            cell.setCellStyle(style);

            //FVI-2 dưlieu IB-BATCH 2
            int rownum1 = 6;
            for (int i = 0; i < listBTC.size(); i++) {
                row = sheet1.getRow(rownum1 + i);
                cell = row.getCell(1);
                cell.setCellValue(listBTC.get(i).getBTC_BATCH_NO() == null ? "" : listBTC.get(i).getBTC_BATCH_NO());
                cell.setCellStyle(style);
                if (i == 8) {
                    break;
                }
            }
            if (listBTC.size() > 8) {
                rownum1 = 6;
                int count = 0;
                for (int i = 8; i < listBTC.size(); i++) {

                    row = sheet1.getRow(rownum1 + count);
                    cell = row.getCell(3);
                    cell.setCellValue(listBTC.get(i).getBTC_BATCH_NO() == null ? "" : listBTC.get(i).getBTC_BATCH_NO());
                    cell.setCellStyle(style);
                    count++;
                }
            }

            row = sheet.getRow(1);
            cell = row.getCell(1);
            cell.setCellValue(shift);
            cell.setCellStyle(style);

            cell = row.getCell(9);
            cell.setCellValue(date);
            cell.setCellStyle(style);

            for (int i = 0;i<listBTC.size();i++){
                row = sheet.createRow(rownum);
                cell = row.createCell(0);
                String s1 = MCS;
                String s2 = s1.replace('|','/');
                cell.setCellValue(s2);
                cell.setCellStyle(style);

                cell = row.createCell(1);
                cell.setCellValue(listBTC.get(i).getBTC_BATCH_NO());
                cell.setCellStyle(style);

                cell = row.createCell(2);
                cell.setCellValue(listBTC.get(i).getBTC_STYLE());
                cell.setCellStyle(style);

                cell = row.createCell(3);
                cell.setCellValue(listBTC.get(i).getBTC_COLOR());
                cell.setCellStyle(style);

                cell = row.createCell(4);
                cell.setCellValue(listBTC.get(i).getPELL_GRAN());
                cell.setCellStyle(style);

                cell = row.createCell(5);
                cell.setCellValue(listBTC.get(i).getPELL_TEMP_1_NOTE() == null ? listBTC.get(i).getPELL_TEMP_1() : listBTC.get(i).getPELL_TEMP_1_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(6);
                cell.setCellValue(listBTC.get(i).getPELL_TEMP_2_NOTE() == null ? listBTC.get(i).getPELL_TEMP_2() : listBTC.get(i).getPELL_TEMP_2_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(7);
                cell.setCellValue(listBTC.get(i).getPELL_TEMP_3_NOTE() == null ? listBTC.get(i).getPELL_TEMP_3() : listBTC.get(i).getPELL_TEMP_3_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(8);
                cell.setCellValue(listBTC.get(i).getPELL_TEMP_6_NOTE() == null ? listBTC.get(i).getPELL_TEMP_5() : listBTC.get(i).getPELL_TEMP_6_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(9);
                cell.setCellValue(listBTC.get(i).getPELL_TEMP_HEAD_NOTE() == null ? listBTC.get(i).getPELL_TEMP_HEAD() : listBTC.get(i).getPELL_TEMP_HEAD_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(10);
                cell.setCellValue(listBTC.get(i).getNEW_BATCH_NO());
                cell.setCellStyle(style);

                cell = row.createCell(11);
                cell.setCellValue(listBTC.get(i).getPELL_MIXING_TIME());
                cell.setCellStyle(style);

                cell = row.createCell(12);
                cell.setCellValue(listBTC.get(i).getPELL_OUT_TEMP_NOTE() == null ? listBTC.get(i).getPELL_OUT_TEMP() : listBTC.get(i).getPELL_OUT_TEMP_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(13);
                cell.setCellValue(listBTC.get(i).getPELL_OUT_TIME());
                cell.setCellStyle(style);

                cell = row.createCell(14);
                cell.setCellValue(listBTC.get(i).getPELL_CLEAN_TIME());
                cell.setCellStyle(style);

                rownum++;
//              getData(ins_no,shift,date);
//              createExcel_FVI_II(arrBTC);



            }
            File check = new File(Environment.getExternalStorageDirectory().toString() +
                    "/QAM_EXCEL/" + FILE_NAME + ".xls");
            if (check.exists()){
                check.delete();
            }
            outFile = new FileOutputStream(check);
            workbook.write(outFile);

            System.out.println("Created file: " + check.getAbsolutePath());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outFile != null) {
                try {
                    outFile.flush();
                    outFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    MODEL_SEND_MAIL model;
//    String[] ListFile;
//
//    String DEST = Environment.getExternalStorageDirectory().toString() + "/QAM_EXCEL/";

    public static void createExcel_FVI_II(Workbook workbook, ArrayList<RP_BTC> arrBTC) throws FileNotFoundException {

        try{
            Sheet sheet = workbook.getSheetAt(4);
            Cell cell;
            Row row;
            CellStyle style = createStyleForTitle(workbook);
//msc vs Er
            row = sheet.getRow(2);
            cell = row.getCell(1);
            cell.setCellValue(pdfActivity.MCS);
            cell.setCellStyle(style);
//hinh the
            row = sheet.getRow(3);
            cell = row.getCell(2);
            cell.setCellValue(pdfActivity.style);
            cell.setCellStyle(style);
//mau sác
            row = sheet.getRow(4);
            cell = row.getCell(2);
            cell.setCellValue(pdfActivity.color);
            cell.setCellStyle(style);
//list ma dot
            int rownum=6;
            for (int i=0;i<arrBTC.size();i++) {
                row = sheet.getRow(rownum + i);
                cell = row.getCell(1);
                cell.setCellValue(arrBTC.get(i).getBTC_BATCH_NO() == null ? "" : arrBTC.get(i).getBTC_BATCH_NO());
                cell.setCellStyle(style);
                if (i == 8) {
                    break;
                }
            }
            if (arrBTC.size()>8){
                rownum=6;
                int count=0;
                for (int i=8;i<arrBTC.size();i++){
                    row = sheet.getRow(rownum+count);
                    cell = row.getCell(3);
                    cell.setCellValue(arrBTC.get(i).getBTC_BATCH_NO() == null ? "" : arrBTC.get(i).getBTC_BATCH_NO());
                    cell.setCellStyle(style);
                    count++;
                }
            }
// mã số sau khi kêt hop
            row = sheet.getRow(15);
            cell = row.getCell(0);
            cell.setCellValue(arrBTC.get(0).getBTC_NO());
            sheet.addMergedRegion(new CellRangeAddress(15, 15, 0, 3));
            cell.setCellStyle(style);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
        Workbook workbook = null;
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }
        return workbook;
    }

    private static CellStyle createStyleForTitle(Workbook workbook) {
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        // Font Color
        font.setColor(IndexedColors.BLACK.getIndex());
        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setBorderLeft((short)1);
        style.setBorderRight((short)1);
        style.setBorderTop((short)1);
        style.setBorderBottom((short)1);
        style.setLocked(true);
        return style;
    }

    public void DeteleFileBefore7days() {
        Calendar time = Calendar.getInstance();
        time.add(Calendar.DAY_OF_YEAR, -4);
        String path = Environment.getExternalStorageDirectory().toString() + "/QAM_EXCEL";
        File directory = new File(path);
        File[] files = directory.listFiles();
        for (int i = 0; i < files.length; i++) {
            File file = new File(Environment.getExternalStorageDirectory().toString() + "/QAM_EXCEL/", files[i].getName());
            Date lastModified1 = new Date(file.lastModified());
            if (lastModified1.before(time.getTime())) {
                boolean deleted = file.delete();
            }
        }
    }

    public class  RP_BTCPELLArm extends ApiReturnModel<ArrayList<BTC_PELL>> {}
}
