package com.deanshoes.AppQAM.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.reportadapter.ListIPBatchAdapter;
import com.deanshoes.AppQAM.adapter.reportadapter.ListIPBatchFVI2Adapter;
import com.deanshoes.AppQAM.model.QAM.RP_LIST_BATCH;
import com.deanshoes.AppQAM.model.QAM.RP_MAC;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step4.pdfActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class IP_BatchFVI2Fragment extends Fragment {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    ArrayList<com.deanshoes.AppQAM.model.QAM.RP_MAC> RP_MAC;

    TextView tv_mtr1,tv_mtr2,tv_mtr3,tv_mtr4,tv_mtr5,tv_mtr6,tv_mtr7,tv_mtr8,tv_mtr9,tv_mtr10;
    TextView tv_chem1,tv_chem2,tv_chem3,tv_chem4,tv_chem5,tv_chem6,tv_chem7,tv_chem8,tv_chem9,tv_chem10;
    TextView tv_chem11,tv_chem12,tv_chem13,tv_chem14,tv_chem15,tv_chem16,tv_chem17,tv_chem18,tv_chem19,tv_chem20;
    TextView tv_pig1,tv_pig2,tv_pig3,tv_pig4,tv_pig5,tv_pig6,tv_pig7,tv_pig8,tv_pig9,tv_pig10;
    TextView tv_smtr1,tv_smtr2,tv_smtr3,tv_smtr4,tv_smtr5,tv_smtr6,tv_smtr7,tv_smtr8,tv_smtr9,tv_smtr10;
    TextView tv_schem1,tv_schem2,tv_schem3,tv_schem4,tv_schem5,tv_schem6,tv_schem7,tv_schem8,tv_schem9,tv_schem10;
    TextView tv_schem11,tv_schem12,tv_schem13,tv_schem14,tv_schem15,tv_schem16,tv_schem17,tv_schem18,tv_schem19,tv_schem20;
    TextView tv_spig1,tv_spig2,tv_spig3,tv_spig4,tv_spig5,tv_spig6,tv_spig7,tv_spig8,tv_spig9,tv_spig10;

    TextView tv_mcs, tv_shift,tv_date;
    ListView rv_ipbatch;

    public static String date="", shift ="", ins_no = "",MCS="";
    String factory="";
    String FILE_NAME="";
    public static int mtr_num=0,chem_num=0,pig_num=0;

    TextView[] list_tv, list_w;
    public static String[] list_vl, list_wvl;
    public static ArrayList<com.deanshoes.AppQAM.model.QAM.RP_LIST_BATCH> RP_LIST_BATCH;

    public static IP_BatchFVI2Fragment newInstance(String title) {
        IP_BatchFVI2Fragment fragment = new IP_BatchFVI2Fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ip__batch, container, false);
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RP_LIST_BATCH = new ArrayList<>();
        factory= pdfActivity.factory;
        ins_no = pdfActivity.ins_no;
        date= pdfActivity.date;
        shift =pdfActivity.shift;
        MCS = pdfActivity.MCS;

        list_vl = new String[]{};
        findbyID();
        tv_mcs.setText("MCS# "+ MCS);
        tv_shift.setText(shift);
        tv_date.setText(date);


//        FILE_NAME = "IP-BATCH_"+ ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
//                +"_" + shift ;

        FILE_NAME = ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
                +"_" + shift ;

        System.out.println("ins no" + ins_no);
        System.out.println("File name" + FILE_NAME);

        list_tv = new TextView[]{tv_mtr1,tv_mtr2,tv_mtr3,tv_mtr4,tv_mtr5,tv_mtr6,tv_mtr7,tv_mtr8,tv_mtr9,tv_mtr10,
                tv_chem1,tv_chem2,tv_chem3,tv_chem4,tv_chem5,tv_chem6,tv_chem7,tv_chem8,tv_chem9,tv_chem10,
                tv_chem11,tv_chem12,tv_chem13,tv_chem14,tv_chem15,tv_chem16,tv_chem17,tv_chem18,tv_chem19,tv_chem20,
                tv_pig1,tv_pig2,tv_pig3,tv_pig4,tv_pig5,tv_pig6,tv_pig7,tv_pig8,tv_pig9,tv_pig10};

        list_w = new TextView[]{tv_smtr1,tv_smtr2,tv_smtr3,tv_smtr4,tv_smtr5,tv_smtr6,tv_smtr7,tv_smtr8,tv_smtr9,tv_smtr10,
                tv_schem1,tv_schem2,tv_schem3,tv_schem4,tv_schem5,tv_schem6,tv_schem7,tv_schem8,tv_schem9,tv_schem10,
                tv_schem11,tv_schem12,tv_schem13,tv_schem14,tv_schem15,tv_schem16,tv_schem17,tv_schem18,tv_schem19,tv_schem20,
                tv_spig1,tv_spig2,tv_spig3,tv_spig4,tv_spig5,tv_spig6,tv_spig7,tv_spig8,tv_spig9,tv_spig10};

        getList_MAC();
        getList_BATCH();

    }

    public void getList_MAC(){
        try{
            CallApi.getRP_MAC(factory, ins_no, date, shift, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    RP_MAC = new ArrayList<>();
                    IP_BatchFragment.RP_MACArm rp_macArm = gson.fromJson(response.toString(), IP_BatchFragment.RP_MACArm.class);
                    RP_MAC = rp_macArm.getItems() == null ? new ArrayList<RP_MAC>() : rp_macArm.getItems();

                    if (RP_MAC.size() > 0) {
                        list_vl = new String[]{RP_MAC.get(0).getMAC_MTR1(), RP_MAC.get(0).getMAC_MTR2(), RP_MAC.get(0).getMAC_MTR3(), RP_MAC.get(0).getMAC_MTR4(),
                                RP_MAC.get(0).getMAC_MTR5(), RP_MAC.get(0).getMAC_MTR6(), RP_MAC.get(0).getMAC_MTR7(), RP_MAC.get(0).getMAC_MTR8(), RP_MAC.get(0).getMAC_MTR9(),
                                RP_MAC.get(0).getMAC_MTR10(), RP_MAC.get(0).getMAC_CHEM1(), RP_MAC.get(0).getMAC_CHEM2(), RP_MAC.get(0).getMAC_CHEM3(), RP_MAC.get(0).getMAC_CHEM4(),
                                RP_MAC.get(0).getMAC_CHEM5(), RP_MAC.get(0).getMAC_CHEM6(), RP_MAC.get(0).getMAC_CHEM7(), RP_MAC.get(0).getMAC_CHEM8(), RP_MAC.get(0).getMAC_CHEM9(),
                                RP_MAC.get(0).getMAC_CHEM10(), RP_MAC.get(0).getMAC_CHEM11(), RP_MAC.get(0).getMAC_CHEM12(), RP_MAC.get(0).getMAC_CHEM13(), RP_MAC.get(0).getMAC_CHEM14(),
                                RP_MAC.get(0).getMAC_CHEM15(), RP_MAC.get(0).getMAC_CHEM16(), RP_MAC.get(0).getMAC_CHEM17(), RP_MAC.get(0).getMAC_CHEM18(), RP_MAC.get(0).getMAC_CHEM19(),
                                RP_MAC.get(0).getMAC_CHEM20(), RP_MAC.get(0).getMAC_PIG1(), RP_MAC.get(0).getMAC_PIG2(), RP_MAC.get(0).getMAC_PIG3(), RP_MAC.get(0).getMAC_PIG4(),
                                RP_MAC.get(0).getMAC_PIG5(), RP_MAC.get(0).getMAC_PIG6(), RP_MAC.get(0).getMAC_PIG7(), RP_MAC.get(0).getMAC_PIG8(), RP_MAC.get(0).getMAC_PIG9(),
                                RP_MAC.get(0).getMAC_PIG10()};

                        list_wvl = new String[]{RP_MAC.get(0).getSMAC_MTR1(), RP_MAC.get(0).getSMAC_MTR2(), RP_MAC.get(0).getSMAC_MTR3(), RP_MAC.get(0).getSMAC_MTR4(),
                                RP_MAC.get(0).getSMAC_MTR5(), RP_MAC.get(0).getSMAC_MTR6(), RP_MAC.get(0).getSMAC_MTR7(), RP_MAC.get(0).getSMAC_MTR8(), RP_MAC.get(0).getSMAC_MTR9(),
                                RP_MAC.get(0).getSMAC_MTR10(), RP_MAC.get(0).getSMAC_CHEM1(), RP_MAC.get(0).getSMAC_CHEM2(), RP_MAC.get(0).getSMAC_CHEM3(), RP_MAC.get(0).getSMAC_CHEM4(),
                                RP_MAC.get(0).getSMAC_CHEM5(), RP_MAC.get(0).getSMAC_CHEM6(), RP_MAC.get(0).getSMAC_CHEM7(), RP_MAC.get(0).getSMAC_CHEM8(), RP_MAC.get(0).getSMAC_CHEM9(),
                                RP_MAC.get(0).getSMAC_CHEM10(), RP_MAC.get(0).getSMAC_CHEM11(), RP_MAC.get(0).getSMAC_CHEM12(), RP_MAC.get(0).getSMAC_CHEM13(), RP_MAC.get(0).getSMAC_CHEM14(),
                                RP_MAC.get(0).getSMAC_CHEM15(), RP_MAC.get(0).getSMAC_CHEM16(), RP_MAC.get(0).getSMAC_CHEM17(), RP_MAC.get(0).getSMAC_CHEM18(), RP_MAC.get(0).getSMAC_CHEM19(),
                                RP_MAC.get(0).getSMAC_CHEM20(), RP_MAC.get(0).getSMAC_PIG1(), RP_MAC.get(0).getSMAC_PIG2(), RP_MAC.get(0).getSMAC_PIG3(), RP_MAC.get(0).getSMAC_PIG4(),
                                RP_MAC.get(0).getSMAC_PIG5(), RP_MAC.get(0).getSMAC_PIG6(), RP_MAC.get(0).getSMAC_PIG7(), RP_MAC.get(0).getSMAC_PIG8(), RP_MAC.get(0).getSMAC_PIG9(),
                                RP_MAC.get(0).getSMAC_PIG10()};

                        setValue(list_tv, list_vl);
                        setValue(list_w, list_wvl);

                        for(int i=0;i<list_vl.length-30;i++){
                            if (list_vl[i]!=null){
                                mtr_num++;
                            }
                        }
                        for(int i=9;i<list_vl.length-10;i++){
                            if (list_vl[i]!=null){
                                chem_num++;
                            }
                        }
                        for(int i=29;i<list_vl.length;i++){
                            if (list_vl[i]!=null){
                                pig_num++;
                            }
                        }

                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void getList_BATCH(){
        try{
            CallApi.getListBatch(factory, ins_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    IP_BatchFragment.RP_BATCHArm rp_batchArm = gson.fromJson(response.toString(), IP_BatchFragment.RP_BATCHArm.class);
                    RP_LIST_BATCH = rp_batchArm.getItems() == null ? new ArrayList<RP_LIST_BATCH>() : rp_batchArm.getItems();
                    ListIPBatchFVI2Adapter listIPBatchAdapter = new ListIPBatchFVI2Adapter(getActivity(),R.layout.rp_ipbatch_adapter_fvi2, RP_LIST_BATCH);
                    rv_ipbatch.setAdapter(listIPBatchAdapter);


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public static void createExcel(Workbook workbook, ArrayList<RP_LIST_BATCH> listBTC, String[] name, String [] weight) throws FileNotFoundException {
        if (list_vl.length>0 && list_wvl.length>0){
            try{

                Sheet sheet = workbook.getSheetAt(0);
                Cell cell;
                Row row;
                int rownum=5,col=0;
                CellRangeAddress mergedCell;

                CellStyle style = createStyleForTitle(workbook);
                CellStyle styletitle =  createStyleBold(workbook);

                if (mtr_num>4 || chem_num>10 || pig_num>6){
                    mergedCell = new CellRangeAddress(0,0,0,3+mtr_num+chem_num+pig_num);
                    sheet.addMergedRegion(mergedCell);

                }else{
                    mergedCell = new CellRangeAddress(0,0,0,3+4+10+6);
                    sheet.addMergedRegion(mergedCell);
                }
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                row = sheet.getRow(0);
                // Creates the cell
                cell = CellUtil.getCell(row,0);

                // Sets the allignment to the created cell
                CellUtil.setAlignment(cell, workbook, CellStyle.ALIGN_CENTER);

                row = sheet.getRow(1);
                //set value standard
                cell = row.createCell(1);
                cell.setCellValue(MCS);
                cell.setCellStyle(style);

                //set shift
                if (mtr_num>4){
                    col=4+mtr_num;
                }else{
                    col=8;
                }
                mergedCell = new CellRangeAddress(1,1,4,col-1);
                sheet.addMergedRegion(mergedCell);
                cell = CellUtil.getCell(row,4);
                cell.setCellValue(pdfActivity.shift);
                cell.setCellStyle(style);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                //set title date
                mergedCell = new CellRangeAddress(1,1,col,col+1);
                sheet.addMergedRegion(mergedCell);
                cell = row.createCell(col);
                cell.setCellValue("日期/Ngày");
                cell.setCellStyle(styletitle);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                //set value date
                if (chem_num>10 && pig_num>6){
                    mergedCell = new CellRangeAddress(1,1,10,7+chem_num+pig_num);
                    sheet.addMergedRegion(mergedCell);
                }else if (chem_num>10 && pig_num<6){
                    mergedCell = new CellRangeAddress(1,1,10,7+chem_num+6);
                    sheet.addMergedRegion(mergedCell);
                }else if (chem_num<10 && pig_num>6) {
                    mergedCell = new CellRangeAddress(1, 1, 10, 7 + 10 + pig_num);
                    sheet.addMergedRegion(mergedCell);
                }else{
                    mergedCell = new CellRangeAddress(1,1,10,7+10+6);
                    sheet.addMergedRegion(mergedCell);
                }
                cell = row.createCell(10);
                cell.setCellValue(pdfActivity.date);
                cell.setCellStyle(style);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                //set title Raw mtr/Chemical/Pigment
                row = sheet.getRow(2);
                cell = row.createCell(4);
                cell.setCellValue("主料(g) LIỆU CHỦ(g)");
                cell.setCellStyle(styletitle);
                mergedCell = new CellRangeAddress(2,2,4,col-1);
                sheet.addMergedRegion(mergedCell);
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                cell = row.createCell(col);
                cell.setCellValue("藥品(g) HÓA CHẤT(g)");
                cell.setCellStyle(styletitle);
                if (chem_num>10){
                    mergedCell = new CellRangeAddress(2,2,col,col+chem_num-1);
                    sheet.addMergedRegion(mergedCell);
                    col=col+chem_num;
                }else{
                    mergedCell = new CellRangeAddress(2,2,col,col+9);
                    sheet.addMergedRegion(mergedCell);
                    col=col+10;
                }
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);

                cell = row.createCell(col);
                cell.setCellValue("色粒(g) LIỆU MÀU(g)");
                cell.setCellStyle(styletitle);
                if (pig_num>6){
                    mergedCell = new CellRangeAddress(2,2,col,col+pig_num-1);
                    sheet.addMergedRegion(mergedCell);
                }else{
                    mergedCell = new CellRangeAddress(2,2,col,col+5);
                    sheet.addMergedRegion(mergedCell);
                }
                RegionUtil.setBorderTop(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);
                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, mergedCell, sheet, workbook);


                //set value Raw mtr/Chemical/Pigment
                int colnum=4;
                for (int i=0;i<name.length-30;i++){
                    if (mtr_num<=4){
                        row = sheet.getRow(3);
                        cell = row.createCell(colnum);
                        cell.setCellValue(name[i]);
                        cell.setCellStyle(style);

                        row = sheet.getRow(4);
                        cell = row.createCell(colnum);
                        cell.setCellValue(weight[i]);
                        cell.setCellStyle(style);
                        colnum++;
                        if (i==3){
                            colnum=8;
                            break;
                        }
                    }else{
                        if (name[i]!=null){
                            row = sheet.getRow(3);
                            cell = row.createCell(colnum);
                            cell.setCellValue(name[i]);
                            cell.setCellStyle(style);

                            row = sheet.getRow(4);
                            cell = row.createCell(colnum);
                            cell.setCellValue(weight[i]);
                            cell.setCellStyle(style);
                            colnum++;

                        }
                    }


                }
                for (int i=10;i<name.length-10;i++){
                    if (chem_num<=10){
                        row = sheet.getRow(3);
                        cell = row.createCell(colnum);
                        cell.setCellValue(name[i]);
                        cell.setCellStyle(style);

                        row = sheet.getRow(4);
                        cell = row.createCell(colnum);
                        cell.setCellValue(weight[i]);
                        cell.setCellStyle(style);
                        colnum++;
                        if (i==19){
                            colnum=18;
                            break;
                        }
                    }else{
                        if (name[i]!=null){
                            row = sheet.getRow(3);
                            cell = row.createCell(colnum);
                            cell.setCellValue(name[i]);
    //                            String s1 = style.getDataFormatString();
    //                            String s2 = s1.replace('_','-');
    //                            cell.setCellValue(s2);
                             cell.setCellStyle(style);




                            row = sheet.getRow(4);
                            cell = row.createCell(colnum);
                            cell.setCellValue(weight[i]);
//                            cell.setCellValue(s2);
                            cell.setCellStyle(style);

                            colnum++;

                        }
                    }

                }

                for (int i=30;i<name.length;i++){
                    if (pig_num<=6){
                        row = sheet.getRow(3);
                        cell = row.createCell(colnum);
                        cell.setCellValue(name[i]);
                        cell.setCellStyle(style);

                        row = sheet.getRow(4);
                        cell = row.createCell(colnum);
                        cell.setCellValue(weight[i]);
                        cell.setCellStyle(style);
                        colnum++;
                        if (i==35){
                            break;
                        }
                    }else {
                        if (name[i]!=null){
                            row = sheet.getRow(3);
                            cell = row.createCell(colnum);
                            cell.setCellValue(name[i]);
                            cell.setCellStyle(style);

                            row = sheet.getRow(4);
                            cell = row.createCell(colnum);
                            cell.setCellValue(weight[i]);
                            cell.setCellStyle(style);
                            colnum++;

                        }
                    }

                }

                //set list IP-Batch

                String[] note;
                for (int i = 0;i<listBTC.size();i++){
                    int column=4;
                    row = sheet.createRow(rownum);
                    cell = row.createCell(0);
                    cell.setCellValue(listBTC.get(i).getBTC_BATCH_NO());
                    cell.setCellStyle(style);

                    cell = row.createCell(1);
                    cell.setCellValue(listBTC.get(i).getSTYLE());
                    cell.setCellStyle(style);

                    cell = row.createCell(2);
                    cell.setCellValue(listBTC.get(i).getCOLOR());
                    cell.setCellStyle(style);

                    cell = row.createCell(3);
                    cell.setCellValue(listBTC.get(i).getHARD());
                    cell.setCellStyle(style);

                    note = new String[]{listBTC.get(i).getNOTE_MTR1(),listBTC.get(i).getNOTE_MTR2(),listBTC.get(i).getNOTE_MTR3(),
                            listBTC.get(i).getNOTE_MTR4(),listBTC.get(i).getNOTE_MTR5(),listBTC.get(i).getNOTE_MTR6(),
                            listBTC.get(i).getNOTE_MTR7(),listBTC.get(i).getNOTE_MTR8(),listBTC.get(i).getNOTE_MTR9(),
                            listBTC.get(i).getNOTE_MTR10(),listBTC.get(i).getNOTE_CHEM1(),listBTC.get(i).getNOTE_CHEM2(),
                            listBTC.get(i).getNOTE_CHEM3(),listBTC.get(i).getNOTE_CHEM4(),listBTC.get(i).getNOTE_CHEM5(),
                            listBTC.get(i).getNOTE_CHEM6(),listBTC.get(i).getNOTE_CHEM7(),listBTC.get(i).getNOTE_CHEM8(),
                            listBTC.get(i).getNOTE_CHEM9(),listBTC.get(i).getNOTE_CHEM10(),listBTC.get(i).getNOTE_CHEM11(),
                            listBTC.get(i).getNOTE_CHEM12(),listBTC.get(i).getNOTE_CHEM13(),listBTC.get(i).getNOTE_CHEM14(),
                            listBTC.get(i).getNOTE_CHEM15(),listBTC.get(i).getNOTE_CHEM16(),listBTC.get(i).getNOTE_CHEM17(),
                            listBTC.get(i).getNOTE_CHEM18(),listBTC.get(i).getNOTE_CHEM19(),listBTC.get(i).getNOTE_CHEM20(),
                            listBTC.get(i).getNOTE_PIG1(),listBTC.get(i).getNOTE_PIG2(),listBTC.get(i).getNOTE_PIG3(),
                            listBTC.get(i).getNOTE_PIG4(),listBTC.get(i).getNOTE_PIG5(),listBTC.get(i).getNOTE_PIG6(),
                            listBTC.get(i).getNOTE_PIG7(),listBTC.get(i).getNOTE_PIG8(),listBTC.get(i).getNOTE_PIG9(),
                            listBTC.get(i).getNOTE_PIG10()};

                    for (int j=0;j<name.length-30;j++){
                        cell = row.createCell(column);
                        if (mtr_num<=4){
                            if (name[j]!=null){
                                cell.setCellValue(note[j] == null ? "V" : note[j]);
                                cell.setCellStyle(style);
                                column++;
                            }else {
                                cell.setCellValue("");
                                cell.setCellStyle(style);
                                column++;
                            }
                            if (mtr_num<=4 && j==3){
                                column=8;
                                break;
                            }

                        }else{
                            if (name[j]!=null){
                                cell.setCellValue(note[j] == null ? "V" : note[j]);
                                cell.setCellStyle(style);
                                column++;
                            }
                        }



                    }
                    for (int j=10;j<name.length-10;j++){
                        cell = row.createCell(column);
                        if (chem_num<=10){
                            if (name[j]!=null){
                                cell.setCellValue(note[j] == null ? "V" : note[j]);
                                cell.setCellStyle(style);
                                column++;
                            }else {
                                cell.setCellValue("");
                                cell.setCellStyle(style);
                                column++;
                            }
                            if (j==19){
                                column=18;
                                break;
                            }
                        }else{
                            if (name[j]!=null){
                                cell.setCellValue(note[j] == null ? "V" : note[j]);
                                cell.setCellStyle(style);
                                column++;
                            }
                        }
                    }
                    for (int j=30;j<name.length;j++){
                        cell = row.createCell(column);
                        if(pig_num<=6){
                            if (name[j]!=null){
                                cell.setCellValue(note[j] == null ? "V" : note[j]);
                                cell.setCellStyle(style);
                                column++;
                            }else {
                                cell.setCellValue("");
                                cell.setCellStyle(style);
                                column++;
                            }
                            if (j==35){
                                break;
                            }
                        }else{
                            if (name[j]!=null){
                                cell.setCellValue(note[j] == null ? "V" : note[j]);
                                cell.setCellStyle(style);
                                column++;
                            }
                        }

                    }
                    rownum++;
                }

                System.out.println("Created sheet IP-Batch " );

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
        Workbook workbook = null;
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }

    public static CellStyle createStyleForTitle(Workbook workbook) {

        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        // Font Color
        font.setColor(IndexedColors.BLACK.getIndex());
        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setBorderLeft((short)1);
        style.setBorderRight((short)1);
        style.setBorderTop((short)1);
        style.setBorderBottom((short)1);

        return style;
    }

    public static CellStyle createStyleBold(Workbook workbook) {

        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setFontName("Times New Roman");
        // Font Color
        font.setColor(IndexedColors.BLACK.getIndex());
        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setBorderLeft((short)1);
        style.setBorderRight((short)1);
        style.setBorderTop((short)1);
        style.setBorderBottom((short)1);
        style.setWrapText(true);

        return style;
    }

    public static void setValue(TextView[] list_tv,String[] str){
        for (int i =0;i<list_tv.length;i++){
            if (checkValue(str[i])){
                list_tv[i].setText(str[i]);
                list_tv[i].setVisibility(View.VISIBLE);
            }else{
                list_tv[i].setVisibility(View.GONE);
            }
        }

    }

    public static boolean checkValue(String str){
        if (str !=null && !str.equals("")){
            return true;
        }
        return false;
    }

    private void findbyID(){

        tv_mcs = getActivity().findViewById(R.id.tv_mcs);
        tv_shift = getActivity().findViewById(R.id.tv_shift);
        tv_date = getActivity().findViewById(R.id.tv_date);

        tv_mtr1 = getActivity().findViewById(R.id.tv_mtr1);
        tv_mtr2 = getActivity().findViewById(R.id.tv_mtr2);
        tv_mtr3 = getActivity().findViewById(R.id.tv_mtr3);
        tv_mtr4 = getActivity().findViewById(R.id.tv_mtr4);
        tv_mtr5 = getActivity().findViewById(R.id.tv_mtr5);
        tv_mtr6 = getActivity().findViewById(R.id.tv_mtr6);
        tv_mtr7 = getActivity().findViewById(R.id.tv_mtr7);
        tv_mtr8 = getActivity().findViewById(R.id.tv_mtr8);
        tv_mtr9 = getActivity().findViewById(R.id.tv_mtr9);
        tv_mtr10 = getActivity().findViewById(R.id.tv_mtr10);

        tv_chem1 = getActivity().findViewById(R.id.tv_chem1);
        tv_chem2 = getActivity().findViewById(R.id.tv_chem2);
        tv_chem3 = getActivity().findViewById(R.id.tv_chem3);
        tv_chem4 = getActivity().findViewById(R.id.tv_chem4);
        tv_chem5 = getActivity().findViewById(R.id.tv_chem5);
        tv_chem6 = getActivity().findViewById(R.id.tv_chem6);
        tv_chem7 = getActivity().findViewById(R.id.tv_chem7);
        tv_chem8 = getActivity().findViewById(R.id.tv_chem8);
        tv_chem9 = getActivity().findViewById(R.id.tv_chem9);
        tv_chem10 = getActivity().findViewById(R.id.tv_chem10);
        tv_chem11 = getActivity().findViewById(R.id.tv_chem11);
        tv_chem12 = getActivity().findViewById(R.id.tv_chem12);
        tv_chem13 = getActivity().findViewById(R.id.tv_chem13);
        tv_chem14 = getActivity().findViewById(R.id.tv_chem14);
        tv_chem15 = getActivity().findViewById(R.id.tv_chem15);
        tv_chem16 = getActivity().findViewById(R.id.tv_chem16);
        tv_chem17 = getActivity().findViewById(R.id.tv_chem17);
        tv_chem18 = getActivity().findViewById(R.id.tv_chem18);
        tv_chem19 = getActivity().findViewById(R.id.tv_chem19);
        tv_chem20 = getActivity().findViewById(R.id.tv_chem20);

        tv_pig1 = getActivity().findViewById(R.id.tv_pig1);
        tv_pig2 = getActivity().findViewById(R.id.tv_pig2);
        tv_pig3 = getActivity().findViewById(R.id.tv_pig3);
        tv_pig4 = getActivity().findViewById(R.id.tv_pig4);
        tv_pig5 = getActivity().findViewById(R.id.tv_pig5);
        tv_pig6 = getActivity().findViewById(R.id.tv_pig6);
        tv_pig7 = getActivity().findViewById(R.id.tv_pig7);
        tv_pig8 = getActivity().findViewById(R.id.tv_pig8);
        tv_pig9 = getActivity().findViewById(R.id.tv_pig9);
        tv_pig10 = getActivity().findViewById(R.id.tv_pig10);

        tv_smtr1 = getActivity().findViewById(R.id.tv_smtr1);
        tv_smtr2 = getActivity().findViewById(R.id.tv_smtr2);
        tv_smtr3 = getActivity().findViewById(R.id.tv_smtr3);
        tv_smtr4 = getActivity().findViewById(R.id.tv_smtr4);
        tv_smtr5 = getActivity().findViewById(R.id.tv_smtr5);
        tv_smtr6 = getActivity().findViewById(R.id.tv_smtr6);
        tv_smtr7 = getActivity().findViewById(R.id.tv_smtr7);
        tv_smtr8 = getActivity().findViewById(R.id.tv_smtr8);
        tv_smtr9 = getActivity().findViewById(R.id.tv_smtr9);
        tv_smtr10 = getActivity().findViewById(R.id.tv_smtr10);

        tv_schem1 = getActivity().findViewById(R.id.tv_schem1);
        tv_schem2 = getActivity().findViewById(R.id.tv_schem2);
        tv_schem3 = getActivity().findViewById(R.id.tv_schem3);
        tv_schem4 = getActivity().findViewById(R.id.tv_schem4);
        tv_schem5 = getActivity().findViewById(R.id.tv_schem5);
        tv_schem6 = getActivity().findViewById(R.id.tv_schem6);
        tv_schem7 = getActivity().findViewById(R.id.tv_schem7);
        tv_schem8 = getActivity().findViewById(R.id.tv_schem8);
        tv_schem9 = getActivity().findViewById(R.id.tv_schem9);
        tv_schem10 = getActivity().findViewById(R.id.tv_schem10);
        tv_schem11 = getActivity().findViewById(R.id.tv_schem11);
        tv_schem12 = getActivity().findViewById(R.id.tv_schem12);
        tv_schem13 = getActivity().findViewById(R.id.tv_schem13);
        tv_schem14 = getActivity().findViewById(R.id.tv_schem14);
        tv_schem15 = getActivity().findViewById(R.id.tv_schem15);
        tv_schem16 = getActivity().findViewById(R.id.tv_schem16);
        tv_schem17 = getActivity().findViewById(R.id.tv_schem17);
        tv_schem18 = getActivity().findViewById(R.id.tv_schem18);
        tv_schem19 = getActivity().findViewById(R.id.tv_schem19);
        tv_schem20 = getActivity().findViewById(R.id.tv_schem20);

        tv_spig1 = getActivity().findViewById(R.id.tv_spig1);
        tv_spig2 = getActivity().findViewById(R.id.tv_spig2);
        tv_spig3 = getActivity().findViewById(R.id.tv_spig3);
        tv_spig4 = getActivity().findViewById(R.id.tv_spig4);
        tv_spig5 = getActivity().findViewById(R.id.tv_spig5);
        tv_spig6 = getActivity().findViewById(R.id.tv_spig6);
        tv_spig7 = getActivity().findViewById(R.id.tv_spig7);
        tv_spig8 = getActivity().findViewById(R.id.tv_spig8);
        tv_spig9 = getActivity().findViewById(R.id.tv_spig9);
        tv_spig10 = getActivity().findViewById(R.id.tv_spig10);

        rv_ipbatch =getActivity().findViewById(R.id.rv_ipbatch);
    }

    public class  RP_MACArm extends ApiReturnModel<ArrayList<RP_MAC>> {}

    public class  RP_BATCHArm extends ApiReturnModel<ArrayList<RP_LIST_BATCH>> {}
}
