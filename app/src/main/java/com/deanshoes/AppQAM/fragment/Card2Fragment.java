package com.deanshoes.AppQAM.fragment;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.reportadapter.ListKneaderAdapter;
import com.deanshoes.AppQAM.adapter.reportadapter.ListPellAdapter;
import com.deanshoes.AppQAM.adapter.rpadapter.BTCAdapter;
import com.deanshoes.AppQAM.model.QAM.BTC;
import com.deanshoes.AppQAM.model.QAM.BTC_OP;
import com.deanshoes.AppQAM.model.QAM.BTC_PELL;
import com.deanshoes.AppQAM.model.QAM.MODEL_SEND_MAIL;
import com.deanshoes.AppQAM.model.report_model.RP_BTC;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step4.ReportActivity;
import com.deanshoes.AppQAM.step4.pdfActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static com.deanshoes.AppQAM.step4.pdfActivity.style;


public class Card2Fragment extends Fragment {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    TextView tv_shift,tv_date;
    TextView tv_temp, tv_time,tv_temp1,tv_time1,tv_temp2,tv_time2,tv_temp3,tv_time3,tv_temp4,
            tv_time4,tv_tempout,tv_timeout, tv_note;

    ListView rv_kneader;
    String factory="";
    ListView lv_btc;
    public static String date="", shift ="", ins_no = "",MCS="",wight ="",datestart="",dateend="";

    ArrayList<String> btc_no1ArrayList;
    BTCAdapter btcAdapter;
//    ArrayList<RP_BTC> arrBTC;


    public static ArrayList<RP_BTC> arrBTC = new ArrayList<>();

//     ArrayList<RP_BTC> arrBTC = new ArrayList<>();



    boolean creMode = false;


    public static Card2Fragment newInstance(String param1) {
        Card2Fragment fragment = new Card2Fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_card2, container, false);
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        Intent intent = getActivity().getIntent();
        final Bundle bundle=intent.getExtras();
        bundle.putString("fac",ReportActivity.factory);
        bundle.putString("user","QAM");

        //findbyid();

        factory= pdfActivity.factory;

        MCS = pdfActivity.MCS;

        ins_no = pdfActivity.ins_no;
        shift = pdfActivity.shift;
        date = pdfActivity.date;
        wight =pdfActivity.wight;



        tv_shift.setText(shift);
        tv_date.setText(date);


      getData(ins_no,shift,date);



    }

    public void getData(String ins_no, String shift, String date) {
        try {
            CallApi.getData(factory, ins_no, shift, date, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    arrBTC = new ArrayList<>();
                    pdfActivity.BtcArm btcArm = gson.fromJson(response.toString(), pdfActivity.BtcArm.class);
                    arrBTC = btcArm.getItems() == null ? new ArrayList<RP_BTC>() : btcArm.getItems();
                    btcAdapter = new BTCAdapter(getActivity(), R.layout.btc_adapter, arrBTC);
//                    lv_btc.setAdapter(btcAdapter);
                    btc_no1ArrayList = new ArrayList<>();
                    for (int i = 0; i < arrBTC.size(); i++) {
                        btc_no1ArrayList.add(arrBTC.get(i).getBTC_BATCH_NO());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("LOGCAT", "Exception", e);
        }
    }



    public static void createExcel_FVI_II(Workbook workbook, ArrayList<RP_BTC> arrBTC) throws FileNotFoundException {


        try{
            Sheet sheet = workbook.getSheetAt(4);
            Cell cell;
            Row row;
            CellStyle style = createStyleForTitle(workbook);
//msc vs Er
            row = sheet.getRow(2);
            cell = row.getCell(1);
            cell.setCellValue(pdfActivity.MCS);
            cell.setCellStyle(style);


//hinh the
            row = sheet.getRow(3);
            cell = row.getCell(2);
            cell.setCellValue(pdfActivity.style);
            cell.setCellStyle(style);
//mau sác
            row = sheet.getRow(4);
            cell = row.getCell(2);
            cell.setCellValue(pdfActivity.color);
            cell.setCellStyle(style);

            //list ma dot
            int rownum=6;
            for (int i=0;i<arrBTC.size();i++){
                row = sheet.getRow(rownum+i);
                cell = row.getCell(1);
                cell.setCellValue(arrBTC.get(i).getBTC_BATCH_NO() == null ? "" : arrBTC.get(i).getBTC_BATCH_NO());
                cell.setCellStyle(style);



                if (i==8){
                    break;
                }
            }

            if (arrBTC.size()>8){
                rownum=6;
                int count=0;
                for (int i=8;i<arrBTC.size();i++){

                    row = sheet.getRow(rownum+count);
                    cell = row.getCell(3);
                    cell.setCellValue(arrBTC.get(i).getBTC_BATCH_NO() == null ? "" : arrBTC.get(i).getBTC_BATCH_NO());
                    cell.setCellStyle(style);
                    count++;
                }
            }



// mã số sau khi kêt hop
            row = sheet.getRow(15);
            cell = row.getCell(0);
            cell.setCellValue(arrBTC.get(0).getBTC_NO());
            sheet.addMergedRegion(new CellRangeAddress(15, 15, 0, 3));
            cell.setCellStyle(style);


    } catch (Exception e) {
        e.printStackTrace();
    } finally {
        }
    }
    private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
        Workbook workbook = null;
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }

    private static CellStyle createStyleForTitle(Workbook workbook) {

        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        // Font Color
        font.setColor(IndexedColors.BLACK.getIndex());
        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setBorderLeft((short)1);
        style.setBorderRight((short)1);
        style.setBorderTop((short)1);
        style.setBorderBottom((short)1);
        style.setLocked(true);

        return style;
    }




    public void findbyid(){
        rv_kneader = getActivity().findViewById(R.id.rv_kneader);
        tv_shift = getActivity().findViewById(R.id.tv_shift_kn);
        tv_date = getActivity().findViewById(R.id.tv_date_kn);
        tv_temp = getActivity().findViewById(R.id.tv_temp);
        tv_time = getActivity().findViewById(R.id.tv_time);
        tv_temp1 = getActivity().findViewById(R.id.tv_temp1);
        tv_time1 = getActivity().findViewById(R.id.tv_time1);
        tv_temp2 = getActivity().findViewById(R.id.tv_temp2);
        tv_time2 = getActivity().findViewById(R.id.tv_time2);
        tv_temp3 = getActivity().findViewById(R.id.tv_temp3);
        tv_time3 = getActivity().findViewById(R.id.tv_time3);
        tv_temp4 = getActivity().findViewById(R.id.tv_temp4);
        tv_time4 = getActivity().findViewById(R.id.tv_time4);
        tv_tempout = getActivity().findViewById(R.id.tv_tempout);
        tv_timeout = getActivity().findViewById(R.id.tv_timeout);
        tv_note = getActivity().findViewById(R.id.tv_note);
    }

    public class  RP_BTCArm extends ApiReturnModel<ArrayList<BTC>> {}
}
