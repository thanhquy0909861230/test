package com.deanshoes.AppQAM.fragment;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.deanshoes.AppQAM.R;
import com.deanshoes.AppQAM.adapter.ListOPAdapter;
import com.deanshoes.AppQAM.adapter.reportadapter.ListOpenMillAdapter;
import com.deanshoes.AppQAM.model.QAM.BTC_OP;
import com.deanshoes.AppQAM.model.response.ApiReturnModel;
import com.deanshoes.AppQAM.network.CallApi;
import com.deanshoes.AppQAM.step4.pdfActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class OpenMillFragment extends Fragment {
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    TextView tv_shift,tv_date;
    ListView rv_openmill;
    String factory="";
    public static String date="", shift ="", ins_no = "",MCS="";
    String FILE_NAME="";

    public static ArrayList<BTC_OP> BTC_OP = new ArrayList<>();

    public static OpenMillFragment newInstance(String param1) {
        OpenMillFragment fragment = new OpenMillFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_open_mill, container, false);
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        factory= pdfActivity.factory;
        MCS = pdfActivity.MCS;
        ins_no = pdfActivity.ins_no;
        date = pdfActivity.date;
        shift = pdfActivity.shift;

        FILE_NAME = ins_no+"_"+ MCS +"_"+date.replaceAll("/","-")
                +"_" + shift ;

        rv_openmill = getActivity().findViewById(R.id.rv_openmill);
        tv_shift = getActivity().findViewById(R.id.tv_shift_op);
        tv_date = getActivity().findViewById(R.id.tv_date_op);

        tv_shift.setText(shift);
        tv_date.setText(date);

        getList_OPENMILL();
    }

    public void getList_OPENMILL(){
        try{
            CallApi.getListOpenMill(factory, ins_no, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    BTC_OP = new ArrayList<>();
                    RP_BTCOPArm rp_btcopArm = gson.fromJson(response.toString(), RP_BTCOPArm.class);
                    BTC_OP = rp_btcopArm.getItems() == null ? new ArrayList<BTC_OP>() : rp_btcopArm.getItems();
                    for (int i=0;i<BTC_OP.size();i++){
                        BTC_OP.get(i).setMCS(MCS);
                    }
                    ListOpenMillAdapter listOpenMillAdapter = new ListOpenMillAdapter(getActivity(),R.layout.rp_openmill_adapter, BTC_OP);
                    rv_openmill.setAdapter(listOpenMillAdapter);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public static void createExcel(Workbook workbook, ArrayList<BTC_OP> listBTC) throws FileNotFoundException {
        try{
//            File ex = new File(Environment.getExternalStorageDirectory().toString() +
//                    FILE_NAME + ".xls");
//            if (!ex.exists()){
//                inputStream = am.open("excel_qam.xls");
//                workbook =getWorkbook(inputStream,"excel_qam.xls");
//            }else{
//                inputStream = new FileInputStream(ex);
//                workbook =getWorkbook(inputStream,FILE_NAME + ".xls");
//            }
            Sheet sheet = workbook.getSheetAt(2);
            Cell cell;
            Row row;
            int rownum=6;

            CellStyle style = createStyleForTitle(workbook);
            row = sheet.getRow(1);
            cell = row.getCell(1);
            cell.setCellValue(shift);
            cell.setCellStyle(style);

            cell = row.getCell(6);
            cell.setCellValue(date);
            cell.setCellStyle(style);

            //set list kneader
            for (int i = 0;i<listBTC.size();i++){
                row = sheet.createRow(rownum);
                cell = row.createCell(0);
                String s1 = MCS;
                String s2 = s1.replace('|','/');
                cell.setCellValue(s2);
                cell.setCellStyle(style);

                cell = row.createCell(1);
                cell.setCellValue(listBTC.get(i).getBTC_BATCH_NO());
                cell.setCellStyle(style);

                cell = row.createCell(2);
                cell.setCellValue(listBTC.get(i).getBTC_STYLE());
                cell.setCellStyle(style);

                cell = row.createCell(3);
                 cell.setCellValue(listBTC.get(i).getBTC_COLOR());
                cell.setCellStyle(style);

                cell = row.createCell(4);
                cell.setCellValue(listBTC.get(i).getOP_TEMP_1_NOTE() == null ? listBTC.get(i).getOP_TEMP_1() : listBTC.get(i).getOP_TEMP_1_NOTE());
                cell.setCellStyle(style);

                cell = row.createCell(5);
                cell.setCellValue(listBTC.get(i).getOP_THICK1_NOTE() == null ? listBTC.get(i).getOP_THICK_1() : listBTC.get(i).getOP_THICK1_NOTE());
                cell.setCellStyle(style);

//                cell = row.createCell(6);
//                cell.setCellValue(listBTC.get(i).getOP_TEMP_THIN_NOTE() == null ? listBTC.get(i).getOP_TEMP_THIN() : listBTC.get(i).getOP_TEMP_THIN_NOTE());
//                cell.setCellStyle(style);

                cell = row.createCell(6);
                cell.setCellValue(listBTC.get(i).getOP_THIN_NOTE() == null ? listBTC.get(i).getOP_THICK_THIN() : listBTC.get(i).getOP_THIN_NOTE());
                cell.setCellStyle(style);

//                cell = row.createCell(8);
//                cell.setCellValue(listBTC.get(i).getOP_TEMP_2_NOTE() == null ? listBTC.get(i).getOP_TEMP_2() : listBTC.get(i).getOP_TEMP_2_NOTE());
//                cell.setCellStyle(style);

                cell = row.createCell(7);
                cell.setCellValue(listBTC.get(i).getOP_THICK2_NOTE() == null ? listBTC.get(i).getOP_THICK_2() : listBTC.get(i).getOP_THICK2_NOTE());
                cell.setCellStyle(style);
                rownum++;
            }


//            File check = new File(Environment.getExternalStorageDirectory().toString() +
//                    "/QAM_EXCEL/" + FILE_NAME + ".xls");
//            if (check.exists()){
//                check.delete();
//            }
//            outFile = new FileOutputStream(check);
//            workbook.write(outFile);
//
//            System.out.println("Created file: " + check.getAbsolutePath());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            if (outFile != null) {
//                try {
//                    outFile.flush();
//                    outFile.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
        }
    }

    private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
        Workbook workbook = null;
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }

    private static CellStyle createStyleForTitle(Workbook workbook) {

        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        // Font Color
        font.setColor(IndexedColors.BLACK.getIndex());
        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setBorderLeft((short)1);
        style.setBorderRight((short)1);
        style.setBorderTop((short)1);
        style.setBorderBottom((short)1);
        style.setLocked(true);

        return style;
    }

    public class  RP_BTCOPArm extends ApiReturnModel<ArrayList<BTC_OP>> {}
}
